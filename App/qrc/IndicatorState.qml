import QtQuick 2.9
import QtQuick.Controls 2.2
import app.render 1.0
import Cicada 1.0

Item
{
    id: indicator
    height: parent ? parent.height : 0
    width: parent ? parent.height : 0
    property string icon: ""
    property string valueTip: "test"
    visible: false
    enabled:  true



    function setStateValue(value)
    {
        valueTip = value
        indicator.visible = true;
    }

    function setHidden()
    {
        indicator.visible = false;
    }


    ImageRender
    {
        id: image
        objectName: "IndicatorIcon"
        anchors.centerIn: parent
        height: 26
        width: 28
        scale: toolTipVisible ? 0.8 : 1
    }

    property bool toolTipVisible: false
    ToolTip.visible: toolTipVisible//timer.running
    ToolTip.text: valueTip
    ToolTip.onVisibleChanged:
    {
        if( ToolTip.visible != toolTipVisible )
        {
            toolTipVisible = ToolTip.visible
        }
    }

    Timer
    {
        id: timer
        running: false
        repeat: false
        interval: 2000
        onTriggered:
        {
            console.debug("TIMER FINISHHH")
        }
    }
    Button
    {
        id: buttonElem
        height: parent.height
        anchors.centerIn: parent
        width: parent.width
        enabled: true
        background: Rectangle{ opacity: 0 }


        onClicked:
        {
            if( valueTip.length === 0 )
                return
            toolTipVisible = !toolTipVisible
             //timer.restart()
        }
    }
}

