import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0



Rectangle
{
    id: monitoringVolumeWidget
    anchors.horizontalCenter: parent ? parent.horizontalCenter : undefined
    anchors.top: parent ? parent.top : undefined
    anchors.topMargin: 140
    height: 90
    width: 500
    radius: 26
    color: cc.blue
    z:5
    visible: false


    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    ShadowRect{type: "custom"; x:-6; y:6; radius: 26}



    function show(_val) //val from 0 to 1
    {
        if(!visible)
        {
            showAnimation.start()
        }
        visible = true
        timer.stop();
        timer.start();
        if(hideAnimation.running)
        {
            hideAnimation.stop()
            showAnimation.start()
        }
        sliderElem.value = _val;
    }


    Timer
    {
        id: timer
        running: false
        repeat: false
        interval: 1800
        onTriggered: hideAnimation.start()
    }

//    Timer //timer for test
//    {
//        id: timer2
//        running: true
//        repeat: true
//        interval: 3000
//        onTriggered: show(Math.random())
//    }



    Slider
    {
        id: sliderElem
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 16
        value: 0.5


        background: Rectangle {
            implicitWidth: 320
            implicitHeight: 30
            width: implicitWidth
            height: implicitHeight
            radius: 16
            color: cc.dark_blue

            layer.enabled: true
            layer.effect:
                OpacityMask
                {
                    maskSource:
                        Rectangle
                        {
                            anchors.centerIn: parent
                            width: sliderElem.width
                            height: sliderElem.height
                            radius: 16
                        }
                }

            Rectangle {
                y: parent.y
                height: parent.height
                width: sliderElem.value * parent.width
                color: cc.light_green
                radius: 16
            }
        }

        handle: Rectangle { opacity: 0 }


//        Rectangle // ( + )
//        {
//            anchors.verticalCenter: parent.verticalCenter
//            x: parent.width - width - 40
//            width: 40
//            height: 6
//            radius: 3
//            color: cc.blue

//            Rectangle
//            {
//                x: parent.width/2 - width/2
//                y: -height/2 + parent.height/2
//                width: 6
//                height: 40
//                radius: 3
//                color: cc.blue
//            }
//        }
//        Rectangle // ( - )
//        {
//            anchors.verticalCenter: parent.verticalCenter
//            x: 40
//            width: 40
//            height: 6
//            radius: 3
//            color: cc.blue
//        }

    }


    Button
    {
        id:blockZone
        anchors.fill: parent
        opacity: 0
    }


    Text
    {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S7 + 2
        color: cc.light_green
        text: qsTr("Громкость прослушивания")
    }



    NumberAnimation
    {
        id:showAnimation
        target: monitoringVolumeWidget
        property: "anchors.topMargin"
        duration: 400
        easing.type: Easing.OutCubic;
        from: -monitoringVolumeWidget.height
        to : 140
    }

    NumberAnimation
    {
        id:hideAnimation
        target: monitoringVolumeWidget
        property: "anchors.topMargin"
        duration: 300
        easing.type: Easing.Linear;
        from: 140
        to : -monitoringVolumeWidget.height

        onStopped: monitoringVolumeWidget.visible = false
    }

}



