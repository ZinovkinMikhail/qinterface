import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Item
{
    id: updateProgramWindow
    width: 1280
    height: 720
    y: -135
    visible: false
//    z:20

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}


    signal cancelButtonClicked()
    signal updateAdapterClicked()
    signal updateRecorderClicked()



    function setFirmwareInfo(_adapterVer, _recorderVer)
    {
        adapterVer.text = _adapterVer
        recorderVer.text = _recorderVer
    }

    function setProgress(_value)
    {
        progress.value = _value
    }


    Button
    {
        id:blockZone
        anchors.fill: parent

        background: Rectangle
        {
            color: cc.fog
            opacity:0.4
        }
    }


    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.topMargin: 165
        anchors.rightMargin: 80
        anchors.leftMargin: 80
        height: 420
        radius: 4
        color: cc.blue

        ShadowRect{type: "panel"}
    }

    Rectangle
    {
        id:mainZone
        objectName: "mainZone"
        anchors.fill: background
        anchors.margins: 10
        color: cc.dark_blue


        BackgroundTitle
        {
            x: parent.width - width - 25
            y: parent.height - height - 10
            font.pixelSize: 52
            text: qsTr("Обновление")
        }




        Item
        {
            id: line1
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 40
            width: mainZone.width - 120
            height: adapterUpdateButton.height

            Text
            {
                id:adapterLabel
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S5
                color: cc.light_green
                text: "Версия адаптера:"
            }

            Text
            {
                id:adapterVer
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S5
                color: cc.white
                text: "2.0-20-10-13-769-g0b0982d"
            }

            ControlListButton
            {
                id:adapterUpdateButton
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                type: "SlimType"
                text: qsTr("Обновить")
                onClicked: updateAdapterClicked()
            }
        }


        Item
        {
            id: line2
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 150
            width: mainZone.width - 120
            height: recorderUpdateButton.height


            Text
            {
                id:nakopitelLabel
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S5
                color: cc.light_green
                text: "Версия накопителя:"
            }

            Text
            {
                id:recorderVer
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S5
                color: cc.white
                text: "1.0"
            }

            ControlListButton
            {
                id:recorderUpdateButton
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                type: "SlimType"
                text: qsTr("Обновить")
                onClicked: updateRecorderClicked()
            }
        }


        ProgressBar
        {
            id: progress
            anchors.horizontalCenter: mainZone.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 100
            hoverEnabled: false
            from: 0.0
            to: 100.0
            value: 0

            background: Rectangle
            {
                implicitWidth: mainZone.width - 120
                implicitHeight: 34
                color: cc.blue
            }

            contentItem: Item
            {
                implicitWidth: 200
                implicitHeight: 4

                Rectangle
                {
                    width: progress.visualPosition * parent.width
                    height: parent.height
                    color: "#52dbb2"
                    opacity: 0.65
                }
            }

            Text
            {
                x:parent.width/2 - width/2
                y:parent.height/2 - height/2
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: fontConf.pixSize_S5
                color: cc.light_green
                text: progress.value + " %"
                z: 2
            }
        }


        ControlListButton
        {
            id:cancelButton
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 17
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked: cancelButtonClicked()
        }


//    CloseSmallButton
        Button
        {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: -30
            anchors.leftMargin: -40
            height: 60
            width: 70
            scale: down ? 0.8 : 1

            background: Rectangle
                {
                    radius: 14
                    color: cc.light_green
                }

            ShadowRect{x: -3; y: 3; radius: 14}

            Image
            {
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                scale: 0.5
                source: "qrc:///icons/cancel_button.png"
            }

            onClicked: cancelButtonClicked()
        }
      }
}
