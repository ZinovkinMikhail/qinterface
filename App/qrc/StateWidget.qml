import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2

Rectangle
{
    id: root

    property var indicatorComponent
    property var processComponent

    height: 66
    y: parent ? parent.height - height : 0
    width: parent ? parent.width : 0
    color: cc.blue
//    z: 3
    ColorConfig{id:cc}

    ListModel
    {
        id: stateWidget
    }

    Rectangle
    {
        id:row1
        objectName: "IndicatorRow"
        x: parent.width - width
        y: parent.height/2 - height/2
        height: parent.height
        width: parent.width / 2
        color: "transparent"
    }

    Rectangle
    {
        id:row2
        objectName: "ProcessRow"
        x: 30
        y: parent.height/2 - height/2
        height: 32
        width: parent.width / 2
        color: "transparent"
    }
}
