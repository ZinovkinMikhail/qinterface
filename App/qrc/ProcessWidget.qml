import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2


Rectangle
{
    id: root
    x: 333
    y: 20
    height: 110
    width: 614
    radius: 3
    color: cc.blue


    ColorConfig{id:cc}
    ShadowRect{type: "panel"}


    signal showChildWidget(string name , var item)


    function setFirstItem(item)
    {
        swView.prevItem = item
    }

    function setCurrentItem(item)
    {
        var list = swView.contentChildren
        for( var i = 0; i < list.length; i++ )
        {
            console.debug(swView.itemAt(i))
            if( swView.itemAt(i) === item )
            {
                swView.setCurrentIndex(i)
            }
        }
    }

    function removeItem(item)
    {
        var list = swView.contentChildren
        for( var i = 0; i < list.length; i++ )
        {
            console.debug(swView.itemAt(i))
            if( swView.itemAt(i) === item )
            {
                swView.removeItem(item)
            }
        }
    }


    function makeBig()
    {
        if(!playerSizeAnimation.makeBig)
        {
            blockFog.visible = true
            swView.interactive = false
            playerSizeAnimation.makeBig = true
            playerSizeAnimation.start()
            z = 2
            dots.visible = false
        }
    }

    function makeNormal()
    {
        blockFog.visible = false
        swView.interactive = true
        playerSizeAnimation.makeBig = false
        playerSizeAnimation.start()
        z = 0
        dots.visible = true
    }


    Button
    {
        id:blockFog
        x: -parent.x
        y: -parent.y
        width: 1280
        height: 720
        z:-1
        visible: false

        background: Rectangle { color: cc.fog; opacity: 0.4 }

        onClicked: makeNormal()
    }


    SwipeView
    {
        property var prevItem
        id: swView
        objectName: "SwipeView"
        anchors.fill: parent
        currentIndex: 0
        clip: true
        onCurrentItemChanged:
        {
            if( swView.prevItem )
                swView.prevItem.swipeDeselected()
            currentItem.swipeSelected()
            prevItem = currentItem
        }
    }


    DotsIndicator{id: dots; indicatorX: parent.width/2 - indicatorWidth/2
                  indicatorY: parent.height - indicatorHeight}



    ParallelAnimation
    {
        id: playerSizeAnimation

        property bool makeBig


        NumberAnimation
        {
            target: root
            property: "scale"
            duration: 100
            easing.type: Easing.InElastic;
            easing.amplitude: 2.0;
            easing.period: 1.5
            from: playerSizeAnimation.makeBig ? 1 : 1.4
            to : playerSizeAnimation.makeBig ? 1.4 : 1
        }

        NumberAnimation
        {
            target: root
            property: "height"
            duration: 100
            easing.type: Easing.InElastic;
            easing.amplitude: 2.0;
            easing.period: 1.5
            from: root.height
            to : playerSizeAnimation.makeBig ? root.height + 40 : root.height - 40
        }
        NumberAnimation
        {
            target: root
            property: "y"
            duration: 100
            easing.type: Easing.InElastic;
            easing.amplitude: 2.0;
            easing.period: 1.5
            from: root.y
            to : playerSizeAnimation.makeBig ? root.y + 50 : root.y - 50
        }
    }



}
