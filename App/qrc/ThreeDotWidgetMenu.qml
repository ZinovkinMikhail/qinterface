import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0

Rectangle
{
    id:root

    x: 110 - width
    y: parent.height + 20
    width: 298
    height: columnElem.height
    radius: 10
    color: cc.white
    visible: false


    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
    ShadowRect{x: -6; y: 6; radius: 10}

    signal closeButtonPressed()
    signal rebootButtonPressed()
    signal lpcButtonPressed()
    signal aboutButtonPressed()
    signal lockButtonPressed()
    signal lockDispButtonPressed()

    property var lockItem

    function setKeyboardButtonState(isLock)
    {
        if( isLock )
        {
            lockItem.iconSource = "qrc:/icons/unlock.png"
            lockItem.buttonText = "Разблок. клавиш"
        }
        else
        {
            lockItem.iconSource = "qrc:/icons/lock.png"
            lockItem.buttonText = "Блок. клавиш"
        }
    }

    function show()
    {
        root.visible = true
        root.z = 5
    }

    function hide()
    {
        root.visible = false
        root.z = 0

    }


    Button // blockZone fix
    {
        anchors.fill: parent
        background: Rectangle {opacity: 0}
    }

    Column
    {
        id:columnElem
        anchors.horizontalCenter: parent.horizontalCenter
        topPadding: 10
        bottomPadding: 10
        spacing: 10

        Loader
        {
            sourceComponent: buttonComponent
            onLoaded:
            {
                item.buttonText = "Клавиатура"
                item.objectName = "lock"
                item.iconSource = "qrc:/icons/lock.png"
                lockItem = item
            }
        }

        Rectangle //line
        {
            width:parent.width
            height: 2
            color: cc.blue
            opacity: 0.6
        }

        Loader
        {
            sourceComponent: buttonComponent
            onLoaded:
            {
                item.buttonText = "Блок. экран"
                item.objectName = "lockDisp"
                item.iconSource = "qrc:/icons/lock.png"
            }
        }

        Rectangle //line
        {
            width:parent.width
            height: 2
            color: cc.blue
            opacity: 0.6
        }


        Loader
        {
            sourceComponent: buttonComponent
            onLoaded:
            {
                item.buttonText = "О программе"
                item.objectName = "about"
                item.iconSource = "qrc:/icons/i21.png"
            }
        }

        Rectangle //line
        {
            width:parent.width
            height: 2
            color: cc.blue
            opacity: 0.6
        }

        Loader
        {
            sourceComponent: buttonComponent
            onLoaded:
            {
                item.buttonText = "Перезап. адаптера"
                item.objectName = "reboot"
                item.iconSource = "qrc:/icons/i22.png"
            }
        }

        Rectangle //line
        {
            width:parent.width
            height: 2
            color: cc.blue
            opacity: 0.6
        }

        Loader
        {
            sourceComponent: buttonComponent
            onLoaded:
            {
                item.buttonText = "П.Э.П. адаптера"
                item.objectName = "lpc"
                item.iconSource = "qrc:/icons/i23.png"
            }

        }
    }


    Component
    {
        id:buttonComponent

        Button
        {
            id:butt
            anchors.horizontalCenter: parent.horizontalCenter
            width: root.width
            height: 42
            scale: down ? 0.9 : 1


            onClicked:
            {
                if( objectName === "lpc" )
                    root.lpcButtonPressed()
                else if(objectName === "reboot")
                    root.rebootButtonPressed()
                else if( objectName === "about")
                    root.aboutButtonPressed()
                else if( objectName === "lock")
                    root.lockButtonPressed()
                else if( objectName === "lockDisp" )
                    root.lockDispButtonPressed()

                console.debug(objectName)
//                root.parent.openWidgetMenu = false
//                root.parent.parent.fullScreenButton(false, root.parent)
                root.closeButtonPressed()
            }

            property alias buttonText: txt.text
            property alias iconSource: icon.source

            background: Rectangle { opacity: 0 }


            Text
            {
                id:txt
                y:parent.height/2 - height/2
                leftPadding: 20
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6 + 4
                color: cc.blue
            }
            Image
            {
                id:icon
                x: parent.width - width - 20
                y:parent.height/2 - height/2
//                width: 28
//                height: 28
//                fillMode: Image.PreserveAspectFit
//                mipmap: true
                visible: false
                sourceSize.width: 28
                sourceSize.height: 28
            }
            ColorOverlay
            {
                anchors.fill: icon
                source: icon
                color: cc.blue
            }
        }
    }
}
