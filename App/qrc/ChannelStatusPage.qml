import Cicada 1.0
import QtQuick 2.9


Item
{
    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal recordButtonClicked()
    signal monitoringButtonClicked()

    function setStatusType(name)
    {
        channelType.text = name
    }

    function setSourceType(name)
    {
        sourceText.text = name
    }

    function setChanelStateStandBy()
    {
        recordButton.setState(false)
        monitoringButton.setState(false)
    }
    function setChanelStateMonitoring()
    {
        monitoringButton.setState(true)
    }
    function setChanelStateRecording()
    {
        recordButton.setState(true)
    }

    Text
    {
        id: channelType
        x: parent.width/2 - width/2
        y: 26 - height/2
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S6 + 4
        color: cc.light_green
        text: qsTr("")
    }

    ControlListButton
    {
        id: recordButton
        x: parent.width/2 - width/2
        y: 60
        type: "Record"
        onClicked: recordButtonClicked()
    }

    ControlListButton
    {
        id: monitoringButton
        x: parent.width/2 - width/2
        y: 160
        type: "Monitoring"
        onClicked: monitoringButtonClicked()
    }

    Text
    {
        id: sourceText
        x: parent.width/2 - width/2 - 5
        y: 225 + 19
        horizontalAlignment: Text.AlignHCenter
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S6 + 4
        color: cc.light_green
    }
}
