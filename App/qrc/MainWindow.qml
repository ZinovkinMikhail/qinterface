import Cicada 1.0
import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
//import QtQuick.Layouts 1.1
//import QtQuick.Controls.Styles 1.4
import QtQuick.VirtualKeyboard 2.2
import QtQuick.VirtualKeyboard.Settings 2.2


Window
{
    id: main
    objectName: "MainWindow"
    width: 1280
    height: 720
    title: qsTr("Hello Ivan")
    visible: true
    visibility: ApplicationWindow.FullScreen
    color: cc.dark_blue
    ColorConfig{id:cc}

    BusyDisplay
    {
        id: loadingwidget
        visible: false
    }

    Item
    {
        objectName: "RootWindow"
        id: root
        anchors.centerIn: parent
        width: 1280
        height: 720
        clip: true
        scale: parent.height / height >= parent.width / width ?
                parent.width / width : parent.height / height

//        property var fullScreenButtonCaller //for fogButton
        property bool stackOversize: false
        property bool stackViewIsBusy: stackView.busy

        signal closeFullScreenButton()

        function showLoading()
        {
            loadingwidget.visible = true
        }

        function hideLoading()
        {
            loadingwidget.visible = false
        }

        function showFullscreenButton()
        {
            fogButton.visible = true
        }

        function hideFullscreenButton()
        {
            fogButton.visible = false
        }

        function showInitialWindow(window)
        {
            stackView.push(window)
        }

        function showWindow(window, isOversize)
        {
            root.stackOversize = isOversize
            stackView.push(window)
        }

        function showPreviousWindow()
        {
            if( stackView.depth > 2 )
            {
                stackView.pop()
                root.stackOversize = false
            }
        }

//        function fullScreenButton(show, object)
//        {
//            fullScreenButtonCaller = object
//            fogButton.visible = show

//            if(show)
//            {
//               fullScreenButtonCaller.z = 4
//            }
//            else
//            {
//               fullScreenButtonCaller.z = 0
//            }
//        }



        StackView
        {
            id: stackView
            objectName: "stackViewTest"
            width: parent.width
            height: root.stackOversize ? 620 : 400
            y: root.stackOversize ? 20 : 150
            initialItem:
            {
                var component  = Qt.createComponent("windows/EmptyWindow.qml");
                return component.createObject();
            }
            z: 1

            property real inputPanelY: inputPanel.y
        }

        Button
        {
            id: fogButton
            anchors.fill: parent
            z: 4
            visible: false

//            onClicked:
//            {
//                root.fullScreenButtonCaller.openWidgetMenu = false
//                root.fullScreenButton(false ,root.fullScreenButtonCaller)
//            }

            background: Rectangle { color: cc.fog; opacity: 0.4 }
        }

        InputPanel
        {
            id: inputPanel
            z: 99
            x: 0
            y: root.height
            width: root.width

    //        KeyboardStyle.keyboardBackground: Rectangle {
    //            color: "black"
    //        }
            Component.onCompleted:
            {
    //            VirtualKeyboardSettings.styleName = "retro"
    //            KeyboardStyle.keyboardBackground = "white"

                VirtualKeyboardSettings.activeLocales = ["en_GB","ru_RU"]
                VirtualKeyboardSettings.locale = "ru_RU"
            }

            states: State {
                name: "visible"
                when: inputPanel.active
                PropertyChanges {
                    target: inputPanel
                    y: root.height - inputPanel.height
                }
            }
            transitions: Transition {
                from: ""
                to: "visible"
                reversible: true
                ParallelAnimation {
                    NumberAnimation {
                        properties: "y"
                        duration: 250
                        easing.type: Easing.InOutQuad
                    }
                }
            }
        }
    }
}
