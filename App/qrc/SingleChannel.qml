import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2



Rectangle
{
    property var itemClass
    property bool isEnabled: false

    id: singleChannel

    height: 440 //415
    width: 299
    radius: 3
    color: cc.blue

    ColorConfig{id:cc}
    ShadowRect{type: "panel"}

//    property string txt1

    Component.onCompleted:
    {
        console.debug("Signle channel completed")
    }


    Connections
    {
        target: itemClass
        onUpdateData:
        {
            console.debug("Update enable " + enabled)
            isEnabled = enabled
        }
    }

    Text
    {
        id:tex
        x: 30 + line1.width/2 - width/2
        y: 16
        font.family: "Source Sans Pro"
        font.pixelSize: 48
        color: cc.light_green
        text: itemClass.getName();
    }


    Rectangle //lines
    {
        x: 30
        y: 20
        width: 2
        height: parent.height - 40
        color: "#2d4752"


        Rectangle
        {
            id:line1
            x: 0
            y: 55
            width: 256
            height: 2
            color: "#2d4752"
        }
    }


    SwipeView
    {
        id: swView
        orientation: Qt.Vertical
        currentIndex:
        {
            return 0
        }

        x: 30
        y: 55 + 22
        width: 256
        height: parent.height - 55 - 20 - 20 + 18
        clip: true
        opacity: isEnabled ? 1 :  0.4

        onCurrentItemChanged:
        {
            if( currentIndex === 1 )
            {
                itemClass.reloadSettingsData()
            }
        }

        Component.onCompleted:
        {
            console.debug("Swipe view completed")
            var map = {"itemClass" : itemClass }
            var component = Qt.createComponent("ControlCommandList.qml");

            component.createObject(swView, map);
            component = Qt.createComponent("ControlParamsList.qml");
            component.createObject(swView, map);
        }
    }


    DotsIndicator{x: 27; y: parent.height/2 - indicatorWidth/2; rotation: 90}


}





