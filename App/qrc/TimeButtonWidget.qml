import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2



Rectangle
{
    id: timeButton
    objectName: "TimeWidget"
    x: 144
    y: 20
    height: 110
    width: 173
    radius: 3
    color: cc.blue

    signal buttonOpenClicked()
    signal buttonCloseClicked()

    function show()
    {
        timeButton.z = 4
    }

    function hide()
    {
        timeButton.z = 0
    }

    function setTime(time)
    {
        timeText.text = time;
    }


    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
    ShadowRect{type: "panel"}

    Text
    {
        id: timeText
        x: parent.width/2 - width/2
        y: parent.height/2 - height/2
        font.family: fontConf.controlFont
        font.pixelSize: fontConf.pixSize_S2
        color: cc.light_green
        text: qsTr("13:00")
    }

    Button
    {
        id: buttonElem
        anchors.fill: parent

        property bool opened: false

        background: Rectangle { opacity: 0 }
    }
}
