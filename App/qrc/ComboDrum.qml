import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0


Item
{
    property var itemClass

    id: mainBaraban
    x:  parent ? parent.width / 2 - width /2 : 0
    y: parent ? parent.height : 0
    height: 260
    width: parent ? parent.width - 90 : 0

    signal cancelComboDrum()
    signal closeComboDrum(var value)
    signal closeComboDrumTime(int hour, int minute, int seconds)
    signal closeComboDrumDate(int day, int month, int year)


    ColorConfig{id:cc}
    FontsConfig{id:fontConf}


    function openComboDrum(index, map)
    {
        barabanList.length = 0
        for (var prop in map)
        {
//            console.debug(prop, "=", map[prop])
            barabanList.push(map[prop])
        }
        tumb.model = 0
        tumb.model = barabanList.length
        tumb.currentIndex = index
        type = "default"
        changeHideAll(false)
    }


    function openComboDrumDate(day, mon, year)
    {
//        console.debug(day + "." + mon + "." + year)
        type = "date"
        changeHideAll(false)
        dateRow.selectDate(day, mon, year)
    }

    function openComboDrumTime(hour, min, sec)
    {
        type = "time"
        changeHideAll(false)
        if( sec === -1 )
            secondsTumb.visible = false
        else
            secondsTumb.visible = true
        timeRow.selectTime(hour, min)
    }

    property string type: ""
    onTypeChanged:
    {
        switch (type)
        {
          case "time":
            timeRow.visible = true
            dateRow.visible = false
            tumb.visible = false
            break;
          case "date":
            dateRow.visible = true
            timeRow.visible = false
            tumb.visible = false
            break;
          case "default":
            tumb.visible = true
            timeRow.visible = false
            dateRow.visible = false
            break;
        }
    }

    property bool hideAll: true

    property string drumColor: "#e5e8df"
    property string buttonColor: "#e5e8df"
    property string gradientsColor: "#e5e8df"
    property string textColor: cc.dark_blue
    property string greenlineColor: cc.light_green
    property string greenLineBorderColor: cc.blue

    property variant barabanList: ["ВЫКЛ.", "МОНО", "СТЕРЕО", "СТЕРЕО-2", "MEGA STEREO-2"]
//    property int selectedItem: -1
    property int centerShift: 23

    function showComboDrum()
    {
        hideAll = !hideAll
//        tumb.forceActiveFocus()
//        tumb.activeFocus = true
    }


    function timeFormat(modelData)
    {
        var data = modelData;
        return data.toString().length < 2 ? "0" + data : data;
    }


    function changeHideAll(hide)
    {
        if( hide === hideAll)
            return
//        console.debug("Hide all changed " + hide)
        if(!hide)
        {
//            if( barabanHideAnimation.running )
//            {
//                console.debug("SUKA STILL RUNNINGS")
//            }

            barabanHideAnimation.from = parent.height
            barabanHideAnimation.to = parent.height - height - 4
            barabanHideAnimation.duration = 200
            barabanHideAnimation.stop()
            barabanHideAnimation.start()
            mainBaraban.z = 5
            mainBaraban.visible = true
        }
        else
        {
            if( parent )
            {
                barabanHideAnimation.from = parent.height - height - 4
                barabanHideAnimation.to = parent.height
                barabanHideAnimation.duration = 500
                barabanHideAnimation.stop()
                barabanHideAnimation.start()
            }
        }
        hideAll = hide
    }


    onYChanged:
    {
        if( y < parent.height )
            visible = true
        else
            visible = false
    }

    NumberAnimation
    {
        id: barabanHideAnimation
        target: mainBaraban
        property: "y"
        easing.type: Easing.InElastic;
        easing.amplitude: 2.0; //bounce,elastic
        easing.period: 1.5 //elastic
    }


    Rectangle
    {
        id:baraban
        anchors.fill: parent
        radius: 30
        color: drumColor
        clip: true

        ShadowRect{x: -15; y: 30; radius: 26}

        Rectangle
        {
            id: greenLine
            width: parent.width
            height: mainBaraban.height/3
            anchors.centerIn: parent
            color: greenlineColor

            Rectangle
            {
                width: parent.width
                height: 3
                color: greenLineBorderColor
            }
            Rectangle
            {
                width: parent.width
                height: 3
                y: parent.height - height
                color: greenLineBorderColor
            }
        }

        Component
        {
           id: delegateComponent

           Rectangle
           {
               color: "transparent"
               opacity: Math.max(0, 1 - Math.pow(Tumbler.displacement, 4)*0.04)
               property real ttt: Tumbler.displacement
               property var mData: modelData

               Item
               {
                   anchors.centerIn: parent
                   scale: 1 - Math.pow(ttt, 4)*0.01

                   Text
                   {
                       id:txt
                       x: parent.width/2 - width/2
                       y:
                       {
                           if(ttt >= 0)
                           {
                               parent.height/2 - height/2 + Math.pow(ttt, 5)
                           }
                           else
                           {
                               parent.height/2 - height/2 + Math.pow(ttt, 5) // pow 4 = -, pow 3 = +
                           }
                       }

                       font.family: fontConf.mainFont
                       font.pixelSize: fontConf.pixSize_S4
                       color: textColor
                       text: type == "time" ? timeFormat(modelData) : modelData
                   }
               }
           }
        }


        Row
        {
            id: timeRow
            height: parent.height
            anchors.centerIn: parent
            spacing: 100
            visible: false

            function selectTime(hour, min)
            {
                houresTumb.currentIndex = hour
                minutesTumb.currentIndex = min
                secondsTumb.currentIndex = 0
            }

            Tumbler
            {
                id:houresTumb
                width: 80
                height: parent.height
                padding: -50
                model: 24

                delegate: delegateComponent
            }

            Tumbler
            {
                id:minutesTumb
                width: 80
                height: parent.height
                padding: -50
                model: 60

                delegate: delegateComponent
            }

            Tumbler
            {
                id:secondsTumb
                width: 80
                height: parent.height
                padding: -50
                model: 60

                delegate: delegateComponent
            }
        }


        Row
        {
            id: dateRow
            height: parent.height
            anchors.centerIn: parent
            spacing: 100
            visible: false

            function selectDate(day, mon, year)
            {
                var currentYear = new Date().getFullYear()
                yearTumb.model = getYears(currentYear - 50, currentYear + 50)
                yearTumb.currentIndex =  year - (currentYear - 50);
                monthTumb.currentIndex = mon - 1;
                dayTumb.currentIndex = day - 1;
            }

            function getYears(start, end)
            {
                var y = [];
                while (start <= end)
                {
                    y.push(start++)
                }
                return y;
            }

            Tumbler
            {
                id:dayTumb
                width: 80
                height: parent.height
                padding: -50
                model: ListModel{id:daysModel}

                readonly property var daysInMonth: [31,28,31,30,31,30,31,31,30,31,30,31]

                function initModel()
                {
                    for(var i = 1; i < 32; i++)
                    {
                        daysModel.append({"day": i})
                    }
                }

                function updateModel()
                {
                    var daysCount = daysInMonth[monthTumb.currentIndex]
                    var selectedYear = yearTumb.currentItem.mData
                    if(selectedYear % 4 == 0 || selectedYear % 100 != 0 && selectedYear % 400 == 0) //Leap year
                    {
                        if(monthTumb.currentIndex == 1)
                        {
                            daysCount++;
                        }
                    }

                    while(daysModel.count < daysCount)
                    {
                        daysModel.append({"day": daysModel.count+1})
                    }
                    while(daysModel.count > daysCount)
                    {
                        daysModel.remove(daysModel.count-1)
                    }
                }

                Component.onCompleted: initModel()

                delegate: delegateComponent
            }

            Tumbler
            {
                id:monthTumb
                width: 80
                height: parent.height
                padding: -50
                model: [1,2,3,4,5,6,7,8,9,10,11,12]

                onCurrentIndexChanged: dayTumb.updateModel()

                delegate: delegateComponent
            }

            Tumbler
            {
                id:yearTumb
                width: 80
                height: parent.height
                padding: -50
//                model: getYears
                wrap: false

                onCurrentIndexChanged: dayTumb.updateModel()

                delegate: delegateComponent
            }
        }



        Tumbler
        {
            id: tumb
            anchors.fill: parent
            model: 5
            wrap: false
            padding: -50

            delegate:
                Rectangle
                {
                    color: "transparent"
                    opacity: Math.max(0, 1 - Math.pow(Tumbler.displacement, 4)*0.04)
                    property real ttt: Tumbler.displacement

                    Rectangle
                    {
                        anchors.centerIn: parent
                        width: 700
                        color: "transparent"
                        scale: 1 - Math.pow(ttt, 4)*0.01

                        Rectangle
                        {
                            x:0
                            y: txt.y + txt.height/2 - height/2
                            width: txt.width > 200 ? 300 - txt.width/2 : 200
                            height: 2
                            color: cc.blue
                        }

                        Text
                        {
                            id:txt
                            x: parent.width/2 - width/2
                            y:
                            {
                                if(ttt >= 0)
                                {
                                    parent.height/2 - height/2 + Math.pow(ttt, 5)
                                }
                                else
                                {
                                    parent.height/2 - height/2 + Math.pow(ttt, 5) // pow 4 = -, pow 3 = +
                                }
                            }
                            font.family: fontConf.mainFont
                            font.pixelSize: fontConf.pixSize_S4
                            color: textColor
                            text: qsTr(barabanList[index])
                        }

                        Rectangle
                        {
                            x: parent.width - width
                            y: txt.y + txt.height/2 - height/2
                            width: txt.width > 200 ? 300 - txt.width/2 : 200
                            height: 2
                            color: cc.blue
                        }
                    }
                }
        }



        Item
        {
            id: barabanGrad
            anchors.fill: parent

            layer.enabled: true
            layer.effect:
                OpacityMask
                {
                    maskSource:
                        Rectangle
                        {
                            anchors.centerIn: parent
                            width: barabanGrad.width
                            height: barabanGrad.height
                            radius: baraban.radius
                        }
                }

            Rectangle
            {

                width: parent.width
                height: mainBaraban.height/3
                anchors.top: parent.top
                gradient:
                    Gradient
                    {
                        GradientStop { position: 0.0; color: gradientsColor }
                        GradientStop { position: 1.5; color: "transparent" }
                    }
            }

            Rectangle
            {

                width: parent.width
                height: mainBaraban.height/3
                anchors.bottom: parent.bottom
                gradient:
                    Gradient
                    {
                        GradientStop { position: 1.1; color: gradientsColor }
                        GradientStop { position: -0.5; color: "transparent" }
                    }
            }
        }
    }



    Rectangle
    {
        id: buttonOK
        x: baraban.x - width/2 + 28
        anchors.bottom: baraban.bottom
        height: mainBaraban.height/2.4
        width: height*1.15 //76
        radius: 14
        color: buttonColor

        ShadowRect{x: -6; y: 6; radius: 16}

        Image
        {
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            scale: 0.6
            source: "qrc:/icons/ok_button.png"
        }


        MouseArea
        {
            anchors.fill: parent

            onClicked:
            {
//                console.debug("ACCCEPT ")
                changeHideAll(true)
                if(type == "time")
                {
                    mainBaraban.closeComboDrumTime(houresTumb.currentIndex,
                                                   minutesTumb.currentIndex, secondsTumb.currentIndex)
                }
                else if(type == "date")
                {
                    mainBaraban.closeComboDrumDate(dayTumb.currentItem.mData,
                                                   monthTumb.currentItem.mData, yearTumb.currentItem.mData)
                }
                else
                {
                    mainBaraban.closeComboDrum(tumb.currentIndex)
                }
            }

            onPressedChanged:
            {
                if(pressed)
                {
                    buttonOK.scale = 0.95
                }
                else
                {
                    buttonOK.scale = 1
                }
            }
        }
    }


    Rectangle
    {
        id: buttonCancel
        x: baraban.x + baraban.width - width/2 - 28
        anchors.bottom: baraban.bottom
        height: mainBaraban.height/2.4 //66
        width: height*1.15 //76
        radius: 14
        color: buttonColor

        ShadowRect{x: -6; y: 6; radius: 16}

        Image
        {
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            scale: 0.6
            source: "qrc:/icons/cancel_button.png"
        }



        MouseArea
        {
            anchors.fill: parent

            onClicked:
            {
//                console.debug("CANCELLL")
                changeHideAll(true)
                mainBaraban.cancelComboDrum()
            }

            onPressedChanged:
            {
                if(pressed)
                {
                    buttonCancel.scale = 0.95
                }
                else
                {
                    buttonCancel.scale = 1
                }
            }
        }
    }
}



