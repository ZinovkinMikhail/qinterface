import Cicada 1.0
import QtQuick 2.9
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.2


Item
{
    id: channelControl

    ListModel
    {
        id: channelsModel
    }

    BackgroundTitle
    {
        text: qsTr("Янтра-4Ц")
    }

    Row
    {
        id: row1
        objectName: "Row"
        x:18
        spacing: 16
    }
}
