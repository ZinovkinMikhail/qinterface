import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2

Rectangle
{
    property var firstItemClass
    property var secondItemClass
    property bool isEnabled: false

    property bool unionType: false

    id: root
    height: 440
    width: unionType ? 614 : 299
    radius: 3
    color: cc.blue

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
    ShadowRect{type: "panel"}

    signal updateParamsData()

    function setChannelName(name)
    {
        channelName.text = name
    }

    function setSecondChannelName(name)
    {
        secondChannelName.text = name
    }

    function hideChannel()
    {
        root.visible = false
    }

    function showChannel()
    {
        root.visible = true
    }

    function setSingleState()
    {
        unionType = false
    }

    function setUnionState()
    {
        unionType = true
    }

    function setChannelEnabled(enabled)
    {
        isEnabled = enabled
    }

    Text
    {
        id:channelName
        x: unionType ? 172.5 - width/2 : 158 - width/2
        y: 16
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S3
        color: cc.light_green
    }

    Text
    {
        id: secondChannelName
        x: 30 + 570 - 570/4 - width/2
        y: 16
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S3
        color: cc.light_green
        visible: unionType
        text: "Set"
    }


    Rectangle //Lines
    {
        x: 30
        y: 20
        width: 2
        height: parent.height - 40
        color: "#2d4752"


        Rectangle
        {
            id:line1
            x: 0
            y: 55
            width: unionType ? 570 : 256
            height: 2
            color: "#2d4752"

        }

        Rectangle
        {
            id:line2
            x: 299 + 7 - 30
            y: 0
            width: 2
            height: 55
            color: "#2d4752"
            visible: unionType
        }
    }


    SwipeView
    {
        id: swView
        objectName: "SwipeView"
        orientation: Qt.Vertical
        currentIndex: 0
        x: 30
        y: 77
        width: parent.width - 40
        height: parent.height - 77
        clip: true
        enabled: isEnabled
        opacity:
        {
//            if( isEnabled )
//                return 1
//            else
//                return 0.4
        }

        onCurrentItemChanged:
        {
            if( currentIndex === 1 )
            {
                root.updateParamsData()
            }
        }
    }


    DotsIndicator{x: 27; y: parent.height/2 - indicatorWidth/2; rotation: 90}

}


