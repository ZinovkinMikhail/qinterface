import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2


Rectangle
{
    property var buttonsComponent
    property var itemClass
    property var pointerPosX: parent ? parent.pointerPosX : 0

    id: root
    x: 18 + 30
    y: 175
    height: 220
    width: 1280 - 36 - 60
    color: cc.dark_blue
    ColorConfig{id:cc}
    visible: false

    Rectangle
    {
        height: 7
        width: parent.width
        color: "#50000000"
    }

    Rectangle
    {
        id: pointer
        x: pointerPosX + 52
        y: -10
        height: 70
        width: 70
        rotation: 45
        color: cc.dark_blue

        Rectangle
        {
            height: 44
            width: 7
            color: "#50000000"
        }
        Rectangle
        {
            x: 16
            y: -10
            height: 80
            width: 7
            rotation: 45
            color: cc.dark_blue
        }
    }

    Rectangle
    {
        y: parent.height
        width: parent.width
        height: 20
        color: cc.blue
    }

    Flickable
    {
        y:7
        width: parent.width
        height: parent.height - 7
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds
        contentWidth: row2.width;
        contentHeight: row2.height
        bottomMargin: 40 - 7
        clip: true
//        pressDelay: 30 //bug with touchscreen

        ScrollBar.vertical: ScrollBar
        {
            policy: row2.children.length > 16 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
        }

        Grid
        {
            id: row2
            objectName: "FolderRow"
            x:24
            y: children.length > 8 ? 15 : 25
            width: root.width
            spacing: 42
            columns: 8
            rowSpacing: 20
        }
    }
}
