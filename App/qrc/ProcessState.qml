import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import app.render 1.0

Item
{
    id: process
    height: parent ? parent.height : 0
    width: 70
   // scale: buttonElem.down ? 0.8 : 1
    visible: false
    property var itemClass

    property string txt: ""
    property string icon: ""

    function setStateValue(value)
    {
        txt = value
        process.visible = true;
    }

    function setHidden()
    {
        process.visible = false;
    }

    function setPixmap(image)
    {
        img.source = image
    }

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    Text
    {
        x: img.width + 5
        y: parent.height/2 - height/2
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S7 + 2
        text: txt
        opacity: enabled ? 1.0 : 0.3
        color: cc.light_green
    }

    ImageRender
    {
        objectName: "ProcessIcon"
        id: img
        y: parent.height/2 - height/2
        width: 28
        height: 28
    }
}

