import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2


Rectangle
{
    id:root
    x: 1152
    y: 20
    height: 110
    width: 110
    radius: 3
    scale: buttonElem.down ? 0.95 : 1
    color: cc.blue

    property var itemClass    
    property var volumeButtonClass
    property bool openWidgetMenu: false

    signal rebootButtonClicked()
    signal lpcButtonClicked()
    signal aboutButtonClicked()
    signal lockButtonClicked()
    signal lockDispButtonClicked()

    signal buttonOpenClicked()
    signal buttonCloseClicked()

    function show()
    {
        widgetMenu.show()
        root.z = 4
        blockZone.z = -4
        blockZone.visible = true
    }

    function hide()
    {
        widgetMenu.hide()
        root.z = 0
        blockZone.z = 0
        blockZone.visible = false
    }

    function setKeyboardButtonState(isLock)
    {
        widgetMenu.setKeyboardButtonState(isLock)
    }

    ColorConfig{id:cc}
    ShadowRect{type: "panel"; opacity: buttonElem.down ? 0 : 1}


    Button
    {
        id:blockZone
        x:-parent.x
        y:-parent.y
        width:1280
        height: 720
        scale: 1.2
        visible: false

        background: Rectangle
        {
            color: cc.fog
            opacity:0.4
        }

        onClicked:
        {
            root.buttonCloseClicked();
            buttonElem.opened = false
        }
    }


    Column //Icon
    {
        x: parent.width/2 - width/2
        y: parent.height/2 - height/2
        spacing: 5

        Repeater
        {
            model: 3
            Rectangle
            {
                height: 18
                width: 18
                radius: width / 2
                color: cc.light_green
            }
        }
    }


    Button
    {
        id: buttonElem
        anchors.fill: parent

        property bool opened: false

        background: Rectangle { opacity: 0 }

        onClicked:
        {
            if( !opened )
                root.buttonOpenClicked();
            else
                root.buttonCloseClicked();
            opened = !opened

//            root.openWidgetMenu = !root.openWidgetMenu
//            root.parent.fullScreenButton(root.openWidgetMenu, root)
        }
    }

    ThreeDotWidgetMenu
    {
        id:widgetMenu; visible: false
        onCloseButtonPressed:
        {
            if( buttonElem.opened )
                root.buttonCloseClicked();
            buttonElem.opened = false
        }
        onRebootButtonPressed:
        {
            rebootPressDelay.start()
        }
        onLpcButtonPressed:
        {
            lpcPressDelay.start()
        }
        onAboutButtonPressed:
        {
            root.aboutButtonClicked()
        }
        onLockButtonPressed:
        {
            root.lockButtonClicked()
        }
        onLockDispButtonPressed:
        {
            root.lockDispButtonClicked()
        }
    }


    Timer
    {
        id: rebootPressDelay
        running: false
        repeat: false
        interval: 10
        onTriggered: root.rebootButtonClicked()
    }

    Timer
    {
        id: lpcPressDelay
        running: false
        repeat: false
        interval: 10
        onTriggered: root.lpcButtonClicked()
    }
}
