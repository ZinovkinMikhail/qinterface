import QtQuick 2.0
import QtQuick.Controls 2.2
import Cicada 1.0

Item
{
    id: root
    width: 1280
    height: 720
    signal sendUserReadRecorderParams()
    signal setMemoryData(string flashCapacity, string curCapacity, string curRecTime, string fullRecTime)

    Rectangle
    {
        anchors.fill: parent
        color: cc.dark_blue
        ColorConfig{id:cc}

        Button
        {
            id: statusButton
            width: 150
            height: 50
            x: 10
            y: 10
            text: "Запрос статуса"

            onClicked:
            {
                root.sendUserReadRecorderParams()
            }
        }

        Rectangle
        {
            id: flashData
            x: statusButton.width + 20
            y: 10

            Text
            {
                id: flashCapacityText
                text: "Flash Capacity:"
                color: "white"
                y: flashCapacity.height / 2 - flashCapacityText.height /2
            }

            TextField
            {
                id: flashCapacity
                x: flashCapacityText.width + 5
                text: "50000"
            }

            Text
            {
                id: curCapacityText
                text: "Cur capacity:"
                color: "white"
                y: curCapacity.height / 2 - curCapacity.height /2 + flashCapacity.y + flashCapacity.height + 20
            }

            TextField
            {
                id: curCapacity
                x: curCapacityText.width + 5
                y: flashCapacity.y + flashCapacity.height + 20
                text: "5000"
            }

            Text
            {
                id: curRecTimeText
                text: "Cur rec time:"
                color: "white"
                y: curRecTime.height / 2 - curRecTime.height /2 + curCapacity.y + curCapacity.height + 20
            }

            TextField
            {
                id: curRecTime
                x: curRecTimeText.width + 5
                y: curCapacity.y + curCapacity.height + 20
                text: "5000"
            }

            Text
            {
                id: fullRecTimeText
                text: "Left time:"
                color: "white"
                y: curRecTime.height / 2 - curRecTime.height /2 + curRecTime.y + curRecTime.height + 20
            }

            TextField
            {
                id: fullRecTime
                x: fullRecTimeText.width + 5
                y: curRecTime.y + curRecTime.height + 20
                text: "5000"
            }

            Button
            {
                id: applyButt
                onClicked:
                {
                    root.setMemoryData(flashCapacity.text, curCapacity.text, curRecTime.text, fullRecTime.text)
                }
            }
        }


    }
}
