import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0



Rectangle
{
    id: root
    y: parent.height + 20
    height: 440
    width: 173
    radius: 3
    color: cc.blue

    ColorConfig{id:cc}
    ShadowRect{type: "panel"}

    signal volumeChanged(var value)

    property var sliderValue: sliderElem.value

    function setVolume(volume)
    {
        sliderElem.value = volume
    }

    function show()
    {
        root.visible = true
        root.z = 2
    }

    function hide()
    {
        root.visible = false
        root.z = 0
    }

    Slider
    {
        id: sliderElem
        orientation: Qt.Vertical
        anchors.centerIn: root
        value: 0.0
        onValueChanged:
        {
            root.volumeChanged(value)
        }

        background: Rectangle {
            implicitWidth: 100
            implicitHeight: 400
            width: implicitWidth
            height: implicitHeight
            radius: 16
            color: cc.dark_blue

            layer.enabled: true
            layer.effect:
                OpacityMask
                {
                    maskSource:
                        Rectangle
                        {
                            anchors.centerIn: parent
                            width: sliderElem.width
                            height: sliderElem.height
                            radius: 16
                        }
                }

            Rectangle {
                y: parent.height - (sliderElem.value * parent.height)
                height: sliderElem.value * parent.height
                width: parent.width
                color: cc.light_green
                radius: 16
            }
        }

        handle: Rectangle { opacity: 0 }
    }


    Rectangle // ( + )
    {
        x: parent.width/2 - width/2
        y: sliderElem.y + 50
        width: 40
        height: 6
        radius: 3
        color: cc.blue

        Rectangle
        {
            x: parent.width/2 - width/2
            y: -height/2 + parent.height/2
            width: 6
            height: 40
            radius: 3
            color: cc.blue
        }
    }
    Rectangle // ( - )
    {
        x: parent.width/2 - width/2
        y: sliderElem.y + sliderElem.height - height - 50
        width: 40
        height: 6
        radius: 3
        color: cc.blue
    }
}
