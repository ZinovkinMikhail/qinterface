import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2


Rectangle
{
    id:root
    x: 963
    y: 20
    height: 110
    width: 173
    radius: 3
    scale: buttonElem.down ? 0.95 : 1
    color: cc.blue

    property var volumeButtonClass
    property bool openWidgetMenu: false

    signal buttonOpenClicked()
    signal buttonCloseClicked()

    function show()
    {
        widgetMenu.show()
        root.z = 4
        blockZone.z = -4
        blockZone.visible = true
    }

    function hide()
    {
        widgetMenu.hide()
        root.z = 0
        blockZone.z = 0
        blockZone.visible = false
    }

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
    ShadowRect{type: "panel"; opacity: buttonElem.down ? 0 : 1}


    Button
    {
        id:blockZone
        x:-parent.x
        y:-parent.y
        width:1280
        height: 720
        scale: 1.2
        visible: false

        background: Rectangle
        {
            color: cc.fog
            opacity:0.4
        }

        onClicked:
        {
            root.buttonCloseClicked();
            buttonElem.opened = false
        }
    }


    Text
    {
        x: parent.width/2 - width/2 - 35
        y: parent.height/2 - height/2
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S5
        color: cc.light_green
        text: Math.round(widgetMenu.sliderValue*100) + "%"
    }


    Image
    {
        x: 65
        fillMode: Image.PreserveAspectFit
        scale: 0.5
//        mipmap: true
        source: "qrc:/icons/i12.png"
    }

    Button
    {
        id: buttonElem
        anchors.fill: parent

        property bool opened: false

        background: Rectangle { opacity: 0 }

        onClicked:
        {
            if( !opened )
                root.buttonOpenClicked();
            else
                root.buttonCloseClicked();
            opened = !opened

//            root.openWidgetMenu = !root.openWidgetMenu
//            root.parent.fullScreenButton(root.openWidgetMenu, root)
        }
    }

    VolumeWidgetSlider
    {
        id:widgetMenu;
        visible: false
        objectName: "VolumeSliderWidget"
    }
}
