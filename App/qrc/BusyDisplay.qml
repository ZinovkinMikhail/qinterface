import QtQuick 2.9
import QtQuick.Controls 2.2


Button
{
    id:busyDisplay
    anchors.fill: parent
    opacity: 0
    z: 2

    background: Rectangle { color: "#6c7980"; opacity: 0.5 }

    onVisibleChanged:
    {
        visible ? opacity = 1 : opacity = 0
    }


//    Behavior on opacity {
//        OpacityAnimator {
//            duration: 1000
//        }
//    }

    BusyIndicator
    {
        id: control
        anchors.centerIn: parent
        visible: busyDisplay.visible
        running: busyDisplay.visible

        contentItem: Item {
            implicitWidth: 200
            implicitHeight: 200

            Item {
                id: item1
                width: 200
                height: 200
                opacity: control.running ? 1 : 0

                Behavior on opacity {
                    OpacityAnimator {
                        duration: 500
                    }
                }

                RotationAnimator {
                    target: item1
                    running: control.visible && control.running
                    from: 0
                    to: 360
                    loops: Animation.Infinite
                    duration: 3000
                }

                Repeater {
                    id: repeater
                    model: 6

                    Rectangle {
                        x: item1.width / 2 - width / 2
                        y: item1.height / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: implicitWidth/2
                        color: "#fcfff5"
                        transform: [
                            Translate {
                                y: -Math.min(item1.width, item1.height) * 0.5 + 5
                            },
                            Rotation {
                                angle: index / repeater.count * 360
                                origin.x: 10
                                origin.y: 10
                            }
                        ]
                    }
                }
            }
        }
    }
}
