import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2



Item
{
    id: mainMenu
    property var component
    property var folder
    property var currentFolder : null

    property real pointerPosX: -1


    ListModel
    {
        id: folders
    }


    function addFolder(name, object)
    {
        var data = {'name': name, 'object': object};
        folders.append(data)
    }

    function showFolder(name)
    {
        for( var index = 0; index < folders.count; index++)
        {
            if( folders.get(index).name === name )
            {
                folders.get(index).object.visible = true
            }
        }
    }

    function hideAllFolders()
    {
        for( var index = 0; index < folders.count; index++)
        {
            folders.get(index).object.visible = false
        }
    }

    BackgroundTitle
    {
        text: qsTr("Янтра-4Ц")
    }

    Rectangle
    {
        x:18
        height: 415
        width: 1280 - 36
        radius: 3
        color: cc.blue
        ColorConfig{id:cc}
        ShadowRect{type: "panel"}

        Item
        {
            anchors.fill: parent
            clip: true

            Flickable
            {
                id:menuButtonsScroll
                width: parent.width
                height: row1.height
                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.StopAtBounds
                contentWidth: row1.width; contentHeight: row1.height
                bottomMargin: row1.height - parent.height + 80
//                pressDelay: 30 //bug with touchscreen

                ScrollBar.vertical:sb

                Grid
                {
                    id: row1
                    objectName: "MenuRow"
                    x:65
                    y:43
                    spacing: 62
                    columns: 7

                    function openFolderClick(posX, posY)
                    {
                        if(mainMenu.pointerPosX === posX) //folder is open
                        {
                            hideAllFolders();
                            mainMenu.pointerPosX = -1;
                            menuButtonsScroll.bottomMargin = row1.height - menuButtonsScroll.parent.height + 80;
                            menuButtonsScroll.interactive = true;
                            return false;
                        }
                        else
                        {
                            hideAllFolders();
                            mainMenu.pointerPosX = posX;
                            menuButtonsScroll.interactive = false;
                            menuButtonsScroll.bottomMargin = posY;
                            menuButtonsScroll.flick(0, -800 - posY);
                        }
                        return true;
                    }
                }
            }
        }

        ScrollBar
        {
            id:sb
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            policy: row1.children.length > 21 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
        }
    }
}
