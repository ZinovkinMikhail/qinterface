import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Item
{
    id: confirmationWindow
    anchors.fill: parent
    z:15
    visible: false

    signal userApproval(bool value)

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    function requestUserConfirm(msg)
    {
        messageText = msg
        confirmationWindow.visible = true
    }

//    property string messageText: "Текст сообщения для сообщения сообщения. Посмотри на это сообщение и продолжай делать, что делал."
    property string messageText: "Текст сообщения для сообщения сообщения."


    Button
    {
        id:blockZone
        anchors.fill: parent

        background: Rectangle
        {
            color: cc.fog
            opacity: 0.5
        }
    }


    Rectangle
    {
        id:mainZone
        anchors.centerIn: parent

        height: 250
        width: 650
        radius: 4
        color: cc.dark_blue

        ShadowRect{type: "panel"}


        Text
        {
            x:parent.width/2 - width/2
            y:parent.height/2 - height/2 - 40
            width: parent.width - 200
            height: 100
            font.family: fontConf.mainFont
            fontSizeMode: Text.Fit
            minimumPixelSize: 10
            font.pixelSize: fontConf.pixSize_S5 + 2
            wrapMode: Text.Wrap
//            elide: Text.ElideMiddle
            color: cc.light_green
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: messageText
        }

        Item
        {
            id:buttons
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenterOffset: 70
            width: parent.width
            height: confirmButton.height



            ControlListButton
            {
                id:confirmButton
                anchors.left: parent.left
                anchors.leftMargin: 50
                type: "SlimType"
                text: qsTr("Подтвердить")

                onClicked: confirmationWindow.userApproval(true)
            }

            ControlListButton
            {
                id:cancelButton
                anchors.right: parent.right
                anchors.rightMargin: 50
                type: "SlimType"
                text: qsTr("Отменить")

                onClicked: confirmationWindow.userApproval(false)
            }
        }
    }


}
