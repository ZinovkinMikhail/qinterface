import Cicada 1.0
import QtQuick 2.9


Item
{
    property var itemClass


    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
    id:root

    signal showSensComboDrum();
    signal showFiltrComboDrum();
    signal showBiasComboDrum();
    signal applySettingsParams();

    property  string tmpSensVal: ""
    property string tmpFiltrVal: ""
    property string tmpBiasVal: ""
    property  string preSensVal: ""
    property string preFiltrVal: ""
    property string preBiasVal: ""

    function paramsDataSetted(name)
    {
        if(name === "all")
        {
            biasDropButton.setState(false)
            filtrDropButton.setState(false)
            sensivityDropButton.setState(false)
            tmpSensVal = preSensVal;
            tmpFiltrVal = preFiltrVal;
            tmpBiasVal = preBiasVal;
        }
        else if(name === "sensitivity")
        {
            sensivityDropButton.setState(false)
            tmpSensVal = preSensVal;
        }
    }

    function setSensivityType(name, comboDrum)
    {
        if( comboDrum )
        {
            if( name === tmpSensVal )
            {
                sensivityDropButton.setState(false)
            }
            else
            {
                sensivityDropButton.setState(true)
            }
        }
        else
        {
            sensivityDropButton.setState(false)
            tmpSensVal = name
        }
        preSensVal = name
        sensivityDropButton.txttxt = name
    }

    function setFiltrType(name, comboDrum)
    {
        if( comboDrum )
        {
            if( name === tmpFiltrVal )
            {
                filtrDropButton.setState(false)
            }
            else
            {
                filtrDropButton.setState(true)
            }
        }
        else
        {
            filtrDropButton.setState(false)
            tmpFiltrVal = name
        }
        preFiltrVal = name
        filtrDropButton.txttxt = name

    }

    function setBiasType(name, comboDrum)
    {
        if( comboDrum )
        {
            if( name === tmpBiasVal )
            {
                biasDropButton.setState(false)
            }
            else
            {
                biasDropButton.setState(true)
            }
        }
        else
        {
           biasDropButton.setState(false)
           tmpBiasVal = name
        }
        preBiasVal = name
        biasDropButton.txttxt = name

    }

    Text
    {
        x: parent.width/2 - width/2
        y: sensivityDropButton.y - 25
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S7
        color: cc.light_green
        text: qsTr("Чувствительность")
    }

    Text
    {
        x: parent.width/2 - width/2
        y: filtrDropButton.y - 25
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S7
        color: cc.light_green
        text: qsTr("Фильтр")
    }

    Text
    {
        x: parent.width/2 - width/2
        y: biasDropButton.y - 25
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S7
        color: cc.light_green
        text: qsTr("Добавочное питание")
        visible: false
    }



    ButtonDrop
    {
        id: sensivityDropButton
        x: parent.width/2 - width/2
        y: 15 + height
        type: "default"
        txttxt: ""

        onButtonClick: root.showSensComboDrum();
    }

    ButtonDrop
    {
        id: filtrDropButton
        x: parent.width/ 2 - width/ 2
        y: sensivityDropButton.y + height + 40
        type: "default"
        txttxt: ""

        onButtonClick: root.showFiltrComboDrum();
    }

    ButtonDrop
    {
        id: biasDropButton
        x: parent.width/2 - width/2
        y: filtrDropButton.y + height + 30
        type: "default"
        txttxt: ""
       // states: "off"
        visible: false

        onButtonClick: root.showBiasComboDrum();
    }


    ControlListButton
    {
        x: parent.width / 2 - width / 2
        y: 270
        type: "Apply"

        onClicked:
        {
            root.applySettingsParams()
        }
    }
}
