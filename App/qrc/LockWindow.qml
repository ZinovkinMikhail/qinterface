import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0
//import app.render 1.0



Item
{
    id: lockWindow
    width: 1280
    height: 720
    z:10
    visible: false


    signal requestAccess(var pass)


    function show()
    {
        visible = true
        enteredNumbers = ""
    }

    function accessDenied()
    {
        accessDeniedAnimation.start()
        enteredNumbers = ""
    }

    function showErrorScreen(msg)
    {
        errorFullScreen.visible = true
        errorText.text = msg
    }

    function hideErrorScreen()
    {
        errorFullScreen.visible = false
        errorText.text = "no message"
    }

    function accessReset()
    {
        enteredNumbers = ""
        resetPassText.text = " Сброс пароля\n Введите дополнительный код."
        resetPassText.visible = true
        console.debug("AccessReset")
    }

    function accessResetApproved()
    {
        resetPassText.visible = false
        enteredNumbers = ""
        console.debug("AccessResetApproved")
    }

    function accessResetFailed()
    {
        resetPassText.visible = false
        accessDeniedAnimation.start()
        enteredNumbers = ""
        console.debug("AccessResetFailed")
    }
    function accessResetCancel()
    {
        resetPassText.visible = false
        enteredNumbers = ""
        console.debug("AccessResetCancel")
        progress.visible = false
        eraseProgressBlock.visible = false;
    }

    function eraseStatusUpdate(val)
    {
        progress.visible = true
        eraseProgressBlock.visible = true
        progress.visible = true
        progress.value = val
    }

    function eraseFinished()
    {
        progress.visible = false
        eraseProgressBlock.visible = false
    }




    property string enteredNumbers: ""


    onEnteredNumbersChanged:
    {
        if(enteredNumbers.length == 6)
        {
            requestAccess(enteredNumbers)
        }
    }


    ColorConfig{ id: cc}
    FontsConfig{id:fontConf}


    Button
    {
        id:blockZone
        anchors.fill: parent
    }


    Rectangle
    {
        id:back
        anchors.fill: parent
        color: cc.dark_blue
    }


    Text
    {
        id:headerLabel
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20

        font.family: fontConf.controlFont
        font.pixelSize: fontConf.pixSize_S4
        color: cc.light_green
        text: "Код блокировки"
    }


    Row
    {
        id:inputIndicator
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 100
        spacing: 32

        property int progress: lockWindow.enteredNumbers.length
        property bool red: false

        Repeater
        {
            model: 6

            Rectangle
            {
                width: 50
                height: 50
                color: inputIndicator.red ? cc.red : index <= inputIndicator.progress-1 ? cc.light_green : "transparent"
                border.color: cc.blue
                border.width: 2
                radius: 4
            }
        }
    }

    Text
    {
        id: resetPassText
        visible: false
        anchors.left: inputIndicator.right
        anchors.leftMargin: 10
        //anchors.top: inputIndicator.top
        anchors.verticalCenter: inputIndicator.verticalCenter
        font.family: fontConf.controlFont
        font.pixelSize: fontConf.pixSize_S5
        color: "white"
        text: " Сброс пароля\n Введите дополнительный код."
    }

    Rectangle
    {
        id: eraseProgressBlock
        visible: false
        anchors.top: parent.top
        anchors.bottom: progress.top
        anchors.right: parent.right
        anchors.left: parent.left
        color: cc.blue
        opacity: 0.7
        z: 5
    }

    Text
    {
        id: eraseProgressText
        visible: eraseProgressBlock.visible
        text: qsTr("Идет стирание памяти ...")
        anchors.horizontalCenter: eraseProgressBlock.horizontalCenter
        anchors.verticalCenter: eraseProgressBlock.verticalCenter
        font.pixelSize: fontConf.pixSize_S3
        font.family: fontConf.controlFont
        color: cc.light_green
        z: 6
    }

    ProgressBar
    {
        visible: false
        //parent: mainZone
        id: progress
        //anchors.horizontalCenter: mainZone.horizontalCenter
        hoverEnabled: false
        from: 0.0
        to: 100.0
        value: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.left: parent.left
        anchors.right: parent.right
        height: 30

        background: Rectangle
        {
            implicitWidth: parent.width - 120
            implicitHeight: 34
            color: cc.blue
        }

        contentItem: Item
        {
            implicitWidth: 200
            implicitHeight: 4

            Rectangle
            {
                width: progress.visualPosition * parent.width
                height: parent.height
                color: "#52dbb2"
                opacity: 0.65
            }
        }

        Text
        {
            x:parent.width/2 - width/2
            y:parent.height/2 - height/2
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S5
            color: cc.light_green
            text: progress.value + " %"
            z: 2
        }

        Text
        {
            id:timeFromStartLABEL
            y: progress.height + 30
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S5 - 1
            color: cc.light_green
            text: qsTr("Время с начала стирания:")
        }
        Text
        {
            id:timeFromStartVALUE
            x: timeFromStartLABEL.width + 10
            y: progress.height + 30
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S5 - 1
            color: cc.light_green
        }

        Text
        {
            id:timeToEndLABEL
            x: progress.width - width - timeToEndVALUE.width - 10
            y: progress.height + 30
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: 25
            color: cc.light_green
            text: qsTr("Время до окончания стирания:")
        }
        Text
        {
            id:timeToEndVALUE
            x: progress.width - width
            y: progress.height + 30
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S5 - 1
            color: cc.light_green
        }
    }


    Rectangle
    {
        visible: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.left: parent.left
        anchors.right: parent.right
        height: 30
        border.color: "black"
        color: "red"
        z: 3

        Text
        {
            id: name
            height: parent.height
            text: qsTr("10%")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.family: fontConf.controlFont
            font.pixelSize: fontConf.pixSize_S6
        }
    }


    Grid
    {
        id:numpad
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 210

        columns: 3
        columnSpacing: 64
        rowSpacing: 32

        Repeater
        {
            model: 12
            delegate: numpadButton
        }
    }


    Rectangle
    {
        id:errorFullScreen
        anchors.fill: parent
        color: cc.dark_blue
        visible: false

        Button { anchors.fill: parent; background: Rectangle { opacity: 0 }}

        Text
        {
            id:errorText
            x:parent.width/2 - width/2
            y:parent.height/2 - height/2 - 40
            width: parent.width - 200
            height: 100
            font.family: fontConf.mainFont
            fontSizeMode: Text.Fit
            minimumPixelSize: 10
            font.pixelSize: fontConf.pixSize_S4
            wrapMode: Text.Wrap
//            elide: Text.ElideMiddle
            color: cc.light_green
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: messageText
        }
    }


    SequentialAnimation
    {
        id: accessDeniedAnimation

        onStarted:
        {
            inputIndicator.red = true
        }

        NumberAnimation
        {
            target: inputIndicator
            property: "anchors.horizontalCenterOffset"
            duration: 200
            easing.type: Easing.InBack;
            easing.amplitude: 50.0;
            easing.period: 5.1
            easing.overshoot: 5
            from: inputIndicator.anchors.horizontalCenterOffset
            to : inputIndicator.anchors.horizontalCenterOffset + 100
        }

        NumberAnimation
        {
            target: inputIndicator
            property: "anchors.horizontalCenterOffset"
            duration: 100
            easing.type: Easing.Linear;
            from: inputIndicator.anchors.horizontalCenterOffset + 100
            to : inputIndicator.anchors.horizontalCenterOffset - 100
        }

        NumberAnimation
        {
            target: inputIndicator
            property: "anchors.horizontalCenterOffset"
            duration: 250
            easing.type: Easing.OutBack;
            easing.amplitude: 50.0;
            easing.period: 5.1
            easing.overshoot: 5
            from: inputIndicator.anchors.horizontalCenterOffset - 100
            to : inputIndicator.anchors.horizontalCenterOffset
        }

        onStopped:
        {
            inputIndicator.red = false
        }
    }




    Component
    {
        id:numpadButton

        Button
        {
           id: control
           text: index == 10 ? "0" : index == 11 ? "" : index+1
           enabled: index == 9 ? 0 : 1
           opacity: enabled

           onPressed:
           {
               if(!accessDeniedAnimation.running)
               {
                   if(index == 11) //delete button
                   {
                       var _str = lockWindow.enteredNumbers
                       lockWindow.enteredNumbers = _str.substring(0,_str.length-1)
                   }
                   else if (index == 10) // 0 button
                   {
                       lockWindow.enteredNumbers += "0"
                   }
                   else
                   {
                       lockWindow.enteredNumbers += index+1
                   }
               }
           }


           contentItem: Text
           {
               text: control.text
               font.family: fontConf.controlFont
               font.pixelSize: fontConf.pixSize_S4
               color: cc.light_green
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
               scale:control.pressed ? 0.94 : 1
            }

            background: Rectangle
            {
                implicitWidth: 160
                implicitHeight: 90
                color: cc.blue
                border.color: "#50000000"
                border.width: control.pressed ? 4 : 2
                radius: 4
            }

            Rectangle
            {
                anchors.fill: parent
                anchors.margins: 3
                radius: 3
                color: "black"
                opacity: control.pressed ? 0.1 : 0
            }

            Image
            {
                anchors.centerIn: parent
                width: 50
                height: 50
//                mipmap: true
                visible: index == 11 ? true : false
                scale:control.pressed ? 0.94 : 1
                source: "qrc:/icons/numpadDelete.png"
            }


//            ImageRender
//            {
//                id: deleteIcon
//                objectName: "DeleteIcon"
//                anchors.centerIn: parent
//                width: 60
//                height: 60
//            }
        }
    }



}

