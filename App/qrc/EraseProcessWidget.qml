import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2



Item
{
    id: root

    signal swipeSelectedSignal()
    signal swipeDeselectedSignal()
    signal showMeParent(string name, var item)

    function swipeSelected()
    {
        root.swipeSelectedSignal()
    }

    function swipeDeselected()
    {
        root.swipeDeselectedSignal()
    }

    function showMe( name, item )
    {
        root.showMeParent(name, item)
    }

    function setEraseData(progressVal, progressStr)
    {
        progress.value = progressVal
        progressText.text = progressStr + "%"
    }

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    Text
    {
        x: parent.width/2 - width/2
        y: 16 - height/2
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S7 + 4
        color: cc.light_green
        text: qsTr("Стирание")
    }


    ProgressBar
    {
        id: progress
        x: parent.width/2 - width/2
        y: parent.height/2 - height/2 - 6
        value: 0
        from: 0.0
        to: 100

        hoverEnabled: false

        background: Rectangle
        {
            implicitWidth: root.width - 40
            implicitHeight: 34
            color: cc.dark_blue
        }

        contentItem: Item
        {
            implicitWidth: 200
            implicitHeight: 4

            Rectangle
            {
                width: progress.visualPosition * parent.width
                height: parent.height
                color: "#52dbb2"
                opacity: 0.65
            }
        }
    }


    Text
    {
        id: progressText
        x: parent.width/2 - width/2
        y: parent.height/2 - height/2 - 6
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S5
        color: cc.light_green
        text: qsTr("0 %")
    }


    Button // ProgressBar anti-handle touch fix
    {
        anchors.fill: parent
        background: Rectangle { opacity: 0 }

    }
}
