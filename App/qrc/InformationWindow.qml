import QtQuick 2.9
import QtQuick.Controls 2.2

import Cicada 1.0


Item
{
    id: popUpMessage
    anchors.fill: parent
    z:15
    visible: false

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    function showMessage(msg)
    {
        messageText = msg
        popUpMessage.visible = true
    }

//    property string messageText: "Текст сообщения для сообщения сообщения. Посмотри на это сообщение и продолжай делать, что делал."
    property string messageText: "Текст сообщения для сообщения сообщения."


    Button
    {
        id:blockZone
        anchors.fill: parent

        background: Rectangle
        {
            color: cc.fog
            opacity: 0.5
        }
    }


    Rectangle
    {
        id:mainZone
        anchors.centerIn: parent

        height: 250
        width: 650
        radius: 4
        color: cc.dark_blue

        ShadowRect{type: "panel"}

//        BackgroundTitle
//        {
//            x: parent.width - width - 25
//            y: parent.height - height - 10
//            font.pixelSize: 52
//            text: qsTr("Сообщение")
//        }


        Text
        {
            x:parent.width/2 - width/2
            y:parent.height/2 - height/2 - 40
            width: parent.width - 200
            height: 100
            font.family: fontConf.mainFont
            fontSizeMode: Text.Fit
            minimumPixelSize: 10
            font.pixelSize: fontConf.pixSize_S5 + 2
            wrapMode: Text.Wrap
//            elide: Text.ElideMiddle
            color: cc.light_green
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: messageText
        }


        ControlListButton
        {
            id:okButton
            x: parent.width/2 - width/2
            y: parent.height - height - 20
            type: "SlimType"
            text: qsTr("OK")

            onClicked: popUpMessage.visible = false
        }
    }
}
