import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2



Rectangle
{
    id: naviButton
    x: 18
    y: 20
    height: 110
    width: 110
    radius: 3
    scale: buttonElem.down ? 0.95 : 1
    color: cc.blue
//    z:2

    signal buttonPressed()

    property var itemClass

    function changeButtonType(type)
    {
        console.debug("Type === " + type)
        if( type === 0 )
        {
            lines.opacity = 1; // menu
            back_img.opacity = 0;
        }
        else
        {
            lines.opacity = 0; // back
            back_img.opacity = 1;
        }
    }

    ColorConfig{id:cc}
    ShadowRect{x: -6; y: 6; radius: 3; opacity: buttonElem.down ? 0 : 1}


    Button
    {
        id: buttonElem
        anchors.fill: parent

        property bool opened: false

        background: Rectangle { opacity: 0 }

        onClicked:
        {
            naviButton.buttonPressed()
            opened = !opened
        }
    }

    Column //Icon
    {
        id: lines
        x: parent.width/2 - width/2
        y: parent.height/2 - height/2
        spacing: 10

        Repeater
        {
            model: 3

            Rectangle
            {
                height: 13
                width: 70
                radius: width / 2
                color: cc.light_green
            }
        }
    }

    Image
    {
        id: back_img
        x: parent.width/2 - width/2 - 4
        y: parent.height/2 - height/2
        height: parent.height - 40
        width: parent.width - 40
        fillMode: Image.PreserveAspectFit
        source: "qrc:/icons/i33.png"
        mipmap: true
        opacity: 0
    }
}
