import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2
import app.render 1.0


Rectangle
{
    property var itemClass

    property string buttonText: ""
//    property string txt: itemClass.representationName()
    property bool folder: false
    property bool inFolderButton: false
//    property string icon: "file:///" + applicationDirPath + "/../CommonUI/Icons/i19.png"

    onParentChanged:
    {
        if(parent.objectName == "MenuRow")
        {
            inFolderButton = false
        }
        else
        {
            inFolderButton = true
        }
    }

    id: menuButton
    width: 105
    height: 105
    color: cc.dark_blue
    scale: buttonElem.down ? 0.9 : 1

    signal buttonClicked()

    function setButtonName(name)
    {
        buttonText = name
    }

    function setButtonFolderType(isFolder)
    {
        folder = isFolder
    }

//    function setButtonIcon(icon)
//    {
//       // iconElem.addImage(icon)
//    }


    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
    ShadowRect{id: shadow; x: -4; y: 12; height: 98; opacity: buttonElem.down ? 0 : 1}


    onInFolderButtonChanged:
    {
        menuButton.height = 80
        menuButton.color = cc.blue
        shadow.height = 74
        iconElem.y = 10
        iconElem.width = 45
        iconElem.height = 45
    }


    Rectangle
    {
        id: tongue
        y: -7
        width: 40
        height: 7
        color: cc.dark_blue
        visible: folder
    }

    ImageRender
    {
        id: iconElem
        objectName: "ButtonIcon"
        x: parent.width/2 - width/2
        y: parent.height/2 - 40
        width: 60
        height: 60
    }

//    Image
//    {
//        id: iconElem
//        x: parent.width/2 - width/2
//        y: parent.height/2 - 40
//        width: 60
//        height: 60
//        fillMode: Image.PreserveAspectFit
//        mipmap: true

//       // source: icon
//    }


    Text
    {
        y: parent.height - height - 2
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        width: parent.width
        fontSizeMode: Text.HorizontalFit
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S7 - 2
        font.bold: true
        minimumPixelSize: 6
        text: buttonText
        color: cc.light_green
    }


    Button
    {
        id: buttonElem
        anchors.fill: parent

        background: Rectangle { opacity: 0 }

        onClicked:
        {
            console.debug("CLick button")

            if(!inFolderButton)
            {
                if(menuButton.parent.openFolderClick(parent.x, parent.y))
                {
                    menuButton.buttonClicked();                   
                }
            }
            else
            {
                menuButton.buttonClicked();
            }
        }

    }
}
