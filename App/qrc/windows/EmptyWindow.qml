import QtQuick 2.0

Item
{
    width: parent.width
    height: parent.height
    Rectangle
    {
        x: parent.x
        y: parent.y
        anchors.fill: parent
        color: "red"
        visible: false
    }
}
