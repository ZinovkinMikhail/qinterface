import QtQuick 2.9
import QtQuick.Controls 2.2

import Cicada 1.0

Item
{
    id: aboutWindow
    y: 150

    signal updateButtonClicked()

    ColorConfig {id: cc}
    FontsConfig{id:fontConf}

    Component.onCompleted:
    {
        adapterVersion.item.label = "Версия адаптера:"
      //  adapterVersion.item.value = "2.0-20-10-13-769-g0b0982d"

        adapterVersionDate.item.label = "от"
      //  adapterVersionDate.item.value = "05.03.2020 12:06"

        azVersion.item.label = "Версия АЗ:"
       // azVersion.item.value = "1"

        azVersionDate.item.label = "от"
      //  azVersionDate.item.value = "Feb 25 2014"

        serialNumber.item.label = "Серийный номер:"
       // serialNumber.item.value = "06303FBB0B01C040"

        nickName.item.label = "Псевдоним:"
       // nickName.item.value = "Цикада-ЦМ ROSS"

        batteryCapacity.item.label = "Емкость АКБ:"
       // batteryCapacity.item.value = "11 Ач"

        commissioningDate.item.label = "Дата ввода в эксплуатацию АКБ:"
       // commissioningDate.item.value = "мес 3 год 2020"
    }


    BackgroundTitle
    {
        text: qsTr("О программе")
    }


    Rectangle
    {
        id:background
        x:18
        height: 380
        width: 1280 - 36
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }


    Rectangle
    {
        id:mainZone
        anchors.centerIn: background
        height: background.height - 36
        width: background.width - 36
        color: cc.dark_blue

        Column
        {
            objectName: "Column"
            anchors.centerIn: parent
            spacing: 12

            Loader{ sourceComponent: infoLine; id:adapterVersion; objectName: "AdapterVersion" }
            Loader{ sourceComponent: infoLine; id:adapterVersionDate; objectName: "AdapterVersionDate"}
            Loader{ sourceComponent: infoLine; id:azVersion; objectName: "AzVersion" }
            Loader{ sourceComponent: infoLine; id:azVersionDate; objectName: "AzDate" }
            Loader{ sourceComponent: infoLine; id:serialNumber; objectName: "SN"}
            Loader{ sourceComponent: infoLine; id:nickName; objectName: "Name" }
            Loader{ sourceComponent: infoLine; id:batteryCapacity; objectName: "Capacity" }
            Loader{ sourceComponent: infoLine; id:commissioningDate; objectName: "Date" }
        }
    }


    Item
    {
        id:controlButtons
        anchors.top: background.bottom
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        width: 470

        ControlListButton
        {
            anchors.right: parent.right
            type: "SlimType"
            width: 300
            text: qsTr("Обновить")
            onClicked: updateButtonClicked()
        }
    }



    Component
    {
        id:infoLine
        Item
        {
            width: 580
            height: labelText.height
            objectName: "item"

            property string label
            property string value

            Text
            {
                id:labelText
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6 - 2
                color: cc.light_green
                text: label
            }
            Item
            {
                anchors.right: parent.right
                width: 230
                Text
                {
                    id:valueText
                    font.family: fontConf.mainFont
                    font.pixelSize: fontConf.pixSize_S6 - 2
                    color: cc.white
                    text: value
                }
            }
        }
    }
}
