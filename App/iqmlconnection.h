#ifndef IQMLCONNECTION_H
#define IQMLCONNECTION_H

#include <QVariantMap>

#define QML_CLASS_NAME "itemClass"

class IQmlConnection
{
public:
    virtual QVariantMap getQmlProperties() = 0;
    virtual ~IQmlConnection() {}
};


#endif // IQMLCONNECTION_H
