#include <assert.h>

#include "dataupdater.h"

using namespace rosslog;
using namespace data_provider;
using namespace recorder_status_ctrl;

DataUpdater::DataUpdater(rosslog::Log &log, std::shared_ptr<DataProvider> dataProvider,
                         std::shared_ptr<DataKeeper> dataKeeper, QObject *parent) :
    QObject(parent),
    logger(&log),
    dataProvider(dataProvider),
    dataKeeper(dataKeeper),
    thread(nullptr),
    recorderSettingsCtrl(nullptr),
    recorderStatusCtrl(nullptr)
{
    assert(logger);
    assert(dataProvider);
}

void DataUpdater::initDataUpdater()
{
    initData();
    if( thread == nullptr )
    {
        thread = new std::thread(threadWork, this);
    }
}

void DataUpdater::initData()
{
    if( recorderSettingsCtrl == nullptr )
    {
        recorderSettingsCtrl = new RecorderSettingsContol(dataProvider, *logger);
    }

    if( recorderStatusCtrl == nullptr )
    {
        recorderStatusCtrl = new RecorderStatusControl(dataProvider, *logger);
    }
}


void DataUpdater::threadWork(void *object)
{
    DataUpdater *updater = reinterpret_cast<DataUpdater*>(object);

    while(1)
    {
        try
        {
            updater->recorderSettingsCtrl->updateStatus();
            updater->recorderStatusCtrl->updateStatus();
            updater->dataKeeper->setRecorderSettings(updater->recorderSettingsCtrl->getRecordSettings());
            updater->dataKeeper->setRecorderStatus(updater->recorderStatusCtrl->getRecorderStatus());
        }
        catch(std::system_error &except)
        {
            updater->logger->Message(LOG_ERR, __LINE__, __func__,
                                     dpr::ProviderError::getInstance().getErrorString(except).c_str());
        }


        updater->logger->Message(LOG_INFO, __LINE__, __func__, "DataUpdater work");
        sleep(SLEEP_TIME);
    }
}
