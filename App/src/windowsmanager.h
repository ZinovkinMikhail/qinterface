#ifndef WINDOWSMANAGER_H
#define WINDOWSMANAGER_H

#include <QObject>
#include <QQmlContext>

#include <rosslog/log.h>

#include <dataprovider/dataprovider.h>

#include "windowsmanagerqml.h"

#include "pluginmanager.h"
#include "mainmenuwindow.h"
#include "iqmlengine.h"
#include "navibuttonwidget.h"
#include "processwidget.h"
#include "statuscontrol.h"
#include "statewidget.h"
#include "iusermanipulators.h"
#include "combodrummanipulators.h"
#include "informationwindow.h"
#include "confirmationwindow.h"
#include "lockwindow.h"
#include "monitoringvolumewidget.h"
#include "dataupdater.h"
#include "datakeeper.h"
#include "aboutwindow.h"

#include "audiodatadownloader.h"


class WindowsManager : public QObject, public IUserManipulators, public std::enable_shared_from_this<WindowsManager>
{
    Q_OBJECT
public:
    explicit WindowsManager(std::shared_ptr<IQmlEngine> qmlEngine, std::string pluginsPath,
                            std::shared_ptr<data_provider::DataProvider> dataProvider,
                            std::shared_ptr<rosslog::Log> log, QObject *parent = nullptr);
    ~WindowsManager();

    void initWindowsManager();

protected:
    std::pair<bool, int> openComboDrum(int currentIndex, std::vector<std::string> values) override;
    void openKeyBoard(std::function<void(int)> &keyPressCallBack) override;
    void closeKeyBoard() override;
    void showLoadingWidget() override;
    void hideLoadingWidget() override;
    std::pair<bool, std::string> openDateComboDrum(int day, int mon, int year) override;
    std::pair<bool, std::string> openTimeComboDrum(int hour, int min, int sec) override;
    void showMessage(std::string msg) override;

    bool requestUserConfirm(std::string msg) override;
    bool closePlugins() override;


private:
    void initDataUpdater();
    void initDataKeeper();
    void initWindowsManagerQML();
    void initPluginManager();
    void initChannelStatusWindow();
    void initMainMenu();
    void initTimeButtonWidget();
    void initVolumeButtonWidget();
    void initNaviButtonWidget();
    void initThreeDotButtonWidget();
    void initProcessWidget();
    void initStatusControl();
    void initStateWidget();
    void initComboDrumManipulator();
    void initInformationWindow();
    void initConfirmationWindow();
    void initLockWindow();
    void initMonitoringVolumeWidget();
    void initAboutWindow();
    void initAudioDataDownloader();

    void loadPluginWindows();


    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::string pluginsPath;
    std::shared_ptr<PluginManager> pluginManager;
    std::shared_ptr<IQmlEngine> qmlEngine;

    std::shared_ptr<DataUpdater> dataUpdater;
    std::shared_ptr<DataKeeper> dataKeeper;

    WindowsManagerQML *windowsManagerQML;

    std::shared_ptr<IEmptyPlaceWindow> channelStatusWindow;
    std::shared_ptr<IEmptyPlaceWindow> mainMenuWindow;
    std::shared_ptr<ISimpleButtonWidget> timeButtonWidget;
    std::shared_ptr<ISimpleButtonWidget> volumeButtonWidget;
    std::shared_ptr<NaviButtonWidget> naviButtonWidget;
    std::shared_ptr<ISimpleButtonWidget> threeDotButtonWidget;
    std::shared_ptr<ProcessWidget> processWidget;
    std::shared_ptr<StatusControl> statusControl;
    std::shared_ptr<StateWidget> stateWidget;
    std::shared_ptr<ComboDrumManipulator> comboDrumManipulator;
    std::shared_ptr<InformationWindow> informationWindow;
    std::shared_ptr<ConfirmationWindow> confirmationWindow;
    std::shared_ptr<LockWindow> lockWindow;
    std::shared_ptr<MonitoringVolumeWidget> monitoringvolumewidget;
    std::shared_ptr<AboutWindow> aboutWindow;
    std::shared_ptr<audio_downloader::AudioDataDownloader> audioDataDownloader;
    std::string windowToShow;

signals:
    void signalShowLoading();
    void signalHideLoading();

public slots:
    void slotSimpleButtonPressed(std::shared_ptr<ISimpleButtonWidget> button);
    void slotSimpleButtonClosePressed(std::shared_ptr<ISimpleButtonWidget> button);
    void slotNaviButtonPressed(ENaviButType type);
    void slotOpenWindow(std::string name);
    void slotShowAboutWindow();
    void slotLockWindow();


};

#endif // WINDOWSMANAGER_H
