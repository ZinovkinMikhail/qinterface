#ifndef IPROCESSWIDGET_H
#define IPROCESSWIDGET_H

#include <memory>

#include <QObject>

#include <rosslog/log.h>

class IProcessWidget
{
public:
    virtual bool isWidgetActive() = 0;
    virtual std::string getWidgetName() = 0;
    virtual QObject *getComponent() = 0;

signals:
    virtual void signalShowMe(std::string name, std::shared_ptr<IProcessWidget> object) = 0;
    virtual void signalSwipeSelected(std::string name, std::shared_ptr<IProcessWidget> object) = 0;
    virtual void signalSwipeDeselected(std::string name, std::shared_ptr<IProcessWidget> object) = 0;
    virtual void signalMakeFullSized() = 0;
};

Q_DECLARE_INTERFACE(IProcessWidget, "IProcessWidget")

#endif // IPROCESSWIDGET_H
