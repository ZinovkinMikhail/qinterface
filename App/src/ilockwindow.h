#ifndef ILOCKWINDOW_H
#define ILOCKWINDOW_H


class ILockWindow
{
public:
    virtual bool isVisible() = 0;

    virtual ~ILockWindow() {}
};


#endif // ILOCKWINDOW_H
