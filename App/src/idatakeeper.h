#ifndef IDATAKEEPER_H
#define IDATAKEEPER_H

#include <axis_softrecorder.h>
#include <axis_softadapter.h>

class IDataKeeper
{
public:
    virtual void setRecorderSettings(SoftRecorderSettingsStruct_t data) = 0;
    virtual void getRecorderSettings(SoftRecorderSettingsStruct_t &data) = 0;

    virtual ~IDataKeeper(){}
};

#endif // IDATAKEEPER_H
