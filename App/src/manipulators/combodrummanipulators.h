#ifndef COMBODRUMMANIPULATORS_H
#define COMBODRUMMANIPULATORS_H

#include <future>
#include <utility>

#include <QObject>
#include <QVariantMap>
#include <QEventLoop>
#include <QQuickItem>
#include <QVariant>
#include <QDateTime>

#include "iqmlengine.h"

#define COMBO_DRUM_QML_PATH "qrc:/ComboDrum.qml"

class ComboDrumManipulator : public QObject
{
    Q_OBJECT
public:
    explicit ComboDrumManipulator(std::shared_ptr<IQmlEngine> qmlEngine, QObject *parent = nullptr);
    std::pair<bool, int> openComboDrum(std::vector<std::string> values);
    std::pair<bool, int> openComboDrum(int currentIndex, std::vector<std::string> values);
    std::pair<bool, std::string> openComboDrumDate(int day, int mon, int year);
    std::pair<bool, std::string> openComboDrumTime(int hour, int min, int sec);

    void initComboDrumManipulator();

private:
    QEventLoop eventLoop;
    int selectedIndex;
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    std::string selectedTime;
    std::string selectedDate;
    bool comboDrumCancel;


public slots:
    void slotCloseComboDrum(QVariant index);
    void slotCloseComboDrumTime(int hour, int min, int sec);
    void slotCloseComboDrumDate(int day, int mon, int year);
    void slotCancelComboDrum();
};

#endif // COMBODRUMMANIPULATORS_H
