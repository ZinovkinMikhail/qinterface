#include <unistd.h>
#include <assert.h>
#include <iostream>

#include <QApplication>

#include "combodrummanipulators.h"

ComboDrumManipulator::ComboDrumManipulator(std::shared_ptr<IQmlEngine> qmlEngine,
                                           QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    currentItem(nullptr)
{
    assert(qmlEngine);
    selectedIndex = 0;
    selectedTime = "";
    selectedDate = "";
    comboDrumCancel = false;
}

std::pair<bool, int> ComboDrumManipulator::openComboDrum(std::vector<std::string> values)
{
    if( currentItem == nullptr )
        throw std::runtime_error("Combo Drum object is not inited");

    QVariantMap map;
    for(uint i = 0; i < values.size(); i++)
    {
        map.insert(QString::number(i), QString::fromStdString(values.at(i)));
    }

    QMetaObject::invokeMethod(currentItem, "openComboDrum",
                              Q_ARG(QVariant, map));

    eventLoop.exit();
    eventLoop.exec();

    if(comboDrumCancel)
        return std::make_pair(false, 0);
    return std::make_pair(true, selectedIndex);
}

std::pair<bool, std::string> ComboDrumManipulator::openComboDrumDate(int day, int mon, int year)
{
    QMetaObject::invokeMethod(currentItem, "openComboDrumDate",
                              Q_ARG(QVariant, day),
                              Q_ARG(QVariant, mon),
                              Q_ARG(QVariant, year));
    eventLoop.exit();
    eventLoop.exec();
    if(comboDrumCancel)
        return std::make_pair(false, selectedDate);
    return std::make_pair(true, selectedDate);
}

std::pair<bool, std::string> ComboDrumManipulator::openComboDrumTime(int hour, int min, int sec)
{
    QMetaObject::invokeMethod(currentItem, "openComboDrumTime",
                              Q_ARG(QVariant, hour),
                              Q_ARG(QVariant, min),
                              Q_ARG(QVariant, sec));
    eventLoop.exit();
    eventLoop.exec();
    if(comboDrumCancel)
        return std::make_pair(false, selectedTime);
    return std::make_pair(true, selectedTime);
}

void ComboDrumManipulator::initComboDrumManipulator()
{
    QObject *currentObject;
    currentObject = qmlEngine->createComponent(COMBO_DRUM_QML_PATH, this);

    if( currentObject == nullptr )
        throw std::runtime_error("Combo drum object not completed");

    currentItem = qobject_cast<QQuickItem*>(currentObject);
    currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

    connect(currentItem, SIGNAL(closeComboDrum(QVariant)),
            this, SLOT(slotCloseComboDrum(QVariant)));
    connect(currentItem, SIGNAL(closeComboDrumTime(int,int,int)),
            this, SLOT(slotCloseComboDrumTime(int,int,int)));
    connect(currentItem, SIGNAL(closeComboDrumDate(int,int,int)),
            this, SLOT(slotCloseComboDrumDate(int,int,int)));
    connect(currentItem, SIGNAL(cancelComboDrum()),
            this, SLOT(slotCancelComboDrum()));
}

void ComboDrumManipulator::slotCloseComboDrum(QVariant index)
{
    std::cerr << "CLOSE COMBO DRUM " << std::endl;
    selectedIndex = index.toInt();
    comboDrumCancel = false;
    eventLoop.exit();
    std::cerr << "QUIT COMBO DRUM READY " << std::endl;
}

void ComboDrumManipulator::slotCloseComboDrumTime(int hour, int min, int sec)
{
    QTime time;
    time.setHMS(hour, min, sec);
    selectedTime = time.toString("hh:mm").toStdString();
    comboDrumCancel = false;
    eventLoop.exit();
}

void ComboDrumManipulator::slotCloseComboDrumDate(int day, int mon, int year)
{
    QDate date;
    date.setDate(year, mon, day );
    comboDrumCancel = false;
    selectedDate = date.toString("dd.MM.yyyy").toStdString();
    eventLoop.exit();
}

void ComboDrumManipulator::slotCancelComboDrum()
{
    comboDrumCancel = true;
    eventLoop.exit();
}

std::pair<bool, int> ComboDrumManipulator::openComboDrum(int currentIndex, std::vector<std::string> values)
{
    if( currentItem == nullptr )
        throw std::runtime_error("Combo Drum object is not inited");

    QVariantMap map;
    for(uint i = 0; i < values.size(); i++)
    {
        map.insert(QString::number(i), QString::fromStdString(values.at(i)));
    }

    QMetaObject::invokeMethod(currentItem, "openComboDrum",
                              Q_ARG(QVariant, currentIndex),
                              Q_ARG(QVariant, map));


    eventLoop.exit();

    if(eventLoop.isRunning() )
    {
        std::cerr << "STILL RUNNING WAIT " << std::endl;
        return std::make_pair(false, 0);
    }
    eventLoop.exec();

    if(comboDrumCancel)
        return std::make_pair(false, 0);
    return std::make_pair(true, selectedIndex);
}

