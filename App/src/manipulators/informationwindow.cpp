#include "informationwindow.h"

#include <assert.h>
#include <iostream>

InformationWindow::InformationWindow(std::shared_ptr<IQmlEngine> qmlEngine,
                                     QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    currentItem(nullptr)
{
    assert(this->qmlEngine);
}

void InformationWindow::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(qmlEngine->createComponent(INFORMATION_WINDOW_QML_PATH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
    }
}

void InformationWindow::showMessage(std::string msg)
{
    QMetaObject::invokeMethod(currentItem, "showMessage", Q_ARG(QVariant, QString::fromStdString(msg)));
    std::cerr << "show message = " << msg << std::endl;
}
