#include "confirmationwindow.h"

#include <assert.h>
#include <QQmlProperty>


ConfirmationWindow::ConfirmationWindow(std::shared_ptr<IQmlEngine> qmlEngine,
                                     QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    currentItem(nullptr)
{
    assert(this->qmlEngine);
    confirmationApproved = false;
}

void ConfirmationWindow::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(qmlEngine->createComponent(CONFIRMATION_WINDOW_QML_PATH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
    }

    connect(currentItem, SIGNAL(userApproval(bool)),
            this, SLOT(slotUserApproval(bool)));
}


bool ConfirmationWindow::requestUserConfirm(std::string msg)
{
    QMetaObject::invokeMethod(currentItem, "requestUserConfirm",
                              Q_ARG(QVariant, QString::fromStdString(msg)));
    eventLoop.quit();
    eventLoop.exec();
    return confirmationApproved;
}

void ConfirmationWindow::slotUserApproval(bool userAnswer)
{
    confirmationApproved = userAnswer;
    QQmlProperty(currentItem, "visible").write(false);
    eventLoop.quit();
}
