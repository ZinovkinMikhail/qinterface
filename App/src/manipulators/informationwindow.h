#ifndef INFORMATIONWINDOW_H
#define INFORMATIONWINDOW_H

#include <memory>

#include <QObject>
#include <QQuickItem>

#include "iqmlengine.h"

#define INFORMATION_WINDOW_QML_PATH "qrc:/InformationWindow.qml"

class InformationWindow : public QObject
{
    Q_OBJECT
public:
    explicit InformationWindow(std::shared_ptr<IQmlEngine> qmlEngine,
                               QObject *parent = nullptr);

    void init();
    void showMessage(std::string msg);

private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;

signals:

};

#endif // INFORMATIONWINDOW_H
