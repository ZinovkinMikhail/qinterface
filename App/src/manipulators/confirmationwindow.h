#ifndef CONFIRMATIONWINDOW_H
#define CONFIRMATIONWINDOW_H

#include <memory>

#include <QObject>
#include <QQuickItem>
#include <QEventLoop>

#include "iqmlengine.h"

#define CONFIRMATION_WINDOW_QML_PATH "qrc:/ConfirmationWindow.qml"

class ConfirmationWindow : public QObject
{
    Q_OBJECT
public:
    explicit ConfirmationWindow(std::shared_ptr<IQmlEngine> qmlEngine,
                               QObject *parent = nullptr);

    void init();
    bool requestUserConfirm(std::string msg);

private:
    QEventLoop eventLoop;
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    bool confirmationApproved;


public slots:
    void slotUserApproval(bool userAnswer);

};

#endif // CONFIRMATIONWINDOW_H
