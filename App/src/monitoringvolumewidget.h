#ifndef MONITORINGVOLUMEWIDGET_H
#define MONITORINGVOLUMEWIDGET_H

#include <memory>

#include <QObject>
#include <QQuickItem>

#include <axis_softrecorder.h>
#include <dataprovider/dataprovider.h>

#include "iqmlengine.h"

#define MONITORING_VOLUME_WIDGET_QML_PATH "qrc:/MonitoringVolumeWidget.qml"


class MonitoringVolumeWidget : public QObject, public IDataWaiter, public enable_shared_from_this<MonitoringVolumeWidget>
{
    Q_OBJECT
public:
    explicit MonitoringVolumeWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                               std::shared_ptr<data_provider::DataProvider> dataProvider,
                               QObject *parent = nullptr);

    void init();
    void showVolume(int val);

protected:
    //IDataWaiter
    void recieveData(int command, std::vector<uint8_t> data, size_t size) override;

private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    std::shared_ptr<data_provider::DataProvider> dataProvider;


public slots:


};

#endif // MONITORINGVOLUMEWIDGET_H
