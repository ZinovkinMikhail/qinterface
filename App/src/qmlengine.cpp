#include <iostream>

#include <QQmlComponent>
#include <QQmlContext>
#include <QDebug>

#include "qmlengine.h"


QmlEngine::QmlEngine(QQmlApplicationEngine &engine, QObject *parent) :
    QObject(parent),
    engine(&engine)
{
}

void QmlEngine::setContextProperty(const QString &name, QObject *object)
{
    engine->rootContext()->setContextProperty(name, object);
}


QObject *QmlEngine::createComponent(const QString &fileName, QObject *parent)
{
    QQmlComponent component(engine, fileName, parent);
    QObject *object = component.create();

    for( const auto &error : component.errors() )
    {
        std::cerr << error.toString().toStdString() << std::endl;
    }
    return object;
}

QObject *QmlEngine::createComponent(const QString &fileName)
{
    QQmlComponent component(engine, fileName);
    QObject *object = component.create();

    if( object == nullptr )
    {
        for( const auto &error : component.errors() )
        {
                std::cerr << error.toString().toStdString() << std::endl;
        }
    }
    return object;
}
