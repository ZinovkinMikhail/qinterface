#ifndef IEMPTYPLACEWINDOW_H
#define IEMPTYPLACEWINDOW_H

#include <memory>

#include <QPixmap>
#include <QtPlugin>
#include <QObject>

#include <dataprovider/dataprovider.h>

#include "iusermanipulators.h"
#include "istatewidget.h"
#include "iprocesswidget.h"
#include "iqmlengine.h"

typedef enum
{
    NORMALSIZE,
    OVERSIZE
} EWindSizeType;

class IEmptyPlaceWindow
{
public:
    virtual std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) = 0;

    virtual QObject *getComponentObject() = 0;
    virtual std::string getWindowName() = 0;
    virtual std::string getWindowAbsolutePath() = 0;
//    virtual bool icanDelete() = 0;
    virtual bool showWindow() = 0;
    virtual bool hideWindow() = 0;
    virtual std::string getIconPresentationName() = 0;
    virtual std::string getWindowFolderPresentationName() = 0;
    virtual std::string getWindowFolderName() = 0;
    virtual QPixmap getIcon() = 0;
    virtual std::pair<bool, QPixmap> getFolderPixmap() = 0;
    virtual std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() = 0;
    virtual std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector() = 0 ;
    virtual bool prepareToDelete() = 0;
    virtual ~IEmptyPlaceWindow() {}
    virtual EWindSizeType getWinSizeType() = 0;
private:

};

QT_BEGIN_NAMESPACE

#define EmptyPlaceWindowInterface "EmptyPlaceInterface"

Q_DECLARE_INTERFACE(IEmptyPlaceWindow, EmptyPlaceWindowInterface)

QT_END_NAMESPACE

#endif // IEMPTYPLACEWINDOW_H
