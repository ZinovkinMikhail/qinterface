#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <memory>
#include <map>

#include <QObject>
#include <QPluginLoader>

#include <rosslog/log.h>

#include "pluginlist.h"
#include "iemptyplacewindow.h"

class PluginManager : public QObject
{
    Q_OBJECT
public:
    explicit PluginManager(std::string pluginsPath, rosslog::Log &log,
                           QObject *parent = nullptr);
    void loadAllPlugins();
    void loadCustomPlugin(std::string path);

    void unloadAllPlugins();
    std::map<std::string, std::shared_ptr<IEmptyPlaceWindow>> &getPlugins();

private:
    QPluginLoader pluginLoader;
    rosslog::Log *logger;
    std::string pluginsPath;
    std::map<std::string, std::shared_ptr<IEmptyPlaceWindow>> plugins;

signals:

public slots:
};

#endif // PLUGINMANAGER_H
