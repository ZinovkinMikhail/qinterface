#include <assert.h>
#include <iostream>

#include "memoryprocesswidget.h"

using namespace rosslog;

MemoryProcessWidget::MemoryProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine, std::string name,
                                         rosslog::Log &log,
                                         QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    name(name),
    logger(&log),
    widgetComponent(nullptr),
    isWidgetSelected(false),
    currentItem(nullptr)
{
    assert(logger);
    assert(qmlEngine);
}

void MemoryProcessWidget::init()
{
    if( widgetComponent == nullptr )
    {
        widgetComponent = qmlEngine->createComponent(MEMORY_PROCESS_COMPONENT_PATH);
        assert(widgetComponent);
        connect(widgetComponent, SIGNAL(swipeSelectedSignal()),
                this, SLOT(slotProcessWidgetActive()));
        connect(widgetComponent, SIGNAL(swipeDeselectedSignal()),
                this, SLOT(slotProcessWidgetUnActive()));
        connect(widgetComponent, SIGNAL(makeFullSizedControll()),
                this, SLOT(slotMakeFullSizedControll()));
        currentItem = qobject_cast<QQuickItem*>(widgetComponent);
    }
}

void MemoryProcessWidget::show()
{
    emit signalShowMe(name, shared_from_this());
}

void MemoryProcessWidget::updateData(uint64_t totalCapacity, uint64_t curCapacity,
                                     std::string curRecTime, std::string remainTime)
{
    float procent = 0;

    std::cerr << "Total capacity = " << totalCapacity << std::endl;
    std::cerr << "Cur capacity = " << curCapacity << std::endl;
    std::cerr << "Cur cur rec time = " << curRecTime << std::endl;
    std::cerr << "Cur remain time = " << remainTime << std::endl;

    if( totalCapacity > 0 )
    {
        procent = curCapacity * 100.0 / totalCapacity;
    }

    QMetaObject::invokeMethod(widgetComponent, "setMemoryData",
                              Q_ARG(QVariant, procent),
                              Q_ARG(QVariant, QString::number(round(procent))),
                              Q_ARG(QVariant, QString::fromStdString(curRecTime)),
                              Q_ARG(QVariant, QString::fromStdString(remainTime)));

//    QMetaObject::invokeMethod(widgetComponent, "setMemoryData",
//                              Q_ARG(QVariant, procent),
//                              Q_ARG(QVariant, QString::number(std::round(procent))),
//                              Q_ARG(QVariant, convertSecondsToHMS(curRecTime)),
//                              Q_ARG(QVariant, QString::fromStdString(remainTime)));
}

bool MemoryProcessWidget::isWidgetActive()
{
    return isWidgetSelected;
}

std::string MemoryProcessWidget::getWidgetName()
{
    return name;
}

QObject *MemoryProcessWidget::getComponent()
{
    assert(widgetComponent);
    return widgetComponent;
}

QString MemoryProcessWidget::convertSecondsToHMS(uint64_t value)
{
    QString res;
    int seconds = (int) (value % 60);
    value /= 60;
    int minutes = (int) (value % 60);
    value /= 60;
    int hours = (int) (value);
    return res.sprintf("%02d ч. %02d м. %02d с.", hours, minutes, seconds);
}

void MemoryProcessWidget::slotProcessWidgetActive()
{
    isWidgetSelected = true;
    emit signalSwipeSelected(name, shared_from_this());
}

void MemoryProcessWidget::slotProcessWidgetUnActive()
{
    isWidgetSelected = false;
    emit signalSwipeDeselected(name, shared_from_this());
}

void MemoryProcessWidget::slotMakeFullSizedControll()
{
    emit signalMakeFullSized();
}
