#ifndef MEMORYPROCESSWIDGET_H
#define MEMORYPROCESSWIDGET_H

#include <future>
#include <math.h>

#include <QObject>
#include <QQuickItem>

#include "iprocesswidget.h"
#include "iqmlengine.h"

#include "datakeeper.h"

#define MEMORY_PROCESS_COMPONENT_PATH "qrc:/MemoryProcessWidget.qml"

class MemoryProcessWidget : public QObject, public IProcessWidget,
        public std::enable_shared_from_this<MemoryProcessWidget>
{
    Q_OBJECT
    Q_INTERFACES(IProcessWidget)
public:
    explicit MemoryProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                 std::string name, rosslog::Log &log,
                                 QObject *parent = nullptr);

    void init();
    void show();
    void updateData(uint64_t totalCapacity, uint64_t curCapacity,
                    std::string curRecTime, std::string remainTime);

    bool isWidgetActive();
    std::string getWidgetName();
    QObject *getComponent();

private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::string name;
    rosslog::Log *logger;
    QObject *widgetComponent;
    bool isWidgetSelected;
    std::shared_ptr<DataKeeper> dataKeeper;
    QQuickItem *currentItem;

    QString convertSecondsToHMS(uint64_t value);

signals:
    void signalShowMe(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalSwipeSelected(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalSwipeDeselected(std::string name, std::shared_ptr<IProcessWidget> object);
    void pushMemoryData(QString capacity, int procent, QString recordTime, QString leftSec);
    void signalMakeFullSized();

public slots:
    void slotProcessWidgetActive();
    void slotProcessWidgetUnActive();
    void slotMakeFullSizedControll();

};

#endif // MEMORYPROCESSWIDGET_H
