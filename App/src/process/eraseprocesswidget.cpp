#include <assert.h>

#include "eraseprocesswidget.h"

using namespace rosslog;

EraseProcessWidget::EraseProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                       std::string name,
                                       rosslog::Log &log, QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    name(name),
    logger(&log),
    widgetComponent(nullptr),
    isWidgetSelected(false)
{
    assert(logger);
    assert(qmlEngine);
}

void EraseProcessWidget::init()
{
    if( widgetComponent == nullptr )
    {
        widgetComponent = qmlEngine->createComponent(ERASE_PROCESS_COMPONENT_PATH);
        assert(widgetComponent);
        connect(widgetComponent, SIGNAL(swipeSelectedSignal()),
                this, SLOT(slotProcessWidgetActive()));
        connect(widgetComponent, SIGNAL(swipeDeselectedSignal()),
                this, SLOT(slotProcessWidgetUnActive()));
    }
}

void EraseProcessWidget::show()
{
    emit signalShowMe(name, shared_from_this());
}

void EraseProcessWidget::updateEraseData(uint16_t progress, uint64_t flashUsed, uint64_t cpacity)
{
    Q_UNUSED(flashUsed);
    Q_UNUSED(cpacity);
    QMetaObject::invokeMethod(widgetComponent, "setEraseData",
                              Q_ARG(QVariant, progress),
                              Q_ARG(QVariant, QString::number(progress)));
}

bool EraseProcessWidget::isWidgetActive()
{
    return isWidgetSelected;
}

std::string EraseProcessWidget::getWidgetName()
{
    return name;
}

QObject *EraseProcessWidget::getComponent()
{
    assert(widgetComponent);
    return widgetComponent;
}

void EraseProcessWidget::slotProcessWidgetActive()
{
    isWidgetSelected = true;
    emit signalSwipeSelected(name, shared_from_this());
}

void EraseProcessWidget::slotProcessWidgetUnActive()
{
    isWidgetSelected = false;
    emit signalSwipeDeselected(name, shared_from_this());
}

