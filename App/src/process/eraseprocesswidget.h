#ifndef ERASEPROCESSWIDGET_H
#define ERASEPROCESSWIDGET_H

#include <QObject>

#include "iprocesswidget.h"
#include "iqmlengine.h"

#define ERASE_PROCESS_QML_CLASS "itemClass"
#define ERASE_PROCESS_COMPONENT_PATH "qrc:/EraseProcessWidget.qml"

class EraseProcessWidget : public QObject, public IProcessWidget,
        public std::enable_shared_from_this<EraseProcessWidget>
{
    Q_OBJECT
public:
    explicit EraseProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                std::string name, rosslog::Log &log, QObject *parent = nullptr);
    void init();
    void show();
    void updateEraseData(uint16_t progress, uint64_t flashUsed, uint64_t cpacity);
    bool isWidgetActive();
    std::string getWidgetName();
    QObject *getComponent();

private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::string name;
    rosslog::Log *logger;
    QObject *widgetComponent;
    bool isWidgetSelected;

signals:
    void signalShowMe(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalSwipeSelected(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalSwipeDeselected(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalMakeFullSized();

public slots:
    void slotProcessWidgetActive();
    void slotProcessWidgetUnActive();
};

#endif // ERASEPROCESSWIDGET_H
