#include "pluginmanager.h"

#include <QPluginLoader>
#include <QDir>

using namespace rosslog;

PluginManager::PluginManager(std::string pluginsPath, rosslog::Log &log,
                             QObject *parent) :
    QObject(parent),
    logger(&log),
    pluginsPath(pluginsPath)
{
#warning test
    loadAllPlugins();
}

void PluginManager::loadAllPlugins()
{
    QDir pluginsDir;
    pluginsDir.cd(QString::fromStdString(pluginsPath));

    foreach (QString fileName, pluginsDir.entryList(QDir::Files))
    {
        if( fileName == "." || fileName == "..")
            continue;

        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();

        if( plugin != nullptr )
        {
            IEmptyPlaceWindow *obj = dynamic_cast<IEmptyPlaceWindow *>(plugin);
            this->logger->Message(LOG_INFO, __LINE__, __func__ ,
                                  "Plugin name = %s",
                                  fileName.toStdString().c_str());
            if( obj != nullptr  )
            {
                this->logger->Message(LOG_INFO, __LINE__, __func__ ,
                                      "Plugin window identificator = %s",
                                      obj->getWindowName().c_str());
                plugins[obj->getWindowName()] = std::shared_ptr<IEmptyPlaceWindow>(obj);
            }
            else
			{
				this->logger->Message(LOG_ERR, __LINE__, __func__ ,
									  "Plugin fail to load window identificator = %s",
									  obj->getWindowName().c_str());
			}
        }
		else
		{
			this->logger->Message(LOG_INFO, __LINE__, __func__ ,
								  "Plugin name fail to load = %s",
								  fileName.toStdString().c_str());
		}
    }
}

void PluginManager::loadCustomPlugin(std::string path)
{
    Q_UNUSED(path);
}

void PluginManager::unloadAllPlugins()
{
    auto it = plugins.find("PlayBackWindow");

    if( it != plugins.end() )
    {
        plugins.erase(it);
    }

    plugins.clear();
}

std::map<string, std::shared_ptr<IEmptyPlaceWindow> > &PluginManager::getPlugins()
{
    return this->plugins;
}

