#ifndef WINDOWSMANAGERQML_H
#define WINDOWSMANAGERQML_H

#include <memory>

#include <QObject>
#include <QVariantMap>

#include <rosslog/log.h>

#include "iqmlengine.h"

#include "isimplebuttonwidget.h"
#include "navibuttonwidget.h"
#include "processwidget.h"
#include "statewidget.h"
#include "combodrummanipulators.h"
#include "iemptyplacewindow.h"
#include "iqmlconnection.h"

#define WINDOWS_MANAGER_QML_CLASS "WindowsManager"
#define START_WINDOW_NAME "ChannelControl"

class WindowsManagerQML : public QObject
{
    Q_OBJECT
public:
    explicit WindowsManagerQML(std::shared_ptr<IQmlEngine> qmlEngine,
                               rosslog::Log &log,
                               QObject *parent = nullptr);

    void initWindowsManagerQML();
    void showInitialWindow();
    void showFullScreenButton();
    void hideFullScreenButton();


    void pushNewWindow(std::string name,
                       std::pair<QObject *, std::shared_ptr<IEmptyPlaceWindow> > window);
    void removeWindow(std::string name);

    void showWindow(std::string windowName);
    void showPreviousWindow(string name);
    void showComboDrumWidget();
    void showLoadingWidget();
    void hideLoadingWidget();
    std::string getPreviousWindowName();
    bool stackViewIsBusy();

private:
    std::pair<QObject *, std::shared_ptr<IEmptyPlaceWindow> > findWindow(std::string name);

    std::shared_ptr<IQmlEngine> qmlEngine;
    rosslog::Log *logger;
    std::map<std::string, std::pair<QObject*, std::shared_ptr<IEmptyPlaceWindow>>> windows;
    std::pair<QObject *, std::shared_ptr<IEmptyPlaceWindow> >previousWindow;
    std::pair<QObject *, std::shared_ptr<IEmptyPlaceWindow> >currentWindow;


signals:
    //QML
    void pushNextWindow(QObject *window);
    void pushPrevWindow();
    void showComboDrum();
    void hideComboDrum();
    void showLoading();
    void hideLoading();

public slots:
    void slotCloseFromFullScreenButton();
};

#endif // WINDOWSMANAGERQML_H
