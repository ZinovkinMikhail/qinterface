#include <assert.h>

#include "processwidget.h"
#include <iostream>

using namespace rosslog;

ProcessWidget::ProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                             rosslog::Log &log,
                             QObject *parent) :
    QObject(parent),
    logger(&log),
    qmlEngine(qmlEngine),
    currentItem(nullptr),
    firstItem(true)
{
    assert(logger);
    assert(qmlEngine);
}

void ProcessWidget::init()
{
    QObject *object = qmlEngine->createComponent(PROCESS_WIDGET_QML_PATH, this->parent());
    assert(object);

    currentItem = qobject_cast<QQuickItem*>(object);
    QQuickItem *parentItem = qobject_cast<QQuickItem*>(this->parent());
    currentItem->setParentItem(parentItem);
    swipeView = currentItem->findChild<QQuickItem*>("SwipeView");
    assert(swipeView);
}

QObject *ProcessWidget::getRootItem()
{
    return currentItem;
}

void ProcessWidget::addProcessWidget(std::shared_ptr<IProcessWidget> widget, std::string name)
{
    if( widget != nullptr && name.compare("") != 0)
    {
#ifdef APP_DEBUG_MSG
        this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Push new process widget name = %s", name.c_str());
#endif
        widgetList[name] = widget;


        QQuickItem *component = qobject_cast<QQuickItem*>(widget->getComponent());
        component->setParentItem(swipeView);

        connect(dynamic_cast<QObject*>(widget.get()), SIGNAL(signalShowMe(std::string, std::shared_ptr<IProcessWidget>)),
                this, SLOT(slotShowProcessWidget(std::string,
                                                 std::shared_ptr<IProcessWidget>)));
        connect(dynamic_cast<QObject*>(widget.get()), SIGNAL(signalMakeFullSized()),
                this, SLOT(slotMakeFullSized()));
        if( firstItem )
        {
            //Only for first item deselect event. Swipe view should know his previous window
            QMetaObject::invokeMethod(currentItem, "setFirstItem",
                                      Q_ARG(QVariant, QVariant::fromValue(component)));
            firstItem = false;
            this->logger->Message(LOG_INFO, __LINE__, __func__,
                                  "Set first item of PROCESS WIDGETS name = %s", name.c_str());
        }
        //SIGNAL OF SWIPE SELECTED OR DESELECTED IS IN QML ( SWIPE VIEW TELL CHILD THAT CHILD IS SELECTED)
    }
}

void ProcessWidget::removeProcessWidget(std::shared_ptr<IProcessWidget> widget, std::string name)
{
    Q_UNUSED(widget);
    auto it = widgetList.find(name);

    if( it != widgetList.end() )
    {
        QMetaObject::invokeMethod(currentItem, "removeItem",
                                  Q_ARG(QVariant, QVariant::fromValue(widget->getComponent())));
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Remove process widget name = %s",
                              name.c_str());
        widgetList.erase(it);
    }
}

void ProcessWidget::slotShowProcessWidget(std::string name, std::shared_ptr<IProcessWidget> widget)
{
    Q_UNUSED(name);
    Q_UNUSED(widget);
    this->logger->Message(LOG_INFO, __LINE__, __func__, "Show me signal handled = %s",
                          name.c_str());
    QMetaObject::invokeMethod(currentItem, "setCurrentItem",
                              Q_ARG(QVariant, QVariant::fromValue(widget->getComponent())));
}

void ProcessWidget::slotMakeFullSized()
{
    QMetaObject::invokeMethod(currentItem, "makeBig");
}


