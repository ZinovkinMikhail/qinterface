#ifndef STATEWIDGET_H
#define STATEWIDGET_H

#include <memory>

#include <QObject>
#include <QVariantMap>
#include <QQuickItem>
#include <QVariant>

#include "istatewidget.h"
#include "state.h"
#include "iqmlengine.h"

#define STATE_WIDGET_QML_PATH "qrc:/StateWidget.qml"

#define INDICATOR_STATE_WIDTH 70
#define PROCESS_STATE_WIDTH 70
#define SPACING 0
#define SPACING_PROCESS 20

class StateWidget : public QObject, public IStateWidget
{
    Q_OBJECT
public:
    explicit StateWidget(std::shared_ptr<IQmlEngine> qmlEngine, QObject *parent = nullptr);

    void init();

    void addState(std::string name, EStateType stateType,
                          QPixmap pixmap, bool prioHigh);
    void showState(std::string name, std::string value);
    void hideState(std::string name);
    void removeState(std::string name);
    std::vector<std::string> countStates();


private:
    void replaceProcessStatesPositions();
    void replaceIndicatorStatesPositions();

    std::vector<std::shared_ptr<State>> hightPriorityIndicator;
    std::vector<std::shared_ptr<State>> lowPriorityIndicator;
    std::vector<std::shared_ptr<State>> hightPriorityProcess;
    std::vector<std::shared_ptr<State>> lowPriorityProcess;

    std::map<std::string, std::shared_ptr<State>> states;
    std::shared_ptr<IQmlEngine> engine;
    QQuickItem *currentItem;
    QQuickItem *indicatorItem;
    QQuickItem *processItem;

};

#endif // STATEWIDGET_H
