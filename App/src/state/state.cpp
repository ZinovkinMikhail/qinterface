#include <assert.h>

#include "state.h"

#include "imagerender.h"

State::State(std::shared_ptr<IQmlEngine> qmlEngine, std::string name, QPixmap pixmap,
             EStateType type, bool prioHigh, QObject *parent) :
    QObject(parent),
    name(name),
    pixmap(pixmap),
    type(type),
    prioHigh(prioHigh),
    engine(qmlEngine),
    currentItem(nullptr)
{
    assert(engine);
    assert(this->parent());
}

void State::init()
{
    if( currentItem == nullptr )
    {
        switch (type)
        {
            case INDICATOR:
            {
                currentItem = qobject_cast<QQuickItem*>(engine->createComponent(STATE_INDICATOR_QML_PATH));
                assert(currentItem);
                currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
                ImageRender *imageRender = currentItem->findChild<ImageRender*>("IndicatorIcon");
                imageRender->addImage(pixmap.toImage());
                break;
            }
            case PROCESS:
            {
                currentItem = qobject_cast<QQuickItem*>(engine->createComponent(STATE_PROCESS_QML_PATH));
                assert(currentItem);
                currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
                ImageRender *imageRender = currentItem->findChild<ImageRender*>("ProcessIcon");

                if(!pixmap.toImage().isNull())
                    imageRender->addImage(pixmap.toImage());
                break;
            }
            default:
                break;
        }

    }
}

void State::move(int x, int y)
{
    this->currentItem->setX(x);
    this->currentItem->setY(y);
}

int State::getPositionX()
{
    return this->currentItem->x();
}

void State::show(std::string value)
{
    this->value = value;
    QMetaObject::invokeMethod(currentItem, "setStateValue",
                              Q_ARG(QVariant, QString::fromStdString(value)));
}

void State::hide()
{
    QMetaObject::invokeMethod(currentItem, "setHidden");
}

bool State::isVisible()
{
    return currentItem->isVisible();
}

std::string State::getName()
{
    return this->name;
}


EStateType State::getType()
{
    return type;
}


void State::slotPress()
{}
