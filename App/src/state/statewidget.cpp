#include <assert.h>

#include "statewidget.h"

StateWidget::StateWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                         QObject *parent) :
    QObject(parent),
    engine(qmlEngine),
    currentItem(nullptr)
{
    assert(engine);
    assert(this->parent());
}

void StateWidget::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(engine->createComponent(STATE_WIDGET_QML_PATH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
        indicatorItem = currentItem->findChild<QQuickItem*>("IndicatorRow");
        assert(indicatorItem);
        processItem = currentItem->findChild<QQuickItem*>("ProcessRow");
        assert(processItem);
    }
}

void StateWidget::addState(std::string name, EStateType stateType, QPixmap pixmap, bool prioHigh)
{
    switch (stateType)
    {
    case INDICATOR:
    {
        std::shared_ptr<State> stateIcon = std::shared_ptr<State>(
                    new State(engine, name, pixmap, stateType, prioHigh, indicatorItem));
        stateIcon->init();
        if( prioHigh )
            hightPriorityIndicator.push_back(stateIcon);
        else
            lowPriorityIndicator.push_back(stateIcon);
        states[name] = stateIcon;
        break;
    }
    case PROCESS:
    {
        std::shared_ptr<State> stateIcon = std::shared_ptr<State>(
                    new State(engine, name, pixmap, stateType, prioHigh, processItem));
        stateIcon->init();
        if( prioHigh )
            hightPriorityProcess.push_back(stateIcon);
        else
            lowPriorityProcess.push_back(stateIcon);

        states[name] = stateIcon;
        break;
    }
    default:
        break;
    }
}

void StateWidget::showState(std::string name, std::string value)
{
    auto it = states.find(name);

    if( it != states.end() )
    {
        bool visibleState = it->second->isVisible();
        it->second->show(value);
        if( !visibleState )
        {
            switch (it->second->getType())
            {
                case INDICATOR:
                    replaceIndicatorStatesPositions();
                    break;
                case PROCESS:
                    replaceProcessStatesPositions();
                    break;
                default:
                    break;
            }
        }
    }
}

void StateWidget::hideState(std::string name)
{
    auto it = states.find(name);

    if( it != states.end() )
    {
        it->second->hide();
        switch (it->second->getType())
        {
            case INDICATOR:
                replaceIndicatorStatesPositions();
                break;
            case PROCESS:
                replaceProcessStatesPositions();
                break;
            default:
                break;
        }
    }
}

void StateWidget::removeState(std::string name)
{
    auto it = states.find(name);

    if( it != states.end() )
    {
        states.erase(name);
        it->second->hide();

        //Reorder states by priority
        switch (it->second->getType())
        {
            case INDICATOR:
                replaceIndicatorStatesPositions();
                break;
            case PROCESS:
                replaceProcessStatesPositions();
                break;
            default:
                break;
        }
        //will delete automatic
    }
}

std::vector<std::string> StateWidget::countStates()
{
    std::vector<std::string> statesName;

    for( auto it : states )
    {
        if( it.second->getType() == PROCESS )
        {
            if( it.second->isVisible() )
            {
                statesName.push_back(it.second->getName());
            }
        }
    }
    return statesName;
}

void StateWidget::replaceProcessStatesPositions()
{
    int initialPosition = 0;


    for(auto it: hightPriorityProcess)
    {
        if( !it->isVisible() )
            continue;
        if( (initialPosition + PROCESS_STATE_WIDTH) < 0 )
        {
            it->hide();
            break;
        }
        it->move(initialPosition, 0);
        initialPosition += PROCESS_STATE_WIDTH;
        initialPosition += SPACING_PROCESS;
    }

    for(auto it: lowPriorityProcess)
    {
        if( !it->isVisible() )
            continue;
        if( (initialPosition + PROCESS_STATE_WIDTH) < 0 )
        {
            it->hide();
            break;
        }
        it->move(initialPosition, 0);
        initialPosition += PROCESS_STATE_WIDTH;
        initialPosition += SPACING_PROCESS;
    }
}

void StateWidget::replaceIndicatorStatesPositions()
{
    int parentWidth = processItem->width();
    int initialPosition = parentWidth - PROCESS_STATE_WIDTH;

    for(auto it: hightPriorityIndicator)
    {
        if( !it->isVisible())
            continue;
        if( (initialPosition - INDICATOR_STATE_WIDTH ) > processItem->width() )
        {
            it->hide();
            break;
        }
        it->move(initialPosition, 0);
        initialPosition -= INDICATOR_STATE_WIDTH;
        initialPosition -= SPACING;
    }

    for(auto it: lowPriorityIndicator)
    {
        if( !it->isVisible() )
            continue;
        if( (initialPosition - INDICATOR_STATE_WIDTH) > processItem->width() )
        {
            it->hide();
            break;
        }
        it->move(initialPosition, 0);
        initialPosition -= INDICATOR_STATE_WIDTH;
        initialPosition -= SPACING;
    }
}
