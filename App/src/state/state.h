#ifndef STATE_H
#define STATE_H

#include <memory>

#include <QObject>
#include <QPixmap>
#include <QVariantMap>
#include <QQuickItem>
#include <QVariant>

#include "istatewidget.h"
#include "iqmlengine.h"

#define STATE_INDICATOR_QML_PATH "qrc:/IndicatorState.qml"
#define STATE_PROCESS_QML_PATH "qrc:/ProcessState.qml"

class State : public QObject
{
    Q_OBJECT
public:
    explicit State(std::shared_ptr<IQmlEngine> qmlEngine, std::string name, QPixmap pixmap,
                   EStateType type, bool prioHigh, QObject *parent = nullptr);

    void init();
    void move(int x, int y);
    int getPositionX();
    void show(std::string value);
    void hide();
    bool isVisible();
    std::string getName();

    EStateType getType();

private:
    std::string name;
    std::string value;
    QPixmap pixmap;
    EStateType type;
    bool prioHigh;
    std::shared_ptr<IQmlEngine> engine;
    QQuickItem* currentItem;

signals:
    void changeValue(QString value);

public slots:
    void slotPress();
};

#endif // STATE_H
