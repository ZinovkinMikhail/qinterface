#ifndef ISATEWIDGET_H
#define ISATEWIDGET_H

#include <QPixmap>

typedef enum
{
    INDICATOR,
    PROCESS
} EStateType;

class IStateWidget
{
public:
    virtual void addState(std::string name, EStateType stateType,
                          QPixmap pixmap, bool prioHigh) = 0;
    virtual void showState(std::string name, std::string value) = 0;
    virtual void hideState(std::string name ) = 0;
    virtual void removeState(std::string name) = 0;
    virtual std::vector<std::string> countStates() = 0;

    virtual ~IStateWidget() {}
};

#endif // ISATEWIDGET_H
