#ifndef IMENUITEMWIDGET_H
#define IMENUITEMWIDGET_H

#include <memory>

#include <QObject>
#include <QPixmap>
#include <QQmlApplicationEngine>
#include <QQuickItem>

typedef enum
{
    BUTTON,
    FOLDER
} ItemType;

class IMenuItemEventsHandler;

class IMenuItemWidget
{
public:
    virtual void openItem() = 0;
    virtual void closeItem() = 0;
    virtual std::string getItemName() = 0;
    virtual void moveItem(int x, int y) = 0;
    virtual QQuickItem* getObjectItem() = 0;
    virtual ItemType getItemType() = 0;

private:
    std::shared_ptr<IMenuItemEventsHandler> eventsHandler;
    std::string name;
    QPixmap pixmap;
    std::string presentationName;

signals:

public slots:
};

#endif // IMENUITEMWIDGET_H
