#ifndef QMLENGINE_H
#define QMLENGINE_H

#include <QObject>

#include "iqmlengine.h"

class QmlEngine : public QObject, public IQmlEngine
{
    Q_OBJECT
public:
    explicit QmlEngine(QQmlApplicationEngine &engine, QObject *parent = nullptr);

    void setContextProperty(const QString &name, QObject *object);
    QObject* createComponent(const QString &fileName);
    QObject* createComponent(const QString &fileName, QObject *parent);

private:
    QQmlApplicationEngine *engine;

signals:

public slots:
};

#endif // QMLENGINE_H
