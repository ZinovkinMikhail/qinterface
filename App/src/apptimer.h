#ifndef APPTIMER_H
#define APPTIMER_H


#include <QDateTime>
#include <QObject>
#include <QTimer>

#include <dataprovider/dataprovider.h>


#define APP_TIMER_INTERVAL 10000 //ms

class AppTimer : public QObject
{
    Q_OBJECT
public:
    explicit AppTimer();
    static AppTimer &getInstance();
    static int64_t getCurrentTime();

private:
    void initAppTimer();

    QTimer *timer;

signals:
    void signalAppTimerPush();

private slots:
    void slotAppTimerTick();

};

#endif // APPTIMER_H
