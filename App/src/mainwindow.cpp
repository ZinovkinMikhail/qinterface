#include <assert.h>
#include <iostream>

#include "mainwindow.h"
#include "dataprovider/adaptercommands.h"


using namespace rosslog;

MainWindow  *MainWindow::_self = nullptr;

MainWindow::MainWindow(std::shared_ptr<IQmlEngine> qmlEngine, QObject *parent) :
    QObject(parent),
    dataProvider(nullptr),
    windowsManager(nullptr),
    qmlEngine(qmlEngine)
{
    MainWindow::_self =this;
    
    qmlEngine->setContextProperty("MainWindowClass", this);

    this->installEventFilter(this);
}

void MainWindow::initMainWindow()
{
    logger = new Log(LOG_PATH, LOG_APP_NAME, 8, LOG_ENABLE_CONSOLE, MAX_LOG_FILE_SIZE); //LOG_LEVEL
    logger->Message(LOG_DEBUG,__LINE__, __func__, "Log inited");
    log = std::shared_ptr<Log>(logger);

    try
    {
        initDataProvider();
#ifdef MASTER
        startTest();
#else
        std::cerr << "Init desging" << std::endl;
        initWindowsManager();
        std::cerr << "Init desging end" << std::endl;
#endif
    }
    catch(std::runtime_error &except)
    {
        log->Message(LOG_ERR, __LINE__, __func__, except.what());
    }

}

void MainWindow::startTest()
{

}

MainWindow::~MainWindow()
{
//    if( logger != nullptr )
//        delete logger;

    MainWindow::_self =nullptr;
    
}
//---------------------------------------------------------
MainWindow*
MainWindow::getOwner()
{
  return MainWindow::_self;
}

QObject *MainWindow::getTestServerWindow()
{
    return nullptr;
}


void MainWindow::slotStartWork()
{
    startTest();
}

void MainWindow::initDataProvider()
{
    assert(logger);
    if( dataProvider == nullptr && logger != nullptr  )
        dataProvider = std::shared_ptr<DataProvider>(new DataProvider(*log.get()));

    try
    {
        dataProvider->startConnection();
    }
    catch(std::system_error &except)
    {
    }
}

void MainWindow::initWindowsManager()
{
    assert(logger);
    assert(dataProvider);

    if( windowsManager == nullptr )
    {
        QObject *parentIt = this->parent()->findChild<QObject*>("RootWindow");
        assert(parentIt);
        windowsManager = std::shared_ptr<WindowsManager>(new WindowsManager(qmlEngine, PLUGINS_PATH, dataProvider,
                                          log, parentIt));
        try
        {
            windowsManager->initWindowsManager();

        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
        }
    }
}
