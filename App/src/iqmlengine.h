#ifndef IQMLENGINE_H
#define IQMLENGINE_H

#include <QObject>

#include <QQmlApplicationEngine>

class IQmlEngine
{
public:
    virtual void setContextProperty(const QString &name, QObject *object) = 0;
    virtual QObject* createComponent(const QString &fileName) = 0;
    virtual QObject* createComponent(const QString &fileName, QObject *parent) = 0;
    virtual ~IQmlEngine(){}

signals:

public slots:
};

QT_BEGIN_NAMESPACE

#define QmlEngineInterface "IQmlEngine"


Q_DECLARE_INTERFACE(IQmlEngine, QmlEngineInterface)

QT_END_NAMESPACE

#endif // IQMLENGINE_H
