#ifndef IMAGERENDER_H
#define IMAGERENDER_H

#include <QQuickPaintedItem>
#include <QQuickItem>
#include <QImage>
#include <future>
class ImageRender : public QQuickPaintedItem
{
public:
    ImageRender(QQuickItem *parent = nullptr);
    void paint(QPainter *painter);
    void addImage(QImage image);
    void setActive(bool active);
    void keepRatio(bool keep);
private:
    QImage finalImage;
    std::mutex lockMutex;
    bool isActive;
    bool keepAspectRatio;
};

#endif // IMAGERENDER_H
