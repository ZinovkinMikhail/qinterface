#include "monitoringvolumewidget.h"

#include <assert.h>
#include <QQmlProperty>


MonitoringVolumeWidget::MonitoringVolumeWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                     std::shared_ptr<data_provider::DataProvider> dataProvider,
                                     QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    dataProvider(dataProvider),
    currentItem(nullptr)
{
    assert(this->qmlEngine);
}


void MonitoringVolumeWidget::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(qmlEngine->createComponent(MONITORING_VOLUME_WIDGET_QML_PATH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
        dataProvider->addDataWaiter(AXIS_INTERFACE_EVENT_VOLUME, shared_from_this());
    }
}


void MonitoringVolumeWidget::showVolume(int val)
{
    float value = 0;
    val += 30;
    value = val * 100.0 /60.0;

    QMetaObject::invokeMethod(currentItem, "show", Q_ARG(QVariant, (value / 100.0)));
}

void MonitoringVolumeWidget::recieveData(int command, std::vector<uint8_t> data, size_t size)
{
    if( command == AXIS_INTERFACE_EVENT_VOLUME )
    {
        if( size > 4 )
        {
            int volumeVal = (int)(data[3]) << 24 | (int)(data[2]) << 16 |
                                                   (int)(data[1]) << 8 |
                                                   (int)(data[0]);

            this->showVolume(volumeVal);
        }
    }
}

