#include <assert.h>

#include "windowsmanager.h"
#include "channelcontrolwidget.h"
#include "buttons/threedotbuttonwidget.h"
#include "timebuttonwidget.h"
#include "volumebuttonwidget.h"
#include "memoryprocesswidget.h"


using namespace data_provider;
using namespace rosslog;
using namespace audio_downloader;

WindowsManager::WindowsManager(std::shared_ptr<IQmlEngine> qmlEngine, std::string pluginsPath,
                               std::shared_ptr<DataProvider> dataProvider,
                               std::shared_ptr<Log> log, QObject *parent) :
    QObject(parent),
    logger(log),
    dataProvider(dataProvider),
    pluginsPath(pluginsPath),
    pluginManager(nullptr),
    dataUpdater(nullptr),
    dataKeeper(nullptr),
    windowsManagerQML(nullptr),
    channelStatusWindow(nullptr),
    mainMenuWindow(nullptr),
    timeButtonWidget(nullptr),
    volumeButtonWidget(nullptr),
    naviButtonWidget(nullptr),
    threeDotButtonWidget(nullptr),
    processWidget(nullptr),
    statusControl(nullptr),
    stateWidget(nullptr),
    comboDrumManipulator(nullptr),
    informationWindow(nullptr),
    confirmationWindow(nullptr),
    lockWindow(nullptr),
    monitoringvolumewidget(nullptr),
    aboutWindow(nullptr),
    audioDataDownloader(nullptr)
{
    this->qmlEngine = qmlEngine;
    qRegisterMetaType<ENaviButType>("ENaviButType");
    qRegisterMetaType<std::shared_ptr<ISimpleButtonWidget>>("std::shared_ptr<ISimpleButtonWidget>");
}

void WindowsManager::initWindowsManager()
{
    connect(this, &WindowsManager::signalHideLoading, [this] ()
    {
        windowsManagerQML->hideLoadingWidget();
    });

    connect(this, &WindowsManager::signalShowLoading, [this] ()
    {
        windowsManagerQML->showLoadingWidget();
    });

    initWindowsManagerQML();

    initStateWidget();
    initPluginManager();
    initChannelStatusWindow();

    initMainMenu();
    initAboutWindow();
    initLockWindow();
    initStatusControl();
    initTimeButtonWidget();
    initVolumeButtonWidget();
    initNaviButtonWidget();
    initThreeDotButtonWidget();
    initProcessWidget();
    initInformationWindow();
    initConfirmationWindow();
    initMonitoringVolumeWidget();
    initComboDrumManipulator();
    initAudioDataDownloader();

    this->windowsManagerQML->showInitialWindow();
    windowToShow = mainMenuWindow->getWindowName();


    if( lockWindow != nullptr )
    {
        lockWindow->show();
    }
}

void WindowsManager::openKeyBoard(std::function<void (int)> &keyPressCallBack)
{
    keyPressCallBack(1);
}

void WindowsManager::closeKeyBoard()
{
}

void WindowsManager::showLoadingWidget()
{
    emit signalShowLoading();
}

void WindowsManager::hideLoadingWidget()
{
    emit signalHideLoading();
}

std::pair<bool, std::string> WindowsManager::openDateComboDrum(int day, int mon, int year)
{
    if( comboDrumManipulator != nullptr )
    {
        windowsManagerQML->showFullScreenButton();
        auto res = comboDrumManipulator->openComboDrumDate(day, mon, year);
        windowsManagerQML->hideFullScreenButton();
        return res;
    }
    return std::make_pair(false, "");
}

std::pair<bool, std::string> WindowsManager::openTimeComboDrum(int hour, int min, int sec)
{
    if( comboDrumManipulator != nullptr )
    {
        windowsManagerQML->showFullScreenButton();
        auto res = comboDrumManipulator->openComboDrumTime(hour, min, sec);
        windowsManagerQML->hideFullScreenButton();
        return res;
    }
    return std::make_pair(false, "");
}

void WindowsManager::showMessage(string msg)
{
    if( informationWindow != nullptr )
    {
        informationWindow->showMessage(msg);
    }
}

bool WindowsManager::requestUserConfirm(string msg)
{
    if( confirmationWindow != nullptr )
    {
        bool res = confirmationWindow->requestUserConfirm(msg);
        return res;
    }
    return false;
}

bool WindowsManager::closePlugins()
{
    bool canClose = true;

    for( auto it : pluginManager->getPlugins() )
    {
        if( !it.second->prepareToDelete() )
        {
            if( it.first == "PlayBackWindow" )
            {
                this->showMessage("В окне воспроизведения выполняется неотложная операция. "
                                  "Пожалуйста завершите её или дождитесь окончания.");
            }
            canClose = false;
            break;
        }
    }

    if( !canClose )
        return false;

    for( auto it : pluginManager->getPlugins() )
    {
        std::pair<bool, std::shared_ptr<IProcessWidget>> res =  it.second->getProcessWidget();
        if(processWidget == nullptr )
        {
        }
        if( res.second == nullptr )
        {
            auto ss = it.second->getProcessWidgetsVector();
            for(auto s : ss.second )
            {
                processWidget->removeProcessWidget(s, s->getWidgetName());
            }
        }
        else
            processWidget->removeProcessWidget(res.second, res.second->getWidgetName());

        if( windowsManagerQML != nullptr )
        {
            windowsManagerQML->removeWindow(it.first);
        }
    }

//    windowsManagerQML->removeWindow(channelStatusWindow->getWindowName());
//    channelStatusWindow->prepareToDelete();
//    channelStatusWindow = nullptr;
    pluginManager->unloadAllPlugins();


//    dataProvider->prepareToDelete();

    return true;
}

void WindowsManager::initDataUpdater()
{
    if( dataUpdater == nullptr )
    {
        dataUpdater = std::shared_ptr<DataUpdater>(new DataUpdater(*logger, dataProvider, dataKeeper));
        dataUpdater->initDataUpdater();
    }
}

void WindowsManager::initDataKeeper()
{
    if( dataKeeper == nullptr )
    {
        dataKeeper = std::shared_ptr<DataKeeper>(new DataKeeper());
    }
}

WindowsManager::~WindowsManager()
{}

void WindowsManager::initWindowsManagerQML()
{
    if( windowsManagerQML == nullptr )
    {
        windowsManagerQML = new WindowsManagerQML(qmlEngine, *logger, this->parent());

        try
        {
            windowsManagerQML->initWindowsManagerQML();
        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
        }
    }
}

void WindowsManager::initPluginManager()
{
    assert(logger);
    assert(windowsManagerQML);

    if( pluginManager == nullptr )
    {
        pluginManager = std::shared_ptr<PluginManager>(new PluginManager(
                                                          pluginsPath, *logger));
        this->loadPluginWindows();
    }
}

void WindowsManager::initChannelStatusWindow()
{
    assert(windowsManagerQML);

    if( channelStatusWindow == nullptr )
    {

        channelStatusWindow = std::shared_ptr<IEmptyPlaceWindow>(new ChannelControlWidget(this->parent()));

        channelStatusWindow->initWindow(NORMALSIZE, dataProvider, stateWidget,
                                              shared_from_this(),
                                              qmlEngine,
                                              std::shared_ptr<Log>(this->logger));

        if( channelStatusWindow != nullptr )
        {
            QObject *readyComponent = channelStatusWindow->getComponentObject();
            if( readyComponent == nullptr )
                throw std::runtime_error("Error while initing channel status window");

            if( windowsManagerQML != nullptr )
            {
                std::pair<QObject* , std::shared_ptr<IEmptyPlaceWindow>> window(readyComponent,
                                                                                channelStatusWindow);
                windowsManagerQML->pushNewWindow(
                            channelStatusWindow->getWindowName(), window);
            }
            else
                throw std::runtime_error("WindowsManagerQML is not inited");
        }
    }
}

void WindowsManager::initMainMenu()
{
    assert(pluginManager);
    assert(windowsManagerQML);

    if( mainMenuWindow == nullptr )
    {
        std::shared_ptr<MainMenuWindow> mainMenu = std::shared_ptr<MainMenuWindow>(
                    new MainMenuWindow(this->parent()));

        mainMenu->initWindow(NORMALSIZE,
                             dataProvider,
                             stateWidget,
                             nullptr,
                             qmlEngine,
                             std::shared_ptr<Log>(this->logger));

        mainMenu->initMainMenu(this->pluginManager);

        connect(mainMenu.get(), SIGNAL(signalOpenWindow(std::string)),
                this, SLOT(slotOpenWindow(std::string)));

        mainMenuWindow = std::dynamic_pointer_cast<IEmptyPlaceWindow>(mainMenu);

        QObject *readyComponent = mainMenuWindow->getComponentObject();

        if( readyComponent == nullptr )
        {
            throw std::runtime_error("Error while initing main menu construct");
        }


        std::pair<QObject* , std::shared_ptr<IEmptyPlaceWindow>> window(readyComponent,
                                                                        mainMenuWindow);

        windowsManagerQML->pushNewWindow(
                    mainMenuWindow->getWindowName(), window);
    }
}

void WindowsManager::initTimeButtonWidget()
{
    if( timeButtonWidget == nullptr )
    {
        TimeButtonWidget *timeButton = new TimeButtonWidget(*this->logger, qmlEngine,
                                                            dataProvider, this->parent());
        try
        {
            timeButton->initTimeButtonWidget();

            connect(timeButton,
                    SIGNAL(signalButtonPressed(std::shared_ptr<ISimpleButtonWidget>)),
                    this,
                    SLOT(slotSimpleButtonPressed(std::shared_ptr<ISimpleButtonWidget>)));

            connect(timeButton,
                    SIGNAL(signalCloseButtonPressed(std::shared_ptr<ISimpleButtonWidget>)),
                    this,
                    SLOT(slotSimpleButtonClosePressed(std::shared_ptr<ISimpleButtonWidget>)));

            timeButtonWidget =
                    std::shared_ptr<ISimpleButtonWidget>(timeButton);
        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
        }
    }
}

void WindowsManager::initVolumeButtonWidget()
{
    if( volumeButtonWidget == nullptr )
    {
        VolumeButtonWidget *volumeButton = new VolumeButtonWidget(qmlEngine, *this->logger,
                                                                  dataProvider, shared_from_this(), this->parent());
        try
        {
            volumeButton->init();


            connect(volumeButton,
                    SIGNAL(signalButtonPressed(std::shared_ptr<ISimpleButtonWidget>)),
                    this,
                    SLOT(slotSimpleButtonPressed(std::shared_ptr<ISimpleButtonWidget>)));
            connect(volumeButton,
                    SIGNAL(signalCloseButtonPressed(std::shared_ptr<ISimpleButtonWidget>)),
                    this,
                    SLOT(slotSimpleButtonClosePressed(std::shared_ptr<ISimpleButtonWidget>)));

            volumeButtonWidget =
                    std::shared_ptr<ISimpleButtonWidget>(volumeButton);
        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
        }
    }
}

void WindowsManager::initNaviButtonWidget()
{
    if( naviButtonWidget == nullptr )
    {
        NaviButtonWidget *naviButton = new NaviButtonWidget(qmlEngine, *logger, this->parent());
        naviButton->init();

        connect(naviButton, SIGNAL(signalButtonPressed(ENaviButType)),
                this, SLOT(slotNaviButtonPressed(ENaviButType)));

        naviButtonWidget = std::shared_ptr<NaviButtonWidget>(naviButton);

    }
}

void WindowsManager::initThreeDotButtonWidget()
{
    if( threeDotButtonWidget == nullptr )
    {
        try
        {
            ThreeDotButtonWidget *threeDotButton = new ThreeDotButtonWidget(qmlEngine, *this->logger,
                                                                           dataProvider, shared_from_this(), stateWidget, this->parent());

            threeDotButton->init();
            connect(threeDotButton,
                    SIGNAL(signalButtonPressed(std::shared_ptr<ISimpleButtonWidget>)),
                    this,
                    SLOT(slotSimpleButtonPressed(std::shared_ptr<ISimpleButtonWidget>)));
            connect(threeDotButton,
                    SIGNAL(signalCloseButtonPressed(std::shared_ptr<ISimpleButtonWidget>)),
                    this,
                    SLOT(slotSimpleButtonClosePressed(std::shared_ptr<ISimpleButtonWidget>)));

            connect(threeDotButton, SIGNAL(signalShowAboutWindow()),
                    this, SLOT(slotShowAboutWindow()));
            connect(threeDotButton, SIGNAL(signalLockWindow()),
                    this, SLOT(slotLockWindow()));
            threeDotButtonWidget =
                    std::shared_ptr<ISimpleButtonWidget>(threeDotButton);
        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
        }
    }
}

void WindowsManager::initProcessWidget()
{
    assert(qmlEngine);

    if( processWidget == nullptr )
    {
        ProcessWidget *process = new ProcessWidget(qmlEngine, *this->logger, this->parent());
        process->init();
        processWidget = std::shared_ptr<ProcessWidget>(process);

        if( statusControl != nullptr )
        {
            for( auto it : statusControl->getProcessWidgets() )
            {
                if( it != nullptr )
                {
                    connect(dynamic_cast<QObject*>(it.get()),
                            SIGNAL(signalShowMe(std::string, std::shared_ptr<IProcessWidget>)),
                            dynamic_cast<QObject*>(processWidget.get()),
                            SLOT(slotShowProcessWidget(std::string, std::shared_ptr<IProcessWidget>)));
                    processWidget->addProcessWidget(it, it->getWidgetName());
                }
            }
        }

        if( channelStatusWindow != nullptr )
        {
            std::shared_ptr<ChannelControlWidget> channelWidget = dynamic_pointer_cast<ChannelControlWidget>(channelStatusWindow);

            for( auto it : channelWidget->getProcessWidgets() )
            {
                if( it != nullptr )
                {
                    connect(dynamic_cast<QObject*>(it.get()),
                            SIGNAL(signalShowMe(std::string, std::shared_ptr<IProcessWidget>)),
                            dynamic_cast<QObject*>(processWidget.get()),
                            SLOT(slotShowProcessWidget(std::string, std::shared_ptr<IProcessWidget>)));
                    processWidget->addProcessWidget(it, it->getWidgetName());
                }
            }
        }

        if( pluginManager != nullptr )
        {
            for( auto it : pluginManager->getPlugins() )
            {
                std::pair<bool, std::shared_ptr<IProcessWidget>> res =  it.second->getProcessWidget();
                if( res.first )
                {
                    connect(dynamic_cast<QObject*>(res.second.get()),
                            SIGNAL(signalShowMe(std::string, std::shared_ptr<IProcessWidget>)),
                            dynamic_cast<QObject*>(processWidget.get()),
                            SLOT(slotShowProcessWidget(std::string, std::shared_ptr<IProcessWidget>)));
                    processWidget->addProcessWidget(res.second, res.second->getWidgetName());
                }
                else
                {
                    auto res =  it.second->getProcessWidgetsVector();
                    if( res.first )
                    {
                       for(auto it: res.second )
                       {
                           connect(dynamic_cast<QObject*>(it.get()),
                                   SIGNAL(signalShowMe(std::string, std::shared_ptr<IProcessWidget>)),
                                   dynamic_cast<QObject*>(processWidget.get()),
                                   SLOT(slotShowProcessWidget(std::string, std::shared_ptr<IProcessWidget>)));
                           processWidget->addProcessWidget(it, it->getWidgetName());
                       }
                    }
                }
            }
        }
    }
}

void WindowsManager::initStatusControl()
{
    if( statusControl == nullptr )
    {
        statusControl =
                std::shared_ptr<StatusControl>(new StatusControl(*this->logger, this->qmlEngine,
                                                                 this->dataProvider, this->stateWidget,
                                                                 shared_from_this(), this->lockWindow));
        statusControl->initStatusControl();
    }
}

void WindowsManager::initStateWidget()
{
    if( stateWidget == nullptr )
    {
        stateWidget = std::shared_ptr<StateWidget>(new StateWidget(qmlEngine, this->parent()));
        stateWidget->init();
    }
}

void WindowsManager::initComboDrumManipulator()
{
    if( comboDrumManipulator == nullptr )
    {
        comboDrumManipulator = std::shared_ptr<ComboDrumManipulator>(
                    new ComboDrumManipulator(qmlEngine, this->parent()));
        comboDrumManipulator->initComboDrumManipulator();
    }
}

void WindowsManager::initInformationWindow()
{
    if( informationWindow == nullptr )
    {
        informationWindow = std::make_shared<InformationWindow>(qmlEngine, this->parent());
        informationWindow->init();
    }
}

void WindowsManager::initConfirmationWindow()
{
    if( confirmationWindow == nullptr )
    {
        confirmationWindow = std::make_shared<ConfirmationWindow>(qmlEngine, this->parent());
        confirmationWindow->init();
    }
}

void WindowsManager::initLockWindow()
{
    if( lockWindow == nullptr )
    {
        lockWindow = std::make_shared<LockWindow>(this->parent());

        lockWindow->initWindow(dataProvider,
                             shared_from_this(),
                             qmlEngine,
                             std::shared_ptr<Log>(this->logger),
                                     stateWidget);
    }   
}

void WindowsManager::initMonitoringVolumeWidget()
{
    if( monitoringvolumewidget == nullptr )
    {
        monitoringvolumewidget = std::make_shared<MonitoringVolumeWidget>(qmlEngine,
                                                                          dataProvider,
                                                                          this->parent());
        monitoringvolumewidget->init();
    }
}

void WindowsManager::initAboutWindow()
{
    if( aboutWindow == nullptr )
    {
        aboutWindow = std::make_shared<AboutWindow>();

        aboutWindow->initWindow(NORMALSIZE,
                             dataProvider,
                             stateWidget,
                             shared_from_this(),
                             qmlEngine,
                             std::shared_ptr<Log>(this->logger));




        QObject *readyComponent = aboutWindow->getComponentObject();

        if( readyComponent == nullptr )
        {
            throw std::runtime_error("Error while initing main menu construct");
        }

        std::pair<QObject* , std::shared_ptr<IEmptyPlaceWindow>> window(readyComponent,
                                                                        aboutWindow);

        windowsManagerQML->pushNewWindow(
                    aboutWindow->getWindowName(), window);
    }
}

void WindowsManager::loadPluginWindows()
{
    assert(windowsManagerQML);
    assert(pluginManager);

    for( auto it : pluginManager->getPlugins() )
    {
        it.second->initWindow(NORMALSIZE,
                                dataProvider,
                                stateWidget,
                                shared_from_this(),
                                qmlEngine,
                                std::shared_ptr<Log>(logger));

        QObject *object = it.second->getComponentObject();
        if( object == nullptr )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  "Error while initing plugin component name = %s", it.first.c_str());
            continue;
        }
        this->logger->Message(LOG_ERR, __LINE__, __func__,
                              "initing plugin component name = %s %p", it.first.c_str(), &it.second);
        std::pair<QObject* , std::shared_ptr<IEmptyPlaceWindow>> window(object, it.second);
        windowsManagerQML->pushNewWindow(it.first,
                                         window);
    }

}

void WindowsManager::slotSimpleButtonPressed(std::shared_ptr<ISimpleButtonWidget> button)
{
    if( button == threeDotButtonWidget )
    {
        threeDotButtonWidget->openButtonWidget();
//        windowsManagerQML->showFullScreenButton();
    }
    else if( button == volumeButtonWidget )
    {
        volumeButtonWidget->openButtonWidget();
//        windowsManagerQML->showFullScreenButton();
    }
//    else if( button == timeButtonWidget )
//    {
//        timeButtonWidget->openButtonWidget();
//        windowsManagerQML->showFullScreenButton();
//       // audioDataDownloader->startTest();
//    }
}

void WindowsManager::slotSimpleButtonClosePressed(std::shared_ptr<ISimpleButtonWidget> button)
{
    if( button == threeDotButtonWidget )
    {
        threeDotButtonWidget->closeButtonWidget();
        windowsManagerQML->hideFullScreenButton();
    }
    else if( button == volumeButtonWidget )
    {
        volumeButtonWidget->closeButtonWidget();
        windowsManagerQML->hideFullScreenButton();
    }
    else if( button == timeButtonWidget )
    {
        timeButtonWidget->closeButtonWidget();
        windowsManagerQML->hideFullScreenButton();
    }
}


void WindowsManager::slotNaviButtonPressed(ENaviButType type)
{
    if(windowsManagerQML->stackViewIsBusy())
    {
        return;
    }

    if( type == MENU )
    {
        if( windowToShow.compare(this->channelStatusWindow->getWindowName()) == 0 )
        {
            this->logger->Message(LOG_INFO, __LINE__, __func__, "Menu button pressed signal in WINDOWS MANAGER");
            windowsManagerQML->showPreviousWindow(this->channelStatusWindow->getWindowName());
            windowToShow = mainMenuWindow->getWindowName();
        }
        else
        {
            windowsManagerQML->showWindow(this->mainMenuWindow->getWindowName());
            windowToShow = channelStatusWindow->getWindowName();
        }
        naviButtonWidget->setButtonType(MENU);
    }
    else
    {
        std::string prevName = windowsManagerQML->getPreviousWindowName();
        windowsManagerQML->showPreviousWindow(windowsManagerQML->getPreviousWindowName());
        naviButtonWidget->setButtonType(MENU);

    }
}

//Signal from MainMenuWindow
void WindowsManager::slotOpenWindow(string name)
{
    if( name.compare("") != 0 )
    {
        naviButtonWidget->setButtonType(BACK);
        this->windowsManagerQML->showWindow(name);
        windowToShow = channelStatusWindow->getWindowName();
    }
}

void WindowsManager::slotShowAboutWindow()
{
    if( aboutWindow != nullptr )
    {
        aboutWindow->showWindow();
        naviButtonWidget->setButtonType(BACK);
        this->windowsManagerQML->showWindow(aboutWindow->getWindowName());
        windowToShow = mainMenuWindow->getWindowName();
    }
}

void WindowsManager::slotLockWindow()
{
    if( lockWindow != nullptr )
    {
        lockWindow->show();
    }
}

void WindowsManager::initAudioDataDownloader()
{
	if( audioDataDownloader == nullptr )
	{
		audioDataDownloader = std::make_shared<AudioDataDownloader>(
			dataProvider, logger);
		
        //audioDataDownloader->startDownload();
		

		//audioDataDownloader->closeSocket();
    }
}

std::pair<bool, int> WindowsManager::openComboDrum(int currentIndex, std::vector<std::string> values)
{
    if( comboDrumManipulator != nullptr )
    {
        windowsManagerQML->showFullScreenButton();
        auto res = comboDrumManipulator->openComboDrum(currentIndex, values);
        windowsManagerQML->hideFullScreenButton();
        return res;
    }
    return std::pair<bool, int>(false, 0);
}

