#ifndef STATUSCONTROL_H
#define STATUSCONTROL_H

#include <list>
#include <memory>
#include <thread>
#include <map>
#include <arpa/inet.h>
#include <net/if.h>
#include <sys/ioctl.h>

#include <axis_softadapter.h>
#include <axis_acc.h>

#include <axis_ifconfig.h>

#include <QObject>

#include <rosslog/log.h>

#include <dataprovider/dataprovider.h>
#include <dataprovider/idatawaiter.h>
#include <dataprovider/adapterstatuscontrol.h>
#include <dataprovider/adaptersettingscontrol.h>
#include <dataprovider/recorderstatuscontrol.h>

#include "istatewidget.h"
#include "iprocesswidget.h"
#include "ilockwindow.h"
#include "iqmlengine.h"

#include <iusermanipulators.h>

#include "util.h"

typedef enum
{
    ACC_STATUS_VERY_LOW,
    ACC_STATUS_LOW,
    ACC_STATUS_HALF,
    ACC_STATUS_FULL,
    ACC_STATUS_SEMIFULL,
    ACC_VOLTAGE,
    COPY_STATUS,
    COPY_PROGRESS,
    EXTERNAL_POWER,
    EXTERNAL_ACC_VOLTAGE,
    EXTERNAL_FLASH,
    EXTERNAL_FLASH_CAPACITY,
    NETWOR_CONNECTION_ETH,
    USERS,
    USB_CONNECTION,
    HEADPHONES,
    ACC_NO_CHARGE,
    KEYBOARD_LOCKED,
    CPU_TEMP_NORMAL,
    CPU_TEMP_ALARM
}StatusControlStates;


#define BATTERY_EXT_POWER_OFF 21873
#define USB_NETWORK_INTERFACE "usb0"
#define CPU_TEMP_FILE "/sys/class/thermal/thermal_zone0/temp"

#define __CPU_TEMPR_ALARM   (95)

class StatusControl : public QObject, public IDataWaiter,
        public std::enable_shared_from_this<StatusControl>
{
    Q_OBJECT
public:
    explicit StatusControl(rosslog::Log &log,
                           std::shared_ptr<IQmlEngine> qmlEngine,
                           std::shared_ptr<DataProvider> dataProvider,
                           std::shared_ptr<IStateWidget> stateWidget,
                           std::shared_ptr<IUserManipulators> userManipulators,
                           std::shared_ptr<ILockWindow> lockWindow,
                           QObject *parent = nullptr);
    void initStatusControl();
    void prepareToDelete();

    std::list<std::shared_ptr<IProcessWidget>> getProcessWidgets();

    ~StatusControl();
protected:
    void recieveData(int command, std::vector<uint8_t> data,
                        size_t size);

private:
    void initStateWidgets();
    void initCopyProcessWidget();
    void threadFunction();
    void initAdapterStatusControl();
    void initAdapterSettingsControl();
    void initRecorderStatusControl();
    void updateBatteryState();

    uint8_t getIpOctet(uint32_t ipAddr, int octetNum);
    std::string GetInterfaceIP(const char* interfaceName);

    rosslog::Log *logger;
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<IStateWidget> stateWidget;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusControl;
    std::shared_ptr<AdapterSettingsControl> adapterSettingsControl;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusCtrl;
    std::shared_ptr<ILockWindow> lockWindow;

    std::list<std::shared_ptr<IProcessWidget>> processWidgets;
    SoftAdapterDeviceCurrentStatus_t adapterStatus;
    SoftRecorderGetAccStateStruct_t adapterAcc;
    SoftRecorderStatusStruct_t recordStatus;
    std::thread statusThread;
//    std::condition_variable cv;
//    std::mutex cv_m;
    std::mutex dataMutex;
//    bool conditionWork;
    bool conditionWait;
    bool threadWork;
    std::map<StatusControlStates, std::string> stateWidgets;
    int adapterErrorCounter;
    int recorderErrorCounter;
    bool batteryHightTempWarning{false};
    bool batteryLowTempWarning{false};
    bool cpuHightTempWarning{false};

signals:
    void signalAdapterStatusUpdated();
    void signalRecorderStatusUpdated();
    void signalAdapterConnectionError();
    void signalRecorderConnectionError();
    void signalNetworkDataUpdated();
    void signalPrepareToClose();
    void signalUpdateCpuTemp();

public slots:
    void slotOnProcessWidgetSwipeSelected(std::string name, std::shared_ptr<IProcessWidget>);
    void slotOnProcessWidgetSwipeDeSelected(std::string name, std::shared_ptr<IProcessWidget>);
    void slotAdapterStatusUpdated();
    void slotRecorderStatusUpdated();
    void slotAdapterConnectionError();
    void slotRecorderConnectionError();
    void slotNetworkDataUpdated();
    void slotPrepareToClose();
    void slotUpdateCpuTemp();
};

#endif // STATUSCONTROL_H
