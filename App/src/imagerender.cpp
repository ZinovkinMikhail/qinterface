#include <QPainter>

#include "imagerender.h"
#include <iostream>

ImageRender::ImageRender(QQuickItem *parent): QQuickPaintedItem(parent)
{
    finalImage.load(":/icons/i14.png");
    keepAspectRatio = false;
}

void ImageRender::paint(QPainter *painter)
{
    painter->drawImage(QPoint(0,0), finalImage);
}

void ImageRender::addImage(QImage image)
{
    finalImage = image;
    finalImage = finalImage.scaled(this->width(),this->height(), Qt::IgnoreAspectRatio,
                                       Qt::FastTransformation);
}

void ImageRender::setActive(bool active)
{
    isActive = active;
}

void ImageRender::keepRatio(bool keep)
{
    keepAspectRatio = keep;
}
