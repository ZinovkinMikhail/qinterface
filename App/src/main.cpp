#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <execinfo.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTimer>
#include <QGuiApplication>
#include <QQmlContext>
#include <QFontDatabase>
#include <QQuickItem>
#include <QQmlComponent>
#include <QQuickView>
#include <QLockFile>
#include <QDir>

#include <rosslog/log.h>

#include "qmlengine.h"
#include "imagerender.h"

#include "mainwindow.h"

#define _STR(x) #x
#define STRINGIFY(x)  _STR(x)

using namespace rosslog;

void printBacktrace(int type);

void SignalHandler ( int sig )
{
    printf("Caught signal %d\n", sig);

    switch ( sig )
    {
        case SIGINT:
        {
            std::cerr << "User termination" << std::endl;
            printBacktrace(SIGINT);
            break;

        }

        case SIGILL:
        {
            std::cerr << "Illegal cpu instruction" << std::endl;
            printBacktrace(SIGILL);
            break;

        }

        case SIGKILL:
        {
            std::cerr << "Killed" << std::endl;
            printBacktrace(SIGKILL);
            break;

        }

        case SIGABRT:
        {
            std::cerr << "Abort" << std::endl;
            printBacktrace(SIGABRT);
            break;

        }

        case SIGSEGV:
        {
            std::cerr << "Memory error" << std::endl;
            printBacktrace(SIGSEGV);
            break;

        }

        case SIGTERM:
        {
            std::cerr << "Terminated" << std::endl;
            printBacktrace(SIGTERM);
            break;
        }
        case SIGBUS:
        {
            printBacktrace(SIGBUS);
            break;
        }
        case SIGPIPE:
        {
            std::cerr << "Broken pipe" << std::endl;
            printBacktrace(SIGPIPE);
            return;

        }

        case SIGUSR1:
        case SIGUSR2:
        {
            std::cerr << "User signal" << std::endl;
            return;

        }
        default:
        {
            std::cerr << "Unknown signal" << std::endl;
            printBacktrace(-1);
            break;
        }
    }

    printf("Closing app\n");
    exit(-1);

}

void printBacktrace(int type)
{
    int j, nptrs;
    int size = 100;

    void *buffer[size];
    char **strings;

    Log *logger = new Log(LOG_PATH, LOG_APP_NAME, 8, LOG_ENABLE_CONSOLE, MAX_LOG_FILE_SIZE); //LOG_LEVEL

    nptrs = backtrace ( buffer, size );

    strings = backtrace_symbols ( buffer, nptrs );

    if ( strings == NULL )
    {
        std::cerr << "Failed backtrace_symbols" << std::endl;
        return;
    }
    logger->Message(LOG_ERR,__LINE__, __func__, "----------backtrace start---------- signal: %i", type);

    for ( j = ( nptrs - 1 ); j > 1 ; j-- )
    {
        logger->Message(LOG_ERR,__LINE__, __func__, strings[j]);
    }

    free ( strings );
    logger->Message(LOG_ERR,__LINE__, __func__, "----------backtrace end---------");
    delete logger;
}


static void catch_signals ( void )
{
    signal ( SIGINT, SignalHandler );
    signal ( SIGILL, SignalHandler );
    signal ( SIGABRT, SignalHandler );
    signal ( SIGTERM, SignalHandler );
    signal ( SIGSEGV, SignalHandler );
    signal ( SIGPIPE, SignalHandler );
    signal ( SIGUSR1, SignalHandler );
    signal ( SIGUSR2, SignalHandler );
    signal ( SIGQUIT, SignalHandler );
    signal ( SIGKILL, SignalHandler );
    signal ( SIGSTOP, SignalHandler );
    signal ( SIGBUS, SignalHandler );
}

int main(int argc, char *argv[])
{
    printf("======= COMMIT VERSION = " STRINGIFY(GIT_VERSION) "=======\n\n");
    Q_UNUSED(argc);
    Q_UNUSED(argv);

    catch_signals();

    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));



    QLockFile lockFile(QDir::temp().absoluteFilePath("qinterface.lock"));

    if(!lockFile.tryLock(100)){
        std::cerr << "Can launch only one App " << std::endl;
        return 1;
    }



    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    qmlRegisterType<ImageRender>("app.render",1,0,"ImageRender");

    engine.addImportPath(STRINGIFY(QML_COMMON_PATH));

    engine.rootContext()->setContextProperty("applicationDirPath",
                                             QGuiApplication::applicationDirPath());

    QFontDatabase::addApplicationFont(STRINGIFY(FONTS_COMMON_PATH)"/CharterITC_Bold_Italic.otf");
    QFontDatabase::addApplicationFont(STRINGIFY(FONTS_COMMON_PATH)"/OpenSans-Semibold.ttf");
    QFontDatabase::addApplicationFont(STRINGIFY(FONTS_COMMON_PATH)"/OpenSans-Regular.ttf");
    QFontDatabase::addApplicationFont(STRINGIFY(FONTS_COMMON_PATH)"/SourceSansPro-Regular.ttf");
    std::cerr << STRINGIFY(QML_COMMON_PATH) << std::endl;
    engine.addImportPath(STRINGIFY(QML_COMMON_PATH));

    QmlEngine customEnigne(engine);
    QQmlComponent component(&engine, QUrl(QStringLiteral("qrc:/MainWindow.qml")));


    QObject *object = component.create();

    if( object == nullptr )
    {
            for( const auto &error : component.errors() )
            {
                    std::cerr << error.toString().toStdString() << std::endl;
            }
    }

    Q_ASSERT(object);

    std::shared_ptr<MainWindow> mainWindow = std::shared_ptr<MainWindow>(
                new MainWindow(std::shared_ptr<IQmlEngine>(&customEnigne), object));

    mainWindow->initMainWindow();

    Q_UNUSED(mainWindow);

    return app.exec();
}
