#include "apptimer.h"

AppTimer::AppTimer() : timer(nullptr)
{
    initAppTimer();
}

AppTimer &AppTimer::getInstance()
{
    static AppTimer appTimer;
    return appTimer;
}

int64_t AppTimer::getCurrentTime()
{
    return QDateTime::currentMSecsSinceEpoch();
}

void AppTimer::initAppTimer()
{
    if( timer == nullptr )
    {
        timer = new QTimer();
        timer->setInterval(APP_TIMER_INTERVAL);
        connect(timer, SIGNAL(timeout()), this, SLOT(slotAppTimerTick()));
        timer->start();
    }
}

void AppTimer::slotAppTimerTick()
{
    emit signalAppTimerPush();
}
