#ifndef THREEDOTMENUWIDGET_H
#define THREEDOTMENUWIDGET_H

#include <memory>

#include <QObject>
#include <QQuickItem>

#include "iqmlengine.h"

#define THREEDOT_MENU_QML_PATH "qrc:/ThreeDotWidgetMenu.qml"


class ThreeDotMenuWidget : public QObject
{
    Q_OBJECT
public:
    explicit ThreeDotMenuWidget(std::shared_ptr<IQmlEngine> qmlEngine, QObject *parent = nullptr);

    void init();
    void showMenu();
    void hideMenu();

private:
    std::shared_ptr<IQmlEngine> engine;
    QQuickItem *currentItem;

signals:

public slots:
};

#endif // THREEDOTMENUWIDGET_H
