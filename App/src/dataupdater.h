#ifndef DATAUPDATER_H
#define DATAUPDATER_H

#include <QObject>

#include <thread>

#include <rosslog/log.h>

#include <dataprovider/dataprovider.h>
#include <dataprovider/dataprovidererror.h>
#include <dataprovider/recordersettingscontol.h>
#include <dataprovider/recorderstatuscontrol.h>

#include "datakeeper.h"

#define SLEEP_TIME 5


class DataUpdater : public QObject
{
    Q_OBJECT
public:
    explicit DataUpdater(rosslog::Log &log, std::shared_ptr<data_provider::DataProvider> dataProvider,
                         std::shared_ptr<DataKeeper> dataKeeper, QObject *parent = nullptr);

    void initDataUpdater();

private:
    void initData();

    rosslog::Log *logger;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<DataKeeper> dataKeeper;
    std::thread *thread;
    RecorderSettingsContol *recorderSettingsCtrl;
    recorder_status_ctrl::RecorderStatusControl *recorderStatusCtrl;

    static void threadWork(void *object);

signals:

public slots:
};

#endif // DATAUPDATER_H
