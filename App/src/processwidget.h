#ifndef PROCESSWIDGET_H
#define PROCESSWIDGET_H

#include <QObject>
#include <QQuickItem>
#include <QVariant>

#include <rosslog/log.h>

#include "iprocesswidget.h"
#include "iqmlengine.h"

#define PROCESS_WIDGET_QML_PATH "qrc:/ProcessWidget.qml"

class ProcessWidget : public QObject
{
    Q_OBJECT
public:
    explicit ProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                           rosslog::Log &log, QObject *parent = nullptr);

    void init();
    QObject *getRootItem();

    void addProcessWidget(std::shared_ptr<IProcessWidget> widget, std::string name);
    void removeProcessWidget(std::shared_ptr<IProcessWidget> widget, std::string name);

private:
    rosslog::Log *logger;
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    QQuickItem *swipeView;
    std::map<std::string, std::shared_ptr<IProcessWidget>> widgetList;
    bool firstItem;

signals:
    void insertNewWidget(QObject *widget);

public slots:
    //Show signal from widget
    void slotShowProcessWidget(std::string name,
                               std::shared_ptr<IProcessWidget> widget);
    void slotMakeFullSized();

};

#endif // PROCESSWIDGET_H
