#include <assert.h>
#include <iostream>
#include <QApplication>
#include <sys/time.h>

#include "statuscontrol.h"

#include "apptimer.h"

//#include <fstream>
//#include <string>
//#include <iostream>

#include <QFile>

using namespace rosslog;
using namespace data_provider;
using namespace adapter_status_ctrl;
using namespace recorder_status_ctrl;

StatusControl::StatusControl(rosslog::Log &log, std::shared_ptr<IQmlEngine> qmlEngine,
                             std::shared_ptr<DataProvider> dataProvider,
                             std::shared_ptr<IStateWidget> stateWidget,
                             std::shared_ptr<IUserManipulators> userManipulators,
                             std::shared_ptr<ILockWindow> lockWindow,
                             QObject *parent) :
    QObject(parent),
    logger(&log),
    qmlEngine(qmlEngine),
    dataProvider(dataProvider),
    stateWidget(stateWidget),
    userManipulators(userManipulators),
    lockWindow(lockWindow),
    adapterStatusControl(nullptr),
    adapterSettingsControl(nullptr),
    conditionWait(false),
    threadWork(true),
    recorderStatusCtrl(nullptr)
{
    assert(this->stateWidget);
    assert(this->dataProvider);
    assert(this->lockWindow);
    memset(&adapterStatus, 0, sizeof(SoftAdapterDeviceCurrentStatus_t));
    memset(&adapterAcc, 0, sizeof(SoftRecorderGetAccStateStruct_t));
    adapterErrorCounter = 0;
    recorderErrorCounter = 0;
}

void StatusControl::initStatusControl()
{
    //initCopyProcessWidget();
    initAdapterStatusControl();
    initAdapterSettingsControl();
    initRecorderStatusControl();

    connect(this, SIGNAL(signalAdapterStatusUpdated()),
            this, SLOT(slotAdapterStatusUpdated()));
    connect(this, SIGNAL(signalRecorderStatusUpdated()),
            this, SLOT(slotRecorderStatusUpdated()));

    connect(this, SIGNAL(signalAdapterConnectionError()),
            this, SLOT(slotAdapterConnectionError()));
    connect(this, SIGNAL(signalRecorderConnectionError()),
            this, SLOT(slotRecorderConnectionError()));
    connect(this, SIGNAL(signalNetworkDataUpdated()),
            this, SLOT(slotNetworkDataUpdated()));

    connect(this, SIGNAL(signalPrepareToClose()),
            this, SLOT(slotPrepareToClose()));

    connect(this, SIGNAL(signalUpdateCpuTemp()),
            this, SLOT(slotUpdateCpuTemp()));

    initStateWidgets();

    statusThread = std::thread(&StatusControl::threadFunction, this);

    dataProvider->addDataWaiter(BATTERY_EXT_POWER_OFF, shared_from_this());
    dataProvider->addDataWaiter(AXIS_INTERFACE_CMD_GO_SLEEP, shared_from_this());
    dataProvider->addDataWaiter(AXIS_INTERFACE_EVENT_SAVE_MODE, shared_from_this());
}

void StatusControl::prepareToDelete()
{
//    dataProvider->removeDataWaiter(BATTERY_EXT_POWER_OFF, shared_from_this());
//    dataProvider->removeDataWaiter(AXIS_INTERFACE_CMD_GO_SLEEP, shared_from_this());
//    dataProvider->removeDataWaiter(AXIS_INTERFACE_EVENT_SAVE_MODE, shared_from_this());
}

std::list<std::shared_ptr<IProcessWidget>> StatusControl::getProcessWidgets()
{
    return processWidgets;
}

StatusControl::~StatusControl()
{
    threadWork = false;
    conditionWait = false;
//    cv.notify_one();

    if( statusThread.joinable() )
    {
        statusThread.join();
    }

    std::cerr << "Remove StatusControl" << std::endl;
}

void StatusControl::recieveData(int command, std::vector<uint8_t> data, size_t size)
{
    Q_UNUSED(command);
    Q_UNUSED(data);
    Q_UNUSED(size);

    std::cerr << "Recieved event " << command << std::endl;

    if( command == BATTERY_EXT_POWER_OFF )
    {
        conditionWait = false;
//        cv.notify_one();
    }
    else if( command == AXIS_INTERFACE_EVENT_SAVE_MODE )
    {
        emit signalNetworkDataUpdated();
    }
    else if( command == AXIS_INTERFACE_CMD_GO_SLEEP )
    {
        emit signalPrepareToClose();
    }
}

void StatusControl::initStateWidgets()
{
    stateWidget->addState("ACC_STATUS_HALF", PROCESS, QPixmap(":/icons/i9.png"), true);
    stateWidgets[ACC_STATUS_HALF] = "ACC_STATUS_HALF";

    stateWidget->addState("ACC_STATUS_FULL", PROCESS, QPixmap(":/icons/battery3.png"), true);
    stateWidgets[ACC_STATUS_FULL] = "ACC_STATUS_FULL";

    stateWidget->addState("ACC_STATUS_LOW", PROCESS, QPixmap(":/icons/battery1.png"), true);
    stateWidgets[ACC_STATUS_LOW] = "ACC_STATUS_LOW";

    stateWidget->addState("ACC_STATUS_VERY_LOW", PROCESS, QPixmap(":/icons/battery0.png"), true);
    stateWidgets[ACC_STATUS_VERY_LOW] = "ACC_STATUS_VERY_LOW";

    stateWidget->addState("ACC_STATUS_SEMIFULL", PROCESS, QPixmap(":/icons/batterySemiFull.png"), true);
    stateWidgets[ACC_STATUS_SEMIFULL] = "ACC_STATUS_SEMIFULL";

    stateWidget->addState("NETWOR_CONNECTION_ETH", INDICATOR, QPixmap(":/icons/i3.png"), true);
    stateWidgets[NETWOR_CONNECTION_ETH] = "NETWOR_CONNECTION_ETH";

    stateWidget->addState("EXTERNAL_ACC_VOLTAGE", PROCESS, QPixmap(":/icons/i8.png"), false);
    stateWidgets[EXTERNAL_ACC_VOLTAGE] = "EXTERNAL_ACC_VOLTAGE";

    stateWidget->addState("USERS", INDICATOR, QPixmap(":/icons/i7.png"), true);
    stateWidgets[USERS] = "USERS";

    stateWidget->addState("USB_CONNECTION", INDICATOR, QPixmap(":/icons/i5.png"), true);
    stateWidgets[USB_CONNECTION] = "USB_CONNECTION";

    stateWidget->addState("HEADPHONES", INDICATOR, QPixmap(":/icons/headphones.png"), true);
    stateWidgets[HEADPHONES] = "HEADPHONES";

    stateWidget->addState("ACC_NO_CHARGE", INDICATOR, QPixmap(":/icons/batteryNoCharge.png"), true);
    stateWidgets[ACC_NO_CHARGE] = "ACC_NO_CHARGE";

    stateWidget->addState("KEYBOARD_LOCKED", INDICATOR, QPixmap(":/icons/lockRed.png"), true);
    stateWidgets[KEYBOARD_LOCKED] = "KEYBOARD_LOCKED";

    stateWidget->addState("CPU_TEMP_NORMAL", PROCESS, QPixmap(":/icons/cpuTemp.png"), true);
    stateWidgets[CPU_TEMP_NORMAL] = "CPU_TEMP_NORMAL";

    stateWidget->addState("CPU_TEMP_ALARM", PROCESS, QPixmap(":/icons/cpuTempAlarm.png"), true);
    stateWidgets[CPU_TEMP_ALARM] = "CPU_TEMP_ALARM";

}

void StatusControl::initCopyProcessWidget()
{
}

void StatusControl::threadFunction()
{
    struct timeval tv;
    __time_t lastTime = 0;

    while(threadWork)
    {
//        std::unique_lock<std::mutex> lk(this->cv_m);
//        cv.wait_for(lk, std::chrono::seconds(10), [this]{return conditionWork;});
//        conditionWork = false;

         gettimeofday(&tv, nullptr);
         if( tv.tv_sec >= (lastTime + 10 ) || !conditionWait || tv.tv_sec < lastTime )
         {
             conditionWait = true;
             lastTime = tv.tv_sec;

             this->logger->Message(LOG_INFO, __LINE__, __func__, "Update adapter data");
             try
             {
                 dataMutex.lock();
                 adapterStatusControl->updateStatus();
                 this->adapterStatus = adapterStatusControl->getAdapterStatus();
                 this->adapterAcc = adapterStatusControl->getAccStatus();
                 dataMutex.unlock();
                 emit signalAdapterStatusUpdated();
             }
             catch( std::system_error &systemErr )
             {
                 dataMutex.unlock();
                 this->logger->Message(LOG_ERR, __LINE__,
                                       __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
                 emit signalAdapterConnectionError();
             }

             this->logger->Message(LOG_INFO, __LINE__, __func__, "Update recorder data");
             try
             {
                 dataMutex.lock();
                 recorderStatusCtrl->updateStatus();
                 this->recordStatus = recorderStatusCtrl->getRecorderStatus();
                 dataMutex.unlock();
                 emit signalRecorderStatusUpdated();
             }
             catch( std::system_error &systemErr )
             {
                 dataMutex.unlock();
                 this->logger->Message(LOG_ERR, __LINE__,
                                       __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
                 emit signalRecorderConnectionError();
             }

             emit signalUpdateCpuTemp();
         }
         else
         {
             usleep(10000);
         }
    }
}

void StatusControl::initAdapterStatusControl()
{
    if( adapterStatusControl == nullptr )
    {
        adapterStatusControl = std::shared_ptr<AdapterStatusControl>(
                    new AdapterStatusControl(dataProvider, *logger));
    }
}

void StatusControl::initAdapterSettingsControl()
{
    if( adapterSettingsControl == nullptr )
    {
        adapterSettingsControl = std::shared_ptr<AdapterSettingsControl>(
                    new AdapterSettingsControl(dataProvider, *logger));
    }
}

void StatusControl::initRecorderStatusControl()
{
    if( recorderStatusCtrl == nullptr )
    {
        recorderStatusCtrl = std::shared_ptr<RecorderStatusControl>(
                    new RecorderStatusControl(dataProvider, *logger));
    }
}

void StatusControl::updateBatteryState()
{

//    int fullcapacity = (adapterAcc.AccStatus.int_acc.acc_full_voltage * 100) -
//            (adapterAcc.AccStatus.int_acc.acc_low_voltage * 100);
//    int currcapacity = (adapterAcc.AccStatus.int_acc.acc_full_voltage * 100) -
//            adapterAcc.AccStatus.int_acc.acc_voltage ;
//    int percent = 100.0 - ((currcapacity * 100.0)/(float)fullcapacity);

    int percent = adapterAcc.AccStatus.int_acc.acc_rem_capacity/ 100.0;


    StatusControlStates currentBatteryState = ACC_STATUS_VERY_LOW;


    if( percent <= 10 )
    {
        stateWidget->hideState("ACC_STATUS_HALF");
        stateWidget->hideState("ACC_STATUS_FULL");
        stateWidget->hideState("ACC_STATUS_LOW");
        stateWidget->hideState("ACC_STATUS_SEMIFULL");
        currentBatteryState = ACC_STATUS_VERY_LOW;
    }
    else if( percent <= 25 && percent > 10  )
    {
        stateWidget->hideState("ACC_STATUS_HALF");
        stateWidget->hideState("ACC_STATUS_FULL");
        stateWidget->hideState("ACC_STATUS_VERY_LOW");
        stateWidget->hideState("ACC_STATUS_SEMIFULL");
        currentBatteryState = ACC_STATUS_LOW;
    }
    else if( percent <= 50 && percent > 25  )
    {
        stateWidget->hideState("ACC_STATUS_LOW");
        stateWidget->hideState("ACC_STATUS_FULL");
        stateWidget->hideState("ACC_STATUS_VERY_LOW");
        stateWidget->hideState("ACC_STATUS_SEMIFULL");
        currentBatteryState = ACC_STATUS_HALF;
    }
    else if( percent <= 75 && percent > 50 )
    {
        stateWidget->hideState("ACC_STATUS_LOW");
        stateWidget->hideState("ACC_STATUS_HALF");
        stateWidget->hideState("ACC_STATUS_VERY_LOW");
        stateWidget->hideState("ACC_STATUS_FULL");
        currentBatteryState = ACC_STATUS_SEMIFULL;
    }
    else if( percent <= 110 && percent > 75  )
    {
        stateWidget->hideState("ACC_STATUS_LOW");
        stateWidget->hideState("ACC_STATUS_HALF");
        stateWidget->hideState("ACC_STATUS_VERY_LOW");
        stateWidget->hideState("ACC_STATUS_SEMIFULL");
        currentBatteryState = ACC_STATUS_FULL;
    }
    else
    {
        stateWidget->hideState("ACC_STATUS_FULL");
        stateWidget->hideState("ACC_STATUS_LOW");
        stateWidget->hideState("ACC_STATUS_HALF");
        stateWidget->hideState("ACC_STATUS_VERY_LOW");
        stateWidget->hideState("ACC_STATUS_SEMIFULL");
        currentBatteryState = ACC_STATUS_LOW;
    }

    stateWidget->showState(stateWidgets.at(currentBatteryState),
                           util::format("%.2f", this->adapterAcc.AccStatus.int_acc.acc_voltage / 1000.0) + " B.");

    LOG(this->logger, LOG_INFO, "========= this->adapterAcc.AccStatus.int_acc.acc_voltage = %d", this->adapterAcc.AccStatus.int_acc.acc_voltage );
}


void StatusControl::slotAdapterStatusUpdated()
{
    this->logger->Message(LOG_INFO, __LINE__, __func__, "DEVICE CONNECTED %x, DEVICE TYPE %x",
                          adapterStatus.DevStatus, adapterStatus.DevType);

    if( adapterStatus.iconnected )
    {
        struct in_addr ip_addr;
//        ip_addr.s_addr = adapterStatus.ip;
        ip_addr.s_addr = htonl(adapterStatus.ip);
        char *ipStr = inet_ntoa(ip_addr);

        stateWidget->showState(stateWidgets.at(NETWOR_CONNECTION_ETH), ipStr);
    }
    else
    {
        stateWidget->hideState(stateWidgets.at(NETWOR_CONNECTION_ETH));
    }


    switch( axis_acc_get_status_power( &adapterAcc.AccStatus ) )
    {
        case AXIS_ACC_IO_STATUS__USB:
        case AXIS_ACC_IO_STATUS__DC:
        case AXIS_ACC_IO_STATUS__AC:
        case AXIS_ACC_IO_STATUS__EXT_ACC:
        {
            stateWidget->showState(stateWidgets.at(EXTERNAL_ACC_VOLTAGE),
                               util::format("%.2f", this->adapterAcc.Voltage / 1000.0) + " B.");
            break;
        }
        default:
        {
            stateWidget->hideState(stateWidgets.at(EXTERNAL_ACC_VOLTAGE));
            break;
        }
    }


    if( adapterStatus.userscount > 0 )
    {
        std::string ipStrResult = "";
        uint32_t clientIp;
        struct in_addr ip_addr;
        int maxIpStatusSize = sizeof(adapterStatus.clientIp)/sizeof(adapterStatus.clientIp[0]);
//        bool haveUsbClient = false;

        for(int i=0; i < adapterStatus.userscount; i++)
        {
            if(i < maxIpStatusSize)
            {
                clientIp = adapterStatus.clientIp[i];

                if( axis_is_ip_empty(clientIp) == 0 )
                {
                    ip_addr.s_addr = htonl(clientIp);
                    char *ipStr = inet_ntoa(ip_addr);

                    if(i > 0)
                        ipStrResult += "\n";

                    ipStrResult += ipStr;

                    // Check if ip address third octet == 4 (USB network) (don't need in new device version)
//                    if(getIpOctet(clientIp, 1) == 4)
//                    {
//                        haveUsbClient = true;
//                    }
                }
            }
        }

        stateWidget->showState(stateWidgets.at(USERS), ipStrResult);

//        if(haveUsbClient)
//            stateWidget->showState(stateWidgets.at(USB_CONNECTION), GetInterfaceIP(USB_NETWORK_INTERFACE));
//        else
//            stateWidget->hideState(stateWidgets.at(USB_CONNECTION));
    }
    else
    {
        stateWidget->hideState(stateWidgets.at(USERS));
//        stateWidget->hideState(stateWidgets.at(USB_CONNECTION));
    }

    this->updateBatteryState();


    LOG(this->logger, LOG_INFO, "========= acc_temperature= %d", adapterAcc.AccStatus.int_acc.acc_temperature );
    
    if(adapterAcc.AccStatus.int_acc.acc_temperature > 33)
    {
        if(!batteryHightTempWarning)
        {
            batteryHightTempWarning = true;
            batteryLowTempWarning = false;
            userManipulators->showMessage(std::string("Перегрев АКБ: Заряд остановлен."));
            stateWidget->showState(stateWidgets.at(ACC_NO_CHARGE),"Перегрев АКБ: Заряд остановлен.");
        }
    }
    else if (adapterAcc.AccStatus.int_acc.acc_temperature < 33)
    {
        if(!batteryLowTempWarning)
        {
            batteryLowTempWarning = true;
            batteryHightTempWarning = false;
            userManipulators->showMessage(std::string("Переохлаждение АКБ: Заряд остановлен."));
            stateWidget->showState(stateWidgets.at(ACC_NO_CHARGE),"Переохлаждение АКБ: Заряд остановлен.");
        }
    }
    else if (batteryHightTempWarning || batteryLowTempWarning)
    {
        batteryHightTempWarning = false;
        batteryLowTempWarning = false;
        stateWidget->hideState(stateWidgets.at(ACC_NO_CHARGE));
    }
}


void StatusControl::slotRecorderStatusUpdated()
{
    if( recordStatus.ExtStatus & RECORDER_EXT_STATUS__PLAY_PHONE )
    {
        stateWidget->showState(stateWidgets.at(HEADPHONES),"");
    }
    else
    {
        stateWidget->hideState(stateWidgets.at(HEADPHONES));
    }    

    if( recordStatus.ExtStatus & RECORDER_EXT_STATUS__KEYBOARD_LOCK )
    {
        stateWidget->showState(stateWidgets.at(KEYBOARD_LOCKED),"Включена блокировка клавиш");
    }
    else
    {
        stateWidget->hideState(stateWidgets.at(KEYBOARD_LOCKED));
    }

    if( recordStatus.ExtStatus & RECORDER_EXT_STATUS__USB_CONNECT )
    {
        stateWidget->showState(stateWidgets.at(USB_CONNECTION), GetInterfaceIP(USB_NETWORK_INTERFACE));
    }
    else
    {
        stateWidget->hideState(stateWidgets.at(USB_CONNECTION));
    }
}

uint8_t StatusControl::getIpOctet(uint32_t ipAddr, int octetNum)
{
    uint8_t bytes[4];
    bytes[0] = ipAddr & 0xFF;
    bytes[1] = (ipAddr >> 8) & 0xFF;
    bytes[2] = (ipAddr >> 16) & 0xFF;
    bytes[3] = (ipAddr >> 24) & 0xFF;

    return bytes[octetNum];
}

std::string StatusControl::GetInterfaceIP(const char* interfaceName)
{
    struct ifreq ifr;

    int fd = socket(AF_INET, SOCK_DGRAM, 0);

    /* I want to get an IPv4 IP address */
     ifr.ifr_addr.sa_family = AF_INET;

     /* I want IP address attached to "eth0" */
     strncpy(ifr.ifr_name, interfaceName, IFNAMSIZ-1);

     ioctl(fd, SIOCGIFADDR, &ifr);

     close(fd);

     /* display result */
//      printf("%s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));

      char *ipStr = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);

      return ipStr;
}

void StatusControl::slotOnProcessWidgetSwipeSelected(std::string name, std::shared_ptr<IProcessWidget>)
{
    Q_UNUSED(name);
    this->logger->Message(LOG_INFO, __LINE__, __func__, "Widget selected %s", name.c_str());
}

void StatusControl::slotOnProcessWidgetSwipeDeSelected(std::string name, std::shared_ptr<IProcessWidget>)
{
    Q_UNUSED(name);
    this->logger->Message(LOG_INFO, __LINE__, __func__, "Widget deselected %s", name.c_str());
}

void StatusControl::slotAdapterConnectionError()
{
    adapterErrorCounter++;
    if(adapterErrorCounter > 2)
    {
      adapterErrorCounter = 0;
      userManipulators->hideLoadingWidget();
      userManipulators->showMessage(std::string("Адаптер: Ошибка соединения."));
    }
}

void StatusControl::slotRecorderConnectionError()
{
    //Recorder connection error message shows from ChannelControlWidget.
}

void StatusControl::slotNetworkDataUpdated()
{
    if( !lockWindow->isVisible() )
    {
        userManipulators->showMessage(std::string("Параметры накопителя обновлены по сети."));
    }
}

void StatusControl::slotPrepareToClose()
{
    this->logger->Message(LOG_INFO, __LINE__, __func__, "GO_SLEEP command. Start close plugins.");
    userManipulators->closePlugins();
    this->logger->Message(LOG_INFO, __LINE__, __func__, "QUIT!");
    QApplication::quit();
}

void StatusControl::slotUpdateCpuTemp()
{
    QFile file;
    file.setFileName(CPU_TEMP_FILE);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        std::cerr << "(updateCpuTempState) Can't open file: " << file.fileName().toStdString() << std::endl;

        stateWidget->hideState("CPU_TEMP_NORMAL");
        stateWidget->hideState("CPU_TEMP_ALARM");
    }
    else
    {
        QString val = file.readAll();
        file.close();

        StatusControlStates currentCpuTempState;

        int cpuTemp = val.toInt()/1000;

        if( cpuTemp < __CPU_TEMPR_ALARM )
        {
            stateWidget->hideState("CPU_TEMP_ALARM");
            currentCpuTempState = CPU_TEMP_NORMAL;
            cpuHightTempWarning = false;
        }
        else
        {
            stateWidget->hideState("CPU_TEMP_NORMAL");
            currentCpuTempState = CPU_TEMP_ALARM;
            if(!cpuHightTempWarning)
            {
                cpuHightTempWarning = true;
                userManipulators->showMessage(std::string("Перегрев ЦПУ: Отключите дисплей на некоторое время для снижения температуры."));
            }
        }

        stateWidget->showState(stateWidgets.at(currentCpuTempState), std::to_string(cpuTemp) + " ℃");
    }
}
