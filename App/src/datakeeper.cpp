#include "datakeeper.h"

DataKeeper::DataKeeper()
{
    memset(&recorderSettings, 0, sizeof(SoftRecorderSettingsStruct_t));
    memset(&recorderStatus, 0, sizeof(SoftRecorderStatusStruct_t));
}

void DataKeeper::setRecorderSettings(SoftRecorderSettingsStruct_t data)
{
    dataMutex.lock();
    this->recorderSettings = data;
    dataMutex.unlock();
}

void DataKeeper::getRecorderSettings(SoftRecorderSettingsStruct_t &data)
{
    dataMutex.lock();
    data = recorderSettings;
    dataMutex.unlock();
}

void DataKeeper::setRecorderStatus(SoftRecorderStatusStruct_t data)
{
    dataMutex.lock();
    this->recorderStatus = data;
    dataMutex.unlock();
}

void DataKeeper::getRecorderStatus(SoftRecorderStatusStruct_t &data)
{
    dataMutex.lock();
    data = recorderStatus;
    dataMutex.unlock();
}

void DataKeeper::getTest()
{
    dataMutex.lock();
    dataMutex.unlock();
}
