#include <assert.h>

#include "threedotmenuwidget.h"

ThreeDotMenuWidget::ThreeDotMenuWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                       QObject *parent)
//    QObject(parent),
//    engine(qmlEngine),
//    currentItem(nullptr)
{
//    assert(engine);
}

void ThreeDotMenuWidget::init()
{
    currentItem = qobject_cast<QQuickItem*>(engine->createComponent(THREEDOT_MENU_QML_PATH, this));
    assert(currentItem);
    currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
}

void ThreeDotMenuWidget::showMenu()
{
    QMetaObject::invokeMethod(currentItem, "show");
}

void ThreeDotMenuWidget::hideMenu()
{
    QMetaObject::invokeMethod(currentItem, "hide");
}
