#include "lockwindow.h"

#include <assert.h>
#include <QQmlProperty>
#include <unistd.h>

using namespace rosslog;
using namespace recorder_status_ctrl;

LockWindow::LockWindow(QObject *parent) :
    QObject(parent),
    qmlEngine(nullptr),
    currentItem(nullptr),
    dataProvider(nullptr),
    recordSettingsControl(nullptr),
    userManipulators(nullptr),
    updateTimer(nullptr)
{
//    assert(this->qmlEngine);
    devicePass = "";
}

LockWindow::~LockWindow()
{
    if( updateTimer != nullptr )
        delete updateTimer;
}


void LockWindow::initWindow(std::shared_ptr<data_provider::DataProvider> dataProvider,
                            std::shared_ptr<IUserManipulators> userManipulators,
                            std::shared_ptr<IQmlEngine> qmlEngine,
                            std::shared_ptr<rosslog::Log> log,
                            std::shared_ptr<IStateWidget> stateWidget)
{
    this->dataProvider = dataProvider;
    this->qmlEngine = qmlEngine;
    this->logger = log;
    this->userManipulators = userManipulators;
    this->stateWidget = stateWidget;

    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(qmlEngine->createComponent(LOCK_WINDOW_QML_PATH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

        connect(currentItem, SIGNAL(requestAccess(QVariant)),
                this, SLOT(slotRequestAccess(QVariant)),Qt::QueuedConnection);
        connect(this, SIGNAL(signalRecivedAdapterData()),
                this, SLOT(slotRecievedAdapterData()));
        connect(this, SIGNAL(signalUpdateDataError()),
                this, SLOT(slotUpdateDataError()));
        connect(this, SIGNAL(signalEraseFinished()),
                this, SLOT(slotEraseFinished()));
        connect(this, SIGNAL(signalRecievedRecorderStatus()),
                this, SLOT(slotRecievedRecorderStatus()));
    }

    initAdapterSettingsControl();
    initRecorderStatusControl();

    updateTimer = new QTimer();

    connect(updateTimer, SIGNAL(timeout()),
            this, SLOT(slotUpdateTimerTimeout()));

    connect(&eraseUpdateTimer, SIGNAL(timeout()),
            this, SLOT(slotEraseTimerTimeout()));

}


void LockWindow::show()
{
    QMetaObject::invokeMethod(currentItem, "showErrorScreen",
                            Q_ARG(QVariant, QString::fromStdString("Ожидание ответа от накопителя...")));

    userManipulators->showLoadingWidget();
    updateData();
    QMetaObject::invokeMethod(currentItem, "show");
}

void LockWindow::hide()
{
    QQmlProperty(currentItem, "visible").write(false);
}

void LockWindow::startEraseInternalMemory()
{

}

void LockWindow::slotEraseFinished()
{
    this->hide();
    //Todo reset password
    QMetaObject::invokeMethod(currentItem, "eraseFinished");

    int errorPassResetCount = 0;

    while (errorPassResetCount < 5 )
    {
        try
        {
            char blockPass[6] = {0};

            strncpy((char*)recordSettings.BlockCode,blockPass,
                    sizeof(recordSettings.BlockCode));
            recordSettingsControl->saveRecordSettings(recordSettings);
            this->userManipulators->showMessage("Пароль сброшен до заводских настроек.");
            return;
        }
        catch( std::system_error &systemErr )
        {
            sleep(1);
            this->logger->Message(LOG_ERR, __LINE__,
                                  __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
            errorPassResetCount++;
        }
    }
    if( errorPassResetCount >= 5 )
    {
        this->userManipulators->showMessage("Не удалось сбросить пароль.");
    }
}

void LockWindow::slotRecievedRecorderStatus()
{
    std::cerr << "FLASH ERASE STATUS = " << recordStatus.FlashStatus << std::endl;
    switch (static_cast<FLASH_STATUS>(recordStatus.FlashStatus))
    {
        case fsFlashErasing:
        {
            isEraseStarted = true;
            this->userManipulators->hideLoadingWidget();
            std::cerr << "Flasg progress = " << recordStatus.FlashProgress << std::endl;
            QMetaObject::invokeMethod(currentItem, "eraseStatusUpdate",
                                      Q_ARG(QVariant, (recordStatus.FlashProgress)));
            break;
        }
        default:
        {
            if( isEraseStarted )
            {
                if( recordStatus.FlashProgress == 0 )
                {
                    eraseUpdateTimer.stop();
                    emit signalEraseFinished();
                }
                isEraseStarted = false;
            }
            else
            {
                if( eraseStartCounter > 5 )
                {
                    this->userManipulators->hideLoadingWidget();
                    eraseUpdateTimer.stop();
                    isEraseStarted = false;
                    QMetaObject::invokeMethod(currentItem, "eraseFinished");
                    this->userManipulators->showMessage("Ошибка при запуске стирания.");
                }
                eraseStartCounter++;
            }
            break;
        }
    }
}

void LockWindow::slotEraseTimerTimeout()
{
    std::cerr << "ERASE STATUS UPDATE " << std::endl;
    if( eraseStatusUpdateMutex.try_lock() )
    {
        updateWorker = std::async(std::launch::async, [this] () mutable
        {
            try
            {
                recorderStatusControl->updateStatus();
                this->recordStatus = recorderStatusControl->getRecorderStatus();
                eraseStatusUpdateMutex.unlock();
                emit signalRecievedRecorderStatus();
            }
            catch( std::system_error &systemErr )
            {
                //TODO STOP ERASE SIGNAL ON FAIL
                this->logger->Message(LOG_ERR, __LINE__,
                                      __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
            }
            eraseStatusUpdateMutex.unlock();
        });
        updateWorker.wait_for(std::chrono::seconds(0));
    }

}

void LockWindow::slotRequestAccess(QVariant pass)
{
    if(devicePass.empty()) //default pass
    {
        if(!pass.toString().toStdString().compare("123456"))
        {
            this->hide();
        }
        else
        {
            QMetaObject::invokeMethod(currentItem, "accessDenied");
        }
        return;
    }

    if(!pass.toString().toStdString().compare(devicePass))
    {
        this->hide();
        return;
    }

    if( !pass.toString().toStdString().compare(DEFAULT_RESET_PASS_CODE_P1) && !isResetPass )
    {
        QMetaObject::invokeMethod(currentItem, "accessReset");
        isResetPass = true;
        return;
    }

    if( isResetPass )
    {
        if( !pass.toString().toStdString().compare(DEFAULT_RESET_PASS_CODE_P2) )
        {
            QMetaObject::invokeMethod(currentItem, "accessResetApproved");
            isResetPass = false;

            if( this->userManipulators->requestUserConfirm(std::string("Для сброса пароля будет произведена очистка памяти. Продолжить?")) )
            {
                auto states = stateWidget->countStates();

                for(auto it: states)
                {
                    if( it == "DATABASE_WORK_PROGRESS" )
                    {
                        this->userManipulators->showMessage("В данный момент производится подготовка данных сессий."
                                                            "Пожалуйста дождитесь завершения операции.");

                        QMetaObject::invokeMethod(currentItem, "accessResetCancel");
                        return;
                    }
                    else if( it == "COPY_WORK_PROGRESS" )
                    {
                        this->userManipulators->showMessage("В данный момент производится копирование данных. "
                                                            "Пожалуйста дождитесь завершения операции.");
                        QMetaObject::invokeMethod(currentItem, "accessResetCancel");
                        return;
                    }
                }

                try
                {
                    recorderStatusControl->updateStatus();
                    this->recordStatus = recorderStatusControl->getRecorderStatus();
                }
                catch( std::system_error &systemErr )
                {
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
                    this->userManipulators->showMessage("Не удалось получить статус накопителя. Повторите сброс.");

                    QMetaObject::invokeMethod(currentItem, "accessResetCancel");
                    return;
                }


                if( recordStatus.Chanel1Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
                    recordStatus.Chanel2Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
                    recordStatus.Chanel3Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
                    recordStatus.Chanel4Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW )
                {
                    this->userManipulators->showMessage("В данный момент идёт запись, пожалуйста остановите запись и повторите сброс.");
                    QMetaObject::invokeMethod(currentItem, "accessResetCancel");
                    return;
                }
                this->userManipulators->showLoadingWidget();

                eraseUpdateTimer.start(1000);

                try
                {
                    recorderStatusControl->eraseInternalMemory(true);
                    isResetPass = false;
                    std::cerr << "START ERASE MEMORY " << std::endl;
                }
                catch( std::system_error &except )
                {
                    userManipulators->showMessage(std::string("Не удалось начать стирание.\nКод ошибки: ").append(
                            std::to_string(except.code().value())));
                    std::cerr << "ERASE FAIL " << std::endl;
                    this->userManipulators->hideLoadingWidget();

                    QMetaObject::invokeMethod(currentItem, "accessResetCancel");
                }
            }
            else
            {
                isResetPass = false;
                QMetaObject::invokeMethod(currentItem, "accessResetCancel");
            }
            return;
        }
        else
        {
            isResetPass = false;
            QMetaObject::invokeMethod(currentItem, "accessResetFailed");
            return;
        }
    }

    isResetPass = false;

    if(!pass.toString().toStdString().compare(devicePass))
    {
        this->hide();
    }
    else
    {
        QMetaObject::invokeMethod(currentItem, "accessDenied");
    }
}


void LockWindow::updateData()
{
    updateWorker = std::async(std::launch::async,
                              &LockWindow::getInfoFromRecorder, this);
    updateWorker.wait_for(std::chrono::seconds(0));
}


void LockWindow::getInfoFromRecorder()
{
    try
    {
        this->recordSettingsControl->updateStatus();
        this->recordSettings = recordSettingsControl->getRecordSettings();
        emit signalRecivedAdapterData();
    }
    catch( std::system_error &systemErr )
    {
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());

        emit signalUpdateDataError();
    }
}

void LockWindow::initAdapterSettingsControl()
{
    if( recordSettingsControl == nullptr )
    {
        recordSettingsControl = std::shared_ptr<RecorderSettingsContol>(
                new RecorderSettingsContol(dataProvider, *logger.get()));
    }
}

void LockWindow::initRecorderStatusControl()
{
    if( recorderStatusControl == nullptr )
    {
        recorderStatusControl = std::make_shared<RecorderStatusControl>(dataProvider, *logger.get());
    }
}


void LockWindow::setRecievedData()
{
//    std::string str((char*)recordSettings.BlockCode);
//    devicePass = str;

    char blockCode[6] = {0};

    strncpy(blockCode,
            (char*)recordSettings.BlockCode,
            sizeof(blockCode));

    devicePass = blockCode;
}

bool LockWindow::isVisible()
{
    QVariant visible = QQmlProperty(currentItem, "visible").read();
    return visible.toBool();
}

void LockWindow::slotUpdateTimerTimeout()
{
    updateData();
}

void LockWindow::slotRecievedAdapterData()
{
    updateTimer->stop();
    setRecievedData();
    QMetaObject::invokeMethod(currentItem, "hideErrorScreen");
    userManipulators->hideLoadingWidget();
}

void LockWindow::slotUpdateDataError()
{
    if(!updateTimer->isActive())
    {
      updateTimer->start(5000);

//      QMetaObject::invokeMethod(currentItem, "showErrorScreen",
//                              Q_ARG(QVariant, QString::fromStdString("Ожидание ответа от накопителя...")));

      userManipulators->hideLoadingWidget();
    }
}




