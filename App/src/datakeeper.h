#ifndef DATAKEEPER_H
#define DATAKEEPER_H

#include <mutex>
#include <string.h>

#include <axis_softrecorder.h>
#include <axis_softadapter.h>

#include "idatakeeper.h"

class DataKeeper : public IDataKeeper
{
public:
    DataKeeper();

    void setRecorderSettings(SoftRecorderSettingsStruct_t data) override;
    void getRecorderSettings(SoftRecorderSettingsStruct_t &data) override;

    void setRecorderStatus(SoftRecorderStatusStruct_t data);
    void getRecorderStatus(SoftRecorderStatusStruct_t &data);
    void getTest();
private:
    std::mutex dataMutex;
    SoftRecorderSettingsStruct_t recorderSettings;
    SoftRecorderStatusStruct_t recorderStatus;

};


#endif // DATAKEEPER_H
