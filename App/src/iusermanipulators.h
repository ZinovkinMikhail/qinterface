#ifndef IUSERMANIPULATORS_H
#define IUSERMANIPULATORS_H

#include <vector>
#include <utility>
#include <string>
#include <functional>


class IUserManipulators
{
public:
    virtual std::pair<bool, int> openComboDrum(int currentIndex, std::vector<std::string> values) = 0;
    virtual void openKeyBoard(std::function<void(int)> &keyPressCallBack) = 0;
    virtual void closeKeyBoard() = 0;
    virtual void showLoadingWidget() = 0;
    virtual void hideLoadingWidget() = 0;
    virtual std::pair<bool, std::string> openDateComboDrum(int day, int mon, int year) = 0;
    virtual std::pair<bool, std::string> openTimeComboDrum(int hour, int min, int sec) = 0;
    virtual void showMessage(std::string msg) = 0;
    virtual bool requestUserConfirm(std::string msg) = 0;
    virtual bool closePlugins() = 0;

    virtual ~IUserManipulators() {}
};

#endif // IUSERMANIPULATORS_H
