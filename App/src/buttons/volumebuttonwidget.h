#ifndef VOLUMEBUTTONWIDGET_H
#define VOLUMEBUTTONWIDGET_H

#include <QObject>
#include <QQuickItem>
#include <QTimer>

#include <cmath>

#include <rosslog/log.h>

#include <dataprovider/dataprovider.h>
#include <dataprovider/dataprovidererror.h>
#include <dataprovider/recorderstatuscontrol.h>

#include "iusermanipulators.h"
#include "isimplebuttonwidget.h"
#include "iqmlengine.h"

#define VOLUME_BUTTON_QML_PATH "qrc:/VolumeButtonWidget.qml"

class VolumeButtonWidget : public QObject, public ISimpleButtonWidget,
        public std::enable_shared_from_this<VolumeButtonWidget>
{
    Q_OBJECT
public:
    explicit VolumeButtonWidget(std::shared_ptr<IQmlEngine> qmlEngine, rosslog::Log &log,
                                std::shared_ptr<data_provider::DataProvider> dataProvider,
                                std::shared_ptr<IUserManipulators> userManipulators,
                                QObject *parent = nullptr);
    ~VolumeButtonWidget();
    void init();

    void openButtonWidget();
    void closeButtonWidget();

protected:
    Q_INVOKABLE void buttonClicked();
    Q_INVOKABLE void buttonCloseClicked();

private:
    void initRecorderStatusControl();
    void setPlayerVolume(int volDb, bool isRepeat);

    rosslog::Log *logger;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recordStatusControl;
    QQuickItem *currentItem;
    QQuickItem *volumeSliderWidget;
    std::future<void> updateWorker;
    std::mutex updateMutex;
    int lastVolDB;
    int currentVol;
    bool volUpdated;
    QTimer *checkVolumeTimer;

signals:
    void signalButtonPressed(std::shared_ptr<ISimpleButtonWidget> button);
    void signalCloseButtonPressed(std::shared_ptr<ISimpleButtonWidget> button);

public slots:
    void slotButtonOpenClicked();
    void slotButtonCloseClicked();
    void slotVolumeChanged(QVariant volume);
    void slotTimerTimeout();
};

#endif // VOLUMEBUTTONWIDGET_H
