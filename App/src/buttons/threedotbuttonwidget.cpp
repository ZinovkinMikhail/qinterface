#include <assert.h>

#include <QApplication>

#include "threedotbuttonwidget.h"

using namespace rosslog;
using namespace data_provider;
using namespace adapter_status_ctrl;
using namespace recorder_status_ctrl;

ThreeDotButtonWidget::ThreeDotButtonWidget(std::shared_ptr<IQmlEngine> qmlEngine, Log &log,
                                           std::shared_ptr<DataProvider> dataProvider,
                                           std::shared_ptr<IUserManipulators> userManipulators,
                                           std::shared_ptr<IStateWidget> stateWidget,
                                           QObject *parent) :
    QObject(parent),
    logger(&log),
    dataProvider(dataProvider),
    qmlEngine(qmlEngine),
    userManipulators(userManipulators),
    stateWidget(stateWidget),
    adapterStatusControl(nullptr),
    isKeyboardLocked(false)
{
    assert(this->logger);
    assert(this->dataProvider);
    assert(this->qmlEngine);
    assert(this->userManipulators);
}

void ThreeDotButtonWidget::init()
{
    QObject *object = qmlEngine->createComponent(THREE_DOT_BUTTON_QML_PATH, this->parent());
    assert(object);
    currentItem = qobject_cast<QQuickItem*>(object);
    QQuickItem *parentItem = qobject_cast<QQuickItem*>(this->parent());
    currentItem->setParentItem(parentItem);
    connect(currentItem, SIGNAL(buttonOpenClicked()),
            this, SLOT(slotButtonOpenClicked()));
    connect(currentItem, SIGNAL(buttonCloseClicked()),
            this, SLOT(slotButtonCloseClicked()));
    connect(currentItem, SIGNAL(rebootButtonClicked()),
            this, SLOT(slotRebootButtonClicked()));
    connect(currentItem, SIGNAL(lpcButtonClicked()),
            this, SLOT(slotLpcButtonClicked()));
    connect(currentItem, SIGNAL(aboutButtonClicked()),
            this, SLOT(slotAboutButtonClicked()));
    connect(currentItem, SIGNAL(lockButtonClicked()),
            this, SLOT(slotLockButtonClicked()));
    connect(currentItem, SIGNAL(lockDispButtonClicked()),
            this, SLOT(slotLockDispButtonClicked()));
    connect(this, SIGNAL(signalRecivedRecorderData()),
            this, SLOT(slotRecievedRecorderData()));

    connect(&lpcTimer, SIGNAL(timeout()),
            this, SLOT(slotLPCTimerTimeout()));

    initAdapterStatusControl();
    initRecorderStatusControl();

}

void ThreeDotButtonWidget::openButtonWidget()
{
    QMetaObject::invokeMethod(currentItem, "show");
    updateRecorderData();
    //set kayboard state on result
}

void ThreeDotButtonWidget::closeButtonWidget()
{
    QMetaObject::invokeMethod(currentItem, "hide");
}

void ThreeDotButtonWidget::initAdapterStatusControl()
{
    if( adapterStatusControl == nullptr )
    {
        adapterStatusControl = std::make_shared<AdapterStatusControl>(dataProvider, *logger);
    }
}

void ThreeDotButtonWidget::initRecorderStatusControl()
{
    if( recorderStatusControl == nullptr )
    {
        recorderStatusControl = std::make_shared<RecorderStatusControl>(dataProvider, *logger);
    }
}


void ThreeDotButtonWidget::updateRecorderData()
{
    this->userManipulators->showLoadingWidget();
    updateWorker = std::async(std::launch::async, [this] () mutable
    {
        try
        {
            recorderStatusControl->updateStatus();
            recorderStatus = recorderStatusControl->getRecorderStatus();

            this->userManipulators->hideLoadingWidget();
            emit signalRecivedRecorderData();
        }
        catch (std::system_error &except)
        {
            this->userManipulators->hideLoadingWidget();
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            userManipulators->showMessage(std::string("Не удалось обновить данные.\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
        }
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}


void ThreeDotButtonWidget::slotRecievedRecorderData()
{
    if( recorderStatus.ExtStatus & RECORDER_EXT_STATUS__KEYBOARD_LOCK )
    {
        isKeyboardLocked = true;
        stateWidget->showState("KEYBOARD_LOCKED","Включена блокировка клавиш");
    }
    else
    {
        isKeyboardLocked = false;
        stateWidget->hideState("KEYBOARD_LOCKED");
    }

    QMetaObject::invokeMethod(currentItem,"setKeyboardButtonState", Q_ARG(QVariant, isKeyboardLocked));
}

void ThreeDotButtonWidget::slotLPCTimerTimeout()
{
    if( !userManipulators->closePlugins() )
    {
        userManipulators->hideLoadingWidget();
        lpcTimer.stop();
        return;
    }


    updateWorker = std::async(std::launch::async, [this] () mutable
    {
        try
        {
            adapterStatusControl->doLPC();
            std::cerr << "DO LPC SENT" << std::endl;
        }
        catch (std::system_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            userManipulators->showMessage(std::string("Не удалось отправить команду ПЭП.\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
        }
        userManipulators->hideLoadingWidget();
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}


void ThreeDotButtonWidget::slotButtonOpenClicked()
{
    this->logger->Message(LOG_INFO, __LINE__, __func__, "On Three dot button clicked");
    emit signalButtonPressed(shared_from_this());
}

void ThreeDotButtonWidget::slotButtonCloseClicked()
{
    this->logger->Message(LOG_INFO, __LINE__, __func__, "On Three dot button close clicked");
    emit signalCloseButtonPressed(shared_from_this());
}

void ThreeDotButtonWidget::slotRebootButtonClicked()
{
    bool approveed = this->userManipulators->requestUserConfirm(std::string("Вы уверены, что хотите перезапустить устройство?"));

    if(approveed)
    {
        userManipulators->showLoadingWidget();
        updateWorker = std::async(std::launch::async, [this] () mutable
        {
            try
            {
                adapterStatusControl->doReboot();
            }
            catch (std::system_error &except)
            {
                this->logger->Message(LOG_ERR, __LINE__, __func__,
                                      dpr::ProviderError::getInstance().getErrorString(except).c_str());
                userManipulators->showMessage(std::string("Не удалось отправить команду перезапуска.\nКод ошибки: ").append(
                                                  std::to_string(except.code().value())));
            }
            userManipulators->hideLoadingWidget();
        });
        updateWorker.wait_for(std::chrono::seconds(0));
    }
}

void ThreeDotButtonWidget::slotLpcButtonClicked()
{
    bool approveed = this->userManipulators->requestUserConfirm(std::string("Вы уверены, что хотите перевести устройство в режим пониженного энергопотребления?"));

    if(approveed)
    {
        userManipulators->showMessage("Подготовка к переходу в П.Э.П. адаптера");

        QApplication::processEvents();


        lpcTimer.start(1);
    }
}

void ThreeDotButtonWidget::slotAboutButtonClicked()
{   
    emit signalShowAboutWindow();
}

void ThreeDotButtonWidget::slotLockButtonClicked()
{
    userManipulators->showLoadingWidget();
    updateWorker = std::async(std::launch::async, [this] () mutable
    {
        bool toLock = !isKeyboardLocked;
        try
        {
            adapterStatusControl->setKeyBoardBlock(toLock);
//            isKeyboardLocked = !isKeyboardLocked;
//            QMetaObject::invokeMethod(currentItem,"setKeyboardButtonState", Q_ARG(QVariant, isKeyboardLocked));
        }
        catch (std::system_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            if( toLock )
            {
                userManipulators->showMessage(std::string("Не удалось отправить команду блокировки клавиатуры.\nКод ошибки: ").append(
                        std::to_string(except.code().value())));
            }
            else
            {
                userManipulators->showMessage(std::string("Не удалось отправить команду разблокировки клавиатуры.\nКод ошибки: ").append(
                        std::to_string(except.code().value())));
            }

        }
        userManipulators->hideLoadingWidget();
    });
    updateWorker.wait_for(std::chrono::seconds(0));

    updateRecorderData();
}

void ThreeDotButtonWidget::slotLockDispButtonClicked()
{
    emit signalLockWindow();
}
