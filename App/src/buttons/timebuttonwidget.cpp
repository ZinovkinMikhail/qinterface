#include <assert.h>
#include <iostream>
#include <ctime>
#include <memory>

#include <QVariant>
#include <QQmlComponent>
#include <QQuickItem>

#include "timebuttonwidget.h"

using namespace rosslog;
using namespace data_provider;

TimeButtonWidget::TimeButtonWidget(rosslog::Log &log,
                                   std::shared_ptr<IQmlEngine> qmlEngine,
                                   std::shared_ptr<DataProvider> dataProvider,
                                   QObject *parent) :
    QObject(parent),
    engine(qmlEngine),
    currentItem(nullptr),
    logger(&log),
    dataProvider(dataProvider),
    recordSettingControl(nullptr)
{
    assert(logger);
    assert(dataProvider);
    assert(parent);
    assert(engine);
}

void TimeButtonWidget::initTimeButtonWidget()
{
    QObject *object = engine->createComponent(TIME_WIDGET_QML_PATH, this);
    assert(object);

    currentItem = qobject_cast<QQuickItem*>(object);
    QQuickItem *parentItem = qobject_cast<QQuickItem*>(this->parent());
    currentItem->setParentItem(parentItem);

//    connect(currentItem, SIGNAL(buttonOpenClicked()),
//            this, SLOT(slotOpenTimeWidget()));
//    connect(currentItem, SIGNAL(buttonCloseClicked()),
//            this, SLOT(slotCloseTimeWidget()));

    timer = std::make_shared<QTimer>(new QTimer());
    connect(timer.get(), SIGNAL(timeout()), this, SLOT(slotTimerTimeout()));
    timer->start(15000);

    initRecorderSettingControl();
#warning ETO NADO???? LAG
    //getTimeFromRecorder();
    updateTime();
}

void TimeButtonWidget::openButtonWidget()
{
    QMetaObject::invokeMethod(currentItem, "show");
}

void TimeButtonWidget::closeButtonWidget()
{
    QMetaObject::invokeMethod(currentItem, "hide");
}

void TimeButtonWidget::initRecorderSettingControl()
{
    if( recordSettingControl == nullptr )
    {
        recordSettingControl =
                std::shared_ptr<RecorderSettingsContol>(new RecorderSettingsContol(dataProvider, *logger));
    }
}

void TimeButtonWidget::updateTime()
{
    std::time_t time = std::time(0);
    std::tm *timeNow = std::localtime(&time);

    std::string minutes = util::format("%02d", timeNow->tm_min);
    std::string hour = util::format("%02d", timeNow->tm_hour);
    QMetaObject::invokeMethod(currentItem, "setTime",
                              Q_ARG(QVariant, (QString::fromStdString(hour) +
                                    ":" + QString::fromStdString(minutes))));
}

uint64_t TimeButtonWidget::getTimeFromRecorder()
{
    try
    {
        if( recordSettingControl == nullptr )
            throw std::runtime_error("RecordSettingsControl is not inited");
        this->recordSettingControl->updateStatus();
        return this->recordSettingControl->getRecordSettings().Time;
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__,
                              dpr::ProviderError::getInstance().getErrorString(except).c_str());
    }
    return 0;
}

//void TimeButtonWidget::slotOpenTimeWidget()
//{
//    emit signalButtonPressed(shared_from_this());
//}

//void TimeButtonWidget::slotCloseTimeWidget()
//{
//    emit signalCloseButtonPressed(shared_from_this());
//}

void TimeButtonWidget::slotTimerTimeout()
{
    updateTime();
}
