#include <assert.h>

#include "volumebuttonwidget.h"

using namespace data_provider;
using namespace recorder_status_ctrl;
using namespace rosslog;

VolumeButtonWidget::VolumeButtonWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                       rosslog::Log &log,
                                       std::shared_ptr<DataProvider> dataProvider,
                                       std::shared_ptr<IUserManipulators> userManipulators,
                                       QObject *parent) :
    QObject(parent),
    logger(&log),
    dataProvider(dataProvider),
    qmlEngine(qmlEngine),
    userManipulators(userManipulators),
    recordStatusControl(nullptr),
    currentItem(nullptr)
{
    assert(logger);
    assert(userManipulators);
    assert(dataProvider);
    assert(qmlEngine);

    volUpdated = false;
    lastVolDB = 0;
    currentVol = 0;
}

void VolumeButtonWidget::init()
{
    QObject *object = qmlEngine->createComponent(VOLUME_BUTTON_QML_PATH, this->parent());
    assert(object);

    currentItem = qobject_cast<QQuickItem*>(object);
    auto *parentItem = qobject_cast<QQuickItem*>(this->parent());
    currentItem->setParentItem(parentItem);
    volumeSliderWidget = currentItem->findChild<QQuickItem*>("VolumeSliderWidget");
    assert(volumeSliderWidget);

    connect(qobject_cast<QObject*>(volumeSliderWidget), SIGNAL(volumeChanged(QVariant)),
            this, SLOT(slotVolumeChanged(QVariant)));

    connect(currentItem, SIGNAL(buttonOpenClicked()),
            this, SLOT(slotButtonOpenClicked()));
    connect(currentItem, SIGNAL(buttonCloseClicked()),
            this, SLOT(slotButtonCloseClicked()));

    initRecorderStatusControl();

    checkVolumeTimer = new QTimer();

    connect(checkVolumeTimer, SIGNAL(timeout()),
            this, SLOT(slotTimerTimeout()));

    QMetaObject::invokeMethod(volumeSliderWidget, "setVolume",
                              Q_ARG(QVariant, (0.5)));

    slotVolumeChanged(0.5);
}

void VolumeButtonWidget::openButtonWidget()
{
    QMetaObject::invokeMethod(currentItem, "show");
}

void VolumeButtonWidget::closeButtonWidget()
{
    QMetaObject::invokeMethod(currentItem, "hide");
}

void VolumeButtonWidget::buttonClicked()
{
    emit signalButtonPressed(shared_from_this());
}

void VolumeButtonWidget::buttonCloseClicked()
{
    emit signalCloseButtonPressed(shared_from_this());
}

void VolumeButtonWidget::initRecorderStatusControl()
{
    if( recordStatusControl == nullptr )
    {
        recordStatusControl =
                std::shared_ptr<RecorderStatusControl>(new RecorderStatusControl(dataProvider, *logger));
    }
}

void VolumeButtonWidget::slotButtonOpenClicked()
{
    emit signalButtonPressed(shared_from_this());
}

void VolumeButtonWidget::slotButtonCloseClicked()
{
    emit signalCloseButtonPressed(shared_from_this());
}

void VolumeButtonWidget::slotVolumeChanged(QVariant volume)
{
    int volProc = static_cast<int>(volume.toFloat() * 100);
    int volM = ceil((double)volProc * 60.0 / 100.0);
    int vol = 0;

    if( volM > 30 )
    {
        vol = volM - 30;
    }
    else if( volM < 30 )
    {
        vol = volM - 30;
    }
    else
        vol = 0;

    setPlayerVolume(vol , false);
}

void VolumeButtonWidget::slotTimerTimeout()
{
    if( !volUpdated )
    {
        if( lastVolDB != currentVol )
        {
            setPlayerVolume(currentVol, true);
        }
        else
        {
            volUpdated = true;
            checkVolumeTimer->stop();
        }
    }
}

VolumeButtonWidget::~VolumeButtonWidget()
{
    if( checkVolumeTimer != nullptr )
        delete checkVolumeTimer;
}

void VolumeButtonWidget::setPlayerVolume(int volDB, bool isRepeat)
{
    if( updateMutex.try_lock() )
    {
        updateWorker = std::async( std::launch::async,[this, volDB, isRepeat]
        {
            try
            {
                recordStatusControl->setVolumePlayAudio(volDB);
                this->lastVolDB = volDB;
            }
            catch (std::system_error &except)
            {
                this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while update volume value");
                updateMutex.unlock();
                currentVol = lastVolDB;
                volUpdated = false;
                checkVolumeTimer->stop();
                this->userManipulators->showMessage("Ошибка установки громкости!");
            }
            updateMutex.unlock();
        });
        updateWorker.wait_for(std::chrono::seconds(0));
    }
    else
    {
        checkVolumeTimer->start(100);
        this->volUpdated = false;
        currentVol = volDB;
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Inpossible to update volume, locked");
    }
}
