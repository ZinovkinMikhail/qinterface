#ifndef TIMEBUTTONWIDGET_H
#define TIMEBUTTONWIDGET_H

#include <QObject>
#include <QQuickItem>
#include <QTimer>

#include <rosslog/log.h>

#include <dataprovider/dataprovider.h>
#include <dataprovider/recordersettingscontol.h>
#include <dataprovider/dataprovidererror.h>

#include "util.h"

#include "isimplebuttonwidget.h"
#include "iqmlengine.h"

#define TIME_WIDGET_QML_PATH "qrc:/TimeButtonWidget.qml"

class TimeButtonWidget : public QObject,  public ISimpleButtonWidget,
        public std::enable_shared_from_this<TimeButtonWidget>
{
    Q_OBJECT
public:
    explicit TimeButtonWidget(rosslog::Log &log,
                              std::shared_ptr<IQmlEngine> qmlEngine,
                              std::shared_ptr<data_provider::DataProvider> dataProvider,
                              QObject *parent = nullptr);
    void initTimeButtonWidget();

    void openButtonWidget();
    void closeButtonWidget();

private:
    void initRecorderSettingControl();
    void updateTime();

    uint64_t getTimeFromRecorder();
    uint64_t currentTime;
    std::shared_ptr<IQmlEngine> engine;
    QQuickItem *currentItem;
    rosslog::Log *logger;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<RecorderSettingsContol> recordSettingControl;
    std::shared_ptr<QTimer> timer;

signals:
    void signalButtonPressed(std::shared_ptr<ISimpleButtonWidget> button);
    void signalCloseButtonPressed(std::shared_ptr<ISimpleButtonWidget> button);

public slots:
//    void slotOpenTimeWidget();
//    void slotCloseTimeWidget();
    void slotTimerTimeout();
};

#endif // TIMEBUTTONWIDGET_H
