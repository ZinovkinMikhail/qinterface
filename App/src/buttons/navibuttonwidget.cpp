#include <assert.h>

#include "navibuttonwidget.h"

NaviButtonWidget::NaviButtonWidget(std::shared_ptr<IQmlEngine> qmlEngine, rosslog::Log &log, QObject *parent)
    : QObject(parent),
      logger(&log),
      qmlEngine(qmlEngine),
      currentItem(nullptr),
      curType(MENU)
{
    assert(logger);
    assert(qmlEngine);
}

void NaviButtonWidget::init()
{
    QObject *object = qmlEngine->createComponent(NAVI_BUTTON_QML_PATH, this->parent());
    assert(object);
    currentItem = qobject_cast<QQuickItem*>(object);
    QQuickItem *parentItem = qobject_cast<QQuickItem*>(this->parent());
    currentItem->setParentItem(parentItem);
    connect(currentItem, SIGNAL(buttonPressed()),
            this, SLOT(slotButtonPressed()));
}

void NaviButtonWidget::setButtonType(ENaviButType type)
{
    curType = type;
    QMetaObject::invokeMethod(currentItem, "changeButtonType",
                              Q_ARG(QVariant, static_cast<int>(type)));
}

ENaviButType NaviButtonWidget::getCurButtonType()
{
    return curType;
}

void NaviButtonWidget::slotButtonPressed()
{
    emit signalButtonPressed(curType);
}

