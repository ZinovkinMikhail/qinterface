#ifndef THREEDOTBUTTONWIDGET_H
#define THREEDOTBUTTONWIDGET_H

#include <QObject>
#include <QQuickItem>
#include <QTimer>

#include <rosslog/log.h>
#include <dataprovider/dataprovider.h>

#include "isimplebuttonwidget.h"
#include "iqmlengine.h"
#include "iusermanipulators.h"
#include "istatewidget.h"
#include <dataprovider/adapterstatuscontrol.h>
#include <dataprovider/recorderstatuscontrol.h>


#define THREE_DOT_BUTTON_QML_PATH "qrc:/ThreeDotButtonWidget.qml"

class ThreeDotButtonWidget : public QObject, public ISimpleButtonWidget,
        public std::enable_shared_from_this<ThreeDotButtonWidget>
{
    Q_OBJECT
public:
    explicit ThreeDotButtonWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                  rosslog::Log &log,
                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                  std::shared_ptr<IUserManipulators> userManipulators,
                                  std::shared_ptr<IStateWidget> stateWidget,
                                  QObject *parent = nullptr);

    void init();

    void openButtonWidget();
    void closeButtonWidget();

private:
    rosslog::Log *logger;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    std::future<void> updateWorker;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<IStateWidget> stateWidget;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusControl;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusControl;
    bool isKeyboardLocked;
    SoftRecorderStatusStruct_t recorderStatus;
    QTimer lpcTimer;

    void initAdapterStatusControl();
    void initRecorderStatusControl();
    void updateRecorderData();

signals:
    void signalButtonPressed(std::shared_ptr<ISimpleButtonWidget> button);
    void signalCloseButtonPressed(std::shared_ptr<ISimpleButtonWidget> button);
    void signalShowAboutWindow();
    void signalLockWindow();
    void signalRecivedRecorderData();

public slots:
    void slotButtonOpenClicked();
    void slotButtonCloseClicked();
    void slotRebootButtonClicked();
    void slotLpcButtonClicked();
    void slotAboutButtonClicked();
    void slotLockButtonClicked();
    void slotLockDispButtonClicked();
    void slotRecievedRecorderData();
    void slotLPCTimerTimeout();

};

#endif // THREEDOTBUTTONWIDGET_H
