#ifndef MENUBUTTONWIDGET_H
#define MENUBUTTONWIDGET_H

#include <QObject>
#include <QVariantMap>
#include <QQuickItem>
#include <QVariant>

#include <rosslog/log.h>

#include "isimplebuttonwidget.h"
#include "iqmlengine.h"

#define NAVI_BUTTON_QML_PATH "qrc:/NaviButtonWidget.qml"

typedef enum
{
    MENU,
    BACK
} ENaviButType;


class NaviButtonWidget : public QObject
{
    Q_OBJECT
public:
    explicit NaviButtonWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                              rosslog::Log &log, QObject *parent = nullptr);

    void init();

    void setButtonType(ENaviButType type);
    ENaviButType getCurButtonType();

private:
    rosslog::Log *logger;
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    ENaviButType curType;

signals:
    void signalButtonPressed(ENaviButType type);
//QML
    void changeButtonType(int type);

public slots:
    void slotButtonPressed();
};

#endif // MENUBUTTONWIDGET_H
