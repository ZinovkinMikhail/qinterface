#ifndef IMENUITEMEVENTSHANDLER_H
#define IMENUITEMEVENTSHANDLER_H

class IMenuItemWidget;

class IMenuItemEventsHandler
{
public:
    virtual void itemPress(std::shared_ptr<IMenuItemWidget> menuItemWidget) = 0;
    virtual void itemRelease(std::shared_ptr<IMenuItemWidget> menuItemWidget) = 0;
    virtual void openEvent(std::shared_ptr<IMenuItemWidget> menuItemWidget) = 0;

    virtual ~IMenuItemEventsHandler() {}
};

#endif // IMENUITEMEVENTSHANDLER_H
