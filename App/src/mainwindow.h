#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QObject>
#include <QKeyEvent>

#include <rosslog/log.h>

#include <dataprovider/dataprovider.h>

#include "windowsmanager.h"

#include "qmlengine.h"


#define LOG_PATH "/tmp/cm_interface"
#define LOG_APP_NAME "cm_interface"
#define LOG_LEVEL 8
#define LOG_ENABLE_CONSOLE true

#define _STR(x) #x
#define STRINGIFY(x)  _STR(x)

#define PLUGINS_PATH STRINGIFY(OUT_DIR)


class MainWindow : public QObject
{
    Q_OBJECT

public:
    explicit MainWindow(std::shared_ptr<IQmlEngine> qmlEngine,
                        QObject *parent = 0);
    void initMainWindow();
    void startTest();
    ~MainWindow();

protected:
    Q_INVOKABLE QObject *getTestServerWindow();

signals:
    void signalStartWork();

private slots:
    void slotStartWork();

private:
    void initDataProvider();
    void initWindowsManager();


    rosslog::Log *logger;
    std::shared_ptr<rosslog::Log> log;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<WindowsManager> windowsManager;
    std::shared_ptr<IQmlEngine> qmlEngine;
    
private:
    static MainWindow   *_self;
public:
    static MainWindow* getOwner();
    rosslog::Log* getLogger(){ return logger; }
};

#endif // MAINWINDOW_H
