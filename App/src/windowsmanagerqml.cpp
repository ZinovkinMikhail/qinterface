#include <QVariant>

#include "windowsmanagerqml.h"
#include <QQmlProperty>

using namespace rosslog;

WindowsManagerQML::WindowsManagerQML(std::shared_ptr<IQmlEngine> qmlEngine,
                                     rosslog::Log &log, QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    logger(&log)
{
    previousWindow = std::pair<QObject *, std::shared_ptr<IEmptyPlaceWindow> >(nullptr, nullptr);
    currentWindow = std::pair<QObject *, std::shared_ptr<IEmptyPlaceWindow> >(nullptr, nullptr);
}

void WindowsManagerQML::initWindowsManagerQML()
{
    if( qmlEngine != nullptr )
    {
        qmlEngine->setContextProperty(WINDOWS_MANAGER_QML_CLASS, this);
        connect(this->parent(), SIGNAL(closeFullScreenButton()),
                this, SLOT(slotCloseFromFullScreenButton()));
    }
}

void WindowsManagerQML::showInitialWindow()
{
    std::pair<QObject*, std::shared_ptr<IEmptyPlaceWindow>> window = findWindow(START_WINDOW_NAME);

    if( window.first != nullptr )
    {
        previousWindow = window;
        currentWindow = window;
        QMetaObject::invokeMethod(this->parent(), "showInitialWindow", Q_ARG(QVariant, QVariant::fromValue(window.first)));
        window.second->showWindow();
    }
}

void WindowsManagerQML::showFullScreenButton()
{
    QMetaObject::invokeMethod(this->parent(), "showFullscreenButton");
}

void WindowsManagerQML::hideFullScreenButton()
{
    QMetaObject::invokeMethod(this->parent(), "hideFullscreenButton");
}

void WindowsManagerQML::pushNewWindow(std::string name, std::pair<QObject *, std::shared_ptr<IEmptyPlaceWindow>> window)
{
    if( window.first != nullptr && name.compare("") != 0)
    {
#ifdef APP_DEBUG_MSG
        this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Push new window name = %s %p", name.c_str(), &window.second);
#endif
        windows[name] = window;
    }
}

void WindowsManagerQML::removeWindow(std::string name)
{
    if( name.empty() )
        return;

    auto it = windows.find(name);
    if( it != windows.end() )
    {
        //if( currentWindow.second->getWindowName() == "PlayBackWindow" )
        {
            showPreviousWindow("MainMenu");
            previousWindow.second = nullptr;
        }
        //if( name == "PlayBackWindow" )
        {
            windows.erase(it);
        }

    }
}


void WindowsManagerQML::showWindow(std::string windowName)
{
    if( windowName.compare("") != 0 )
    {
        std::pair<QObject*, std::shared_ptr<IEmptyPlaceWindow>> window = findWindow(windowName);
        if( window.first != nullptr  )
        {
            previousWindow = currentWindow;
            currentWindow = window;
            QMetaObject::invokeMethod(this->parent(), "showWindow",
                                      Q_ARG(QVariant, QVariant::fromValue(window.first)),
                                      Q_ARG(QVariant, QVariant::fromValue(
                                                static_cast<int>(window.second->getWinSizeType()))));
            window.second->showWindow();
//            previousWindow.second->hideWindow();
            this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Open window name = %s %p", window.second->getWindowName().c_str(), &window.second);
        }
    }
}

void WindowsManagerQML::showPreviousWindow(std::string name)
{
    std::pair<QObject*, std::shared_ptr<IEmptyPlaceWindow>> window = findWindow(name);
#warning Tut show na vseh idiot nado toliko na channel control
  //  window.second->showWindow();
    currentWindow.second->hideWindow();
    currentWindow  = window;
    QMetaObject::invokeMethod(this->parent(), "showPreviousWindow");
}

void WindowsManagerQML::showComboDrumWidget()
{
    emit showComboDrum();
}

void WindowsManagerQML::showLoadingWidget()
{
    QMetaObject::invokeMethod(this->parent(), "showLoading");
}

void WindowsManagerQML::hideLoadingWidget()
{
    QMetaObject::invokeMethod(this->parent(), "hideLoading");
}

std::string WindowsManagerQML::getPreviousWindowName()
{
    if( currentWindow.second != nullptr )
    {
        return currentWindow.second->getWindowName();
    }
    return "";
}

std::pair<QObject*, std::shared_ptr<IEmptyPlaceWindow>> WindowsManagerQML::findWindow(std::string name)
{
    auto it = windows.find(name);
    if( it != windows.end() )
    {
        return it->second;
    }
    return std::pair<QObject* , std::shared_ptr<IEmptyPlaceWindow>>(nullptr, nullptr);
}

void WindowsManagerQML::slotCloseFromFullScreenButton()
{

}

bool WindowsManagerQML::stackViewIsBusy()
{
    return QQmlProperty(this->parent(), "stackViewIsBusy").read().toBool();
}
