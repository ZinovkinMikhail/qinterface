//
// Created by ross on 30.06.2020.
//

#ifndef CM_INTERFACE_AUDIODATADOWNLOADER_H
#define CM_INTERFACE_AUDIODATADOWNLOADER_H

#include <memory>
#include <vector>
#include <fstream>

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <iostream>
#include <sys/time.h>

#include <sqlite3.h>


#include <rosslog/log.h>
#include <dataprovider/dataprovider.h>
#include <dataprovider/adapterstatuscontrol.h>

#ifdef DATABASE_USE
#include<watcher_database_manager/dbconnector.h>
#include<watcher_database_manager/packetcontrol.h>
#include<watcher_database_manager/packet.h>
#endif
#include <QDateTime>
#include <QFile>
#include <QTextStream>

namespace audio_downloader
{
typedef enum
{
	SOCKET_NOT_OPENED,
	SOCKET_NOT_CONNECTED,
	SOCKET_CONNECTED
} SocketState;

typedef enum
{
	THREAD_DISABLED,
	THREAD_ENABLED,
	THREAD_ERROR
} ThreadState;

typedef struct
{
	int socketFd;
	SocketState socketState;
	std::string socketPath;
	int socketClientFd;
	std::vector<uint8_t> dataVector;
} SocketData;

#define SOCKET_PATH "/tmp/monitoring"
#define BLOCK_SIZE 512
#define SOCKET_READ_TIMEOUT 1
#define VECTOR_MAX_SIZE 1024

typedef enum
{
    MONO_1,
    MONO_2,
    MONO_3,
    MONO_4,
    STEREO_1,
    STEREO_2
}ChannelType;

typedef struct
{
    uint64_t startTime;
    uint64_t endTime;
    bool isWrited;
    int packetsCount;
    int channel;
    int id;
    int pkd;
} ChannelParams;

typedef struct
{
    uint8_t id[2];
    uint8_t line[4];
    uint64_t time;
} AudioPack;

class AudioDataDownloader
{
public:
	explicit AudioDataDownloader(std::shared_ptr<data_provider::DataProvider> dataProvider,
		std::shared_ptr<rosslog::Log> log);
	
	bool createSocket();
	bool closeSocket();
	bool startDownload();
	bool startFileDownload();
    void startTest();

private:
	void initAdapterStatusControl();
	bool startThread();
	bool startFileThread();
	bool stopThread();
	size_t readSocket(SocketData *socketData, uint8_t &buff, size_t size, int timeout);
	void threadFileFunction();
	void threadFunction();
	bool bindSocket();
    bool connectSocket(SocketType socketType);


	std::shared_ptr<data_provider::DataProvider> dataProvider;
	std::shared_ptr<rosslog::Log> logger;
	std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusControl;
	SocketData socketData;
	std::thread channelThread;
	std::condition_variable cv;
	std::mutex cv_m;
	std::mutex dataMutex;
	bool threadWork;
	bool conditionWork;
    bool fileInited;
    FILE *fp;
    sqlite3 *db;
    std::vector<AudioPack> audioPacks;
};
}

#endif //CM_INTERFACE_AUDIODATADOWNLOADER_H
