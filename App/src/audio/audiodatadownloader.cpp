//
// Created by ross on 30.06.2020.
//
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <string>
#include <string.h>
#include <assert.h>
#include <chrono>

#include "audiodatadownloader.h"

using namespace audio_downloader;
using namespace rosslog;
using namespace data_provider;
using namespace adapter_status_ctrl;

static void sqlite3_test(sqlite3_context *context, int argc, sqlite3_value **argv)
{
    sqlite3 *db = sqlite3_context_db_handle(context);
    int rc;
    sqlite3_stmt *pTrunk = 0;
    switch (sqlite3_value_type(argv[0]) )
    {
        case SQLITE_TEXT:
        {
            const unsigned char *tVal = sqlite3_value_text(argv[0]);
            printf("TAL = %s\n", tVal);
        }
        case SQLITE_INTEGER:
        {
            long long int ival = sqlite3_value_int64(argv[0]);
            int ivalChannel = sqlite3_value_int64(argv[1]);

            printf("TAL = %lli\n", ival);
            printf("TAL = %i\n", ivalChannel);
            char buff[256];
            memset(&buff, 0 , sizeof(buff));

            snprintf(buff, 256, "SELECT data FROM Dummy WHERE start_time = %lli OR start_time - 1 = %lli", ival, ival);

            std::string insert = "INSERT into Dummy VALUES(?,?);";
            std::string select = "SELECT start_time FROM Dummy ORDER BY start_time DESC LIMIT 1 ;";


//            sprintf(buffReq, "INSERT INTO (start_time) Dummy VALUES (?)");
//            rc = sqlite3_prepare_v2(db,  insert.c_str(), -1, &stmt, &tail);
//
//            printf("RC = %i\n", rc);
//
//            rc = sqlite3_prepare_v2(db,  select.c_str(), -1, &stmtSEl, &tail);
//            printf("RC = %i\n", rc);

         //   rc = sqlite3_bind_int64(  pTrunk, 1, ival);

//            rc = sqlite3_prepare_v2(db, buff, -1, &pTrunk, 0);
//            if( rc!=SQLITE_OK )
//            {
//                printf("FUCKING ERROR = \n");
//            }
//            else
//            {
//             //   printf("FUCKING ZBS = \n");
//            }
//
//            uint32_t value = 0;
//
            bool sessionExist = false;
//            do
//            {
//                rc = sqlite3_step( pTrunk);
//                switch( rc )
//                {
//                    /** No more data */
//                    case SQLITE_DONE:
//                        break;
//
//                        /** New data */
//                    case SQLITE_ROW:
//                    {
//                        uint16_t size = sqlite3_column_count( pTrunk);
//
//                        if(size == 1) // should always be one
//                        {
//                            sessionExist = true;
//                            value   = sqlite3_column_int( pTrunk, 0);
//                            printf("test 1 = %i %s\n",value, sqlite3_column_text(pTrunk, 0));
//                            value   = sqlite3_column_int( pTrunk, 1);
//                            printf("test 2 = %i %s\n",value, sqlite3_column_text(pTrunk, 0));
//                        }
//                    }
//                        break;
//
//                    default:
//                        break;
//                }
//            } while( rc==SQLITE_ROW );

//            if( !sessionExist )
//            {
//                snprintf(buff, 256, "INSERT INTO Dummy (start_time, channel) VALUES( %lli, %i) ", ival, ivalChannel);
//                rc = sqlite3_prepare_v2(db, buff, -1, &pTrunk, 0);
//                if( rc!=SQLITE_OK )
//                {
//                    printf("FUCKING ERROR = \n");
//                }
//                else
//                {
//                    printf("FUCKING ZBS = \n");
//                }
//                rc = sqlite3_step( pTrunk);
//            }

            //uint32_t iTrunk = (uint32_t)sqlite3_column_int(pTrunk, 0);

           // printf("FUCKING ZBS = %i\n", value);
            const char *zTrunk = "";
        }
    }
    sqlite3_result_int(context, 22);
}

static int callback(void *data, int argc, char **argv, char **azColName) {
    int i;
    fprintf(stderr, "%s: ", (const char*)data);

    for(i = 0; i<argc; i++) {
        printf("suka %s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

AudioDataDownloader::AudioDataDownloader(std::shared_ptr<data_provider::DataProvider> dataProvider,
										std::shared_ptr<rosslog::Log> log) :
	dataProvider(dataProvider),
	logger(log),
	adapterStatusControl(nullptr),
    threadWork(false),
    fileInited(false)
{
	assert(this->dataProvider);
	assert(this->logger);
	initAdapterStatusControl();
    socketData.socketState = SOCKET_NOT_OPENED;
//
//    try
//    {
//        int rc;
//
//        rc = sqlite3_open("dummy.db", &db);
//        const char* data = "Callback function called";
//        char *zErrMsg = 0;
//
//        if( rc ) {
//            fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
//            abort();
//        } else {
//            fprintf(stderr, "Opened database successfully\n");
//        }
//        rc = sqlite3_create_function(db, "sqlite3_test", 2, SQLITE_UTF8, NULL, sqlite3_test, NULL, NULL);
//
//        if( rc != SQLITE_OK ) {
//            fprintf(stderr, "SQL error create func: %s\n", zErrMsg);
//            sqlite3_free(zErrMsg);
//        } else {
//            fprintf(stdout, "Operation done successfully\n");
//        }

//        rc = sqlite3_exec(db ,"SELECT sqlite3_test(1, 2)", callback, (void*)data, &zErrMsg );
//
//        if( rc != SQLITE_OK ) {
//            fprintf(stderr, "SQL error: %s\n", zErrMsg);
//            sqlite3_free(zErrMsg);
//        } else {
//            fprintf(stdout, "Operation done successfully\n");
//        }

//        std::cerr << "OPen db " << rc << std::endl;
//        Poco::Data::SQLite::Connector::registerConnector();
//
//        Session ses("SQLite", "dummy.db");
//
//        ses << "DROP TABLE IF EXISTS Dummy", now;
//
//        Statement stmt = (ses << "CREATE TABLE IF NOT EXISTS Dummy ( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, start_time INTEGER, end_time INTEGER, channel INTEGER )");
//
//        stmt.execute();

//        stmt = (ses << "CREATE INDEX index_name \n"
//                       "ON Dummy(start_time, end_time, channel);");
//
//        stmt.execute();

//        stmt = (ses << "DROP TABLE IF EXISTS Dummy_pkf");
//
//        stmt.execute();
//
//
//        stmt = (ses << "CREATE TABLE IF NOT EXISTS Dummy_pkf (  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, session_id INTEGER, peak INTEGER, time INTEGER)");
//
//        stmt.execute();
//    }
//    catch (Poco::Exception &except)
//    {
//        std::cerr << except.what() << std::endl;
//        abort();
//    }



}

bool AudioDataDownloader::createSocket()
{
    if( socketData.socketState == SOCKET_NOT_OPENED )
    {
        socketData.socketFd = -1;
        socketData.socketPath = SOCKET_PATH;
        socketData.socketState = SOCKET_NOT_OPENED;
        socketData.socketClientFd = -1;
        socketData.dataVector.resize(BLOCK_SIZE);

        struct stat buffer;
    //	if(stat(SOCKET_PATH, &buffer) != 0 && mkfifo(SOCKET_PATH, 0777) < 0) {
    //		return false;
    //	}

        int ret = socket(AF_UNIX, SOCK_STREAM , 0);

        if( ret == -1 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Unable to open socket path %s errno = %i",
                            socketData.socketPath.c_str(), errno);
            return false;
        }
        socketData.socketFd = ret;
        socketData.socketState = SOCKET_NOT_CONNECTED;
    }
	return true;
}

bool AudioDataDownloader::closeSocket()
{
	if( socketData.socketFd > 0 )
	{
		close(socketData.socketFd);
		socketData.socketState = SOCKET_NOT_OPENED;
		return  true;
	}
	return false;
}

bool AudioDataDownloader::startThread()
{
	if( threadWork )
		return false;
    threadWork = true;
	channelThread = std::thread(&AudioDataDownloader::threadFunction, this);
	return true;
}


size_t AudioDataDownloader::readSocket(SocketData *socketData, uint8_t &buff,
									   size_t size, int timeout)
{
    fd_set rfds;
    struct timeval tv;
    int ret = 0;
    int readed = 0;
    if( socketData->socketFd < 0 )
        return -1;

    FD_ZERO(&rfds);
    FD_SET(socketData->socketFd, &rfds );

    tv.tv_sec = timeout;
    tv.tv_usec = 0;

    logger->Message(LOG_ERR, __LINE__, __func__, "Select at socket = %i", socketData->socketFd);
    ret = select( socketData->socketFd + 1, &rfds, NULL, NULL, &tv);
    if( ret < 0 )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Select error errno = %i", errno);
        return 0;
    }
    else if( !ret )
    {
        logger->Message(LOG_INFO, __LINE__, __func__, "Timeout to read from partner");
        return 0;
    }
    else if( FD_ISSET( socketData->socketFd, &rfds ) )
    {
        readed = read(socketData->socketFd, &buff, size);

        if( readed <= 0 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Can't read from socket");
            return 0;
        }
    }
    return readed;
}

bool AudioDataDownloader::startDownload()
{
#ifdef DATABASE_USE
	SoftAdapterAudioHeaderDownload_s audio;
	audio.socket = socketData.socketFd;
	audio.from = 0x0000;
	audio.to = 1024;
    fp = fopen("/tmp/test.csv", "w+");

    if ( fp == NULL )
    {
        return false;
    }

	try
	{
        startThread();
        //adapterStatusControl->startAudioHeaderDownload(audio);
	}

	catch(std::system_error &except)
	{
        closeSocket();
		return false;
	}
	return true;
#endif
}

void AudioDataDownloader::initAdapterStatusControl()
{

	if( adapterStatusControl == nullptr )
	{
		adapterStatusControl = std::make_shared<AdapterStatusControl>(dataProvider, *logger.get());

#ifdef DATABASE_USE
        PacketControl packetControl;
        DbConnector connector;
        Packet pack;
        try
        {
            connector.dbConnect("cicada_db","127.0.0.1","postgres","555432");
            std::string tmp = "SELECT * FROM cicada_sessions;";
            packetControl.customRequest(tmp,&pack,connector.connection);
            connector.CloseConn(connector.connection);
        }
        catch(std::runtime_error &excep)
        {
            connector.CloseConn(connector.connection);
            //throw std::runtime_error(excep.what());
        }

        std::cerr << "Val size = " << pack.getValsSize() << std::endl;
#endif
	}

}



void AudioDataDownloader::threadFunction()
{
}

bool AudioDataDownloader::stopThread()
{
	threadWork = false;
	conditionWork = true;
	cv.notify_one();
	return true;
}


bool AudioDataDownloader::connectSocket(SocketType socketType)
{
    if( socketData.socketState == SOCKET_NOT_CONNECTED )
    {
        logger->Message(LOG_INFO, __LINE__, __func__, "Connect socket");
        struct sockaddr_un addr;
        memset(&addr, 0, sizeof(struct sockaddr_un));
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, SOCKET_PATH, sizeof(addr.sun_path) -1);

        if( connect(socketData.socketFd, (struct sockaddr*) &addr, sizeof(struct sockaddr_un) ) == -1 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Unable to connect socket errno = %i", errno);
            return false;
        }
        socketData.socketState = SOCKET_CONNECTED;

        logger->Message(LOG_INFO, __LINE__, __func__, "New socket client");
    }
    return true;
}

void AudioDataDownloader::startTest()
{
}

bool AudioDataDownloader::bindSocket()
{
	if( socketData.socketState == SOCKET_NOT_CONNECTED )
	{
		//unlink(currentSocket->socketPath.c_str());
		struct sockaddr_un addr;
		memset(&addr, 0, sizeof(struct sockaddr_un));
		addr.sun_family = AF_UNIX;
		strncpy(addr.sun_path, socketData.socketPath.c_str(), sizeof(addr.sun_path) -1);
		addr.sun_path[ sizeof( addr.sun_path ) - 1 ] = 0x00;
		if( bind( socketData.socketFd, (struct sockaddr *)&addr, sizeof(addr)) < 0 )
		{
			logger->Message(LOG_ERR, __LINE__, __func__, "Can't bind unix socket errno = %i", errno);
			close(socketData.socketFd);
			return false;
		}
		
		if( listen( socketData.socketFd, 2 ) )
		{
			logger->Message(LOG_ERR, __LINE__, __func__, "Can't listen unix socket= %i", errno);
			close(socketData.socketFd);
			return false;
		}
		
		logger->Message(LOG_INFO, __LINE__, __func__, "New socket server");
		socketData.socketState = SOCKET_CONNECTED;
	}
	return true;
}

void AudioDataDownloader::threadFileFunction()
{
    while(threadWork)
    {
        QFile inputFile("/home/ivan/test6");

        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        if (inputFile.open(QIODevice::ReadOnly))
        {
            char data[1024];
            memset(&data, 0, sizeof(data));
            int ret = 0;
            bool isWork = true;
            char *zErrMsg = 0;
            int rc;

            std::vector<uint64_t> packetByOrder;
            std::vector<uint64_t> droppedPackets;

            std::vector<uint64_t> packetByOrderMono;
            std::vector<uint64_t> droppedPacketsMono;

            int countPacks = 0;
            int countPacksMono = 0;

            std::map<int ,std::vector<uint64_t>> sessionsMap;


            uint64_t currentDate = QDateTime::currentDateTime().toSecsSinceEpoch();
            uint64_t lowPossibleYear = currentDate - (1 * 31536000);

            uint64_t time = 0;
            uint64_t timeSel = 0;
            int channel = 0;
            uint64_t pkd = 0;

            sqlite3_stmt * stmt;
            sqlite3_stmt * stmtPKD;
            sqlite3_stmt * stmtSEl;
            sqlite3_stmt * stmtUpdate;

            const char * tail = 0;
            char buffReq[256];
            std::string newReq = "SELECT sqlite3_test(?, ?)";
            std::string insertPKD = "INSERT into Dummy_pkf (session_id, peak, time) VALUES(?,?,?);";
            std::string insert = "INSERT into Dummy (start_time, end_time, channel) VALUES(?,?,?);";
            std::string update = "UPDATE Dummy SET end_time = ? WHERE start_time = ? AND channel = ?";

            rc = sqlite3_prepare_v2(db,  insert.c_str(), -1, &stmt, &tail);
            printf("RC = %i\n", rc);

            rc = sqlite3_prepare_v2(db,  update.c_str(), -1, &stmtUpdate, &tail);
            printf("RC = %i\n", rc);

            rc = sqlite3_prepare_v2(db,  insertPKD.c_str(), -1, &stmtPKD, &tail);
            printf("RC = %i\n", rc);


            std::vector<ChannelParams> channelParams;
            channelParams.resize(6); //WE HAVE 6 types

            rc = sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &zErrMsg);

            while( isWork )
            {
                uint8_t *dataU = (uint8_t*)&data;
                int k = 0;

                uint64_t lastTime = 0;
                uint64_t lastTime4a = 0;
                ret = inputFile.read(data, 1024);

                ChannelParams *currentChannel;
                //Нам надо писать и проверять если есть такая сессия

                if( ret == 0 )
                {
                    isWork = false;
                    break;
                }

                for( int x = 0; x < 1024; x++ )
                {
                    int res;
                    if( dataU[x] == 0x5a ) //SETEREO
                    {
                        time = 0;
                        time <<= 8;
                        time |= dataU[x + 5];
                        time <<= 8;
                        time |= dataU[x + 4];
                        time <<= 8;
                        time |= dataU[x + 7];
                        time <<= 8;
                        time |= dataU[x + 6];
                        channel = dataU[x + 1];

                        pkd <<= 8;
                        pkd |= dataU[x + 3];
                        pkd <<= 8;
                        pkd |= dataU[x + 2];

                        int count = 0;


                        if( channel >= 0 && channel <= 4 )
                        {
                            if( channel == 0 )
                            {
                                currentChannel = &channelParams.at(static_cast<int>(STEREO_1));
                            }
                            else if( channel == 2)
                            {
                                currentChannel = &channelParams.at(static_cast<int>(STEREO_2));
                            }
                        }

                        if( !currentChannel )
                        {
                            logger->Message(LOG_ERR, __LINE__, __func__, "Channel not found error\n");
                            continue;
                        }

                        if( !currentChannel->isWrited )
                        {
                            logger->Message(LOG_ERR, __LINE__, __func__, "New session inserting \n");
                            //INSERT NEW SESSION
                            currentChannel->startTime = time;
                            currentChannel->endTime = time;
                            currentChannel->isWrited = true;
                            currentChannel->packetsCount++;
                            currentChannel->channel = channel;
                            currentChannel->pkd = pkd;

                            res = sqlite3_bind_int64(stmt, 1 ,time);
                            res = sqlite3_bind_int64(stmt, 2 , currentChannel->endTime);
                            res = sqlite3_bind_int64(stmt, 3 , channel);

                            rc = sqlite3_step(stmt);

                            sqlite3_clear_bindings(stmt);
                            sqlite3_reset(stmt);

                            int id = sqlite3_last_insert_rowid(db);
                            currentChannel->id = id;

                            res = sqlite3_bind_int64(stmtPKD, 1 ,id);
                            res = sqlite3_bind_int64(stmtPKD, 2 , currentChannel->pkd);
                            res = sqlite3_bind_int64(stmtPKD, 3 , currentChannel->endTime);


                            printf("The last Id of the inserted row is %d\n", id);

                            rc = sqlite3_step(stmtPKD);

                            sqlite3_clear_bindings(stmtPKD);
                            sqlite3_reset(stmtPKD);
                        }
                        else
                        {
                            if ( time > currentDate || time < lowPossibleYear)
                            {
                                continue;
                            }

                            if( currentChannel->startTime <= time && currentChannel->endTime + 1 == time || currentChannel->startTime <= time && currentChannel->endTime  == time)
                            {
                                //UPDATE PACKET END TIME
                                currentChannel->packetsCount++;
                                currentChannel->endTime = time;

                                sqlite3_bind_int64(stmtUpdate, 1 , time);
                                sqlite3_bind_int64(stmtUpdate, 2 , currentChannel->startTime);
                                sqlite3_bind_int64(stmtUpdate, 3 , currentChannel->channel);

                                rc = sqlite3_step(stmtUpdate);

                                sqlite3_clear_bindings(stmtUpdate);
                                sqlite3_reset(stmtUpdate);

                                res = sqlite3_bind_int64(stmtPKD, 1 ,currentChannel->id);
                                res = sqlite3_bind_int64(stmtPKD, 2 , pkd);
                                res = sqlite3_bind_int64(stmtPKD, 3 , currentChannel->endTime);

                                rc = sqlite3_step(stmtPKD);

                                sqlite3_clear_bindings(stmtPKD);
                                sqlite3_reset(stmtPKD);
                            }
                            else if( currentChannel->startTime <= time && currentChannel->endTime == time )
                            {
                                currentChannel->packetsCount++;
                           //     logger->Message(LOG_ERR, __LINE__, __func__, "INSER PKD WITH SESSION ID\n");
                            }
                            else if( time < currentChannel->endTime && time > currentChannel->startTime) //Возможно кольцевая запись, надо подумать
                            {
                                logger->Message(LOG_ERR, __LINE__, __func__, "Error packet\n");
                                //IT IS ERROR
                            }
                            else
                            {
                                logger->Message(LOG_ERR, __LINE__, __func__, "New session inserting \n");
                                //INSERT NEW SESSION
                                currentChannel->startTime = time;
                                currentChannel->endTime = time;
                                currentChannel->isWrited = true;
                                currentChannel->packetsCount++;
                                currentChannel->channel = channel;
                                currentChannel->pkd = pkd;

                                res = sqlite3_bind_int64(stmt, 1 ,time);
                                res = sqlite3_bind_int64(stmt, 2 , currentChannel->endTime);
                                res = sqlite3_bind_int64(stmt, 3 , channel);

                                rc = sqlite3_step(stmt);

                                int id = sqlite3_last_insert_rowid(db);
                                currentChannel->id = id;

                                sqlite3_clear_bindings(stmt);
                                sqlite3_reset(stmt);

                                printf("The last Id of the inserted row is %d\n", id);

                                res = sqlite3_bind_int64(stmtPKD, 1 ,id);
                                res = sqlite3_bind_int64(stmtPKD, 2 , currentChannel->pkd);
                                res = sqlite3_bind_int64(stmtPKD, 3 , currentChannel->endTime);

                                rc = sqlite3_step(stmtPKD);

                                sqlite3_clear_bindings(stmtPKD);
                                sqlite3_reset(stmtPKD);
                            }
                        }
                    }
                    if( dataU[x] == 0x4a )
                    {
                        time = 0;
                        time <<= 8;
                        time |= dataU[x + 5];
                        time <<= 8;
                        time |= dataU[x + 4];
                        time <<= 8;
                        time |= dataU[x + 7];
                        time <<= 8;
                        time |= dataU[x + 6];

                        if( lastTime4a == 0 )
                        {
                            lastTime4a = time;
                            packetByOrderMono.push_back(time);
                         }
                        else if ( time > currentDate || time < lowPossibleYear)
                        {
                            //logger->Message(LOG_ERR, __LINE__, __func__, "!!!!!!!!!!!!!!!!! BAD VALUE PACK TIME %li \n",  time);
                        }
                        else if( lastTime4a == time || lastTime4a + 1 == time )
                        {
                            packetByOrderMono.push_back(time);
                            lastTime4a = time;
                        }
                        else
                        {
                            droppedPacketsMono.push_back(time);

                            logger->Message(LOG_ERR, __LINE__, __func__, "0x4a TIME = droppend packets %i \n",  droppedPackets.size());
                            logger->Message(LOG_ERR, __LINE__, __func__, " LAST TIME = %s NEW TIME %s \n",
                                            QDateTime::fromTime_t(lastTime).toString("dd:MM:yyyy hh:mm:ss").toStdString().c_str(),
                                            QDateTime::fromTime_t(time).toString("dd:MM:yyyy hh:mm:ss").toStdString().c_str());
                            logger->Message(LOG_ERR, __LINE__, __func__, "!!!!!!!!!!!!!!!!! LAST TIME = %li PACK TIME %li \n",  lastTime4a, time);
                            lastTime4a = time;
                        }
                        countPacksMono++;
                        //logger->Message(LOG_ERR, __LINE__, __func__, "0x4a TIME = %li readed = %i \n", time, ret);
                    }
                }
            }

            sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &zErrMsg);
            logger->Message(LOG_ERR, __LINE__, __func__, "0x5a TIME = droppend packets : %i | packets by row: %i | all packets : %i \n",  droppedPackets.size(), packetByOrder.size(), countPacks);

            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
            std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "[ms]" << std::endl;
            std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
            std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() << "[ns]" << std::endl;
            return;


            inputFile.close();
        }
        usleep(10000);
    }
}

bool AudioDataDownloader::startFileThread()
{
    if( threadWork )
        return false;
    threadWork = true;
    channelThread = std::thread(&AudioDataDownloader::threadFileFunction, this);
    return true;
}

bool AudioDataDownloader::startFileDownload()
{
    try
    {
        if( !startFileThread() )
        {
            return false;
        }

        //adapterStatusControl->startAudioHeaderDownload(audio);
    }

    catch(std::system_error &except)
    {
        return false;
    }
}
