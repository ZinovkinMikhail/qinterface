#ifndef LOCKWINDOW_H
#define LOCKWINDOW_H

#include <QObject>
#include <QQuickItem>
#include <QTimer>
#include <memory>
#include <future>

#include <iemptyplacewindow.h>
#include <dataprovider/recordersettingscontol.h>
#include <dataprovider/recorderstatuscontrol.h>
#include "ilockwindow.h"

#include "iqmlengine.h"


#define LOCK_WINDOW_QML_PATH "qrc:/LockWindow.qml"
#define DEFAULT_RESET_PASS_CODE_P1 "731954"
#define DEFAULT_RESET_PASS_CODE_P2 "592683"

class LockWindow : public QObject, public ILockWindow
{
    Q_OBJECT
public:
    explicit LockWindow(QObject *parent = nullptr);

    void initWindow(std::shared_ptr<data_provider::DataProvider> dataProvider,
                    std::shared_ptr<IUserManipulators> userManipulators,
                    std::shared_ptr<IQmlEngine> engine,
                    std::shared_ptr<rosslog::Log> log,
                    std::shared_ptr<IStateWidget> stateWidget);
    ~LockWindow();
    void show();    
    bool isVisible();

private:
    void hide();
    void initAdapterSettingsControl();
    void initRecorderStatusControl();
    void getInfoFromRecorder();
    void setRecievedData();
    void updateData();
    void startEraseInternalMemory();

    QQuickItem *currentItem;
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<IStateWidget> stateWidget;
    SoftRecorderSettingsStruct_t recordSettings;
    std::shared_ptr<RecorderSettingsContol> recordSettingsControl;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusControl {nullptr};
    std::future<void> updateWorker;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::string devicePass;
    QTimer *updateTimer;
    QTimer eraseUpdateTimer;
    bool isResetPass{false};
    bool isEraseStarted{false};
    std::mutex eraseStatusUpdateMutex;
    SoftRecorderStatusStruct_s recordStatus;
    int eraseStartCounter{0};

signals:
    void signalRecivedAdapterData();
    void signalUpdateDataError();
    void signalEraseFinished();
    void signalRecievedRecorderStatus();

public slots:
    void slotRequestAccess(QVariant pass);
private slots:
    void slotRecievedAdapterData();
    void slotUpdateDataError();
    void slotUpdateTimerTimeout();
    void slotEraseFinished();
    void slotEraseTimerTimeout();
    void slotRecievedRecorderStatus();

};

#endif // LOCKWINDOW_H
