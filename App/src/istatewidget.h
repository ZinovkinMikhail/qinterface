#ifndef ISTATEWIDGET_H
#define ISTATEWIDGET_H

#include <QPixmap>

#include <string>


typedef enum
{
    INDICATOR,
    PROCESS
}EStateType;

class IStateWidget
{
public:
    virtual void addState(std::string name, EStateType stateType,
                          QPixmap pixmap, bool prioHigh) = 0;
    virtual void showState(std::string name, std::string value) = 0;
    virtual void hideState(std::string name) = 0;
    virtual void removeState(std::string name) = 0;
    virtual std::vector<std::string> countStates() = 0;


    virtual ~IStateWidget() {}
};


#endif // ISTATEWIDGET_H
