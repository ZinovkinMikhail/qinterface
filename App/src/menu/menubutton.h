#ifndef MENUBUTTON_H
#define MENUBUTTON_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QPixmap>

#include "imenuitemwidget.h"
#include "imenuitemeventshandler.h"

#include "iqmlengine.h"

#define BUTTON_COMPONENT_PATH "qrc:/MenuButton.qml"


class MenuButton : public QObject, public IMenuItemWidget, public std::enable_shared_from_this<MenuButton>
{
    Q_OBJECT
public:
    explicit MenuButton(std::shared_ptr<IQmlEngine> qmlEngine, std::string name,
                        std::shared_ptr<IMenuItemEventsHandler> eventsHandler,
                        std::string presentationName,
                        QPixmap pixmap,
                        QObject *parent = nullptr);
    void init(bool isFolderButton);

    void openItem() override;
    void closeItem() override;
    std::string getItemName() override;
    void moveItem(int x, int y) override;
    QQuickItem *getObjectItem() override;
    ItemType getItemType() override;

private:
    std::string name;
    std::shared_ptr<IMenuItemEventsHandler> eventsHandler;
    std::string presentationName;
    std::shared_ptr<IQmlEngine> engine;
    QPixmap pixmap;
    QQuickItem *currentItem;
    ItemType itemType;

signals:

public slots:
    void slotButtonClicked();
};


#endif // MENUBUTTON_H
