#ifndef MENUFOLDER_H
#define MENUFOLDER_H

#include <QObject>
#include <QQuickItem>

#include "iqmlengine.h"
#include "imenuitemwidget.h"
#include "imenuitemeventshandler.h"

#define FOLDER_COMPONENT_PATH "qrc:/MenuFolder.qml"


class MenuFolder : public QObject, public IMenuItemWidget, public std::enable_shared_from_this<MenuFolder>
{
    Q_OBJECT
public:
    explicit MenuFolder(std::shared_ptr<IQmlEngine> qmlEngine, std::string name,
                        std::shared_ptr<IMenuItemEventsHandler> eventsHandler,
                        std::string presentationName, QPixmap pixmap, QObject *parent = nullptr);

    void init();
    QQuickItem *getFolderRowItem();
    void addItemToFolder(std::shared_ptr<IMenuItemWidget> button);
    void openItem();
    void closeItem();
    std::string getItemName();
    void moveItem(int x, int y);
    QQuickItem *getObjectItem();
    ItemType getItemType();

private:
    std::list<std::shared_ptr<IMenuItemWidget>> folderItems;
    std::string name;
    std::shared_ptr<IMenuItemEventsHandler> eventsHandler;
    std::string presentationName;
    QPixmap pixmap;
    std::shared_ptr<IQmlEngine> engine;
    QQuickItem *currentItem;
    QQuickItem *rowItem;
    ItemType itemType;

signals:
    void pushButton(QVariantMap map);

public slots:
};

#endif // MENUFOLDER_H
