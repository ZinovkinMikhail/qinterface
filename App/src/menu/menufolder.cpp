#include <iostream>
#include <assert.h>

#include "menufolder.h"

MenuFolder::MenuFolder(std::shared_ptr<IQmlEngine> qmlEngine, std::string name,
                       std::shared_ptr<IMenuItemEventsHandler> eventsHandler,
                       std::string presentationName, QPixmap pixmap, QObject *parent) :
    QObject(parent),
    name(name),
    eventsHandler(eventsHandler),
    presentationName(presentationName),
    pixmap(pixmap),
    engine(qmlEngine),
    currentItem(nullptr),
    rowItem(nullptr)
{
    assert(engine);
}

void MenuFolder::init()
{
    currentItem = qobject_cast<QQuickItem*>(engine->createComponent(FOLDER_COMPONENT_PATH));
    assert(currentItem);
    itemType = FOLDER;

    currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
}

QQuickItem *MenuFolder::getFolderRowItem()
{
    QQuickItem* rowItem = currentItem->findChild<QQuickItem*>("FolderRow");
    assert(rowItem);
    return rowItem;
}


void MenuFolder::addItemToFolder(std::shared_ptr<IMenuItemWidget> button)
{
    folderItems.push_back(button);
}

void MenuFolder::openItem()
{
}

void MenuFolder::closeItem()
{
}

std::string MenuFolder::getItemName()
{
    return name;
}

void MenuFolder::moveItem(int x, int y)
{
    Q_UNUSED(x);
    Q_UNUSED(y);
}

QQuickItem *MenuFolder::getObjectItem()
{
    return currentItem;
}

ItemType MenuFolder::getItemType()
{
    return itemType;
}


