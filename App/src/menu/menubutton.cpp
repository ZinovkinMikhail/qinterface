#include <assert.h>
#include <QQmlComponent>
#include <QImage>

#include "menubutton.h"

#include "imagerender.h"

MenuButton::MenuButton(std::shared_ptr<IQmlEngine> qmlEngine, std::string name,
                       std::shared_ptr<IMenuItemEventsHandler> eventsHandler,
                       std::string presentationName, QPixmap pixmap, QObject *parent) :
    QObject(parent),
    name(name),
    eventsHandler(eventsHandler),
    presentationName(presentationName),
    engine(qmlEngine),
    pixmap(pixmap)
{
    assert(engine);
}

void MenuButton::init(bool isFolderButton)
{
    currentItem = qobject_cast<QQuickItem*>(engine->createComponent(BUTTON_COMPONENT_PATH));
    assert(currentItem);
    currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

    if( isFolderButton )
        itemType = FOLDER;
    else
        itemType = BUTTON;

    QMetaObject::invokeMethod(currentItem, "setButtonName",
                              Q_ARG(QVariant, QString::fromStdString(presentationName)));

    QMetaObject::invokeMethod(currentItem, "setButtonFolderType",
                              Q_ARG(QVariant, QVariant::fromValue(isFolderButton)));

    ImageRender *render =  currentItem->findChild<ImageRender*>("ButtonIcon");
    render->addImage(pixmap.toImage());

    connect(currentItem, SIGNAL(buttonClicked()), this, SLOT(slotButtonClicked()));
}

void MenuButton::openItem()
{
}

void MenuButton::closeItem()
{
}

std::string MenuButton::getItemName()
{
    return name;
}

void MenuButton::moveItem(int x, int y)
{
    Q_UNUSED(x);
    Q_UNUSED(y);
}

QQuickItem *MenuButton::getObjectItem()
{
    return currentItem;
}

ItemType MenuButton::getItemType()
{
    return itemType;
}

void MenuButton::slotButtonClicked()
{
    eventsHandler->itemPress(shared_from_this());
}

