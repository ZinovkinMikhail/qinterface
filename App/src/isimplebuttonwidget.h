#ifndef ISIMPLEBUTTONWIDGET_H
#define ISIMPLEBUTTONWIDGET_H

#include <memory>

#include <QObject>
#include <QVariantMap>

class ISimpleButtonWidget
{
public:
    virtual void openButtonWidget() = 0;
    virtual void closeButtonWidget() = 0;
    virtual ~ISimpleButtonWidget(){}

signals:
    virtual void signalButtonPressed(std::shared_ptr<ISimpleButtonWidget> button) = 0;
    virtual void signalCloseButtonPressed(std::shared_ptr<ISimpleButtonWidget> button) = 0;

};
//Q_DECLARE_INTERFACE(ISimpleButtonWidget, "ISimpleButtonWidget")

#endif // ISIMPLEBUTTONWIDGET_H
