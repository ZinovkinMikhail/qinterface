#ifndef ICHANNELCONTROL_H
#define ICHANNELCONTROL_H

#include "channelmodel.h"

class IChannelControl
{
public:
    virtual void setNewSettingsParams(int channelNum, ChannelType channelType,
                                      ChannelSettingsParams settingParams) = 0;
    virtual void unionParamsChanged(int channelNum, ChannelType channelType,
                                    ChannelSettingsParams settingsParams) = 0;
    virtual void pairParamChanged(int channelNum,
                                    ChannelSettingsParams settingsParams) = 0;
    virtual void startRecord(int channelNum, ChannelType channelType) = 0;
    virtual void stopRecord(int channelNum, ChannelType channelType) = 0;
    virtual void startMonitor(int channelNum, ChannelType channelType) = 0;
    virtual void stopMonitor(int channelNum, ChannelType channelType) = 0;
    virtual bool getErasureStatus() = 0;
    virtual ~IChannelControl(){}
};

#endif // ICHANNELCONTROL_H
