#ifndef UPDATEPROGRAMWINDOW_H
#define UPDATEPROGRAMWINDOW_H

#include <memory>
#include <string>
#include <utility>
#include <iterator>

#include <QObject>
#include <QQuickItem>

#include <rosslog/log.h>
#include <iqmlengine.h>

#include <iusermanipulators.h>

#define UPDATEWINDOW_QML_PARH "qrc:/UpdateProgramWindow.qml"



class UpdateProgramWindow: public QObject, public std::enable_shared_from_this<UpdateProgramWindow>
{
    Q_OBJECT
public:
    explicit UpdateProgramWindow(std::shared_ptr<IUserManipulators> userManipulators,
    std::shared_ptr<IQmlEngine> qmlEngine,
    std::shared_ptr<rosslog::Log> log,
    QObject *parent = nullptr);

    void init();
    void show();
    void hide();
    void setFirmwareInfo(QString _adapterVer, QString _recorderVer);
    void setProgress(int _value);


private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<rosslog::Log> logger;
    QQuickItem *currentItem;



public slots:
    void slotCancelButtonClicked();
    void slotUpdateAdapterClicked();
    void slotUpdateRecorderClicked();
};


#endif //UPDATEPROGRAMWINDOW_H
