#include <assert.h>

#include "channelparamspage.h"

ChannelParamsPage::ChannelParamsPage(std::shared_ptr<IChannel> channelInterface,
                                     std::shared_ptr<IUserManipulators> userManipulators,
                                     std::shared_ptr<IQmlEngine> qmlEngine,
                                     QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    userManipulators(userManipulators),
    channelInterface(channelInterface)
{
    assert(qmlEngine);
    assert(parent);
    assert(userManipulators);

    std::vector<std::string> vecLinear
    {
        "125 мВ.",
        "250 мВ.",
        "500 мВ.",
        "1000 мВ."
    };

    sensivityMap[LINEAR] = vecLinear;

    std::vector<std::string> vecElectric
    {
        "5 мВ.",
        "10 мВ.",
        "20 мВ.",
        "40 мВ."
    };

    sensivityMap[ELECTRIC] = vecElectric;

    std::vector<std::string> vecElectricDyn
    {
        "50 мкВ.",
        "100 мкВ.",
        "200 мкВ.",
        "400 мкВ."
    };

    sensivityMap[ELECTRIC_DINAMIC] = vecElectricDyn;

    biasMap[BIAS_OFF] = "Выкл";
    biasMap[BIAS_ON] = "Вкл";

    //Vector for combo drum fill only one time same with filtr
    biasVector.reserve(4);
    biasVector.push_back("Выкл");
    biasVector.push_back("Вкл");

    filtrMap[FILTR_OFF] = "Выкл";
    filtrMap[FILTR_ON] = "Вкл";

    biasVector.reserve(4);
    filtrVector.push_back("Выкл");
    filtrVector.push_back("Вкл");

    settingsParams.biasType = BIAS_OFF;
    settingsParams.filtrType = FILTR_OFF;
    settingsParams.sensivityType = LINEAR;
    settingsParams.sensVal = SENSIVITY_5;
}

void ChannelParamsPage::iniChannelParamsPage()
{
    QObject *currentObject;
    currentObject = qmlEngine->createComponent(CHANNEL_PARAMS_PAGE_QML, this);

    assert(currentObject);

    currentItem = qobject_cast<QQuickItem*>(currentObject);
    currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

    //Connect signals with qml buttons for open Combo drum on press
    connect(currentItem, SIGNAL(showSensComboDrum()),
            this, SLOT(slotShowSensComboDrum()));
    connect(currentItem, SIGNAL(showFiltrComboDrum()),
            this, SLOT(slotShowFiltrComboDrum()));
    connect(currentItem, SIGNAL(showBiasComboDrum()),
            this, SLOT(slotShowBiasComboDrum()));
    connect(currentItem, SIGNAL(applySettingsParams()),
            this, SLOT(slotApplySettingsParams()));

}

void ChannelParamsPage::setChannelSettingsParams(ChannelSettingsParams params)
{
    settingsParams = params;
    setSensivityTypeQml(QString::fromStdString(
                            sensivityMap.at(params.sensivityType).at(params.sensVal)), false);


    setFiltrTypeQml(QString::fromStdString(filtrMap.at(params.filtrType)), false);
    setBiasTypeQml(QString::fromStdString(biasMap.at(params.biasType)), false);
}

void ChannelParamsPage::paramsDataSetted(std::string paramName)
{   
    QMetaObject::invokeMethod(currentItem, "paramsDataSetted",
                              Q_ARG(QVariant, QString::fromStdString(paramName)));
}

void ChannelParamsPage::changeUnionParams(ChannelSettingsParams params)
{
//    settingsParams.sensVal = params.sensVal;
    settingsParams.filtrType = params.filtrType;
    settingsParams.biasType = params.biasType;
//    setSensivityTypeQml(QString::fromStdString(sensivityMap.at(params.sensivityType).at(params.sensVal)), true);
    setFiltrTypeQml(QString::fromStdString(filtrMap.at(params.filtrType)), true);
    setBiasTypeQml(QString::fromStdString(biasMap.at(params.biasType)), true);
}

void ChannelParamsPage::changePairParam(ChannelSettingsParams params)
{
    settingsParams.sensVal = params.sensVal;
    setSensivityTypeQml(QString::fromStdString(sensivityMap.at(settingsParams.sensivityType).at(params.sensVal)), true);
}

void ChannelParamsPage::setSensivityTypeQml(QString type, bool isComboDrum)
{
    QMetaObject::invokeMethod(currentItem, "setSensivityType",
                              Q_ARG(QVariant, type),
                              Q_ARG(QVariant, isComboDrum));
}

void ChannelParamsPage::setFiltrTypeQml(QString type, bool isComboDrum)
{
    QMetaObject::invokeMethod(currentItem, "setFiltrType",
                                  Q_ARG(QVariant, type),
                                  Q_ARG(QVariant, isComboDrum));
}

void ChannelParamsPage::setBiasTypeQml(QString type, bool isComboDrum)
{
    QMetaObject::invokeMethod(currentItem, "setBiasType",
                              Q_ARG(QVariant, type),
                              Q_ARG(QVariant, isComboDrum));
}

//Signal from ChannelParamsPage.qml
void ChannelParamsPage::slotShowSensComboDrum()
{
    std::pair<bool, int> res = userManipulators->openComboDrum(settingsParams.sensVal,sensivityMap.at(settingsParams.sensivityType));
    if( res.first )
    {
        if( res.second < SENSIVITY_VAL_SIZE )
        {
            settingsParams.sensVal = static_cast<SensivityVal>(res.second);
            setSensivityTypeQml(QString::fromStdString(sensivityMap.at(settingsParams.sensivityType).at(res.second)),
                                true);
            this->channelInterface->pairParamChanged(settingsParams);
        }
    }
}

//Signal from ChannelParamsPage.qml
void ChannelParamsPage::slotShowFiltrComboDrum()
{
    std::pair<bool, int> res = userManipulators->openComboDrum(settingsParams.filtrType, filtrVector);
    if( res.first )
    {
        if( res.second < FILTR_VAL_SIZE )
        {
            settingsParams.filtrType = static_cast<FiltrType>(res.second);
            setFiltrTypeQml(QString::fromStdString(filtrMap.at(settingsParams.filtrType)),
                            true);
            this->channelInterface->unionParamsChanged(settingsParams);
        }
    }
}

//Signal from ChannelParamsPage.qml
void ChannelParamsPage::slotShowBiasComboDrum()
{
    std::pair<bool, int> res = userManipulators->openComboDrum(settingsParams.biasType, biasVector);
    if( res.first )
    {
        if( res.second < BIAS_VAL_SIZE )
        {
            settingsParams.biasType = static_cast<BiasType>(res.second);
            setBiasTypeQml(QString::fromStdString(biasMap.at(settingsParams.biasType)),
                           true);
            this->channelInterface->unionParamsChanged(settingsParams);
        }
    }
}

void ChannelParamsPage::slotApplySettingsParams()
{
    this->channelInterface->setNewSettingParams(settingsParams);
}



