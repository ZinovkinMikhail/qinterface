#ifndef ICHANNEL_H
#define ICHANNEL_H

#include "channelmodel.h"

class IChannel
{
public:
    virtual void setNewSettingParams(ChannelSettingsParams settingParams) = 0;
    virtual void unionParamsChanged(ChannelSettingsParams settingsParams) = 0;
    virtual void pairParamChanged(ChannelSettingsParams settingsParams) = 0;
    virtual void startRecord() = 0;
    virtual void stopRecord() = 0;
    virtual void startMonitor() = 0;
    virtual void stopMonitor() = 0;
    virtual bool getErasureStatus() = 0;
    virtual ~IChannel() {}
};


#endif // ICHANNEL_H
