#include <QQmlComponent>
#include <QQmlContext>

#include <assert.h>
#include <functional>
#include <iostream>
//#include <condition_variable>
#include <sys/time.h>

#include "channelcontrolwidget.h"

using namespace rosslog;
using namespace recorder_status_ctrl;
using namespace std::placeholders;

ChannelControlWidget::ChannelControlWidget(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    recorderStatusCtrl(nullptr),
    recorderSettingsCtrl(nullptr),
    memoryProcessWidget(nullptr),
    eraseProcessWidget(nullptr),
    stateWidget(nullptr),
    activeProcessWidget(ACTIVE_MEMORY),
    threadWork(true),
    conditionWait(false),
    wasErasing(false),
    isProcessShown(true)
{
    assert(parent);

    memset(&recordSettings, 0, sizeof(recordSettings));
    memset(&recordStatus, 0, sizeof(recordStatus));

    previousCfgMode = 0;
    isChannelInited = false;
    isAdapterConnected = false;
    isFirstLoad = true;
    connectionLostCounter = 0;

    qRegisterMetaType<ChannelSettingsParams>("ChannelSettingsParams");
    qRegisterMetaType<ChannelType>("ChannelType");

}

std::list<std::shared_ptr<IProcessWidget> > ChannelControlWidget::getProcessWidgets()
{
    return processWidgets;
}

void ChannelControlWidget::prepareQmlData()
{
    if( windowComponent == nullptr )
    {
        windowComponent = qmlEngine->createComponent(COMPONENT_PATH, parent());
        currentItem = qobject_cast<QQuickItem*>(windowComponent);
    }
}

std::shared_ptr<IUserManipulators> ChannelControlWidget::initWindow(EWindSizeType winSizeType,
                                                                    std::shared_ptr<DataProvider> dataProvider,
                                                                    std::shared_ptr<IStateWidget> stateWidget,
                                                                    std::shared_ptr<IUserManipulators> userManipulators,
                                                                    std::shared_ptr<IQmlEngine> engine,
                                                                    std::shared_ptr<Log> log)
{
    Q_UNUSED(stateWidget);
    Q_UNUSED(winSizeType);

    assert(dataProvider);
    assert(userManipulators);
    assert(engine);
    assert(log);
    assert(stateWidget);

    this->dataProvider = dataProvider;
    this->userManipulators = userManipulators;
    this->logger = log;
    this->qmlEngine = engine;
    this->stateWidget = stateWidget;
    startLoadingAnim = true;

    initMemoryProcessWidget();
    initEraseProcessWidget();
    initStateWidgets();
    prepareQmlData();

    initChannels();
    initRecorderSettingsControl();
    initRecorderStatusControl();

    dataProvider->addDataWaiter(AXIS_INTERFACE_EVENT_READ_STATUS , shared_from_this());
    dataProvider->addDataWaiter(AXIS_INTERFACE_EVENT_READ_STATUS_REC , shared_from_this());

    channelThread = std::thread(&ChannelControlWidget::threadFunction, this);

    //Update local forms by updater thread signal
    connect(this, SIGNAL(signalUpdateLocalForms()),
            this, SLOT(slotUpdateForms()));
    connect(this, SIGNAL(signalChannelsParamsApplyed()),
            this, SLOT(slotChannelParamsApplyed()));
    connect(this, SIGNAL(signalConnectionLost()),
            this, SLOT(slotConnectionLost()));
    return nullptr;
}

QObject *ChannelControlWidget::getComponentObject()
{
    return windowComponent;
}

std::string ChannelControlWidget::getWindowName()
{
    return CHANNEL_WINDOW_IDENTIFICATOR;
}

std::string ChannelControlWidget::getWindowAbsolutePath()
{
    return "";
}

std::string ChannelControlWidget::getWindowFolderName()
{
    return "";
}

std::string ChannelControlWidget::getIconPresentationName()
{
    return "";
}

std::string ChannelControlWidget::getWindowFolderPresentationName()
{
    return "";
}

QPixmap ChannelControlWidget::getIcon()
{
#warning Set PIXMAP
    return QPixmap();
}

std::pair<bool, QPixmap> ChannelControlWidget::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/icons/i19.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > ChannelControlWidget::getProcessWidget()
{
    return std::make_pair(false, nullptr);
}

bool ChannelControlWidget::showWindow()
{
    conditionWait = false;
//    cv.notify_one();

    if( activeProcessWidget == ACTIVE_MEMORY )
    {
        eraseProcessWidget->show();
        memoryProcessWidget->show();
    }
    memoryProcessWidget->show();
    return true;
}

bool ChannelControlWidget::hideWindow()
{
    return true;
}

EWindSizeType ChannelControlWidget::getWinSizeType()
{
    return NORMALSIZE;
}

bool ChannelControlWidget::prepareToDelete()
{
    for( auto chan : channels )
    {
        chan.second->prepareToDelete();
    }
    channels.clear();
    dataProvider->removeDataWaiter(AXIS_INTERFACE_EVENT_READ_STATUS , shared_from_this());
    dataProvider->removeDataWaiter(AXIS_INTERFACE_EVENT_READ_STATUS_REC , shared_from_this()); 
    return true;
}

void ChannelControlWidget::setNewSettingsParams(int channelNum, ChannelType channelType,
                                                ChannelSettingsParams settingParams)
{
    SoftRecorderSettingsStruct_t setTmp;
    setTmp = this->recordSettings;

    switch(channelNum)
    {
        case CHANNEL1_NUM:
        {
            std::bitset<16> bias(setTmp.ChanelBias);
            std::bitset<16> filtr(setTmp.ChanelAGCLPFMode);

            bias.set(CHANNEL1_ENABLE_POS, static_cast<bool>(settingParams.biasType));
            filtr.set(CHANNEL_1_LPF_ENABLE, static_cast<bool>(settingParams.filtrType));


            if( channelType == UNION_CHANNEL )
            {
                bias.set(CHANNEL2_ENABLE_POS, static_cast<bool>(settingParams.biasType));
                filtr.set(CHANNEL_2_LPF_ENABLE, static_cast<bool>(settingParams.filtrType));
            }

#warning need to setBit not full value
            setTmp.Chanel1Sens = static_cast<uint16_t>(settingParams.sensVal);
            setTmp.Chanel2Sens = static_cast<uint16_t>(settingParams.sensVal);
            setTmp.ChanelAGCLPFMode = filtr.to_ulong();
            setTmp.ChanelBias = bias.to_ulong();

            break;
        }
        case CHANNEL2_NUM:
        {
            std::bitset<16> bias(setTmp.ChanelBias);
            std::bitset<16> filtr(setTmp.ChanelAGCLPFMode);

            bias.set(CHANNEL2_ENABLE_POS, static_cast<bool>(settingParams.biasType));
            filtr.set(CHANNEL_2_LPF_ENABLE, static_cast<bool>(settingParams.filtrType));

            if( channelType == UNION_CHANNEL )
            {
                bias.set(CHANNEL1_ENABLE_POS, static_cast<bool>(settingParams.biasType));
                filtr.set(CHANNEL_1_LPF_ENABLE, static_cast<bool>(settingParams.filtrType));
            }

            setTmp.Chanel1Sens = static_cast<uint16_t>(settingParams.sensVal);
            setTmp.Chanel2Sens = static_cast<uint16_t>(settingParams.sensVal);
            setTmp.ChanelAGCLPFMode = filtr.to_ulong();
            setTmp.ChanelBias = bias.to_ulong();
            break;
        }
        case CHANNEL3_NUM:
        {
            std::bitset<16> bias(setTmp.ChanelBias);
            std::bitset<16> filtr(setTmp.ChanelAGCLPFMode);

            bias.set(CHANNEL3_ENABLE_POS, static_cast<bool>(settingParams.biasType));
            filtr.set(CHANNEL_3_LPF_ENABLE, static_cast<bool>(settingParams.filtrType));

            if( channelType == UNION_CHANNEL )
            {
                bias.set(CHANNEL4_ENABLE_POS, static_cast<bool>(settingParams.biasType));
                filtr.set(CHANNEL_4_LPF_ENABLE, static_cast<bool>(settingParams.filtrType));
            }

            setTmp.Chanel3Sens = static_cast<uint16_t>(settingParams.sensVal);
            setTmp.Chanel4Sens = static_cast<uint16_t>(settingParams.sensVal);
            setTmp.ChanelAGCLPFMode = filtr.to_ulong();
            setTmp.ChanelBias = bias.to_ulong();
            break;
        }
        case CHANNEL4_NUM:
        {
            std::bitset<16> bias(setTmp.ChanelBias);
            std::bitset<16> filtr(setTmp.ChanelAGCLPFMode);

            bias.set(CHANNEL4_ENABLE_POS, static_cast<bool>(settingParams.biasType));
            filtr.set(CHANNEL_4_LPF_ENABLE, static_cast<bool>(settingParams.filtrType));

            if( channelType == UNION_CHANNEL )
            {
                bias.set(CHANNEL3_ENABLE_POS, static_cast<bool>(settingParams.biasType));
                filtr.set(CHANNEL_3_LPF_ENABLE, static_cast<bool>(settingParams.filtrType));
            }

            setTmp.Chanel3Sens = static_cast<uint16_t>(settingParams.sensVal);
            setTmp.Chanel4Sens = static_cast<uint16_t>(settingParams.sensVal);
            setTmp.ChanelAGCLPFMode = filtr.to_ulong();
            setTmp.ChanelBias = bias.to_ulong();
            break;
        }
        default:
            break;
    }

    if( recorderSettingsCtrl != nullptr )
    {
        userManipulators->showLoadingWidget();

        updateWorker = std::async(std::launch::async,
                                  std::bind(&ChannelControlWidget::updateDataOnAdapter,
                                            this,  std::placeholders::_1), setTmp);
        currentUpdateChannelNum = channelNum;
        updateWorker.wait_for(std::chrono::seconds(0));
    }
}

void ChannelControlWidget::unionParamsChanged(int channelNum, ChannelType channelType,
                                              ChannelSettingsParams settingsParams)
{
    if(channelType == UNION_CHANNEL )
    {
        if( channelNum == CHANNEL1_NUM )
            channels.at(channelNum + 1)->changeUnionParams(settingsParams);
        else if( channelNum == CHANNEL2_NUM )
            channels.at(channelNum - 1)->changeUnionParams(settingsParams);
        else if( channelNum == CHANNEL3_NUM )
            channels.at(channelNum + 1)->changeUnionParams(settingsParams);
        else if( channelNum == CHANNEL4_NUM )
            channels.at(channelNum - 1)->changeUnionParams(settingsParams);
    }
}

void ChannelControlWidget::pairParamChanged(int channelNum,
                                              ChannelSettingsParams settingsParams)
{
    if( channelNum == CHANNEL1_NUM )
        channels.at(channelNum + 1)->changePairParam(settingsParams);
    else if( channelNum == CHANNEL2_NUM )
        channels.at(channelNum - 1)->changePairParam(settingsParams);
    else if( channelNum == CHANNEL3_NUM )
        channels.at(channelNum + 1)->changePairParam(settingsParams);
    else if( channelNum == CHANNEL4_NUM )
        channels.at(channelNum - 1)->changePairParam(settingsParams);
}

void ChannelControlWidget::recieveData(int command, std::vector<uint8_t> data, size_t size)
{
    Q_UNUSED(data);
    Q_UNUSED(size);

    if( command == AXIS_INTERFACE_EVENT_READ_STATUS || command == AXIS_INTERFACE_EVENT_READ_STATUS_REC )
    {
        conditionWait = false;
//        cv.notify_one();
    }
    this->logger->Message(LOG_ERR, __LINE__, __func__, "Recieved command %x", command);
}

void ChannelControlWidget::startRecord(int channelNum, ChannelType channelType)
{
    std::bitset<4> channels = 0;

    if( recordStatus.Chanel1Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
        channels.set(CHANNEL1_NUM - 1, true);

    if( recordStatus.Chanel2Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
        channels.set(CHANNEL2_NUM - 1, true);

    if( recordStatus.Chanel3Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
        channels.set(CHANNEL3_NUM - 1, true);

    if( recordStatus.Chanel4Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
        channels.set(CHANNEL4_NUM - 1, true);

    if( channelType == UNION_CHANNEL )
    {
        if( channelNum == CHANNEL1_NUM || channelNum == CHANNEL2_NUM )
        {
            channels.set(CHANNEL1_NUM - 1, true);
            channels.set(CHANNEL2_NUM - 1, true);
        }
        else
        {
            channels.set(CHANNEL3_NUM - 1, true);
            channels.set(CHANNEL4_NUM - 1, true);
        }
    }
    if( channelType == SINGLE_CHANNEL )
    {
        channels.set(channelNum - 1, true);
    }

    userManipulators->showLoadingWidget();
    updateWorker = std::async( std::launch::async,[this, channels]
    {
        try
        {
            sleep(1);
            recorderStatusCtrl->startRecord(channels);
            userManipulators->hideLoadingWidget();
        }
        catch (std::system_error &except)
        {
            userManipulators->hideLoadingWidget();
            userManipulators->showMessage(std::string("Не удалось отправить команду.\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
        }

        return;
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}

void ChannelControlWidget::stopRecord(int channelNum, ChannelType channelType)
{
    std::bitset<4> channels = 0;

    if( channelType == UNION_CHANNEL )
    {
        if( channelNum == CHANNEL1_NUM || channelNum == CHANNEL2_NUM )
        {
            channels.set(CHANNEL1_NUM - 1, true);
            channels.set(CHANNEL2_NUM - 1, true);
        }
        else
        {
            channels.set(CHANNEL3_NUM - 1, true);
            channels.set(CHANNEL4_NUM - 1, true);
        }
    }
    if( channelType == SINGLE_CHANNEL )
    {
        channels.set(channelNum - 1, true);
    }


    userManipulators->showLoadingWidget();
    updateWorker = std::async( std::launch::async,[this, channels]
    {
        try
        {
            recorderStatusCtrl->stopRecord(channels);
            userManipulators->hideLoadingWidget();
        } catch (std::system_error &except)
        {
            userManipulators->hideLoadingWidget();
            userManipulators->showMessage(std::string("Не удалось отправить команду.\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
        }

        return;
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}

void ChannelControlWidget::startMonitor(int channelNum, ChannelType channelType)
{
    std::bitset<4> channels = 0;
    if( channelType == UNION_CHANNEL )
    {
        if( channelNum == CHANNEL1_NUM || channelNum == CHANNEL2_NUM )
        {
            channels.set(CHANNEL1_NUM - 1, true);
            channels.set(CHANNEL2_NUM - 1, true);
        }
        else
        {
            channels.set(CHANNEL3_NUM - 1, true);
            channels.set(CHANNEL4_NUM - 1, true);
        }
    }
    else
    {
        channels.set(channelNum - 1, true);
    }


    userManipulators->showLoadingWidget();
    updateWorker = std::async( std::launch::async,[this, channels]
    {
        try
        {
            recorderStatusCtrl->startAudioMonitoring(channels);
            userManipulators->hideLoadingWidget();
        } catch (std::system_error &except)
        {
            userManipulators->hideLoadingWidget();
            userManipulators->showMessage(std::string("Не удалось отправить команду.\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
        }

        return;
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}

void ChannelControlWidget::stopMonitor(int channelNum, ChannelType channelType)
{
    std::bitset<4> channels = 0;
    if( channelType == UNION_CHANNEL )
    {
        if( channelNum == CHANNEL1_NUM || channelNum == CHANNEL2_NUM )
        {
            channels.set(CHANNEL1_NUM - 1, true);
            channels.set(CHANNEL2_NUM - 1, true);
        }
        else
        {
            channels.set(CHANNEL3_NUM - 1, true);
            channels.set(CHANNEL4_NUM - 1, true);
        }
    }
    else
    {
        channels.set(channelNum - 1, true);
    }

    userManipulators->showLoadingWidget();
    updateWorker = std::async( std::launch::async,[this, channels]
    {
        try
        {
            std::cerr << "STOP MONITOR NOW BUTT " << std::endl;
            recorderStatusCtrl->stopAudioMonitoring(channels);
            userManipulators->hideLoadingWidget();
        } catch (std::system_error &except)
        {
            userManipulators->hideLoadingWidget();
            userManipulators->showMessage(std::string("Не удалось отправить команду .\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
        }

        return;
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}

void ChannelControlWidget::initMemoryProcessWidget()
{
    if( memoryProcessWidget == nullptr )
    {
        memoryProcessWidget = std::shared_ptr<MemoryProcessWidget>(
                    new MemoryProcessWidget(this->qmlEngine, MEMORY_PROCES_WIDGET_NAME, *logger));

        memoryProcessWidget->init();
        connect(memoryProcessWidget.get(), SIGNAL(signalSwipeSelected(std::string, std::shared_ptr<IProcessWidget>)),
                this, SLOT(slotOnProcessWidgetSwipeSelected(std::string, std::shared_ptr<IProcessWidget>)));
        connect(memoryProcessWidget.get(), SIGNAL(signalSwipeDeselected(std::string,std::shared_ptr<IProcessWidget>)),
                this, SLOT(slotOnProcessWidgetSwipeDeSelected(std::string, std::shared_ptr<IProcessWidget>)));

        processWidgets.push_back(memoryProcessWidget);
    }
}

void ChannelControlWidget::initEraseProcessWidget()
{
    if( eraseProcessWidget == nullptr )
    {
        eraseProcessWidget = std::shared_ptr<EraseProcessWidget>(
                    new EraseProcessWidget(this->qmlEngine, "EraseProcessWidget", *logger));

        eraseProcessWidget->init();
        connect(eraseProcessWidget.get(), SIGNAL(signalSwipeSelected(std::string,std::shared_ptr<IProcessWidget>)),
                this, SLOT(slotOnProcessWidgetSwipeSelected(std::string, std::shared_ptr<IProcessWidget>)));
        connect(eraseProcessWidget.get(), SIGNAL(signalSwipeDeselected(std::string,std::shared_ptr<IProcessWidget>)),
                this, SLOT(slotOnProcessWidgetSwipeDeSelected(std::string, std::shared_ptr<IProcessWidget>)));

        processWidgets.push_back(eraseProcessWidget);
    }
}

void ChannelControlWidget::initStateWidgets()
{
    assert(stateWidget);
    stateWidget->addState("UNSAVED_DATA_STATE", INDICATOR, QPixmap(":/icons/i6.png"), false);
    stateWidgets[UNSAVED_DATA_STATE] = "UNSAVED_DATA_STATE";
    stateWidget->addState("ERASE_DATA_STATE", PROCESS, QPixmap(":/icons/i2.png"), false);
    stateWidgets[ERASE_DATA_STATE] = "ERASE_DATA_STATE";
}

bool ChannelControlWidget::getErasureStatus()
{
    return static_cast<FLASH_STATUS>(recordStatus.FlashStatus) == fsFlashErasing;
}


//ChannelControlWidget - here we will order our channels.

void ChannelControlWidget::setChannelsStatus()
{
    if( !isChannelInited )
        return;

    ChannelStatusParams params1 = {};
    ChannelStatusParams params2 = {};
    ChannelStatusParams params3 = {};
    ChannelStatusParams params4 = {};

//    memset(&params1, 0, sizeof(ChannelStatusParams));
//    memset(&params2, 0, sizeof(ChannelStatusParams));
//    memset(&params3, 0, sizeof(ChannelStatusParams));
//    memset(&params4, 0, sizeof(ChannelStatusParams));

    std::bitset<16> bits(recordSettings.ChanelCfgMode);


    params1.enabled = bits.test(CHANNEL1_ENABLE_POS);
    params1.recordSource = getRecorderSourceType(recordStatus.Chanel1Status);

    params2.enabled = bits.test(CHANNEL2_ENABLE_POS);
    params2.recordSource = getRecorderSourceType(recordStatus.Chanel2Status);

    params3.enabled = bits.test(CHANNEL3_ENABLE_POS);
    params3.recordSource = getRecorderSourceType(recordStatus.Chanel3Status);

    params4.enabled = bits.test(CHANNEL4_ENABLE_POS);
    params4.recordSource = getRecorderSourceType(recordStatus.Chanel4Status);


    if( !bits.test(CHANNEL1_STEREO_POS) )
    {
        channels.at(CHANNEL1_NUM)->changeChannelType(SINGLE_CHANNEL);
        channels.at(CHANNEL2_NUM)->changeChannelType(SINGLE_CHANNEL);
    }

    if( bits.test(CHANNEL1_STEREO_POS))
    {
        params1.type = UNION_CHANNEL;
        params2.type = UNION_CHANNEL;
        channels.at(CHANNEL1_NUM)->changeChannelType(UNION_CHANNEL);
        channels.at(CHANNEL2_NUM)->changeChannelType(UNION_CHANNEL);
        //channels.at(CHANNEL2_NUM)->hideChannel();
    }

    if( !bits.test(CHANNEL3_STEREO_POS) )
    {
        channels.at(CHANNEL3_NUM)->changeChannelType(SINGLE_CHANNEL);
        channels.at(CHANNEL4_NUM)->changeChannelType(SINGLE_CHANNEL);
    }

    if( bits.test(CHANNEL3_STEREO_POS) )
    {
        channels.at(CHANNEL3_NUM)->changeChannelType(UNION_CHANNEL);
        params3.type = UNION_CHANNEL;
        channels.at(CHANNEL4_NUM)->changeChannelType(UNION_CHANNEL);
        params4.type = UNION_CHANNEL;
        //channels.at(CHANNEL4_NUM)->hideChannel();
    }

    previousCfgMode = recordSettings.ChanelCfgMode;
    isFirstLoad = false;

    params1.channelState = getChannelState(recordStatus.Chanel1Status);
    std::cerr << "CHANNEL 1 STATE = " << params1.channelState << std::endl;
    printf("CHANNEL 1 STATUS = %x\n", recordStatus.Chanel1Status);
    params2.channelState = getChannelState(recordStatus.Chanel2Status);
    std::cerr << "CHANNEL 2 STATE = " << params2.channelState << std::endl;
    printf("CHANNEL 2 STATUS = %x\n", recordStatus.Chanel2Status);
    params3.channelState = getChannelState(recordStatus.Chanel3Status);
    std::cerr << "CHANNEL 3 STATE = " << params3.channelState << std::endl;
    printf("CHANNEL 3 STATUS = %x\n", recordStatus.Chanel3Status);
    params4.channelState = getChannelState(recordStatus.Chanel4Status);
    std::cerr << "CHANNEL 4 STATE = " << params4.channelState << std::endl;
    printf("CHANNEL 4 STATUS = %x\n", recordStatus.Chanel4Status);

    channels.at(CHANNEL1_NUM)->updateStatusParams(params1);
    channels.at(CHANNEL2_NUM)->updateStatusParams(params2);
    channels.at(CHANNEL3_NUM)->updateStatusParams(params3);
    channels.at(CHANNEL4_NUM)->updateStatusParams(params4);
}

void ChannelControlWidget::setChannelSettings(bool immediately)
{
    Q_UNUSED(immediately);
    if( !isChannelInited )
        return;

    std::bitset<16> bias(recordSettings.ChanelBias);
    std::bitset<16> filtr(recordSettings.ChanelAGCLPFMode);

    ChannelSettingsParams params1;
    ChannelSettingsParams params2;
    ChannelSettingsParams params3;
    ChannelSettingsParams params4;

    memset(&params1, 0, sizeof(ChannelSettingsParams));
    memset(&params2, 0, sizeof(ChannelSettingsParams));
    memset(&params3, 0, sizeof(ChannelSettingsParams));
    memset(&params4, 0, sizeof(ChannelSettingsParams));

#warning sensivity type and value!
    params1.biasType = static_cast<BiasType>(bias.test(CHANNEL1_ENABLE_POS));
    params1.sensivityType = getSensivityType(recordSettings.Chanel1MixCFG);
    params1.sensVal = static_cast<SensivityVal>(recordSettings.Chanel1Sens);
    params1.filtrType = static_cast<FiltrType>(filtr.test(CHANNEL_1_LPF_ENABLE));

    params2.biasType = static_cast<BiasType>(bias.test(CHANNEL2_ENABLE_POS));
    params2.sensivityType = getSensivityType(recordSettings.Chanel2MixCFG);
    params2.sensVal = static_cast<SensivityVal>(recordSettings.Chanel2Sens);
    params2.filtrType = static_cast<FiltrType>(filtr.test(CHANNEL_2_LPF_ENABLE));

    params3.biasType = static_cast<BiasType>(bias.test(CHANNEL3_ENABLE_POS));
    params3.sensivityType = getSensivityType(recordSettings.Chanel3MixCFG);
    params3.sensVal = static_cast<SensivityVal>(recordSettings.Chanel3Sens);
    params3.filtrType = static_cast<FiltrType>(filtr.test(CHANNEL_3_LPF_ENABLE));


    params4.biasType = static_cast<BiasType>(bias.test(CHANNEL4_ENABLE_POS));
    params4.sensivityType = getSensivityType(recordSettings.Chanel4MixCFG);
    params4.sensVal = static_cast<SensivityVal>(recordSettings.Chanel4Sens);
    params4.filtrType = static_cast<FiltrType>(filtr.test(CHANNEL_4_LPF_ENABLE));

    channels.at(CHANNEL1_NUM)->updateSettingsParams(params1);
    channels.at(CHANNEL2_NUM)->updateSettingsParams(params2);
    channels.at(CHANNEL3_NUM)->updateSettingsParams(params3);
    channels.at(CHANNEL4_NUM)->updateSettingsParams(params4);
}


void ChannelControlWidget::initRecorderStatusControl()
{
    if( recorderStatusCtrl == nullptr )
    {
        recorderStatusCtrl = std::shared_ptr<RecorderStatusControl>(
                    new RecorderStatusControl(dataProvider, *logger.get()));
    }
}

SensivityType ChannelControlWidget::getSensivityType(uint16_t value)
{
    switch( value & INPUT_CONFIG )
    {
        case INPUT_OFF:
        {
            break;
        }
        case INPUT_LINE:
        {
            return LINEAR;
        }
        case INPUT_ELECTRET:
        {
            return ELECTRIC;
        }
        case INPUT_DYNAMIC:
        {
            return ELECTRIC_DINAMIC;
        }
        default:
            break;
    }
    return LINEAR;
}

ChannelState ChannelControlWidget::getChannelState(const uint16_t &value)
{
    if( value & AXIS_SOFTRECORDER_STATUS__PLAY_NOW && value & AXIS_SOFTRECORDER_STATUS__RECORD_NOW )
    {
        return BOTH;
    }
    else if(value & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
    {
        return RECORDING;
    }
    else if ( value & AXIS_SOFTRECORDER_STATUS__PLAY_NOW)
    {
        return MONITORING;
    }

    return STANDBY;
}

void ChannelControlWidget::getCurAndRemainTime(double &remain, double &cur)
{
    double dblSectorRate = 0;
//    if( recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_1_2_STEREO )
//    {

//        dblSectorRate = (129.032 * 2) / (recordSettings.Chanel1AudioBand
//                         * recordSettings.Chanel1AudioCommpression);
//    }
//    else if(recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_1_ENABLED &&
//        recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_2_ENABLED)
//    {
//        dblSectorRate = (129.032 * 1) / (recordSettings.Chanel1AudioBand
//                         * recordSettings.Chanel1AudioCommpression);
//        dblSectorRate += (129.032 * 1) / (recordSettings.Chanel2AudioBand
//                          * recordSettings.Chanel2AudioCommpression);
//    }

//    if( recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_3_4_STEREO )
//    {

//        dblSectorRate += (129.032 * 2) / (recordSettings.Chanel1AudioBand
//                         * recordSettings.Chanel3AudioCommpression);
//    }
//    else if(recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_3_ENABLED &&
//        recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_4_ENABLED)
//    {
//        dblSectorRate = (129.032 * 1) / (recordSettings.Chanel1AudioBand
//                         * recordSettings.Chanel3AudioCommpression);
//        dblSectorRate += (129.032 * 1) / (recordSettings.Chanel2AudioBand
//                          * recordSettings.Chanel4AudioCommpression);
//    }


    if( recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_1_2_STEREO )
    {

        dblSectorRate += (129.032 * 2) / (recordSettings.Chanel1AudioBand
                         * recordSettings.Chanel1AudioCommpression);
    }
    else
    {
        if(recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_1_ENABLED)
        {
            dblSectorRate += (129.032 * 1) / (recordSettings.Chanel1AudioBand
                             * recordSettings.Chanel1AudioCommpression);
        }

        if(recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_2_ENABLED)
        {
            dblSectorRate += (129.032 * 1) / (recordSettings.Chanel2AudioBand
                              * recordSettings.Chanel2AudioCommpression);
        }
    }


    if( recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_3_4_STEREO )
    {

        dblSectorRate += (129.032 * 2) / (recordSettings.Chanel1AudioBand
                         * recordSettings.Chanel3AudioCommpression);
    }
    else
    {
        if(recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_3_ENABLED)
        {
            dblSectorRate += (129.032 * 1) / (recordSettings.Chanel1AudioBand
                             * recordSettings.Chanel3AudioCommpression);
        }

        if(recordSettings.ChanelCfgMode & AXIS_SOFTRECORDER_SETTINGS__CHANEL_4_ENABLED)
        {
            dblSectorRate += (129.032 * 1) / (recordSettings.Chanel2AudioBand
                              * recordSettings.Chanel4AudioCommpression);
        }
    }

    uint64_t llDiskSize = recordStatus.FlashCapacity;
    uint64_t llRecSize = recordStatus.FlashUsed;

    uint64_t llBlockNum = llDiskSize / (1024.0 * 1024.0);
    llDiskSize = llDiskSize - llBlockNum * 8192;

    llBlockNum = llRecSize / (1024.0 * 1024.0);
    llRecSize = llRecSize - llBlockNum * 8192;

    remain = ((llDiskSize - llRecSize) /
                512) / dblSectorRate;

    cur = (llRecSize /
                512) / dblSectorRate;
}

void ChannelControlWidget::initRecorderSettingsControl()
{
    if( recorderSettingsCtrl == nullptr )
    {
        recorderSettingsCtrl = std::shared_ptr<RecorderSettingsContol>(
                    new RecorderSettingsContol(dataProvider, *logger.get()));
    }
}

void ChannelControlWidget::initChannels()
{
    if( !isChannelInited )
    {
        QQuickItem *rowItem = currentItem->findChild<QQuickItem*>("Row");
        if( rowItem == nullptr )
            throw std::runtime_error("Row item was not found");

        channels[CHANNEL1_NUM] = std::shared_ptr<Channel>(new Channel(*this->logger, qmlEngine, SINGLE_CHANNEL,
                                        CHANNEL1_NUM, "Канал 1", userManipulators,
                                        shared_from_this(),
                                        rowItem));

        channels[CHANNEL2_NUM] = std::shared_ptr<Channel>(new Channel(*this->logger, qmlEngine, SINGLE_CHANNEL,
                                        CHANNEL2_NUM, "Канал 2", userManipulators,
                                        shared_from_this(),
                                        rowItem));

        channels[CHANNEL3_NUM] = std::shared_ptr<Channel>(new Channel(*this->logger, qmlEngine, SINGLE_CHANNEL,
                                        CHANNEL3_NUM, "Канал 3", userManipulators,
                                        shared_from_this(),
                                        rowItem));

        channels[CHANNEL4_NUM] = std::shared_ptr<Channel>(new Channel(*this->logger, qmlEngine, SINGLE_CHANNEL,
                                        CHANNEL4_NUM, "Канал 4", userManipulators,
                                        shared_from_this(),
                                        rowItem));



        try
        {
            channels.at(CHANNEL1_NUM)->initChannel();
            channels.at(CHANNEL2_NUM)->initChannel();
            channels.at(CHANNEL3_NUM)->initChannel();
            channels.at(CHANNEL4_NUM)->initChannel();
        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
        }
    }
    isChannelInited = true;
}

void ChannelControlWidget::getRecordData()
{
    try
    {
        dataMutex.lock();
        this->recordSettings = recorderSettingsCtrl->getRecordSettings();
        this->recordStatus = recorderStatusCtrl->getRecorderStatus();
        dataMutex.unlock();
        isAdapterConnected = true;

        setChannelsStatus();
        setChannelSettings(true);
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__,
                              dpr::ProviderError::getInstance().getErrorString(except).c_str());
    }
}

std::string ChannelControlWidget::getRecorderSourceType(uint16_t source)
{
    std::bitset<16> bits(source);
    std::string typeStr = "";

//    RECORD_SOURCE_REMOTE, = 1
//    RECORD_SOURCE_VOX,
//    RECORD_SOURCE_TIMER,
//    RECORD_SOURCE_PROGRAM,
//    RECORD_SOURCE_MANUAL,
//    RECORD_SOURCE_DIGITAL,
//    RECORD_SOURCE_RADIO

    if( bits.test(0) )
    {
        if(typeStr.empty())
            typeStr = "удаленный";
        else
            typeStr += "\n + удаленный";
    }
    if( bits.test(1) )
    {
        if(typeStr.empty())
            typeStr = "vox";
        else
            typeStr += "\n + vox";
    }
    if( bits.test(2) )
    {
        if(typeStr.empty())
            typeStr = "таймер";
        else
            typeStr += "\n + таймер";
    }
    if( bits.test(3) )
    {
        if(typeStr.empty())
            typeStr = "программа";
        else
            typeStr += "\n + программа";
    }
    if( bits.test(4) )
    {
        if(typeStr.empty())
            typeStr = "ручной";
        else
            typeStr += "\n + ручной";
    }
    if( bits.test(5) )
    {
        if(typeStr.empty())
            typeStr = "digital";
        else
            typeStr += "\n + digital";
    }
    if( bits.test(6) )
    {
        if(typeStr.empty())
            typeStr = "радио";
        else
            typeStr += "\n + радио";
    }

    if(typeStr.empty())
    {
        typeStr = "---";
    }


    return typeStr;
}

void ChannelControlWidget::updateDataOnAdapter(SoftRecorderSettingsStruct_t data)
{
    try
    {
        recorderSettingsCtrl->applyRecordSettings(data);
        dataMutex.lock();
        this->recordSettings = data;
        dataMutex.unlock();
        emit signalChannelsParamsApplyed();
    }
    catch( std::system_error &except )
    {
        dataMutex.unlock();
        Q_UNUSED(except);
        userManipulators->showMessage(std::string("Не удалось применить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

void ChannelControlWidget::threadFunction()
{
    struct timeval tv;
    __time_t lastTime = 0;
    int timeout = 0;

    while(threadWork)
    {
        if( isProcessShown )
            timeout = 1;
        else
            timeout = 15;

        gettimeofday(&tv, nullptr);
        if( tv.tv_sec >= (lastTime + timeout) || !conditionWait || tv.tv_sec < lastTime )
        {
            conditionWait = true;
            lastTime = tv.tv_sec;


            //t this->logger->Message(LOG_INFO, __LINE__, __func__, "Update recorder data Channel");
            try
            {
                dataMutex.lock();
                recorderSettingsCtrl->updateStatus();
                recorderStatusCtrl->updateStatus();
                this->recordSettings = recorderSettingsCtrl->getRecordSettings();
                this->recordStatus = recorderStatusCtrl->getRecorderStatus();
                dataMutex.unlock();
                emit signalUpdateLocalForms();
            }
            catch( std::system_error &systemErr )
            {
                dataMutex.unlock();
                this->logger->Message(LOG_ERR, __LINE__,
                                      __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
                if(!dataProvider->getConnectStatus())
                {
                    emit signalConnectionLost();
                }
            }
        }
        else
        {
            usleep(10000);
        }
    }
}

void ChannelControlWidget::updateEraseProcessWidget()
{

    switch (static_cast<FLASH_STATUS>(recordStatus.FlashStatus))
    {
        case fsFlashErasing:
        {
            if(wasErasing)
                this->eraseProcessWidget->show();
            this->eraseProcessWidget->updateEraseData(recordStatus.FlashProgress,
                                                      recordStatus.FlashUsed,
                                                      recordStatus.FlashCapacity);
            wasErasing = true;
            this->stateWidget->showState("ERASE_DATA_STATE",
                                        util::format("%i", recordStatus.FlashProgress) + "%");

            break;
        }
        default:
        {
            if( activeProcessWidget == ACTIVE_ERASE )
            {
                this->eraseProcessWidget->updateEraseData(0,
                                                      recordStatus.FlashUsed, recordStatus.FlashCapacity);
            }
            if( wasErasing )
            {
                wasErasing = false;
                this->stateWidget->hideState("ERASE_DATA_STATE");
                memoryProcessWidget->show();
                userManipulators->showMessage(std::string("Стирание завершено"));
            }
            break;
        }
    }
}

void ChannelControlWidget::updateMemoryProcessWidget()
{
    double dblRemainTime = 0;
    double dblCurRecTime = 0;

    getCurAndRemainTime(dblRemainTime, dblCurRecTime);

    uint16_t remH, remM, remS;
    remH = (uint16_t)(dblRemainTime / 3600);
    remM = (uint16_t)((dblRemainTime - remH * 3600.0) / 60);
    remS = (uint16_t)(dblRemainTime -  remM * 60 - remH * 3600);

    uint16_t curH, curM, curS;
    curH = (uint16_t)(dblCurRecTime / 3600);
    curM = (uint16_t)((dblCurRecTime - curH * 3600.0) / 60);
    curS = (uint16_t)(dblCurRecTime -  curM * 60 - curH * 3600);

    this->memoryProcessWidget->updateData(recordStatus.FlashCapacity,
                                          recordStatus.FlashUsed,
                                          util::format("%.2dч. %.2dм. %.2dс.",curH,curM,curS),
                                          util::format("%.2dч. %.2dм. %.2dс.",remH,remM,remS));

//    this->memoryProcessWidget->updateData(recordStatus.FlashCapacity,
//                                          recordStatus.FlashUsed,
//                                          recordStatus.FullRecSecs,
//                                          util::format("%.2dч. %.2dм. %.2dс.",h,m,s));
}

void ChannelControlWidget::slotUpdateForms()
{
    if(startLoadingAnim && dataProvider->getConnectStatus())
    {
        userManipulators->hideLoadingWidget();
        startLoadingAnim = false;
    }

    setChannelsStatus();
    setChannelSettings(true);

    updateEraseProcessWidget();

    switch (activeProcessWidget)
    {
        case ACTIVE_MEMORY:
            updateMemoryProcessWidget();
            break;
        case ACTIVE_ERASE:
            break;
        default:
            break;
    }

    //if status that we have usaved data show state
    try
    {
        std::cerr << "SAVE FLAG = " << recordStatus.ExtStatus << std::endl;
        printf("SAVE FLAG hex %x\n", recordStatus.ExtStatus);

        if( recordStatus.ExtStatus & RECORDER_EXT_STATUS__NO_DATA_SAVE)
        {
            stateWidget->showState(stateWidgets.at(UNSAVED_DATA_STATE),"Параметры накопителя не сохранены");
        }
        else
        {
            stateWidget->hideState(stateWidgets.at(UNSAVED_DATA_STATE));
        }
    }
    catch(std::exception &except)
    {
        Q_UNUSED(except);
        this->logger->Message(LOG_ERR, __LINE__, __func__, "State doesn't exist !!!!");
#warning show message that map State doesn exist
    }
}

void ChannelControlWidget::slotOnProcessWidgetSwipeSelected(string name, std::shared_ptr<IProcessWidget> object)
{
    this->logger->Message(LOG_INFO, __LINE__, __func__, "Widget selected %s", name.c_str());

    if( object == memoryProcessWidget )
    {
        activeProcessWidget = ACTIVE_MEMORY;
        isProcessShown = true;
    }
    else if( object == eraseProcessWidget )
    {
        isProcessShown = true;
        activeProcessWidget = ACTIVE_ERASE;
        updateEraseProcessWidget();
    }
    else
    {
        activeProcessWidget = NONE;
    }

}

void ChannelControlWidget::slotOnProcessWidgetSwipeDeSelected(string name, std::shared_ptr<IProcessWidget> object)
{
    Q_UNUSED(object);
    isProcessShown = false;
    activeProcessWidget = NONE;
    this->logger->Message(LOG_INFO, __LINE__, __func__, "Widget selected %s", name.c_str());
}

void ChannelControlWidget::slotChannelParamsApplyed()
{
    if(this->channels.at(currentUpdateChannelNum)->getChannelType() == UNION_CHANNEL )
    {
        if( currentUpdateChannelNum == CHANNEL1_NUM || currentUpdateChannelNum == CHANNEL2_NUM  )
        {
            this->channels.at(CHANNEL1_NUM)->channelDataSetted("all");
            this->channels.at(CHANNEL2_NUM)->channelDataSetted("all");
        }
        else
        {
            this->channels.at(CHANNEL3_NUM)->channelDataSetted("all");
            this->channels.at(CHANNEL4_NUM)->channelDataSetted("all");
        }
    }
    else
    {
        this->channels.at(currentUpdateChannelNum)->channelDataSetted("all");

        if( currentUpdateChannelNum == CHANNEL1_NUM || currentUpdateChannelNum == CHANNEL2_NUM  )
        {
            this->channels.at(CHANNEL1_NUM)->channelDataSetted("sensitivity");
            this->channels.at(CHANNEL2_NUM)->channelDataSetted("sensitivity");
        }
        else
        {
            this->channels.at(CHANNEL3_NUM)->channelDataSetted("sensitivity");
            this->channels.at(CHANNEL4_NUM)->channelDataSetted("sensitivity");
        }
    }

}

void ChannelControlWidget::slotConnectionLost()
{
    memset(&recordSettings, 0, sizeof (SoftRecorderSettingsStruct_t));
    setChannelsStatus();

    connectionLostCounter++;
    if(connectionLostCounter > 10)
    {
      connectionLostCounter = 0;
//      userManipulators->hideLoadingWidget();
      userManipulators->showMessage(std::string("Накопитель: Ошибка соединения."));
    }

}

ChannelControlWidget::~ChannelControlWidget()
{
    threadWork = false;
    conditionWait = false;
//    cv.notify_all();

    if( channelThread.joinable() )
    {
        channelThread.join();
    }
    std::cerr << "Remove ChannelControlWidget" << std::endl;
}
