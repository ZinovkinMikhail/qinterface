#ifndef CHANNEL_H
#define CHANNEL_H

#include <memory>

#include <QObject>
#include <QVariantMap>
#include <QString>
#include <QQuickItem>

#include <rosslog/log.h>

#include "iusermanipulators.h"
#include "iqmlengine.h"
#include "channelstatuspage.h"
#include "channelparamspage.h"
#include "ichannel.h"
#include "ichannelcontrol.h"

typedef enum
{
    COMBO_SENSIVITY
} ComboDrumType;

#define CHANNEL_SINGLE_QML_PATH "qrc:/Channel.qml"

class Channel : public QObject, public IChannel, public std::enable_shared_from_this<Channel>
{
    Q_OBJECT
public:
    explicit Channel(rosslog::Log &log, std::shared_ptr<IQmlEngine> qmlEngine,
                     ChannelType type, int channelNum, std::string name,
                     std::shared_ptr<IUserManipulators> userManipulators,
                     std::shared_ptr<IChannelControl> channelControlInterface,
                     QObject *parent = nullptr);
    ~Channel();

    void initChannel();
    void changeChannelType(ChannelType type);
    void showChannel();
    void hideChannel();
    void channelDataSetted(std::string paramName);
    void changeUnionParams(ChannelSettingsParams settParams);
    void changePairParam(ChannelSettingsParams settParams);
    void prepareToDelete();

    ChannelType getChannelType();

    void updateSettingsParams(ChannelSettingsParams params);
    void updateStatusParams(ChannelStatusParams params);


protected:
    void setNewSettingParams(ChannelSettingsParams settingParams) override;
    void unionParamsChanged(ChannelSettingsParams settingsParams) override;
    void pairParamChanged(ChannelSettingsParams settingsParams) override;
    void startRecord() override;
    void stopRecord() override;
    void startMonitor() override;
    void stopMonitor() override;
    bool getErasureStatus() override;

private:
    rosslog::Log *logger;
    std::shared_ptr<IQmlEngine> qmlEngine;
    ChannelType type;
    int channelNum;
    std::string name;
    std::shared_ptr<ChannelStatusPage> channelStatusPage;
    std::shared_ptr<ChannelParamsPage> channelParamsPage;
    QQuickItem *channelItem;
    std::shared_ptr<IUserManipulators> userManipulators;
    ChannelSettingsParams settingsParams;
    std::shared_ptr<IChannelControl> channelControlInterface;

signals:

public slots:
    void slotUpdateParamsData();

};

#endif // CHANNEL_H
