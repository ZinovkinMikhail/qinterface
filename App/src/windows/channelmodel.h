#ifndef CHANNELMODEL_H
#define CHANNELMODEL_H

#define INPUT_CONFIG AXIS_SOFTRECORDER_SETTINGS__INPUT_1_CFG
#define INPUT_OFF AXIS_SOFTRECORDER_SETTINGS__INPUT_1_OFF
#define INPUT_LINE AXIS_SOFTRECORDER_SETTINGS__INPUT_1_LINE
#define INPUT_ELECTRET AXIS_SOFTRECORDER_SETTINGS__INPUT_1_ELECTRET
#define INPUT_DYNAMIC AXIS_SOFTRECORDER_SETTINGS__INPUT_1_DYNAMIC

typedef enum
{
    LINEAR,
    ELECTRIC,
    ELECTRIC_DINAMIC
} SensivityType;

typedef enum
{
    STANDBY,
    MONITORING,
    RECORDING,
    BOTH
} ChannelState;

typedef enum
{
    FILTR_OFF,
    FILTR_ON
} FiltrType;

typedef enum
{
    BIAS_OFF,
    BIAS_ON
} BiasType;

typedef enum
{
    SENSIVITY_5,
    SENSIVITY_10,
    SENSIVITY_20,
    SENSIVITY_40
} SensivityVal;

typedef struct
{
    SensivityType sensivityType;
    BiasType biasType;
    FiltrType filtrType;
    SensivityVal sensVal;
} ChannelSettingsParams;

typedef enum
{
    SINGLE_CHANNEL,
    UNION_CHANNEL
} ChannelType;

#endif // CHANNELMODEL_H
