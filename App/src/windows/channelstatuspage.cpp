#include <assert.h>

#include "channelstatuspage.h"

ChannelStatusPage::ChannelStatusPage(std::shared_ptr<IChannel> channelInterface,
                                     std::shared_ptr<IQmlEngine> qmlEngine,
                                     std::shared_ptr<IUserManipulators> userManipulators,
                                     QObject *parent)
    : QObject(parent),
      channelInterface(channelInterface),
      qmlEngine(qmlEngine),
      userManipulators(userManipulators),
      currentItem(nullptr)
{
    assert(parent);
    assert(qmlEngine);

    statusParams.enabled = false;
}

void ChannelStatusPage::iniChannelStatusPage()
{
    QObject *currentObject;
    currentObject = qmlEngine->createComponent(CHANNEL_STATUS_PAGE_QML, this);
    assert(currentObject);

    currentItem = qobject_cast<QQuickItem*>(currentObject);
    currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

    connect( currentItem, SIGNAL(recordButtonClicked()),
             this, SLOT(slotRecordButtonClicked()));
    connect( currentItem, SIGNAL(monitoringButtonClicked()),
             this, SLOT(slotMonitoringButtonClicked()));
}

void ChannelStatusPage::setStatusParams(ChannelStatusParams params)
{
    assert(currentItem);

    QString channelTypeName = "Моно";
    if( params.type == UNION_CHANNEL )
        channelTypeName = "Стерео";

    QMetaObject::invokeMethod(currentItem, "setStatusType",
                                  Q_ARG(QVariant, channelTypeName));

    QMetaObject::invokeMethod(currentItem, "setSourceType",
                              Q_ARG(QVariant, QString::fromStdString(params.recordSource)));

    QMetaObject::invokeMethod(currentItem, "setChanelStateStandBy");
    switch (params.channelState)
    {
        case STANDBY:
        {
            QMetaObject::invokeMethod(currentItem, "setChanelStateStandBy");
            break;
        }
        case MONITORING:
        {
            QMetaObject::invokeMethod(currentItem, "setChanelStateMonitoring");
            break;
        }
        case RECORDING:
        {
            QMetaObject::invokeMethod(currentItem, "setChanelStateRecording");
            break;
        }
        case BOTH:
        {
            QMetaObject::invokeMethod(currentItem, "setChanelStateMonitoring");
            QMetaObject::invokeMethod(currentItem, "setChanelStateRecording");
            break;
        }
        default:
            break;
    }
    statusParams = params;
}

void ChannelStatusPage::slotRecordButtonClicked()
{
    if(this->channelInterface->getErasureStatus())
    {
        userManipulators->showMessage(std::string("Идет стирание. Управление записью недоступно."));
        return;
    }

    if ( statusParams.channelState == RECORDING || statusParams.channelState == BOTH )
        this->channelInterface->stopRecord();
    else if( statusParams.channelState != RECORDING || statusParams.channelState != BOTH )
        this->channelInterface->startRecord();
}

void ChannelStatusPage::slotMonitoringButtonClicked()
{
    if( statusParams.channelState == MONITORING || statusParams.channelState == BOTH )
    {
        this->channelInterface->stopMonitor();
    }
    else if( statusParams.channelState != MONITORING || statusParams.channelState != BOTH )
    {
        this->channelInterface->startMonitor();
    }
}

ChannelStatusPage::~ChannelStatusPage()
{
    std::cerr << "Removed ChannelStatusPage" << std::endl;
}
