#ifndef CHANNELCONTROLWIDGET_H
#define CHANNELCONTROLWIDGET_H

#include <thread>
#include <future>

#include <QObject>
#include <QQuickItem>

#include <rosslog/log.h>

#include <dataprovider/dataprovider.h>
#include <dataprovider/recordersettingscontol.h>
#include <dataprovider/recorderstatuscontrol.h>
#include <dataprovider/dataprovidererror.h>
#include <dataprovider/idatawaiter.h>

#include "iemptyplacewindow.h"
#include "channel.h"
#include "apptimer.h"
#include "iusermanipulators.h"
#include "datakeeper.h"
#include "iqmlengine.h"
#include "ichannelcontrol.h"
#include "memoryprocesswidget.h"
#include "eraseprocesswidget.h"
#include "util.h"


#define WINDOW_NAME "Channel"
#define COMPONENT_PATH "qrc:/ChannelControl.qml"
#define CHANNEL_COMPONENT_NAME "ChannelControlClass"
#define CHANNEL_WINDOW_IDENTIFICATOR "ChannelControl"
#define CHANNEL_PRESENTATION_NAME "Цикад-ЦМ 2"

#define MEMORY_PROCES_WIDGET_NAME "MemoryProcessWidget"

#define CHANNEL_STATUS_UPDATE_TIME 1 //sec

#define CHANNEL1_ENABLE_POS 0
#define CHANNEL2_ENABLE_POS 1
#define CHANNEL3_ENABLE_POS 2
#define CHANNEL4_ENABLE_POS 3
#define CHANNEL1_STEREO_POS 8
#define CHANNEL3_STEREO_POS 9

#define CHANNEL_1_LPF_ENABLE 8
#define CHANNEL_2_LPF_ENABLE 9
#define CHANNEL_3_LPF_ENABLE 10
#define CHANNEL_4_LPF_ENABLE 11

#define CHANNEL1_NUM 1
#define CHANNEL2_NUM 2
#define CHANNEL3_NUM 3
#define CHANNEL4_NUM 4

#define _STR(x) #x
#define STRINGIFY(x)  _STR(x)

typedef enum
{
    NONE,
    ACTIVE_MEMORY,
    ACTIVE_ERASE
} ActiveProcessWidget;

typedef enum
{
    UNSAVED_DATA_STATE,
    ERASE_DATA_STATE
} ChannelControlStates;

class ChannelControlWidget : public QObject, public IEmptyPlaceWindow,
        public IDataWaiter, public IChannelControl, public std::enable_shared_from_this<ChannelControlWidget>
{
    Q_OBJECT
public:
    explicit ChannelControlWidget(QObject *parent = nullptr);

    ~ChannelControlWidget();

    std::list<std::shared_ptr<IProcessWidget>> getProcessWidgets();

    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;

    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;


protected:
    void setNewSettingsParams(int channelNum, ChannelType channelType,
                              ChannelSettingsParams settingParams) override;
    void unionParamsChanged(int channelNum, ChannelType channelType,
                            ChannelSettingsParams settingsParams) override;
    void pairParamChanged(int channelNum,
                            ChannelSettingsParams settingsParams) override;
    void recieveData(int command, std::vector<uint8_t> data, size_t size) override;
    void startRecord(int channelNum, ChannelType channelType) override;
    void stopRecord(int channelNum, ChannelType channelType) override;
    void startMonitor(int channelNum, ChannelType channelType) override;
    void stopMonitor(int channelNum, ChannelType channelType) override;
    bool getErasureStatus() override;

private:
    void initMemoryProcessWidget();
    void initEraseProcessWidget();
    void initStateWidgets();
    void prepareQmlData();
    void initRecorderStatusControl();
    void initRecorderSettingsControl();
    void initChannels();
    void getRecordData();
    void setChannelSettings(bool immediately);
    void setChannelsStatus();
    std::string getRecorderSourceType(uint16_t source);
    void updateDataOnAdapter(SoftRecorderSettingsStruct_t data);
    void threadFunction();
    void updateEraseProcessWidget();
    void updateMemoryProcessWidget();
    SensivityType getSensivityType(uint16_t value);
    ChannelState getChannelState(const uint16_t &value);
    void getCurAndRemainTime(double &remain, double &cur);

    QObject *windowComponent;
    QQuickItem *currentItem;

    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusCtrl;
    std::shared_ptr<RecorderSettingsContol> recorderSettingsCtrl;
    std::shared_ptr<MemoryProcessWidget> memoryProcessWidget;
    std::shared_ptr<EraseProcessWidget> eraseProcessWidget;
    std::shared_ptr<IStateWidget> stateWidget;

    SoftRecorderSettingsStruct_t recordSettings;
    SoftRecorderStatusStruct_t recordStatus;
    std::map<int, std::shared_ptr<Channel>> channels;
    uint16_t previousCfgMode;
    std::list<std::shared_ptr<IProcessWidget>> processWidgets;

    bool isChannelInited;
    bool isAdapterConnected;
    bool isFirstLoad;
    std::future<void> updateWorker;
    int currentUpdateChannelNum;
    std::thread channelThread;
//    std::condition_variable cv;
//    std::mutex cv_m;
    std::mutex dataMutex;
    std::map<ChannelControlStates, std::string> stateWidgets;
    ActiveProcessWidget activeProcessWidget;
    bool threadWork;
//    bool conditionWork;
    bool conditionWait;
    bool wasErasing;
    bool isProcessShown;
    bool startLoadingAnim;
    int connectionLostCounter;

signals:
    void pushSingleChannel(int id, int type, QVariant map, bool reorder);
    void pushStereoChannel(int id, int type,QVariant map, bool reorder);

signals:
    void signalUpdateLocalForms(); //slotUpdateForms
    void signalChannelsParamsApplyed();
    void signalConnectionLost();

public slots:
    void slotUpdateForms();
    void slotOnProcessWidgetSwipeSelected(std::string name, std::shared_ptr<IProcessWidget> object);
    void slotOnProcessWidgetSwipeDeSelected(std::string name, std::shared_ptr<IProcessWidget> object);
    void slotChannelParamsApplyed();
    void slotConnectionLost();
};

#endif // CHANNELCONTROLWIDGET_H
