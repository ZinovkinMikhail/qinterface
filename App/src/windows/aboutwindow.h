#ifndef ABOUTWINDOW_H
#define ABOUTWINDOW_H

#include <memory>
#include <future>

#include <QObject>
#include <QQuickItem>
#include <QDateTime>
#include <QTextCodec>

#include <axis_time.h>

#include <dataprovider/adapterstatuscontrol.h>
#include <dataprovider/recorderstatuscontrol.h>
#include <dataprovider/recordersettingscontol.h>

#include "iemptyplacewindow.h"
#include "updateprogramwindow.h"

#define WINDOW_NAME "AboutWindow"
#define ABOUT_WINDOW_QML_PATH "qrc:/windows/AboutWindow.qml"

class AboutWindow : public QObject, public IEmptyPlaceWindow
{
    Q_OBJECT
public:
    explicit AboutWindow(QObject *parent = nullptr);

    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;

    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;


private:
    void initAdapterStatusControl();
    void initRecorderStatusControl();
    void initRecorderSettingsControl();
    void initUdateWindow();
    void updateData();
    std::string recorderLibPrintIniVersionDateString( uint32_t *ver );
    std::string recorderLibPrintIniVersionDateBuild( uint32_t *ver );

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusControl;
    std::shared_ptr<RecorderSettingsContol> recorderSettingsControl;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusControl;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<UpdateProgramWindow> updateWindow;
    std::future<void> updateWorker;
    SoftAdapterDeviceCurrentStatus_t adapterStatus;
    SoftRecorderGetAccStateStruct_t adapterAccStatus;
    SoftRecorderSettingsStruct_t recorderSettings;
    SoftRecorderStatusStruct_t recorderStatus;

signals:
    void signalRecivedAdapterData();

private slots:
    void slotRecievedAdapterData();
    void slotUpdateButtonClicked();

};

#endif // ABOUTWINDOW_H
