#ifndef CHANNELPARAMSPAGE_H
#define CHANNELPARAMSPAGE_H

#include <memory>


#include <QObject>
#include <QQuickItem>

#include "iqmlengine.h"
#include "iusermanipulators.h"
#include "ichannel.h"
#include "channelmodel.h"

#define SENSIVITY_VAL_SIZE 4
#define FILTR_VAL_SIZE 2
#define BIAS_VAL_SIZE 2

#define CHANNEL_PARAMS_PAGE_QML "qrc:/ChannelParamsPage.qml"


class ChannelParamsPage : public QObject
{
    Q_OBJECT
public:
    explicit ChannelParamsPage(std::shared_ptr<IChannel> channelInterface,
                               std::shared_ptr<IUserManipulators> userManipulators,
                               std::shared_ptr<IQmlEngine> qmlEngine,
                               QObject *parent = nullptr);

    void iniChannelParamsPage();
    void setChannelSettingsParams(ChannelSettingsParams params);
    void paramsDataSetted(std::string paramName);
    void changeUnionParams(ChannelSettingsParams params);
    void changePairParam(ChannelSettingsParams params);

private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    ChannelSettingsParams settingsParams;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<IChannel> channelInterface;

    std::map<SensivityType, std::vector<std::string>> sensivityMap;
    std::map<FiltrType, std::string> filtrMap;
    std::map<BiasType, std::string> biasMap;

    std::vector<std::string> filtrVector;
    std::vector<std::string> biasVector;

    void setSensivityTypeQml(QString type, bool isComboDrum);
    void setFiltrTypeQml(QString type, bool isComboDrum);
    void setBiasTypeQml(QString type, bool isComboDrum);

public slots:
    void slotShowSensComboDrum();
    void slotShowFiltrComboDrum();
    void slotShowBiasComboDrum();
    void slotApplySettingsParams();



};

#endif // CHANNELPARAMSPAGE_H
