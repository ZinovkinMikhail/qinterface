#include <cassert>
#include <QQmlProperty>
#include <iostream>
#include "updateprogramwindow.h"



UpdateProgramWindow::UpdateProgramWindow(std::shared_ptr<IUserManipulators> userManipulators,
                             std::shared_ptr<IQmlEngine> qmlEngine,
                             std::shared_ptr<rosslog::Log> log,
                             QObject *parent) :
        QObject(parent),
        qmlEngine(qmlEngine),
        logger(log),
        currentItem(nullptr)
{
    assert(qmlEngine);
    assert(logger);
    assert(parent);
}


void UpdateProgramWindow::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(
                qmlEngine->createComponent(UPDATEWINDOW_QML_PARH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

        connect(currentItem, SIGNAL(updateAdapterClicked()),
                this, SLOT(slotUpdateAdapterClicked()));
        connect(currentItem, SIGNAL(updateRecorderClicked()),
                this, SLOT(slotUpdateRecorderClicked()));
        connect(currentItem, SIGNAL(cancelButtonClicked()),
                this, SLOT(slotCancelButtonClicked()));
    }
}


void UpdateProgramWindow::show()
{
    QQmlProperty(currentItem, "visible").write(true);

    setFirmwareInfo("2.0-20-10-13-769-g0b0982d", "1.0");
    setProgress(0);
}


void UpdateProgramWindow::hide()
{
    QQmlProperty(currentItem, "visible").write(false);
}


void UpdateProgramWindow::setFirmwareInfo(QString _adapterVer, QString _recorderVer)
{
    QMetaObject::invokeMethod(currentItem,"setFirmwareInfo",
                              Q_ARG(QVariant, _adapterVer),
                              Q_ARG(QVariant, _recorderVer));
}


void UpdateProgramWindow::setProgress(int _value)
{
    QMetaObject::invokeMethod(currentItem,"setProgress", Q_ARG(QVariant, _value));
}


void UpdateProgramWindow::slotUpdateAdapterClicked()
{
    std::cout << "slotUpdateAdapterClicked" << std::endl;
}

void UpdateProgramWindow::slotUpdateRecorderClicked()
{
    std::cout << "slotUpdateRecorderClicked" << std::endl;
}

void UpdateProgramWindow::slotCancelButtonClicked()
{
    hide();
}
