#include <assert.h>
#include <iostream>

#include "aboutwindow.h"

using namespace adapter_status_ctrl;
using namespace recorder_status_ctrl;
using namespace data_provider;
using namespace rosslog;

AboutWindow::AboutWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    adapterStatusControl(nullptr),
    recorderSettingsControl(nullptr),
    recorderStatusControl(nullptr)
{

}

std::shared_ptr<IUserManipulators> AboutWindow::initWindow(EWindSizeType winSizeType,
                                                           std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                           std::shared_ptr<IStateWidget> stateWidget,
                                                           std::shared_ptr<IUserManipulators> userManipulators,
                                                           std::shared_ptr<IQmlEngine> engine,
                                                           std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(dataProvider);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    Q_UNUSED(log);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(ABOUT_WINDOW_QML_PATH);
        assert(windowComponent);
        connect(this, SIGNAL(signalRecivedAdapterData()),
                this, SLOT(slotRecievedAdapterData()));
        connect(windowComponent, SIGNAL(updateButtonClicked()),
                this, SLOT(slotUpdateButtonClicked()));
    }
    initAdapterStatusControl();
    initRecorderStatusControl();
    initRecorderSettingsControl();
    initUdateWindow();
    return nullptr;
}

QObject *AboutWindow::getComponentObject()
{
    return windowComponent;
}

std::string AboutWindow::getWindowName()
{
    return WINDOW_NAME;
}

std::string AboutWindow::getWindowAbsolutePath()
{
    return "";
}

std::string AboutWindow::getWindowFolderName()
{
    return "";
}

std::string AboutWindow::getIconPresentationName()
{
    return "";
}

std::string AboutWindow::getWindowFolderPresentationName()
{
    return "";
}

QPixmap AboutWindow::getIcon()
{
    return QPixmap();
}

std::pair<bool, QPixmap> AboutWindow::getFolderPixmap()
{
    return std::make_pair(false, QPixmap());
}

std::pair<bool, std::shared_ptr<IProcessWidget> > AboutWindow::getProcessWidget()
{
    return std::make_pair(false, nullptr);
}

bool AboutWindow::showWindow()
{
    updateData();
    return  true;
}

bool AboutWindow::hideWindow()
{
    return true;
}

EWindSizeType AboutWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool AboutWindow::prepareToDelete()
{
    return true;
}

void AboutWindow::initAdapterStatusControl()
{
    if( adapterStatusControl == nullptr )
    {
        adapterStatusControl = std::make_shared<AdapterStatusControl>(dataProvider, *logger.get());
    }
}

void AboutWindow::initRecorderStatusControl()
{
    if( recorderStatusControl == nullptr )
    {
        recorderStatusControl = std::make_shared<RecorderStatusControl>(dataProvider, *logger.get());
    }
}

void AboutWindow::initRecorderSettingsControl()
{
    if( recorderSettingsControl == nullptr )
    {
        recorderSettingsControl = std::make_shared<RecorderSettingsContol>(dataProvider, *logger.get());
    }
}

void AboutWindow::initUdateWindow()
{
    if( updateWindow == nullptr )
    {
        updateWindow = std::make_shared<UpdateProgramWindow>(userManipulators, engine,
                                                             logger, windowComponent);
        updateWindow->init();
    }
}



void AboutWindow::updateData()
{
    this->userManipulators->showLoadingWidget();
    updateWorker = std::async(std::launch::async, [this] () mutable
    {
        try
        {
            adapterStatusControl->updateStatus();
            adapterStatus = adapterStatusControl->getAdapterStatus();
            adapterAccStatus = adapterStatusControl->getAccStatus();
            recorderStatusControl->updateStatus();
            recorderStatus = recorderStatusControl->getRecorderStatus();
            recorderSettingsControl->updateStatus();
            recorderSettings = recorderSettingsControl->getRecordSettings();
            this->userManipulators->hideLoadingWidget();
            emit signalRecivedAdapterData();
        }
        catch (std::system_error &except)
        {
            this->userManipulators->hideLoadingWidget();
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            userManipulators->showMessage(std::string("Не удалось обновить данные.\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
        }
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}

void AboutWindow::slotRecievedAdapterData()
{

    QQuickItem *column = windowComponent->findChild<QQuickItem*>("Column");

    for( auto it: column->children() )
    {
        if( it->objectName() == "AdapterVersion" )
        {
            QQuickItem *item = it->findChild<QQuickItem*>("item");
            item->setProperty("value", QString(adapterStatus.version));
        }
        else if( it->objectName() == "AdapterVersionDate" )
        {
            QDateTime dateTime = QDateTime::fromSecsSinceEpoch(adapterStatus.datebuild);
            QQuickItem *item = it->findChild<QQuickItem*>("item");
            item->setProperty("value", dateTime.toString("hh:mm dd.MM.yyyy"));
        }
        else if( it->objectName() == "AzVersion" )
        {
            QQuickItem *item = it->findChild<QQuickItem*>("item");
//            item->setProperty("value", QString("1"));
            uint32_t *ver = (uint32_t *)&recorderStatus.Version;
            item->setProperty("value", QString::fromStdString(recorderLibPrintIniVersionDateBuild(ver)));

        }
        else if( it->objectName() == "AzDate" )
        {
            QQuickItem *item = it->findChild<QQuickItem*>("item");
//            item->setProperty("value", QString((char*)recorderStatus.Version.czDate));
            uint32_t *ver = (uint32_t *)&recorderStatus.Version;
            item->setProperty("value", QString::fromStdString(recorderLibPrintIniVersionDateString(ver)));
        }
        else if( it->objectName() == "SN" )
        {
            char sn[64];
            QQuickItem *item = it->findChild<QQuickItem*>("item");
            sprintf(sn,"%016lX",adapterStatus.sn);
            item->setProperty("value", QString(sn));
        }
        else if( it->objectName() == "Name" )
        {
            QTextCodec *codec = QTextCodec::codecForName("CP1251");
            QString name = codec->toUnicode(recorderSettings.Alias);
            QQuickItem *item = it->findChild<QQuickItem*>("item");
            item->setProperty("value", name);
        }
        else if( it->objectName() == "Capacity" )
        {
            QQuickItem *item = it->findChild<QQuickItem*>("item");
            item->setProperty("value", QString::number(adapterAccStatus.AccStatus.int_acc.acc_capacity / 10).append(" Ач"));
        }
        else if( it->objectName() == "Date" )
        {
           QString date = QString("мес %1 год %2").arg(
                                 QString::number(adapterAccStatus.AccStatus.int_acc.acc_manuf_mon)).arg(QString::number(adapterAccStatus.AccStatus.int_acc.acc_manuf_year));
           QQuickItem *item = it->findChild<QQuickItem*>("item");
           item->setProperty("value", date);
        }
    }
}

void AboutWindow::slotUpdateButtonClicked()
{
    updateWindow->show();
}

std::string AboutWindow::recorderLibPrintIniVersionDateString( uint32_t *ver )
{
    char vers_string[ 256 ];
    uint16_t year;
    uint8_t  mon;
    uint8_t  day;
    uint8_t  hour;
    uint8_t  min;
    uint8_t  sec;
    int cc;

    if( !ver )
    {
        return "";
    }

    sec  = ver[2] >> 0;
    min  = ver[2] >> 8;
    hour = ver[2] >> 16;

    day  = ver[1] >> 0;
    mon  = ver[1] >> 8;
    year = ver[1] >> 16;

    cc = snprintf( vers_string, sizeof( vers_string ), "%04u.%02u.%02u %02u:%02u:%02u",
                                                            year, mon, day, hour, min, sec );

    if( cc < 0 || cc >= sizeof( vers_string ) )
    {
        return "";
    }

    return vers_string;
}

std::string AboutWindow::recorderLibPrintIniVersionDateBuild( uint32_t *ver )
{
    char vers_string[ 256 ];
    int cc;

    if( !ver )
    {
        return "";
    }

    cc = snprintf( vers_string, sizeof( vers_string ), "g%x", ver[0] );

    if( cc < 0 || cc >= sizeof( vers_string ) )
    {
        return "";
    }

    return vers_string;
}
