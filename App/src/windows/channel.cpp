#include <assert.h>

#include "channel.h"

Channel::Channel(rosslog::Log &log, std::shared_ptr<IQmlEngine> qmlEngine, ChannelType type, int channelNum, std::string name,
                 std::shared_ptr<IUserManipulators> userManipulators,
                 std::shared_ptr<IChannelControl> channelControlInterface,
                 QObject *parent) :
    QObject(parent),
    logger(&log),
    qmlEngine(qmlEngine),
    type(type),
    channelNum(channelNum),
    name(name),
    userManipulators(userManipulators),
    channelControlInterface(channelControlInterface)
{
    assert(userManipulators);
    assert(parent);
}

Channel::~Channel()
{
    std::cerr << "Removed Channel" << std::endl;
}

void Channel::initChannel()
{
    QObject *currentObject;

    currentObject = qmlEngine->createComponent(CHANNEL_SINGLE_QML_PATH, this);

    assert(currentObject);

    channelItem = qobject_cast<QQuickItem*>(currentObject);

    //Have to find page's parent it is SwipeView in qml file. than we fill parent by pages
    QQuickItem *swipeViewItem = channelItem->findChild<QQuickItem*>("SwipeView");

    if( swipeViewItem == nullptr )
        throw std::runtime_error("Swipe view was not found");


    channelItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

    QObject::connect(channelItem, SIGNAL(updateParamsData()),
                     this, SLOT(slotUpdateParamsData()));

    QMetaObject::invokeMethod(channelItem, "setChannelName",
                              Q_ARG(QVariant, QString::fromStdString(name)));

    QString secondChannelName = "Канал " + QString::number(channelNum + 1);

    QMetaObject::invokeMethod(channelItem, "setSecondChannelName",
                              Q_ARG(QVariant, secondChannelName));

    channelStatusPage = std::shared_ptr<ChannelStatusPage>(new ChannelStatusPage(
                                                               shared_from_this(), qmlEngine, userManipulators, swipeViewItem));
    channelStatusPage->iniChannelStatusPage();

    channelParamsPage = std::shared_ptr<ChannelParamsPage>(new ChannelParamsPage(shared_from_this(), userManipulators,
                                              qmlEngine, swipeViewItem));
    channelParamsPage->iniChannelParamsPage();
}

void Channel::changeChannelType(ChannelType type)
{
    this->showChannel();

    if( channelItem == nullptr )
        throw std::runtime_error("Channel item is not inited");

    if( type != this->type )
    {
        QMetaObject::invokeMethod(channelItem, "setSingleState");
//        if( type == SINGLE_CHANNEL )
//            QMetaObject::invokeMethod(channelItem, "setSingleState");
//        else
//            QMetaObject::invokeMethod(channelItem, "setUnionState");
    }

    this->type = type;
}

void Channel::showChannel()
{
    if( channelItem == nullptr )
        throw std::runtime_error("Channel item is not inited");

    QMetaObject::invokeMethod(channelItem, "showChannel");
}

void Channel::hideChannel()
{
    if( channelItem == nullptr )
        throw std::runtime_error("Channel item is not inited");

    QMetaObject::invokeMethod(channelItem, "hideChannel");
}

void Channel::channelDataSetted(std::string paramName)
{
    this->channelParamsPage->paramsDataSetted(paramName);
}

void Channel::changeUnionParams(ChannelSettingsParams settParams)
{
    this->channelParamsPage->changeUnionParams(settParams);
}

void Channel::changePairParam(ChannelSettingsParams settParams)
{
    this->channelParamsPage->changePairParam(settParams);
}

void Channel::prepareToDelete()
{
    channelStatusPage = nullptr;
    channelParamsPage = nullptr;
}

ChannelType Channel::getChannelType()
{
    return type;
}

void Channel::updateStatusParams(ChannelStatusParams params)
{
    if( channelStatusPage == nullptr )
        throw std::runtime_error("Channel status page is not inited");

    channelStatusPage->setStatusParams(params);

    QMetaObject::invokeMethod(channelItem, "setChannelEnabled", Q_ARG(QVariant, params.enabled));
}

void Channel::setNewSettingParams(ChannelSettingsParams settingParams)
{
    channelControlInterface->setNewSettingsParams(channelNum, type, settingParams);
}

void Channel::unionParamsChanged(ChannelSettingsParams settingsParams)
{
    channelControlInterface->unionParamsChanged(channelNum, type, settingsParams);
}

void Channel::pairParamChanged(ChannelSettingsParams settingsParams)
{
    channelControlInterface->pairParamChanged(channelNum, settingsParams);
}

void Channel::startRecord()
{
    channelControlInterface->startRecord(channelNum, type);
}

void Channel::stopRecord()
{
    channelControlInterface->stopRecord(channelNum, type);
}

void Channel::startMonitor()
{
    channelControlInterface->startMonitor(channelNum, type);
}

void Channel::stopMonitor()
{
    channelControlInterface->stopMonitor(channelNum, type);
}

bool Channel::getErasureStatus()
{
    return channelControlInterface->getErasureStatus();
}

//Signal from Channel.qml
void Channel::slotUpdateParamsData()
{
    channelParamsPage->setChannelSettingsParams(settingsParams);
}


void Channel::updateSettingsParams(ChannelSettingsParams params)
{
    if( channelParamsPage == nullptr )
        throw std::runtime_error("Channel params page is not inited");
    settingsParams = params;
}

