#include <assert.h>
#include <iostream>

#include <QQmlComponent>
#include <QQmlContext>

#include "mainmenuwindow.h"

using namespace rosslog;

MainMenuWindow::MainMenuWindow(QObject *parent) :
    QObject(parent),
    logger(nullptr),
    windowComponent(nullptr)
{
}

std::shared_ptr<IUserManipulators> MainMenuWindow::initWindow(EWindSizeType winSizeType,
                                                              std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                              std::shared_ptr<IStateWidget> stateWidget,
                                                              std::shared_ptr<IUserManipulators> userManipulators,
                                                              std::shared_ptr<IQmlEngine> engine,
                                                              std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(dataProvider);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);

    assert(engine);
    assert(log);

    this->engine = engine;
    this->logger = log;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent("qrc:/MainMenu.qml");
        assert(windowComponent);
    }

    return nullptr;
}

void MainMenuWindow::initMainMenu(std::shared_ptr<PluginManager> pluginManager)
{

    QQuickItem *rowItem = windowComponent->findChild<QQuickItem*>("MenuRow");
    assert(rowItem);

    for( auto kv: pluginManager->getPlugins() )
    {
        IEmptyPlaceWindow *emptyWindow = kv.second.get();
        std::string folderName = emptyWindow->getWindowFolderName();
        std::string buttonName = emptyWindow->getWindowName();
        std::string iconName = emptyWindow->getIconPresentationName();
        std::string folderPresentationName = emptyWindow->getWindowFolderPresentationName();

        auto it = folders.find(folderName);

        if( it != folders.end() )
        {
            std::shared_ptr<MenuButton> button = std::shared_ptr<MenuButton>(
                        new MenuButton(engine, buttonName, shared_from_this(),
                                       iconName, emptyWindow->getIcon(), it->second->getFolderRowItem()));
            button->init(false);
            it->second->addItemToFolder(button);
        }
        else
        {
            if( folderName.empty() )
            {
                std::shared_ptr<MenuButton> menuButton = std::shared_ptr<MenuButton>(
                            new MenuButton(engine, buttonName, shared_from_this(),
                                           iconName, emptyWindow->getIcon(), rowItem));
                menuButton->init(false);
                menuItems[buttonName] = menuButton;
                continue;
            }
            std::shared_ptr<MenuFolder> folder = std::shared_ptr<MenuFolder>(new MenuFolder(engine, folderName,
                                                shared_from_this(), folderPresentationName,
                                                emptyWindow->getFolderPixmap().second, windowComponent));
            folder->init();

            std::shared_ptr<MenuButton> menuButton = std::shared_ptr<MenuButton>(
                        new MenuButton(engine, folderName, shared_from_this(),
                                       folderPresentationName, emptyWindow->getFolderPixmap().second, rowItem));

            //It is folder button we need this info for design
            menuButton->init(true);

            std::shared_ptr<MenuButton> button =
                    std::shared_ptr<MenuButton>(menuButton);


            QMetaObject::invokeMethod(windowComponent, "addFolder",
                                              Q_ARG(QVariant, QString::fromStdString(folderName)),
                                              Q_ARG(QVariant, QVariant::fromValue(folder->getObjectItem())));
            menuItems[folderName] = menuButton;


            std::shared_ptr<MenuButton> menuSubButton = std::shared_ptr<MenuButton>(
                        new MenuButton(engine, buttonName, shared_from_this(),
                                       iconName, emptyWindow->getIcon(), folder->getFolderRowItem()));

            menuSubButton->init(false);


            std::shared_ptr<MenuButton> sbButton =
                    std::shared_ptr<MenuButton>(menuSubButton);
            folder->addItemToFolder(sbButton);
            menuItems[buttonName] = sbButton;
            folders[folderName] = folder;
        }

        this->logger->Message(LOG_INFO, __LINE__, __func__, "Window name =  %s",
                              kv.first.c_str());
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Folder name =  %s",
                              folderName.c_str());
    }
}

QObject *MainMenuWindow::getComponentObject()
{
    return windowComponent;
}

std::string MainMenuWindow::getWindowName()
{
    return MENU_WINDOW_IDENTIFICATOR;
}

std::string MainMenuWindow::getWindowAbsolutePath()
{
    return "";
}

std::string MainMenuWindow::getWindowFolderName()
{
    return "";
}

std::string MainMenuWindow::getIconPresentationName()
{
    return "";
}

std::string MainMenuWindow::getWindowFolderPresentationName()
{
    return "";
}

QPixmap MainMenuWindow::getIcon()
{
    return QPixmap();
}

std::pair<bool, QPixmap> MainMenuWindow::getFolderPixmap()
{
    return std::pair<bool, QPixmap>(false, QPixmap());
}

std::pair<bool, std::shared_ptr<IProcessWidget> > MainMenuWindow::getProcessWidget()
{
    return std::make_pair(false, nullptr);
}

bool MainMenuWindow::showWindow()
{
    return true;
}

bool MainMenuWindow::hideWindow()
{
    return true;
}

EWindSizeType MainMenuWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool MainMenuWindow::prepareToDelete()
{
    return true;
}

void MainMenuWindow::itemPress(std::shared_ptr<IMenuItemWidget> menuItemWidget)
{
    auto it = menuItems.find(menuItemWidget->getItemName());

    if( it != menuItems.end() && menuItemWidget->getItemType() == FOLDER )
    {
        QMetaObject::invokeMethod(windowComponent, "showFolder",
                                          Q_ARG(QVariant, QString::fromStdString(menuItemWidget->getItemName())));
    }
    else
    {
        emit signalOpenWindow(menuItemWidget->getItemName());
    }
}

void MainMenuWindow::itemRelease(std::shared_ptr<IMenuItemWidget> menuItemWidget)
{
    Q_UNUSED(menuItemWidget);
}

void MainMenuWindow::openEvent(std::shared_ptr<IMenuItemWidget> menuItemWidget)
{
    Q_UNUSED(menuItemWidget);
}

