#ifndef CHANNELSTATUSPAGE_H
#define CHANNELSTATUSPAGE_H

#include <memory>
#include <iostream>

#include <QObject>
#include <QQuickItem>
#include <QVariant>

#include "iusermanipulators.h"
#include "iqmlengine.h"
#include "channelmodel.h"
#include "ichannel.h"


typedef struct
{
    ChannelType type;
    bool enabled;
    std::string recordSource;
    ChannelState channelState;
} ChannelStatusParams;


#define CHANNEL_STATUS_PAGE_QML "qrc:/ChannelStatusPage.qml"

class ChannelStatusPage : public QObject
{
    Q_OBJECT
public:
    explicit ChannelStatusPage(std::shared_ptr<IChannel> channelInterface,
                               std::shared_ptr<IQmlEngine> qmlEngine,
                               std::shared_ptr<IUserManipulators> userManipulators,
                               QObject *parent = nullptr);
    ~ChannelStatusPage();

    void iniChannelStatusPage();
    void setStatusParams(ChannelStatusParams params);

private:
    std::shared_ptr<IChannel> channelInterface;
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    ChannelStatusParams statusParams;
    std::shared_ptr<IUserManipulators> userManipulators;

signals:

public slots:
    void slotRecordButtonClicked();
    void slotMonitoringButtonClicked();
};

#endif // CHANNELSTATUSPAGE_H
