#ifndef MAINMENUWINDOW_H
#define MAINMENUWINDOW_H

#include <QObject>
#include <QQuickItem>

#include <rosslog/log.h>

#include "iemptyplacewindow.h"
#include "pluginmanager.h"
#include "menufolder.h"
#include "menubutton.h"

#include "imenuitemeventshandler.h"
#include "imenuitemwidget.h"
#include "iqmlengine.h"

class IMenuItemWidget;

#define MENU_WINDOW_IDENTIFICATOR "MainMenu"
#define MENU_COMPONENT_CLASS "MainMenuClass"

class MainMenuWindow : public QObject, public IEmptyPlaceWindow, public IMenuItemEventsHandler,
        public std::enable_shared_from_this<MainMenuWindow>
{
    Q_OBJECT
public:
    explicit MainMenuWindow(QObject *parent = nullptr);

    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;

    void initMainMenu(std::shared_ptr<PluginManager> pluginManager);

    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;


protected:
    void itemPress(std::shared_ptr<IMenuItemWidget> menuItemWidget) override;
    void itemRelease(std::shared_ptr<IMenuItemWidget> menuItemWidget) override;
    void openEvent(std::shared_ptr<IMenuItemWidget> menuItemWidget) override;

private:
    std::shared_ptr<rosslog::Log> logger;
    QObject *windowComponent;
    std::map<std::string, std::shared_ptr<MenuFolder>> folders;
    std::map<std::string, std::shared_ptr<IMenuItemWidget>> menuItems;

    std::shared_ptr<IQmlEngine> engine;
    QQuickItem rootItem;

signals:
    void signalOpenWindow(std::string name);

};

#endif // MAINMENUCONSTRUCT_H
