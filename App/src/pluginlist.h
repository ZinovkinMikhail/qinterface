#ifndef PLUGINLIST_H
#define PLUGINLIST_H

#include <string>
#include <list>

class PluginList
{
public:
    PluginList(std::string pluginsPath);
    std::list<std::string> getPluginList();

private:
    std::list<std::string> plugins;
    std::string pluginsPath;

};

#endif // PLUGINLIST_H
