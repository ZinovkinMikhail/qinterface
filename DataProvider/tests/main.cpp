#include <getopt.h>

#include <QCoreApplication>


#include <signal.h>
#include <memory>

#include "dataprovider.h"
#include <rosslog/log.h>


#include "serveremulator.h"



#ifdef TESTS
#include "testdatasourceconnect.h"
#include "testdataprovider.h"
#include "testinternal.h"
#include "testinternal.h"
#include "testinternalmaster.h"
#include "testdatawaiter.h"
#endif

#define _STR(x) #x
#define STRINGIFY(x)  _STR(x)

using namespace rosslog;

#define LOG_PATH "/tmp/cm_server"
#define LOG_APP_NAME "cm_server"
#define LOG_LEVEL 8
#define LOG_ENABLE_CONSOLE true

void SignalHandler ( int sig )
{
    printf("Caught signal %d\n", sig);
    printf("Closing app\n");
    if( sig != SIGTERM || sig != SIGTERM )
        exit(-1);
}

static void catch_signals ( void )
{
    signal ( SIGINT, SignalHandler );
    signal ( SIGILL, SignalHandler );
    signal ( SIGABRT, SignalHandler );
    signal ( SIGTERM, SignalHandler );
    signal ( SIGSEGV, SignalHandler );
    signal ( SIGPIPE, SignalHandler );
    signal ( SIGUSR1, SignalHandler );
    signal ( SIGUSR2, SignalHandler );
    signal ( SIGQUIT, SignalHandler );
    signal ( SIGKILL, SignalHandler );
    signal ( SIGSTOP, SignalHandler );
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    catch_signals();

    std::shared_ptr<rosslog::Log> logger = std::make_shared<rosslog::Log>(LOG_PATH, LOG_APP_NAME, LOG_LEVEL, LOG_ENABLE_CONSOLE);
    std::shared_ptr<data_provider::DataProvider> dataProvider =
            std::make_shared<data_provider::DataProvider>(*logger.get());

#ifdef TESTS
printf("======= COMMIT VERSION = " STRINGIFY(GIT_VERSION) "=======\n\n");
#endif

#ifdef SERVER_EMULATOR

    std:shared_ptr<ServerEmulator> serverEmulator = std::make_shared<ServerEmulator>(dataProvider, logger);

    serverEmulator->init();
    serverEmulator->start();
    return a.exec();
#endif

#ifdef TESTS
#ifdef MASTER
    if( QTest::qExec(new TestInternalMaster(dataProvider, *logger.get(), INTERNAL_TEST_ALL)) != 0)
        return 0;
#else
    if( QTest::qExec(new TestInternal(dataProvider, *logger.get(), INTERNAL_TEST_ALL)) != 0)
        return 0;

    qDebug("=============================================================================================");
#endif
    if( QTest::qExec(new TestDataProvider(dataProvider, *logger.get())) != 0 )
        return 0;
    qDebug("=============================================================================================");
    if( QTest::qExec(new TestDataSourceConnect(*logger.get())) != 0 )
        return 0;
    qDebug("=============================================================================================");

    getchar();
    getchar();
#else

    return a.exec();
#endif
    return 0;
}
