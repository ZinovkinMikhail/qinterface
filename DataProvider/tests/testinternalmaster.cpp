#include <unistd.h>

#include "testinternalmaster.h"

TestInternalMaster::TestInternalMaster(std::shared_ptr<DataProvider> dataProvider, QObject *parent) :
    QObject(parent),
    dataProvider(dataProvider)
{}

TestInternalMaster::~TestInternalMaster()
{}

void TestInternalMaster::testStartServer()
{
    QVERIFY(dataProvider);
    QVERIFY(dataProvider->startConnection());
}

void TestInternalMaster::testServerConnection()
{
    QVERIFY(dataProvider);
    int testCounter = 5;
    bool connectionState = false;
    while(testCounter)
    {
        connectionState = dataProvider->getConnectStatus();
        testCounter--;
        usleep(10000);
    }
    QVERIFY(connectionState);
}

void TestInternalMaster::testMasterWork()
{
    QVERIFY(dataProvider);
    int sendCommandCoutner = 2;
    while( sendCommandCoutner )
    {
        QVERIFY(dataProvider->getConnectStatus());

        if( sendCommandCoutner > 0 )
        {
            testInterrogationCMD();
        }
        sendCommandCoutner--;
        sleep(2);
    }
}

void TestInternalMaster::testStopServer()
{
}


bool TestInternalMaster::testInterrogationCMD()
{
    if( dataProvider == nullptr )
        return false;

    uint16_t buff[AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE];
    size_t size = 0;
    std::vector<uint8_t> vect;
//    SoftRecordeExchangeStruct_t str;
    SocketData sockClient;

    memset(buff, 0, AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE * sizeof(uint16_t));
    buff[0] = AXIS_DISPATCHER_EXCHANGE__ID;
    size += 2;
    buff[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET] = 0x0000;
    buff[ AXIS_SOFTRECORDER_EXCHANGE__CMD_WORD_OFFSET ] = 0x5901;
    buff[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET] = axis_crc16( buff, AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE );
    size += 2;

    vect.reserve(AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE * 2);
    uint8_t *data = reinterpret_cast<uint8_t*>(&buff);
    std::copy(&data[0], &data[AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE * 2], std::back_inserter(vect));

    try
    {
        //dataProvider->sendCommand(0x5901, vect, size, true);
    }
    catch( std::runtime_error &except )
    {
        return false;
    }
    return true;
}
