#ifndef TESTDATAPROVIDER_H
#define TESTDATAPROVIDER_H

#include <QObject>
#include <QTest>

#include "dataprovider.h"
#include "testscommon.h"


class TestDataProvider : public QObject
{
    Q_OBJECT
public:
    explicit TestDataProvider(std::shared_ptr<data_provider::DataProvider> dataProvider,
                              rosslog::Log &log, QObject *parent = nullptr);
    ~TestDataProvider();

private:
    std::shared_ptr<data_provider::DataProvider> dataProvider;

signals:

private slots:
    void testDataProviederAddDataWaiter();
    void testDataProviderRemoveAddedDataWaiter();
    void testDataProviederRemoveAddedAndNotAdded();
};

#endif // TESTDATAPROVIDER_H
