#ifndef DATAPROVIDERTESTS_H
#define DATAPROVIDERTESTS_H

#include <QObject>
#include <QTest>

#include <rosslog/log.h>

#include "dataprovider.h"

using namespace data_provider;

class DataProviderTests : public QObject
{
    Q_OBJECT
public:
    explicit DataProviderTests(std::shared_ptr<DataProvider> dataProvider, QObject *parent = nullptr);
    bool runTests();

signals:

private:
    std::shared_ptr<DataProvider> dataProvider;

public slots:
};

#endif // DATAPROVIDERTESTS_H
