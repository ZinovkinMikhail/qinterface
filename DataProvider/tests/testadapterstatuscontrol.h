#ifndef TESTADAPTERSTATUSCONTROL_H
#define TESTADAPTERSTATUSCONTROL_H

#include <QObject>
#include <QTest>
#include <rosslog/log.h>

#include "adapterstatuscontrol.h"
#include "dataprovider.h"
#include "testscommon.h"


typedef enum
{
    ADAPTER_STATUS_TEST_ALL,
    ADAPTER_STATUS_TEST_GET_STATUS
} AdapterStatusTestType;

class TestAdapterStatusControl : public QObject
{
    Q_OBJECT
public:
    explicit TestAdapterStatusControl(std::shared_ptr<data_provider::DataProvider> dataProvider,
                                      rosslog::Log &log,AdapterStatusTestType testType, QObject *parent = nullptr);


private:
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    adapter_status_ctrl::AdapterStatusControl *adapterStatusControl;
    rosslog::Log *logger;
    AdapterStatusTestType testType;
signals:

private slots:
    void testAdapterStatusControlUpdateStatus();
    void testAdapterStatusControlReboot();
    void testAdapterStatusControlLPC();
    void testAdapterStatusControlSetBright();
    void testAdapterStatusControlLockKeyboard();
};

#endif // TESTADAPTERSTATUSCONTROL_H
