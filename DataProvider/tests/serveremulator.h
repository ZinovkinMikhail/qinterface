#ifndef SERVEREMULATOR_H
#define SERVEREMULATOR_H

#include <QObject>
#include <QTextCodec>

#include <memory>
#include <thread>
#include <map>
#include <bitset>
#include <sys/time.h>

#include <rosslog/log.h>
#include <axis_softadapter.h>

#include "dataprovider.h"
#include "adaptercommands.h"

#define CHANNEL1_ENABLE_POS 0
#define CHANNEL2_ENABLE_POS 1
#define CHANNEL3_ENABLE_POS 2
#define CHANNEL4_ENABLE_POS 3
#define CHANNEL1_STEREO_POS 8
#define CHANNEL3_STEREO_POS 9

#define CHANNEL_1_LPF_ENABLE 8
#define CHANNEL_2_LPF_ENABLE 9
#define CHANNEL_3_LPF_ENABLE 10
#define CHANNEL_4_LPF_ENABLE 11

using namespace data_provider;

class ServerEmulator : public QObject , public IDataWaiter, public enable_shared_from_this<ServerEmulator>
{
    Q_OBJECT
public:
    explicit ServerEmulator(std::shared_ptr<DataProvider> dataProvider,
                            std::shared_ptr<rosslog::Log> rosslog, QObject *parent = nullptr);

    void init();
    void start();

protected:
    void recieveData(int command, std::vector<uint8_t> data,
                        size_t size) override;

private:
    void threadFunction();

    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<DataProvider> dataProvider;
    std::thread statusThread;
    std::condition_variable cv;
    std::mutex cv_m;
    std::mutex dataMutex;
    bool conditionWork;
    bool threadWork;
    std::vector<uint8_t> answerData;
    int commandSent;
    uint16_t lastTime;

    SoftRecorderSettingsStruct_t recorderSettings;
    SoftRecorderStatusStruct_t recorderStatus;
    SoftRecorderGetAccStateStruct_t accStatus;
    SoftAdapterDeviceCurrentStatus_t adapterStatus;
    SoftAdapterSettingsStruct_t adapterSettings;
    DspRecorderExchangeTimerSettingsStruct_t recorderTimers;
    DspRecorderExchangeTimerSettingsStruct_t adapterTimers;

signals:

};

#endif // SERVEREMULATOR_H
