#ifndef TESTRECORDERSETTINGSCONTROL_H
#define TESTRECORDERSETTINGSCONTROL_H

#include <QObject>
#include <QTest>

#include <rosslog/log.h>

#include "recordersettingscontol.h"
#include "dataprovider.h"
#include "testscommon.h"

class TestRecorderSettingsControl : public QObject
{
    Q_OBJECT
public:
    explicit TestRecorderSettingsControl(std::shared_ptr<DataProvider> dataProvider,
                                         rosslog::Log &log,
                                         QObject *parent = nullptr);
private:
    std::shared_ptr<DataProvider> dataProvider;
    RecorderSettingsContol *recorderSettingsControl;
    rosslog::Log *logger;
signals:

private slots:
    void testRecorderSettingsUpdate();
    void testRecordSettingsSave();
    void testRecorderSettingsUpdateTimers();
    void testSetRecorderSettingsTimers();
};

#endif // TESTRECORDERSETTINGSCONTROL_H
