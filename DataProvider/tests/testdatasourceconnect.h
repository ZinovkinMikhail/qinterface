#ifndef TESTDATASOURCECONNECT_H
#define TESTDATASOURCECONNECT_H


#include <QObject>
#include <QtTest/QTest>

#include <axis_crc.h>

#include "datasourceconnect.h"
#include "testscommon.h"

class TestDataSourceConnect : public QObject
{
    Q_OBJECT
public:
    explicit TestDataSourceConnect(rosslog::Log &log, QObject *parent = nullptr);
    ~TestDataSourceConnect();

private:
    DataSourceConnect *dataSource;

signals:

private slots:
    void testMultiBlocks();
    void testSingleBlock();

public slots:
};

#endif // TESTDATASOURCECONNECT_H
