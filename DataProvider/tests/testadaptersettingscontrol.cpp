#include "testadaptersettingscontrol.h"

using namespace rosslog;

TestAdapterSettingsControl::TestAdapterSettingsControl(std::shared_ptr<DataProvider> dataProvider,
                                                       rosslog::Log &log, QObject *parent) :
    QObject(parent),
    dataProvider(dataProvider),
    adapterSettingsControl(nullptr),
    logger(&log)
{
    adapterSettingsControl = new AdapterSettingsControl(dataProvider, log);

    QVERIFY(dataProvider);
    QVERIFY(dataProvider->getConnectStatus());
}

void TestAdapterSettingsControl::testUpdateAdapterSettingsControl()
{
    QVERIFY(dataProvider);
    QVERIFY(adapterSettingsControl);
    QVERIFY(dataProvider->getConnectStatus());
    bool result = false;
    try
    {
        SoftAdapterExchangeSettingsStruct_t adapterSettings;
        adapterSettingsControl->updateSettings();

        adapterSettings = adapterSettingsControl->getAdapterSettings();
        result = true;

        this->logger->Message(LOG_INFO, __LINE__, __func__, "Recived Adapter settings block 0 connect = %i",
                                  adapterSettings.Block0.AdapterSettings.Connect);

        QVERIFY(adapterSettings.Block0.AdapterSettings.Connect == 231);
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
    QVERIFY(result);
}

void TestAdapterSettingsControl::testUpdateAdapterTimers()
{
    QVERIFY(dataProvider);
    QVERIFY(adapterSettingsControl);
    QVERIFY(dataProvider->getConnectStatus());
    bool result = false;

    try
    {
        DspRecorderExchangeTimerSettingsStruct_t adapterTimers;
        adapterSettingsControl->updateTimers();

        adapterTimers = adapterSettingsControl->getAdapterTimers();
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Recieved after send data TIMERS 1 min = %i Timers 4 reserv = %i",
                              adapterTimers.Timers[0].start.min , adapterTimers.Timers[4].stop.reserv_0);

        QVERIFY(adapterTimers.Timers[4].stop.reserv_0 == 222);
        result = true;
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
    QVERIFY(result);
}

void TestAdapterSettingsControl::testSaveAdapterSettingsControl()
{
    QVERIFY(dataProvider);
    QVERIFY(adapterSettingsControl);
    QVERIFY(dataProvider->getConnectStatus());

    bool result = false;
    try
    {
        SoftAdapterExchangeSettingsStruct_t settings;
        settings.Block0.AdapterSettings.Connect = 7878;
        adapterSettingsControl->saveAdapterSettings(settings.Block0.AdapterSettings);
        result = true;

        memset(&settings, 0, sizeof(SoftAdapterExchangeSettingsStruct_t));
        settings = adapterSettingsControl->getAdapterSettings();
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Recived Adapter settings block 0 connect = %i",
                              settings.Block0.AdapterSettings.Connect);
        QVERIFY(settings.Block0.AdapterSettings.Connect == 7878);
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
    QVERIFY(result);
}

void TestAdapterSettingsControl::testSaveAdapterTimers()
{
    QVERIFY(dataProvider);
    QVERIFY(adapterSettingsControl);
    QVERIFY(dataProvider->getConnectStatus());
    bool result = false;
    int timerTestMin = 222;
    try
    {
        DspRecorderExchangeTimerSettingsStruct_t settings;
        settings.Timers[0].start.min = timerTestMin;
        settings.Timers[4].stop.reserv_0 = 111;

        adapterSettingsControl->saveAdapterTimers(settings);

        memset(&settings.Timers, 0 ,sizeof(settings.Timers));
        settings = adapterSettingsControl->getAdapterTimers();

        this->logger->Message(LOG_INFO, __LINE__, __func__, "Recieved after save data TIMERS 1 min = %i Timers 4 reserv = %i",
                              settings.Timers[0].start.min , settings.Timers[4].stop.reserv_0);
        QVERIFY(settings.Timers[0].start.min == timerTestMin);
        result = true;
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
    QVERIFY(result);
}
