#include <assert.h>

#include "dataprovidertests.h"
#include "dataprovidererror.h"

#include "testdatasourceconnect.h"
#include "testdataprovider.h"
#include "testinternal.h"
#include "testinternal.h"
#include "testinternalmaster.h"
#include "testdatawaiter.h"
#include "testadapterstatuscontrol.h"
#include "testrecorderstatuscontrol.h"
#include "testrecordersettingscontrol.h"
#include "testadaptersettingscontrol.h"

DataProviderTests::DataProviderTests(std::shared_ptr<DataProvider> dataProvider, QObject *parent) :
    QObject(parent),
    dataProvider(dataProvider)
{}

bool DataProviderTests::runTests()
{
    assert(dataProvider);
    rosslog::Log *logger = new rosslog::Log(TEST_LOG_PATH, TEST_LOG_APP_NAME, TEST_LOG_LEVEL, true);

#ifdef MASTER
    Q_UNUSED(logger);
    if( QTest::qExec(new TestInternalMaster(dataProvider)) != 0)
        return false;
#else
    if( QTest::qExec(new TestInternal(dataProvider, *logger, INTERNAL_TEST_CONNECTION)) != 0)
    {
        return false;
    }
//    return true;
    qDebug("=============================================================================================");
    if( QTest::qExec(new TestDataProvider(dataProvider, *logger)) != 0 )
        return false;
    qDebug("=============================================================================================");
    if( QTest::qExec(new TestDataSourceConnect(*logger)) != 0 )
        return false;
    qDebug("=============================================================================================");
    if( QTest::qExec(new TestAdapterStatusControl(dataProvider, *logger, ADAPTER_STATUS_TEST_ALL)) != 0 )
    {
        return false;
    }
    qDebug("=============================================================================================");
    if( QTest::qExec(new TestRecorderStatusControl(dataProvider, *logger, RECORDER_TEST_ALL)) != 0 )
        return false;
    qDebug("=============================================================================================");
    if( QTest::qExec(new TestRecorderSettingsControl(dataProvider, *logger)) != 0 )
        return false;
    qDebug("=============================================================================================");
    if( QTest::qExec(new TestAdapterSettingsControl(dataProvider, *logger)) != 0 )
        return false;


#endif
    return true;
}


