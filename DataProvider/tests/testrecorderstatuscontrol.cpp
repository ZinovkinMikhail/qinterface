#include "testrecorderstatuscontrol.h"

using namespace rosslog;
using namespace recorder_status_ctrl;

TestRecorderStatusControl::TestRecorderStatusControl(std::shared_ptr<DataProvider> dataProvider,
                                                     rosslog::Log &log, RecorderStatusTestType testType,
                                                     QObject *parent) :
    QObject(parent),
    dataProvider(dataProvider),
    recorderStatusControl(nullptr),
    logger(&log),
    testType(testType)
{
    recorderStatusControl = new RecorderStatusControl(dataProvider, log);
    QVERIFY(dataProvider);
    QVERIFY(dataProvider->getConnectStatus());

}

void TestRecorderStatusControl::testRecorderStatusUpdate()
{
    QVERIFY(dataProvider);
    QVERIFY(logger);
    QVERIFY(dataProvider->getConnectStatus());
    QVERIFY(recorderStatusControl);

    if( testType != RECORDER_TEST_ALL && testType != RECORDER_TEST_STATUS_UPDATE )
    {
        return;
    }

    bool result = false;
    int sendCounter = 5;
    try
    {
        while(sendCounter)
        {
            this->recorderStatusControl->updateStatus();
            SoftRecorderStatusStruct_t status = this->recorderStatusControl->getRecorderStatus();

            this->logger->Message(LOG_INFO, __LINE__, __func__, "Recieved after send data Alarm 1 = %x Total Rec Secs = %li",
                                  status.Alarm1, status.TotalRecSecs);

            QVERIFY(status.Alarm1 == 0xff00);

            sleep(1);
            sendCounter--;
        }

        result = true;
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while updating Recorder status");
    }
    QVERIFY(result);
}

void TestRecorderStatusControl::testRecorderEraseInternalMemory()
{
    if( testType != RECORDER_TEST_ALL )
    {
        return;
    }

    QVERIFY(dataProvider);
    QVERIFY(logger);
    QVERIFY(dataProvider->getConnectStatus());

    bool result = false;
    try
    {
        this->recorderStatusControl->eraseInternalMemory(false);
        result = true;
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while erase internal memory");
    }
    QVERIFY(result);
}

void TestRecorderStatusControl::testStopCopyCommand()
{
    if( testType != RECORDER_TEST_ALL )
    {
        return;
    }
    QVERIFY(dataProvider);
    QVERIFY(logger);
    QVERIFY(dataProvider->getConnectStatus());

    bool result = false;
    try
    {
        this->recorderStatusControl->stopCopy();
        result = true;
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while erase internal memory");
    }
    QVERIFY(result);
}
