#include "serveremulator.h"

using namespace data_provider;
using namespace rosslog;

ServerEmulator::ServerEmulator(std::shared_ptr<DataProvider> dataProvider,
                               std::shared_ptr<rosslog::Log> rosslog, QObject *parent) :
    QObject(parent),
    logger(rosslog),
    dataProvider(dataProvider)
{

}

void ServerEmulator::init()
{
    commandSent = 0;
//HERE SET DEFAULT DATA
    memset(&recorderSettings, 0, sizeof(recorderSettings));
    answerData.resize(1024);

    lastTime = 0;

    memset(&recorderStatus, 0, sizeof (SoftRecorderStatusStruct_t));
    memset(&accStatus, 0, sizeof (SoftRecorderGetAccStateStruct_t));
    memset(&accStatus, 0, sizeof (SoftAdapterDeviceCurrentStatus_t));
    memset(&adapterSettings, 0, sizeof (SoftAdapterSettingsStruct_t));
    memset(&recorderTimers, 0, sizeof (DspRecorderExchangeTimerSettingsStruct_t));
    memset(&adapterTimers, 0, sizeof (DspRecorderExchangeTimerSettingsStruct_t));
    adapterSettings.Connect = 96;
    adapterSettings.ControlPort = 8443;
    adapterSettings.NetMask = 4294967040;
    adapterSettings.Power = 13;


    snprintf(adapterSettings.AdapterUser, 64, "%s", "12");
    snprintf(adapterSettings.AdapterPasswd, 64, "%s", "jnltk");
    adapterSettings.ServerIPAddress = 3232236187;

    snprintf(adapterSettings.VPNPasswd, 64, "%s", "internet");
    snprintf(adapterSettings.VPNServer,  32, "%s", "#777");
    snprintf(adapterSettings.VPNUser,  32, "%s", "mobile");
    adapterSettings.HWAddress[0] = 0x40;
    adapterSettings.HWAddress[1] = 0x8D;
    adapterSettings.HWAddress[2] = 0x5C;
    adapterSettings.HWAddress[3] = 0x53;
    adapterSettings.HWAddress[4] = 0xEA;
    adapterSettings.HWAddress[5] = 0x87;
    adapterSettings.ControlPort = 443;
    adapterSettings.UploadPort = 80;
    adapterSettings.MonitoringPort = 17951;
  //  snprintf(adapterSettings.HWAddress, ETH_ALEN, "40:8D:5C:53:EA:87");


    accStatus.AccStatus.int_acc.acc_full_voltage = 42;
    accStatus.AccStatus.int_acc.acc_low_voltage = 37;
    accStatus.AccStatus.int_acc.acc_voltage = 4196;
    accStatus.AccStatus.int_acc.acc_temperature = 30;
    accStatus.AccStatus.int_acc.acc_rem_capacity = 99;
    accStatus.AccStatus.int_acc.acc_present = 161;
    accStatus.AccStatus.int_acc.acc_manuf_year = 2020;
    accStatus.AccStatus.int_acc.acc_manuf_mon = 3;
    accStatus.AccStatus.int_acc.acc_capacity = 110;
    accStatus.AccStatus.int_acc.acc_dc_resolut = 32;

    adapterStatus.datebuild = 1596293033;
    adapterStatus.sn = 445926435685777472;
    snprintf(adapterStatus.version, sizeof (adapterStatus.version), "%s", "1.0-20-07-20-800-gad352f");
    adapterStatus.recorderscount = 1;


    std::bitset<16> bias(recorderSettings.ChanelBias);
    bias.set(CHANNEL1_ENABLE_POS);
    bias.set(CHANNEL2_ENABLE_POS);
    std::bitset<16> filtr(recorderSettings.ChanelAGCLPFMode);
    filtr.set(CHANNEL_3_LPF_ENABLE);
    filtr.set(CHANNEL_4_LPF_ENABLE);

    std::bitset<16> cfg(recorderSettings.ChanelCfgMode);
    cfg.set(CHANNEL1_STEREO_POS);
    cfg.set(CHANNEL1_ENABLE_POS);
    cfg.set(CHANNEL2_ENABLE_POS);
    cfg.set(CHANNEL3_ENABLE_POS);
    cfg.set(CHANNEL4_ENABLE_POS);

    QTextCodec *codec = QTextCodec::codecForName("CP1251");
    QString name = QString("ЯНТРА-4Ц");

    char str[64];
    memset(&str, 0 , sizeof(str));
    strncpy(str, codec->fromUnicode(name).toStdString().c_str(), sizeof(str));
    memcpy(&recorderSettings.Alias, (uint16_t*)str, 64);



    recorderSettings.ChanelCfgMode = cfg.to_ulong();
    recorderSettings.ChanelBias = bias.to_ulong();
    recorderSettings.ChanelAGCLPFMode = filtr.to_ulong();
    recorderSettings.Chanel1AudioBand = 4;
    recorderSettings.Chanel1AudioCommpression = 1;
    recorderSettings.Chanel2AudioBand = 4;
    recorderSettings.Chanel2AudioCommpression = 1;
    recorderSettings.Chanel3AudioBand = 4;
    recorderSettings.Chanel3AudioCommpression = 1;
    recorderSettings.Chanel4AudioBand = 4;
    recorderSettings.Chanel4AudioCommpression = 1;







    recorderSettings.Chanel1MixCFG  |= AXIS_SOFTRECORDER_SETTINGS__INPUT_1_LINE;
    recorderSettings.Chanel2MixCFG  |= AXIS_SOFTRECORDER_SETTINGS__INPUT_1_LINE;
    recorderSettings.Chanel3MixCFG  |= AXIS_SOFTRECORDER_SETTINGS__INPUT_3_LINE;
    recorderSettings.Chanel4MixCFG  |= AXIS_SOFTRECORDER_SETTINGS__INPUT_2_LINE;

    recorderSettings.Chanel1Sens = AXIS_SOFTRECORDER_SETTINGS__SENSE_10;
    recorderSettings.Chanel2Sens = AXIS_SOFTRECORDER_SETTINGS__SENSE_10;
    recorderSettings.Chanel3Sens = AXIS_SOFTRECORDER_SETTINGS__SENSE_20;
    recorderSettings.Chanel4Sens = AXIS_SOFTRECORDER_SETTINGS__SENSE_40;


    std::cerr << recorderStatus.FullRecSecs << std::endl;

    char *dateAz = "2020";

    memcpy(&recorderStatus.Version.czDate , (uint16_t*)dateAz, 6);


    recorderStatus.FlashCapacity = 63350767616;
    recorderStatus.FlashUsed = 0;
    recorderStatus.FullRecSecs = 0;
    recorderStatus.TotalRecSecs = 698793;

    std::cerr << recorderStatus.FullRecSecs << std::endl;
}

void ServerEmulator::start()
{
    dataProvider->startConnection();
    threadWork = true;
    statusThread = std::thread(&ServerEmulator::threadFunction, this);

    dataProvider->addDataWaiter(GET_RECORDER_STATUS, shared_from_this());
    dataProvider->addDataWaiter(GET_ADAPTER_STATUS, shared_from_this());
    dataProvider->addDataWaiter(GET_RECORDER_SETTINGS, shared_from_this());
    dataProvider->addDataWaiter(SAVE_RECORDER_SETTINGS, shared_from_this());
    dataProvider->addDataWaiter(START_RECORD, shared_from_this());
    dataProvider->addDataWaiter(START_MONITORING, shared_from_this());
    dataProvider->addDataWaiter(GET_ADAPTER_ACC_STATUS, shared_from_this());
    dataProvider->addDataWaiter(GET_ADAPTER_SETTINGS, shared_from_this());
    dataProvider->addDataWaiter(SAVE_RECORDER_TIMERS, shared_from_this());
    dataProvider->addDataWaiter(GET_RECORDER_TIMERS, shared_from_this());
    dataProvider->addDataWaiter(SAVE_ADAPTER_SETTINGS, shared_from_this());
    dataProvider->addDataWaiter(GET_ADAPTER_TIMERS, shared_from_this());
    dataProvider->addDataWaiter(SAVE_ADAPTER_TIMERS, shared_from_this());
    dataProvider->addDataWaiter(APPLY_ADAPTER_SETTINGS, shared_from_this());
    dataProvider->addDataWaiter(APPLY_RECORDER_SETTINGS, shared_from_this());
}

void ServerEmulator::recieveData(int command, std::vector<uint8_t> data, size_t size)
{
    this->logger->Message(LOG_INFO, __LINE__, __func__, "COMMAND = %x, size = %i", command, size);
    std::cerr << recorderStatus.FullRecSecs << std::endl;
    try {
        switch (command )
        {
            case GET_RECORDER_SETTINGS:
            {
                if( !data.empty() && size > 0 )
                {
                    size_t sizeExp = sizeof(SoftRecorderSettingsStruct_t);
                    SoftRecorderSettingsStruct_t *dataPtr = reinterpret_cast<SoftRecorderSettingsStruct_t*>(answerData.data());
                    *dataPtr = recorderSettings;
                    dataProvider->sendCommand(static_cast<int>(GET_RECORDER_SETTINGS), answerData, sizeExp, false);
                }
                else
                {
                    this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                }
                break;
            }
            case GET_RECORDER_STATUS:
            {
                if( !data.empty() && size > 0 )
                {
                    size_t sizeExp = sizeof(SoftRecorderStatusStruct_t);
                    std::cerr << recorderStatus.FullRecSecs << std::endl;

                    SoftRecorderStatusStruct_t *dataPtr = reinterpret_cast<SoftRecorderStatusStruct_t*>(answerData.data());
                    *dataPtr = recorderStatus;

                    std::cerr << recorderStatus.FullRecSecs << std::endl;

                    this->logger->Message(LOG_INFO,__LINE__, __func__, "Test Get recoder status");
                    dataProvider->sendCommand(GET_RECORDER_STATUS, answerData, sizeExp, false);
                }
                else
                {
                    this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                }
                break;
            }
            case SAVE_RECORDER_SETTINGS:
            {
                const size_t exceptedSize = sizeof(SoftRecorderSettingsStruct_t);
                if( size >= exceptedSize && !data.empty() )
                {
                    memcpy(&this->recorderSettings, data.data(), exceptedSize);
                    if( lastTime != recorderSettings.Time )
                    {
                        std::cerr << "CHANGEEEE TIME " << std::endl;
                        struct timeval tv;
                        struct timezone tz;
                        tv.tv_usec = 0;
                        tv.tv_sec = recorderSettings.Time;
                        tz.tz_dsttime = 0;
                        tz.tz_minuteswest = 180;
                        int rc = settimeofday(&tv, NULL);
                        if(rc==0) {
                             printf("settimeofday() successful.\n");
                         }
                         else {
                             printf("settimeofday() failed, "
                             "errno = %d\n",errno);
                         }
                    }
                    this->logger->Message(LOG_ERR, "Apply new recorder settings data");
                }
                dataProvider->sendCommand(SAVE_RECORDER_SETTINGS, data, size, false);
                break;
            }
            case START_RECORD:
            {
                SoftRecorderControlStruct_t controlStruct;
                const size_t exceptedSize = sizeof(SoftRecorderControlStruct_t);
                if( size >= exceptedSize && !data.empty() )
                {
                    memcpy(&controlStruct, data.data(), exceptedSize);
                    this->logger->Message(LOG_ERR, "Apply start record");
                    recorderStatus.Chanel1Status = controlStruct.Chanel1Control;
                    recorderStatus.Chanel2Status = controlStruct.Chanel2Control;
                    recorderStatus.Chanel3Status = controlStruct.Chanel3Control;
                    recorderStatus.Chanel4Status = controlStruct.Chanel4Control;

                    SoftRecorderControlStruct_t *dataPtr = reinterpret_cast<SoftRecorderControlStruct_t*>(answerData.data());
                    *dataPtr = controlStruct;

                }
                dataProvider->sendCommand(START_RECORD, data, size, false);
                dataProvider->sendCommandInterrogation(AXIS_INTERFACE_EVENT_READ_STATUS, data, size, false);
                break;
            }
            case START_MONITORING:
            {
                SoftRecorderControlStruct_t controlStruct;
                const size_t exceptedSize = sizeof(SoftRecorderControlStruct_t);
                if( size >= exceptedSize && !data.empty() )
                {
                    memcpy(&controlStruct, data.data(), exceptedSize);
                    this->logger->Message(LOG_ERR, "Apply start monitor");
                    std::cerr << controlStruct.Chanel1Control << std::endl;
                    recorderStatus.Chanel1Status = controlStruct.Chanel1Control;
                    recorderStatus.Chanel2Status = controlStruct.Chanel2Control;
                    recorderStatus.Chanel3Status = controlStruct.Chanel3Control;
                    recorderStatus.Chanel4Status = controlStruct.Chanel4Control;

                }
                dataProvider->sendCommand(START_MONITORING, data, size, false);
                dataProvider->sendCommandInterrogation(AXIS_INTERFACE_EVENT_READ_STATUS, data, size, false);
                break;
            }
            case GET_ADAPTER_STATUS:
            {
                size_t sizeExp = sizeof(SoftAdapterDeviceCurrentStatus_t);
                SoftAdapterDeviceCurrentStatus_t *dataPtr = reinterpret_cast<SoftAdapterDeviceCurrentStatus_t*>(answerData.data());
                *dataPtr = adapterStatus;
                commandSent = GET_ADAPTER_STATUS;
                dataProvider->sendCommand(GET_ADAPTER_STATUS, answerData, sizeExp, false);
                break;
            }
            case GET_ADAPTER_ACC_STATUS:
            {
                size_t sizeExp = sizeof(SoftRecorderGetAccStateStruct_t) - ADAPTER_DATA_WORD_POSITION;
           //     uint8_t *dataPtr = reinterpret_cast<uint8_t*>(&accStatus.AccStatus);

                axis_acc_status_t *dataPtr = reinterpret_cast<axis_acc_status_t*>(answerData.data());
                *dataPtr = accStatus.AccStatus;

                //memcpy(answerData.data(), dataPtr, sizeof(SoftRecorderGetAccStateStruct_t));
                this->logger->Message(LOG_INFO,__LINE__, __func__, "Test Get acc status");
                dataProvider->sendCommand(GET_ADAPTER_ACC_STATUS, answerData, sizeExp, false);

                break;
            }
            case GET_ADAPTER_SETTINGS:
            {
                size_t sizeExp = sizeof(SoftAdapterSettingsStruct_t);
                SoftAdapterSettingsStruct_t *dataPtr = reinterpret_cast<SoftAdapterSettingsStruct_t*>(answerData.data());
                *dataPtr = adapterSettings;
                this->logger->Message(LOG_INFO,__LINE__, __func__, "Test Get acc status");
                dataProvider->sendCommand(GET_ADAPTER_SETTINGS, answerData, sizeExp, false);
                break;
            }
            case SAVE_RECORDER_TIMERS:
            {
                const size_t exceptedSize = sizeof(DspRecorderExchangeTimerSettingsStruct_t) -ADAPTER_DATA_WORD_POSITION;
                if( size >= exceptedSize && !data.empty() )
                {
                    DspRecorderExchangeTimerSettingsStruct_t *ptr = &recorderTimers;
                    memcpy(ptr->Timers, data.data(), sizeof(dsp_timer_t) * MAX_NUM_TIMERS);

                    for(int i = 0; i < MAX_NUM_TIMERS; i++)
                    {
                        if( ptr->Timers[i].start.reserv_0 == 0x1 )
                            ptr->Timers[i].stop.reserv_0 = 0x1;
                    }
                    this->logger->Message(LOG_ERR, "Apply new recorder settings data");
                }
                dataProvider->sendCommand(SAVE_RECORDER_TIMERS, data, size, false);
                break;
            }
            case GET_RECORDER_TIMERS:
            {
                size_t sizeExp = sizeof(DspRecorderExchangeTimerSettingsStruct_t) - ADAPTER_DATA_WORD_POSITION;

                dsp_timer_t *dataPtr = reinterpret_cast<dsp_timer_t*>(answerData.data());

                memcpy(dataPtr, recorderTimers.Timers, sizeof(dsp_timer_t) * MAX_NUM_TIMERS);
                this->logger->Message(LOG_INFO,__LINE__, __func__, "Test Get acc status");
                dataProvider->sendCommand(GET_RECORDER_TIMERS, answerData, sizeExp, false);
                break;
            }
            case SAVE_ADAPTER_SETTINGS:
            {
                const size_t exceptedSize = sizeof(SoftAdapterSettingsStruct_t);
                if( size >= exceptedSize && !data.empty() )
                {
                    SoftAdapterSettingsStruct_t *ptr = &adapterSettings;
                    memcpy(ptr, data.data(), exceptedSize);
                    this->logger->Message(LOG_ERR, "Apply new recorder settings data");
                }
                dataProvider->sendCommand(SAVE_ADAPTER_SETTINGS, data, size, false);
                break;
            }
            case GET_ADAPTER_TIMERS:
            {
                size_t sizeExp = sizeof(DspRecorderExchangeTimerSettingsStruct_t) - ADAPTER_DATA_WORD_POSITION;

                dsp_timer_t *dataPtr = reinterpret_cast<dsp_timer_t*>(answerData.data());

                memcpy(dataPtr, adapterTimers.Timers, sizeof(dsp_timer_t) * MAX_NUM_TIMERS);
                this->logger->Message(LOG_INFO,__LINE__, __func__, "Test Get acc status");
                dataProvider->sendCommand(GET_ADAPTER_TIMERS, answerData, sizeExp, false);
                break;
            }
            case SAVE_ADAPTER_TIMERS:
            {
                const size_t exceptedSize = sizeof(DspRecorderExchangeTimerSettingsStruct_t) -ADAPTER_DATA_WORD_POSITION;
                if( size >= exceptedSize && !data.empty() )
                {
                    DspRecorderExchangeTimerSettingsStruct_t *ptr = &adapterTimers;
                    memcpy(ptr->Timers, data.data(), sizeof(dsp_timer_t) * MAX_NUM_TIMERS);

                    for(int i = 0; i < MAX_NUM_TIMERS; i++)
                    {
                        if( ptr->Timers[i].start.reserv_0 == 0x1 )
                            ptr->Timers[i].stop.reserv_0 = 0x1;
                    }
                    this->logger->Message(LOG_ERR, "Apply new recorder settings data");
                }
                dataProvider->sendCommand(SAVE_ADAPTER_TIMERS, data, size, false);
                break;
            }
            case APPLY_ADAPTER_SETTINGS:
            {
                const size_t exceptedSize = sizeof(SoftAdapterSettingsStruct_t);
                if( size >= exceptedSize && !data.empty() )
                {
                    SoftAdapterSettingsStruct_t *ptr = &adapterSettings;
                    memcpy(ptr, data.data(), exceptedSize);
                    this->logger->Message(LOG_ERR, "Apply new recorder settings data");
                }
                dataProvider->sendCommand(APPLY_ADAPTER_SETTINGS, data, size, false);
                break;
            }
            case APPLY_RECORDER_SETTINGS:
            {
                const size_t exceptedSize = sizeof(SoftRecorderSettingsStruct_t);
                if( size >= exceptedSize && !data.empty() )
                {
                    memcpy(&this->recorderSettings, data.data(), exceptedSize);
                    if( lastTime != recorderSettings.Time )
                    {
                        std::cerr << "CHANGEEEE TIME " << std::endl;
                        struct timeval tv;
                        struct timezone tz;
                        tv.tv_usec = 0;
                        tv.tv_sec = recorderSettings.Time;
                        tz.tz_dsttime = 0;
                        tz.tz_minuteswest = 180;
                        int rc = settimeofday(&tv, NULL);
                        if(rc==0) {
                             printf("settimeofday() successful.\n");
                         }
                         else {
                             printf("settimeofday() failed, "
                             "errno = %d\n",errno);
                         }
                    }
                    this->logger->Message(LOG_ERR, "Apply new recorder settings data");
                }
                dataProvider->sendCommand(APPLY_RECORDER_SETTINGS, data, size, false);
                break;
            }
        }

    } catch (std::system_error &except)
    {
        this->logger->Message(LOG_ERR,__LINE__, __func__, "ERRRRRRRRRRRRRRRRRRRRRRORR");
    }

}

void ServerEmulator::threadFunction()
{
    while(threadWork)
    {
        //this->logger->Message(LOG_INFO, __LINE__, __func__, "Update adapter data");
        try
        {
            dataMutex.lock();
            dataMutex.unlock();
        }
        catch( std::system_error &systemErr )
        {
            dataMutex.unlock();
            this->logger->Message(LOG_ERR, __LINE__,
                                  __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        }
        std::unique_lock<std::mutex> lk(this->cv_m);
        cv.wait_for(lk, std::chrono::seconds(1), [this]{return conditionWork;});
        conditionWork = false;
    }
}
