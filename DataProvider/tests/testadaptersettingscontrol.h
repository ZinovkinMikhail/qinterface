#ifndef TESTADAPTERSETTINGSCONTROL_H
#define TESTADAPTERSETTINGSCONTROL_H

#include <QObject>
#include <QTest>

#include <rosslog/log.h>

#include "adaptersettingscontrol.h"
#include "dataprovider.h"
#include "testscommon.h"


class TestAdapterSettingsControl : public QObject
{
    Q_OBJECT
public:
    explicit TestAdapterSettingsControl(std::shared_ptr<DataProvider> dataProvider,
                                        rosslog::Log &log, QObject *parent = nullptr);

private:
    std::shared_ptr<DataProvider> dataProvider;
    AdapterSettingsControl *adapterSettingsControl;
    rosslog::Log *logger;

signals:

private slots:
    void testUpdateAdapterSettingsControl();
    void testUpdateAdapterTimers();
    void testSaveAdapterSettingsControl();
    void testSaveAdapterTimers();
};

#endif // TESTADAPTERSETTINGSCONTROL_H
