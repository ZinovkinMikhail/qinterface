#ifndef TESTSCOMMON_H
#define TESTSCOMMON_H

#define TEST_LOG_PATH "/tmp/test_log"
#define TEST_LOG_APP_NAME "dataprovider_test"
#define TEST_LOG_LEVEL 8


#define TEST_WAIT_SLEEP_TIMEOUT 15

#endif // TESTSCOMMON_H
