#ifndef TESTINTERNALMASTER_H
#define TESTINTERNALMASTER_H

#include <QObject>
#include <QTest>

#include <axis_crc.h>

#include "dataprovider.h"
#include "testscommon.h"

using namespace data_provider;

class TestInternalMaster : public QObject
{
    Q_OBJECT
public:
    explicit TestInternalMaster(std::shared_ptr<DataProvider> dataProvider,
                                QObject *parent = nullptr);
    ~TestInternalMaster();
signals:

private:
    bool testInterrogationCMD();

    std::shared_ptr<DataProvider> dataProvider;
private slots:
    void testStartServer();
    void testServerConnection();
    void testMasterWork();
    void testStopServer();
};

#endif // TESTINTERNALMASTER_H
