#ifndef TESTRECORDERSTATUSCONTROL_H
#define TESTRECORDERSTATUSCONTROL_H

#include <QObject>
#include <QTest>

#include "dataprovider.h"
#include "recorderstatuscontrol.h"


typedef enum
{
    RECORDER_TEST_ALL,
    RECORDER_TEST_STATUS_UPDATE
} RecorderStatusTestType;

class TestRecorderStatusControl : public QObject
{
    Q_OBJECT
public:
    TestRecorderStatusControl(std::shared_ptr<DataProvider> dataProvider,
                              rosslog::Log &log,
                              RecorderStatusTestType testType,
                              QObject *parent = nullptr);

private:
    std::shared_ptr<DataProvider> dataProvider;
    recorder_status_ctrl::RecorderStatusControl *recorderStatusControl;
    rosslog::Log *logger;
    RecorderStatusTestType testType;

private slots:
    void testRecorderStatusUpdate();
    void testRecorderEraseInternalMemory();
    void testStopCopyCommand();
};

#endif // TESTRECORDERSTATUSCONTROL_H
