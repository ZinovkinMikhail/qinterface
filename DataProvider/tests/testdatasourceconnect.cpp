#include "testdatasourceconnect.h"


TestDataSourceConnect::TestDataSourceConnect(rosslog::Log &log , QObject *parent) :
    QObject(parent),
    dataSource(nullptr)
{
    qRegisterMetaType<uint8_t>("uint8_t");
    dataSource = new DataSourceConnect(log);
}

TestDataSourceConnect::~TestDataSourceConnect()
{
    if( dataSource != nullptr )
        delete dataSource;
}

void TestDataSourceConnect::testMultiBlocks()
{
    if( dataSource == nullptr )
        return;

    uint8_t buff[512];
    size_t size = 0;
    buff[0] = 0x0c;
    size += 1;
    buff[1] = 0xe1;
    size += 1;
    buff[2] = 0x00;
    size += 1;
    buff[3] = 0x00;
    size += 1;
    buff[4] = 0x00;
    size += 1;
    buff[5] = 0x02;
    size += 1;
    std::vector<uint8_t> vect(&buff[0], &buff[size]);
    QVERIFY(!dataSource->recieveData(vect,size));
}

void TestDataSourceConnect::testSingleBlock()
{
    if( dataSource == nullptr )
        return;

    uint16_t buff[AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE];
    memset(buff, 0, AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE * sizeof(uint16_t));
    size_t size = 0;
    buff[0] = AXIS_DISPATCHER_EXCHANGE__ID;
    size += 2;
    buff[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET] = 0x0000;
    buff[ AXIS_SOFTRECORDER_EXCHANGE__CMD_WORD_OFFSET ] = 0x5901;
    buff[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET] = axis_crc16( buff, AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE );
    size += 2;

    std::vector<uint8_t> vect;
    vect.reserve(AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE * 2);
    uint8_t *data = reinterpret_cast<uint8_t*>(&buff);
    std::copy(&data[0], &data[AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE * 2], std::back_inserter(vect));

    QVERIFY(dataSource->recieveData(vect,sizeof(SoftRecordeExchangeStruct_s)));
}
