#include "testadapterstatuscontrol.h"

using namespace rosslog;
using namespace adapter_status_ctrl;

TestAdapterStatusControl::TestAdapterStatusControl(std::shared_ptr<DataProvider> dataProvider,
                                                   Log &log, AdapterStatusTestType testType, QObject *parent) :
    QObject(parent),
    dataProvider(nullptr),
    adapterStatusControl(nullptr),
    logger(&log),
    testType(testType)
{
    this->dataProvider = dataProvider;
    adapterStatusControl = new AdapterStatusControl(dataProvider, *logger);

    QVERIFY(dataProvider);
    QVERIFY(dataProvider->getConnectStatus());
}

void TestAdapterStatusControl::testAdapterStatusControlUpdateStatus()
{
    if( testType != ADAPTER_STATUS_TEST_ALL && testType != ADAPTER_STATUS_TEST_GET_STATUS )
        return;

    QVERIFY(dataProvider);
    QVERIFY(adapterStatusControl);
    QVERIFY(dataProvider->getConnectStatus());
    bool result = false;
    int workCounter = 5;
    try
    {
        while(workCounter)
        {
            SoftAdapterDeviceCurrentStatus_t adapterCurrentStatus;
            SoftRecorderGetAccStateStruct_t adapterAccStatus;
            adapterStatusControl->updateStatus();
            adapterCurrentStatus = adapterStatusControl->getAdapterStatus();
            adapterAccStatus = adapterStatusControl->getAccStatus();


            this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Datebuild = %li\n",
                          adapterCurrentStatus.datebuild);

            this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Voltage = %i\n", adapterAccStatus.Voltage);

            QVERIFY(adapterCurrentStatus.datebuild == 121212);
            QVERIFY(adapterAccStatus.Voltage == 0xffff);

            result = true;
            sleep(1);
            workCounter--;
        }

    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "%s",
                              dpr::ProviderError::getInstance().getErrorString(except).c_str());
    }
    catch(std::exception &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Errpor = %s",  except.what());
    }
    QVERIFY(result);
}

void TestAdapterStatusControl::testAdapterStatusControlReboot()
{
    if( testType != ADAPTER_STATUS_TEST_ALL )
        return;
    QVERIFY(dataProvider);
    QVERIFY(adapterStatusControl);
    QVERIFY(dataProvider->getConnectStatus());
    bool result = false;
    try
    {
        adapterStatusControl->doReboot();
        result = true;
    }
    catch(std::exception &excep)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, excep.what());
    }
    QVERIFY(result);
}

void TestAdapterStatusControl::testAdapterStatusControlLPC()
{
    if( testType != ADAPTER_STATUS_TEST_ALL )
        return;

    QVERIFY(dataProvider);
    QVERIFY(adapterStatusControl);
    QVERIFY(dataProvider->getConnectStatus());

    int testSend = 5;

    while(testSend)
    {
        bool result = false;
        try
        {
            this->logger->Message(LOG_INFO, __LINE__, __func__, "Send test LPC remain times %i", testSend);
            adapterStatusControl->doLPC();
            result = true;
        }
        catch(std::system_error &excep)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, excep.what());
        }
        QVERIFY(result);
        testSend--;
        usleep(10000);
    }

}

void TestAdapterStatusControl::testAdapterStatusControlSetBright()
{
    if( testType != ADAPTER_STATUS_TEST_ALL )
        return;
    QVERIFY(dataProvider);
    QVERIFY(adapterStatusControl);
    QVERIFY(dataProvider->getConnectStatus());
    bool result = false;

    try
    {
        adapterStatusControl->setDisplayBrightness(12312);
        result = true;
    }
    catch(std::system_error &excep)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, excep.what());
    }
    QVERIFY(result);
}

void TestAdapterStatusControl::testAdapterStatusControlLockKeyboard()
{
    if( testType != ADAPTER_STATUS_TEST_ALL )
        return;
    QVERIFY(dataProvider);
    QVERIFY(adapterStatusControl);
    QVERIFY(dataProvider->getConnectStatus());
    bool result = false;

    try
    {
        adapterStatusControl->setKeyBoardBlock(true);
        result = true;
    }
    catch(std::system_error &excep)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, excep.what());
    }
    QVERIFY(result);
}
