#ifndef TESTINTERNAL_H
#define TESTINTERNAL_H

#include <QObject>
#include <QTest>

#include "dataprovider.h"
#include "testscommon.h"
#include "adaptercommands.h"

#include "adapterstatuscontrol.h"

#define COMMAND_TRY_SEND_TIMES 2

//Internal test master must be started!

typedef enum
{
    INTERNAL_TEST_ALL,
    INTERNAL_TEST_CONNECTION
} InternalTestType;

class TestInternal : public QObject
{
    Q_OBJECT
public:
    explicit TestInternal(std::shared_ptr<DataProvider> dataProvider,
                          rosslog::Log &log,InternalTestType testType,
                          QObject *parent = nullptr);

signals:

private:
    bool sendMainCommand();

    std::shared_ptr<DataProvider> dataProvider;
    AdapterCommands adapterCommands;
    rosslog::Log *logger;
    InternalTestType testType;

private slots:
    void testStartClient();
    void testGetConnectionStatus();
    void testSendGetRecorderStatusCommand();
    void testSendGetAdapterStatusCommand();
    void testSendGetTimersCommand();
};

#endif // TESTINTERNAL_H
