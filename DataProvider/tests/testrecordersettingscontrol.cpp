#include "testrecordersettingscontrol.h"

using namespace rosslog;

TestRecorderSettingsControl::TestRecorderSettingsControl(std::shared_ptr<DataProvider> dataProvider,
                                                         rosslog::Log &log,
                                                         QObject *parent) :
    QObject(parent),
    dataProvider(dataProvider),
    logger(&log)
{
    QVERIFY(dataProvider);
    QVERIFY(dataProvider->getConnectStatus());

    recorderSettingsControl = new RecorderSettingsContol(dataProvider, log);
}

void TestRecorderSettingsControl::testRecorderSettingsUpdate()
{
    QVERIFY(dataProvider);
    QVERIFY(logger);

    try
    {
        SoftRecorderSettingsStruct_t recorderSettings;
        this->recorderSettingsControl->updateStatus();
        recorderSettings = recorderSettingsControl->getRecordSettings();

        this->logger->Message(LOG_INFO, __LINE__, __func__, "Recieved after send data Chanel1AudioBand 1 = %x softwareECC= %i",
                                  recorderSettings.Chanel1AudioBand, recorderSettings.softwareECC);
        QVERIFY(recorderSettings.softwareECC == 7777);
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }

}

void TestRecorderSettingsControl::testRecordSettingsSave()
{
    QVERIFY(dataProvider);
    QVERIFY(logger);

    SoftRecorderSettingsStruct_t settings;

    settings.Chanel1AudioBand = 0xfff;
    settings.Chanel1Sens = 8888;

    try
    {
        this->recorderSettingsControl->saveRecordSettings(settings);
        settings = recorderSettingsControl->getRecordSettings();
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Recieved after send data Chanel1AudioBand 1 = %x Chanel1Sens= %i",
                              settings.Chanel1AudioBand, settings.Chanel1Sens);
        QVERIFY(settings.Chanel1Sens == 8888);
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
}

void TestRecorderSettingsControl::testRecorderSettingsUpdateTimers()
{
    QVERIFY(dataProvider);
    QVERIFY(logger);

    try
    {
        DspRecorderExchangeTimerSettingsStruct_t timers;
        this->recorderSettingsControl->updateTimers();
        this->recorderSettingsControl->updateTimers();

        timers = recorderSettingsControl->getRecorderTimers();

        this->logger->Message(LOG_INFO, __LINE__, __func__, "Recieved after send data TIMERS 1 min = %i Timers 4 reserv = %i",
                                  timers.Timers[0].start.min , timers.Timers[4].stop.reserv_0);

        QVERIFY(timers.Timers[0].start.min == 15);

    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
}

void TestRecorderSettingsControl::testSetRecorderSettingsTimers()
{
    QVERIFY(dataProvider);
    QVERIFY(logger);
    DspRecorderExchangeTimerSettingsStruct_t recorderTimers;
    memset(&recorderTimers, 0 ,sizeof(DspRecorderExchangeTimerSettingsStruct_t));
    recorderTimers.Timers[0].start.min = 155;
    recorderTimers.Timers[4].stop.reserv_0 = 200;

    try
    {
        this->recorderSettingsControl->saveRecorderTimers(recorderTimers);
        recorderTimers = recorderSettingsControl->getRecorderTimers();

        this->logger->Message(LOG_INFO, __LINE__, __func__, "Recieved after send data TIMERS 1 min = %i Timers 4 reserv = %i",
                              recorderTimers.Timers[0].start.min , recorderTimers.Timers[4].stop.reserv_0);
        QVERIFY(recorderTimers.Timers[4].stop.reserv_0 == 200);
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
}
