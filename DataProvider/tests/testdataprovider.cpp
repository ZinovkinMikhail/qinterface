#include "testdataprovider.h"

#include "testdatawaiter.h"

TestDataProvider::TestDataProvider(std::shared_ptr<DataProvider> dataProvider,
                                   rosslog::Log &log,  QObject *parent) :
    QObject(parent)
{
    Q_UNUSED(log);
    this->dataProvider = dataProvider;
}

TestDataProvider::~TestDataProvider()
{}

void TestDataProvider::testDataProviederAddDataWaiter()
{
    QVERIFY(dataProvider);

    std::shared_ptr<TestDataWaiter> testData2 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));
    std::shared_ptr<TestDataWaiter> testData1 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));
    std::shared_ptr<TestDataWaiter> testData3 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));

    testData1->addWaiterTest(1);
    testData2->addWaiterTest(1);
    testData3->addWaiterTest(2);
    testData3->addWaiterTest(1);
    testData1->addWaiterTest(2);
}

void TestDataProvider::testDataProviderRemoveAddedDataWaiter()
{
    QVERIFY(dataProvider);

    std::shared_ptr<TestDataWaiter> testData2 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));
    std::shared_ptr<TestDataWaiter> testData1 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));
    std::shared_ptr<TestDataWaiter> testData3 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));

    testData1->addWaiterTest(1);
    testData2->addWaiterTest(1);
    testData3->addWaiterTest(2);
    testData3->addWaiterTest(1);
    testData1->addWaiterTest(2);

    QVERIFY(dataProvider->removeDataWaiter(1,testData2));
    QVERIFY(dataProvider->removeDataWaiter(1,testData1));
    QVERIFY(dataProvider->removeDataWaiter(2,testData1));
    QVERIFY(dataProvider->removeDataWaiter(1,testData3));
    QVERIFY(dataProvider->removeDataWaiter(2,testData3));
}

void TestDataProvider::testDataProviederRemoveAddedAndNotAdded()
{
    QVERIFY(dataProvider);

    std::shared_ptr<TestDataWaiter> testData2 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));
    std::shared_ptr<TestDataWaiter> testData1 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));
    std::shared_ptr<TestDataWaiter> testData3 =
            std::make_shared<TestDataWaiter>(TestDataWaiter(dataProvider));

    testData1->addWaiterTest(1);
    testData2->addWaiterTest(1);
    testData3->addWaiterTest(2);
    testData3->addWaiterTest(3);
    testData1->addWaiterTest(2);
    testData1->addWaiterTest(3);

    QVERIFY(dataProvider->removeDataWaiter(1,testData2));
    QVERIFY(!dataProvider->removeDataWaiter(1,testData3));
    QVERIFY(dataProvider->removeDataWaiter(1,testData1));
    QVERIFY(dataProvider->removeDataWaiter(2,testData1));
    QVERIFY(!dataProvider->removeDataWaiter(1,testData2));
    QVERIFY(dataProvider->removeDataWaiter(3,testData1));
    QVERIFY(!dataProvider->removeDataWaiter(5,testData1));
    QVERIFY(!dataProvider->removeDataWaiter(6,testData1));
}
