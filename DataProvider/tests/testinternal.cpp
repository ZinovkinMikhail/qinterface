#include <unistd.h>

#include <axis_crc.h>
#include <axis_softadapter.h>

#include "testinternal.h"

using namespace rosslog;

TestInternal::TestInternal(std::shared_ptr<DataProvider> dataProvider,
                           Log &log, InternalTestType testType, QObject *parent) :
    QObject(parent),
    testType(testType)
{
    logger = &log;
    this->dataProvider = dataProvider;
}

void TestInternal::testStartClient()
{
    QVERIFY(dataProvider);
    QVERIFY(dataProvider->startConnection());
}

void TestInternal::testGetConnectionStatus()
{
    QVERIFY(dataProvider);
    sleep(2);

    int sendCommandCounter = 40;
    bool result = false;

    while( sendCommandCounter )
    {
        if(  dataProvider->getConnectStatus() )
        {
            result = true;
            break;
        }
        else
        {
            result = false;
        }
        sleep(2);
        sendCommandCounter--;
    }

    QVERIFY(result);
}



void TestInternal::testSendGetRecorderStatusCommand()
{
    if( testType != INTERNAL_TEST_ALL )
        return;
    QVERIFY(dataProvider);
    QVERIFY(logger);

    int sendCommandCounter = COMMAND_TRY_SEND_TIMES;
    bool result = false;

    std::vector<uint8_t> data;
    size_t size = 0;

    SoftRecorderStatusStruct_t recorderStatus;
    memset(&recorderStatus, 0, sizeof(SoftRecorderStatusStruct_t));

    recorderStatus.Chanel1Status = 0xffff;
    recorderStatus.Chanel8Status = 0x00ff;

    data.resize(VECTOR_MAX_SIZE);

    SoftRecorderStatusStruct_t *dataPtr = reinterpret_cast<SoftRecorderStatusStruct_t*>(data.data());

    *dataPtr = recorderStatus;

    while( sendCommandCounter )
    {
        QVERIFY(dataProvider->getConnectStatus());
        size = sizeof(SoftRecorderStatusStruct_t);
        try
        {
            adapterCommands.getRecorderStatusCommand(dataProvider, data, size);
            result = true;
        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "%s", except.what());
            result = false;
        }
        if( result )
        {
            if( size > 0 && !data.empty() )
            {
                memcpy(&recorderStatus, data.data(), sizeof(SoftRecorderStatusStruct_t));
                this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Size = %i res channel 1 = %x", size, recorderStatus.Chanel1Status);
                this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Size = %i res channel 8 = %x", size, recorderStatus.Chanel8Status);
                QVERIFY(recorderStatus.Chanel1Status == 0xffff);
                QVERIFY(recorderStatus.Chanel8Status == 0x00ff);
            }
        }
        sendCommandCounter--;
        sleep(1);
    }
    QVERIFY(result);
}

void TestInternal::testSendGetAdapterStatusCommand()
{
    if( testType != INTERNAL_TEST_ALL )
        return;
    QVERIFY(dataProvider);
    QVERIFY(logger);

    int sendCommandCounter = COMMAND_TRY_SEND_TIMES;
    bool result = false;

    std::vector<uint8_t> data;
    size_t size = 0;

    SoftAdapterExchangeConnectionStatusStruct_t adapterStatus;
    memset(&adapterStatus, 0, sizeof(SoftAdapterExchangeConnectionStatusStruct_t));
    adapterStatus.Status.iconnected = 1;

    data.reserve(VECTOR_MAX_SIZE);

    uint8_t *dataPtr = reinterpret_cast<uint8_t*>(&adapterStatus.Status);
    size = sizeof(SoftAdapterDeviceCurrentStatus_t);

    std::copy(&dataPtr[0], &dataPtr[sizeof(SoftAdapterDeviceCurrentStatus_t)], std::back_inserter(data));

    while( sendCommandCounter )
    {
        QVERIFY(dataProvider->getConnectStatus());
        size = sizeof(SoftAdapterDeviceCurrentStatus_t);

        try
        {
            adapterCommands.getAdapterStatusCommand(dataProvider, data, size);
            result = true;
        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "%s", except.what());
            result = false;
        }
        if( result )
        {
            if( size > 0 && !data.empty() )
            {
                memcpy(&adapterStatus.Status, data.data(), sizeof(SoftAdapterDeviceCurrentStatus_t));
                this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Adapter status size = %i connected = %i", size, adapterStatus.Status.iconnected);
                QVERIFY(adapterStatus.Status.iconnected == 1);
            }
        }
        sendCommandCounter--;
        sleep(1);
    }
    QVERIFY(result);
}

void TestInternal::testSendGetTimersCommand()
{
    if( testType != INTERNAL_TEST_ALL )
        return;
    QVERIFY(dataProvider);
    QVERIFY(logger);

    int sendCommandCounter = COMMAND_TRY_SEND_TIMES;
    bool result = false;

    std::vector<uint8_t> data;
    size_t size = 0;

    DspRecorderExchangeTimerSettingsStruct_t timersStatus;
    memset(&timersStatus, 0, sizeof(DspRecorderExchangeTimerSettingsStruct_t));

    timersStatus.Timers[0].start.min = 0x59;
    timersStatus.Timers[0].start.sec = 0x58;
    timersStatus.Timers[0].start.day = 0x31;
    timersStatus.Timers[0].start.mon = 0x12;

    data.reserve(VECTOR_MAX_SIZE);

    uint8_t *dataPtr = reinterpret_cast<uint8_t*>(&timersStatus.Timers);
    size = sizeof(dsp_timer_t) * MAX_NUM_TIMERS;

    std::copy(&dataPtr[0], &dataPtr[sizeof(dsp_timer_t) * MAX_NUM_TIMERS], std::back_inserter(data));

    this->logger->Message(LOG_ERR, __LINE__, __func__, "SIZE = %i 2 %i", size, sizeof(dsp_timer_t));
    while( sendCommandCounter )
    {
        QVERIFY(dataProvider->getConnectStatus());
        try
        {
            size = sizeof(dsp_timer_t) * MAX_NUM_TIMERS;
            adapterCommands.getRecorderTimersCommand(dataProvider, data, size);
            result = true;
        }
        catch(std::runtime_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "%s", except.what());
            result = false;
        }

        if( result )
        {
            if( size > 0 && !data.empty() )
            {
                memcpy(&timersStatus.Timers, (uint8_t*)data.data(), sizeof(dsp_timer_t) * MAX_NUM_TIMERS);
                this->logger->Message(LOG_DEBUG, __LINE__, __func__,
                                      "Timers get size = %i time min %x sec %x day %x mon %x",
                                      size,
                                      timersStatus.Timers[0].start.min,
                                      timersStatus.Timers[0].start.sec,
                                      timersStatus.Timers[0].start.day,
                                      timersStatus.Timers[0].start.mon);

                QVERIFY(timersStatus.Timers[0].start.min == 0x59);
                QVERIFY(timersStatus.Timers[0].start.sec == 0x58);
                QVERIFY(timersStatus.Timers[0].start.day == 0x31);
                QVERIFY(timersStatus.Timers[0].start.mon == 0x12);
            }
        }
        sendCommandCounter--;
        sleep(1);
    }
    QVERIFY(result);
}
