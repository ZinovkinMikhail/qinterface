#ifndef RECORDERSTATUSCONTROL_H
#define RECORDERSTATUSCONTROL_H

#include <QObject>

#include <bitset>
#include <sstream>

#include <axis_softrecorder.h>
#include <axis_softadapter.h>
#include <rosslog/log.h>

#include "dataprovider.h"
#include "adaptercommands.h"

namespace recorder_status_ctrl
{

class RecorderStatusControl : public QObject
{
    Q_OBJECT
public:
    explicit RecorderStatusControl(std::shared_ptr<DataProvider> dataProvider,
                                   rosslog::Log &log,
                                   QObject *parent = nullptr);

    void updateStatus();
    SoftRecorderStatusStruct_t getRecorderStatus();
    void startRecord(std::bitset<4> channels);
    void stopRecord(std::bitset<4> channels);
    void startAudioMonitoring(std::bitset<4> channels);
    void stopAudioMonitoring(std::bitset<4> channels);
    void changeVolume(uint8_t volume);
    void eraseInternalMemory(bool full);
    void startCopy(size_t fromAddr, size_t size, std::string copyPath);
    void stopCopy();
    void startPlayAudio(uint16_t channel, uint16_t volume, uint64_t from,
                   uint64_t to, uint64_t poss, const char *code);
    void loopPlayAudio(uint64_t from, uint64_t to, uint64_t poss);
    void setPlayAudio(uint16_t channel, uint16_t volume, uint64_t from,
                      uint64_t to, uint64_t poss, const char *code);

    void stopLoopPlayAudio();
    void pausePlayAudio();
    void stopPlayAudio();
    void setPosPlayAudio(uint64_t poss);
    void setVolumePlayAudio(int volume);
    void requestSDCard();
    void returnSDCard();
    //Start play audio

private:
    int pswdConv( char* dest, const char* source, size_t count );

    rosslog::Log *logger;
    std::shared_ptr<DataProvider> dataProvider;
    AdapterCommands adapterCommands;
    SoftRecorderStatusStruct_t recorderStatus;

    std::vector<uint8_t> dataReserv;

signals:

public slots:
};

}

#endif // RECORDERSTATUSCONTROL_H
