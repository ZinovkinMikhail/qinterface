#include <assert.h>

#include "adaptersettingscontrol.h"

using namespace rosslog;

AdapterSettingsControl::AdapterSettingsControl(std::shared_ptr<DataProvider> dataProvider,
                                               rosslog::Log &log, QObject *parent) :
    QObject(parent),
    dataProvider(dataProvider),
    logger(&log)
{
    assert(dataProvider);
    assert(logger);

    dataReserv.resize(VECTOR_MAX_SIZE);
    memset(&adapterSettings, 0, sizeof(SoftAdapterExchangeSettingsStruct_t));
    memset(&adapterTimers, 0, sizeof(DspRecorderExchangeTimerSettingsStruct_t));
}

void AdapterSettingsControl::updateSettings() noexcept(false)
{
    const size_t exceptedSize = sizeof(SoftAdapterSettingsStruct_t);
    size_t size = 0;
    try
    {
#ifdef TESTS
        size = exceptedSize;
        SoftAdapterSettingsStruct_t *dataVect =
                reinterpret_cast<SoftAdapterSettingsStruct_t*>(dataReserv.data());

        this->adapterSettings.Block0.AdapterSettings.Connect = 231;
        *dataVect = this->adapterSettings.Block0.AdapterSettings;
        memset(&adapterSettings, 0, exceptedSize);
#endif

        this->adapterCommands.getAdapterSettingsCommand(dataProvider, dataReserv, size);

        if( size >= exceptedSize && !dataReserv.empty() )
        {
#ifdef CLIENT_EMULATOR
            memcpy(&adapterSettings.Block0.AdapterSettings, dataReserv.data(), sizeof(SoftAdapterSettingsStruct_t));
#else
            memcpy(&adapterSettings.Block0.AdapterSettings, &dataReserv[4], sizeof(SoftAdapterSettingsStruct_t));
#endif
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to update adapter settings");
        throw except;
    }

}

void AdapterSettingsControl::updateTimers() noexcept(false)
{
    const size_t exceptedSize = sizeof(DspRecorderExchangeTimerSettingsStruct_t) - ADAPTER_DATA_WORD_POSITION;
    size_t size = 0;
    try
    {
#ifdef TESTS
        size = exceptedSize;

        adapterTimers.Timers[0].start.min = 155;
        adapterTimers.Timers[4].stop.reserv_0 = 222;
        memcpy(dataPtr, adapterTimers.Timers, sizeof(adapterTimers.Timers));

#endif
        this->adapterCommands.getAdapterNetTimersCommnad(dataProvider, dataReserv, size);

        if( size >= exceptedSize && !dataReserv.empty() )
        {
#ifdef CLIENT_EMULATOR
            memcpy(&adapterTimers.Timers, dataReserv.data(), sizeof (adapterTimers.Timers));
#else
            memcpy(&adapterTimers.Timers, &dataReserv[8], sizeof (adapterTimers.Timers));
#endif

        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to update adapter timers");
        throw except;
    }
}

void AdapterSettingsControl::applyAdapterSettings(SoftAdapterSettingsStruct_t settings)
{
    const size_t exceptedSize = sizeof(SoftAdapterSettingsStruct_t);
    size_t size = 0;
    try
    {
        size = exceptedSize;
        uint8_t *dataPtr =
                reinterpret_cast<uint8_t*>(dataReserv.data());

        memset(dataPtr, 0 , sizeof(dataReserv));
#ifdef CLIENT_EMULATOR
        memcpy(dataPtr, &settings, exceptedSize);
#else
        memcpy(&dataPtr[4], &settings, exceptedSize);
#endif


        this->adapterCommands.applyAdapterSettingsCommand(dataProvider, dataReserv, size);

        if( size >= exceptedSize && !dataReserv.empty() )
        {
#ifdef TESTS
            memset(&adapterSettings, 0, sizeof(SoftAdapterExchangeSettingsStruct_t));
            memcpy(&this->adapterSettings.Block0.AdapterSettings, dataReserv.data(), exceptedSize);
#endif
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to save adapter settings");
        throw except;
    }
}

void AdapterSettingsControl::saveAdapterSettings(SoftAdapterSettingsStruct_t settings)
{
    const size_t exceptedSize = sizeof(SoftAdapterSettingsStruct_t);
    size_t size = 0;
    try
    {
        size = exceptedSize;
        uint8_t *dataPtr =
                reinterpret_cast<uint8_t*>(dataReserv.data());

        memset(dataPtr, 0 , sizeof(dataReserv));
#ifdef CLIENT_EMULATOR
        memcpy(dataPtr, &settings, exceptedSize);
#else
        memcpy(&dataPtr[4], &settings, exceptedSize);
#endif


        this->adapterCommands.saveAdapterSettingsCommand(dataProvider, dataReserv, size);

        if( size >= exceptedSize && !dataReserv.empty() )
        {
#ifdef TESTS
            memset(&adapterSettings, 0, sizeof(SoftAdapterExchangeSettingsStruct_t));
            memcpy(&this->adapterSettings.Block0.AdapterSettings, dataReserv.data(), exceptedSize);
#endif
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to save adapter settings");
        throw except;
    }
}

void AdapterSettingsControl::saveAdapterTimers(DspRecorderExchangeTimerSettingsStruct_t timers)
{
    const size_t exceptedSize = sizeof(timers.Timers) + 8;
    size_t size = 0;
    try
    {
        size = exceptedSize;

        uint8_t *dataPtr =
                reinterpret_cast<uint8_t*>(dataReserv.data());

        memset(dataPtr, 0 , sizeof(dataReserv));

        memcpy(&dataPtr[8], &timers.Timers, size);

        this->adapterCommands.saveAdapterNetTimersCommand(dataProvider, dataReserv, size);

        if( size >= exceptedSize && !dataReserv.empty() )
        {
#ifdef TESTS
            memset(&adapterTimers.Timers, 0 ,exceptedSize);
            memcpy(&adapterTimers.Timers, dataReserv.data(), exceptedSize);
#endif
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to save adapter timers");
        throw except;
    }
}

SoftAdapterExchangeSettingsStruct_t AdapterSettingsControl::getAdapterSettings()
{
    return adapterSettings;
}

DspRecorderExchangeTimerSettingsStruct_t AdapterSettingsControl::getAdapterTimers()
{
    return adapterTimers;
}
