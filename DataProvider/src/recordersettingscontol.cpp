#include <assert.h>

#include "recordersettingscontol.h"

using namespace rosslog;

RecorderSettingsContol::RecorderSettingsContol(std::shared_ptr<DataProvider> dataProvider,
                                               rosslog::Log &log,
                                               QObject *parent) :
    QObject(parent),
    dataProvider(dataProvider),
    logger(&log)
{
    assert(dataProvider);
    assert(logger);

    dataReserv.resize(VECTOR_MAX_SIZE);
    dataReserv.reserve(VECTOR_MAX_SIZE);
    memset(&recorderSettings, 0, sizeof(SoftRecorderSettingsStruct_t));
    memset(&recorderTimers, 0, sizeof(DspRecorderExchangeTimerSettingsStruct_t));
}

void RecorderSettingsContol::updateStatus()
{
    const size_t exceptedSize = sizeof(SoftRecorderSettingsStruct_t);
    size_t size = 0;
    try
    {
#ifdef TESTS
        recorderSettings.Chanel1AudioBand = 0xff00;
        recorderSettings.softwareECC = 7777;
        size = exceptedSize;

        SoftRecorderSettingsStruct_t *dataPtr = reinterpret_cast<SoftRecorderSettingsStruct_t*>(dataReserv.data());
        *dataPtr = recorderSettings;
        memset(&recorderSettings, 0, exceptedSize);
#endif
        this->adapterCommands.getRecorderSettingsCommand(dataProvider, dataReserv, size);

        if( size >= exceptedSize && !dataReserv.empty() )
        {
            memcpy(&this->recorderSettings, dataReserv.data(), exceptedSize);
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
        //this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while updating record settings status");
    }
    catch(std::system_error &except)
    {
        //this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while updating record settings status");
        throw except;
    }
}

void RecorderSettingsContol::applyRecordSettings(SoftRecorderSettingsStruct_t settings) noexcept(false)
{
    try
    {
        size_t size = sizeof(SoftRecorderSettingsStruct_t);
        SoftRecorderSettingsStruct_t *dataPtr = reinterpret_cast<SoftRecorderSettingsStruct_t*>(dataReserv.data());
        *dataPtr = settings;

        this->adapterCommands.applyRecorderSettingsCommand(dataProvider, dataReserv, size);

        if( size > 0 && !dataReserv.empty() )
        {
#ifdef TESTS
            memcpy(&this->recorderSettings, dataReserv.data(), sizeof(SoftRecorderSettingsStruct_t));
#endif
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        throw except;
    }
}

void RecorderSettingsContol::saveRecordSettings(SoftRecorderSettingsStruct_t settings) noexcept(false)
{
    try
    {
        size_t size = sizeof(SoftRecorderSettingsStruct_t);
        SoftRecorderSettingsStruct_t *dataPtr = reinterpret_cast<SoftRecorderSettingsStruct_t*>(dataReserv.data());
        *dataPtr = settings;

        this->adapterCommands.saveRecorderSettingsCommand(dataProvider, dataReserv, size);

        if( size > 0 && !dataReserv.empty() )
        {
#ifdef TESTS
            memcpy(&this->recorderSettings, dataReserv.data(), sizeof(SoftRecorderSettingsStruct_t));
#endif
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        throw except;
    }
}

void RecorderSettingsContol::updateTimers() noexcept(false)
{
    const size_t exceptedSize = sizeof(DspRecorderExchangeTimerSettingsStruct_t) - ADAPTER_DATA_WORD_POSITION;
    size_t size = 0;
    try
    {
#ifdef TESTS
        recorderTimers.Timers[0].start.min = 15;
        recorderTimers.Timers[4].stop.reserv_0 = 20;

        size = exceptedSize;

        uint8_t *dataPtr =
                reinterpret_cast<uint8_t*>(dataReserv.data());
        memcpy(dataPtr, recorderTimers.Timers, size);
        memset(&recorderTimers.Timers, 0, exceptedSize);
#endif
        this->adapterCommands.getRecorderTimersCommand(dataProvider, dataReserv, size);


        if( size >= exceptedSize && !dataReserv.empty() )
        {
#warning test it
#ifdef CLIENT_EMULATOR
            memcpy(&recorderTimers.Timers, dataReserv.data(), sizeof(dsp_timer) * MAX_NUM_TIMERS);
#else
            memcpy(&recorderTimers.Timers, &dataReserv[8], sizeof(dsp_timer) * MAX_NUM_TIMERS);
#endif
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }

    }
    catch(std::system_error &except)
    {
        throw except;
    }
}

DspRecorderExchangeTimerSettingsStruct_t RecorderSettingsContol::getRecorderTimers()
{
    return this->recorderTimers;
}

SoftRecorderSettingsStruct_t RecorderSettingsContol::getRecordSettings()
{
    return this->recorderSettings;
}

void RecorderSettingsContol::saveRecorderTimers(DspRecorderExchangeTimerSettingsStruct_t timers) noexcept(false)
{
    const size_t exceptedSize = sizeof(timers.Timers) + 8;
    try
    {
        size_t size = 0;

        size = sizeof(timers.Timers) + 8;


        uint8_t *dataPtr =
                reinterpret_cast<uint8_t*>(dataReserv.data());

        memset(dataPtr, 0 , sizeof(dataReserv));

#ifdef CLIENT_EMULATOR
        memcpy(dataPtr, &timers.Timers, exceptedSize);
#else
        memcpy(&dataPtr[8], &timers.Timers, exceptedSize);

#endif
        this->adapterCommands.saveRecorderTimersCommand(dataProvider, dataReserv, size);

        if( size  >= exceptedSize && size <= BLOCK_SIZE && !dataReserv.empty() )
        {
#ifdef TESTS
            memset(&recorderTimers.Timers, 0, exceptedSize);
            memcpy(&recorderTimers.Timers, dataReserv.data(), sizeof(recorderTimers.Timers));
#endif
        }
        else
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }

    }
    catch(std::system_error &except)
    {
        throw except;
    }
}
