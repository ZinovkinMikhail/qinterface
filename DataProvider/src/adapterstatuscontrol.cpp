#include <assert.h>

#include "adapterstatuscontrol.h"

using namespace rosslog;
using namespace adapter_status_ctrl;

AdapterStatusControl::AdapterStatusControl(std::shared_ptr<DataProvider> dataProvider,
                                           rosslog::Log &log,
                                           QObject *parent) :
    QObject(parent),
    logger(&log)
{
    assert(dataProvider);
    assert(logger);

    this->dataProvider = dataProvider;
    dataReserv.resize(VECTOR_MAX_SIZE);
    memset(&adapterCurrentStatus, 0, sizeof(SoftAdapterDeviceCurrentStatus_t));
    memset(&adapterAccStatus, 0, sizeof(SoftRecorderGetAccStateStruct_t));
}

void AdapterStatusControl::updateStatus() noexcept(false)
{
    getAdapterDeviceStatus();
    getAdapterAccStatus();
}

SoftAdapterDeviceCurrentStatus_t AdapterStatusControl::getAdapterStatus()
{
    return this->adapterCurrentStatus;
}

SoftRecorderGetAccStateStruct_t AdapterStatusControl::getAccStatus()
{
    return this->adapterAccStatus;
}

void AdapterStatusControl::doReboot() noexcept(false)
{
    try
    {
        this->adapterCommands.adapterRebootCommand(this->dataProvider);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send reboot command");
        throw except;
    }
}

void AdapterStatusControl::doLPC() noexcept(false)
{
#warning LPC TIMEOUT?
    try
    {
        this->adapterCommands.adapterLPCCommand(this->dataProvider, LPC_TIOMEOUT);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send LPC command");
        throw except;
    }
}

void AdapterStatusControl::setDisplayBrightness(uint32_t val) noexcept(false)
{
    try
    {
        this->adapterCommands.adapterSetDisplayBrightnessCommand(this->dataProvider, val);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send Set Bright command");
        throw except;
    }
}

void AdapterStatusControl::setKeyBoardBlock(bool lock)
{
    try
    {
        if( lock )
            this->adapterCommands.adapterSetKeyboardLock(this->dataProvider);
        else
            this->adapterCommands.adapterSetKeyboardUnLock(this->dataProvider);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send Lock/Unlock command");
        throw except;
    }
}

void AdapterStatusControl::getAdapterDeviceStatus()  noexcept(false)
{
    const size_t exceptedSize = sizeof(SoftAdapterDeviceCurrentStatus_t);
    size_t size = 0;

#ifdef TESTS
    size = exceptedSize;
    adapterCurrentStatus.datebuild = 121212;
    SoftAdapterDeviceCurrentStatus_t *dataPtr = reinterpret_cast<SoftAdapterDeviceCurrentStatus_t*>(dataReserv.data());
    *dataPtr = adapterCurrentStatus;
#endif

    this->adapterCommands.getAdapterStatusCommand(dataProvider, dataReserv, size);

    if( size >= exceptedSize && !dataReserv.empty() )
    {
        this->adapterCurrentStatus = *reinterpret_cast<SoftAdapterDeviceCurrentStatus_t*>(dataReserv.data());
        this->adapterCommands.getAdapterStatusCommand(dataProvider, dataReserv, size);

      //  dataProvider->sendCommand(GET_ADAPTER_STATUS, dataReserv, size, false);
    }
    else
    {
        throw std::system_error(dpr::INVALID_PACKET_DATA, generic_category());
    }
}

void AdapterStatusControl::getAdapterAccStatus()  noexcept(false)
{
    const size_t exceptedSize = sizeof(SoftRecorderGetAccStateStruct_t) - ADAPTER_DATA_WORD_POSITION;
    size_t size = 0;

#ifdef TESTS
    size = exceptedSize;
    adapterAccStatus.Voltage = 0xffff;
    uint8_t *adapterPtr = reinterpret_cast<uint8_t*>(&adapterAccStatus);
    memcpy(dataReserv.data(), &adapterPtr[ADAPTER_DATA_WORD_POSITION], size);
    memset(&adapterAccStatus, 0, sizeof(SoftRecorderGetAccStateStruct_t));
#endif

    this->adapterCommands.getAdapterAccStatusCommand(dataProvider, dataReserv, size);

    if(  size >= exceptedSize && !dataReserv.empty() )
    {
        uint8_t *dataPtr = reinterpret_cast<uint8_t*>(&this->adapterAccStatus);
        memcpy(&dataPtr[ADAPTER_DATA_WORD_POSITION], dataReserv.data(), exceptedSize);
        //dataProvider->sendCommand(GET_ADAPTER_ACC_STATUS, dataReserv, size, false);
    }
    else
    {
        throw std::system_error(dpr::INVALID_PACKET_DATA, generic_category());
    }
}

void AdapterStatusControl::requestUSB(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    try
    {
        this->adapterCommands.adapterRequestUSB(this->dataProvider);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send request USB command");
        throw except;
    }
}

void AdapterStatusControl::returnUSB(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    try
    {
        this->adapterCommands.adapterReturnUSB(dataProvider);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send return USB command");
        throw except;
    }
}

#ifdef DATABASE_USE
void AdapterStatusControl::startAudioHeaderDownload(SoftAdapterAudioHeaderDownload_t audioHeader)
{
	const size_t exceptedSize = sizeof(SoftAdapterAudioHeaderDownload_t);
	size_t size = 0;
	try
	{
		size = exceptedSize;
		SoftAdapterAudioHeaderDownload_t *dataPtr =
			reinterpret_cast<SoftAdapterAudioHeaderDownload_t*>(dataReserv.data());
		
		memset(dataPtr, 0 , sizeof(dataReserv));
		*dataPtr = audioHeader;
		
		this->adapterCommands.startAudioHeaderDownload(dataProvider, dataReserv, size);
		
		if( size >= exceptedSize && !dataReserv.empty() )
		{
			#ifdef TESTS
			memset(&adapterSettings, 0, sizeof(SoftAdapterExchangeSettingsStruct_t));
            memcpy(&this->adapterSettings.Block0.AdapterSettings, dataReserv.data(), exceptedSize);
			#endif
		}
		else
		{
			throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
		}
	}
	catch(std::system_error &except)
	{
		this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to start audio header download");
		throw except;
	}
}
#endif
