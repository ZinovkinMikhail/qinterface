#ifndef ADAPTERSETTINGSCONTROL_H
#define ADAPTERSETTINGSCONTROL_H

#include <QObject>

#include <rosslog/log.h>
#include <axis_softadapter.h>

#include "dataprovider.h"
#include "adaptercommands.h"

class AdapterSettingsControl : public QObject
{
    Q_OBJECT
public:
    AdapterSettingsControl(std::shared_ptr<DataProvider> dataProvider,
                           rosslog::Log &log, QObject *parent = nullptr);

    void updateSettings() noexcept(false);
    void updateTimers() noexcept(false);
    void applyAdapterSettings(SoftAdapterSettingsStruct_t settings) noexcept(false);
    void saveAdapterSettings(SoftAdapterSettingsStruct_t settings) noexcept(false);
    void saveAdapterTimers(DspRecorderExchangeTimerSettingsStruct_t timers) noexcept(false);
    SoftAdapterExchangeSettingsStruct_t getAdapterSettings();
    DspRecorderExchangeTimerSettingsStruct_t getAdapterTimers();


private:
    std::shared_ptr<DataProvider> dataProvider;
    AdapterCommands adapterCommands;
    rosslog::Log *logger;
    SoftAdapterExchangeSettingsStruct_t adapterSettings;
    DspRecorderExchangeTimerSettingsStruct_t adapterTimers;

    std::vector<uint8_t> dataReserv;
};

#endif // ADAPTERSETTINGSCONTROL_H
