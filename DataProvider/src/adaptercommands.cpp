#include "adaptercommands.h"

AdapterCommands::AdapterCommands(QObject *parent) : QObject(parent)
{}

void AdapterCommands::getRecorderStatusCommand(std::shared_ptr<DataProvider> dataProvider,
                                               std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(GET_RECORDER_STATUS, data, size, true);
}

void AdapterCommands::getAdapterStatusCommand(std::shared_ptr<DataProvider> dataProvider,
                                              std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(GET_ADAPTER_STATUS, data, size, true);
}

void AdapterCommands::getRecorderSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                                 std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(GET_RECORDER_SETTINGS, data, size, true);
}

void AdapterCommands::saveRecorderSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                                  std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(SAVE_RECORDER_SETTINGS, data, size, true);
}

void AdapterCommands::applyRecorderSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                                   std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(APPLY_RECORDER_SETTINGS, data, size, true);
}

void AdapterCommands::getAdapterAccStatusCommand(std::shared_ptr<DataProvider> dataProvider,
                                                 std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(GET_ADAPTER_ACC_STATUS, data, size, true);
}

void AdapterCommands::getRecorderTimersCommand(std::shared_ptr<DataProvider> dataProvider,
                                               std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(GET_RECORDER_TIMERS, data, size, true);
}

void AdapterCommands::saveRecorderTimersCommand(std::shared_ptr<DataProvider> dataProvider,
                                                std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(SAVE_RECORDER_TIMERS, data, size, true);
}

void AdapterCommands::adapterRebootCommand(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    std::vector<uint8_t> data;
    data.resize(BLOCK_SIZE);
    size_t size = 0;
    dataProvider->sendCommand(REBOOT, data, size, true);
}

void AdapterCommands::adapterLPCCommand(std::shared_ptr<DataProvider> dataProvider,
                                        uint32_t timeout) noexcept(false)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    data.resize(BLOCK_SIZE);
    size = 0;
    dataProvider->sendCommand(LPC, data, size, true);
}

void AdapterCommands::adapterSetDisplayBrightnessCommand(std::shared_ptr<DataProvider> dataProvider,
                                                         uint32_t val) noexcept(false)
{
    const size_t exceptedSize = sizeof(int);
    size_t size = 0;
    std::vector<uint8_t> data;

    size = sizeof(uint8_t);
    data.push_back((uint8_t)val);

    data.resize(sizeof(SoftRecorderExchangeSettingsStruct_t));

    try
    {
        dataProvider->sendCommand(DISPLAY_BRIGHTNES, data, size, true);

        if( size < exceptedSize && data.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        throw except;
    }
}

void AdapterCommands::adapterSetKeyboardUnLock(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    dataProvider->sendCommand(KEYBOARD_UNLOCK, data, size, true);
}

void AdapterCommands::eraseInternalMemoryCommand(std::shared_ptr<DataProvider> dataProvider)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    dataProvider->sendCommand(ERASE_INTERNAL_MEMORY, data, size, true);
}

void AdapterCommands::stopCopyCommand(std::shared_ptr<DataProvider> dataProvider)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    dataProvider->sendCommand(STOP_COPY, data, size, true);
}

void AdapterCommands::getAdapterSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                                std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(GET_ADAPTER_SETTINGS, data, size, true);
}

void AdapterCommands::getAdapterNetTimersCommnad(std::shared_ptr<DataProvider> dataProvider,
                                                 std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(GET_ADAPTER_TIMERS, data, size, true);
}

void AdapterCommands::saveAdapterNetTimersCommand(std::shared_ptr<DataProvider> dataProvider,
                                                  std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(SAVE_ADAPTER_TIMERS, data, size, true);
}

void AdapterCommands::saveAdapterSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                                 std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(SAVE_ADAPTER_SETTINGS, data, size, true);
}

void AdapterCommands::applyAdapterSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                                  std::vector<uint8_t> &data, size_t &size) noexcept(false)
{
    dataProvider->sendCommand(APPLY_ADAPTER_SETTINGS, data, size, true);
}

void AdapterCommands::startRecordCommand(std::shared_ptr<DataProvider> dataProvider,
                                         std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(START_RECORD, data, size, false);
}

void AdapterCommands::startMonitorCommand(std::shared_ptr<DataProvider> dataProvider,
                                          std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(START_MONITORING, data, size, false);
}

void AdapterCommands::stopMonitorCommand(std::shared_ptr<DataProvider> dataProvider,
                                         std::vector<uint8_t> &data, size_t &size)
{
    dataProvider->sendCommand(STOP_MONITORING, data, size, true);
}

void AdapterCommands::adapterSetKeyboardLock(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    dataProvider->sendCommand(KEYBOARD_LOCK, data, size, true);
}

void AdapterCommands::startAudioHeaderDownload( std::shared_ptr<DataProvider> dataProvider,
												std::vector<uint8_t> &data, size_t &size)
{
	dataProvider->sendCommand(START_AUDIO_DOWNLOAD, data, size, true);
}

void AdapterCommands::recorderRequestSD(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    dataProvider->sendCommand(REQUEST_SD, data, size, true);
}

void AdapterCommands::recorderReturnSD(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    dataProvider->sendCommand(RETURN_SD, data, size, true);
}

void AdapterCommands::sendPlayerCommand(std::shared_ptr<DataProvider> dataProvider, vector<uint8_t> &data,
                                        size_t &size) noexcept(false)
{
    dataProvider->sendCommand(PLAYER_COMMAND, data, size, true);
}

void AdapterCommands::adapterRequestUSB(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    dataProvider->sendCommand(REQUEST_USB, data, size, true);
}

void AdapterCommands::adapterReturnUSB(std::shared_ptr<DataProvider> dataProvider) noexcept(false)
{
    size_t size = 0;
    std::vector<uint8_t> data;
    dataProvider->sendCommand(RETURN_USB, data, size, true);
}
