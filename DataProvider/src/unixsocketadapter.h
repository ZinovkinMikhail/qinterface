#ifndef UNIXSOCKETADAPTER_H
#define UNIXSOCKETADAPTER_H

#include <string>
#include <memory>
#include <future>
#include <pthread.h>
#include <memory>

#include <axis_softrecorder.h> /* SoftRecorderExchangeStruct */
#include <rosslog/log.h>

#include "idatareciever.h"

#ifdef SLAVE
#define SOCKET_PATH_IN          "interface_out_socket" //"socket_path_in"
#define SOCKET_PATH_OUT       "interface_in_socket" //"socket_path_out"
#else
#define SOCKET_PATH_IN "socket_path_out"
#define SOCKET_PATH_OUT "socket_path_in"
#endif

#define SOCKET_READ_TIMEOUT 10
#define BLOCK_SIZE 512
#define SOCKET_MAX_READ_BLOCK 10 // No multiblock packets
#define VECTOR_MAX_SIZE 1024
#define SOCKET_WRITE_TIMEOUT 5

namespace UnixAdapter
{

typedef enum
{
    SOCKET_IN,
    SOCKET_OUT
} SocketType;

typedef enum
{
    THREAD_DISABLED,
    THREAD_ENABLED,
    THREAD_ERROR
} ThreadState;

typedef enum
{
    SOCKET_NOT_OPENED,
    SOCKET_NOT_CONNECTED,
    SOCKET_CONNECTED
} SocketState;

typedef struct
{
    int socketFd;
    SocketState socketState;
    std::string socketPath;
    int socketClientFd;
    std::vector<uint8_t> dataVector;
} SocketData;


class UnixSocketAdapter
{
public:
    UnixSocketAdapter(rosslog::Log &log, const std::string socketPath);
    ~UnixSocketAdapter();
    bool connectAdapter();
    void disconnect();
    void disconnectClient();
    bool startListenSocket();
    bool sendData(uint8_t *data, size_t size,SocketType type);
    size_t readData(uint8_t *data, size_t size);
    bool getInConnectStatus();
    bool getOutConnectStatus();
    void setGA( std::shared_ptr<IDataReciever> dataReciever);
    int getErrorsCount();

    bool checkSocketWork(SocketType type);
    size_t readBlockFromAdapter(SocketType socketType);

    /* Only for internal test */
    void masterWork();

private:
    bool startThread();
    void createSocket(SocketData &socketData, const std::string &socketPath);
    bool openSocket(SocketType socketType);
    bool bindSocket(SocketType socketType);
    bool connectSocket(SocketType socketType);
    bool acceptClient(SocketData *socketData);
    int writeSocket(SocketData *socketData, uint8_t *buff, size_t size, int timeout);
    size_t readSocket(SocketData *socketData, uint8_t &buff, size_t size, int timeout);
    void threadWorkFunction();

    SocketData socketDataIn;
    SocketData socketDataOut;

    ThreadState threadState;
    std::future<void> listenWork();

    bool threadWork{true};

    std::shared_ptr<IDataReciever> dataReciever;
    pthread_t thread;
    std::thread mainThread;
    pid_t pid;
    pthread_mutex_t mainMutex;
    rosslog::Log *logger;
    int errorCount;
};

}

#endif // UNIXSOCKETADAPTER_H
