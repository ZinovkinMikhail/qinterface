#ifndef TESTDATAWAITER_H
#define TESTDATAWAITER_H

#include <QObject>

#include <memory>

#include <rosslog/log.h>

#include "idatawaiter.h"
#include "dataprovider.h"

using namespace data_provider;

class TestDataWaiter : public std::enable_shared_from_this<TestDataWaiter> , public IDataWaiter
{
public:
    TestDataWaiter(std::shared_ptr<DataProvider> dataProvider);
    TestDataWaiter(std::shared_ptr<DataProvider> dataProvider, rosslog::Log &log);

    void setMemoryData(uint64_t capacity, uint64_t curCapacity, uint64_t record, uint64_t totatRecord);

    bool addWaiterTest(int command);
    int test;
    virtual ~TestDataWaiter();
protected:
     void recieveData(int command, std::vector<uint8_t> data,
                      size_t size);

private:
     std::shared_ptr<DataProvider> dataProvider;
     rosslog::Log *logger;
     SoftRecorderSettingsStruct_t recorderSettings;
     SoftRecorderStatusStruct_t recorderStatus;
};

#endif // TESTDATAWAITER_H
