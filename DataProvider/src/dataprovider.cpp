#include <assert.h>
#include <iostream>

#include "dataprovider.h"

using namespace rosslog;
using namespace data_provider;

DataProvider::DataProvider(Log &log, QObject *parent) :
    QObject(parent),
    logger(&log),
    dataSource(nullptr)
{
    assert(logger);
    qRegisterMetaType<std::vector<uint8_t>>("std::vector<uint8_t>");
    qRegisterMetaType<uint8_t>("uint8_t");
    qRegisterMetaType<size_t>("size_t");
}

DataProvider::~DataProvider()
{
    std::cerr << "Removed data provider" << std::endl;
}

bool DataProvider::startConnection()
{
    initDataSourceConnect();
    if( dataSource != nullptr )
        dataSource->startConnection();
    return true;
}

bool DataProvider::getConnectStatus()
{
    if( dataSource != nullptr )
        return dataSource->getConnectionState();
    return false;
}

void DataProvider::sendCommand(int command, std::vector<uint8_t> &data,
                               size_t &size, bool waitAnswer) noexcept(false)
{
    lock.lock();

    if( dataSource != nullptr )
    {
        try
        {
            //t logger->Message(LOG_DEBUG, __LINE__, __func__, "send command %x %i", command, command);
            if( !dataSource->sendData(command, data, size) )
            {
                lock.unlock();
                throw std::system_error(dpr::ERROR_COMMAND,
                                              std::generic_category());
            }

            if( waitAnswer )
            {
                //t logger->Message(LOG_DEBUG, __LINE__, __func__, "Read after send command %x %i", command, command);
                int ret = dataSource->readData(command, data);
                size = ret;
    //            if( ret <= 0 )
    //                throw std::runtime_error("Read data failed");
            }
        }
        catch(std::system_error &except)
        {
            lock.unlock();
            throw except;
        }
    }
    lock.unlock();
}

void DataProvider::sendCommandInterrogation(int command, std::vector<uint8_t> &data, size_t &size, bool waitAnswer)
{
    lock.lock();
    if( dataSource != nullptr )
    {
        try
        {
            if( !dataSource->sendDataInterrogation(command, data, size) )
            {
                lock.unlock();
                throw std::system_error(dpr::ERROR_COMMAND,
                                              std::generic_category());
            }

            if( waitAnswer )
            {
                logger->Message(LOG_DEBUG, __LINE__, __func__, "Read after send command %x", command);
                int ret = dataSource->readData(command, data);
                size = ret;
    //            if( ret <= 0 )
    //                throw std::runtime_error("Read data failed");
            }
        }
        catch(std::system_error &except)
        {
            lock.unlock();
            throw except;
        }
    }
    lock.unlock();
}

bool DataProvider::addDataWaiter(int commandWait, std::shared_ptr<IDataWaiter> dataWaiter)
{
    if( dataWaiter == nullptr )
        return false;

    auto dataWaiterList = dataWaiters.find(commandWait);

    if( dataWaiterList != dataWaiters.end() )
    {
        dataWaiterList->second.push_back(dataWaiter);
        logger->Message(LOG_DEBUG, __LINE__, __func__, "[Command %i] Added new dataWaiter in same list object %p",
                        commandWait, &dataWaiter);
    }
    else
    {
        std::list<std::shared_ptr<IDataWaiter>> dataWaiterList;
        dataWaiterList.push_back(dataWaiter);
        dataWaiters[commandWait] = dataWaiterList;
        logger->Message(LOG_DEBUG, __LINE__, __func__, "[Command %i] Added new dataWaiter in same list object %p",
                        commandWait, &dataWaiter);
    }
    return true;
}

bool DataProvider::removeDataWaiter(int command, std::shared_ptr<IDataWaiter> dataWaiter)
{
    if( dataWaiter == nullptr )
        return false;

    auto dataWaiterList = dataWaiters.find(command);

    if( dataWaiterList != dataWaiters.end() )
    {
        auto dataWaiterFound = std::find(dataWaiterList->second.begin(), dataWaiterList->second.end(), dataWaiter);

        if( dataWaiterFound != dataWaiterList->second.end() )
        {
            dataWaiterList->second.erase(dataWaiterFound);
            logger->Message(LOG_DEBUG, __LINE__, __func__, "[Command %i] Added new dataWaiter in same list object %p",
                            command, &dataWaiter);
            return true;
        }
        else
        {
            logger->Message(LOG_DEBUG, __LINE__, __func__, "[Command %i] Added new dataWaiter in same list object %p",
                            command, &dataWaiter);
        }
    }
    else
    {
        logger->Message(LOG_INFO, __LINE__, __func__, "[Command %i] Data list not found",
                        command);
    }
    return false;
}

void DataProvider::prepareToDelete()
{
    isPrepareToDelete = true;
    dataSource->closeConnection();
    std::cerr << "Prepare to delete!!!!!" << std::endl;
}

void DataProvider::initDataSourceConnect()
{
    if( dataSource == nullptr )
    {
        dataSource = std::shared_ptr<DataSourceConnect>(new DataSourceConnect(*this->logger, nullptr));
        connect(dataSource.get(), SIGNAL(signalNewDataRecieved(int, std::vector<uint8_t>,size_t)),
                this, SLOT(slotNewDataRecieved(int,std::vector<uint8_t>, size_t)));
    }
}

void DataProvider::slotNewDataRecieved(int command, std::vector<uint8_t> data, size_t size)
{
    if( isPrepareToDelete )
    {
        std::cerr << "Clear data Waiters start" << std::endl;
        dataWaiters.clear();
        std::cerr << "Clear data Waiters" << std::endl;
        return;
    }

    logger->Message(LOG_DEBUG, __LINE__, __func__, "[Command %x] Recieved data by signal size %i",
                    command, size);

    auto dataWaiterList = dataWaiters.find(command);

    if( dataWaiterList != dataWaiters.end() )
    {
        for( auto elem = dataWaiterList->second.begin(); elem != dataWaiterList->second.end(); elem++ )
        {
            if( elem->get() != nullptr )
            {
                logger->Message(LOG_DEBUG, __LINE__, __func__, "[Command %i] Send Datawaiter data callback %p",
                                command, elem->get());
                elem->get()->recieveData(command, data, size);
            }
        }
    }
    else
    {
        logger->Message(LOG_INFO, __LINE__, __func__, "[Command %i] Datawaiter list not found",
                        command);
    }
}


