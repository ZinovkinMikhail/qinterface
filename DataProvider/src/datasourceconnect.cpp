#include <assert.h>

#include <axis_crc.h>
#include <axis_softadapter.h>

#include "datasourceconnect.h"
#include "dataprovidererror.h"

using namespace rosslog;

DataSourceConnect::DataSourceConnect(rosslog::Log &log, QObject *parent) :
    QObject(parent),
    logger(&log),
    socketAdapter(nullptr)
{
    assert(logger);
    dataVector.reserve(1000);
    qRegisterMetaType<std::vector<uint8_t>>("std::vector<uint8_t>");
    qRegisterMetaType<uint8_t>("uint8_t");
    qRegisterMetaType<size_t>("size_t");
}

DataSourceConnect::~DataSourceConnect()
{}

bool DataSourceConnect::startConnection()
{
    initSocketAdapter();
    socketAdapter->connectAdapter();
    return false;
}

void DataSourceConnect::closeConnection()
{
    logger->Message(LOG_DEBUG, __LINE__, __func__, "Not implemented yet");
    socketAdapter->disconnect();
}

bool DataSourceConnect::sendData(int command, std::vector<uint8_t> &data, size_t dataSize)
{
    SoftRecordeExchangeStruct_t exchange;
    memset(&exchange, 0, sizeof(SoftRecordeExchangeStruct_s));

    if( dataSize > BLOCK_SIZE )
    {
        //Now we pack single packet Data in vector
        //if we want to send multi block will pack SoftRecordeExchangeStruct_s and sort here by command
        logger->Message(LOG_ERR, __LINE__, __func__, "Multi Block send is not implemented");
        return false;
    }

    if( dataSize <= (AXIS_SOFTRECORDER_EXCHANGE__MESSAGE_WORD_SIZE * 2)
            - (AXIS_SOFTRECORDER_EXCHANGE__DATA_WORD_OFFSET * 2 ) )
    {
        memcpy(&exchange.Data, reinterpret_cast<uint16_t*>(data.data()), dataSize);

        if( !createExchangePacket(command, reinterpret_cast<uint8_t*>(&exchange), sizeof(SoftRecordeExchangeStruct_t)) )
            return false;

        //t logger->Message(LOG_DEBUG, __LINE__, __func__, "Command %x %i", command, sizeof(exchange));

#ifdef MASTER
        return sendPacketToAdapter(command, reinterpret_cast<uint8_t*>(&exchange), sizeof(SoftRecordeExchangeStruct_t), SOCKET_IN);
#else
        return sendPacketToAdapter(command, reinterpret_cast<uint8_t*>(&exchange), sizeof(SoftRecordeExchangeStruct_t), SOCKET_OUT);
#endif
    }
    else
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Multi block send is not implemented %i", dataSize);
    }
    return false;
}

bool DataSourceConnect::sendDataInterrogation(int command, std::vector<uint8_t> &data, size_t dataSize)
{
    SoftRecordeExchangeStruct_t exchange;
    memset(&exchange, 0, sizeof(SoftRecordeExchangeStruct_s));

    if( dataSize > BLOCK_SIZE )
    {
        //Now we pack single packet Data in vector
        //if we want to send multi block will pack SoftRecordeExchangeStruct_s and sort here by command
        logger->Message(LOG_ERR, __LINE__, __func__, "Multi Block send is not implemented");
        return false;
    }

    if( dataSize <= (AXIS_SOFTRECORDER_EXCHANGE__MESSAGE_WORD_SIZE * 2)
            - (AXIS_SOFTRECORDER_EXCHANGE__DATA_WORD_OFFSET * 2 ) )
    {
        memcpy(&exchange.Data, reinterpret_cast<uint16_t*>(data.data()), dataSize);

        if( !createExchangePacket(command, reinterpret_cast<uint8_t*>(&exchange), sizeof(SoftRecordeExchangeStruct_t)) )
            return false;

        logger->Message(LOG_DEBUG, __LINE__, __func__, "Command %x %i", command, sizeof(exchange));

        return sendPacketToAdapter(command, reinterpret_cast<uint8_t*>(&exchange), sizeof(SoftRecordeExchangeStruct_t), SOCKET_OUT);
    }
    else
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Multi block send is not implemented %i", dataSize);
    }
    return false;
}

bool DataSourceConnect::getConnectionState()
{
    if( socketAdapter != nullptr )
    {
        if( socketAdapter->getInConnectStatus() &&
                socketAdapter->getOutConnectStatus() )
            return true;
    }
    return false;
}

size_t DataSourceConnect::readData(int command, std::vector<uint8_t> &data)
{
    size_t sizeRecv = 0;

    if( socketAdapter != nullptr )
    {
        SoftRecordeExchangeStruct_t exchange;
        memset(&exchange, 0, sizeof(SoftRecordeExchangeStruct_t));

        uint8_t *dataRecv = reinterpret_cast<uint8_t*>(&exchange);

        sizeRecv = socketAdapter->readData(dataRecv, sizeof(SoftRecordeExchangeStruct_t));

        if( sizeRecv > 0 )
        {
            //logger->Message(LOG_DEBUG, __LINE__, __func__, "Recieved data size %i", sizeRecv);


                if( checkPacketHeader(dataRecv, sizeRecv, command) == SINGLE_PACKET )
                {
                    if( sizeRecv <= VECTOR_MAX_SIZE )
                    {
                        uint8_t *readyData = reinterpret_cast<uint8_t*>(data.data());

                        if( readyData != nullptr )
                        {
                            memcpy(readyData, exchange.Data, sizeof(exchange.Data));
                            //logger->Message(LOG_DEBUG, __LINE__, __func__, "Data Size recieved = %i", sizeof(exchange.Data));
                            //*readyData = *reinterpret_cast<uint8_t*>(exchange.Data);
                        }
                        else
                        {
                            logger->Message(LOG_ERR, __LINE__, __func__, "Data is null can't fill by recieved");
                            return 0;
                        }
                        return sizeRecv - (AXIS_SOFTRECORDER_EXCHANGE__DATA_WORD_OFFSET * 2);
                    }
                }
                else
                {
                    //We will pack readed data by SoftRecordeExchangeStruct_t in data vector
                    throw std::system_error(dpr::MULTIPACKET,
                                            std::generic_category());
                }

        }
    }
    return sizeRecv;
}

bool DataSourceConnect::recieveData(std::vector<uint8_t> &data, size_t size)
{
    SoftRecordeExchangeStruct_t *exchangeData = reinterpret_cast<SoftRecordeExchangeStruct_t*>(data.data());
    int command = -1;

    try
    {
        PacketType type = checkPacketHeader(data.data(), size, command);


        if( type == SINGLE_PACKET )
        {
            std::vector<uint8_t> readyData;
            readyData.resize(BLOCK_SIZE);

//            if( !workHandledPacketByInterrogation(readyData, command, size) )
//            {
//                logger->Message(LOG_ERR, __LINE__, __func__, "Unable to parse packet by command");
//                return false;
//            }

#ifndef MASTER
            if( !sendPacketToAdapter( command, data.data(), size, SOCKET_IN ) )
            {
                logger->Message(LOG_ERR, __LINE__, __func__, "Error while sending answer!");
                //return false;
#warning RETURN FALSE?
            }
#endif
            size_t dataSize = size - (AXIS_SOFTRECORDER_EXCHANGE__DATA_WORD_OFFSET * 2);
            uint8_t *dataPtr =  readyData.data();
            memcpy(dataPtr, exchangeData->Data, dataSize);
            logger->Message(LOG_DEBUG, __LINE__, __func__, "We recieved command emit # %x",command);
            emit signalNewDataRecieved(command, readyData , dataSize);


        }
        else if( type == MULTI_PACKET )
        {
#warning SORRY NOT IMPLEMENTED YET
            return false;
        }
    }
    catch(std::exception &except)
    {
        logger->Message(LOG_DEBUG, __LINE__, __func__, "%s", except.what());
        return false;
    }

    return true;
}

void DataSourceConnect::initSocketAdapter()
{
    if( socketAdapter == nullptr )
    {
        socketAdapter = std::unique_ptr<UnixSocketAdapter>(new UnixSocketAdapter(*this->logger, "/tmp"));
        socketAdapter->setGA(shared_from_this());
    }
}

void DataSourceConnect::checkExchange(uint16_t *data, uint16_t cmd, size_t size)
{
//    if( !data )
//        throw dpr::provider_error(dpr::INVALID_DATA);

    //throw dpr::provider_error();
    if( size != sizeof( SoftRecordeExchangeStruct_t ) )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Invlaid data size %zu (%zu)" ,
                        size, sizeof( SoftRecordeExchangeStruct_t ));

        throw std::system_error(dpr::INVALID_DATA_SIZE, std::generic_category());
    }

    if( data[AXIS_DISPATCHER_EXCHANGE__ID_WORD_OFFSET] != AXIS_ADAPTER_EXCHANGE__ID )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Invalid ID %04x (%04x)",
            data[ AXIS_DISPATCHER_EXCHANGE__ID_WORD_OFFSET ], AXIS_ADAPTER_EXCHANGE__ID);
        throw std::system_error(dpr::INVALID_PACKET_ID, std::generic_category());
    }

    if( cmd && (data[ AXIS_SOFTRECORDER_EXCHANGE__CMD_WORD_OFFSET ] & ~AXIS_DISPATCHER_CMD_DIR) !=
        (cmd & ~AXIS_DISPATCHER_CMD_DIR) )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Invalid Command %04x (%04x)",
         data[ AXIS_SOFTRECORDER_EXCHANGE__CMD_WORD_OFFSET ], cmd );

        if( data[ AXIS_SOFTRECORDER_EXCHANGE__CMD_WORD_OFFSET ] == AXIS_INTERFACE_CMD_ERROR )
        {
            throw std::system_error(dpr::ERROR_COMMAND, std::generic_category());
        }
        throw std::system_error(dpr::INVALID_PACKET_COMMAND, std::generic_category());
    }

    if( data[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET] )
    {
        uint16_t crc = 	data[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET];
        data[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET] = 0x0000;

        uint16_t ncrc =
            axis_crc16( data, AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE);

        if( crc != ncrc )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Invalid CRC %x (%x)\n",
                 crc, ncrc );
            throw std::system_error(dpr::INVALID_PACKET_CRC,
                                    std::generic_category());
        }
    }
    else
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "No CRC check from buffer\n");
    }
}


bool DataSourceConnect::workHandledPacketByInterrogation(std::vector<uint8_t> &readyData,
                                                           int cmd, size_t size)
{
    Q_UNUSED(readyData);
    Q_UNUSED(size);
    //TODO Check if we have data in interrogation cmds
    switch( cmd )
    {
        case AXIS_INTERFACE_EVENT_SAVE_MODE:
        {
            break;
        }
        case AXIS_INTERFACE_EVENT_READ_STATUS:
        {
            break;
        }
        case AXIS_DISPATCHER_CMD_STOP_COPY_TO_FILE:
        {
            break;
        }
        case AXIS_INTERFACE_EVENT_RECORDER_OUT:
        {
            break;
        }
        case AXIS_INTERFACE_EVENT_RECORDER_IN:
        {
            break;
        }
        case AXIS_INTERFACE_EVENT_READ_RECORDER_ERROR:
        {
            break;
        }
        case AXIS_INTERFACE_CMD_GO_SLEEP:
        {
            break;
        }
        case AXIS_ADAPTER_CMD_GET_ACC_STATE:
        {
            break;
        }
        case AXIS_INTERFACE_EVENT_READ_STATUS_REC:
        {
            break;
        }
    default:
        break;
    }
    return true;
}



// Parse commands channel from adapter
bool DataSourceConnect::parsePacketByCmd(std::vector<uint8_t> &readyData,
                                         uint16_t *data, uint16_t cmd, size_t size)
{
    Q_UNUSED(readyData);
    Q_UNUSED(data);
    Q_UNUSED(cmd);
    Q_UNUSED(size);
    switch( cmd )
    {
        default:
            break;
    }
    return false;
}

bool DataSourceConnect::sendPacketToAdapter(int command, uint8_t *data, size_t dataSize, SocketType type)
{
    if( socketAdapter == nullptr  )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Socket adapter is not inited");
        return false;
    }

    uint8_t blocksNum;
    int ret = 0;
    int crcOffset = 0;

    uint16_t *bytes = reinterpret_cast<uint16_t*>(data);

    if( !bytes )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send . data is empty");
        return false;
    }

    ret = getNumberOfPacketBlocks(bytes);
    if(ret <= 0 )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Unable to get number of blocks");
        return false;
    }
    blocksNum = ret;

    if( ret > 1 )
    {
        command = bytes[ AXIS_DISPATCHER_EXCHANGE_EXT__CMD_WORD_OFFSET ];
        crcOffset = AXIS_DISPATCHER_EXCHANGE_EXT__CRC_WORD_OFFSET;
    }
    else
    {
        command = bytes[ AXIS_SOFTRECORDER_EXCHANGE__CMD_WORD_OFFSET ];
        crcOffset = AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET;
    }

    if( !command )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "[%s]Invalid command from buff cmd = %i",__func__, command);
        //TODO CHECK WHY NOT RET
    }

    if( dataSize != blocksNum * sizeof( SoftRecordeExchangeStruct_t ))
    {
        logger->Message(LOG_ERR, __LINE__, __func__,"Invalid data size for send %zu (%zu)",
                                    dataSize, sizeof( SoftRecordeExchangeStruct_t ));
        return false;
    }

    for( int i = 0; i < blocksNum; i++ )
    {
        uint16_t *buff = bytes + i * AXIS_DISPATCHER_EXCHANGE__MESSAGE_WORD_SIZE;
        buff[ crcOffset ] = 0x0000;
        buff[ crcOffset ] =
                    axis_crc16( buff, AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE );


        if( !socketAdapter->sendData(reinterpret_cast<uint8_t*>(buff),
                            sizeof( SoftRecordeExchangeStruct_t ), type) )
        {
            logger->Message(LOG_ERR, __LINE__, __func__,"Unable to send block %i", i);
            return false;
        }
    }
    return true;
}

PacketType DataSourceConnect::checkPacketHeader(uint8_t *data, size_t size, int &command)
{
    uint16_t *words = reinterpret_cast<uint16_t*>(data);

    if( size > 0 && words )
    {
        int blocksNum = 1;
        int ret = 0;
        for( int i = 0; i < blocksNum; i++ )
        {
            if( !i )
            {
                ret = getNumberOfPacketBlocks(words);
                if( ret <= 0 )
                {
                    logger->Message(LOG_ERR, __LINE__, __func__, "No data about num of blocks or id is not implemented");
                    throw std::system_error(dpr::INVALID_NUM_BLOCK_DATA,
                                            std::generic_category());
                }
                blocksNum = ret;

                if( ret > SOCKET_MAX_READ_BLOCK )
                {
                    logger->Message(LOG_ERR, __LINE__, __func__, "We recieved %i / %i blocks this is more than need",
                                               blocksNum , SOCKET_MAX_READ_BLOCK);
                    throw std::system_error(dpr::INVALID_NUM_OF_BLOCKS,
                                            std::generic_category());
                }
                //t logger->Message(LOG_DEBUG, __LINE__, __func__, "We recieved %i blocks", blocksNum);
            }

            if( blocksNum > 1 )
            {
                logger->Message(LOG_ERR, __LINE__, __func__,
                                "Recieved more than one block %i Sorry not implemented =(",
                                blocksNum);
                if( command < 0 )
                {
                    command = words[AXIS_DISPATCHER_EXCHANGE_EXT__CMD_WORD_OFFSET];
                }
                return MULTI_PACKET;
            }
            else
            {
                if( command < 0 )
                {
                    command = words[AXIS_SOFTRECORDER_EXCHANGE__CMD_WORD_OFFSET];
                }

                checkExchange(words, command, sizeof(SoftRecordeExchangeStruct_s));
                return SINGLE_PACKET;
            }
        }
    }

    throw std::system_error(dpr::INVALID_PACKET_HEADER,
                            std::generic_category());
}

bool DataSourceConnect::createExchangePacket(int command, uint8_t *data, size_t size)
{
    uint16_t *words = reinterpret_cast<uint16_t*>(data);

    if( !words || !command )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Data or command is invalid!");
        return false;
    }

    if( size != sizeof(SoftRecordeExchangeStruct_t) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid data size %zu (%zu)!",
                              size, sizeof(SoftRecordeExchangeStruct_t));
        return false;
    }
    words[AXIS_DISPATCHER_EXCHANGE__ID_WORD_OFFSET] = AXIS_ADAPTER_EXCHANGE__ID;
    words[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET] = 0x0000;
    words[AXIS_SOFTRECORDER_EXCHANGE__CMD_WORD_OFFSET] = command;
    words[AXIS_SOFTRECORDER_EXCHANGE__CRC_WORD_OFFSET]	=
        axis_crc16(words, AXIS_ADAPTER_EXCHANGE__MESSAGE_WORD_SIZE);
    return true;
}

int DataSourceConnect::getNumberOfPacketBlocks(uint16_t *data)
{
    int blocks = 0;

    if( !data )
        return -1;

    switch( data[AXIS_DISPATCHER_EXCHANGE_EXT__ID_WORD_OFFSET] )
    {
        case AXIS_DISPATCHER_EXCHANGE_BLOKS__ID:
        {
            blocks =
                (data[ AXIS_SOFTRECORDER_EXCHANGE_EXT__BLOCK_COUNTER_WORD_OFFSET ] >> 8 );

            break;
        }
        case AXIS_SOFTRECORDER_EXCHANGE__ID:
        {
            blocks = 1;
            break;
        }
        default:
        {
            return -1;
        }
    }
    return blocks;
}

