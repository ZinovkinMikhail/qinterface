#ifndef RECORDERSETTINGSCONTOL_H
#define RECORDERSETTINGSCONTOL_H

#include <QObject>

#include <axis_softrecorder.h>
#include <rosslog/log.h>

#include "dataprovider.h"
#include "adaptercommands.h"

class RecorderSettingsContol : public QObject
{
    Q_OBJECT
public:
    explicit RecorderSettingsContol(std::shared_ptr<DataProvider> dataProvider,
                                    rosslog::Log &log, QObject *parent = nullptr);

    void updateStatus() noexcept(false);
    SoftRecorderSettingsStruct_t getRecordSettings();
    void applyRecordSettings(SoftRecorderSettingsStruct_t settings) noexcept(false);
    void saveRecordSettings(SoftRecorderSettingsStruct_t settings) noexcept(false);
    void updateTimers() noexcept(false);
    DspRecorderExchangeTimerSettingsStruct_t getRecorderTimers();
    void saveRecorderTimers(DspRecorderExchangeTimerSettingsStruct_t timers) noexcept(false);

private:
    std::shared_ptr<DataProvider> dataProvider;
    AdapterCommands adapterCommands;
    SoftRecorderSettingsStruct_t recorderSettings;
    DspRecorderExchangeTimerSettingsStruct_t  recorderTimers;
    rosslog::Log *logger;

    std::vector<uint8_t> dataReserv;

signals:

public slots:
};

#endif // RECORDERSETTINGSCONTOL_H
