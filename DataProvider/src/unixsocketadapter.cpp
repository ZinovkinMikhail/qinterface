#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <string>
#include <string.h>
#include <assert.h>
#include <iostream>

#include <axis_crc.h>
#include <rosslog/log.h>

#include "unixsocketadapter.h"

using namespace rosslog;
using namespace UnixAdapter;

UnixSocketAdapter::UnixSocketAdapter(rosslog::Log &log,
                                     const std::string socketPath) :
    threadState(THREAD_DISABLED),
    logger(&log),
    errorCount(0)
{
    assert(logger);
    createSocket(this->socketDataIn, socketPath + "/" + SOCKET_PATH_IN);
    createSocket(this->socketDataOut, socketPath + "/" + SOCKET_PATH_OUT );
}

UnixSocketAdapter::~UnixSocketAdapter()
{
    threadWork = false;
    if( mainThread.joinable() )
    {
        mainThread.join();
    }
    std::cerr << "Remove UnixSocketAdapter" << std::endl;
}

bool UnixSocketAdapter::connectAdapter()
{
    if( threadState == THREAD_DISABLED )
    {
        return startThread(); //BOOL
    }
    if( threadState == THREAD_ERROR )
    {

    }
    return true;
}

void UnixSocketAdapter::disconnect()
{
    if( socketDataIn.socketState == SOCKET_CONNECTED )
    {
        shutdown(socketDataIn.socketFd, SHUT_RDWR);
        close(socketDataIn.socketFd);
        socketDataIn.socketState = SOCKET_NOT_OPENED;
    }
    if( socketDataOut.socketState == SOCKET_CONNECTED )
    {
        shutdown(socketDataOut.socketFd, SHUT_RDWR);
        close(socketDataOut.socketFd);
        socketDataOut.socketState = SOCKET_NOT_OPENED;
    }
    return;
}

void UnixSocketAdapter::disconnectClient()
{
    if( socketDataIn.socketState == SOCKET_CONNECTED )
    {
        shutdown(socketDataIn.socketClientFd, SHUT_RDWR);
        close(socketDataIn.socketClientFd);
        socketDataIn.socketClientFd = -1;
    }
    if( socketDataOut.socketState == SOCKET_CONNECTED )
    {
        shutdown(socketDataOut.socketClientFd, SHUT_RDWR);
        close(socketDataOut.socketClientFd);
        socketDataIn.socketClientFd = -1;
    }
    return;
}

bool UnixSocketAdapter::startListenSocket()
{
    return false;
}

bool UnixSocketAdapter::sendData(uint8_t *data, size_t size, SocketType type)
{
    SocketData *currentSocket = NULL;
    if( type == SOCKET_IN )
        currentSocket = &socketDataIn;
    else
        currentSocket = &socketDataOut;

    if( !currentSocket  )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Socket is not inited");
        return false;
    }

#ifdef MASTER
    if( currentSocket->socketClientFd > 0 )
    {
        SocketData clientData;
        clientData  = *currentSocket;
        clientData.socketFd = currentSocket->socketClientFd;
        return this->writeSocket(&clientData, data, size , SOCKET_WRITE_TIMEOUT);
    }
    return false;

#else
   // readSocket(&socketDataOut, *data, size, 1);
    return this->writeSocket(currentSocket, data, size , SOCKET_WRITE_TIMEOUT);
#endif

}

size_t UnixSocketAdapter::readData(uint8_t *data, size_t size)
{
#ifdef MASTER
    if( socketDataOut.socketClientFd > 0 )
    {
        SocketData clientData;
        clientData = socketDataOut;
        clientData.socketFd = socketDataOut.socketClientFd;
        return readSocket(&clientData, *data, size, SOCKET_READ_TIMEOUT);
    }
    return 0;
#endif
    return readSocket(&socketDataOut, *data, size, SOCKET_READ_TIMEOUT);
}

bool UnixSocketAdapter::getInConnectStatus()
{
    if( socketDataIn.socketState == SOCKET_CONNECTED )
        return true;
    return false;
}

bool UnixSocketAdapter::getOutConnectStatus()
{
    if( socketDataOut.socketState == SOCKET_CONNECTED )
        return true;
    return false;
}

void UnixSocketAdapter::setGA(std::shared_ptr<IDataReciever> dataReciever)
{
    this->dataReciever = dataReciever;
}

int UnixSocketAdapter::getErrorsCount()
{
    return errorCount;
}


bool UnixSocketAdapter::checkSocketWork(SocketType type)
{
    if( !this->openSocket(type) )
        return false;

#ifdef SLAVE
    if( !this->connectSocket(type ))
        return false;
#else
    if( !this->bindSocket(type) )
        return false;
#endif

    return true;
}

bool UnixSocketAdapter::openSocket(SocketType socketType)
{
    SocketData *currentSocket = NULL;

    if( socketType == SOCKET_IN )
        currentSocket = &socketDataIn;
    else
        currentSocket = &socketDataOut;

    if( currentSocket->socketState == SOCKET_NOT_OPENED  )
    {
#ifdef MASTER
        if( remove(currentSocket->socketPath.c_str()) == -1 && errno != ENOENT )
            return false;
#endif
        printf("OPEN SOCKET %s \n", currentSocket->socketPath.c_str());
        int fd = socket(AF_UNIX, SOCK_STREAM, 0);
        if( fd == -1 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Unable to open socket path %s errno = %i",
                                       currentSocket->socketPath.c_str(), errno);
        }
        currentSocket->socketState = SOCKET_NOT_CONNECTED;
        currentSocket->socketFd = fd;
        return true;
    }
    return true;
}

bool UnixSocketAdapter::bindSocket(SocketType socketType)
{
    SocketData *currentSocket;

    if( socketType == SOCKET_IN )
        currentSocket = &socketDataIn;
    else
        currentSocket = &socketDataOut;

    if( currentSocket->socketState == SOCKET_NOT_CONNECTED )
    {

        //unlink(currentSocket->socketPath.c_str());
        struct sockaddr_un addr;
        memset(&addr, 0, sizeof(struct sockaddr_un));
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, currentSocket->socketPath.c_str(), sizeof(addr.sun_path) -1);
        addr.sun_path[ sizeof( addr.sun_path ) - 1 ] = 0x00;
        if( bind( currentSocket->socketFd, (struct sockaddr *)&addr, sizeof(addr)) < 0 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Can't bind unix socket errno = %i", errno);
            close(currentSocket->socketFd);
            return false;
        }

        if( listen( currentSocket->socketFd, 2 ) )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Can't listen unix socket= %i", errno);
            close(currentSocket->socketFd);
            return false;
        }

        logger->Message(LOG_INFO, __LINE__, __func__, "New socket server");
        currentSocket->socketState = SOCKET_CONNECTED;
    }
    return true;
}

bool UnixSocketAdapter::connectSocket(SocketType socketType)
{
    SocketData *currentSocket;

    if( socketType == SOCKET_IN )
        currentSocket = &socketDataIn;
    else
        currentSocket = &socketDataOut;

    if( currentSocket->socketState == SOCKET_NOT_CONNECTED )
    {
        struct sockaddr_un addr;
        memset(&addr, 0, sizeof(struct sockaddr_un));
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, currentSocket->socketPath.c_str(), sizeof(addr.sun_path) -1);

        printf("CONNECT SOCKET %s \n", currentSocket->socketPath.c_str());
        if( connect(currentSocket->socketFd, (struct sockaddr*) &addr, sizeof(struct sockaddr_un) ) == -1 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Unable to connect socket errno = %i", errno);
            return false;
        }
        currentSocket->socketState = SOCKET_CONNECTED;

        logger->Message(LOG_INFO, __LINE__, __func__, "New socket client");
    }
    return true;
}

bool UnixSocketAdapter::acceptClient(SocketData *socketData)
{
    int fd = 0;
    if( socketData->socketClientFd < 0 )
    {
        fd = accept(socketData->socketFd, NULL, NULL );
        if( fd < 0  )
        {
            logger->Message(LOG_DEBUG, __LINE__, __func__, "No client for accept or error = %i", errno );
            return false;
        }
        socketData->socketClientFd = fd;
        logger->Message(LOG_INFO, __LINE__, __func__, "New client accepted");
    }
    return true;
}

int UnixSocketAdapter::writeSocket(SocketData *socketData, uint8_t *buff, size_t size, int timeout)
{
    fd_set wfds;
    struct timeval tv;
    int ret = 0;
    int writed = 0;
    if( !socketData )
        return -1;

    if( socketData->socketFd < 0 )
    {
        logger->Message(LOG_ERR, __LINE__, __func__, "Socket fd is unavailable");
        return -1;
    }

    FD_ZERO(&wfds);
    FD_SET(socketData->socketFd, &wfds );

    tv.tv_sec = timeout;
    tv.tv_usec = 0;

    ret = select( socketData->socketFd + 1, NULL, &wfds, NULL, &tv);

    if( ret < 0 )
    {
        errorCount++;
        logger->Message(LOG_ERR, __LINE__, __func__, "Select error errno = %i", errno);
        return -1;
    }
    else if( !ret )
    {
        errorCount++;
        logger->Message(LOG_INFO, __LINE__, __func__, "Timeout to write partner");
        return -1;
    }
    else if( FD_ISSET( socketData->socketFd, &wfds ) )
    {
        writed = send(socketData->socketFd, buff, size, MSG_NOSIGNAL);

        if( writed <= 0 )
        {
            errorCount++;
            logger->Message(LOG_ERR, __LINE__, __func__, "Can't read from client");
            return -1;
        }
    }
    errorCount = 0;
    return writed;
}

size_t UnixSocketAdapter::readSocket(SocketData *socketData, uint8_t &buff, size_t size, int timeout)
{
    fd_set rfds;
    struct timeval tv;
    int ret = 0;
    int readed = 0;
    if( socketData->socketFd < 0 )
        return -1;

    FD_ZERO(&rfds);
    FD_SET(socketData->socketFd, &rfds );

    tv.tv_sec = timeout;
    tv.tv_usec = 0;

    ret = select( socketData->socketFd + 1, &rfds, NULL, NULL, &tv);
    if( ret < 0 )
    {
        errorCount++;
        logger->Message(LOG_ERR, __LINE__, __func__, "Select error errno = %i", errno);
        return 0;
    }
    else if( !ret )
    {
        errorCount++;
        logger->Message(LOG_INFO, __LINE__, __func__, "Timeout to read from partner");
        return 0;
    }
    else if( FD_ISSET( socketData->socketFd, &rfds ) )
    {
        readed = read(socketData->socketFd, &buff, size);

        if( readed <= 0 )
        {
            errorCount++;
            logger->Message(LOG_ERR, __LINE__, __func__, "Can't read from socket");
            return 0;
        }
    }
    errorCount = 0;
    return readed;
}

size_t UnixSocketAdapter::readBlockFromAdapter(SocketType socketType)
{
    SocketData *currentSocket = NULL;
    size_t ret = 0;

    if( socketType == SOCKET_IN )
    {
        currentSocket = &socketDataIn;
    }
    else
    {
        currentSocket = &socketDataOut;
    }
    uint8_t *buffer = currentSocket->dataVector.data();



    if( currentSocket != nullptr && currentSocket->socketState == SOCKET_CONNECTED )
    {
#ifdef MASTER
        SocketData clientData;
        clientData = *currentSocket;
        clientData.socketFd = currentSocket->socketClientFd;
        ret = this->readSocket(&clientData, *buffer,
                                   sizeof(SoftRecordeExchangeStruct_s), SOCKET_READ_TIMEOUT);
#else
        ret = this->readSocket(currentSocket, *buffer,
                                   BLOCK_SIZE, SOCKET_READ_TIMEOUT);
#endif
        if( ret > 0 && socketType == SOCKET_IN )
        {
            if( ret <= VECTOR_MAX_SIZE )
            {
                dataReciever->recieveData(currentSocket->dataVector, ret);
            }
        }
    }
    return ret;
}

void UnixSocketAdapter::masterWork()
{
    if( this->acceptClient(&socketDataIn) )
    {
    }
    if( this->acceptClient(&socketDataOut) )
    {
    }

    this->readBlockFromAdapter(SOCKET_IN);
}



void UnixSocketAdapter::threadWorkFunction()
{
    while(this->threadWork)
    {
        if( !this->checkSocketWork(SOCKET_IN ) )
        {
            sleep(1);
            continue;
        }

        if( !this->checkSocketWork(SOCKET_OUT ) )
        {
            sleep(1);
            continue;
        }
#ifdef SLAVE
        this->readBlockFromAdapter(SOCKET_IN);
        if( this->getErrorsCount() > 10 )
        {
            this->disconnect();
        }
#else
        this->masterWork();
        if( this->getErrorsCount() > 10 )
        {
            this->disconnectClient();
        }
#endif
        usleep(200000);
        //sleep(1);
    }
    return;
}

bool UnixSocketAdapter::startThread()
{
    mainThread = std::thread(&UnixSocketAdapter::threadWorkFunction, this);
    threadState = THREAD_ENABLED;
//    int ret = pthread_create(&thread, NULL, threadWorkFunction, this);
//    if( ret != 0  )
//    {
//        logger->Message(LOG_ERR, __LINE__, __func__, "Unable to start thread, thread error num = %i", ret);
//        return false;
//    }
    return true;
}

void UnixSocketAdapter::createSocket(SocketData &socketData, const std::string &socketPath)
{
    socketData.socketFd = -1;
    socketData.socketPath = socketPath;
    socketData.socketState = SOCKET_NOT_OPENED;
    socketData.socketClientFd = -1;
    socketData.dataVector.resize(BLOCK_SIZE);
}

std::future<void> UnixSocketAdapter::listenWork()
{
    return std::future<void>();
}
