#include <assert.h>
#include <axis_softadapter.h>
#include <axis_softrecorder.h>

#include "testdatawaiter.h"

#include "adaptercommands.h"
#include "testscommon.h"

using namespace rosslog;

TestDataWaiter::TestDataWaiter(std::shared_ptr<DataProvider> dataProvider) :
    dataProvider(dataProvider)
{

}

TestDataWaiter::TestDataWaiter(std::shared_ptr<DataProvider> dataProvider, Log &log) :
    dataProvider(dataProvider),
    logger(&log)
{
    memset(&recorderSettings , 0, sizeof(SoftRecorderSettingsStruct_t));
    memset(&recorderStatus, 0, sizeof(SoftRecorderStatusStruct_t));

    recorderStatus.TotalRecSecs = 0;
    recorderStatus.FullRecSecs = 0;
    recorderStatus.FlashCapacity = 0;
    recorderStatus.FlashUsed = 0;
}

void TestDataWaiter::setMemoryData(uint64_t capacity, uint64_t curCapacity,
                                   uint64_t record, uint64_t totatRecord)
{
    recorderStatus.TotalRecSecs = record;
    recorderStatus.FullRecSecs = totatRecord;
    recorderStatus.FlashCapacity = capacity;
    recorderStatus.FlashUsed = curCapacity;
}

bool TestDataWaiter::addWaiterTest(int command)
{
    return dataProvider->addDataWaiter(command, shared_from_this());
}

TestDataWaiter::~TestDataWaiter()
{
    //DataProviderDebug::Message(INFO,"TestDataWaiter: Destructor");
}

void TestDataWaiter::recieveData(int command, std::vector<uint8_t> data,
                                 size_t size)
{
    //We need here log for test
    assert(logger);

    bool answered = false;

    static bool recordSettingsReaded = false;
    static bool recordStatusReaded = false;
    //

    switch(static_cast<EAdapterCommands>(command))
    {
        case LPC:
        {
            uint32_t timeout = 0;
            if( !data.empty() && size > 0 )
            {
                memcpy(&timeout, data.data(), sizeof(uint32_t));
                if( timeout != TEST_WAIT_SLEEP_TIMEOUT )
                {
                    this->logger->Message(LOG_ERR, "Test do not  pass Expected timeout = %i, recieved %i",
                                          TEST_WAIT_SLEEP_TIMEOUT, timeout);
                    abort();
                }
                this->logger->Message(LOG_INFO, "Test passed Expected timeout = %i, recieved %i",
                                      TEST_WAIT_SLEEP_TIMEOUT, timeout);
            }
            else
            {
                this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                abort();
            }
            break;
        }
        case GET_ADAPTER_ACC_STATUS:
        {
            if( !data.empty() && size > 0 )
            {
                uint8_t buff[BLOCK_SIZE];
                memset(&buff, 0, BLOCK_SIZE);
                memcpy(&buff[AXIS_SOFTRECORDER_EXCHANGE__DATA_WORD_OFFSET * 2], data.data(),
                        size);

                SoftRecorderGetAccStateStruct_t dataStruct = *reinterpret_cast<SoftRecorderGetAccStateStruct_t*>(&buff);
                this->logger->Message(LOG_INFO,__LINE__, __func__, "Test Get adapter ACC Voltage = %x", dataStruct.Voltage);
            }
            else
            {
                this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                abort();
            }
            break;
        }
        case GET_RECORDER_STATUS:
        {
            if( !data.empty() && size > 0 )
            {

                if( !recordStatusReaded )
                {
                    recorderStatus.Chanel1Status = AXIS_SOFTRECORDER_CONTROL__RECORD_SOURCE_REMOUTE;

                    this->logger->Message(LOG_INFO,__LINE__, __func__, "Test Get recoder status Alarm 1 = %x  Total secs = %i",
                                          recorderStatus.Alarm1, recorderStatus.TotalRecSecs);


                    recordStatusReaded = true;
                }
                memcpy(data.data(), &recorderStatus, sizeof(recorderStatus));

                dataProvider->sendCommand(command, data, size, false);
                answered = true;
            }
            else
            {
                this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                abort();
            }
            break;
        }
        case GET_RECORDER_SETTINGS:
        {
            if( !data.empty() && size > 0 )
            {
//                memcpy(&recorderSettings, data.data(),
//                        size);

                if( !recordSettingsReaded )
                {
                    recorderSettings.ChanelCfgMode = AXIS_SOFTRECORDER_SETTINGS__CHANEL_1_ENABLED |
                            AXIS_SOFTRECORDER_SETTINGS__CHANEL_2_ENABLED |
                            AXIS_SOFTRECORDER_SETTINGS__CHANEL_3_4_STEREO |
                            AXIS_SOFTRECORDER_SETTINGS__CHANEL_3_ENABLED |
                            AXIS_SOFTRECORDER_SETTINGS__CHANEL_4_ENABLED;
                    recorderSettings.ChanelBias = AXIS_SOFTRECORDER_SETTINGS__CHANEL_1_BIAS_ENABLE |
                            AXIS_SOFTRECORDER_SETTINGS__CHANEL_2_BIAS_ENABLE;
                    recorderSettings.Chanel1Sens = AXIS_SOFTRECORDER_SETTINGS__SENSE_05;
                    recorderSettings.Chanel2Sens = AXIS_SOFTRECORDER_SETTINGS__SENSE_20 ;
                    recorderSettings.Chanel3Sens = AXIS_SOFTRECORDER_SETTINGS__SENSE_40 ;

                    this->logger->Message(LOG_INFO,__LINE__, __func__, "Test Get recoder settings Chanel1AudioBand 1 = %x  softwareECC = %i",
                                          recorderSettings.Chanel1AudioBand, recorderSettings.softwareECC);

                    recordSettingsReaded = true;
                }
                else
                {
                    this->logger->Message(LOG_INFO,__LINE__, __func__, "Resend data");
                }

                memcpy(data.data(), &recorderSettings, sizeof(recorderSettings));
                dataProvider->sendCommand(command, data, size, false);
                answered = true;
            }
            else
            {
                this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                abort();
            }
            break;
        }
        case SAVE_RECORDER_SETTINGS:
        {
            if( !data.empty() && size > 0 )
            {
               // SoftRecorderSettingsStruct_t recorderSettings;
                int sizeDAta = sizeof(recorderSettings);
                memcpy(&recorderSettings, data.data(),
                        sizeDAta);

                this->logger->Message(LOG_INFO,__LINE__, __func__,
                                      "Test Get recoder settings Chanel1AudioBand 1 = %x  ChanelBias = %i",
                                      recorderSettings.Chanel1AudioBand, recorderSettings.ChanelBias);
            }
            else
            {
                this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                abort();
            }
            break;
        }
        case SAVE_ADAPTER_TIMERS:
        case GET_ADAPTER_TIMERS:
        case SAVE_RECORDER_TIMERS:
        case GET_RECORDER_TIMERS:
        {
            if( !data.empty() && size > 0 )
            {
               DspRecorderExchangeTimerSettingsStruct_t timers;

                memcpy(&timers.Timers, data.data(), sizeof(timers.Timers));

                this->logger->Message(LOG_INFO, __LINE__, __func__,
                                      "Recieved from client TIMERS 1 min = %i Timers 4 reserv = %i",
                                      timers.Timers[0].start.min , timers.Timers[4].stop.reserv_0);
            }
            else
            {
                this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                abort();
            }
            break;
        }
        case SAVE_ADAPTER_SETTINGS:
        case GET_ADAPTER_SETTINGS:
        {
            if( !data.empty() && size > 0 )
            {
                SoftAdapterSettingsStruct_t recorderSettings;
                memcpy(&recorderSettings, data.data(),
                        size);

                this->logger->Message(LOG_INFO,__LINE__, __func__, "Test GET_ADAPTER_SETTINGS Connect  = %i",
                                      recorderSettings.Connect);
            }
            else
            {
                this->logger->Message(LOG_ERR, "Test do not  pass no data recieved");
                abort();
            }
            break;
        }
        default:
            break;
    }
    switch (command)
    {
        case AXIS_INTERFACE_EVENT_RECORDER_OUT:
        {
            this->logger->Message(LOG_INFO,__LINE__, __func__, "Cicada is connected or disconnected by usb!!!");
            break;
        }
        default:
            break;
    }

    if( !answered )
    {
        this->dataProvider->sendCommand(command, data, size, false);
    }
}
