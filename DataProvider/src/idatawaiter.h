#ifndef IDATAWAITER_H
#define IDATAWAITER_H

#include <vector>
#include <stdint.h>

class IDataWaiter
{
public:
    virtual void recieveData(int command, std::vector<uint8_t> data,
                        size_t size) = 0;

    virtual ~IDataWaiter() {}
};

#endif // IDATAWAITER_H
