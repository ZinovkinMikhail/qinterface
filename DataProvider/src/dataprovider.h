#ifndef DATAPROVIDER_H
#define DATAPROVIDER_H

#include <map>
#include <memory>
#include <list>
#include <QObject>
#include <iostream>

#include <rosslog/log.h>

#include "idatawaiter.h"
#include "datasourceconnect.h"
#include "dataprovidererror.h"

#define DATAPROVIDER_LOG_FILE "/tmp/data_provider.log"

namespace data_provider
{
class DataProvider : public QObject
{
    Q_OBJECT
public:
    explicit DataProvider(rosslog::Log &log, QObject *parent = nullptr);
    ~DataProvider();
    bool startConnection();
    bool getConnectStatus();
    void sendCommand(int command, std::vector<uint8_t> &data, size_t &size,
                     bool waitAnswer) noexcept(false);
    void sendCommandInterrogation(int command, std::vector<uint8_t> &data, size_t &size,
                     bool waitAnswer);
    bool addDataWaiter(int commandWait, std::shared_ptr<IDataWaiter> dataWaiter);
    bool removeDataWaiter(int command, std::shared_ptr<IDataWaiter> dataWaiter);
    void prepareToDelete();

private:
    void initDataSourceConnect();

    rosslog::Log *logger;
    std::map<int, std::list<std::shared_ptr<IDataWaiter>>> dataWaiters;
    std::shared_ptr<DataSourceConnect> dataSource;
    std::mutex lock;
    bool isPrepareToDelete{false};

public slots:
    void slotNewDataRecieved(int command, std::vector<uint8_t> data, size_t size);
};
}

#endif // DATAPROVIDER_H
