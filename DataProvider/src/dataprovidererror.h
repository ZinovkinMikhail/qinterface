#ifndef DATAPROVIDERERROR_H
#define DATAPROVIDERERROR_H


#include <system_error>
#include <stdarg.h>
#include <map>

using namespace std;

namespace dpr
{

typedef enum
{
    INVALID_DATA,
    INVALID_PACKET_ID,
    INVALID_PACKET_COMMAND,
    INVALID_DATA_SIZE,
    INVALID_PACKET_CRC,
    INVALID_NUM_OF_BLOCKS,
    INVALID_NUM_BLOCK_DATA,
    INVALID_PACKET_HEADER,
    MULTIPACKET,
    NO_DATA_RECIEVED,
    ADAPTER_ANSWER_ERROR,
    ERROR_COMMAND,
    INVALID_PACKET_DATA
}ErrType;

class ProviderError
{
public:
    ProviderError();
    static ProviderError &getInstance();

    std::string getErrorString(std::system_error &error);

private:
    static ProviderError *instance;
    std::map<ErrType, std::string> errorString;
};

}

#endif // DATAPROVIDERERROR_H
