#include <iostream>

#include "dataprovidererror.h"

using namespace dpr;

ProviderError::ProviderError()
{
    errorString[INVALID_DATA] = "Данные недоступны";
    errorString[ERROR_COMMAND] = "Адаптер ответил ошибкой";
}

ProviderError &ProviderError::getInstance()
{
    static ProviderError provider;
    return provider;
}

std::string ProviderError::getErrorString(system_error &error)
{
    auto search = errorString.find(static_cast<ErrType>(error.code().value()));
    if (search != errorString.end())
    {
        return search->second;
    }
    else
    {
        return "Ошибка не найдена";
    }
}
