#include <assert.h>

#include "recorderstatuscontrol.h"

using namespace rosslog;
using namespace recorder_status_ctrl;

RecorderStatusControl::RecorderStatusControl(std::shared_ptr<DataProvider> dataProvider, rosslog::Log &log,
                                             QObject *parent) :
    QObject(parent),
    logger(&log),
    dataProvider(dataProvider)
{
    assert(dataProvider);
    assert(logger);
    dataReserv.resize(VECTOR_MAX_SIZE);
    memset(&recorderStatus, 0, sizeof(SoftRecorderStatusStruct_t));
}

void RecorderStatusControl::updateStatus()
{
    const size_t exceptedSize = sizeof(SoftRecorderStatusStruct_t);
    size_t size = 0;
    try
    {

#ifdef TESTS
        recorderStatus.Alarm1 = 0xff00;
        recorderStatus.TotalRecSecs = 777777;

        size = exceptedSize;

        SoftRecorderStatusStruct_t *dataPtr = reinterpret_cast<SoftRecorderStatusStruct_t*>(this->dataReserv.data());
        *dataPtr = recorderStatus;
        memset(&recorderStatus, 0, exceptedSize);
#endif
        this->adapterCommands.getRecorderStatusCommand(dataProvider, dataReserv, size);

        if( size >= exceptedSize && !dataReserv.empty() )
        {
            memcpy(&this->recorderStatus, dataReserv.data(), exceptedSize);
        }
        else
        {
            throw std::system_error(dpr::INVALID_PACKET_DATA, generic_category());
        }
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to update recorder status");
    }
}

SoftRecorderStatusStruct_t RecorderStatusControl::getRecorderStatus()
{
    return  this->recorderStatus;
}

void RecorderStatusControl::stopRecord(std::bitset<4> channels)
{
    Q_UNUSED(channels);
   SoftRecorderStatusStruct_t record = recorderStatus;
   SoftRecorderExchangeControlStruct_t pack;
   memset(&pack, 0, sizeof (SoftRecorderExchangeControlStruct_t));
   SoftRecorderControlStruct_t controlStruct;
   memset(&controlStruct, 0, sizeof (SoftRecorderControlStruct_t));

   controlStruct.Chanel1Control = recorderStatus.Chanel1Status;
   controlStruct.Chanel2Control = recorderStatus.Chanel2Status;
   controlStruct.Chanel3Control = recorderStatus.Chanel3Status;
   controlStruct.Chanel4Control = recorderStatus.Chanel4Status;
   uint16_t source = 0;
   source |= AXIS_SOFTRECORDER_CONTROL__RECORD_SOURCE_MANUAL;


   if(channels.test(0))
   {
       printf("STOP RECORD  1 NOWW !!!!!!!!!!!!!!!\n");
       controlStruct.Chanel1Control ^= AXIS_SOFTRECORDER_STATUS__RECORD_NOW;
       controlStruct.Chanel1Control |= source;
   }
   if(channels.test(1))
   {
       printf("STOP RECORD  2 NOWW !!!!!!!!!!!!!!!\n");
       controlStruct.Chanel2Control ^= AXIS_SOFTRECORDER_STATUS__RECORD_NOW;
       controlStruct.Chanel2Control |= source;
   }
   if(channels.test(2))
   {
       printf("STOP RECORD  3 NOWW !!!!!!!!!!!!!!!\n");
       controlStruct.Chanel3Control ^= AXIS_SOFTRECORDER_STATUS__RECORD_NOW;
       controlStruct.Chanel3Control |= source;
   }
   if(channels.test(3))
   {
       printf("STOP RECORD  4 NOWW !!!!!!!!!!!!!!!\n");
       controlStruct.Chanel4Control ^= AXIS_SOFTRECORDER_STATUS__RECORD_NOW;
       controlStruct.Chanel4Control |= source;
   }
#warning do start recorder

   size_t size = sizeof(SoftRecorderControlStruct_t);

   SoftRecorderControlStruct_t *dataPtr = reinterpret_cast<SoftRecorderControlStruct_t*>(dataReserv.data());
   *dataPtr = controlStruct;
   try
   {
       this->adapterCommands.startRecordCommand(dataProvider, dataReserv, size);

   }
   catch( std::system_error &except )
   {
       this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to start record");
       throw except;
   }
}

void RecorderStatusControl::startRecord(std::bitset<4> channels)
{
    SoftRecorderControlStruct_t controlStruct;
    memset(&controlStruct, 0, sizeof (SoftRecorderControlStruct_t));

    uint16_t source = 0;
    source |= AXIS_SOFTRECORDER_CONTROL__RECORD_SOURCE_MANUAL;
    controlStruct.Chanel1Control = recorderStatus.Chanel1Status;
    controlStruct.Chanel2Control = recorderStatus.Chanel2Status;
    controlStruct.Chanel3Control = recorderStatus.Chanel3Status;
    controlStruct.Chanel4Control = recorderStatus.Chanel4Status;

    if(channels.test(0))
    {
        printf("START RECORD  1 NOWW !!!!!!!!!!!!!!!\n");
        controlStruct.Chanel1Control |= AXIS_SOFTRECORDER_STATUS__RECORD_NOW;
        controlStruct.Chanel1Control |= source;
        printf("Value rec1 = %x\n", controlStruct.Chanel1Control);
    }
    if(channels.test(1))
    {
        printf("START RECORD 2 NOWW !!!!!!!!!!!!!!!\n");
        controlStruct.Chanel2Control |= AXIS_SOFTRECORDER_STATUS__RECORD_NOW;
        controlStruct.Chanel2Control |= source;
        printf("Value rec2 = %x\n", controlStruct.Chanel2Control);
    }
    if(channels.test(2))
    {
        printf("START RECORD  3 NOWW !!!!!!!!!!!!!!!\n");
        controlStruct.Chanel3Control |= AXIS_SOFTRECORDER_STATUS__RECORD_NOW;
        controlStruct.Chanel3Control |= source;
        printf("Value rec3 = %x\n", controlStruct.Chanel3Control);
    }
    if(channels.test(3))
    {
        printf("START RECORD  4 NOWW !!!!!!!!!!!!!!!\n");
        controlStruct.Chanel4Control |= AXIS_SOFTRECORDER_STATUS__RECORD_NOW;
        controlStruct.Chanel4Control |= source;
        printf("Value rec4 = %x\n", controlStruct.Chanel4Control);
    }

    size_t size = sizeof(SoftRecorderControlStruct_t);

    SoftRecorderControlStruct_t *dataPtr = reinterpret_cast<SoftRecorderControlStruct_t*>(dataReserv.data());
    *dataPtr = controlStruct;
    try
    {
        this->adapterCommands.startRecordCommand(dataProvider, dataReserv, size);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to start record");
        throw except;
    }
}

void RecorderStatusControl::changeVolume(uint8_t volume)
{
    Q_UNUSED(volume);
#warning do change volume
}

#warning FULL NOT FOUND
void RecorderStatusControl::eraseInternalMemory(bool full)
{
    Q_UNUSED(full);
    try
    {
        this->adapterCommands.eraseInternalMemoryCommand(this->dataProvider);
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to start erase internal memory");
        throw except;
    }
}

void RecorderStatusControl::startCopy(size_t fromAddr, size_t size, std::string copyPath)
{
    Q_UNUSED(fromAddr);
    Q_UNUSED(size);
    Q_UNUSED(copyPath);
#warning NEED NEW STRUCT FOR START COPY
}

void RecorderStatusControl::stopCopy()
{
    try
    {
        this->adapterCommands.stopCopyCommand(this->dataProvider);
    }
    catch(std::system_error &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to stop copy data");
        throw except;
    }
}

void RecorderStatusControl::stopAudioMonitoring(std::bitset<4> channels)
{
    SoftRecorderControlStruct_t controlStruct;
    memset(&controlStruct, 0, sizeof (SoftRecorderControlStruct_t));

    uint16_t source = 0;

    source &= AXIS_SOFTRECORDER_CONTROL__RECORD_SOURCE_PROGRAM;
    std::cerr << "STOP MONITOR NOW " << std::endl;

    controlStruct.Chanel1Control = recorderStatus.Chanel1Status;
    controlStruct.Chanel2Control = recorderStatus.Chanel2Status;
    controlStruct.Chanel3Control = recorderStatus.Chanel3Status;
    controlStruct.Chanel4Control = recorderStatus.Chanel4Status;

    if(channels.test(0))
    {
            std::cerr << "STOP MONITOR NOW  1" << std::endl;
        controlStruct.Chanel1Control &= ~AXIS_SOFTRECORDER_STATUS__PLAY_NOW;
        controlStruct.Chanel1Control |= AXIS_SOFTRECORDER_CONTROL__STOP_LISTEN_NOW;
    }
    if(channels.test(1))
    {
         std::cerr << "STOP MONITOR NOW 2" << std::endl;
         controlStruct.Chanel2Control &= ~AXIS_SOFTRECORDER_STATUS__PLAY_NOW;
         controlStruct.Chanel2Control |=AXIS_SOFTRECORDER_CONTROL__STOP_LISTEN_NOW;
    }
    if(channels.test(2))
    {
            std::cerr << "STOP MONITOR NOW 3" << std::endl;
         controlStruct.Chanel3Control &= ~AXIS_SOFTRECORDER_STATUS__PLAY_NOW;
         controlStruct.Chanel3Control |=AXIS_SOFTRECORDER_CONTROL__STOP_LISTEN_NOW;
    }
    if(channels.test(3))
    {
            std::cerr << "STOP MONITOR NOW 4" << std::endl;
        controlStruct.Chanel4Control &= ~AXIS_SOFTRECORDER_STATUS__PLAY_NOW;
        controlStruct.Chanel4Control |=AXIS_SOFTRECORDER_CONTROL__STOP_LISTEN_NOW;
    }

    size_t size = sizeof(SoftRecorderControlStruct_t);

    auto *dataPtr = reinterpret_cast<SoftRecorderControlStruct_t*>(dataReserv.data());
    *dataPtr = controlStruct;

    try
    {
        this->adapterCommands.startMonitorCommand(dataProvider, dataReserv, size);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to stop monitor");
        throw except;
    }
}

void RecorderStatusControl::startAudioMonitoring(std::bitset<4> channels)
{
    SoftRecorderControlStruct_t controlStruct;
    memset(&controlStruct, 0, sizeof (SoftRecorderControlStruct_t));

    uint16_t source = 0;

    source |= AXIS_SOFTRECORDER_CONTROL__RECORD_SOURCE_PROGRAM;

    controlStruct.Chanel1Control = recorderStatus.Chanel1Status;
    controlStruct.Chanel2Control = recorderStatus.Chanel2Status;
    controlStruct.Chanel3Control = recorderStatus.Chanel3Status;
    controlStruct.Chanel4Control = recorderStatus.Chanel4Status;


    if(channels.test(0))
    {
        printf("START MONITOR NOWW !!!!!!!!!!!!!!!\n");
        controlStruct.Chanel1Control |= AXIS_SOFTRECORDER_STATUS__PLAY_NOW;
        controlStruct.Chanel1Control |= source;
    }
    if(channels.test(1))
    {
        printf("START MONITOR 2 NOWW !!!!!!!!!!!!!!!\n");
        controlStruct.Chanel2Control |= AXIS_SOFTRECORDER_STATUS__PLAY_NOW;
        controlStruct.Chanel2Control |= source;
    }
    if(channels.test(2))
    {
        printf("START MONITOR  3 NOWW !!!!!!!!!!!!!!!\n");
        controlStruct.Chanel3Control |= AXIS_SOFTRECORDER_STATUS__PLAY_NOW;
        controlStruct.Chanel3Control |= source;
    }
    if(channels.test(3))
    {
        printf("START MONITOR 4 NOWW !!!!!!!!!!!!!!!\n");
        controlStruct.Chanel4Control |= AXIS_SOFTRECORDER_STATUS__PLAY_NOW;
        controlStruct.Chanel4Control |= source;
    }

    size_t size = sizeof(SoftRecorderControlStruct_t);

    auto *dataPtr = reinterpret_cast<SoftRecorderControlStruct_t*>(dataReserv.data());
    *dataPtr = controlStruct;
    try
    {
        this->adapterCommands.startMonitorCommand(dataProvider, dataReserv, size);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to start record");
        throw except;
    }
}

/**
 * @brief Функция, которая конвертирует строку с кодом маскировании в структуру
 *
 * @param dest - куда класть конвертируемый код маскирования
 * @param source - откуда брать исходную строку для кода маскирования
 * @param count - количество места под код маскирования
 * @return int - 0 все Ок, менее 0 в случаи ошибки
 */
int RecorderStatusControl::pswdConv( char* dest, const char* source, size_t count )
{
    char buff[3];
    buff[2] = 0x00;
    unsigned int i;

    if( !dest || ! source || !count  )
    {
        return -1;
    }

    int len = strlen( source );

    for( i = 0; i < count; i++ )
    {
        if( len > 0 )
        {
            strncpy( buff, source, 2 );
            dest[i] = strtol( buff, NULL, 16 );
            source += 2;
            len -= 2;
        }
        else
        {
            break;
        }
    }
    for(  ; i<count; i++ )
    {
        dest[i] = 0x00;
    }
    return 0;
}

void RecorderStatusControl::startPlayAudio(uint16_t channel, uint16_t volume, uint64_t from,
                                           uint64_t to, uint64_t poss, const char* code)
{
    SoftrecorderPlayControlStruct_t playCommand;
    int ret = 0;

    std::cerr << "Size of play command = " << sizeof(SoftrecorderPlayControlStruct_t) << std::endl;
    memset( &playCommand, 0, sizeof( SoftrecorderPlayControlStruct_t ) );

    size_t size = sizeof(SoftrecorderPlayControlStruct_t);

    playCommand.SubCommand		= AXIS_SOFTRECORDER_PLAYER_COMMAND__PLAY;
    playCommand.Channel			= channel;
    playCommand.Possition		= poss;
    playCommand.StartPosition	= from;
    playCommand.StopPosition	= to;
    playCommand.Volume			= volume;

    memcpy(&playCommand.SecurityCode, code, AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE);

//    for(int i = 0; i < AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE; i++)
//    {
//        printf("Secur code[%i] = %x, %c\n", i, playCommand.SecurityCode[i], playCommand.SecurityCode[i]);
//    }

    auto *dataPtr = reinterpret_cast<SoftrecorderPlayControlStruct_t*>(dataReserv.data());
    *dataPtr = playCommand;

    try
    {
        this->adapterCommands.sendPlayerCommand(dataProvider, dataReserv, size);

//        std::cerr << "Size = " << size << "isEmpty = " << dataReserv.empty() << std::endl;

        if( size == 0 || dataReserv.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send play command");
        throw except;
    }
}

void RecorderStatusControl::requestSDCard()
{
    try
    {
        std::cerr << "REQUEST SD CARD !!" << std::endl;
        this->adapterCommands.recorderRequestSD(this->dataProvider);
        std::cerr << "REQUEST SD CARD OK !!" << std::endl;
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send Request SD Card");
        throw except;
    }
}

void RecorderStatusControl::returnSDCard()
{
    try
    {
        this->adapterCommands.recorderReturnSD(this->dataProvider);
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send return SD command");
        throw except;
    }
}

void RecorderStatusControl::pausePlayAudio()
{
    SoftrecorderPlayControlStruct_t playCommand;
    int ret = 0;

    memset( &playCommand, 0, sizeof( SoftrecorderPlayControlStruct_t ) );

    size_t size = sizeof(SoftrecorderPlayControlStruct_t);

    playCommand.SubCommand	= AXIS_SOFTRECORDER_PLAYER_COMMAND__PAUSE;

    auto *dataPtr = reinterpret_cast<SoftrecorderPlayControlStruct_t*>(dataReserv.data());
    *dataPtr = playCommand;
    try
    {
        this->adapterCommands.sendPlayerCommand(dataProvider, dataReserv, size);

        if( size == 0 || dataReserv.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send pause audio command");
        throw except;
    }
}

void RecorderStatusControl::loopPlayAudio(uint64_t from, uint64_t to, uint64_t poss)
{
    SoftrecorderPlayControlStruct_t playCommand;
    int ret = 0;

    memset( &playCommand, 0, sizeof( SoftrecorderPlayControlStruct_t ) );

    size_t size = sizeof(SoftrecorderPlayControlStruct_t);

    playCommand.SubCommand		= AXIS_SOFTRECORDER_PLAYER_COMMAND__LOOP;
    playCommand.Possition		= poss;
    playCommand.StartPosition	= from;
    playCommand.StopPosition	= to;

    auto *dataPtr = reinterpret_cast<SoftrecorderPlayControlStruct_t*>(dataReserv.data());
    *dataPtr = playCommand;
    try
    {
        this->adapterCommands.sendPlayerCommand(dataProvider, dataReserv, size);

        if( size == 0 || dataReserv.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send loop play command");
        throw except;
    }
}

void RecorderStatusControl::stopLoopPlayAudio()
{
    SoftrecorderPlayControlStruct_t playCommand;
    int ret = 0;

    memset( &playCommand, 0, sizeof( SoftrecorderPlayControlStruct_t ) );

    size_t size = sizeof(SoftrecorderPlayControlStruct_t);

    playCommand.SubCommand		= AXIS_SOFTRECORDER_PLAYER_COMMAND__LOOP;
    playCommand.Possition		= 0;
    playCommand.StartPosition	= 0;
    playCommand.StopPosition	= 0;

    auto *dataPtr = reinterpret_cast<SoftrecorderPlayControlStruct_t*>(dataReserv.data());
    *dataPtr = playCommand;
    try
    {
        this->adapterCommands.sendPlayerCommand(dataProvider, dataReserv, size);

        if( size == 0 || dataReserv.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to stop loop play command");
        throw except;
    }
}

void RecorderStatusControl::stopPlayAudio()
{
    SoftrecorderPlayControlStruct_t playCommand;
    int ret = 0;

    memset( &playCommand, 0, sizeof( SoftrecorderPlayControlStruct_t ) );

    size_t size = sizeof(SoftrecorderPlayControlStruct_t);

    playCommand.SubCommand	= AXIS_SOFTRECORDER_PLAYER_COMMAND__STOP;

    auto *dataPtr = reinterpret_cast<SoftrecorderPlayControlStruct_t*>(dataReserv.data());
    *dataPtr = playCommand;
    try
    {
        this->adapterCommands.sendPlayerCommand(dataProvider, dataReserv, size);

        if( size == 0 || dataReserv.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send stop audio play command");
        throw except;
    }
}

void RecorderStatusControl::setPosPlayAudio( uint64_t pos )
{
    SoftrecorderPlayControlStruct_t playCommand;
    int ret = 0;

    memset( &playCommand, 0, sizeof( SoftrecorderPlayControlStruct_t ) );

    size_t size = sizeof(SoftrecorderPlayControlStruct_t);

    playCommand.SubCommand		= AXIS_SOFTRECORDER_PLAYER_COMMAND__SET;
    playCommand.Possition		= pos;

    auto *dataPtr = reinterpret_cast<SoftrecorderPlayControlStruct_t*>(dataReserv.data());
    *dataPtr = playCommand;
    try
    {
        this->adapterCommands.sendPlayerCommand(dataProvider, dataReserv, size);

        if( size == 0 || dataReserv.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to set pos play command");
        throw except;
    }
}

void RecorderStatusControl::setVolumePlayAudio(int volume)
{
    SoftrecorderPlayControlStruct_t playCommand;
    int ret = 0;

    memset( &playCommand, 0, sizeof( SoftrecorderPlayControlStruct_t ) );

    size_t size = sizeof(SoftrecorderPlayControlStruct_t);

    playCommand.SubCommand		= AXIS_SOFTRECORDER_PLAYER_COMMAND__VOLUME;
    playCommand.Volume		= volume;

    auto *dataPtr = reinterpret_cast<SoftrecorderPlayControlStruct_t*>(dataReserv.data());
    *dataPtr = playCommand;
    try
    {
        this->adapterCommands.sendPlayerCommand(dataProvider, dataReserv, size);

        if( size == 0 || dataReserv.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to set volume play command");
        throw except;
    }
}

void RecorderStatusControl::setPlayAudio(uint16_t channel, uint16_t volume, uint64_t from,
                                         uint64_t to, uint64_t poss, const char* code)
                                         {
    SoftrecorderPlayControlStruct_t playCommand;
    int ret = 0;

    std::cerr << "Size of play command = " << sizeof(SoftrecorderPlayControlStruct_t) << std::endl;
    memset( &playCommand, 0, sizeof( SoftrecorderPlayControlStruct_t ) );

    size_t size = sizeof(SoftrecorderPlayControlStruct_t);

    playCommand.SubCommand		= AXIS_SOFTRECORDER_PLAYER_COMMAND__SET;
    playCommand.Channel			= channel;
    playCommand.Possition		= poss;
    playCommand.StartPosition	= from;
    playCommand.StopPosition	= to;
    playCommand.Volume			= volume;

    memcpy(&playCommand.SecurityCode, code, AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE);


    std::cerr << "SET PLAY SEC CODE " << playCommand.SecurityCode << std::endl;

    auto *dataPtr = reinterpret_cast<SoftrecorderPlayControlStruct_t*>(dataReserv.data());
    *dataPtr = playCommand;

    try
    {
        this->adapterCommands.sendPlayerCommand(dataProvider, dataReserv, size);

        if( size == 0 || dataReserv.empty() )
        {
            throw std::system_error(dpr::NO_DATA_RECIEVED, generic_category());
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to send play command");
        throw except;
    }
}
