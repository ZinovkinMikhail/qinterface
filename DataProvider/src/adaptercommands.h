#ifndef ADAPTERCOMMANDS_H
#define ADAPTERCOMMANDS_H

#include <axis_softrecorder.h>
#include <rosslog/log.h>

#include "dataprovider.h"

#define ADAPTER_DATA_WORD_POSITION AXIS_SOFTRECORDER_EXCHANGE__DATA_WORD_OFFSET * 2

typedef enum
{
    GET_RECORDER_STATUS = AXIS_DISPATCHER_CMD_GET_STATUS,
    GET_ADAPTER_STATUS = AXIS_DISPATCHER_CMD_GET_ADAPTER_STATUS,
    GET_ADAPTER_ACC_STATUS = AXIS_DISPATCHER_CMD_GET_ACC_STATE,
    START_RECORD = AXIS_DISPATCHER_CMD_RECORD_CONTROL,
    STOP_RECORD = AXIS_DISPATCHER_CMD_RECORD_CONTROL,
    START_MONITORING = AXIS_DISPATCHER_CMD_LISTEN_CONTROL,
    STOP_MONITORING = AXIS_DISPATCHER_CMD_LISTEN_CONTROL,
    ERASE_INTERNAL_MEMORY = AXIS_DISPATCHER_CMD_START_ERASE,
    START_COPY,
    STOP_COPY = AXIS_DISPATCHER_CMD_STOP_COPY_TO_FILE,
    REBOOT = AXIS_ADAPTER_CMD_REBOOT,
    LPC =  AXIS_ADAPTER_CMD_POWER_OFF,
    DISPLAY_BRIGHTNES = AXIS_INTERFACE_CMD_SET_BRIGHT,
    KEYBOARD_LOCK = AXIS_INTERFACE_CMD_LOCK_KEYBOARD,
    KEYBOARD_UNLOCK = AXIS_INTERFACE_CMD_UNLOCK_KEYBOARD,
    GET_RECORDER_TIMERS = DSP_RECORDER_CMD_GET_TIMERS, //OK
    SAVE_RECORDER_TIMERS = DSP_RECORDER_CMD_PUT_TIMERS,
    GET_ADAPTER_SETTINGS = DSP_RECORDER_CMD_GET_NET_PARAM,
    SAVE_ADAPTER_SETTINGS = DSP_RECORDER_CMD_SET_NET_PARAM,
    APPLY_ADAPTER_SETTINGS = DSP_RECORDER_CMD_APPLY_NET_PARAM,
    GET_ADAPTER_TIMERS = DSP_RECORDER_CMD_GET_NET_TIMERS,
    SAVE_ADAPTER_TIMERS = DSP_RECORDER_CMD_PUT_NET_TIMERS,
    GET_RECORDER_SETTINGS = AXIS_DISPATCHER_CMD_GET_PARAM,
    SAVE_RECORDER_SETTINGS = AXIS_DISPATCHER_CMD_SET_PARAM,
    APPLY_RECORDER_SETTINGS = AXIS_DISPATCHER_CMD_SET_AUDIO_CFG,
    START_AUDIO_DOWNLOAD = AXIS_DISPATCHER_CMD_START_UPLOAD_MTD_PKD,
    PLAYER_COMMAND = AXIS_DISPATCHER_CMD_PLAY_CONTROL,
    REQUEST_SD = AXIS_DISPATHCER_CMD_SD_REQUEST_GET,
    RETURN_SD = AXIS_DISPATHCER_CMD_SD_REQUEST_RET,
    REQUEST_USB = AXIS_DISPATCHER_CMD_SWITCH_USB_OUT,
    RETURN_USB = AXIS_DISPATCHER_CMD_SWITCH_USB_IN
} EAdapterCommands;

using namespace data_provider;

class AdapterCommands : public QObject
{
    Q_OBJECT
public:
    explicit AdapterCommands(QObject *parent = nullptr);

    void getRecorderStatusCommand(std::shared_ptr<DataProvider> dataProvider,
                                  std::vector<uint8_t> &data, size_t &size);
    void getAdapterStatusCommand(std::shared_ptr<DataProvider> dataProvider,
                                 std::vector<uint8_t> &data, size_t &size);

    void getRecorderSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                    std::vector<uint8_t> &data, size_t &size);

    void saveRecorderSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                         std::vector<uint8_t> &data, size_t &size);

    void applyRecorderSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                         std::vector<uint8_t> &data, size_t &size);

    void getAdapterAccStatusCommand(std::shared_ptr<DataProvider> dataProvider,
                             std::vector<uint8_t> &data, size_t &size) noexcept(false);

    void getRecorderTimersCommand(std::shared_ptr<DataProvider> dataProvider,
                                 std::vector<uint8_t> &data, size_t &size);
    void saveRecorderTimersCommand(std::shared_ptr<DataProvider> dataProvider,
                                 std::vector<uint8_t> &data, size_t &size);

    void adapterRebootCommand(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

    void adapterLPCCommand(std::shared_ptr<DataProvider> dataProvider, uint32_t timeout) noexcept(false);

    void adapterSetDisplayBrightnessCommand(std::shared_ptr<DataProvider> dataProvider,
                                            uint32_t val) noexcept(false);

    void adapterSetKeyboardLock(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

    void adapterSetKeyboardUnLock(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

    void eraseInternalMemoryCommand(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

    void stopCopyCommand(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

    void getAdapterSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                   std::vector<uint8_t> &data, size_t &size) noexcept(false);

    void getAdapterNetTimersCommnad(std::shared_ptr<DataProvider> dataProvider,
                                    std::vector<uint8_t> &data, size_t &size) noexcept(false);

    void saveAdapterNetTimersCommand(std::shared_ptr<DataProvider> dataProvider,
                                     std::vector<uint8_t> &data, size_t &size) noexcept(false);

    void saveAdapterSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                    std::vector<uint8_t> &data, size_t &size) noexcept(false);

    void applyAdapterSettingsCommand(std::shared_ptr<DataProvider> dataProvider,
                                     std::vector<uint8_t> &data, size_t &size) noexcept(false);

    void startRecordCommand(std::shared_ptr<DataProvider> dataProvider,
                            std::vector<uint8_t> &data, size_t  &size);

    void startMonitorCommand(std::shared_ptr<DataProvider> dataProvider,
                            std::vector<uint8_t> &data, size_t  &size);
    void stopMonitorCommand(std::shared_ptr<DataProvider> dataProvider,
                            std::vector<uint8_t> &data, size_t  &size);
    
    void startAudioHeaderDownload(std::shared_ptr<DataProvider> dataProvider,
								std::vector<uint8_t> &data, std::size_t &size);

    void recorderRequestSD(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

    void recorderReturnSD(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

    void sendPlayerCommand(std::shared_ptr<DataProvider> dataProvider,
                                     std::vector<uint8_t> &data, size_t &size) noexcept(false);

    void adapterRequestUSB(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

    void adapterReturnUSB(std::shared_ptr<DataProvider> dataProvider) noexcept(false);

signals:

public slots:
};

#endif // ADAPTERCOMMANDS_H
