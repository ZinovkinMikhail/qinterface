#ifndef IDATARECIEVER_H
#define IDATARECIEVER_H

#include <vector>
#include <stdint.h>
#include <stdbool.h>

class IDataReciever
{
public:
    virtual bool recieveData(std::vector<uint8_t> &data, size_t size) = 0;
    //virtual ~IDataReciever();
};

#endif // IDATARECIEVER_H
