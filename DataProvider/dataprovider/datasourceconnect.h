#ifndef DATASOURCECONNECT_H
#define DATASOURCECONNECT_H

#include <stdint.h>
#include <memory>

#include <QObject>

#include <rosslog/log.h>

#include "idatareciever.h"
#include "unixsocketadapter.h"

using namespace UnixAdapter;

typedef enum
{
    SINGLE_PACKET,
    MULTI_PACKET
} PacketType;

class DataSourceConnect :
        public QObject,
        public std::enable_shared_from_this<DataSourceConnect>,
        public IDataReciever
{
    Q_OBJECT
public:
    DataSourceConnect(rosslog::Log &log, QObject *parent = nullptr);
    ~DataSourceConnect();
    bool startConnection();
    void closeConnection();
    bool sendData(int command, std::vector<uint8_t> &data, size_t dataSize);
    bool sendDataInterrogation(int command, std::vector<uint8_t> &data, size_t dataSize);
    bool getConnectionState();
    size_t readData(int command, std::vector<uint8_t> &data);
    bool recieveData(std::vector<uint8_t> &data, size_t size);


private:
    void initSocketAdapter();
    void checkExchange(uint16_t *data, uint16_t cmd, size_t size);
    bool workHandledPacketByInterrogation(std::vector<uint8_t> &readyData, int cmd, size_t size);
    bool parsePacketByCmd(std::vector<uint8_t> &readyData,
                          uint16_t *data, uint16_t cmd, size_t size);
    bool sendPacketToAdapter(int command, uint8_t *data, size_t dataSize, SocketType type);
    PacketType checkPacketHeader(uint8_t *data, size_t size, int &command);
    bool createExchangePacket(int command, uint8_t *data, size_t size);
    int getNumberOfPacketBlocks(uint16_t *data);

    rosslog::Log *logger;
    std::unique_ptr<UnixSocketAdapter> socketAdapter;
    std::vector<uint8_t> dataVector;

signals:
    void signalNewDataRecieved(int command, std::vector<uint8_t> data, size_t size);
};

#endif // DATASOURCECONNECT_H
