#ifndef ADAPTERSTATUSCONTROL_H
#define ADAPTERSTATUSCONTROL_H


#include <QObject>

#include <rosslog/log.h>
#include <axis_softadapter.h>

#include "dataprovider.h"
#include "adaptercommands.h"

namespace adapter_status_ctrl
{
#define LPC_TIOMEOUT 15

class AdapterStatusControl : public QObject
{
    Q_OBJECT
public:
    explicit AdapterStatusControl(std::shared_ptr<DataProvider>  dataProvider,
                                  rosslog::Log &log, QObject *parent = nullptr);

    void updateStatus() noexcept(false);
    SoftAdapterDeviceCurrentStatus_t getAdapterStatus();
    SoftRecorderGetAccStateStruct_t getAccStatus();
    void doReboot() noexcept(false);
    void doLPC() noexcept(false);
    void setDisplayBrightness(uint32_t val) noexcept(false);
    void setKeyBoardBlock(bool lock) noexcept(false);

#ifdef DATABASE_USE
    void startAudioHeaderDownload(SoftAdapterAudioHeaderDownload_t audioHeader);
#endif

private:
    void getAdapterDeviceStatus() noexcept(false);
    void getAdapterAccStatus() noexcept(false);

    std::shared_ptr<DataProvider> dataProvider;
    AdapterCommands adapterCommands;
    SoftAdapterDeviceCurrentStatus_t adapterCurrentStatus;
    SoftRecorderGetAccStateStruct_t adapterAccStatus;
    rosslog::Log *logger;

    std::vector<uint8_t> dataReserv;

signals:

public slots:
};
}

#endif // ADAPTERSTATUSCONTROL_H
