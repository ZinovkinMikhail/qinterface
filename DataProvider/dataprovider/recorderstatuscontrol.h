#ifndef RECORDERSTATUSCONTROL_H
#define RECORDERSTATUSCONTROL_H

#include <QObject>

#include <bitset>

#include <axis_softrecorder.h>
#include <axis_softadapter.h>
#include <rosslog/log.h>

#include "dataprovider.h"
#include "adaptercommands.h"

namespace recorder_status_ctrl
{

class RecorderStatusControl : public QObject
{
    Q_OBJECT
public:
    explicit RecorderStatusControl(std::shared_ptr<DataProvider> dataProvider,
                                   rosslog::Log &log,
                                   QObject *parent = nullptr);

    void updateStatus();
    SoftRecorderStatusStruct_t getRecorderStatus();
    void startRecord(std::bitset<4> channels);
    void stopRecord(std::bitset<4> channels);
    void startAudioMonitoring(std::bitset<4> channels);
    void stopAudioMonitoring(std::bitset<4> channels);
    void changeVolume(uint8_t volume);
    void eraseInternalMemory(bool full);
    void startCopy(size_t fromAddr, size_t size, std::string copyPath);
    void stopCopy();

private:
    rosslog::Log *logger;
    std::shared_ptr<DataProvider> dataProvider;
    AdapterCommands adapterCommands;
    SoftRecorderStatusStruct_t recorderStatus;

    std::vector<uint8_t> dataReserv;

signals:

public slots:
};

}

#endif // RECORDERSTATUSCONTROL_H
