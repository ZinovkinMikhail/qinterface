#include <assert.h>

#include <QQmlComponent>

#include "recordersecuritywindow.h"

#define VENDOR_PASSWORD "h2cHm2H7oMlA2b8FG5xM3PzYus3Cy7V5"

using namespace rosslog;

RecorderSecurityWindow::RecorderSecurityWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    dataProvider(nullptr),
    recordSettingsControl(nullptr),
    userManipulators(nullptr)
{}

std::shared_ptr<IUserManipulators> RecorderSecurityWindow::initWindow(EWindSizeType winSizeType,
                                                                      std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                      std::shared_ptr<IStateWidget> stateWidget,
                                                                      std::shared_ptr<IUserManipulators> userManipulators,
                                                                      std::shared_ptr<IQmlEngine> engine,
                                                                      std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    Q_UNUSED(log);
    assert(engine);
    assert(dataProvider);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(QML_PATH);
        assert(windowComponent);

        connect(this, SIGNAL(signalRecivedAdapterData()),
                this, SLOT(slotRecievedAdapterData()));
        connect(windowComponent, SIGNAL(saveButtonClicked()) ,
                this, SLOT(slotSaveButtonClicked()));
        connect(windowComponent, SIGNAL(applyButtonClicked()) ,
                this, SLOT(slotApplyButtonClicked()));
        connect(windowComponent, SIGNAL(cancelButtonClicked()) ,
                this, SLOT(slotCancelButtonClicked()));
    }
    initAdapterSettingsControl();

    return nullptr;
}

QObject *RecorderSecurityWindow::getComponentObject()
{
    return windowComponent;
}

std::string RecorderSecurityWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string RecorderSecurityWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string RecorderSecurityWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string RecorderSecurityWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string RecorderSecurityWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap RecorderSecurityWindow::getIcon()
{
    return QPixmap(":/RecorderSecurity/icons/i32.png");
}

std::pair<bool, QPixmap> RecorderSecurityWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/RecorderSecurity/icons/i19.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > RecorderSecurityWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool RecorderSecurityWindow::showWindow()
{
    updateData();
    return true;
}

bool RecorderSecurityWindow::hideWindow()
{
    setItemValue("Password", "text", QString(""));
    setItemValue("PasswordRepeat", "text", QString(""));
    return  true;
}

EWindSizeType RecorderSecurityWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool RecorderSecurityWindow::prepareToDelete()
{
    return true;
}

void RecorderSecurityWindow::initAdapterSettingsControl()
{
    if( recordSettingsControl == nullptr )
    {
        recordSettingsControl = std::shared_ptr<RecorderSettingsContol>(
                new RecorderSettingsContol(dataProvider, *logger.get()));
    }
}

void RecorderSecurityWindow::setItemValue(QString itemName, QString valueName, QString value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

void RecorderSecurityWindow::setItemValue(QString itemName, QString valueName, bool value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

void RecorderSecurityWindow::getInfoFromAdapter()
{
    try
    {
        this->recordSettingsControl->updateStatus();
        this->recordSettings = recordSettingsControl->getRecordSettings();
        emit signalRecivedAdapterData();
    }
    catch( std::system_error &systemErr )
    {
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        userManipulators->showMessage(std::string("Не удалось получить данные.\nКод ошибки: ").append(
                                          std::to_string(systemErr.code().value())));
    }
}

void RecorderSecurityWindow::setRecievedData()
{
    QTextCodec *codec = QTextCodec::codecForName("CP1251");
    QString name = codec->toUnicode(recordSettings.Alias);
    QString blockCode = codec->toUnicode((char*)recordSettings.BlockCode);

    setItemValue("Name", "text", name);
    setItemValue("BlockPasswordField", "text", blockCode);
    setItemValue("BlockPasswordRepeatField", "text", blockCode);

    if( recordSettings.Security == AXIS_SOFTRECORDER_SETTINGS__SECURITY_COMBO ||
            recordSettings.Security == AXIS_SOFTRECORDER_SETTINGS__SECURITY_USER)
    {
        setItemValue("MaskCheckBox", "checked", true);
    }
    else
    {
        setItemValue("MaskCheckBox", "checked", false);
    }

}

void RecorderSecurityWindow::updateData()
{
    updateWorker = std::async(std::launch::async,
                              &RecorderSecurityWindow::getInfoFromAdapter, this);
    updateWorker.wait_for(std::chrono::seconds(0));
}

void RecorderSecurityWindow::saveData()
{
    SoftRecorderSettingsStruct_t settings = recordSettings;
    if( packSettedData(settings) )
    {
        userManipulators->showLoadingWidget();
        updateWorker = std::async( std::launch::async,[this, settings]
        {
            try
            {
                sleep(1);
                recordSettingsControl->saveRecordSettings(settings);
                userManipulators->hideLoadingWidget();
                recordSettings = settings;
            } catch (std::system_error &except)
            {
                userManipulators->hideLoadingWidget();
                userManipulators->showMessage(std::string("Не удалось сохранить изменения.\nКод ошибки: ").append(
                                                  std::to_string(except.code().value())));
            }

            return;
        });
        updateWorker.wait_for(std::chrono::seconds(0));
    }
}

void RecorderSecurityWindow::applyData()
{
    SoftRecorderSettingsStruct_t settings = recordSettings;
    if( packSettedData(settings) )
    {
        userManipulators->showLoadingWidget();
        updateWorker = std::async( std::launch::async,[this, settings]
        {
            try
            {
                sleep(1);
                recordSettingsControl->applyRecordSettings(settings);
                userManipulators->hideLoadingWidget();
                recordSettings = settings;
            } catch (std::system_error &except)
            {
                userManipulators->hideLoadingWidget();
                userManipulators->showMessage(std::string("Не удалось применить изменения.\nКод ошибки: ").append(
                                                  std::to_string(except.code().value())));
            }

            return;
        });
        updateWorker.wait_for(std::chrono::seconds(0));
    }
}

bool RecorderSecurityWindow::packSettedData(SoftRecorderSettingsStruct_t &settings)
{
    QTextCodec *codec = QTextCodec::codecForName("CP1251");
    QString name = getItemValue("Name", "text");

    char str[64];
    memset(&str, 0 , sizeof(str));
    strncpy(str, codec->fromUnicode(name).toStdString().c_str(), sizeof(str));

    QString fromCp = codec->toUnicode(str);
    strncpy(settings.Alias,
            str,
            sizeof(settings.Alias));

    bool checked = false;
    getItemValue("MaskCheckBox", "checked", checked);
    if( checked )
    {
        char pass[32];
        char user[32];
        memset(&user, 0, sizeof(user));
        memset(&pass, 0, sizeof(pass));

        QString password = getItemValue("Password", "text");
        QString passwordRepeat = getItemValue("PasswordRepeat", "text");
        if( !password.isEmpty() )
        {
            if( password.compare(passwordRepeat)  )
            {
                #warning error
                userManipulators->showMessage("Подтверждение кода маскирования не совпадает");
                return false;
            }
            strncpy(pass, VENDOR_PASSWORD, sizeof (pass));
            strncpy(user, codec->fromUnicode(password).toStdString().c_str(), sizeof(user));


            for(int i = 0; i < 32; i++)
            {
                pass[i] = pass[i] ^ user[i];
                if(pass[i] == 0)
                {
                    pass[i] = 0x5a;
                }
                printf("%x", (uint8_t)pass[i]);
            }
            printf("\n");

            strncpy((char*)settings.SecurityCode,pass,
                    sizeof(settings.SecurityCode));

//            std::cerr << "SAVE CODE " << settings.SecurityCode << std::endl;
//            std::cerr << "SAVE CODE " << pass << std::endl;
//            settings.Security = AXIS_SOFTRECORDER_SETTINGS__SECURITY_COMBO;
        }
//        else
//        {
//            userManipulators->showMessage("Введите код маскирования.");
//            return false;
//        }

        settings.Security = AXIS_SOFTRECORDER_SETTINGS__SECURITY_COMBO;
    }
    else
    {
        settings.Security = AXIS_SOFTRECORDER_SETTINGS__SECURITY_DISABLE;
    }


    char blockPass[6] = {0};

    QString blockPassword = getItemValue("BlockPasswordField", "text");
    QString blockPasswordRepeat = getItemValue("BlockPasswordRepeatField", "text");

    if( blockPassword.count() == 6 )
    {
        if( blockPassword.compare(blockPasswordRepeat) )
        {
            userManipulators->showMessage("Подтверждение кода блокировки не совпадает.");
            return false;
        }

        strncpy(blockPass,
                codec->fromUnicode(blockPassword).toStdString().c_str(),
                sizeof(blockPass));

        strncpy((char*)settings.BlockCode,blockPass,
                sizeof(settings.BlockCode));
    }
    else
    {
        userManipulators->showMessage("Введите 6 цифр кода блокировки.");
        return false;
    }

    return true;
}

QString RecorderSecurityWindow::getItemValue(QString itemName, QString valueName)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    return QQmlProperty(item, valueName).read().toString();
}

void RecorderSecurityWindow::getItemValue(QString itemName, QString valueName, bool &value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    value = QQmlProperty(item, valueName).read().toBool();
}

void RecorderSecurityWindow::slotRecievedAdapterData()
{
    setRecievedData();
}

void RecorderSecurityWindow::slotSaveButtonClicked()
{
    saveData();
}

void RecorderSecurityWindow::slotApplyButtonClicked()
{
    applyData();
}

void RecorderSecurityWindow::slotCancelButtonClicked()
{
    setRecievedData();
}

RecorderSecurityWindow::~RecorderSecurityWindow()
{
    std::cerr << "Removed RecorderSecurityWindow" << std::endl;
}
