import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Item
{
    id: securityWindow
    y: 150
    width: 1280
    height: 484

    ColorConfig {id: cc}
    FontsConfig{id:fontConf}

    property int localFontSize: fontConf.pixSize_S6 - 2

    signal saveButtonClicked()
    signal cancelButtonClicked()
    signal applyButtonClicked()

    property var inputPanelY: parent ? parent.inputPanelY : 0
    property real pressedFieldY //for shift when virtual keyboard open


    BackgroundTitle
    {
        text: qsTr("Безопасность")
    }

    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 390
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }

    Rectangle
    {
        id:mainZone
        anchors.fill: background
        anchors.margins: 18
        color: cc.dark_blue
        clip: true

        Rectangle
        {
            id:line
            anchors.centerIn: parent
            anchors.verticalCenterOffset: 5
            height: mainZone.height - 100
            width: 2
            color: cc.blue
        }

        Item
        {
            id:shiftZone
            height: parent.height
            width: parent.width
            y:
            {
                if(inputPanelY < pressedFieldY)
                {
                    inputPanelY - pressedFieldY
                }
                else
                {
                    0
                }
            }
        }

        Item
        {
            id:leftBlock
            parent: shiftZone
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 5
            x:parent.width/10

            width: nameField.width
            height: parent.height


            Text
            {
                id:nameLabel
                anchors.horizontalCenter: nameField.horizontalCenter
                anchors.bottom: nameField.top
                anchors.bottomMargin: 5
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: localFontSize
                color: cc.light_green
                text: qsTr("Псевдоним")
            }

            TextField
            {
                id: nameField
                objectName: "Name"
                anchors.top: parent.top
                anchors.topMargin: 50
                height: 50
                width: 350
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: fontConf.pixSize_S6

                background: Rectangle { color: "transparent" }

                Rectangle
                {
                    anchors.fill: parent
                    z:-1
                    color: "#fcfff5"
                }

                ShadowRect{x: -4; y: 4}

                onPressed:
                {
                    if(inputPanelY > pressedFieldY)
                    {
                        pressedFieldY = mapToItem(securityWindow,x,y).y + 65
                    }
                }

                onFocusChanged:
                {
                    if(focus)
                    {
                        selectAll()
                    }
                }
            }


            Text
            {
                id:blockPasswordLabel
                anchors.horizontalCenter: blockPasswordField.horizontalCenter
                anchors.bottom: blockPasswordField.top
                anchors.bottomMargin: 5
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: localFontSize
                color: cc.light_green
                text: qsTr("Код блокировки")
            }

            TextField
            {
                id: blockPasswordField
                objectName: "BlockPasswordField"
                anchors.centerIn: parent
                height: 50
                width: 350
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: fontConf.pixSize_S6
                echoMode: TextInput.Password
                passwordCharacter: "*"
                placeholderText: qsTr("******")
                validator: IntValidator {bottom: 0; top: 999999;}
                maximumLength: 6
                inputMethodHints: Qt.ImhDigitsOnly

                background: Rectangle { color: "transparent" }

                Rectangle
                {
                    anchors.fill: parent
                    z:-1
                    color: "#fcfff5"
                }

                ShadowRect{x: -4; y: 4}

                onPressed:
                {                                       
                    if(inputPanelY > pressedFieldY)
                    {
                        pressedFieldY = mapToItem(securityWindow,x,y).y + 65
                    }
                }

                onFocusChanged:
                {
                    if(focus)
                    {
                        selectAll()
                    }
                }
            }


            Text
            {
                id:blockPasswordRepeatLabel
                anchors.horizontalCenter: blockPasswordRepeatField.horizontalCenter
                anchors.bottom: blockPasswordRepeatField.top
                anchors.bottomMargin: 5
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: localFontSize
                color: cc.light_green
                text: qsTr("Подтверждение кода")
            }

            TextField
            {
                id: blockPasswordRepeatField
                objectName: "BlockPasswordRepeatField"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 50
                height: 50
                width: 350
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: fontConf.pixSize_S6
                echoMode: TextInput.Password
                passwordCharacter: "*"
                placeholderText: qsTr("******")
                validator: IntValidator {bottom: 0; top: 999999;}
                maximumLength: 6
                inputMethodHints: Qt.ImhDigitsOnly

                background: Rectangle { color: "transparent" }

                Rectangle
                {
                    anchors.fill: parent
                    z:-1
                    color: "#fcfff5"
                }

                ShadowRect{x: -4; y: 4}

                onPressed:
                {                                      
                    if(inputPanelY > pressedFieldY)
                    {
                        pressedFieldY = mapToItem(securityWindow,x,y).y - 40
                    }
                }

                onFocusChanged:
                {
                    if(focus)
                    {
                        selectAll()
                    }
                }
            }
        }


        Item
        {
            id: rightBlock
            parent: shiftZone
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 5
            x:parent.width * 0.6
            width: maskPasswordField.width
            height: parent.height


            TextCheckBox
            {
                id: checkBoxButton
                objectName: "MaskCheckBox"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 50
                text: qsTr("Код маскирования:")
                spacing: 15
                checked: false
            }


            Text
            {
                id:maskPasswordLabel
                anchors.horizontalCenter: maskPasswordField.horizontalCenter
                anchors.bottom: maskPasswordField.top
                anchors.bottomMargin: 5
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: localFontSize
                color: checkBoxButton.checked ? cc.light_green : "#7f7f7f"
                text: qsTr("Код маскирования")
                enabled: false
            }


            TextField
            {
                id: maskPasswordField
                objectName: "Password"
                anchors.centerIn: parent
                height: 50
                width: 350
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: fontConf.pixSize_S6
//                echoMode: TextInput.Password
//                passwordCharacter: "*"
                enabled: checkBoxButton.checked

//                placeholderText: qsTr("**********")

                background: Rectangle { color: "transparent" }

                Rectangle
                {
                    anchors.fill: parent
                    z:-1
                    color: checkBoxButton.checked ? "#fcfff5" : "#7f7f7f"
                }

                ShadowRect{x: -4; y: 4}

                onPressed:
                {                   
                    if(inputPanelY > pressedFieldY)
                    {
                        pressedFieldY = mapToItem(securityWindow,x,y).y + 65
                    }
                }
            }


            Text
            {
                id:maskPasswordRepeatLabel
                anchors.horizontalCenter: maskPasswordRepeatField.horizontalCenter
                anchors.bottom: maskPasswordRepeatField.top
                anchors.bottomMargin: 5
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: localFontSize
                color: checkBoxButton.checked ? cc.light_green : "#7f7f7f"
                text: qsTr("Потдверждение кода")
                enabled: false
            }

            TextField
            {
                id: maskPasswordRepeatField
                objectName: "PasswordRepeat"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 50
                height: 50
                width: 350
                font.family: fontConf.mainFont
                font.italic: true
                font.pixelSize: fontConf.pixSize_S6
//                echoMode: TextInput.Password
//                passwordCharacter: "*"
                enabled: checkBoxButton.checked

//                placeholderText: qsTr("**********")

                background: Rectangle { color: "transparent" }

                Rectangle
                {
                    anchors.fill: parent
                    z:-1
                    color: checkBoxButton.checked ? "#fcfff5" : "#7f7f7f"
                }

                ShadowRect{x: -4; y: 4}

                onPressed:
                {                   
                    if(inputPanelY > pressedFieldY)
                    {
                        pressedFieldY = mapToItem(securityWindow,x,y).y - 40
                    }
                }
            }
        }
    }


    Item
    {
        id:controlButtons
        anchors.top: background.bottom
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        height: saveButton.height
        width: 460 //700

        ControlListButton
        {
            id:saveButton
            type: "SlimType"
            text: qsTr("Сохранить")
            onClicked: saveButtonClicked()
        }
//        ControlListButton
//        {
//            id:acceptButton
//            anchors.centerIn: parent
//            type: "SlimType"
//            text: qsTr("Применить")
//            onClicked: applyButtonClicked()
//        }
        ControlListButton
        {
            id:cancelButton
            anchors.right: parent.right
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked: cancelButtonClicked()
        }
    }
}
