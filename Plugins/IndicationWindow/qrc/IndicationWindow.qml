import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0

Item
{
    id: indicatorWindow
    y: 150
    width: 1280
    height: 484

    ColorConfig {id: cc}
    FontsConfig{id:fontConf}

    // 0 - LED
    // 1 - RECORDER
    // 2 - ADAPTER
    // 3 - DISPLAY
    // TODO 4 - DISPLAY BRIGHTNESS
    signal showComboDrum(var type)
    signal saveButtonClicked()
    signal cancelButtonClicked()

    BackgroundTitle
    {
        text: qsTr("Индикация и автовыключение")
    }


    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 420
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }

    Rectangle
    {
        id:mainZone
        anchors.fill: background
        anchors.margins: 10
        color: cc.dark_blue
        clip: true

        Column
        {
            anchors.centerIn: parent
            spacing: 25

            Row
            {
               Loader{sourceComponent: lineName; id:labelLED; anchors.verticalCenter: parent.verticalCenter}
               ButtonDrop {  type: "default" ; longStyle: true ; objectName: "LED" ; onButtonClick: showComboDrum(0) }
            }
            Row
            {
               Loader{sourceComponent: lineName; id:labelStorageAutoOFF; anchors.verticalCenter: parent.verticalCenter}
               ButtonDrop {  type: "default" ; longStyle: true ; objectName: "RecorderAutoOff"; onButtonClick: showComboDrum(1)  }
            }
            Row
            {
               Loader{sourceComponent: lineName; id:labelAdapterAutoOFF; anchors.verticalCenter: parent.verticalCenter}
               ButtonDrop {  type: "default" ; longStyle: true ; objectName: "AdapterAutoOff"; onButtonClick: showComboDrum(2)  }
            }
            Row
            {
               Loader{sourceComponent: lineName; id:labelDisplayAutoOFF; anchors.verticalCenter: parent.verticalCenter}
               ButtonDrop {  type: "default" ; longStyle: true ; objectName: "DisplayAutoOff"; onButtonClick: showComboDrum(3)  }
            }
            Row
            {
               Loader{sourceComponent: lineName; id:labelDisplayBrightness; anchors.verticalCenter: parent.verticalCenter}
               ButtonDrop {  type: "default" ; longStyle: true ; objectName: "DisplayBrightness"; onButtonClick: showComboDrum(4)  }
            }


            Component.onCompleted:
            {
                labelLED.item.text = "Управление индикацией";
                labelStorageAutoOFF.item.text = "Автовыключение накопителя";
                labelAdapterAutoOFF.item.text = "Автовыключение адаптера";
                labelDisplayAutoOFF.item.text = "Автовыключение дисплея";
                labelDisplayBrightness.item.text = "Яркость дисплея";
            }
        }
    }


    Item
    {
        id:controlButtons
        anchors.top: background.bottom
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        width: 470

        ControlListButton
        {
            anchors.right: parent.right
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked: cancelButtonClicked()
        }
        ControlListButton
        {
            anchors.left: parent.left
            type: "SlimType"
            text: qsTr("Сохранить")
            onClicked: saveButtonClicked()
        }
    }


    Component
    {
        id:lineName

        Text
        {
            width: 450
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S5
            color: cc.light_green
        }
    }

    Component
    {
        id:dropField
        ButtonDrop
        {
           // longStyle: true
            type: "default"
        }
    }
}
