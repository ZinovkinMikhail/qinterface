#ifndef SETTINGSPARAMSWINDOW_H
#define SETTINGSPARAMSWINDOW_H

#include <future>
#include <arpa/inet.h>
#include <iostream>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlProperty>


#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/adaptersettingscontrol.h>
#include <dataprovider/recordersettingscontol.h>
#include <dataprovider/adapterstatuscontrol.h>

#include "indicatorsettingsmodel.h"

#define WINDOW_IDENTIFICATOR "IndicationWindow"
#define WINDOW_FOLDER_NAME "System"
#define ICON_PRESENTATION_NAME "Индикация"
#define FOLDER_PRESENTATION_NAME "Система"

#define QML_PATH "qrc:/IndicationWindow/IndicationWindow.qml"

#define START_BRIGHTNESS 99

class IndicationWindow : public QObject, public IEmptyPlaceWindow
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "IndicationWindow")
public:
    explicit IndicationWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~IndicationWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;

private:
    void initAdapterSettingsControl();
    void initRecorderSettingsControl();
    void initAdapterStatusControl();
    void setRecievedData();
    void saveData();
    bool packSettedData();
    void updateData();

    void setItemValue(QString itemName, QString valueName, QString value);
    void setItemValue(QString itemName, QString valueName, bool value);
    QString getItemValue(QString itemName, QString valueName);
    void getItemValue(QString itemName, QString valueName, bool &value);
    void setDisplayStartValue(uint16_t val);
    void sendDisplayStartValue();

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    SoftAdapterSettingsStruct_t adapterSettings;
    SoftRecorderSettingsStruct_t recorderSettings;
    std::shared_ptr<AdapterSettingsControl> adapterSettingsControl;
    std::shared_ptr<RecorderSettingsContol> recorderSettingsControl;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusControl;
    std::future<void> updateWorker;
    std::shared_ptr<IUserManipulators> userManipulators;
    IndicatorSettingsModel settingsModel;
    IndicatorIndexModel indexModel;
    bool isBrightnessChanged;
    bool isIndicatorDataChanged;
    int lastIndicationBrightnessIndex;


signals:
    void signalRecivedAdapterData();
private slots:
    void slotRecievedAdapterData();
    void slotShowComboDrum(QVariant type);
    void slotSaveButtonClicked();
    void slotCancelButtonClicked();
};

QT_BEGIN_NAMESPACE

#define IndicationWindowPluginInterface "IndicationWindow"

Q_DECLARE_INTERFACE(IndicationWindow, IndicationWindowPluginInterface)

QT_END_NAMESPACE

#endif // MONITORINGWINDOWPLUGIN_H
