#include <assert.h>

#include <QQmlComponent>

#include "indicationwindow.h"

using namespace rosslog;

IndicationWindow::IndicationWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    dataProvider(nullptr),
    adapterSettingsControl(nullptr),
    recorderSettingsControl(nullptr),
    adapterStatusControl(nullptr),
    userManipulators(nullptr),
    isBrightnessChanged(false),
    isIndicatorDataChanged(false)
{
    memset(&indexModel, 0, sizeof(IndicatorIndexModel));
}

std::shared_ptr<IUserManipulators> IndicationWindow::initWindow(EWindSizeType winSizeType,
                                                                      std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                      std::shared_ptr<IStateWidget> stateWidget,
                                                                      std::shared_ptr<IUserManipulators> userManipulators,
                                                                      std::shared_ptr<IQmlEngine> engine,
                                                                      std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    Q_UNUSED(log);
    assert(engine);
    assert(dataProvider);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;
    lastIndicationBrightnessIndex = START_BRIGHTNESS;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(QML_PATH);
        assert(windowComponent);

        connect(this, SIGNAL(signalRecivedAdapterData()),
                this, SLOT(slotRecievedAdapterData()));
        connect(windowComponent, SIGNAL(saveButtonClicked()) ,
                this, SLOT(slotSaveButtonClicked()));
        connect(windowComponent, SIGNAL(cancelButtonClicked()) ,
                this, SLOT(slotCancelButtonClicked()));
        connect(windowComponent, SIGNAL(showComboDrum(QVariant)) ,
                this, SLOT(slotShowComboDrum(QVariant)));
    }
    initAdapterSettingsControl();
    initRecorderSettingsControl();
    initAdapterStatusControl();

    indexModel.displayBrightness = 99;
//    setDisplayStartValue();
    sendDisplayStartValue();
    return nullptr;
}

QObject *IndicationWindow::getComponentObject()
{
    return windowComponent;
}

std::string IndicationWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string IndicationWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string IndicationWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string IndicationWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string IndicationWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap IndicationWindow::getIcon()
{
    return QPixmap(":/IndicationWindow/icons/i30.png");
}

std::pair<bool, QPixmap> IndicationWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/IndicationWindow/icons/i20.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > IndicationWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool IndicationWindow::showWindow()
{
    updateData();
    return true;
}

bool IndicationWindow::hideWindow()
{
    return true;
}

EWindSizeType IndicationWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool IndicationWindow::prepareToDelete()
{
    return true;
}

void IndicationWindow::initAdapterSettingsControl()
{
    if( adapterSettingsControl == nullptr )
    {
        adapterSettingsControl = std::make_shared<AdapterSettingsControl>(
                dataProvider, *logger.get());
    }
}

void IndicationWindow::initAdapterStatusControl()
{
    if( adapterStatusControl == nullptr )
    {
        adapterStatusControl = std::make_shared<adapter_status_ctrl::AdapterStatusControl>(
                dataProvider, *logger.get());
    }
}


void IndicationWindow::initRecorderSettingsControl()
{
    if( recorderSettingsControl == nullptr )
    {
        recorderSettingsControl = std::make_shared<RecorderSettingsContol>(
                dataProvider, *logger.get());
    }
}

void IndicationWindow::setItemValue(QString itemName, QString valueName, QString value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

void IndicationWindow::setItemValue(QString itemName, QString valueName, bool value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

void IndicationWindow::setRecievedData()
{
    uint8_t index = 0;
    try
    {

        setItemValue("AdapterAutoOff", "txttxt",
             QString::fromStdString(settingsModel.getAdapterValue(adapterSettings.Sleep, index)));

        indexModel.adapterValuesIndex = index;
        if( recorderSettings.Sleep > 0 )
        {
            setItemValue("RecorderAutoOff", "txttxt",
                            QString::fromStdString(settingsModel.getValuesName(RECORDER, RECORDER_5_MIN)));
            indexModel.recorderValuesIndex = RECORDER_5_MIN;
        }
        else
        {
            setItemValue("RecorderAutoOff", "txttxt",
                            QString::fromStdString(settingsModel.getValuesName(RECORDER, RECORDER_FORBIDDEN)));
            indexModel.recorderValuesIndex = RECORDER_FORBIDDEN;
        }


        switch( recorderSettings.Power & AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE )
        {
            case AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_OFF:
            {
                setItemValue("LED", "txttxt",
                                QString::fromStdString(settingsModel.getValuesName(LED, LED_OFF)));
                indexModel.ledValuesIndex = LED_OFF;
                break;
            }
            case AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_ON:
            {
                setItemValue("LED", "txttxt",
                                QString::fromStdString(settingsModel.getValuesName(LED, LED_ON)));
                indexModel.ledValuesIndex = LED_ON;
                break;
            }
            case AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_AUTO:
            default:
            {
                setItemValue("LED", "txttxt",
                                QString::fromStdString(settingsModel.getValuesName(LED, LED_AUTO)));
                indexModel.ledValuesIndex = LED_AUTO;
            }
        }

        setItemValue("DisplayAutoOff", "txttxt",
             QString::fromStdString(settingsModel.getDisplayValue(recorderSettings.Padding1, index)));
        indexModel.displayVauesIndex = index;

        std::cerr << "RECIEVE BRIGHTNSESS = " << recorderSettings.displayBright << std::endl;

        if( recorderSettings.displayBright != indexModel.displayBrightness )
        {
            isBrightnessChanged = true;
            indexModel.displayBrightness = recorderSettings.displayBright;
            lastIndicationBrightnessIndex = recorderSettings.displayBright;

            setItemValue("DisplayBrightness", "txttxt",
                         QString::fromStdString(settingsModel.getValuesName(DISPLAY_BRIGHTNESS, indexModel.displayBrightness)));

            saveData();
        }

    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
#warning Show Error
    }
}

void IndicationWindow::saveData()
{
        userManipulators->showLoadingWidget();

        updateWorker = std::async( std::launch::async,[this]
        {
            if( isBrightnessChanged )
            {
                try
                {
                    recorderSettingsControl->updateStatus();
                    recorderSettings = recorderSettingsControl->getRecordSettings();
                    recorderSettings.displayBright = (uint16_t)indexModel.displayBrightness;
                    recorderSettingsControl->saveRecordSettings(recorderSettings);

                    std::cerr << "SAVE DISPLAY INDEX = " << (int)indexModel.displayBrightness << std::endl;
                    adapterStatusControl->setDisplayBrightness(indexModel.displayBrightness + 1);
                    isBrightnessChanged = false;
                }
                catch (std::system_error &except)
                {
                    userManipulators->hideLoadingWidget();
                    this->logger->Message(LOG_ERR, __LINE__, __func__,
                                          dpr::ProviderError::getInstance().getErrorString(except).c_str());
                    userManipulators->showMessage(std::string("Не удалось сохранить данные подсветки.\nКод ошибки: ").append(
                            std::to_string(except.code().value())));
                    return;
                }
            }
            if( isIndicatorDataChanged  )
            {
                try
                {
                    adapterSettingsControl->updateSettings();
                    adapterSettings = adapterSettingsControl->getAdapterSettings().Block0.AdapterSettings;
                    recorderSettingsControl->updateStatus();
                    recorderSettings = recorderSettingsControl->getRecordSettings();


                    if( !packSettedData() )
                    {
                        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while packing indicator data");
                        this->userManipulators->showMessage("Не удалось подготовить данные");
                    }
                    else
                    {
                        recorderSettingsControl->saveRecordSettings(recorderSettings);
                        adapterSettingsControl->saveAdapterSettings(adapterSettings);
                    }
                    isIndicatorDataChanged = false;
                    userManipulators->hideLoadingWidget();
                }
                catch (std::system_error &except)
                {
                    userManipulators->hideLoadingWidget();
                    this->logger->Message(LOG_ERR, __LINE__, __func__,
                                          dpr::ProviderError::getInstance().getErrorString(except).c_str());
                    userManipulators->showMessage(std::string("Не удалось сохранить данные.\nКод ошибки: ").append(
                            std::to_string(except.code().value())));
                }
            }
            userManipulators->hideLoadingWidget();
        });
        updateWorker.wait_for(std::chrono::seconds(0));
}


bool IndicationWindow::packSettedData()
{
    switch( indexModel.ledValuesIndex )
    {
        case LED_OFF:
        {
            recorderSettings.Power &= ~AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE;
            recorderSettings.Power|= AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_OFF;
            break;
        }
        case LED_ON:
        {
            recorderSettings.Power &= ~AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE;
            recorderSettings.Power |= AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_ON;
            break;
        }
        case LED_AUTO:
        {
            recorderSettings.Power &= ~AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE;
            recorderSettings.Power |= AXIS_SOFTRECORDER_SETTINGS__POWER_LED_MODE_AUTO;
            break;
        }
        default:
        {
            break;
        }
    }

    switch( indexModel.recorderValuesIndex )
    {
        case RECORDER_FORBIDDEN:
        {
            recorderSettings.Sleep = 0;
            break;
        }
        case RECORDER_5_MIN:
        {
            recorderSettings.Sleep = 300;
            break;
        }
        default:
        {
            break;
        }
    }

    int res = settingsModel.getAdapterDigitValue(indexModel.adapterValuesIndex);

    if( res < 0 )
        return  false;

    adapterSettings.Sleep = res;

    res = settingsModel.getDisplayDigitValue(indexModel.displayVauesIndex);

    if( res < 0 )
        return  false;

    recorderSettings.Padding1 = res;

    return true;
}

void IndicationWindow::updateData()
{
    userManipulators->showLoadingWidget();
    updateWorker = std::async(std::launch::async, [this] () mutable
    {
        try
        {

            adapterSettingsControl->updateSettings();
            adapterSettings = adapterSettingsControl->getAdapterSettings().Block0.AdapterSettings;
            recorderSettingsControl->updateStatus();
            recorderSettings = recorderSettingsControl->getRecordSettings();
            userManipulators->hideLoadingWidget();
            emit signalRecivedAdapterData();
        }
        catch (std::system_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            userManipulators->showMessage(std::string("Не удалось обновить данные.\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
            userManipulators->hideLoadingWidget();
        }

    });
    updateWorker.wait_for(std::chrono::seconds(0));
}

QString IndicationWindow::getItemValue(QString itemName, QString valueName)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    return QQmlProperty(item, valueName).read().toString();
}

void IndicationWindow::getItemValue(QString itemName, QString valueName, bool &value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    value = QQmlProperty(item, valueName).read().toBool();
}

void IndicationWindow::slotRecievedAdapterData()
{
    setRecievedData();
}

void IndicationWindow::slotShowComboDrum(QVariant type)
{
    SettingsType settingsType = static_cast<SettingsType>(type.toInt());
    try
    {
        int index = 0;
        switch (settingsType)
        {
            case LED:
                index = indexModel.ledValuesIndex;
                break;
            case ADAPTER:
                index = indexModel.adapterValuesIndex;
                break;
            case RECORDER:
                index = indexModel.recorderValuesIndex;
                break;
            case DISPLAY:
                index = indexModel.displayVauesIndex;
                break;
            case DISPLAY_BRIGHTNESS:
                index = indexModel.displayBrightness;
        }
        auto res = userManipulators->openComboDrum(index,
                    settingsModel.getValuesByType(settingsType));
        if( res.first )
        {
            switch (settingsType)
            {
                case LED:
                    if( res.second != indexModel.ledValuesIndex )
                    {
                        setItemValue("LED", "txttxt",
                                     QString::fromStdString(settingsModel.getValuesName(settingsType, res.second)));
                        indexModel.ledValuesIndex = res.second;
                        isIndicatorDataChanged = true;
                    }
                    break;
                case ADAPTER:
                    if( res.second != indexModel.adapterValuesIndex )
                    {
                        setItemValue("AdapterAutoOff", "txttxt",
                                     QString::fromStdString(settingsModel.getValuesName(settingsType, res.second)));
                        indexModel.adapterValuesIndex = res.second;
                        isIndicatorDataChanged = true;
                    }
                    break;
                case RECORDER:
                    if( res.second != indexModel.recorderValuesIndex )
                    {
                        setItemValue("RecorderAutoOff", "txttxt",
                                     QString::fromStdString(settingsModel.getValuesName(settingsType, res.second)));
                        indexModel.recorderValuesIndex = res.second;
                        isIndicatorDataChanged = true;
                    }
                    break;
                case DISPLAY:
                    if( res.second != indexModel.displayVauesIndex )
                    {
                        setItemValue("DisplayAutoOff", "txttxt",
                                     QString::fromStdString(settingsModel.getValuesName(settingsType, res.second)));
                        indexModel.displayVauesIndex = res.second;
                        isIndicatorDataChanged = true;
                    }
                    break;
                case DISPLAY_BRIGHTNESS:
                    if(res.second != indexModel.displayBrightness)
                    {
                        isBrightnessChanged = true;
                        setItemValue("DisplayBrightness", "txttxt",
                                     QString::fromStdString(settingsModel.getValuesName(settingsType, res.second)));
                        indexModel.displayBrightness = res.second;
                        //saveData();
                    }

                default:
                    break;
            }
        }
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
#warning show error
    }
}


void IndicationWindow::slotSaveButtonClicked()
{
    if(indexModel.adapterValuesIndex != 0 && indexModel.displayVauesIndex == 0)
    {
        userManipulators->showMessage("Для активации автовыключения адаптера необходимо разрешить автовыключение дисплея.");
    }
    else
    {
        saveData();
    }
}


void IndicationWindow::slotCancelButtonClicked()
{
    updateData();
    setItemValue("DisplayBrightness", "txttxt",
                 QString::fromStdString(settingsModel.getValuesName(DISPLAY_BRIGHTNESS, lastIndicationBrightnessIndex)));
    indexModel.displayBrightness = lastIndicationBrightnessIndex;
}

void IndicationWindow::setDisplayStartValue(uint16_t val)
{
    setItemValue("DisplayBrightness", "txttxt",
                 QString::fromStdString(settingsModel.getValuesName(DISPLAY_BRIGHTNESS, val)));
    indexModel.displayBrightness = val;
}

void IndicationWindow::sendDisplayStartValue()
{
    updateWorker = std::async( std::launch::async,[this]
    {
        while(true)
        {
            try
            {
                recorderSettingsControl->updateStatus();
                setDisplayStartValue(recorderSettingsControl->getRecordSettings().displayBright);

                lastIndicationBrightnessIndex = recorderSettingsControl->getRecordSettings().displayBright;
                adapterStatusControl->setDisplayBrightness((recorderSettingsControl->getRecordSettings().displayBright + 1));
                break;
            }
            catch (std::system_error &except)
            {
                sleep(2);
                this->logger->Message(LOG_ERR, __LINE__, __func__,
                                      dpr::ProviderError::getInstance().getErrorString(except).c_str());
//                userManipulators->showMessage(std::string("Не удалось установить параметры подсветки.\nКод ошибки: ").append(
//                        std::to_string(except.code().value())));
                continue;
            }
        }
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}


IndicationWindow::~IndicationWindow()
{
    std::cerr << "Removed IndicationWindow" << std::endl;
}
