#ifndef INDICATORSETTINGSMODEL_H
#define INDICATORSETTINGSMODEL_H


#include <vector>
#include <map>

typedef enum
{
    LED,
    RECORDER,
    ADAPTER,
    DISPLAY,
    DISPLAY_BRIGHTNESS
} SettingsType;

typedef enum
{
    LED_OFF = 0,
    LED_ON ,
    LED_AUTO
} LedValuesTypes;

typedef enum
{
    ADAPTER_OFF_0 = 0,
    ADAPTER_OFF_30 ,
    ADAPTER_OFF_60,
    ADAPTER_OFF_300,
    ADAPTER_OFF_1800,
    ADAPTER_OFF_3600,
    ADAPTER_OFF_10800,
    ADAPTER_OFF_18000
} AdapterValuesTypes;

typedef enum
{
    DISPLAY_OFF_0 = 0,
    DISPLAY_OFF_60,
    DISPLAY_OFF_300
} DisplayValuesTypes;

typedef enum
{
    RECORDER_FORBIDDEN,
    RECORDER_5_MIN
} RecorderValuesTypes;

typedef struct
{
    uint8_t adapterValuesIndex;
    uint8_t displayVauesIndex;
    uint8_t recorderValuesIndex;
    uint8_t ledValuesIndex;
    uint8_t displayBrightness;
} IndicatorIndexModel;

class IndicatorSettingsModel
{
public:
    IndicatorSettingsModel();

    const std::vector<std::string>& getValuesByType(SettingsType type);
    std::string getValuesName(const SettingsType &type, const int &val);
    std::string getAdapterValue(int val, uint8_t &index);
    std::string getDisplayValue(int val, uint8_t &index);
    int getAdapterDigitValue(const uint8_t &index);
    int getDisplayDigitValue(const uint8_t &index);

private:
    std::map<SettingsType, std::vector<std::string>> values;
    std::map<int, AdapterValuesTypes> adapterValuesMap;
    std::map<int, DisplayValuesTypes> displayValuesMap;



};

#endif // INDICATORSETTINGSMODEL_H
