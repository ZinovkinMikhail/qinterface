#include "indicatorsettingsmodel.h"

IndicatorSettingsModel::IndicatorSettingsModel()
{
    std::vector<std::string> ledValues = {"Всегда выключена", "Всегда включена" ,
                                          " Автовыключение"};
    std::vector<std::string> recorderValues = {"Запрещено", "Через 5 мин."};
    std::vector<std::string> adapterValues = {"Запрещено","Через 30 сек." ,"Через 1 мин.",
                                             "Через 5 мин.", "Через 30 мин.",
                                             "Через 1 час", "Через 3 часа"
                                             ,"Через 5 часов"};
    std::vector<std::string> displayValues = {"Никогда", "1 мин." ,
                                          "5 мин."};

    std::vector<std::string> displayBrightnessVector;

    displayBrightnessVector.reserve(100);
    for( int i = 0; i < 100; i++ )
    {
        displayBrightnessVector.push_back(std::to_string(i + 1));
    }

    values[LED] = ledValues;
    values[RECORDER] = recorderValues;
    values[ADAPTER] = adapterValues;
    values[DISPLAY] = displayValues;
    values[DISPLAY_BRIGHTNESS] = displayBrightnessVector;

    adapterValuesMap[0] = ADAPTER_OFF_0;
    adapterValuesMap[30] = ADAPTER_OFF_30;
    adapterValuesMap[60] = ADAPTER_OFF_60;
    adapterValuesMap[300] = ADAPTER_OFF_300;
    adapterValuesMap[1800] = ADAPTER_OFF_1800;
    adapterValuesMap[3600] = ADAPTER_OFF_3600;
    adapterValuesMap[10800] = ADAPTER_OFF_10800;
    adapterValuesMap[18000] = ADAPTER_OFF_18000;

    displayValuesMap[0] = DISPLAY_OFF_0;
    displayValuesMap[60] = DISPLAY_OFF_60;
    displayValuesMap[300] = DISPLAY_OFF_300;


}

const std::vector<std::string>& IndicatorSettingsModel::getValuesByType(SettingsType type)
{
    return values[type];
}

std::string IndicatorSettingsModel::getValuesName(const SettingsType &type,
                                                  const int &val)
{
    return values[type].at(val);
}

std::string IndicatorSettingsModel::getAdapterValue(int val, uint8_t &index)
{
    auto it = adapterValuesMap.find(val);
    if( it != adapterValuesMap.end() )
    {
        index = it->second;
        return values[ADAPTER].at(it->second);
    }
    return values[ADAPTER].at(ADAPTER_OFF_0);
}

std::string IndicatorSettingsModel::getDisplayValue(int val, uint8_t &index)
{
    auto it = displayValuesMap.find(val);
    if( it != displayValuesMap.end() )
    {
        index = it->second;
        return values[DISPLAY].at(it->second);
    }
    return values[DISPLAY].at(DISPLAY_OFF_0);
}

int IndicatorSettingsModel::getAdapterDigitValue(const uint8_t &index)
{
    for( auto &it : adapterValuesMap )
    {
        if( it.second == static_cast<AdapterValuesTypes>(index) )
        {
            return  it.first;
        }
    }
    return  -1;
}

int IndicatorSettingsModel::getDisplayDigitValue(const uint8_t &index)
{
    for( auto &it : displayValuesMap )
    {
        if( it.second == static_cast<DisplayValuesTypes>(index) )
        {
            return  it.first;
        }
    }
    return  -1;
}

