#ifndef SETTINGSPARAMSWINDOW_H
#define SETTINGSPARAMSWINDOW_H

#include <future>
#include <arpa/inet.h>
#include <iostream>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlProperty>
#include <QTextCodec>
#include <QDateTime>

#include <axis_ifconfig.h>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/recordersettingscontol.h>

#define WINDOW_IDENTIFICATOR "DateTimeWindow"
#define WINDOW_FOLDER_NAME "Recorder"
#define ICON_PRESENTATION_NAME "Дата и время"
#define FOLDER_PRESENTATION_NAME "Накопитель"

#define QML_PATH "qrc:/DateTime/DateTimeWindow.qml"

class DateTimeWindow : public QObject, public IEmptyPlaceWindow
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "DateTimeWindow")
public:
    explicit DateTimeWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~DateTimeWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;

private:
    void initRecordSettingsControl();
    void setRecievedData();
    void saveData();
    bool packSettedData(uint64_t &time);

    void setItemValue(QString itemName, QString valueName, QString value);
    void setItemValue(QString itemName, QString valueName, bool value);
    QString getItemValue(QString itemName, QString valueName);
    void getItemValue(QString itemName, QString valueName, bool &value);

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    SoftRecorderSettingsStruct_t recordSettings;
    std::shared_ptr<RecorderSettingsContol> recordSettingsControl;
    std::future<void> updateWorker;
    std::shared_ptr<IUserManipulators> userManipulators;


signals:
    void signalRecivedAdapterData();
private slots:
    void slotRecievedAdapterData();
    void slotShowComboDrumDate();
    void slotShowComboDrumTime();
    void slotSaveButtonClicked();
    void slotCancelButtonClicked();
};

QT_BEGIN_NAMESPACE

#define DateTimeWindowPluginInterface "DateTimeWindow"

Q_DECLARE_INTERFACE(DateTimeWindow, DateTimeWindowPluginInterface)

QT_END_NAMESPACE

#endif // MONITORINGWINDOWPLUGIN_H
