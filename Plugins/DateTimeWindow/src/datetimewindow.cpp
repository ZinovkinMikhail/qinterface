#include <assert.h>

#include <QQmlComponent>

#include "datetimewindow.h"

using namespace rosslog;

DateTimeWindow::DateTimeWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    dataProvider(nullptr),
    recordSettingsControl(nullptr),
    userManipulators(nullptr)
{}

std::shared_ptr<IUserManipulators> DateTimeWindow::initWindow(EWindSizeType winSizeType,
                                                                      std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                      std::shared_ptr<IStateWidget> stateWidget,
                                                                      std::shared_ptr<IUserManipulators> userManipulators,
                                                                      std::shared_ptr<IQmlEngine> engine,
                                                                      std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    Q_UNUSED(log);
    assert(engine);
    assert(dataProvider);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(QML_PATH);
        assert(windowComponent);

        connect(this, SIGNAL(signalRecivedAdapterData()),
                this, SLOT(slotRecievedAdapterData()));
        connect(windowComponent, SIGNAL(saveButtonClicked()) ,
                this, SLOT(slotSaveButtonClicked()));
        connect(windowComponent, SIGNAL(cancelButtonClicked()) ,
                this, SLOT(slotCancelButtonClicked()));
        connect(windowComponent, SIGNAL(showComboDrumTime()) ,
                this, SLOT(slotShowComboDrumTime()));
        connect(windowComponent, SIGNAL(showComboDrumDate()) ,
                this, SLOT(slotShowComboDrumDate()));
    }
    initRecordSettingsControl();

    return nullptr;
}

QObject *DateTimeWindow::getComponentObject()
{
    return windowComponent;
}

std::string DateTimeWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string DateTimeWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string DateTimeWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string DateTimeWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string DateTimeWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap DateTimeWindow::getIcon()
{
    return QPixmap(":/DateTime/icons/i28.png");
}

std::pair<bool, QPixmap> DateTimeWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/DateTime/icons/i18.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > DateTimeWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool DateTimeWindow::showWindow()
{
    setRecievedData();
    return true;
}

bool DateTimeWindow::hideWindow()
{
    return  true;
}

EWindSizeType DateTimeWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool DateTimeWindow::prepareToDelete()
{
    return true;
}

void DateTimeWindow::initRecordSettingsControl()
{
    if( recordSettingsControl == nullptr )
    {
        recordSettingsControl = std::shared_ptr<RecorderSettingsContol>(
                new RecorderSettingsContol(dataProvider, *logger.get()));
    }
}

void DateTimeWindow::setItemValue(QString itemName, QString valueName, QString value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

void DateTimeWindow::setItemValue(QString itemName, QString valueName, bool value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

void DateTimeWindow::setRecievedData()
{
    QDateTime dateTime = QDateTime::currentDateTime();

    setItemValue("Time", "txttxt", dateTime.time().toString("hh:mm"));
    setItemValue("Date", "txttxt", dateTime.toString("dd.MM.yyyy"));
}

void DateTimeWindow::saveData()
{
    uint64_t time;
    if( packSettedData(time) )
    {
        userManipulators->showLoadingWidget();
        updateWorker = std::async( std::launch::async,[this, time]
        {
            try
            {
                recordSettingsControl->updateStatus();
                recordSettings = recordSettingsControl->getRecordSettings();
                recordSettings.Time = time;
                recordSettingsControl->saveRecordSettings(recordSettings);
                userManipulators->hideLoadingWidget();
            } catch (std::system_error &except)
            {
                userManipulators->hideLoadingWidget();
                this->logger->Message(LOG_ERR, __LINE__, __func__,
                                      dpr::ProviderError::getInstance().getErrorString(except).c_str());
                userManipulators->showMessage(std::string("Не удалось сохранить время.\nКод ошибки: ").append(
                                                  std::to_string(except.code().value())));
            }

            return;
        });
        updateWorker.wait_for(std::chrono::seconds(0));
    }
    else
    {
#warning show Error
    }
}


bool DateTimeWindow::packSettedData(uint64_t &time)
{
    QDate date = QDate::fromString(getItemValue("Date", "txttxt"), "dd.MM.yyyy");
    QTime qttime = QTime::fromString(getItemValue("Time", "txttxt"), "hh:mm");

    QDateTime dateTime;
    dateTime.setDate(date);
    dateTime.setTime(qttime);

    if( !dateTime.isValid() )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Date time is invalid");
        return  false;
    }

//    struct_data data;
//    memset(&data,0,sizeof(struct_data));

//    data.hour =  system_time_d_to_hexd(time.hour());
//    data.min =  system_time_d_to_hexd(time.minute());
//    data.day = system_time_d_to_hexd(date.day());
//    data.mon = system_time_d_to_hexd(date.month());
//    data.year = system_time_d_to_hexd(date.year() - 2000);
//    settings.Time = system_time_convert(data);
    time = dateTime.toSecsSinceEpoch(); //Test on device
    return true;
}

QString DateTimeWindow::getItemValue(QString itemName, QString valueName)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    return QQmlProperty(item, valueName).read().toString();
}

void DateTimeWindow::getItemValue(QString itemName, QString valueName, bool &value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    value = QQmlProperty(item, valueName).read().toBool();
}

void DateTimeWindow::slotRecievedAdapterData()
{
    setRecievedData();
}

void DateTimeWindow::slotShowComboDrumDate()
{
    QDate date = QDate::currentDate();
    auto res = this->userManipulators->openDateComboDrum(date.day(), date.month(), date.year());
    if( res.first )
    {
        setItemValue("Date", "txttxt", QString::fromStdString(res.second));
    }
}

void DateTimeWindow::slotShowComboDrumTime()
{
    QTime time = QTime::currentTime();
    auto res = this->userManipulators->openTimeComboDrum(time.hour(), time.minute(), time.second());
    if( res.first )
    {
        setItemValue("Time", "txttxt", QString::fromStdString(res.second));
    }
}

void DateTimeWindow::slotSaveButtonClicked()
{
    saveData();
}


void DateTimeWindow::slotCancelButtonClicked()
{
    setRecievedData();
}

DateTimeWindow::~DateTimeWindow()
{
    std::cerr << "Removed DateTimeWindow" << std::endl;
}
