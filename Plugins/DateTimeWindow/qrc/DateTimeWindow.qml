import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Item
{
    id: dateTimeWindow
    y: 150
    width: 1280
    height: 484

    ColorConfig{ id: cc}
    FontsConfig{id:fontConf}

    signal showComboDrumTime()
    signal showComboDrumDate()
    signal saveButtonClicked()
    signal cancelButtonClicked()

    BackgroundTitle
    {
        text: qsTr("Время и дата")
    }

    Rectangle
    {
        id:back
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 300
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }

    Rectangle
    {
        id:mainZone
        anchors.fill: back
        anchors.margins: 18
        color: cc.dark_blue
        clip: true

        Column
        {
            anchors.centerIn: parent
            spacing: 40

            Row
            {
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 150
                    font.family: fontConf.mainFont
                    font.pixelSize: fontConf.pixSize_S5 + 6
                    color: cc.light_green
                    text: qsTr("Время:")
                }

                ButtonDrop
                {
                    type: "time"
                    objectName: "Time"
                    txttxt: "00:00:00"
                    onButtonClick: showComboDrumTime()
                }
            }

            Row
            {
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 150
                    font.family: fontConf.mainFont
                    font.pixelSize: fontConf.pixSize_S5 + 6
                    color: cc.light_green
                    text: qsTr("Дата:")
                }

                ButtonDrop
                {
                    type: "date"
                    objectName: "Date"
                    txttxt: "01.01.2019"
                    onButtonClick: showComboDrumDate()
                }
            }
        }
    }

    Item
    {
        anchors.top: back.bottom
        anchors.right: back.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        width: 470

        ControlListButton
        {
            anchors.right: parent.right
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked: cancelButtonClicked()

        }
        ControlListButton
        {
            anchors.left: parent.left
            type: "SlimType"
            text: qsTr("Сохранить")
            onClicked: saveButtonClicked()
        }
    }
}
