#ifndef IRECORDERWINDOW_H
#define IRECORDERWINDOW_H


#include <memory>
#include "recorderparamsmodel.h"

class IRecorderWindow
{
public:
    virtual void valueChanged(int channel, ParameterRow row, int index) = 0;
};


#endif // IRECORDERWINDOW_H
