#ifndef CHANNELCOLUMN_H
#define CHANNELCOLUMN_H

#include <memory>

#include <QObject>
#include <QQuickItem>

#include <iqmlengine.h>
#include <iusermanipulators.h>

#include "recorderparamsmodel.h"
#include "parameterbox.h"
#include "iparametercolumn.h"

#define PARAMETERS_COLUMN_QML_PATH "qrc:/ParametersColumn.qml"

class ParametersColumn : public QObject, public IParameterUserChanges,
        public std::enable_shared_from_this<ParametersColumn>
{
    Q_OBJECT
public:
    explicit ParametersColumn(std::shared_ptr<IQmlEngine> qmlEngine,
                           std::shared_ptr<RecorderParamsModel> paramsModel,
                           std::shared_ptr<IUserManipulators> userManipulators,
                           std::shared_ptr<IParameterUserChanges> userChangesInterface,
                           QObject *parent = nullptr);

    void init(int channelNum, std::list<ParameterRow> rows);
    void disable();
    void disableRecordParams();
    void enableRecordParams();
    void enableRecordParam(ParameterRow row);
    void enable();
    void setStereoParam(int index, ParameterRow row);
    void update();
    void newData();
    void resetParams();
    void changeParameter(ParameterRow row);
    void disableParameter(ParameterRow row);
    void valueChanged(int channelNum, ParameterRow row, int index) override;

private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<RecorderParamsModel> paramsModel;
    QQuickItem *currentItem;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<IParameterUserChanges> userChangesInterface;
    int channelNum;

    std::map<ParameterRow, std::shared_ptr<ParameterBox>> parameterBoxes;


signals:

public slots:
};

#endif // CHANNELCOLUMN_H
