#ifndef MONITORINGWINDOWPLUGIN_H
#define MONITORINGWINDOWPLUGIN_H

#include <math.h>
#include <future>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/recordersettingscontol.h>
#include <dataprovider/recorderstatuscontrol.h>

#include "parameterspage.h"
#include "recorderparamsmodel.h"
#include "irecorderwindow.h"

#define WINDOW_IDENTIFICATOR "RecorderParamsWindow"
#define WINDOW_FOLDER_NAME "Recorder"
#define ICON_PRESENTATION_NAME "Параметры"
#define FOLDER_PRESENTATION_NAME "Накопитель"

#define PARAMS_PER_PAGE 5

#define CHANNEL1_NUM 0
#define CHANNEL2_NUM 1
#define CHANNEL3_NUM 2
#define CHANNEL4_NUM 3



class RecorderParamsWindow : public QObject, public IEmptyPlaceWindow,
        public IRecorderWindow
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "RecorderParamsWindow")
public:
    explicit RecorderParamsWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~RecorderParamsWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;


    void valueChanged(int channel, ParameterRow row, int index) override;

private:
    void initRecorderStatusControl();
    void initRecorderSettingsControl();
    void sethighlightsChannel(int channel, bool state);
    void fillPages();
    void changeValuesByMode(int channel, ParameterRow row);
    void setRecievedData();
    void updateData();
    void getInfoFromAdapter();
    ChannelCompression getCompressionType(uint16_t value);
    SensivityType getSensivityType(uint16_t value);
    void setCompressionPosition(uint16_t &compression,
                                const ChannelCompression type);
    void setSensivityValue(uint16_t &input, SensivityType type);
    ChannelAudioBand getAudiBandType(const uint16_t &value);
    bool packDataForSend();

    void saveDataOnAdapter(SoftRecorderSettingsStruct_t data);
    void applyDataOnAdapter(SoftRecorderSettingsStruct_t data);

    bool lastEnabledChannelCheck();
    bool isRecordingNow();
    bool isMonitoringNow();
    bool isAudioPlayNow();

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusCtrl;
    std::shared_ptr<RecorderSettingsContol> recorderSettingsCtrl;
    std::shared_ptr<IProcessWidget> monitoringProcessWidget;
    std::map<int, std::shared_ptr<ParametersPage>> parametersPages;
    std::shared_ptr<RecorderParamsModel> recorderParamsModel;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::vector<std::shared_ptr<ParametersPage>> pages;
    std::map<int, std::shared_ptr<RecorderParamsModel>> channelsMap;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    std::future<void> updateWorker;

    SoftRecorderSettingsStruct_t recordSettings;
    SoftRecorderStatusStruct_s recordStatus;


signals:
    void signalUpdateLocalForms();
    void signalSettingsApplyed();

public slots:
    void slotTimerTimeout();
    void slotCancelButtonClicked();
    void slotApplyButtonClicked();
    void slotSaveButtonClicked();
    void slotUpdateLocalForms();
    void slotSettingsApplyed();
};

#endif // MONITORINGWINDOWPLUGIN_H
