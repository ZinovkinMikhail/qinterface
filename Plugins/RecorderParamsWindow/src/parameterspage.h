#ifndef PARAMETERSPAGE_H
#define PARAMETERSPAGE_H

#include <QObject>
#include <QQuickItem>

#include <iqmlengine.h>
#include <iusermanipulators.h>

#include "recorderparamsmodel.h"
#include "parameterscolumn.h"
#include "irecorderwindow.h"

#define PARAMETERS_PAGE_QML_PATH "qrc:/ParametersList.qml"

class ParametersPage : public QObject, public IParameterUserChanges,
        public std::enable_shared_from_this<ParametersPage>
{
    Q_OBJECT
public:
    explicit ParametersPage(std::shared_ptr<IQmlEngine> qmlEngine,
                            std::shared_ptr<RecorderParamsModel> paramsModel,
                            std::shared_ptr<IUserManipulators> userManipulators,
                            IRecorderWindow *recorderWindowInterf,
                            QObject *parent = nullptr);

    void init(std::map<int, std::shared_ptr<RecorderParamsModel> > channels,
              std::list<ParameterRow> rows);
    void enableChannel(int channel);
    void enableRecordParams();
    void enableRecordParam(ParameterRow row);
    void disableChannel(int channel);
    void disableRecordParams();
    void setStereoParam(int index, int channel, ParameterRow row);
    void updateParams();
    void setNewData();
    void resetParams();
    void changeRowParameter(ParameterRow row);
    void disableParameter(int channelNum, ParameterRow row);

    void valueChanged(int channelNum, ParameterRow row, int index) override;

private:
    std::shared_ptr<RecorderParamsModel> paramsModel;
    std::shared_ptr<IQmlEngine> engine;
    QQuickItem *currentItem;
    std::shared_ptr<IUserManipulators> userManipulators;
    IRecorderWindow *recorderWindowInterf;

    std::map<int, std::shared_ptr<ParametersColumn>> columns;

signals:

public slots:
};

#endif // PARAMETERSPAGE_H
