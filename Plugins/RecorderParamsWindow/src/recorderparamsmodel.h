#ifndef RECORDERPARAMSMODEL_H
#define RECORDERPARAMSMODEL_H

#include <map>
#include <vector>
#include <list>
#include <algorithm>

#include <axis_softrecorder.h>

#define CHANNEL1_ENABLE_POS 0
#define CHANNEL2_ENABLE_POS 1
#define CHANNEL3_ENABLE_POS 2
#define CHANNEL4_ENABLE_POS 3
#define CHANNEL1_STEREO_POS 8
#define CHANNEL3_STEREO_POS 9

#define CHANNEL_1_LPF_ENABLE 8
#define CHANNEL_2_LPF_ENABLE 9
#define CHANNEL_3_LPF_ENABLE 10
#define CHANNEL_4_LPF_ENABLE 11

#define CHANNEL_COMPRESSION_1_1 AXIS_SOFTRECORDER_SETTINGS__AUDIO_COMPRESSION_1_1
#define CHANNEL_COMPRESSION_1_3 AXIS_SOFTRECORDER_SETTINGS__AUDIO_COMPRESSION_1_3
#define CHANNEL_COMPRESSION_1_4 AXIS_SOFTRECORDER_SETTINGS__AUDIO_COMPRESSION_1_4
#define CHANNEL_COMPRESSION_1_5 AXIS_SOFTRECORDER_SETTINGS__AUDIO_COMPRESSION_1_5
#define CHANNEL_COMPRESSION_1_8 AXIS_SOFTRECORDER_SETTINGS__AUDIO_COMPRESSION_1_8

#define INPUT_CONFIG AXIS_SOFTRECORDER_SETTINGS__INPUT_1_CFG
#define INPUT_OFF AXIS_SOFTRECORDER_SETTINGS__INPUT_1_OFF
#define INPUT_LINE AXIS_SOFTRECORDER_SETTINGS__INPUT_1_LINE
#define INPUT_ELECTRET AXIS_SOFTRECORDER_SETTINGS__INPUT_1_ELECTRET
#define INPUT_DYNAMIC AXIS_SOFTRECORDER_SETTINGS__INPUT_1_DYNAMIC

typedef enum
{
    MONO,
    STEREO
} ChannelType;

typedef enum
{
    LINEAR,
    ELECTRIC,
    ELECTRIC_DINAMIC
} SensivityType;

typedef enum
{
    FILTR_OFF,
    FILTR_ON
} FiltrType;

typedef enum
{
    BIAS_OFF,
    BIAS_ON
} BiasType;

typedef enum
{
    SENSIVITY_5,
    SENSIVITY_10,
    SENSIVITY_20,
    SENSIVITY_40
} SensivityVal;

typedef enum
{
    AUDIO_3_6,
    AUDIO_6_3
} ChannelAudioBand;

typedef enum
{
    RING_REC_DISABLED,
    RING_REC_ENABLED
} RingRecordingState;

typedef enum
{
    DISABLED,
    ENABLED
} ChannelState;

typedef struct
{
    uint16_t sensValue;
    uint16_t filtrValue;
    uint16_t biasValue;
    uint16_t sensivityValue;
} ChannelParams;

typedef enum
{    
    MODE,
    CHANNEL,
    INPUT,
    SENS,
    COMPRESSION,
    FILTR,
    AUDIO_BAND,
    RING_RECORDING
} ParameterRow;

typedef enum
{
    COMPRESSION_1_1,
    COMPRESSION_1_3,
    COMPRESSION_1_4,
    COMPRESSION_1_5,
    COMPRESSION_1_8
} ChannelCompression;

typedef struct
{
    ParameterRow currentRow;
    ParameterRow dependencyRow;
    bool hasDependency;
    std::vector<std::vector<std::string>> values;
    std::string name;
    int selectedIndex;
    int dependencyIndex;
    int value;
} ChannelParameter;

#define PARAMETER_ROW_COUNT 8


class RecorderParamsModel
{
public:
    RecorderParamsModel();

    void changeSelectedIndex(int index, ParameterRow parameter);
    void setSelectedIndex(int index, ParameterRow parameter);
    void resetParamSelectedIndex(ParameterRow parameter);
    void clearUpdateParamsList();
    void applySelectedParams();

    void getParametersList(std::list<ParameterRow> &params);
    std::string getBoxName(ParameterRow parameter);
    std::string getTitleName(ParameterRow parameter);
    std::vector<std::string> getComboValues(ParameterRow parameter);
    int getSelectedIndex(ParameterRow parameter);
    std::pair<bool, ParameterRow> getDependency(ParameterRow parameter);
    std::list<ParameterRow> getUpdateParamList();
    int getParamSelectedIndex(ParameterRow parameter);
    bool isUniqueParamForStereo(ParameterRow parameter);
    std::vector<ParameterRow> getEqualStereoParams();
    std::vector<ParameterRow> getDisableParams();
    std::vector<ParameterRow> getDisableRecordParams();
    int getParamValue(ParameterRow parameter);
    bool isEqualParam(ParameterRow parameter);

private:
    void addParameter(std::string name, std::vector<std::vector<std::string>> &values,
                      std::pair<bool, ParameterRow> dependent, ParameterRow type);

    std::map<ParameterRow, ChannelParameter> comboParams;
    std::vector<ParameterRow> equalStereoParams;
    std::list<ParameterRow> listForUpdate;
    std::map<ParameterRow, int> equalParams;
    std::vector<ParameterRow> disableParams;
    std::vector<ParameterRow> disableRecordParams;
    ChannelParams channelParams;
    int hasNewValue;

};

#endif // RECORDERPARAMSMODEL_H
