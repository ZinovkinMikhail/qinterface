#include <assert.h>

#include "parameterspage.h"

ParametersPage::ParametersPage(std::shared_ptr<IQmlEngine> qmlEngine,
                               std::shared_ptr<RecorderParamsModel> paramsModel,
                               std::shared_ptr<IUserManipulators> userManipulators,
                               IRecorderWindow *recorderWindowInterf,
                               QObject *parent) :
    QObject(parent),
    paramsModel(paramsModel),
    engine(qmlEngine),
    currentItem(nullptr),
    userManipulators(userManipulators),
    recorderWindowInterf(recorderWindowInterf)
{
    assert(qmlEngine);
    assert(paramsModel);
    assert(this->parent());
}

void ParametersPage::init(std::map<int, std::shared_ptr<RecorderParamsModel>> channels, std::list<ParameterRow> rows)
{
     currentItem = qobject_cast<QQuickItem*>(engine->createComponent(PARAMETERS_PAGE_QML_PATH, this->parent()));
     assert(currentItem);
     currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

     QQuickItem *rootRow = currentItem->findChild<QQuickItem*>("ParamsRow");
     assert(rootRow);

     try
     {
         for( auto &it: rows )
         {
            QMetaObject::invokeMethod(currentItem, "addTitle", Q_ARG(
                                          QVariant, QString::fromStdString(
                                              channels.at(0)->getTitleName(it))));
         }
     }
     catch(std::exception &except)
     {
         //что-то тип модель плагина сконфигурирована плохо
#warning show error pop up
     }

     for( auto it: channels )
     {
         ParametersColumn *column = new ParametersColumn(engine, it.second,
                                                         userManipulators,shared_from_this(), rootRow);
         columns[it.first] = std::shared_ptr<ParametersColumn>(column);
         column->init(it.first, rows);
     }
}

void ParametersPage::enableChannel(int channel)
{
    columns.at(channel)->enable();
}

void ParametersPage::enableRecordParams()
{
    for( auto &it: columns)
    {
        it.second->enableRecordParams();
    }
}

void ParametersPage::enableRecordParam(ParameterRow row)
{
    for( auto &it: columns)
    {
        it.second->enableRecordParam(row);
    }
}

void ParametersPage::disableChannel(int channel)
{
    columns.at(channel)->disable();
}

void ParametersPage::disableRecordParams()
{
    for( auto &it: columns)
    {
        it.second->disableRecordParams();
    }
}

void ParametersPage::setStereoParam(int index, int channel, ParameterRow row)
{
    columns.at(channel)->setStereoParam(index, row);
}

void ParametersPage::updateParams()
{
    for( auto &it : columns )
    {
        it.second->update();
    }
}

void ParametersPage::setNewData()
{
    for( auto &it : columns )
    {
        it.second->newData();
    }
}

void ParametersPage::resetParams()
{
    for( auto &it : columns )
    {
        it.second->resetParams();
    }
}

void ParametersPage::changeRowParameter(ParameterRow row)
{
    for( auto &it : columns )
    {
        it.second->changeParameter(row);
    }
}

void ParametersPage::valueChanged(int channelNum, ParameterRow row, int index)
{
    recorderWindowInterf->valueChanged(channelNum, row, index);
}

void ParametersPage::disableParameter(int channelNum, ParameterRow row)
{
    columns.at(channelNum)->disableParameter(row);
}
