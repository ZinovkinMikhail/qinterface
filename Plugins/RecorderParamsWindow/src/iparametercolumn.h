#ifndef IPARAMETERBOX_H
#define IPARAMETERBOX_H

#include <memory>
#include "recorderparamsmodel.h"

class IParameterUserChanges
{
public:
    virtual void valueChanged(int channelNum, ParameterRow row, int index) = 0;
};


#endif // IPARAMETERBOX_H
