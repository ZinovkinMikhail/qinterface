#ifndef PARAMETERBOX_H
#define PARAMETERBOX_H

#include <memory>
#include <assert.h>

#include <QObject>
#include <QQuickItem>

#include <iqmlengine.h>
#include <iusermanipulators.h>

#include "recorderparamsmodel.h"
#include "iparametercolumn.h"

#define PARAMETER_BOX_QML_PATH "qrc:/ParameterBox.qml"

class ParameterBox : public QObject
{
    Q_OBJECT
public:
    explicit ParameterBox(std::shared_ptr<IQmlEngine> qmlEngine,
                          std::shared_ptr<RecorderParamsModel> recorderModel,
                          std::shared_ptr<IUserManipulators> userManipulators,
                          std::shared_ptr<IParameterUserChanges> columnInterface,
                          QObject *parent = nullptr);

    void init(ParameterRow rowParameter);
    void dependencyRowChanged(int index);
    void setStereoParamValue(int index);
    void disableParam();
    void enableParam();
    void update();
    void newData();
    void parameterChanged();
    void reset();

private:
    void setBoxName(bool comboDrum);


    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<RecorderParamsModel> recorderModel;
    QQuickItem *currentItem;
    std::shared_ptr<IUserManipulators> userManipulators;
    ParameterRow rowParameter;
    std::shared_ptr<IParameterUserChanges> columnInterface;
    int dependencyIndex;

signals:

public slots:
    void slotShowComboDrum();
};

#endif // PARAMETERBOX_H
