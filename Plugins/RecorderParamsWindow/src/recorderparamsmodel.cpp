#include <assert.h>
#include <iostream>

#include "recorderparamsmodel.h"

RecorderParamsModel::RecorderParamsModel()
{
    std::vector<std::string> mode { "Моно", "Стерео"};
    std::vector<std::vector<std::string>> modeTypes { mode };
    this->addParameter("Режим", modeTypes, std::make_pair(false,MODE), MODE);

    std::vector<std::string> channel { "Выкл", "Вкл"};
    std::vector<std::vector<std::string>> channelTypes { channel };
    this->addParameter("Канал", channelTypes, std::make_pair(false,MODE), CHANNEL);

//    std::vector<std::string> input {"Линейный", "Микрофон", "Микрофон ЭД", "Генератор"};
    std::vector<std::string> input {"Линейный", "Микрофон", "Микрофон ЭД"};
    std::vector<std::vector<std::string>> inputTypes { input };
    this->addParameter("Вход", inputTypes, std::make_pair(true, SENS), INPUT);

    std::vector<std::string> sensInputLinear {"125 мВ.","250 мВ.","500 мВ.", "1000 мВ."};
    std::vector<std::string> sensInputElectric {"5 мВ.", "10 мВ.", "20 мВ.", "40 мВ."};
    std::vector<std::string> sendInputDynamic {"50 мкВ.", "100 мкВ.","200 мкВ.", "400 мкВ."};
    std::vector<std::string> sendGeneratorDynamic {"5 мВ.", "10 мВ.", "25 мВ.", "40 мВ."};
    std::vector<std::vector<std::string>> sensTypes { sensInputLinear, sensInputElectric,
                sendInputDynamic, sendGeneratorDynamic };
    this->addParameter("Чувств.", sensTypes, std::make_pair(false, MODE), SENS);

    std::vector<std::string> channelPack {"1:1", "1:3", "1:4", "1:5", "1:8"};
    std::vector<std::vector<std::string>> packTypes {channelPack};
    this->addParameter("Сжатие", packTypes, std::make_pair(false, MODE), COMPRESSION);

    std::vector<std::string> filtrPack{"Выкл", "Вкл"};
    std::vector<std::vector<std::string>> filtrTypes {filtrPack};
    this->addParameter("Фильтр", filtrTypes, std::make_pair(false, MODE), FILTR);

    std::vector<std::string> audioBandVec{"3.6", "6.3"};
    std::vector<std::vector<std::string>> audioBandTypes {audioBandVec};
    this->addParameter("Полоса частот", audioBandTypes, std::make_pair(false, MODE), AUDIO_BAND);

    std::vector<std::string> ringVec{"Выкл", "Вкл"};
    std::vector<std::vector<std::string>> ringVecTypes {ringVec};
    this->addParameter("Запись по кольцу", ringVecTypes, std::make_pair(false, MODE), RING_RECORDING);

    equalStereoParams.reserve(3);
    equalStereoParams.push_back(MODE);
    equalStereoParams.push_back(INPUT);
    equalStereoParams.push_back(FILTR);

    equalParams[COMPRESSION] = 0;
    equalParams[AUDIO_BAND] = 0;
    equalParams[RING_RECORDING] = 0;

    disableParams.reserve(6);
    disableParams.push_back(MODE);
    disableParams.push_back(SENS);
    disableParams.push_back(FILTR);
    disableParams.push_back(INPUT);
    disableParams.push_back(AUDIO_BAND);
    disableParams.push_back(COMPRESSION);
    disableParams.push_back(RING_RECORDING);

    disableRecordParams.reserve(5);
    disableRecordParams.push_back(AUDIO_BAND);
    disableRecordParams.push_back(MODE);
    disableRecordParams.push_back(CHANNEL);
    disableRecordParams.push_back(COMPRESSION);
    disableRecordParams.push_back(RING_RECORDING);



    assert(comboParams.size() == PARAMETER_ROW_COUNT);

}

void RecorderParamsModel::getParametersList(std::list<ParameterRow> &params)
{
    for( auto &param : comboParams )
    {
        params.push_back(param.first);
    }
}


std::string RecorderParamsModel::getBoxName(ParameterRow parameter)
{
    ChannelParameter *current = &comboParams.at(parameter);

    if( current->hasDependency )
    {
        ChannelParameter *depend = &comboParams.at(current->dependencyRow);
        depend->dependencyIndex = current->selectedIndex;
    }
    return current->values.at(current->dependencyIndex).at(current->selectedIndex);
}

std::string RecorderParamsModel::getTitleName(ParameterRow parameter)
{
    return comboParams.at(parameter).name;
}

std::vector<std::string> RecorderParamsModel::getComboValues(ParameterRow parameter)
{
    ChannelParameter *current = &comboParams[parameter];
    return current->values[current->dependencyIndex];
}

void RecorderParamsModel::changeSelectedIndex(int index, ParameterRow parameter)
{
    ChannelParameter *current = &comboParams.at(parameter);
    if( current->value != index )
    {
        listForUpdate.push_back(parameter);
    }
    else if( current->value == index )
    {
        listForUpdate.remove(parameter);
    }
    current->selectedIndex = index;
}

void RecorderParamsModel::setSelectedIndex(int index, ParameterRow parameter)
{
    comboParams.at(parameter).value = index;
    comboParams.at(parameter).selectedIndex = index;
}

int RecorderParamsModel::getSelectedIndex(ParameterRow parameter)
{
    return comboParams.at(parameter).selectedIndex;
}

std::pair<bool, ParameterRow> RecorderParamsModel::getDependency(ParameterRow parameter)
{
    ChannelParameter *current = &comboParams.at(parameter);
    return std::make_pair(current->hasDependency, current->dependencyRow);
}

std::list<ParameterRow> RecorderParamsModel::getUpdateParamList()
{
    return listForUpdate;
}

int RecorderParamsModel::getParamSelectedIndex(ParameterRow parameter)
{
    return comboParams.at(parameter).selectedIndex;
}

bool RecorderParamsModel::isUniqueParamForStereo(ParameterRow parameter)
{
    if ( std::find(equalStereoParams.begin(), equalStereoParams.end(), parameter) != equalStereoParams.end() )
        return true;
    return false;
}

std::vector<ParameterRow> RecorderParamsModel::getEqualStereoParams()
{
    return equalStereoParams;
}

std::vector<ParameterRow> RecorderParamsModel::getDisableParams()
{
    return disableParams;
}

std::vector<ParameterRow> RecorderParamsModel::getDisableRecordParams()
{
    return disableRecordParams;
}

void RecorderParamsModel::resetParamSelectedIndex(ParameterRow parameter)
{
    comboParams.at(parameter).selectedIndex = comboParams.at(parameter).value;
}

void RecorderParamsModel::clearUpdateParamsList()
{
    listForUpdate.clear();
}

void RecorderParamsModel::applySelectedParams()
{
    for( auto &it: comboParams )
    {
        it.second.value = it.second.selectedIndex;
    }
}

int RecorderParamsModel::getParamValue(ParameterRow parameter)
{
    return comboParams.at(parameter).selectedIndex;
}

bool RecorderParamsModel::isEqualParam(ParameterRow parameter)
{
    auto it = equalParams.find(parameter);
    if( it != equalParams.end() )
        return true;
    return false;
}

void RecorderParamsModel::addParameter(std::string name,
                                       std::vector<std::vector<std::string> > &values, std::pair<bool,
                                       ParameterRow> dependent, ParameterRow type )
{
    ChannelParameter parameter;
    parameter.dependencyRow = dependent.second;
    parameter.hasDependency = dependent.first;
    parameter.currentRow = type;
    parameter.values = values;
    parameter.name = name;
    parameter.selectedIndex = 0;
    parameter.value = 0;
    parameter.dependencyIndex = 0;
    comboParams[type] = parameter; //попробуем std move
}


