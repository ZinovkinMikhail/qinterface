#include <assert.h>

#include "parameterscolumn.h"

ParametersColumn::ParametersColumn(std::shared_ptr<IQmlEngine> qmlEngine,
                             std::shared_ptr<RecorderParamsModel> paramsModel,
                             std::shared_ptr<IUserManipulators> userManipulators,
                             std::shared_ptr<IParameterUserChanges> userChangesInterface,
                             QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    paramsModel(paramsModel),
    currentItem(nullptr),
    userManipulators(userManipulators),
    userChangesInterface(userChangesInterface),
    channelNum(0)
{
    assert(this->qmlEngine);
    assert(paramsModel);
    assert(this->parent());
}

void ParametersColumn::init(int channelNum, std::list<ParameterRow> rows)
{
    this->channelNum = channelNum;
    currentItem = qobject_cast<QQuickItem*>(qmlEngine->createComponent(PARAMETERS_COLUMN_QML_PATH));
    assert(currentItem);
    currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
    QQuickItem *rootRow = currentItem->findChild<QQuickItem*>("ParamsColumn");
    assert(rootRow);

    for(auto &it: rows)
    {
        ParameterBox *box = new ParameterBox(qmlEngine, paramsModel,
                                             userManipulators, shared_from_this(), rootRow);
        parameterBoxes[it] = std::shared_ptr<ParameterBox>(box);
        box->init(it);
    }
}

void ParametersColumn::disable()
{
    for( auto &it : this->paramsModel->getDisableParams() )
    {
        auto findit = parameterBoxes.find(it);
        if(findit != parameterBoxes.end() )
            parameterBoxes[it]->disableParam();
    }
}

void ParametersColumn::disableRecordParams()
{
    for( auto &it : this->paramsModel->getDisableRecordParams() )
    {
        auto findit = parameterBoxes.find(it);
        if(findit != parameterBoxes.end() )
            parameterBoxes[it]->disableParam();
    }
}

void ParametersColumn::enableRecordParams()
{
    for( auto &it : this->paramsModel->getDisableRecordParams() )
    {
        auto findit = parameterBoxes.find(it);
        if(findit != parameterBoxes.end() )
            parameterBoxes[it]->enableParam();
    }
}

void ParametersColumn::enableRecordParam(ParameterRow row)
{
    for( auto &it : this->paramsModel->getDisableRecordParams() )
    {
        auto findit = parameterBoxes.find(it);
        if(findit != parameterBoxes.end() )
        {
            if(it == row)
            {
                parameterBoxes[it]->enableParam();
            }
        }
    }
}

void ParametersColumn::enable()
{
    for( auto &it : this->paramsModel->getDisableParams() )
    {
        auto findit = parameterBoxes.find(it);
        if(findit != parameterBoxes.end() )
            parameterBoxes[it]->enableParam();
    }
}

void ParametersColumn::setStereoParam(int index, ParameterRow row)
{
    if(parameterBoxes.find(row) != parameterBoxes.end() )
        parameterBoxes[row]->setStereoParamValue(index);
}

void ParametersColumn::update()
{
    for( auto &it: parameterBoxes )
    {
        it.second->update();
    }
}

void ParametersColumn::newData()
{
    for( auto &it: parameterBoxes )
    {
        it.second->newData();
    }
}

void ParametersColumn::resetParams()
{
    for( auto &it: parameterBoxes )
    {
        it.second->reset();
    }
}

void ParametersColumn::changeParameter(ParameterRow row)
{
    auto it = parameterBoxes.find(row);
    if( it != parameterBoxes.end() )
        it->second->parameterChanged();

}

void ParametersColumn::valueChanged(int channelNum, ParameterRow row, int index)
{
    Q_UNUSED(channelNum);
    userChangesInterface->valueChanged(this->channelNum, row, index);
    try
    {
         std::pair<bool, ParameterRow> dependency = paramsModel->getDependency(row);
         if( dependency.first )
         {
            parameterBoxes.at(dependency.second)->dependencyRowChanged(index);
         }
    }
    catch(std::exception &except)
    {
#warning show Errro
    }
}

void ParametersColumn::disableParameter(ParameterRow row)
{
    for( auto &it : this->paramsModel->getDisableRecordParams() )
    {
        auto findit = parameterBoxes.find(it);
        if(findit != parameterBoxes.end() )
        {
            if(it == row)
            {
                parameterBoxes[it]->disableParam();
            }
        }
    }
}
