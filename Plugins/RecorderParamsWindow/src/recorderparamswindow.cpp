#include <assert.h>

#include <QQmlComponent>
#include <QQuickItem>

#include "recorderparamswindow.h"

using namespace recorder_status_ctrl;
using namespace rosslog;

RecorderParamsWindow::RecorderParamsWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    recorderStatusCtrl(nullptr),
    recorderSettingsCtrl(nullptr),
    monitoringProcessWidget(nullptr)
{
    memset(&recordSettings, 0, sizeof(SoftRecorderSettingsStruct_t));
    memset(&recordStatus, 0, sizeof(SoftRecorderStatusStruct_t));

    connect(this, SIGNAL(signalSettingsApplyed()),
            this, SLOT(slotSettingsApplyed()));
}

std::shared_ptr<IUserManipulators> RecorderParamsWindow::initWindow(EWindSizeType winSizeType,
                                                                    std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                    std::shared_ptr<IStateWidget> stateWidget,
                                                                    std::shared_ptr<IUserManipulators> userManipulators,
                                                                    std::shared_ptr<IQmlEngine> engine,
                                                                    std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);

    assert(userManipulators);

    dataProvider->getConnectStatus();

    this->engine = engine;
    this->userManipulators = userManipulators;
    this->dataProvider = dataProvider;
    this->logger = log;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent("qrc:/ParametersWindow.qml");
        assert(windowComponent);
        connect(windowComponent, SIGNAL(cancelButtonClicked()),
                this, SLOT(slotCancelButtonClicked()));
        connect(windowComponent, SIGNAL(applyButtonClicked()),
                this, SLOT(slotApplyButtonClicked()));
        connect(windowComponent, SIGNAL(saveButtonClicked()),
                this, SLOT(slotSaveButtonClicked()));
    }

    initRecorderStatusControl();
    initRecorderSettingsControl();

    connect(this, SIGNAL(signalUpdateLocalForms()),
            this, SLOT(slotUpdateLocalForms()));

    RecorderParamsModel *recorderParams = new RecorderParamsModel();

    recorderParamsModel = std::shared_ptr<RecorderParamsModel>(recorderParams);

    fillPages();

    return nullptr;
}

QObject *RecorderParamsWindow::getComponentObject()
{
    return windowComponent;
}

std::string RecorderParamsWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string RecorderParamsWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string RecorderParamsWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string RecorderParamsWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string RecorderParamsWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap RecorderParamsWindow::getIcon()
{
    return QPixmap(":/icons/i24.png");
}

std::pair<bool, QPixmap> RecorderParamsWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/icons/i18.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > RecorderParamsWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool RecorderParamsWindow::showWindow()
{
    updateData();
    return false;
}

bool RecorderParamsWindow::hideWindow()
{
    return false;
}

EWindSizeType RecorderParamsWindow::getWinSizeType()
{
    return OVERSIZE;
}

bool RecorderParamsWindow::prepareToDelete()
{
    return true;
}

void RecorderParamsWindow::valueChanged(int channel, ParameterRow row, int index)
{
    Q_UNUSED(row);
    Q_UNUSED(index);

    std::shared_ptr<RecorderParamsModel> channelModel = channelsMap.at(channel);
    bool hasChannelChanges = false;
    bool mode = static_cast<bool>(channelsMap.at(channel)->getParamSelectedIndex(MODE));

    try
    {
        //Here highlight Channel Title if we have changed params in column by channel
        if( channelsMap.at(channel)->getUpdateParamList().size() > 0 )
        {
            hasChannelChanges = true;
            sethighlightsChannel(channel, true);
        }
        else
            sethighlightsChannel(channel, false);


        if( channelModel->isEqualParam(row) )
        {
            for(auto channelTmp : channelsMap )
            {
                if(channelTmp.second == channelModel )
                    continue;
                channelTmp.second->changeSelectedIndex(index, row);
                for( auto &it: pages)
                {
                    it->changeRowParameter(row);
                }
            }
        }


        //IF we have STEREO MODE FOR EX CHANNELS 1 AND 2 when we change state ON OFF of channel 1
        // We have to change mode of channel 2 to MONO
        if( row == CHANNEL  )
        {
            if( index == DISABLED )
            {
                for( auto &it: pages)
                {
                    if( channel == CHANNEL1_NUM || channel == CHANNEL2_NUM )
                    {
                        it->setStereoParam(MONO, CHANNEL1_NUM, MODE);
                        it->setStereoParam(MONO, CHANNEL2_NUM, MODE);
                    }
                    if( channel == CHANNEL3_NUM || channel == CHANNEL4_NUM )
                    {
                        it->setStereoParam(MONO, CHANNEL3_NUM, MODE);
                        it->setStereoParam(MONO, CHANNEL4_NUM, MODE);
                    }
                    channelsMap.at(channel)->changeSelectedIndex(DISABLED, CHANNEL);
                    it->disableChannel(channel);
                    QMetaObject::invokeMethod(windowComponent, "setChannelMode",
                                              Q_ARG(QVariant, QVariant::fromValue(channel)),
                                              Q_ARG(QVariant, QVariant::fromValue(false)));
                }
            }
            else
            {
                for( auto &it: pages)
                {
                    channelsMap.at(channel)->changeSelectedIndex(ENABLED, CHANNEL);
                    it->enableChannel(channel);
                }
            }
        }


        // Here we change channel's disabled state if in another was selected
        // Stereo mode
        if( mode && row == MODE )
        {
            for( auto &it: pages)
            {
                if( channel == CHANNEL1_NUM )
                {
                    channelsMap.at(CHANNEL2_NUM)->changeSelectedIndex(ENABLED, CHANNEL);
                    it->setStereoParam(1, CHANNEL2_NUM, CHANNEL);
                    it->enableChannel(CHANNEL2_NUM);
                }
                if( channel == CHANNEL2_NUM )
                {
                    channelsMap.at(CHANNEL1_NUM)->changeSelectedIndex(ENABLED, CHANNEL);
                    it->setStereoParam(1, CHANNEL1_NUM, CHANNEL);
                    it->enableChannel(CHANNEL1_NUM);
                }
                if( channel == CHANNEL3_NUM )
                {
                    channelsMap.at(CHANNEL4_NUM)->changeSelectedIndex(ENABLED, CHANNEL);
                    it->setStereoParam(1, CHANNEL4_NUM, CHANNEL);
                    it->enableChannel(CHANNEL4_NUM);
                }
                if( channel == CHANNEL4_NUM )
                {
                    channelsMap.at(CHANNEL3_NUM)->changeSelectedIndex(ENABLED, CHANNEL);
                    it->setStereoParam(1, CHANNEL3_NUM, CHANNEL);
                    it->enableChannel(CHANNEL3_NUM);
                }
            }
        }

        //Here we change state of each value when mode was changed
        if( row == MODE )
        {
            //Here we call qml func for split or inique channel's design
            QMetaObject::invokeMethod(windowComponent, "setChannelMode",
                                      Q_ARG(QVariant, QVariant::fromValue(channel)),
                                      Q_ARG(QVariant, QVariant::fromValue(mode)));

            for( auto &it: channelsMap.at(channel)->getEqualStereoParams() )
            {
                if( channel == CHANNEL1_NUM || channel == CHANNEL2_NUM)
                {
                    changeValuesByMode(CHANNEL1_NUM, it);
                }
                else
                {
                    changeValuesByMode(CHANNEL3_NUM, it);
                }
            }
        }


        //Here changes row(if row is in list of equal rows for stereo )
        // in another channel if Mode is STEREO
        if( mode || row == MODE )
        {
            if( channelsMap.at(channel)->isUniqueParamForStereo(row))
            {
                ParameterRow dependencyRow = channelsMap.at(channel)->getDependency(row).second;

                if( channel == CHANNEL1_NUM  || channel == CHANNEL2_NUM)
                {
                    for( auto it: pages)
                    {
                        it->setStereoParam(index, CHANNEL1_NUM, row);
                        it->setStereoParam(index, CHANNEL2_NUM, row);
                        it->changeRowParameter(dependencyRow);

                    }
                }
                if( channel == CHANNEL3_NUM  || channel == CHANNEL4_NUM)
                {
                    for( auto it: pages)
                    {
                        it->setStereoParam(index, CHANNEL3_NUM, row);
                        it->setStereoParam(index, CHANNEL4_NUM, row);
                        it->changeRowParameter(dependencyRow);
                    }
                }
            }

            if( channel == CHANNEL1_NUM || channel == CHANNEL2_NUM)
            {
                if( hasChannelChanges )
                {
                    sethighlightsChannel(CHANNEL1_NUM, true);
                    sethighlightsChannel(CHANNEL2_NUM, true);
                }
                else
                {
                    sethighlightsChannel(CHANNEL1_NUM, false);
                    sethighlightsChannel(CHANNEL2_NUM, false);
                }
            }
            else
            {
                if( hasChannelChanges )
                {
                    sethighlightsChannel(CHANNEL3_NUM, true);
                    sethighlightsChannel(CHANNEL4_NUM, true);
                }
                else
                {
                    sethighlightsChannel(CHANNEL3_NUM, false);
                    sethighlightsChannel(CHANNEL4_NUM, false);
                }
            }
        }


        //Here we disable channel button if it is last enabled channel
        if(lastEnabledChannelCheck())
        {
            int enabledChannelNum = 0;

            if(channelsMap.at(CHANNEL1_NUM)->getParamValue(CHANNEL))
            {
                enabledChannelNum = CHANNEL1_NUM;
            }
            else if(channelsMap.at(CHANNEL2_NUM)->getParamValue(CHANNEL))
            {
                enabledChannelNum = CHANNEL2_NUM;
            }
            else if(channelsMap.at(CHANNEL3_NUM)->getParamValue(CHANNEL))
            {
                enabledChannelNum = CHANNEL3_NUM;
            }
            else if(channelsMap.at(CHANNEL4_NUM)->getParamValue(CHANNEL))
            {
                enabledChannelNum = CHANNEL4_NUM;
            }

            for( auto &it: pages)
            {
                it->disableParameter(enabledChannelNum, CHANNEL);
            }
        }
        else if(!isRecordingNow())
        {
            for( auto &it: pages)
            {
                it->enableRecordParam(CHANNEL);
            }
        }


        // SENS is a paired parameter. (1 and 2, 3 and 4 channel pairs).
        if( row == SENS  )
        {
            if(channel == CHANNEL1_NUM)
                channelsMap.at(CHANNEL2_NUM)->changeSelectedIndex(index, SENS);

            else if(channel == CHANNEL2_NUM)
                channelsMap.at(CHANNEL1_NUM)->changeSelectedIndex(index, SENS);

            else if(channel == CHANNEL3_NUM)
                channelsMap.at(CHANNEL4_NUM)->changeSelectedIndex(index, SENS);

            else if(channel == CHANNEL4_NUM)
                channelsMap.at(CHANNEL3_NUM)->changeSelectedIndex(index, SENS);

            for( auto &it: pages)
            {
                it->changeRowParameter(SENS);
            }
        }

        // INPUT is a paired parameter for MONO too. (1 and 2, 3 and 4 channel pairs).
        if( !mode && row == INPUT  )
        {
            if( channel == CHANNEL1_NUM  || channel == CHANNEL2_NUM)
            {
                for( auto it: pages)
                {
                    it->setStereoParam(index, CHANNEL1_NUM, INPUT);
                    it->setStereoParam(index, CHANNEL2_NUM, INPUT);
                    it->changeRowParameter(SENS);

                }
            }
            if( channel == CHANNEL3_NUM  || channel == CHANNEL4_NUM)
            {
                for( auto it: pages)
                {
                    it->setStereoParam(index, CHANNEL3_NUM, INPUT);
                    it->setStereoParam(index, CHANNEL4_NUM, INPUT);
                    it->changeRowParameter(SENS);
                }
            }
        }
    }
    catch(std::exception &except)
    {
        this->logger->Message(LOG_ERR ,__LINE__,__func__, except.what());
#warning error popup
    }
}

void RecorderParamsWindow::initRecorderStatusControl()
{
    if( recorderStatusCtrl == nullptr )
    {
        RecorderStatusControl *statCtrl = new RecorderStatusControl(dataProvider, *logger.get());
        recorderStatusCtrl = std::shared_ptr<RecorderStatusControl>(statCtrl);
    }
}

void RecorderParamsWindow::initRecorderSettingsControl()
{
    if( recorderSettingsCtrl == nullptr )
    {
        RecorderSettingsContol *setCtrl = new RecorderSettingsContol(dataProvider, *logger.get());
        recorderSettingsCtrl = std::shared_ptr<RecorderSettingsContol>(setCtrl);
    }
}

void RecorderParamsWindow::sethighlightsChannel(int channel, bool state)
{
    if( state )
        QMetaObject::invokeMethod(windowComponent, "highlightChannel", Q_ARG(
                                      QVariant, QVariant::fromValue(channel)));
    else
        QMetaObject::invokeMethod(windowComponent, "unHihglightChannel", Q_ARG(
                                      QVariant, QVariant::fromValue(channel)));

}

void RecorderParamsWindow::changeValuesByMode(int channel, ParameterRow row)
{
    for( auto &itPage: pages)
    {
        itPage->setStereoParam(channelsMap.at(channel)->getParamSelectedIndex(row), channel, row);
        itPage->setStereoParam(channelsMap.at(channel)->getParamSelectedIndex(row), channel + 1, row);
    }
}

void RecorderParamsWindow::setRecievedData()
{
    std::list<ParameterRow> rows;
    recorderParamsModel->getParametersList(rows);

    std::bitset<16> bits(recordSettings.ChanelCfgMode);

    std::shared_ptr<RecorderParamsModel> channel1 = channelsMap.at(CHANNEL1_NUM);
    std::shared_ptr<RecorderParamsModel> channel2 = channelsMap.at(CHANNEL2_NUM);
    std::shared_ptr<RecorderParamsModel> channel3 = channelsMap.at(CHANNEL3_NUM);
    std::shared_ptr<RecorderParamsModel> channel4 = channelsMap.at(CHANNEL4_NUM);

    for( auto &param : rows )
    {
        switch(param)
        {
            case MODE:
            {
                if( !bits.test(CHANNEL1_STEREO_POS) )
                {
                    channel1->setSelectedIndex(MONO, MODE);
                    channel2->setSelectedIndex(MONO, MODE);
                    valueChanged(CHANNEL1_NUM, MODE, MONO);
                }
                else
                {
                    channel1->setSelectedIndex(STEREO, MODE);
                    channel2->setSelectedIndex(STEREO, MODE);
                    valueChanged(CHANNEL1_NUM, MODE, STEREO);
                }
                channel1->setSelectedIndex(ENABLED, CHANNEL);
                channel2->setSelectedIndex(ENABLED, CHANNEL);

                if( !bits.test(CHANNEL3_STEREO_POS) )
                {
                    channel3->setSelectedIndex(MONO, MODE);
                    channel4->setSelectedIndex(MONO, MODE);
                    valueChanged(CHANNEL3_NUM, MODE, MONO);
                }
                else
                {
                    channel3->setSelectedIndex(STEREO, MODE);
                    channel4->setSelectedIndex(STEREO, MODE);
                    valueChanged(CHANNEL3_NUM, MODE, STEREO);
                }
                break;
            }
            case CHANNEL:
            {
                bool mode = static_cast<bool>(bits.test(CHANNEL1_ENABLE_POS));
                channel1->setSelectedIndex(mode, CHANNEL);
                valueChanged(CHANNEL1_NUM, CHANNEL, mode); // need to update visible part

                mode = static_cast<bool>(bits.test(CHANNEL2_ENABLE_POS));
                valueChanged(CHANNEL2_NUM, CHANNEL, mode);

                mode = static_cast<bool>(bits.test(CHANNEL3_ENABLE_POS));
                valueChanged(CHANNEL3_NUM, CHANNEL, mode);

                mode = static_cast<bool>(bits.test(CHANNEL4_ENABLE_POS));
                valueChanged(CHANNEL4_NUM, CHANNEL, mode);
                break;
            }
            case INPUT:
            {
                channel1->setSelectedIndex(getSensivityType(recordSettings.Chanel1MixCFG),
                                           INPUT);
                channel2->setSelectedIndex(getSensivityType(recordSettings.Chanel2MixCFG),
                                           INPUT);
                channel3->setSelectedIndex(getSensivityType(recordSettings.Chanel3MixCFG),
                                           INPUT);
                channel4->setSelectedIndex(getSensivityType(recordSettings.Chanel4MixCFG),
                                           INPUT);
#warning check generator flag BIAS
                break;
            }
            case SENS:
            {
            //params3.sensVal = static_cast<SensivityVal>(recordSettings.Chanel3Sens);
                channel1->setSelectedIndex(static_cast<int>(recordSettings.Chanel1Sens), SENS);
                channel2->setSelectedIndex(static_cast<int>(recordSettings.Chanel2Sens), SENS);
                channel3->setSelectedIndex(static_cast<int>(recordSettings.Chanel3Sens), SENS);
                channel4->setSelectedIndex(static_cast<int>(recordSettings.Chanel4Sens), SENS);

                break;
            }
            case FILTR:
            {
                std::bitset<16> filtr(recordSettings.ChanelAGCLPFMode);
                channel1->setSelectedIndex(static_cast<int>(filtr.test(CHANNEL_1_LPF_ENABLE)), FILTR);
                channel2->setSelectedIndex(static_cast<int>(filtr.test(CHANNEL_2_LPF_ENABLE)), FILTR);
                channel3->setSelectedIndex(static_cast<int>(filtr.test(CHANNEL_3_LPF_ENABLE)), FILTR);
                channel4->setSelectedIndex(static_cast<int>(filtr.test(CHANNEL_4_LPF_ENABLE)), FILTR);
                break;
            }
            case COMPRESSION:
            {
                channel1->setSelectedIndex(getCompressionType(recordSettings.Chanel1AudioCommpression), COMPRESSION);
                channel2->setSelectedIndex(getCompressionType(recordSettings.Chanel2AudioCommpression), COMPRESSION);
                channel3->setSelectedIndex(getCompressionType(recordSettings.Chanel3AudioCommpression), COMPRESSION);
                channel4->setSelectedIndex(getCompressionType(recordSettings.Chanel4AudioCommpression), COMPRESSION);
                break;
            }
            case AUDIO_BAND:
            {
                channel1->setSelectedIndex(getAudiBandType(recordSettings.Chanel1AudioBand), AUDIO_BAND);
                channel2->setSelectedIndex(getAudiBandType(recordSettings.Chanel2AudioBand), AUDIO_BAND);
                channel3->setSelectedIndex(getAudiBandType(recordSettings.Chanel3AudioBand), AUDIO_BAND);
                channel4->setSelectedIndex(getAudiBandType(recordSettings.Chanel4AudioBand), AUDIO_BAND);
                break;
            }
            case RING_RECORDING:
            {
                RingRecordingState state;
                if( recordSettings.FlashMode & AXIS_SOFTRECORDER_SETTINGS__FLASH_LOOP_RECORD_ON )
                    state = RING_REC_ENABLED;
                else
                    state = RING_REC_DISABLED;

                channel1->setSelectedIndex(state, RING_RECORDING);
                channel2->setSelectedIndex(state, RING_RECORDING);
                channel3->setSelectedIndex(state, RING_RECORDING);
                channel4->setSelectedIndex(state, RING_RECORDING);
            }
            default:
                break;
        }
    }

    sethighlightsChannel(CHANNEL1_NUM, false);
    sethighlightsChannel(CHANNEL2_NUM, false);
    sethighlightsChannel(CHANNEL3_NUM, false);
    sethighlightsChannel(CHANNEL4_NUM, false);


    if( isRecordingNow() || isAudioPlayNow() || isMonitoringNow())
    {
        for( auto &it: pages)
        {
            it->disableRecordParams();
            it->setNewData();
        }
    }
    else
    {
        for( auto &it: pages)
        {
            it->setNewData();
//#warning add check that was enabled
            if(!lastEnabledChannelCheck())
                it->enableRecordParam(CHANNEL);
        }
    }
}

void RecorderParamsWindow::updateData()
{
    updateWorker = std::async(std::launch::async,
                              &RecorderParamsWindow::getInfoFromAdapter, this);
}

void RecorderParamsWindow::getInfoFromAdapter()
{
    this->userManipulators->showLoadingWidget();
    try
    {
        recorderSettingsCtrl->updateStatus();
        recorderStatusCtrl->updateStatus();
        this->recordSettings = recorderSettingsCtrl->getRecordSettings();
        this->recordStatus = recorderStatusCtrl->getRecorderStatus();
        emit signalUpdateLocalForms();
        this->userManipulators->hideLoadingWidget();
    }
    catch( std::system_error &systemErr )
    {
        this->userManipulators->hideLoadingWidget();
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        userManipulators->showMessage(std::string("Не удалось получить данные.\nКод ошибки: ").append(
                                          std::to_string(systemErr.code().value())));
    }
}

ChannelCompression RecorderParamsWindow::getCompressionType(uint16_t value)
{
    std::bitset<16> bits(value);
    if( value == CHANNEL_COMPRESSION_1_1 )
        return  COMPRESSION_1_1;
    else if( value == CHANNEL_COMPRESSION_1_3 )
        return  COMPRESSION_1_3;
    else if( value == CHANNEL_COMPRESSION_1_4)
        return  COMPRESSION_1_4;
    else if( value == CHANNEL_COMPRESSION_1_5 )
        return  COMPRESSION_1_5;
    else if( value == CHANNEL_COMPRESSION_1_8 )
        return  COMPRESSION_1_8;
    return COMPRESSION_1_1;
}

SensivityType RecorderParamsWindow::getSensivityType(uint16_t value)
{
    switch( value & INPUT_CONFIG )
    {
        case INPUT_OFF:
        {
            break;
        }
        case INPUT_LINE:
        {
            return LINEAR;
        }
        case INPUT_ELECTRET:
        {
            return ELECTRIC;
        }
        case INPUT_DYNAMIC:
        {
            return ELECTRIC_DINAMIC;
        }
        default:
            break;
    }
    return LINEAR;
}

void RecorderParamsWindow::setCompressionPosition(uint16_t &compression, const ChannelCompression type)
{
    switch (type)
    {
        case COMPRESSION_1_1:
        {
            compression = CHANNEL_COMPRESSION_1_1;
            break;
        }
        case COMPRESSION_1_3:
        {
            compression = CHANNEL_COMPRESSION_1_3;
            break;
        }
        case COMPRESSION_1_4:
        {
            compression = CHANNEL_COMPRESSION_1_4;
            break;
        }
        case COMPRESSION_1_5:
        {
            compression = CHANNEL_COMPRESSION_1_5;
            break;
        }
        case COMPRESSION_1_8:
        {
            compression = CHANNEL_COMPRESSION_1_8;
            break;
        }
    }
}

void RecorderParamsWindow::setSensivityValue(uint16_t &input, SensivityType type)
{
    switch (type)
    {
        case LINEAR:
        {
            input &= ~INPUT_CONFIG;
            input |= INPUT_LINE;
            break;
        }
        case ELECTRIC_DINAMIC:
        {
            input &= ~INPUT_CONFIG;
            input |= INPUT_DYNAMIC;
            break;
        }
        case ELECTRIC:
        {
            input &= ~INPUT_CONFIG;
            input |= INPUT_ELECTRET;
            break;
        }
    }
}

void RecorderParamsWindow::slotCancelButtonClicked()
{
    this->updateData();
}

void RecorderParamsWindow::slotApplyButtonClicked()
{
    if( packDataForSend() )
    {
        userManipulators->showLoadingWidget();

        updateWorker = std::async(std::launch::async,
                                  std::bind(&RecorderParamsWindow::applyDataOnAdapter,
                                            this,  std::placeholders::_1), recordSettings);
        updateWorker.wait_for(std::chrono::seconds(0));

    }
}

void RecorderParamsWindow::slotSaveButtonClicked()
{
    if( packDataForSend() )
    {
        userManipulators->showLoadingWidget();

        updateWorker = std::async(std::launch::async,
                                  std::bind(&RecorderParamsWindow::saveDataOnAdapter,
                                            this,  std::placeholders::_1), recordSettings);
        updateWorker.wait_for(std::chrono::seconds(0));

    }
}

void RecorderParamsWindow::saveDataOnAdapter(SoftRecorderSettingsStruct_t data)
{
    try
    {
        recorderSettingsCtrl->saveRecordSettings(data);
        emit signalSettingsApplyed();
    }
    catch( std::system_error &except )
    {
        userManipulators->showMessage(std::string("Не удалось сохранить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

void RecorderParamsWindow::applyDataOnAdapter(SoftRecorderSettingsStruct_t data)
{
    try
    {
        recorderSettingsCtrl->applyRecordSettings(data);
        emit signalSettingsApplyed();
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
        userManipulators->showMessage(std::string("Не удалось применить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

ChannelAudioBand RecorderParamsWindow::getAudiBandType(const uint16_t &value)
{
    if(value == AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_3_6)
        return AUDIO_3_6;
    else if( value == AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_6_3)
        return AUDIO_6_3;
#warning add error
    return AUDIO_3_6;
}

bool RecorderParamsWindow::packDataForSend()
{
    try
    {
        std::shared_ptr<RecorderParamsModel> channel1 = channelsMap.at(CHANNEL1_NUM);
        std::shared_ptr<RecorderParamsModel> channel2 = channelsMap.at(CHANNEL2_NUM);
        std::shared_ptr<RecorderParamsModel> channel3 = channelsMap.at(CHANNEL3_NUM);
        std::shared_ptr<RecorderParamsModel> channel4 = channelsMap.at(CHANNEL4_NUM);

        std::list<ParameterRow> params;
        channel1->getParametersList(params);
        std::bitset<16> bits(recordSettings.ChanelCfgMode);

        for( auto &param : params )
        {
            switch (param) {
                case CHANNEL: {
                    bits.set(CHANNEL1_ENABLE_POS, channel1->getParamValue(param));
                    bits.set(CHANNEL2_ENABLE_POS, channel2->getParamValue(param));
                    bits.set(CHANNEL3_ENABLE_POS, channel3->getParamValue(param));
                    bits.set(CHANNEL4_ENABLE_POS, channel4->getParamValue(param));
                    break;
                }
                case MODE: {
                    bits.set(CHANNEL1_STEREO_POS, channel1->getParamValue(param));
                    bits.set(CHANNEL1_STEREO_POS, channel2->getParamValue(param));
                    bits.set(CHANNEL3_STEREO_POS, channel3->getParamValue(param));
                    bits.set(CHANNEL3_STEREO_POS, channel4->getParamValue(param));
                    break;
                }
                case INPUT: {
                    setSensivityValue(recordSettings.Chanel1MixCFG,
                                      static_cast<SensivityType>(channel1->getParamValue(param)));
                    setSensivityValue(recordSettings.Chanel2MixCFG,
                                      static_cast<SensivityType>(channel2->getParamValue(param)));
                    setSensivityValue(recordSettings.Chanel3MixCFG,
                                      static_cast<SensivityType>(channel3->getParamValue(param)));
                    setSensivityValue(recordSettings.Chanel4MixCFG,
                                      static_cast<SensivityType>(channel4->getParamValue(param)));
                    break;
                }
                case COMPRESSION: {
                    setCompressionPosition(recordSettings.Chanel1AudioCommpression,
                                           static_cast<ChannelCompression>(channel1->getParamValue(param)));
                    setCompressionPosition(recordSettings.Chanel2AudioCommpression,
                                           static_cast<ChannelCompression>(channel2->getParamValue(param)));
                    setCompressionPosition(recordSettings.Chanel3AudioCommpression,
                                           static_cast<ChannelCompression>(channel3->getParamValue(param)));
                    setCompressionPosition(recordSettings.Chanel4AudioCommpression,
                                           static_cast<ChannelCompression>(channel4->getParamValue(param)));
                    break;
                }
                case FILTR: {
                    std::bitset<16> filtr(recordSettings.ChanelAGCLPFMode);
                    filtr.set(CHANNEL_1_LPF_ENABLE, channel1->getParamValue(param));
                    filtr.set(CHANNEL_2_LPF_ENABLE, channel2->getParamValue(param));
                    filtr.set(CHANNEL_3_LPF_ENABLE, channel3->getParamValue(param));
                    filtr.set(CHANNEL_4_LPF_ENABLE, channel4->getParamValue(param));

                    recordSettings.ChanelAGCLPFMode = filtr.to_ulong();
                    break;
                }
                case SENS: {
                    recordSettings.Chanel1Sens = static_cast<uint16_t>(channel1->getParamValue(param));
                    recordSettings.Chanel2Sens = static_cast<uint16_t>(channel2->getParamValue(param));
                    recordSettings.Chanel3Sens = static_cast<uint16_t>(channel3->getParamValue(param));
                    recordSettings.Chanel4Sens = static_cast<uint16_t>(channel4->getParamValue(param));
                    break;
                }
                case AUDIO_BAND:
                {
                    if (channel1->getParamValue(param) == AUDIO_3_6)
                    {
                        recordSettings.Chanel1AudioBand = AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_3_6;
                        recordSettings.Chanel2AudioBand = AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_3_6;
                        recordSettings.Chanel3AudioBand = AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_3_6;
                        recordSettings.Chanel4AudioBand = AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_3_6;
                    } else if (channel1->getParamValue(param) == AUDIO_6_3)
                    {
                        recordSettings.Chanel1AudioBand = AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_6_3;
                        recordSettings.Chanel2AudioBand = AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_6_3;
                        recordSettings.Chanel3AudioBand = AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_6_3;
                        recordSettings.Chanel4AudioBand = AXIS_SOFTRECORDER_SETTINGS__AUDIO_BAND_6_3;
                    }
                    break;
                }
                case RING_RECORDING:
                {
                    if( channel1->getParamValue(param) )
                        recordSettings.FlashMode |= AXIS_SOFTRECORDER_SETTINGS__FLASH_LOOP_RECORD_ON;
                    else
                        recordSettings.FlashMode &=~AXIS_SOFTRECORDER_SETTINGS__FLASH_LOOP_RECORD_ON;
                    break;
                }
                default:
                    break;
            }

        }
        recordSettings.Time = 0;
        recordSettings.ChanelCfgMode = bits.to_ulong();
    }
    catch (std::exception &exception)
    {
#warning make error popup
        return false;
    }
    return true;
}


void RecorderParamsWindow::slotUpdateLocalForms()
{
    setRecievedData();
}

void RecorderParamsWindow::slotSettingsApplyed()
{
    std::shared_ptr<RecorderParamsModel> channel1 = channelsMap.at(CHANNEL1_NUM);
    std::shared_ptr<RecorderParamsModel> channel2 = channelsMap.at(CHANNEL2_NUM);
    std::shared_ptr<RecorderParamsModel> channel3 = channelsMap.at(CHANNEL3_NUM);
    std::shared_ptr<RecorderParamsModel> channel4 = channelsMap.at(CHANNEL4_NUM);
    channel1->applySelectedParams();
    channel2->applySelectedParams();
    channel3->applySelectedParams();
    channel4->applySelectedParams();

    for( auto &it: pages )
    {
        it->resetParams();
    }

    sethighlightsChannel(CHANNEL1_NUM, false);
    sethighlightsChannel(CHANNEL2_NUM, false);
    sethighlightsChannel(CHANNEL3_NUM, false);
    sethighlightsChannel(CHANNEL4_NUM, false);

    updateData();
}


void RecorderParamsWindow::fillPages()
{
    std::list<ParameterRow> rows;
    recorderParamsModel->getParametersList(rows);
    std::list<ParameterRow>::iterator it = rows.begin();

    std::shared_ptr<RecorderParamsModel> channel1 = std::shared_ptr<RecorderParamsModel>(new RecorderParamsModel);
    std::shared_ptr<RecorderParamsModel> channel2 = std::shared_ptr<RecorderParamsModel>(new RecorderParamsModel);
    std::shared_ptr<RecorderParamsModel> channel3 = std::shared_ptr<RecorderParamsModel>(new RecorderParamsModel);
    std::shared_ptr<RecorderParamsModel> channel4 = std::shared_ptr<RecorderParamsModel>(new RecorderParamsModel);

    channelsMap[CHANNEL1_NUM] = channel1;
    channelsMap[CHANNEL2_NUM] = channel2;
    channelsMap[CHANNEL3_NUM] = channel3;
    channelsMap[CHANNEL4_NUM] = channel4;

    int pageCount = ceil(rows.size() / float(PARAMS_PER_PAGE));
    QQuickItem *root = windowComponent->findChild<QQuickItem*>("SwipeView");
    pages.reserve(pageCount+1);

    for( int i = 1; i <= pageCount; i++ )
    {
        it = rows.begin();
        int result = PARAMS_PER_PAGE;
        if( rows.size() < PARAMS_PER_PAGE )
            result = rows.size();
        std::advance(it, result);

        std::list<ParameterRow> currentRows;
        currentRows.splice(currentRows.begin(), rows, rows.begin(), it);
        ParametersPage *page = new ParametersPage(engine, recorderParamsModel,
                                                  userManipulators, this, root);
        std::shared_ptr<ParametersPage> pagePtr = std::shared_ptr<ParametersPage>(page);
        pages.push_back(pagePtr);
        page->init(channelsMap, currentRows);
    }
}

bool RecorderParamsWindow::lastEnabledChannelCheck()
{
    int enabledChannelCount = 0;
    enabledChannelCount += channelsMap.at(CHANNEL1_NUM)->getParamValue(CHANNEL);
    enabledChannelCount += channelsMap.at(CHANNEL2_NUM)->getParamValue(CHANNEL);
    enabledChannelCount += channelsMap.at(CHANNEL3_NUM)->getParamValue(CHANNEL);
    enabledChannelCount += channelsMap.at(CHANNEL4_NUM)->getParamValue(CHANNEL);

    if(enabledChannelCount == 1)
    {
        return true;
    }

    return false;
}

bool RecorderParamsWindow::isRecordingNow()
{
    if( recordStatus.Chanel1Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
            recordStatus.Chanel2Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
            recordStatus.Chanel3Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
            recordStatus.Chanel4Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool RecorderParamsWindow::isMonitoringNow()
{
    if( recordStatus.Chanel1Status & AXIS_SOFTRECORDER_STATUS__PLAY_NOW ||
            recordStatus.Chanel2Status & AXIS_SOFTRECORDER_STATUS__PLAY_NOW ||
            recordStatus.Chanel3Status & AXIS_SOFTRECORDER_STATUS__PLAY_NOW ||
            recordStatus.Chanel4Status & AXIS_SOFTRECORDER_STATUS__PLAY_NOW )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool RecorderParamsWindow::isAudioPlayNow()
{
    if( recordStatus.PlayStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__STOP )
    {
        return false;
    }
    else
    {
        return true;
    }
}

RecorderParamsWindow::~RecorderParamsWindow()
{
    std::cerr << "Removed RecoderParamsWindow" << std::endl;
}
