#include <iostream>

#include "parameterbox.h"

ParameterBox::ParameterBox(std::shared_ptr<IQmlEngine> qmlEngine,
                           std::shared_ptr<RecorderParamsModel> recorderModel,
                           std::shared_ptr<IUserManipulators> userManipulators,
                           std::shared_ptr<IParameterUserChanges> columnInterface,
                           QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    recorderModel(recorderModel),
    currentItem(nullptr),
    userManipulators(userManipulators),
    columnInterface(columnInterface)
{
    assert(this->qmlEngine);
    assert(this->recorderModel);
    assert(this->parent());
}

void ParameterBox::init(ParameterRow rowParameter)
{
    this->rowParameter = rowParameter;
    currentItem = qobject_cast<QQuickItem*>(qmlEngine->createComponent(
                                                PARAMETER_BOX_QML_PATH, this->parent()));
    assert(currentItem);
    currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
    connect(currentItem, SIGNAL(showComboDrum()), this, SLOT(slotShowComboDrum()));
    this->setBoxName(false);
}

void ParameterBox::dependencyRowChanged(int index)
{
    Q_UNUSED(index);
    this->setBoxName(true);
}

void ParameterBox::setStereoParamValue(int index)
{
    recorderModel->changeSelectedIndex(index, rowParameter);
    QMetaObject::invokeMethod(currentItem, "setBoxNameStereo",
                              Q_ARG(QVariant, QString::fromStdString(
                                        recorderModel->getBoxName(rowParameter))),
                              Q_ARG(QVariant, recorderModel->getSelectedIndex(rowParameter)));
}

void ParameterBox::disableParam()
{
    this->setBoxName(true);
    QMetaObject::invokeMethod(currentItem, "disable");
}

void ParameterBox::enableParam()
{
    this->setBoxName(true);
    QMetaObject::invokeMethod(currentItem, "enable");
}

#warning delete function
void ParameterBox::update()
{
    this->setBoxName(false);
}

void ParameterBox::newData()
{
    this->setBoxName(false);
}

void ParameterBox::parameterChanged()
{
    this->setBoxName(true);
}

void ParameterBox::reset()
{
    recorderModel->resetParamSelectedIndex(rowParameter);
    setBoxName(false);
//    this->columnInterface->valueChanged(0, rowParameter,
//                                              recorderModel->getSelectedIndex(rowParameter));
}

void ParameterBox::setBoxName(bool comboDrum)
{
    try
    {
        QMetaObject::invokeMethod(currentItem, "setBoxName",
                                  Q_ARG(QVariant, QString::fromStdString(
                                            recorderModel->getBoxName(rowParameter))),
                                  Q_ARG(QVariant, comboDrum),
                                  Q_ARG(QVariant, recorderModel->getSelectedIndex(rowParameter)));
    }
    catch(std::exception &except)
    {
#warning  error pop up
        std::cerr << except.what() << std::endl;
    }
}

void ParameterBox::slotShowComboDrum()
{
    try
    {
        std::pair<bool, int> res = this->userManipulators->openComboDrum(recorderModel->getSelectedIndex(rowParameter),
                    recorderModel->getComboValues(rowParameter));
        if( res.first )
        {
            recorderModel->changeSelectedIndex(res.second, rowParameter);
            this->setBoxName(true);
            this->columnInterface->valueChanged(0, rowParameter, res.second);
        }
    }
    catch(std::exception &except)
    {
#warning  error pop up
        std::cerr << except.what() << std::endl;
    }
}
