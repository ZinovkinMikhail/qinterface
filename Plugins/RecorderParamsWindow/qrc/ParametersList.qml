import QtQuick 2.9
import Cicada 1.0


Item
{
    x: 0
    y: 0
    width: width
    height: height

    Rectangle
    {
        width: parent.width
        height: parent.height
        color: "transparent"
    }

    function addTitle(name)
    {
        var component = Qt.createComponent( "ParameterTitle.qml");
        var object = component.createObject(titleColumn);
        object.title = name
    }

    Column
    {
        id: titleColumn
        y: 0
        spacing: 25
        x: 10
        width: 100
        height:  parent.height

        Repeater
        {
            model: 2
        }
    }

    Row
    {
        x: 180
        y: 0
        spacing: 73
        width: parent.width - x
        height: parent.height
        objectName: "ParamsRow"
    }

}
