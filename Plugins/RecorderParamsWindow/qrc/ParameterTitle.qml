import QtQuick 2.0
import Cicada 1.0

Item
{
    height: 52
    width: 100
    x: 0
    y: 0

    property string title: ""

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    Text
    {
        y: parent.height/2 - height/2        
        width: parent.width + 14
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: 12
        horizontalAlignment: Text.AlignHCenter
        font.family: fontConf.mainFont
        font.italic: true
        font.pixelSize: fontConf.pixSize_S7
        fontSizeMode: Text.HorizontalFit
        minimumPixelSize: 6
        color: cc.light_green
        text: title + ":"
    }
}
