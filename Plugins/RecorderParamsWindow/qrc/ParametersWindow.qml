import QtQuick 2.9
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0
import Cicada 1.0


Item
{
    id: parametersWindow
    width: 1280
    height: 620


    property alias controlListButton: controlListButton

    signal cancelButtonClicked()
    signal applyButtonClicked()
    signal saveButtonClicked()

    function setChannelMode(channelNum, mode)
    {
        if(channelNum === 0 || channelNum === 1 )
        {
            modeRect.setChannel1Mode(mode)
        }
        if(channelNum === 2 || channelNum === 3 )
        {
            modeRect.setChannel3Mode(mode)
        }
    }

    function highlightChannel(num)
    {
        switch(num)
        {
            case 0:
                channel1.color = "#eec070"
                break
            case 1:
                channel2.color = "#eec070"
                break
            case 2:
                channel3.color = "#eec070"
                break
            case 3:
                channel4.color = "#eec070"
                break
        }
    }
    function unHihglightChannel(num)
    {
        switch(num)
        {
            case 0:
                channel1.color = cc.light_green
                break
            case 1:
                channel2.color = cc.light_green
                break
            case 2:
                channel3.color = cc.light_green
                break
            case 3:
                channel4.color = cc.light_green
                break

        }
    }


    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    BackgroundTitle
    {
        id: backgroundTitle
        y: 560
        text: qsTr("Параметры")
    }


    Rectangle
    {
        x: 144
        y: 0
        height: 146
        width: 1121
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}

        Rectangle //Shadow little patch
        {
            x: parent.width
            y: parent.height - 14
            height: 24
            width: 6
            radius: 3
            color: cc.dark_blue
        }

        Rectangle //Shadow big patch
        {
            x:-8
            z:-4
            width: parent.width+16
            height: parent.height
            color: cc.dark_blue
        }

        Button //antipress for top panel hidden buttons
        {
            anchors.fill: parent
            background: Rectangle { color: "transparent" }
        }
    }


    Rectangle
    {
        x:15
        y: 126
        height:395
        width: parent.width - x - 15
        radius: 3
        color: cc.blue
        opacity: 1

        ShadowRect{type: "panel"}
    }

    Rectangle //Little corner
    {
        x: 139
        y: 123
        height: 20
        width: height
        rotation: 45
        color: cc.blue
    }


    Rectangle
    {
        id: modeRect
        x: 154
        y: 10

        width: 1092
        height: 500
        color: cc.dark_blue

        function setChannel1Mode(mode)
        {
            if(mode)
            {
                channel1Separator.visible = false
            }
            else
            {
                channel1Separator.visible = true
            }
        }

        function setChannel3Mode(mode)
        {
            if(mode)
            {
                channel3Separator.visible = false
            }
            else
            {
                channel3Separator.visible = true
            }
        }

        Rectangle //Line
        {
            x: 10
            y: 80
            width: parent.width - 20
            height: 5
            color: cc.blue
        }

        Rectangle //Line
        {
            id: channel1Separator
            x: 273
            width: 5
            height: parent.height
            color: cc.blue
        }
        Rectangle //Line
        {
            x: 273 + 273
            width: 5
            height: parent.height
            color: cc.blue
        }
        Rectangle //Line
        {
            id: channel3Separator
            x: 273 + 273 + 273
            width: 5
            height: parent.height
            color: cc.blue
        }
    }



    Text
    {
        id: channel1
        x: modeRect.x + 136 - width/2
        y: modeRect.y  + 40 - height/2
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S4
        color: cc.light_green
        text: "Канал 1"
    }
    Text
    {
        id: channel2
        x: 154 + 136 + 273 - width/2
        y: modeRect.y  + 40 - height/2
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S4
        color: cc.light_green
        text: "Канал 2"
    }
    Text
    {
        id: channel3
        x: 154 + 136 + 273*2 - width/2
        y: modeRect.y  + 40 - height/2
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S4
        color: cc.light_green
        text: "Канал 3"
    }
    Text
    {
        id: channel4
        x: 154 + 136 + 273*3 - width/2
        y: modeRect.y  + 40 - height/2
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S4
        color: cc.light_green
        text: "Канал 4"
    }

    ControlListButton
    {
        id: cancelButton
        x: view.x + view.width - width
        y: view.y + view.height + 20
        type: "SlimType"
        text: qsTr("Отменить")
        onClicked:
        {
            cancelButtonClicked()
        }
    }

    ControlListButton
    {
        id: controlListButton
        x: cancelButton.x - cancelButton.width - 15
        y: view.y + view.height + 20
        type: "SlimType"
        text: qsTr("Применить")
        z: 2
        onClicked:
        {
            applyButtonClicked()
        }
    }

    ControlListButton
    {
        x: controlListButton.x - controlListButton.width - 15
        y: view.y + view.height + 20
        type: "SlimType"
        text: qsTr("Сохранить")
        z:2
        onClicked:
        {
            saveButtonClicked()
        }
    }

    SwipeView
    {
        id: view
        objectName: "SwipeView"
        orientation: Qt.Vertical
        currentIndex: 0
        x: 8
        y: 127
        width: 1231
        height: 383
        clip: true
    }

    Item
    {
        x: 1265
        y: 300 - indicator.width/2
        rotation: 90

        PageIndicator
        {
            id: indicator
            count: view.count
            currentIndex: view.currentIndex

            delegate:
                Rectangle
                {
                    implicitWidth: 10
                    implicitHeight: 10
                    radius: width / 2
                    border.color: cc.white
                    border.width: 1

                    color: index === indicator.currentIndex ? cc.blue : cc.white

                    Behavior on color
                    {
                        ColorAnimation
                        {
                            from: cc.white
                            duration: 200
                        }
                    }
                }
        }
    }
}
