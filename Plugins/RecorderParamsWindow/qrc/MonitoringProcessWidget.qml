import QtQuick 2.0

Item
{
    id: root
    property var itemClass
    height: 110
    width: 614

    function swipeSelected()
    {
        console.debug("Swipe on process")
        itemClass.setActiveWindow()
    }

    Rectangle
    {
        anchors.fill: parent
        color: "yellow"

        Text
        {
            id: name
            anchors.centerIn: parent
            text: qsTr("Plugin process widget")
        }
    }
}
