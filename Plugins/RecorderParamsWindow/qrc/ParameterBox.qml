import QtQuick 2.0
import Cicada 1.0

Item
{
    id: root
    width:  220;
    height:  52

    signal showComboDrum();

    property int prevName: 0
    property int tmpName: 0

    function disable()
    {
        root.enabled = false
        root.opacity = 0.3
    }

    function enable()
    {
        root.enabled = true
        root.opacity = 1
    }

    function setBoxNameStereo(name, index)
    {
        box.txttxt = name
        tmpName = index
        if( tmpName != prevName )
        {
            box.setState(true)
        }
        if( tmpName === prevName )
        {
            box.setState(false)
        }
    }

    function setBoxName(name, isComboDrum, index)
    {
        box.txttxt = name

        if( isComboDrum )
        {
            tmpName = index
            if( tmpName != prevName )
            {
                box.setState(true)
            }
        }
        else
        {
            prevName = index
            box.setState(false)
        }

        if( tmpName === prevName )
        {
            box.setState(false)
        }
    }

    ButtonDrop
    {
        id: box
        txttxt: "test"//listNumber == 1 ? pField[columnIndex + 1][index] : pField[columnIndex + 1][index + list_1_size]
        type: "default"

        state:
        {
            return ""
        }
        onButtonClick: root.showComboDrum();
    }
}
