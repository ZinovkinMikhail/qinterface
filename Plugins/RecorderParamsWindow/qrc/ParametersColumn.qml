import QtQuick 2.9
import Cicada 1.0

Item
{
    width: 200
    height: parent ?  parent.height : 0

    function disable()
    {
        column.enabled = false
        column.opacity = 0.3
    }
    function enable()
    {
        column.enabled = true
        column.opacity = 1.0
    }

    Column
    {
        id: column
        width: parent.width
        height: parent.height
        spacing: 25
        objectName: "ParamsColumn"
        opacity: 1
    }
}
