#ifndef SETTINGSPARAMSWINDOW_H
#define SETTINGSPARAMSWINDOW_H

#include <future>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlProperty>
#include <QTextCodec>

#include <axis_ifconfig.h>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/adaptersettingscontrol.h>
#include <dataprovider/adapterstatuscontrol.h>

#define WINDOW_IDENTIFICATOR "ConnectionSettings"
#define WINDOW_FOLDER_NAME "Adapter"
#define ICON_PRESENTATION_NAME "Подключение"
#define FOLDER_PRESENTATION_NAME "Адаптер"

typedef enum
{
    CLIENT,
    SERVER
} ConnectionType;

#define CONNECTION_SETTINGS_WINDOW_QML_PATH "qrc:/ConnectionSettings/ConnectionSettingsWindow.qml"

class ConnectionSettingsWindow : public QObject, public IEmptyPlaceWindow
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ConnectionSettingsWindow")
public:
    explicit ConnectionSettingsWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~ConnectionSettingsWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;

private:
    void initAdapterSettingsControl();
    void getInfoFromAdapter();
    void changeDesignByConnectionType(const ConnectionType &connectionType);
    ConnectionType getConnectionType(const uint32_t &adapterSettingsCon);
    std::pair<bool, uint32_t> getValidIp(QString ip);
    void updateDataOnAdapter();
    void saveDataOnAdapter();
    void updateData();
    bool packNetworkParams(SoftAdapterSettingsStruct_t &settings);

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    SoftAdapterSettingsStruct_t adapterSettings;
    std::shared_ptr<AdapterSettingsControl> adapterSettingsControl;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::vector<std::string> comboDrumModel;
    std::future<void> updateWorker;
    std::mutex dataMutex;
    ConnectionType currentConnType;

private:
    QQuickItem *grid;
    QQuickItem *connectionType;
    QQuickItem *ipAddressItem;
    QQuickItem *loginItem;
    QQuickItem *passwordItem;
    QQuickItem *portSSLItem;
    QQuickItem *portHTTPItem;
    QQuickItem *portUDPItem;





signals:
    void signalRecivedAdapterData();
private slots:
    void slotRecievedAdapterData();
    void slotShowComboDrum();
    void slotApplyParams();
    void slotSaveParams();
    void slotCancelChanges();
    void slotCheckProtocolType();
};

QT_BEGIN_NAMESPACE

#define ConnectionSettingsWindowPluginInterface "ConnectionSettingsWindow"

Q_DECLARE_INTERFACE(ConnectionSettingsWindow, ConnectionSettingsWindowPluginInterface)

QT_END_NAMESPACE

#endif // MONITORINGWINDOWPLUGIN_H
