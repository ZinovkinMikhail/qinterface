#include <assert.h>

#include <QQmlComponent>

#include "connectionsettingswindow.h"

using namespace rosslog;

ConnectionSettingsWindow::ConnectionSettingsWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    dataProvider(nullptr),
    adapterSettingsControl(nullptr),
    userManipulators(nullptr)
{
    // init combo drum model
    comboDrumModel = {"ВУ подключается к серверу", "КП подключается к ВУ"};
}

std::shared_ptr<IUserManipulators> ConnectionSettingsWindow::initWindow(EWindSizeType winSizeType,
                                                                      std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                      std::shared_ptr<IStateWidget> stateWidget,
                                                                      std::shared_ptr<IUserManipulators> userManipulators,
                                                                      std::shared_ptr<IQmlEngine> engine,
                                                                      std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    assert(engine);
    assert(userManipulators);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;

    windowComponent = engine->createComponent(CONNECTION_SETTINGS_WINDOW_QML_PATH);
    assert(windowComponent);

    grid = windowComponent->findChild<QQuickItem*>("Grid");
    assert(grid);
    connectionType = windowComponent->findChild<QQuickItem*>("ConnectionType");
    assert(connectionType);

    ipAddressItem = grid->findChild<QQuickItem*>("IpAddress");
    assert(ipAddressItem);
    loginItem = grid->findChild<QQuickItem*>("Login");
    assert(loginItem);
    passwordItem = grid->findChild<QQuickItem*>("Password");
    assert(passwordItem);
    portSSLItem = grid->findChild<QQuickItem*>("PortSSL");
    assert(portSSLItem);
    portHTTPItem = grid->findChild<QQuickItem*>("PortHTTP");
    assert(portHTTPItem);
    portUDPItem = grid->findChild<QQuickItem*>("PortUDP");
    assert(portUDPItem);


    initAdapterSettingsControl();
    connect(this, SIGNAL(signalRecivedAdapterData()),
            this, SLOT(slotRecievedAdapterData()));
    connect(windowComponent, SIGNAL(showComboDrum()),
            this, SLOT(slotShowComboDrum()));
    connect(windowComponent, SIGNAL(applyParams()),
            this, SLOT(slotApplyParams()));
    connect(windowComponent, SIGNAL(saveParams()),
            this, SLOT(slotSaveParams()));
    connect(windowComponent, SIGNAL(cancelChanges()),
            this, SLOT(slotCancelChanges()));
    connect(windowComponent, SIGNAL(checkProtocolType()),
            this, SLOT(slotCheckProtocolType()));

    return nullptr;
}

QObject *ConnectionSettingsWindow::getComponentObject()
{
    return windowComponent;
}

std::string ConnectionSettingsWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string ConnectionSettingsWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string ConnectionSettingsWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string ConnectionSettingsWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string ConnectionSettingsWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap ConnectionSettingsWindow::getIcon()
{
    return QPixmap(":/ConnectionSetts/icons/ConnectionSettings.png");
}

std::pair<bool, QPixmap> ConnectionSettingsWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/ConnectionSetts/icons/i19.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > ConnectionSettingsWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool ConnectionSettingsWindow::showWindow()
{
    this->updateData();
    return true;
}

bool ConnectionSettingsWindow::hideWindow()
{
    return  true;
}

EWindSizeType ConnectionSettingsWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool ConnectionSettingsWindow::prepareToDelete()
{
    return true;
}

void ConnectionSettingsWindow::initAdapterSettingsControl()
{
    if( adapterSettingsControl == nullptr )
    {
        adapterSettingsControl = std::shared_ptr<AdapterSettingsControl>(
                new AdapterSettingsControl(dataProvider, *logger.get()));
    }
}

void ConnectionSettingsWindow::getInfoFromAdapter()
{
    try
    {
        dataMutex.lock();
        this->adapterSettingsControl->updateSettings();
        this->adapterSettings = adapterSettingsControl->getAdapterSettings().Block0.AdapterSettings;
        dataMutex.unlock();
        emit signalRecivedAdapterData();
    }
    catch( std::system_error &systemErr )
    {
        dataMutex.unlock();
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        userManipulators->showMessage(std::string("Не удалось получить данные.\nКод ошибки: ").append(
                                          std::to_string(systemErr.code().value())));
    }
}

void ConnectionSettingsWindow::changeDesignByConnectionType(const ConnectionType &connType)
{
    QQuickItem *serverAddress = grid->findChild<QQuickItem*>("IpAddress");

    if( connType == SERVER )
    {
        QQmlProperty(serverAddress, "disableTexfield").write(true);
        QQmlProperty(connectionType, "txttxt").write(QString::fromStdString(comboDrumModel.at(connType)));
    }
    else
    {
        QQmlProperty(serverAddress, "disableTexfield").write(false);
        QQmlProperty(connectionType, "txttxt").write(QString::fromStdString(comboDrumModel.at(connType)));
    }
}

ConnectionType ConnectionSettingsWindow::getConnectionType(const uint32_t &adapterSettingsCon)
{
    if( adapterSettingsCon & AXIS_SOFTADAPTER_CONNECT__CLIENT_MODE )
    {
        return CLIENT;
    }
    else
        return SERVER;
}

std::pair<bool, uint32_t> ConnectionSettingsWindow::getValidIp(QString ip)
{
    if(ip.length() == 0)
    {
        return std::make_pair(false, 0);
    }

    char c_ip[32] = {0};
    uint32_t ipNum;

    memcpy(&c_ip, ip.toLocal8Bit().data(),sizeof(c_ip));
    ipNum = axis_create_ip(c_ip);

    if(ip <= 0)
    {
        return std::make_pair(false, 0);
    }
    return std::make_pair(true, ipNum);
}

void ConnectionSettingsWindow::updateDataOnAdapter()
{
    try
    {
        sleep(2);
        dataMutex.lock();
        adapterSettingsControl->applyAdapterSettings(adapterSettings);
        dataMutex.unlock();
    }
    catch( std::system_error &except )
    {
        dataMutex.unlock();
        userManipulators->showMessage(std::string("Не удалось применить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

void ConnectionSettingsWindow::saveDataOnAdapter()
{
    try
    {
        sleep(2);
        dataMutex.lock();
        adapterSettingsControl->saveAdapterSettings(adapterSettings);
        dataMutex.unlock();
        userManipulators->showMessage(std::string("Изменения втупят в силу после перезагрузки устройства."));
    }
    catch( std::system_error &except )
    {
        dataMutex.unlock();
        userManipulators->showMessage(std::string("Не удалось сохранить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

void ConnectionSettingsWindow::updateData()
{
    updateWorker = std::async(std::launch::async,
                              &ConnectionSettingsWindow::getInfoFromAdapter, this);
    updateWorker.wait_for(std::chrono::seconds(0));
}

bool ConnectionSettingsWindow::packNetworkParams(SoftAdapterSettingsStruct_t &settings)
{
    if( currentConnType == CLIENT )
    {
        settings.Connect |= AXIS_SOFTADAPTER_CONNECT__CLIENT_MODE;
    }
    else
    {
        settings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__CLIENT_MODE;
    }

    int portSSL = QQmlProperty(portSSLItem, "fieldText").read().toInt();
    int portHTTP = QQmlProperty(portHTTPItem, "fieldText").read().toInt();

    if(portSSL == 0)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "portSSL == 0");
        userManipulators->showMessage(std::string("Порт управления не может быть нулевым"));
        return  false;
    }

    if(portHTTP == 0)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "portHTTP == 0");
        userManipulators->showMessage(std::string("Порт копирования не может быть нулевым"));
        return  false;
    }

    if(portSSL == portHTTP)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "portSSL == portHTTP");
        userManipulators->showMessage(std::string("Порт управления не может быть равен порту копирования"));
        return  false;
    }

    settings.ControlPort = portSSL;
    settings.UploadPort = portHTTP;
    settings.MonitoringPort = QQmlProperty(portUDPItem, "fieldText").read().toInt();


    std::pair<bool, uint32_t> res = getValidIp(QQmlProperty(ipAddressItem, "fieldText").read().toString());

    if( !res.first )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "invalid ip address");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля адрес сервера."));
        return  false;
    }


    if(currentConnType == CLIENT)
    {
        // Check ip address is empty
        if( axis_is_ip_empty(res.second) == 1 )
        {
           this->logger->Message(LOG_ERR, __LINE__, __func__, "ip address is empty");
           userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля адрес сервера."));
           return  false;
        }

        // Check ip address is full
        if( axis_is_ip_full(res.second) == 1 )
        {
          this->logger->Message(LOG_ERR, __LINE__, __func__, "ip address is full");
          userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля адрес сервера."));
          return  false;
        }

        if(settings.Connect & AXIS_SOFTADAPTER_CONNECT__IP_STATIC) //check connect type not work
        {
            if( axis_is_ip_empty(settings.GWAddress) == 1 ) // Check gateway address is empty
            {
                if( axis_is_ip_in_network(res.second, settings.IPAddress, settings.NetMask) == 0 ) // Check ip address belongs to the network
                {
                  this->logger->Message(LOG_ERR, __LINE__, __func__, "ip address out of network");
                  userManipulators->showMessage(std::string("Адрес сервера не принадлежит сети устройства."));
                  return  false;
                }
            }
        }

        settings.ServerIPAddress = res.second;
    }



    QString userLogin = QQmlProperty(loginItem, "fieldText").read().toString();

    if( userLogin.isEmpty() )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid Login is empty");
        userManipulators->showMessage(std::string("Логин не может быть пустым."));
        return  false;
    }

    strncpy(settings.AdapterUser, userLogin.toLocal8Bit().data(), sizeof (settings.AdapterUser));


    QString password = QQmlProperty(passwordItem, "fieldText").read().toString();

    if( password.isEmpty() )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid password is empty");
        userManipulators->showMessage(std::string("Пароль не может быть пустым."));
        return  false;
    }

    strncpy(settings.AdapterPasswd, password.toLocal8Bit().data(), sizeof (settings.AdapterPasswd));

    return  true;
}

void ConnectionSettingsWindow::slotRecievedAdapterData()
{
    ConnectionType connType = getConnectionType(adapterSettings.Connect);
    changeDesignByConnectionType(connType);
    currentConnType = connType;

    for(auto &it : grid->children() )
    {
        if( it->objectName() == "Login" )
        {
            QQmlProperty(it, "fieldText").write(QString(adapterSettings.AdapterUser));
        }
        if( it->objectName() == "Password" )
        {
            QQmlProperty(it, "fieldText").write(QString(adapterSettings.AdapterPasswd));
        }
        if( it->objectName() == "IpAddress" )
        {
            char ip[32] = {0};
            if( !axis_sprint_ip(ip, adapterSettings.ServerIPAddress) )
            {
                this->logger->Message(LOG_ERR, __LINE__, __func__, "Canno't convert ip to char");
                continue;
            }
            QQmlProperty(it, "fieldText").write(QString(ip));
        }
        if( it->objectName() == "PortSSL" )
        {
            QQmlProperty(it, "fieldText").write(QString::number(adapterSettings.ControlPort));
        }
        if( it->objectName() == "PortHTTP" )
        {
            QQmlProperty(it, "fieldText").write(QString::number(adapterSettings.UploadPort));
        }
        if( it->objectName() == "PortUDP" )
        {
            QQmlProperty(it, "fieldText").write(QString::number(adapterSettings.MonitoringPort));
        }
    }
}


void ConnectionSettingsWindow::slotShowComboDrum()
{
    std::pair<bool, int> res = userManipulators->openComboDrum(currentConnType, comboDrumModel);
    if( res.first )
    {
        currentConnType = static_cast<ConnectionType>(res.second);
        changeDesignByConnectionType(currentConnType);
    }
}

void ConnectionSettingsWindow::slotApplyParams()
{
    if( packNetworkParams(adapterSettings) )
    {
        userManipulators->showLoadingWidget();
        updateWorker = std::async(std::launch::async,
                                  &ConnectionSettingsWindow::updateDataOnAdapter,
                                  this);
        updateWorker.wait_for(std::chrono::seconds(0));
    }
    else
    {
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных."));
    }
}

void ConnectionSettingsWindow::slotSaveParams()
{
    if( packNetworkParams(adapterSettings) )
    {
        userManipulators->showLoadingWidget();
        updateWorker = std::async(std::launch::async,
                                  &ConnectionSettingsWindow::saveDataOnAdapter,
                                  this);
        updateWorker.wait_for(std::chrono::seconds(0));
    }

}

void ConnectionSettingsWindow::slotCancelChanges()
{
    updateData();
}

void ConnectionSettingsWindow::slotCheckProtocolType()
{
    changeDesignByConnectionType(getConnectionType(adapterSettings.Connect));
}

ConnectionSettingsWindow::~ConnectionSettingsWindow()
{
    std::cerr << "Removed ConnectionSettingsWindow" << std::endl;
}
