import QtQuick 2.0
import QtQuick.Controls 2.2
import Cicada 1.0

Item
{
    width: 450
    height: 45
    property string fieldName
    property string fieldText
    property string fieldType: ""
    property bool disableTexfield: false

    property var ipValidator: RegExpValidator {
        regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
    }
    property var portValidator: RegExpValidator {
        regExp:  /^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/
    }

    ColorConfig {id: cc}

    Text
    {
        anchors.verticalCenter: parent.verticalCenter
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S6 - 2
        color: cc.light_green
        text: fieldName
    }

    Component.onCompleted:
    {
        if(fieldType == "ipMask")
        {
            tField.validator = ipValidator
            tField.placeholderText = "0.0.0.0"
            tField.inputMethodHints = Qt.ImhDigitsOnly
        }
        else if(fieldType == "port")
        {
            tField.width = 180
            tField.validator = portValidator
            tField.inputMethodHints = Qt.ImhDigitsOnly
        }
        else if(fieldType == "password")
        {
            tField.echoMode = TextInput.Password
            tField.passwordCharacter
            tField.placeholderText = "**********"
        }
    }


    TextField
    {
        id:tField
        anchors.right: parent.right
        height: 45
        width: 280
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S6
        horizontalAlignment: Text.AlignHCenter
//        inputMask: fieldType == "ipMask" ? "000.000.000.000;_" : ""
        enabled: !disableTexfield
        text: parent.fieldText
//        inputMethodHints: fieldType === "ipMask" ? Qt.ImhDigitsOnly : Qt.ImhNone


        background: Rectangle { color: "transparent" }

        Rectangle //background fix for yocto
        {
            anchors.fill: parent
            z:-1
            color: disableTexfield ? "#7f7f7f" : cc.white
        }

        onTextChanged:
        {
            parent.fieldText = text
        }

        ShadowRect{x: -4; y: 4}

        onPressed:
        {            
            if(inputPanelY > pressedFieldY)
            {
                pressedFieldY = mapToItem(connectionSettingWindow,x,y).y + 210
            }
        }
    }


}
