import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0

Item
{
    id: connectionSettingWindow
    y: 150
    width: 1280
    height: 484

    property var inputPanelY: parent ? parent.inputPanelY : 0
    property real pressedFieldY //for shift when virtual keyboard open

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal showComboDrum()
    signal applyParams()
    signal saveParams()
    signal cancelChanges()
    signal checkProtocolType()



    BackgroundTitle
    {
        text: qsTr("Параметры подключения")
    }


    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 395
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }


    Rectangle
    {
        id:mainZone
        anchors.fill: background
        anchors.margins: 13
        color: cc.dark_blue
        clip: true
    }


    Item
    {
        id:shiftZone
        parent: mainZone
        width: mainZone.width
        height: mainZone.height
        y:
        {
            if(inputPanelY < pressedFieldY)
            {
                inputPanelY - pressedFieldY
            }
            else
            {
                0
            }
        }


        ButtonDrop
        {
            id: protocolButton
            width: 452
            height: 52
            objectName: "ConnectionType"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 30
            anchors.leftMargin: 69
            txttxt: "ВУ подключается к серверу"
            longlongStyle: true
            onButtonClick:
            {
                connectionSettingWindow.showComboDrum()
            }
        }


        Grid
        {
            id:inputRows
            objectName: "Grid"
            y:120
            anchors.horizontalCenter: parent.horizontalCenter
            flow: Grid.TopToBottom
            columns: 2
            columnSpacing: 180
            rowSpacing: 40

            ConnectionTextField { id: fieldIP ; objectName: "IpAddress"; fieldName: "Адрес сервера"; fieldType: "ipMask"}
            ConnectionTextField { id: fieldLOGIN ; objectName: "Login" ; fieldName: "Логин"; fieldType: ""}
            ConnectionTextField { id: fieldPASS ; objectName: "Password"; fieldName: "Пароль"; fieldType: "password"}
            ConnectionTextField { id: fieldSSL ; objectName: "PortSSL"; fieldName: "Порт управления(SSL)"; fieldType: "port"}
            ConnectionTextField { id: fieldHTTP ; objectName: "PortHTTP"; fieldName: "Порт копирования(HTTP)"; fieldType: "port"}
            ConnectionTextField { id: fieldUDP ; objectName: "PortUDP"; fieldName: "Порт мониторига(UDP)"; fieldType: "port"}
        }
    }


    Item
    {
        id:controlButtons
        anchors.top: background.bottom
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        width: 480

        ControlListButton
        {
            anchors.right: parent.right
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked:
            {
                connectionSettingWindow.cancelChanges()
            }
        }

        ControlListButton
        {
            anchors.left: parent.left
            type: "SlimType"
            text: qsTr("Сохранить")
            onClicked:
            {
                connectionSettingWindow.saveParams()
            }
        }
    }
}

