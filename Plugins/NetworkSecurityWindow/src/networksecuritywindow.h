#ifndef NETWORSECURITYWINDOW_H
#define NETWORSECURITYWINDOW_H

#include <future>
#include <arpa/inet.h>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlProperty>

#include <axis_ifconfig.h>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/adaptersettingscontrol.h>
#include <dataprovider/adapterstatuscontrol.h>

#define WINDOW_IDENTIFICATOR "NetworkSecurity"
#define WINDOW_FOLDER_NAME "Adapter"
#define ICON_PRESENTATION_NAME "Безопасность"
#define FOLDER_PRESENTATION_NAME "Адаптер"

#define NETWORK_PARAMETERS_WINDOW_QML_PATH "qrc:/NetworkSecurity/NetworkSecurityWindow.qml"

class NetworkSecurityWindow : public QObject, public IEmptyPlaceWindow
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "NetworkSecurityWindow")
public:
    explicit NetworkSecurityWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~NetworkSecurityWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;

private:
    void initAdapterSettingsControl();
    void getInfoFromAdapter();
    void setRecievedData();
    void updateData();
    void saveData();

    bool convertIpToString(uint32_t ip, uint32_t mask, QString &start, QString &end);
    bool convertStringToIp(const QString &start, const QString &end, uint32_t &ip, uint32_t &mask);
    void setItemValue(QString itemName, QString valueName, QString value);
    QString getItemValue(QString itemName, QString valueName);

    void updateDataOnAdapter();

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    SoftAdapterSettingsStruct_t adapterSettings;
    std::shared_ptr<AdapterSettingsControl> adapterSettingsControl;
    std::future<void> updateWorker;
    std::shared_ptr<IUserManipulators> userManipulators;


signals:
    void signalRecivedAdapterData();
private slots:
    void slotRecievedAdapterData();
    void slotSaveButtonClicked();
    void slotCancelButtonClicked();
};

QT_BEGIN_NAMESPACE

#define NetworkSecurityWindowPluginInterface "NetworkSecurityWindow"

Q_DECLARE_INTERFACE(NetworkSecurityWindow, NetworkSecurityWindowPluginInterface)

QT_END_NAMESPACE

#endif // NETWORSECURITYWINDOW_H
