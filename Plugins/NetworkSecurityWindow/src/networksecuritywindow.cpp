#include <assert.h>

#include <QQmlComponent>

#include "networksecuritywindow.h"

using namespace rosslog;

NetworkSecurityWindow::NetworkSecurityWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    dataProvider(nullptr),
    adapterSettingsControl(nullptr),
    userManipulators(nullptr)
{}

std::shared_ptr<IUserManipulators> NetworkSecurityWindow::initWindow(EWindSizeType winSizeType,
                                                                      std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                      std::shared_ptr<IStateWidget> stateWidget,
                                                                      std::shared_ptr<IUserManipulators> userManipulators,
                                                                      std::shared_ptr<IQmlEngine> engine,
                                                                      std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    Q_UNUSED(log);
    assert(engine);
    assert(dataProvider);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(NETWORK_PARAMETERS_WINDOW_QML_PATH);
        assert(windowComponent);

        connect(this, SIGNAL(signalRecivedAdapterData()),
                this, SLOT(slotRecievedAdapterData()));
        connect(windowComponent, SIGNAL(saveButtonClicked()) ,
                this, SLOT(slotSaveButtonClicked()));
        connect(windowComponent, SIGNAL(cancelButtonClicked()) ,
                this, SLOT(slotCancelButtonClicked()));
    }
    initAdapterSettingsControl();

    return nullptr;
}

QObject *NetworkSecurityWindow::getComponentObject()
{
    return windowComponent;
}

std::string NetworkSecurityWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string NetworkSecurityWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string NetworkSecurityWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string NetworkSecurityWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string NetworkSecurityWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap NetworkSecurityWindow::getIcon()
{
    return QPixmap(":/NetworkSecurity/icons/i32.png");
}

std::pair<bool, QPixmap> NetworkSecurityWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/NetworkSecurity/icons/i19.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > NetworkSecurityWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool NetworkSecurityWindow::showWindow()
{
    updateData();
    return true;
}

bool NetworkSecurityWindow::hideWindow()
{
    return  true;
}

EWindSizeType NetworkSecurityWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool NetworkSecurityWindow::prepareToDelete()
{
    return true;
}

void NetworkSecurityWindow::initAdapterSettingsControl()
{
    if( adapterSettingsControl == nullptr )
    {
        adapterSettingsControl = std::shared_ptr<AdapterSettingsControl>(
                new AdapterSettingsControl(dataProvider, *logger.get()));
    }
}

void NetworkSecurityWindow::getInfoFromAdapter()
{
    try
    {
        this->adapterSettingsControl->updateSettings();
        this->adapterSettings = adapterSettingsControl->getAdapterSettings().Block0.AdapterSettings;
        emit signalRecivedAdapterData();
    }
    catch( std::system_error &systemErr )
    {
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        userManipulators->showMessage(std::string("Не удалось получть данные.\nКод ошибки: ").append(
                                          std::to_string(systemErr.code().value())));
    }
}

void NetworkSecurityWindow::setRecievedData()
{
    QString startIP;
    QString endIp;

    if( !convertIpToString(adapterSettings.AllowNet[0], adapterSettings.AllowMask[0], startIP, endIp) )
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Error while converting ip to string CKP");
    }
    setItemValue("CKPfrom", "text", startIP);
    setItemValue("CKPto", "text", endIp);

    if( !convertIpToString(adapterSettings.AllowNet[1], adapterSettings.AllowMask[1], startIP, endIp) )
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Error while converting ip to string KP");
    }

    setItemValue("KPfrom", "text", startIP);
    setItemValue("KPto", "text", endIp);

    if( !convertIpToString(adapterSettings.AllowNet[2], adapterSettings.AllowMask[2], startIP, endIp) )
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Error while converting ip to string MKP");
    }

    setItemValue("MKPfrom", "text", startIP);
    setItemValue("MKPto", "text", endIp);

    if( !convertIpToString(adapterSettings.AllowNet[3], adapterSettings.AllowMask[3], startIP, endIp) )
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__, "Error while converting ip to string OTHER");
    }

    setItemValue("OTHERfrom", "text", startIP);
    setItemValue("OTHERto", "text", endIp);
}

void NetworkSecurityWindow::updateData()
{
    updateWorker = std::async(std::launch::async,
                              &NetworkSecurityWindow::getInfoFromAdapter, this);
    updateWorker.wait_for(std::chrono::seconds(0));
}

void NetworkSecurityWindow::saveData()
{
    uint32_t ip = 0;
    uint32_t mask = 0;
    if( !convertStringToIp(getItemValue("CKPfrom", "text"),
                           getItemValue("CKPto", "text"), ip, mask) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while convert string to ip CKP");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля ЦКП."));
        return;
    }

    adapterSettings.AllowNet[0] = ip;
    adapterSettings.AllowMask[0] = mask;

    if( !convertStringToIp(getItemValue("KPfrom", "text"),
                           getItemValue("KPto", "text"), ip, mask) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while convert string to ip KP");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля КП."));
        return;
    }

    adapterSettings.AllowNet[1] = ip;
    adapterSettings.AllowMask[1] = mask;

    if( !convertStringToIp(getItemValue("MKPfrom", "text"),
                           getItemValue("MKPto", "text"), ip, mask) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while convert string to ip MKP");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля МКП."));
        return;
    }

    adapterSettings.AllowNet[2] = ip;
    adapterSettings.AllowMask[2] = mask;


    if( !convertStringToIp(getItemValue("OTHERfrom", "text"),
                           getItemValue("OTHERto", "text"), ip, mask) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while convert string to ip OTHER");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля Другие сети."));
        return;
    }

    adapterSettings.AllowNet[3] = ip;
    adapterSettings.AllowMask[3] = mask;

    updateWorker = std::async(std::launch::async,
                              &NetworkSecurityWindow::updateDataOnAdapter, this);
    this->userManipulators->showLoadingWidget();
    updateWorker.wait_for(std::chrono::seconds(0));
}

bool NetworkSecurityWindow::convertIpToString(uint32_t ip, uint32_t mask, QString &start, QString &end)
{
    uint32_t beginIp = ip;
    uint32_t uMask = mask;
    uint32_t endIP = beginIp | (~uMask);

    char value[32] = {0};

    if( !axis_sprint_ip(value, beginIp) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Cannot convert ip start");
        return  false;
    }
    start = value;

    if( !axis_sprint_ip(value, endIP) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Cannot convert ip end");
        return  false;
    }

    end = value;
    return true;
}

bool NetworkSecurityWindow::convertStringToIp(const QString &start, const QString &end,
                                              uint32_t &ip, uint32_t &mask)
{
    if( start.isEmpty() || end.isEmpty() )
        return false;

    ip = ntohl(inet_addr(start.toLocal8Bit().data()));
    uint32_t uEndIP = ntohl(inet_addr(end.toLocal8Bit().data()));

    mask = ip ^ uEndIP;
    mask = ~ntohl(mask);
    return true;
}

void NetworkSecurityWindow::setItemValue(QString itemName, QString valueName, QString value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

QString NetworkSecurityWindow::getItemValue(QString itemName, QString valueName)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    return QQmlProperty(item, valueName).read().toString();
}

void NetworkSecurityWindow::updateDataOnAdapter()
{
    try
    {
        sleep(2);
        adapterSettingsControl->saveAdapterSettings(adapterSettings);
        emit signalRecivedAdapterData();
    }
    catch( std::system_error &except )
    {
        userManipulators->showMessage(std::string("Не удалось сохранить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

void NetworkSecurityWindow::slotRecievedAdapterData()
{
    setRecievedData();
}

void NetworkSecurityWindow::slotSaveButtonClicked()
{
    saveData();
}

void NetworkSecurityWindow::slotCancelButtonClicked()
{
    setRecievedData();
}

NetworkSecurityWindow::~NetworkSecurityWindow()
{
    std::cerr << "Removed NetworkSecurityWindow" << std::endl;
}
