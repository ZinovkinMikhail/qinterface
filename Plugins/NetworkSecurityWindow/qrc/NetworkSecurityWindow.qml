import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2


Item
{
    id: networkSecurityWindow
    y: 150
    width: 1280
    height: 484

    property var inputPanelY: parent ? parent.inputPanelY : 0
    property real pressedFieldY //for shift when virtual keyboard open

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal saveButtonClicked()
    signal cancelButtonClicked()

    BackgroundTitle
    {
        text: qsTr("Безопасность сети")
    }


    Component.onCompleted:
    {
        labelCKP.item.text = "Сеть ЦКП";
        labelKP.item.text = "Сеть КП";
        labelMKP.item.text = "Сеть МКП";
        labelOther.item.text = "Другие сети";


        fieldCKPfrom.text = "0.0.0.0";
        fieldCKPto.text = "255.255.255.255";

        fieldKPfrom.text = "0.0.0.0";
        fieldKPto.text = "255.255.255.255";

        fieldMKPfrom.text = "0.0.0.0";
        fieldMKPto.text = "255.255.255.255";

        fieldOTHERfrom.text = "0.0.0.0";
        fieldOTHERto.text = "255.255.255.255";
    }


    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 370
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }



    Rectangle
    {
        id:mainZone
        anchors.fill: background
        anchors.margins: 18
        color: cc.dark_blue
        clip: true


        Column
        {
            id: namesAndFields
            objectName: "Column"
            x: parent.width/2 - width/2
            y:
            {
                if(inputPanelY < pressedFieldY)
                {
                    parent.height/2 - height/2 + inputPanelY - pressedFieldY
                }
                else
                {
                    parent.height/2 - height/2
                }
            }

            spacing: 30

            Row
            {
                spacing: 60
                Loader{sourceComponent: lineName; id: labelCKP; anchors.verticalCenter: parent.verticalCenter}
                NetworkInputField {id:fieldCKPfrom; objectName: "CKPfrom"}
                Loader{sourceComponent: dash; anchors.verticalCenter: parent.verticalCenter}
                NetworkInputField {id:fieldCKPto; objectName: "CKPto"}
            }
            Row
            {
                spacing: 60
                Loader{sourceComponent: lineName; id: labelKP; anchors.verticalCenter: parent.verticalCenter}
                NetworkInputField {id:fieldKPfrom; objectName: "KPfrom"}
                Loader{sourceComponent: dash; anchors.verticalCenter: parent.verticalCenter}
                NetworkInputField {id:fieldKPto; objectName: "KPto"}
            }
            Row
            {
                spacing: 60
                Loader{sourceComponent: lineName; id: labelMKP; anchors.verticalCenter: parent.verticalCenter}
                NetworkInputField {id:fieldMKPfrom; objectName: "MKPfrom"}
                Loader{sourceComponent: dash; anchors.verticalCenter: parent.verticalCenter}
                NetworkInputField {id:fieldMKPto; objectName: "MKPto"}
            }
            Row
            {
                spacing: 60
                Loader{sourceComponent: lineName; id: labelOther; anchors.verticalCenter: parent.verticalCenter}
                NetworkInputField {id:fieldOTHERfrom; objectName: "OTHERfrom"}
                Loader{sourceComponent: dash; anchors.verticalCenter: parent.verticalCenter}
                NetworkInputField {id:fieldOTHERto; objectName: "OTHERto"}
            }
        }
    }


    Item
    {
        id:controlButtons
        anchors.top: background.bottom
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        width: 470

        ControlListButton
        {
            anchors.right: parent.right
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked: cancelButtonClicked()
        }
        ControlListButton
        {
            anchors.left: parent.left
            type: "SlimType"
            text: qsTr("Сохранить")
            onClicked: saveButtonClicked()
        }
    }



    Component
    {
        id:lineName

        Text
        {
            width: 100
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S6 - 2
            color: cc.light_green
        }
    }

    Component
    {
        id:dash

       Rectangle
       {
           width: 10
           height: 4
           color: cc.light_green
       }
    }
}
