import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2



TextField
{
    id:textField

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    height: 50
    width: 350
    font.family: fontConf.mainFont
    font.pixelSize: fontConf.pixSize_S6
    horizontalAlignment: Text.AlignHCenter
//    inputMask:"000.000.000.000;_"
    placeholderText: "0.0.0.0"
    validator: RegExpValidator {
        regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
    }
    inputMethodHints:  Qt.ImhDigitsOnly

    background: Rectangle { color: "transparent" }

    Rectangle //background fix for yocto
    {
        anchors.fill: parent
        z:-1
        color: cc.white
    }

    ShadowRect{x: -4; y: 4}

    onPressed:
    {
        if(inputPanelY > pressedFieldY)
        {
            pressedFieldY = mapToItem(networkSecurityWindow.parent.parent,x,y).y + 60
        }
    }
}

