import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Item
{

    id: powerInfoWindow
    y: 150
    width: 1280
    height: 484

    ColorConfig { id: cc }
    FontsConfig{id:fontConf}

    property int localFontSize: fontConf.pixSize_S5 + 2

    BackgroundTitle
    {
        text: qsTr("Питание")
    }


    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 420
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }


    Item
    {
        id:mainZone
        anchors.fill: background
        anchors.margins: 18

        Text
        {
            id:externalDeviceLabel
            font.family: fontConf.mainFont
            font.pixelSize: localFontSize
            color: cc.light_green
            text: qsTr("Внешний источник:")
        }

        Text
        {
            id:externalDevice
            objectName: "DeviceSource"
            anchors.left: externalDeviceLabel.right
            anchors.leftMargin: 4
            font.family: fontConf.mainFont
            font.pixelSize: localFontSize
            color: cc.white
            text: qsTr("---")
        }


        ListView
        {
            id:listView
            objectName: "ListView"
            anchors.top: externalDeviceLabel.bottom
            anchors.topMargin: 15
            width: parent.width
            anchors.bottom: parent.bottom
            clip: true
            spacing: 2

            headerPositioning: ListView.OverlayHeader

            header: powerInfoHeader
//            model: powerInfoModel
            delegate: powerInfoRow

            ScrollBar.vertical:
                ScrollBar
                {
                    policy: listView.count > 8 ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                }
        }
    }




    Component
    {
        id: powerInfoHeader

        Row
        {
            z:2
            spacing: 2

            Rectangle
            {
                width: 402
                height: 32
                color: cc.dark_blue

                Rectangle
                {
                    y:parent.height - height
                    width: parent.width
                    height: 2
                    color: cc.blue
                }
            }

            Rectangle
            {
                width: 402+402
                height: 32
                color: cc.dark_blue

                Text
                {
                    anchors.centerIn: parent
                    font.family: fontConf.mainFont
                    font.italic: true
                    font.pixelSize: fontConf.pixSize_S6 - 2
                    color: cc.light_green
                    text: qsTr("Внутренний источник")
                }

                Rectangle
                {
                    y:parent.height - height
                    width: parent.width
                    height: 2
                    color: cc.blue
                }
            }

//            Rectangle
//            {
//                width: 402
//                height: 32
//                color: cc.dark_blue

//                Text
//                {
//                    anchors.centerIn: parent
//                    font.family: fontConf.mainFont
//                    font.italic: true
//                    font.pixelSize: fontConf.pixSize_S6 - 2
//                    color: cc.light_green
//                    text: qsTr("Внешний источник")
//                }

//                Rectangle
//                {
//                    y:parent.height - height
//                    width: parent.width
//                    height: 2
//                    color: cc.blue
//                }
//            }
        }
    }


    Component
    {
        id: powerInfoRow

        Row
        {
            spacing: 2

            Rectangle
            {
                width: 402
                height: nameField.height + 10
                color: cc.dark_blue

                Text
                {
                    id:nameField
                    anchors.verticalCenter: parent.verticalCenter
                    leftPadding: 10
                    font.family: fontConf.mainFont
                    font.pixelSize: fontConf.pixSize_S6 - 2
                    color: cc.light_green
                    text: statusName
                }
            }

            Rectangle
            {
                width: 402+402
                height: sourceInField.height + 10
                color: cc.dark_blue

                Text
                {
                    id:sourceInField
                    anchors.verticalCenter: parent.verticalCenter
                    leftPadding: 10
                    font.family: fontConf.mainFont
                    font.pixelSize: fontConf.pixSize_S6 - 2
                    color: cc.white
                    text: sourceIN
                }
            }

//            Rectangle
//            {
//                width: 402
//                height: sourceOutField.height + 10
//                color: cc.dark_blue

//                Text
//                {
//                    id:sourceOutField
//                    anchors.verticalCenter: parent.verticalCenter
//                    leftPadding: 10
//                    font.family: fontConf.mainFont
//                    font.pixelSize: fontConf.pixSize_S6 - 2
//                    color: cc.white
//                    text: sourceOUT
//                }
//            }
        }
    }


    ListModel
    {
      id: powerInfoModel


      ListElement { statusName:"Емкость, А/ч"; sourceIN:"11.00"; sourceOUT:"-" }
      ListElement { statusName:"Рабочее напряжение, В"; sourceIN:"3.70 - 4.20"; sourceOUT:"-" }
      ListElement { statusName:"Напряжение, В"; sourceIN:"4.14"; sourceOUT:"-" }
      ListElement { statusName:"Ток заряда, А"; sourceIN:"-0.49"; sourceOUT:"-" }
      ListElement { statusName:"Уровень заряда, %"; sourceIN:"87.00"; sourceOUT:"-" }
      ListElement { statusName:"Температура, С"; sourceIN:"30.00"; sourceOUT:"-" }
      ListElement { statusName:"Ожидаемое время записи"; sourceIN:"87ч : 0м"; sourceOUT:"-" }
      ListElement { statusName:"Ожидаемое время работы"; sourceIN:"19ч : 31м"; sourceOUT:"-" }
    }
}

