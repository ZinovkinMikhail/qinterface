#include "powerinfolistmodel.h"

PowerInfoListModel::PowerInfoListModel(QObject *parent)
{
}

void PowerInfoListModel::updateModel(QList<ItemModel> list)
{
    beginResetModel();
    items = list;
    endResetModel();
}

int PowerInfoListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return items.count();
}

QVariant PowerInfoListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= items.count())
         return QVariant();

     const ItemModel &info = items[index.row()];
     switch (role)
     {
         case NameRole:
            return info.name;
         case InternalRole:
            return info.internalVal;
         case ExternalRole:
            return info.externalVal;
         default:
             break;
     }
     return QVariant();
}

QHash<int, QByteArray> PowerInfoListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "statusName";
    roles[InternalRole] = "sourceIN";
    roles[ExternalRole] = "sourceOUT";
    return roles;
}
