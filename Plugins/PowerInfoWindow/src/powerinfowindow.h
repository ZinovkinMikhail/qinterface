#ifndef SETTINGSPARAMSWINDOW_H
#define SETTINGSPARAMSWINDOW_H

#include <future>
#include <arpa/inet.h>
#include <iostream>
#include <memory>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlProperty>
#include <QTextCodec>
#include <QDateTime>
#include <QVariant>

#include <axis_softadapter.h>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/adapterstatuscontrol.h>

#include "powerinfolistmodel.h"
#include "powerinfomodel.h"

#define WINDOW_IDENTIFICATOR "PowerInfoWindow"
#define WINDOW_FOLDER_NAME "System"
#define ICON_PRESENTATION_NAME "Питание"
#define FOLDER_PRESENTATION_NAME "Система"

#define POWER_STRING_TEMPLATE "%s ( %.2fВ )"

#define QML_PATH "qrc:/PowerInfo/PowerInfoWindow.qml"

class PowerInfoWindow : public QObject, public IEmptyPlaceWindow
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "PowerInfoWindow")
public:
    explicit PowerInfoWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~PowerInfoWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;


private:
    void initAdapterStatusControl();
    void initPowerListModel();
    void threadFunction();
//    void getAdapterData();
    void setRecievedData();
    void setSourceType();
    void packStringByType(const PowerInfoType type, const axis_acc_state_s &data,
                          const double &accWork, const double &accRecotd, QString &value);

    void setItemValue(QString itemName, QString valueName, QString value);
    void setItemValue(QString itemName, QString valueName, bool value);
    QString getItemValue(QString itemName, QString valueName);
    void getItemValue(QString itemName, QString valueName, bool &value);

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    SoftRecorderGetAccStateStruct_t adapterStatus;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusControl;
    std::future<void> updateWorker;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<PowerInfoListModel> powerListModel;
    PowerInfoModel infoModel;
    std::thread statusThread;
//    std::condition_variable cv;
//    std::mutex cv_m;
    std::mutex dataMutex;
    bool conditionWait;
    bool threadWork;
    bool updateLoop;


signals:
    void signalRecivedAdapterData();
private slots:
    void slotRecievedAdapterData();
};

QT_BEGIN_NAMESPACE

#define PowerInfoWindowPluginInterface "PowerInfoWindow"

Q_DECLARE_INTERFACE(PowerInfoWindow, PowerInfoWindowPluginInterface)

QT_END_NAMESPACE

#endif // MONITORINGWINDOWPLUGIN_H
