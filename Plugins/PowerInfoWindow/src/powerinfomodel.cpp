#include "powerinfomodel.h"

PowerInfoModel::PowerInfoModel()
{
    ItemModel model;
    model.name = "Емкость, А/ч";
    model.externalVal = "-";
    model.internalVal = "-";
    items[CAPACITY] = model;
    model.name = "Рабочее напряжение, В";
    model.externalVal = "-";
    model.internalVal = "-";
    items[WORK_VOLTAGE] = model;
    model.name = "Напряжение, В";
    model.externalVal = "-";
    model.internalVal = "-";
    items[VOLTAGE] = model;
    model.name = "Ток заряда, А";
    model.externalVal = "-";
    model.internalVal = "-";
    items[AMPERAGE] = model;
    model.name = "Уровень заряда, %";
    model.externalVal = "-";
    model.internalVal = "-";
    items[PROCENT] = model;
//    model.name = "Температура, С";
//    model.externalVal = "-";
//    model.internalVal = "-";
//    items[TEMPERATURE] = model;
#warning Comented for emulator
//    model.name = "Ожидаемое время записи";
//    model.externalVal = "-";
//    model.internalVal = "-";
//    items[LEFT_RECORD_TIME] = model;
//    model.name = "Ожидаемое время работы";
//    model.externalVal = "-";
//    model.internalVal = "-";
//    items[LEFT_WORK_TIME] = model;
}

std::map<PowerInfoType, ItemModel> &PowerInfoModel::getModelItems()
{
    return items;
}
