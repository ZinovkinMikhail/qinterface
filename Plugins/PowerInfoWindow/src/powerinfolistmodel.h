#ifndef POWERINFOLISTMODEL_H
#define POWERINFOLISTMODEL_H

#include <QObject>

#include <QAbstractListModel>
#include <QStringList>

#include "powerinfomodel.h"

typedef enum
{
    NameRole = Qt::UserRole + 1,
    InternalRole,
    ExternalRole
}ListRoles;

class PowerInfoListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit PowerInfoListModel(QObject *parent = nullptr);

    void updateModel(QList<ItemModel> list);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<ItemModel> items;

public slots:
};

#endif // POWERINFOLISTMODEL_H
