#include <assert.h>

#include <QQmlComponent>
#include <sys/time.h>

#include <rosslog/log.h>
#include "powerinfowindow.h"

using namespace rosslog;
using namespace adapter_status_ctrl;

PowerInfoWindow::PowerInfoWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    dataProvider(nullptr),
    adapterStatusControl(nullptr),
    userManipulators(nullptr),
    powerListModel(nullptr),
    conditionWait(false),
    threadWork(true),
    updateLoop(false)
{}

std::shared_ptr<IUserManipulators> PowerInfoWindow::initWindow(EWindSizeType winSizeType,
                                                                      std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                      std::shared_ptr<IStateWidget> stateWidget,
                                                                      std::shared_ptr<IUserManipulators> userManipulators,
                                                                      std::shared_ptr<IQmlEngine> engine,
                                                                      std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    Q_UNUSED(log);
    assert(engine);
    assert(dataProvider);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(QML_PATH);
        assert(windowComponent);

        connect(this, SIGNAL(signalRecivedAdapterData()),
                this, SLOT(slotRecievedAdapterData()));

    }
    initPowerListModel();
    initAdapterStatusControl();
    statusThread = std::thread(&PowerInfoWindow::threadFunction, this);

    return nullptr;
}

QObject *PowerInfoWindow::getComponentObject()
{
    return windowComponent;
}

std::string PowerInfoWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string PowerInfoWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string PowerInfoWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string PowerInfoWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string PowerInfoWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap PowerInfoWindow::getIcon()
{
    return QPixmap(":/PowerInfo/icons/i9.png");
}

std::pair<bool, QPixmap> PowerInfoWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/PowerInfo/icons/i20.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > PowerInfoWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool PowerInfoWindow::showWindow()
{
    this->userManipulators->showLoadingWidget();
    updateLoop = true;
    return true;
}

bool PowerInfoWindow::hideWindow()
{
    updateLoop = false;
    return  true;
}

EWindSizeType PowerInfoWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool PowerInfoWindow::prepareToDelete()
{
    return true;
}

void PowerInfoWindow::initAdapterStatusControl()
{
    if( adapterStatusControl == nullptr )
    {
        adapterStatusControl = std::make_shared<AdapterStatusControl>(dataProvider, *logger.get());
    }
}

void PowerInfoWindow::initPowerListModel()
{
    if( powerListModel == nullptr )
    {

        powerListModel = std::make_shared<PowerInfoListModel>();
        QQuickItem *listView = windowComponent->findChild<QQuickItem*>("ListView");
        assert(listView);
        listView->setProperty("model", QVariant::fromValue(powerListModel.get()));
    }
}

//void PowerInfoWindow::getAdapterData()
//{
//    this->userManipulators->showLoadingWidget();
//    updateWorker = std::async(std::launch::async, [this]() mutable
//    {
//        try
//        {
//            this->userManipulators->hideLoadingWidget();
//            adapterStatusControl->updateStatus();
//            adapterStatus = adapterStatusControl->getAccStatus();
//            emit signalRecivedAdapterData();
//        }
//        catch(std::system_error &except)
//        {
//            this->userManipulators->hideLoadingWidget();
//            this->logger->Message(LOG_ERR, __LINE__, __func__,
//                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
//            userManipulators->showMessage(std::string("Не удалось получть данные.\nКод ошибки: ").append(
//                                              std::to_string(except.code().value())));
//        }
//    });
//    updateWorker.wait_for(std::chrono::seconds(0));
//}

void PowerInfoWindow::threadFunction()
{
    struct timeval tv;
    __time_t lastTime = 0;

    while(threadWork)
    {
        if(updateLoop)
        {
            gettimeofday(&tv, nullptr);
            if( tv.tv_sec >= (lastTime + 1) || !conditionWait || tv.tv_sec < lastTime )
            {
                conditionWait = true;
                lastTime = tv.tv_sec;


    //            this->logger->Message(LOG_INFO, __LINE__, __func__, "---Update POWER data---");
                try
                {
                    dataMutex.lock();
                    adapterStatusControl->updateStatus();
                    adapterStatus = adapterStatusControl->getAccStatus();
                    dataMutex.unlock();
                    emit signalRecivedAdapterData();
                }
                catch( std::system_error &systemErr )
                {
                    dataMutex.unlock();
                    this->userManipulators->hideLoadingWidget();
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
                }
    //            std::unique_lock<std::mutex> lk(this->cv_m);
    //            cv.wait_for(lk, std::chrono::seconds(1), [this]{return conditionWork;});
    //            conditionWork = false;
            }
            else
            {
                usleep(10000);
            }
        }
        else
            usleep(10000);
    }
}

void PowerInfoWindow::setItemValue(QString itemName, QString valueName, QString value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

void PowerInfoWindow::setItemValue(QString itemName, QString valueName, bool value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    QQmlProperty(item, valueName).write(value);
}

void PowerInfoWindow::setRecievedData()
{
    this->userManipulators->hideLoadingWidget();

    QList<ItemModel> list;

    setSourceType();

    auto items = infoModel.getModelItems();

    packStringByType(CAPACITY, adapterStatus.AccStatus.int_acc, 0, 0, items[CAPACITY].internalVal);
    packStringByType(WORK_VOLTAGE, adapterStatus.AccStatus.int_acc, 0, 0, items[WORK_VOLTAGE].internalVal);
    packStringByType(VOLTAGE, adapterStatus.AccStatus.int_acc, 0, 0, items[VOLTAGE].internalVal);
    packStringByType(AMPERAGE, adapterStatus.AccStatus.int_acc, 0, 0, items[AMPERAGE].internalVal);
    packStringByType(PROCENT, adapterStatus.AccStatus.int_acc, 0, 0, items[PROCENT].internalVal);
//    packStringByType(TEMPERATURE, adapterStatus.AccStatus.int_acc, 0, 0, items[TEMPERATURE].internalVal);
#warning Commented for emulator
//    packStringByType(LEFT_RECORD_TIME, adapterStatus.AccStatus.int_acc, adapterStatus.DCIntACCWork,
//                     adapterStatus.DCIntACCRecord, items[LEFT_RECORD_TIME].internalVal);

    LOG(this->logger, LOG_INFO, "========= Volt internal Val= %s", items[VOLTAGE].internalVal.toLocal8Bit().data() );
    if(adapterStatus.AccStatus.ext_acc.acc_present & AXIS_ACC_STATE_ACC_PRESENT)
    {
        LOG(this->logger, LOG_INFO, "========= adapterStatus.AccStatus.ext_acc.acc_present & AXIS_ACC_STATE_ACC_PRESENT");
        
        packStringByType(CAPACITY, adapterStatus.AccStatus.ext_acc, 0, 0, items[CAPACITY].externalVal);
        packStringByType(WORK_VOLTAGE, adapterStatus.AccStatus.ext_acc, 0, 0, items[WORK_VOLTAGE].externalVal);
        packStringByType(VOLTAGE, adapterStatus.AccStatus.ext_acc, 0, 0, items[VOLTAGE].externalVal);
        packStringByType(AMPERAGE, adapterStatus.AccStatus.ext_acc, 0, 0, items[AMPERAGE].externalVal);
        packStringByType(PROCENT, adapterStatus.AccStatus.ext_acc, 0, 0, items[PROCENT].externalVal);
//        packStringByType(TEMPERATURE, adapterStatus.AccStatus.ext_acc, 0, 0, items[TEMPERATURE].externalVal);
        packStringByType(LEFT_RECORD_TIME, adapterStatus.AccStatus.ext_acc, adapterStatus.DCExtACCWork,
                         adapterStatus.DCExtACCRecord, items[LEFT_RECORD_TIME].externalVal);
        LOG(this->logger, LOG_INFO, "========= Volt external Val= %s", items[VOLTAGE].externalVal.toLocal8Bit().data() );
    }
    list.append(items[CAPACITY]);
    list.append(items[WORK_VOLTAGE]);
    list.append(items[VOLTAGE]);
    list.append(items[AMPERAGE]);
    list.append(items[PROCENT]);
//    list.append(items[TEMPERATURE]);
    list.append(items[LEFT_RECORD_TIME]);

    powerListModel->updateModel(list);
}

void PowerInfoWindow::setSourceType()
{
    QString source;
    int statusPower = axis_acc_get_status_power( &adapterStatus.AccStatus );
    double statusPowerVol = (double)adapterStatus.Voltage/(double)1000;

    switch( statusPower )
    {
        case AXIS_ACC_IO_STATUS__ACC:
        {
            source.sprintf( "%s","отсутствует" );
            break;
        }
        case AXIS_ACC_IO_STATUS__USB:
        {
            source.sprintf( POWER_STRING_TEMPLATE, "USB", statusPowerVol );
            break;
        }
        case AXIS_ACC_IO_STATUS__DC:
        {
            source.sprintf( POWER_STRING_TEMPLATE, "DC", statusPowerVol );
            break;
        }
        case AXIS_ACC_IO_STATUS__AC:
        {
            source.sprintf( POWER_STRING_TEMPLATE, "ИБП+220В", statusPowerVol );
            break;
        }
        case AXIS_ACC_IO_STATUS__EXT_ACC:
        {
            source.sprintf( POWER_STRING_TEMPLATE, "ИБП", statusPowerVol );
            break;
        }
        default:
        {
            source.sprintf( "%s", "неопределенен" );
            break;
        }
    }

    QQuickItem *device = windowComponent->findChild<QQuickItem*>("DeviceSource");
    device->setProperty("text", QVariant::fromValue(source));
}

void PowerInfoWindow::packStringByType(const PowerInfoType type,
                                       const axis_acc_state_s &data, const double &accWork,
                                       const double &accRecotd, QString &value)
{
    switch (type)
    {
        case CAPACITY:
        {
            value = QString::number( (axis_acc_get_capacity( &data )) ,'f',2);
            break;
        }
        case WORK_VOLTAGE:
        {
            value = QString("%1 - %2").arg(QString::number(
                        (axis_acc_get_low_voltage( &data)/10), 'f', 2)).arg(
                            QString::number( (axis_acc_get_full_voltage( &data )/10) , 'f', 2));
            break;
        }
        case VOLTAGE:
        {
            //dep value = QString::number(axis_acc_get_current_voltage(&data), 'f', 2);
            float val = data.acc_voltage / 1000.;
            value = QString::number( val, 'f', 2);
            break;
        }
        case AMPERAGE:
        {
            double int_acc_dc = axis_acc_get_dc( &data );
            value = QString::number( int_acc_dc ,'f',2);
            break;
        }
        case PROCENT:
        {
            value = QString::number( (data.acc_rem_capacity / 100.0) ,'f',2);
            break;
        }
//        case TEMPERATURE:
//        {
//            value = QString::number( (data.acc_temperature) ,'f',2);
//            break;
//        }
        case LEFT_RECORD_TIME:
        {
            double int_acc_dc = axis_acc_get_dc( &data );
            double acc_record_dc = accRecotd;
            double acc_work_dc = accWork; //ток при работе от внутреннего АКБ

            double int_work_dc = acc_work_dc;
            if( int_acc_dc < AXIS_ACC_DISCHARGE_CURRENT )
            {
                int_work_dc = int_acc_dc;
            }
            if( acc_record_dc )
            {
                int HrReac = (int)( (double)axis_acc_get_decharge_time_sec( &data, int_work_dc ) / 3600.0 );
                int MinRec = (int)( (double)axis_acc_get_decharge_time_sec( &data, int_work_dc ) / 60.0 ) - HrReac * 60;

                value.sprintf( "%i ч : %i м", HrReac, MinRec );
            }
            else
            {
                value = "-";
            }
            break;
        }
        default:
            break;
    }
}


QString PowerInfoWindow::getItemValue(QString itemName, QString valueName)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    return QQmlProperty(item, valueName).read().toString();
}

void PowerInfoWindow::getItemValue(QString itemName, QString valueName, bool &value)
{
    QQuickItem *item = windowComponent->findChild<QQuickItem*>(itemName);
    assert(item);
    value = QQmlProperty(item, valueName).read().toBool();
}

void PowerInfoWindow::slotRecievedAdapterData()
{
    setRecievedData();
}


PowerInfoWindow::~PowerInfoWindow()
{
    threadWork = false;
    conditionWait = false;
//    cv.notify_one();
    if( statusThread.joinable() )
    {
        statusThread.join();
    }
    std::cerr << "Removed PowerInfoWindow" << std::endl;
}
