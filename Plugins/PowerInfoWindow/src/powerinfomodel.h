#ifndef POWERINFOMODEL_H
#define POWERINFOMODEL_H

#include <map>

#include <QString>



typedef enum
{
    CAPACITY,
    WORK_VOLTAGE,
    VOLTAGE,
    AMPERAGE,
    PROCENT,
//    TEMPERATURE,
    LEFT_RECORD_TIME,
    LEFT_WORK_TIME
} PowerInfoType;

typedef struct
{
    QString name;
    QString internalVal;
    QString externalVal;
} ItemModel;


class PowerInfoModel
{
public:
    explicit PowerInfoModel();

    std::map<PowerInfoType, ItemModel> &getModelItems();

private:
    std::map<PowerInfoType, ItemModel> items;


};

#endif // POWERINFOMODEL_H
