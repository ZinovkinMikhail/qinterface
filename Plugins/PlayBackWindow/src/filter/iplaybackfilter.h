//
// Created by ivan on 22.02.2021.
//

#ifndef CM_INTERFACE_IPLAYBACKFILTER_H
#define CM_INTERFACE_IPLAYBACKFILTER_H

#include <cstdint>

#include "filtermodel.h"

class IPlayBackFilter
{
public:
    virtual bool filterSessionsData(SessionFilter filterData) = 0;
    virtual bool filterMaskData(SessionMask maskData) = 0;
    virtual SessionFilter getFilterData() = 0;
    virtual SessionMask getMaskData() = 0;

};

#endif //CM_INTERFACE_IPLAYBACKFILTER_H
