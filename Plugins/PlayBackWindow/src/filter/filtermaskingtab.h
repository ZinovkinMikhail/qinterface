#ifndef FILTERMASKINGTAB_H
#define FILTERMASKINGTAB_H

#include <memory>
#include <string>
#include <utility>
#include <iterator>

#include <QObject>
#include <QQuickItem>
//#include <QQmlProperty>

#include <rosslog/log.h>
#include <iqmlengine.h>

#include <iusermanipulators.h>
#include <dataprovider/dataprovider.h>
#include <dataprovider/recordersettingscontol.h>

#include "src/database/audiopkdworker.h"
#include "iplaybackfilter.h"

#define FILTER_QML_PARH "qrc:/PlayBackWindow/FilterMaskingTab.qml"
#define FILTER_MASKING_TAB_TITLE "Фильтр и Маскирование"


class FilterMaskingTab: public QObject, public std::enable_shared_from_this<FilterMaskingTab>
{
    Q_OBJECT
public:
    explicit FilterMaskingTab(std::shared_ptr<IUserManipulators> userManipulators,
    std::shared_ptr<IQmlEngine> qmlEngine,
    std::shared_ptr<rosslog::Log> log,
    IPlayBackFilter &playBackFilterInterface,
    std::shared_ptr<data_provider::DataProvider> dataProvider,
    QObject *parent = nullptr);

    void init();
    void show();
    void hide();

    std::string getWindowTitle();
    void setDateInterval(const uint64_t startDate, const uint64_t endDate);

private:
//    void initRecoderSettingsControl();
    void setCheckBoxFilter(const bool &value);
    bool getCheckBoxFilterState();
    void setCheckBoxMasking(const bool &value);
    bool getCheckBoxMaskingState();
    void setRadioButtonDate(const bool &value);
    bool getRadioButtonDateState();
    void setRadioButtonChannel(const bool &value);
    bool getRadioButtonChannelState();
    void setDateStartButton(const QString &date);
    QString getDateStart();
    void setDateEndButton(const QString &date);
    QString getDateEnd();
    void setChannelButton(const QString &channelName);
    void setMaskingField(const QString &fieldText);
    QString getMaskingField();
//    void updateRecorderSettingsParams();
//    void saveRecorderSettingsParams();
    void updateParams();


    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<rosslog::Log> logger;
    IPlayBackFilter &playBackFilterInterface;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    QQuickItem *currentItem;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::map<int, SessionFilter> sessions;
    uint64_t startDate;
    uint64_t endDate;
    std::vector<std::string> channelsTypeVector;
    uint8_t currentChannelType;
    std::shared_ptr<RecorderSettingsContol> recorderSettingsContol;
    std::future<void> updateWorker;
    SoftRecorderSettingsStruct_t recorderSettings;
    QDateTime lastEndDate;
    QDateTime lastStartDate;
    bool useLastDate{false};

    //DesignItems
    QQuickItem *checkBoxFilterItem;
    QQuickItem *checkBoxMaskFilterItem;
    QQuickItem *radioButtonDateItem;
    QQuickItem *radioButtonChannelItem;
    QQuickItem *dateStartItem;
    QQuickItem *dateEndItem;
    QQuickItem *channelButtonItem;
    QQuickItem *maskFieldItem;

signals:
//    void signalUpdateRecorderSettings();

public slots:
    void slotShowDateComboDrum(int type);
    void slotSaveButtonClicked();
    void slotCancelButtonClicked();
    void slotShowComboDrumChannel();
//    void slotUpdateRecorderSettings();

};


#endif //FILTERMASKINGTAB_H
