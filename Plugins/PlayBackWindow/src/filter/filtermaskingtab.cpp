//
// Created by ivan and yaroslav on 23.10.2020.
//

#include <cassert>
#include <QQmlProperty>
#include "filtermaskingtab.h"


using namespace data_provider;
using namespace rosslog;

FilterMaskingTab::FilterMaskingTab(std::shared_ptr<IUserManipulators> userManipulators,
                             std::shared_ptr<IQmlEngine> qmlEngine,
                             std::shared_ptr<rosslog::Log> log,
                             IPlayBackFilter &playBackFilterInterface,
                             std::shared_ptr<data_provider::DataProvider> dataProvider,
                             QObject *parent) :
        QObject(parent),
        userManipulators(userManipulators),
        qmlEngine(qmlEngine),
        logger(log),
        playBackFilterInterface(playBackFilterInterface),
        dataProvider(dataProvider),
        currentItem(nullptr),
        startDate(0),
        endDate(0),
        currentChannelType(0),
        recorderSettingsContol(nullptr)
{
    assert(dataProvider),
    assert(qmlEngine);
    assert(logger);
    assert(parent);

    memset(&recorderSettings, 0, sizeof(SoftRecorderSettingsStruct_t));
}


void FilterMaskingTab::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(
                qmlEngine->createComponent(FILTER_QML_PARH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
        auto * mainZone = currentItem->findChild<QQuickItem*>("mainZone");

        checkBoxFilterItem = mainZone->findChild<QQuickItem*>("CheckBoxFilter");
        assert(checkBoxFilterItem);
        checkBoxMaskFilterItem = mainZone->findChild<QQuickItem*>("CheckBoxMasking");
        assert(checkBoxMaskFilterItem);
        radioButtonDateItem = mainZone->findChild<QQuickItem*>("RadioButtonDate");
        assert(radioButtonDateItem);
        radioButtonChannelItem = mainZone->findChild<QQuickItem*>("RadioButtonChannel");
        assert(radioButtonChannelItem);
        dateStartItem = mainZone->findChild<QQuickItem*>("DateStartButton");
        assert(dateStartItem);
        dateEndItem = mainZone->findChild<QQuickItem*>("DateEndButton");
        assert(dateEndItem);
        channelButtonItem = mainZone->findChild<QQuickItem*>("ChannelButton");
        assert(channelButtonItem);
        maskFieldItem = mainZone->findChild<QQuickItem*>("MaskingField");
        assert(maskFieldItem);

        connect(currentItem, SIGNAL(showComboDrumDate(int)),
                this, SLOT(slotShowDateComboDrum(int)));
        connect(currentItem, SIGNAL(showComboDrumChannel()),
                this, SLOT(slotShowComboDrumChannel()));
        connect(currentItem, SIGNAL(saveButtonClicked()),
                this, SLOT(slotSaveButtonClicked()));
        connect(currentItem, SIGNAL(cancelButtonClicked()),
                this, SLOT(slotCancelButtonClicked()));

//        connect(this, SIGNAL(signalUpdateRecorderSettings()),
//                this, SLOT(slotUpdateRecorderSettings()));

        channelsTypeVector.reserve(7);
        channelsTypeVector.emplace_back("Моно 1");
        channelsTypeVector.emplace_back("Моно 2");
        channelsTypeVector.emplace_back("Моно 3");
        channelsTypeVector.emplace_back("Моно 4");
        channelsTypeVector.emplace_back("Стерео 1");
        channelsTypeVector.emplace_back("Стерео 3");

//        initRecoderSettingsControl();
    }
}

//void FilterMaskingTab::initRecoderSettingsControl()
//{
//    if( recorderSettingsContol == nullptr )
//    {
//        recorderSettingsContol = std::make_shared<RecorderSettingsContol>(dataProvider, *logger.get());
//    }
//}

void FilterMaskingTab::show()
{
//    updateRecorderSettingsParams();
    updateParams();
    QQmlProperty(currentItem, "visible").write(true);

    if( !useLastDate )
    {
        setDateStartButton(QDateTime::fromSecsSinceEpoch(startDate).toString("dd.MM.yyyy"));
        setDateEndButton(QDateTime::fromSecsSinceEpoch(endDate).toString("dd.MM.yyyy"));
    }
    else
    {
        setDateStartButton(lastStartDate.toString("dd.MM.yyyy"));
        setDateEndButton(lastEndDate.toString("dd.MM.yyyy"));
    }
    try
    {
        setChannelButton( QString::fromStdString(channelsTypeVector.at(currentChannelType)));
    }
    catch (std::exception &except)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "%s", except.what());
    }
}


void FilterMaskingTab::hide()
{
    QQmlProperty(currentItem, "visible").write(false);
}


std::string FilterMaskingTab::getWindowTitle()
{
    return FILTER_MASKING_TAB_TITLE;
}


void FilterMaskingTab::setCheckBoxFilter(const bool &value)
{
    QQmlProperty(checkBoxFilterItem, "checked").write(value);
}
bool FilterMaskingTab::getCheckBoxFilterState()
{
    return QQmlProperty(checkBoxFilterItem, "checked").read().toBool();
}


void FilterMaskingTab::setCheckBoxMasking(const bool &value)
{
    QQmlProperty(checkBoxMaskFilterItem, "checked").write(value);
}
bool FilterMaskingTab::getCheckBoxMaskingState()
{
    return QQmlProperty(checkBoxMaskFilterItem, "checked").read().toBool();
}


void FilterMaskingTab::setRadioButtonDate(const bool &value)
{
    QQmlProperty(radioButtonDateItem, "checked").write(value);
}
bool FilterMaskingTab::getRadioButtonDateState()
{
    return QQmlProperty(radioButtonDateItem, "checked").read().toBool();
}


void FilterMaskingTab::setRadioButtonChannel(const bool &value)
{
    QQmlProperty(radioButtonChannelItem, "checked").write(value);
}
bool FilterMaskingTab::getRadioButtonChannelState()
{
    return QQmlProperty(radioButtonChannelItem, "checked").read().toBool();
}

void FilterMaskingTab::setDateStartButton(const QString &date)
{
    QQmlProperty(dateStartItem, "txttxt").write(date);
}
QString FilterMaskingTab::getDateStart()
{
    return QQmlProperty(dateStartItem, "txttxt").read().toString();
}

void FilterMaskingTab::setDateEndButton(const QString &date)
{
    QQmlProperty(dateEndItem, "txttxt").write(date);
}
QString FilterMaskingTab::getDateEnd()
{
    return QQmlProperty(dateEndItem, "txttxt").read().toString();
}

void FilterMaskingTab::setChannelButton(const QString &channelName)
{
    QQmlProperty(channelButtonItem, "txttxt").write(channelName);
}

void FilterMaskingTab::setMaskingField(const QString &fieldText)
{
    QQmlProperty(maskFieldItem, "text").write(fieldText);
}
QString FilterMaskingTab::getMaskingField()
{
    return QQmlProperty(maskFieldItem, "text").read().toString();
}


void FilterMaskingTab::slotShowDateComboDrum(int type)
{
    QDateTime dateTime = QDateTime::currentDateTime();
    if(type)
        dateTime = QDateTime::fromSecsSinceEpoch(endDate);
    else
        dateTime = QDateTime::fromSecsSinceEpoch(startDate);

    auto res = this->userManipulators->openDateComboDrum(dateTime.date().day(), dateTime.date().month(), dateTime.date().year());
    if( res.first )
    {
        if(type)
            setDateEndButton(QString::fromStdString(res.second));
        else
            setDateStartButton(QString::fromStdString(res.second));
    }
}


void FilterMaskingTab::slotShowComboDrumChannel()
{
    auto res = this->userManipulators->openComboDrum(currentChannelType, channelsTypeVector);
    if( res.first )
    {
        currentChannelType = res.second;
        try
        {
            setChannelButton(QString::fromStdString(channelsTypeVector.at(res.second)));
        }
        catch (std::exception &except )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ ,"Channel combodrum error %s", except.what());
        }
    }
}

void FilterMaskingTab::slotSaveButtonClicked()
{
    QQmlProperty(currentItem, "visible").write(true);
    QQuickItem * mainZone = currentItem->findChild<QQuickItem*>("mainZone");

    std::cout << "getCheckBoxFilterState= "<< getCheckBoxFilterState() << std::endl;
    std::cout << "getCheckBoxMaskingState= "<< getCheckBoxMaskingState() << std::endl;
    std::cout << "getRadioButtonDateState= "<< getRadioButtonDateState() << std::endl;
    std::cout << "getRadioButtonChannelState= "<< getRadioButtonChannelState() << std::endl;
    std::cout << "getDateStart= "<< getDateStart().toStdString() << std::endl;
    std::cout << "getDateEnd= "<< getDateEnd().toStdString() << std::endl;
    std::cout << "getMaskingField= "<< getMaskingField().toStdString() << std::endl;

    SessionFilter data;

    if( getCheckBoxFilterState() )
    {
        if( getRadioButtonDateState() )
        {
            auto startDate = QDateTime::fromString(getDateStart(), "dd.MM.yyyy");
            startDate.setTime(QTime::fromString("00:00:00", "hh:mm:ss"));
            auto endDate = QDateTime::fromString(getDateEnd(), "dd.MM.yyyy");
            endDate.setTime(QTime::fromString("23:59:59", "hh:mm:ss"));


            useLastDate = true;
            lastStartDate = startDate;
            lastEndDate = endDate;
            data.type = DATE_FILTER;
            data.startDate = startDate.toSecsSinceEpoch();
            data.endDate = endDate.toSecsSinceEpoch();
        }
        else if( getRadioButtonChannelState() )
        {
            data.type = CHANNEL_FILTER;
            data.channelType = currentChannelType;
        }
    }
    else
    {
        useLastDate = false;
        data.type = NONE_FILTER;
    }

    SessionMask mask;

    if( getCheckBoxMaskingState() )
    {
        QString codeStr = getMaskingField();

        if( codeStr.isEmpty() )
        {
            this->userManipulators->showMessage("Поле маска пустое. Пожалуйста введите значение или отключите поле");
            return;
        }

        mask.isActive = true;
        mask.code = codeStr.toStdString();
    }
    else
    {
//        mask.code = ""; //SIGSEGV if empty..
        mask.code = " ";
        mask.isActive = false;
    }
    this->playBackFilterInterface.filterMaskData(mask);
    this->playBackFilterInterface.filterSessionsData(data);
}

void FilterMaskingTab::slotCancelButtonClicked()
{
    updateParams();
}

void FilterMaskingTab::setDateInterval(const uint64_t startDate, const uint64_t endDate)
{
    this->startDate = startDate;
    this->endDate = endDate;
}

void FilterMaskingTab::updateParams()
{
    SessionFilter data;
    SessionMask mask;

    data = playBackFilterInterface.getFilterData();
    mask = playBackFilterInterface.getMaskData();

    if(data.type == NONE_FILTER)
    {
        setCheckBoxFilter(false);
    }
    else if(data.type == DATE_FILTER)
    {
        setCheckBoxFilter(true);
        setRadioButtonDate(true);

        setDateStartButton(QDateTime::fromSecsSinceEpoch(data.startDate).toString("dd.MM.yyyy"));
        setDateEndButton(QDateTime::fromSecsSinceEpoch(data.endDate).toString("dd.MM.yyyy"));
    }
    else if(data.type == CHANNEL_FILTER)
    {
        setCheckBoxFilter(true);
        setRadioButtonChannel(true);

        setChannelButton(QString::fromStdString(channelsTypeVector.at(data.channelType)));
    }


    if(mask.isActive)
    {
        setCheckBoxMasking(true);
        setMaskingField(QString::fromStdString(mask.code));
    }
    else
    {
        setCheckBoxMasking(false);
        setMaskingField(QString::fromStdString(""));
    }

}

//void FilterMaskingTab::updateRecorderSettingsParams()
//{
////    this->userManipulators->showLoadingWidget();
////    updateWorker = std::async(std::launch::async, [this] () mutable
////    {
////        try
////        {
////            recorderSettingsContol->updateStatus();
////            recorderSettings = recorderSettingsContol->getRecordSettings();
////            this->userManipulators->hideLoadingWidget();
////            emit signalUpdateRecorderSettings();
////        }
////        catch (std::system_error &except)
////        {
////            this->userManipulators->hideLoadingWidget();
////            this->logger->Message(LOG_ERR, __LINE__, __func__,
////                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
////            userManipulators->showMessage(std::string("Не удалось обновить данные.\nКод ошибки: ").append(
////                    std::to_string(except.code().value())));
////        }
////    });
////    updateWorker.wait_for(std::chrono::seconds(0));
//}

//void FilterMaskingTab::slotUpdateRecorderSettings()
//{
//    setMaskingField(tr(recorderSettings.Alias));
//    if( !tr(recorderSettings.Alias).isEmpty() ) //TODO Узнать если есть флаг используем или нет
//    {
//        setCheckBoxMasking(true);
//    }
//}

//void FilterMaskingTab::saveRecorderSettingsParams()
//{


//    //Tut ne to мы должны кинуть измкенение выше родителю и он уже будет применять к старту записи.

// // memcpy(recorderSettings.SecurityCode, codeStr.toLatin1().data(), sizeof(recorderSettings.SecurityCode));

//}



