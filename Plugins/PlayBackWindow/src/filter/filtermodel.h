//
// Created by ivan on 22.02.2021.
//

#ifndef CM_INTERFACE_FILTERMODEL_H
#define CM_INTERFACE_FILTERMODEL_H

#include <cstdint>

typedef enum
{
    NONE_FILTER,
    DATE_FILTER,
    CHANNEL_FILTER
}FilterType;

typedef struct
{
    FilterType type;
    uint64_t startDate;
    uint64_t endDate;
    uint8_t channelType;
} SessionFilter;

typedef struct
{
    bool isActive;
    std::string code;
} SessionMask;

#endif //CM_INTERFACE_FILTERMODEL_H
