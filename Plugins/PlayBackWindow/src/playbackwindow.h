#ifndef PLAYBACKWINDOW_H
#define PLAYBACKWINDOW_H

#include <future>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fstream>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlProperty>

#include <axis_ifconfig.h>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <errormessageformat.h>
#include <dataprovider/recordersettingscontol.h>
#include <dataprovider/recorderstatuscontrol.h>
#include <dataprovider/dataprovidererror.h>
#include <dataprovider/idatawaiter.h>

#include "sessions/playbacksessions.h"
#include "graph/playbackgraph.h"
#include "filter/filtermaskingtab.h"
#include "copy/copywindow.h"

#include "src/database/iaudiopkdworker.h"
#include "src/database/audiopkdworker.h"
#include "src/sessions/iplaybacksessions.h"
#include "graph/iplaybackgraph.h"
#include "filter/filtermodel.h"
#include "copy/iplaybackcopy.h"
#include "process/copyprocesswidget.h"
#include "parser/playbackparser.h"
#include "parser/iplaybackparser.h"
#include "process/playbackprocess.h"
#include "process/iplaybackprocess.h"


//#define DEBUG_NO_ADAPTER
//#define DEBUG_TEST
//#define DEBUG_RECORDED_BYTES 111935224//104857600
#define DEBUG_RECORDED_BYTES 187200512  // ON DEVICE
#define DEBUG_CAPACITY 234881024
//#define DEBUG_CAPACITY 62646124544 // ON DEVICE


#define WINDOW_IDENTIFICATOR "PlayBackWindow"
#define WINDOW_FOLDER_NAME "Recorder"
#define ICON_PRESENTATION_NAME "Плеер"
#define FOLDER_PRESENTATION_NAME "Накопитель"
#define PLAYBACK_WINDOW_TITLE "Воспроизведение"


#define NETWORK_PARAMETERS_WINDOW_QML_PATH "qrc:/PlayBackWindow/PlayBackWindow.qml"

typedef enum
{
    SESSION_SECTION,
    GRAPH_SECTION,
    FILTER_SECTION
} CurrentSection;

typedef enum
{
    FILTER_BUTTON,
    GRAPH_BUTTON
} SqareButton;

class PlayBackWindow : public QObject,
                        public IEmptyPlaceWindow,
                        public IAudioPKDWorker,
                        public IPlayBackSessions,
                        public IPlayBackGraph,
                        public IPlayBackFilter,
                        public IPlayBackCopy,
                        public IDataWaiter,
                        public IPlayBackParser,
                        public IPlayBackProcess
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "PlayBackWindow")
public:
    explicit PlayBackWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    virtual std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector();
    bool prepareToDelete() override;

    ~PlayBackWindow();
protected:
    //IAudioPKDWorker
    void pkdFileParseFinished(bool isError, const std::string &msg) override;
    void pkdFileParseProgress(int procent) override;
    //IPlayBackSessions
    bool sessionSelectedStateChanged(SessionsModelNS::SessionData sessionData, bool state) override;
    void playSelectedSessions(const QList<SessionsModelNS::SessionData> &listData) override;
    void updateSessionListFinished() override;
    void showGraph() override;
    void showCopy() override;
    void tapMenuPlay() override;

    //IPlayBackGraph
    bool seekAudioToPosFromGraph(int sessionID, uint64_t seekSec) override;
    //IPlayBackFilter
    bool filterSessionsData(SessionFilter filterData) override;
    bool filterMaskData(SessionMask maskData) override;
    SessionFilter getFilterData() override;
    SessionMask getMaskData() override;
    //IPlayBackCopy
    bool getAllSessions(std::vector<SessionsModelNS::SessionData> &sessions) override;
    bool getSelectedSessions(std::vector<SessionsModelNS::SessionData> &sessions) override;
    void copyProcessData(int percent, uint32_t timeLeft, int speed) override;
    void copyStarted() override;
    //IDataWaiter
    void recieveData(int command, std::vector<uint8_t> data, size_t size) override;

    //IPlayBackParser
    void setSectionLoadingScreen(bool isShown) override;
    void updateSessionsList(size_t recordedSize,
                            size_t recordedCapacity) override;

    //IPlayBackProcess
    void currentPlayTimeChanged(int sessionID, uint64_t currentPlayTime) override;


private:
    void initAdapterSettingsControl();
    void initRecorderSettingsControl();
    void initSessionsSection();
    void initGraphSection();
    void initFilterSection();
    void initCopySection();
    void initAudioPKDWorker();
    void initRecorderStatusControl();
    void initPlayBackParser();
    void initCopyProcessWidget();
    void initPlayBackProcess();
    void initPlayBackProcessWidget();
    void audioPlayStopped();

    void audioPlayThreadFunction();

    void getInfoFromAdapter();
    void updatePKDFileData(bool repeatOnFail);


    void getRecorderSettingsData();

    int checkSessionInList(int sessionId, bool isAppend);

    std::string getWindowTitle();

    bool getSessionsDateInterval(uint64_t &startDate, uint64_t &endDate);
    bool checkAnySessionSelected();

    //Check for db
    void updateErasureStatus();

    //UI
    void setHighligthButton(int button, bool activeState);
    void setBackgroundTitle(std::string newtitle);

    //Blocking funcs
    void showLoadingScreen(bool isShow);
    void showBlockScreen(bool isShow);
    void enableSqareButtons(bool isEnable);

    void erasureStatusChecker();



    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<IStateWidget> stateWidget;
    SoftAdapterSettingsStruct_t adapterSettings;
    std::future<void> updateWorker;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<PlayBackSessions> sessionsSection;
    std::shared_ptr<PlayBackGraph> graphSection;
    std::shared_ptr<FilterMaskingTab> filterSection;
    std::shared_ptr<CopyWindow> copySection;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusControl;
    std::shared_ptr<RecorderSettingsContol> recorderSettingsControl;
    std::shared_ptr<PlayBackParser> playBackParser{nullptr};
    std::shared_ptr<PlayBackProcess> playBackProcess{nullptr};

    CurrentSection currentSectionShown;
    std::shared_ptr<AudioPKDWorker> audioPKDWorker;
    std::list<std::shared_ptr<IProcessWidget>> processWidgets;
    SoftRecorderStatusStruct_t recordStatus;
    SoftRecorderSettingsStruct_t recordSettings;


    bool threadWork;
    bool conditionWait;
    bool audioThreadWork;
    std::thread playThread;
//    std::condition_variable cv;
//    std::mutex cv_m;
    std::mutex dataMutex;

    SessionFilter filterData;
    SessionMask maskData;

    std::shared_ptr<CopyProcessWidget> copyProcessWidget;
    bool blockScreen;
    bool initCheck;

    int64_t currentPlaySessionId{-1};

    bool isMainWindowVisible{false};

    std::mutex recorderDataUpdate;

    bool waitForStartUpdatePkd{false};
    bool repeatAllParseOnFail{false};

    bool eraseIsRunning{false};

//    bool isPkdUpdating{false};

signals:
    void signalRecivedAdapterData();
    void signalStartTimer();
    void signalParseFileFinished(bool isError, std::string error);
    void signalParseFileProgress(int procent);
    void signalRecorderStatusUpdated();
    void signalRecorderSettingsUpdated();
    void signalErasureStatusUpdated();

public slots:
    void slotShowGraphClicked();

private slots:
    void slotShowFilterClicked();
    void slotShowCopyClicked();
    void slotTimerTimeout();
    void slotStartTimer();
    void slotParseFileFinished(bool isError, std::string error);
    void slotParseFileProgress(int procent);
    void slotRecorderStatusUpdated();
    void slotRecorderSettingsUpdated();
    void slotErasureStatusUpdated();
    void slotUpdateDatabase();
};

QT_BEGIN_NAMESPACE

#define PlayBackWindowInterface "PlayBackWindow"

Q_DECLARE_INTERFACE(PlayBackWindow, PlayBackWindowInterface)

QT_END_NAMESPACE

#endif // PLAYBACKWINDOW_H
