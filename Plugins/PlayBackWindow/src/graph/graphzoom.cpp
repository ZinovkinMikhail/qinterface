//
// Created by ivan on 27.01.2021.
//
#include <iostream>

#include "graphzoom.h"

void GraphZoom::insertPeak(int peak, uint64_t time, bool id)
{
    if(id)
    {
        lastId = true;
    }

    if ( averageCount == zoomCoef )
    {
        ChannelPkdLite pkd;
        pkd.peak = averageSumm / averageCount;

        if( time != 0 )
        {
            pkd.endTime = time;
            lastTime = time;
        }
        else //Это сделано чтобы у последней записалось последнее время
        {
            pkd.endTime = lastTime;
        }
        pkd.id = lastId;
        lastId = false;
        pages.push_back(pkd);
        if( pkd.id )
        {
            separatorPos.push_back(pages.size());
        }
        averageCount = 0;
        averageSumm = 0;
    }
    else if( peak == 0 && time == 0 && averageCount > 0 )
    {
        ChannelPkdLite pkd;

        pkd.peak = averageSumm / averageCount;
        pkd.endTime = lastTime;

        pkd.id = lastId;
        lastId = false;
        pages.push_back(pkd);
        if( pkd.id )
        {
            separatorPos.push_back(pages.size());
        }
        averageCount = 0;
        averageSumm = 0;
        return;
    }
    else if( peak == 0 && time == 0 )
    {
        ChannelPkdLite pkd;

        pkd.peak = averageSumm;
        pkd.endTime = lastTime;
        pkd.id = lastId;
        lastId = false;
        pages.push_back(pkd);
        if( pkd.id )
        {
            separatorPos.push_back(pages.size());
        }
        averageCount = 0;
        averageSumm = 0;
        return;
    }
    averageCount++;
    averageSumm += peak;
}
