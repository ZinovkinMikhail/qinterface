//
// Created by ivan on 23.10.2020.
//

#include <cassert>

#include <QQmlProperty>

#include "playbackgraph.h"

PlayBackGraph::PlayBackGraph(std::shared_ptr<IUserManipulators> userManipulators,
                             std::shared_ptr<IQmlEngine> qmlEngine,
                             std::shared_ptr<rosslog::Log> log,
                             std::shared_ptr<AudioPKDWorker> pkdWorker,
                             IPlayBackGraph &playBackGraphInterface,
                             QObject *parent) :
        QObject(parent),
        qmlEngine(qmlEngine),
        logger(log),
        currentItem(nullptr),
        pkdWorker(pkdWorker),
        playBackGraphInterface(playBackGraphInterface),
        userManipulators(userManipulators),
        isGraphShown(false)
{
    assert(qmlEngine);
    assert(logger);
    assert(parent);
    assert(this->pkdWorker);
    threadWork = true;
    conditionWork = false;

    memset(&graphWorkData, 0, sizeof(GaphWorkData));
    memset(&playProcessData, 0, sizeof(GraphPlayProcessData));
    memset(&graphDataForSeek, 0, sizeof(GraphDataForSeek));
    graphWorkData.prevZoom = 0;
    graphWorkData.currentZoom = 0;
    playProcessData.currentSession = -1;
    playProcessData.isSliderPressed = false;
    pointsPerPage = POINTS_PER_PAGE;
    sliderResetId = -1;
}

PlayBackGraph::~PlayBackGraph()
{
    threadWork = false;
    if(graphThread.joinable())
        graphThread.join();
}

void PlayBackGraph::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(
                qmlEngine->createComponent(GRAPH_QML_PARH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

        graphItem = currentItem->findChild<QQuickItem*>("GraphItem");
        assert(graphItem);

        connect(currentItem, SIGNAL(graphDrag(QVariant, QVariant, QVariant)),
                this, SLOT(slotGraphDrag(QVariant, QVariant, QVariant)));
        connect(currentItem, SIGNAL(graphZoom(QVariant, QVariant, QVariant)),
                        this, SLOT(slotGraphZoom(QVariant, QVariant, QVariant)));
        connect(currentItem, SIGNAL(sliderSeekTo(QVariant)),
                this, SLOT(slotSliderSeekTo(QVariant)));
        connect(currentItem, SIGNAL(sliderSeekPress(QVariant)),
                this, SLOT(slotSliderSeekPress(QVariant)));
        connect(this, SIGNAL(signalDrawPoints(QVariant, QVariant, QVariant, QVariant, QVariant)),
                this, SLOT(slotDrawPoints(QVariant, QVariant, QVariant, QVariant, QVariant)));

        connect(this, SIGNAL(signalChangeCurrentPageTime(QVariant, QVariant)),
                this, SLOT(slotChangeCurrentPageTime(QVariant, QVariant)));
        connect(this, SIGNAL(signalChangeMaxZoom(QVariant)),
                this, SLOT(slotChangeMaxZoom(QVariant)));

        connect(currentItem, SIGNAL(sliderSeekValueChanged(QVariant, QVariant)),
                this, SLOT(slotSliderSeekValueChanged(QVariant, QVariant)));

        connect(this, SIGNAL(signalMoveSliders(QVariant, QVariant,QVariant)),
                this, SLOT(slotMoveSliders(QVariant, QVariant,QVariant)));


        connect(this, SIGNAL(signalHighlightCurrentSession(QVariant, QVariant)),
                this, SLOT(slotHighlightCurrentSession(QVariant, QVariant)));

        sessionsPages.resize(6);

        graphThread = std::thread(&PlayBackGraph::threadFunction, this);
    }
}

void PlayBackGraph::show()
{
    QQmlProperty(currentItem, "visible").write(true);
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    memset(&graphWorkData, 0, sizeof(GaphWorkData));
//    this->userManipulators->showLoadingWidget();
    showLoadingScreen(true);    
    graphWorkData.state = GRAPH_REFILL;
    isGraphShown = true;
    isDataToSeek = false;
}

void PlayBackGraph::hide()
{
    isGraphShown = false;
    QQmlProperty(currentItem, "visible").write(false);
}

void PlayBackGraph::sessionSelectedStateChanged(SessionsModelNS::SessionData sessionData, bool state)
{
    std::cerr << "--------PlayBackGraph::sessionSelectedStateChanged--------" << std::endl;
    auto it = sessions.find(sessionData.sessionID);
    std::cerr << "sessionData.sessionID = " << sessionData.sessionID << std::endl;
    if( it != sessions.end() )
    {
        sessions[sessionData.sessionID].isSelected = state;
    }
    else
    {
        if( state )
        {
            SessionGraph graph;
            graph.isSelected = state;
            graph.sessionID = sessionData.sessionID;
            graph.startTime = sessionData.startTime;
            graph.endTime = sessionData.endTime;
            sessions[sessionData.sessionID] = graph;
        }
    }
}

void PlayBackGraph::threadFunction()
{
    while(threadWork)
    {
        if( graphWorkData.state == GRAPH_IDLE )
        {
            usleep(100);
            continue;
        }

        if( graphWorkData.state == GRAPH_REFILL )
        {
            refillGraph();
        }

        QList<int> listValues;
        QVariantList varLIst;
        QVariantList nextSessionPoints;
        int pointCounter = 0;

        pointsPerPage = 30000;
        int totalPosible = 30000 * 1;

        int coef = ceil(graphWorkData.totalPKDSize / (float)totalPosible);


        int prevCoef = ceil(graphWorkData.totalPKDSize / (float)totalPosible);

        int prevCoefByZoom = static_cast<int>(round((100 - graphWorkData.prevZoom) * prevCoef / 100.0));
        if( prevCoefByZoom <= 1 )
            prevCoefByZoom = 1;

        int prevPkdsCount = ceil(graphWorkData.totalPKDSize / prevCoefByZoom);


        if( graphWorkData.currentZoom == 1 )
        {
            if( coef > 0 && coef <= 1 )
            {
                coef = 2;
            }
        }
        else
        {
            int coefByZoom = static_cast<int>(round((100 - graphWorkData.currentZoom) * coef / 100.0));
            coef = coefByZoom;
            if( coef <= 1 )
            {
                coef = 1;
            }
        }

        if( graphWorkData.state == GRAPH_MOVE )
        {
            if( graphWorkData.currentZoom <= 1 )
            {
                graphWorkData.currentPos = 0;
                graphWorkData.movePercent = 0;
                showLoadingScreen(false);
                graphWorkData.state = GRAPH_IDLE;
                continue;
            }

            std::cerr << "MOVE -------------------------------------------------" << std::endl;

            int currentCoef = ceil(graphWorkData.totalPKDSize / 30000.0);
            float currentCoef2 = graphWorkData.totalPKDSize / 30000.0;
            prevCoef = ceil(graphWorkData.totalPKDSize / 30000.0);
            int totalPKDSCur = static_cast<int>(graphWorkData.totalPKDSize / currentCoef);
            int totalPKDSPrev = static_cast<int>(graphWorkData.totalPKDSize / prevCoef);
            int totalPkds = ceil(graphWorkData.totalPKDSize / currentCoef );

            if( graphWorkData.currentZoom > 0 )
            {
                totalPKDSCur = static_cast<int>(floor((graphWorkData.currentZoom / 5) * 7500) + 30000);
                totalPKDSPrev = static_cast<int>(floor((graphWorkData.prevZoom / 5) * 7500) + 30000);

                totalPkds = ceil(graphWorkData.totalPKDSize / static_cast<float>(totalPKDSCur));
                prevCoef = ceil(graphWorkData.totalPKDSize / static_cast<float>(totalPKDSPrev));
            }


            prevCoefByZoom = ceil(static_cast<float>((100 - graphWorkData.prevZoom)) * static_cast<float>(prevCoef) / 100.0);
            float prevCoefByZoom2 = static_cast<float>(graphWorkData.currentZoom) * graphWorkData.totalPKDSize / static_cast<float>(totalPKDSCur) / 100.0;



            coef = ceil(static_cast<float>((100 - graphWorkData.currentZoom)) * static_cast<float>(totalPkds) / 100.0 );

            if( coef == 1 || coef == 0 )
            {
                int prevSS = 100;
                for(auto it: zoomPages )
                {
                    if( prevSS >  it.second.zoomCoef)
                    {
                        if( it.second.zoomCoef > 0 )
                        {
                            prevSS = it.second.zoomCoef;
                        }
                    }
                }
                coef = prevSS;
                prevCoef = prevSS;
                totalPkds = totalPKDSPrev;
//                userManipulators->hideLoadingWidget();
//                graphWorkData.state = GRAPH_IDLE;
//                continue;
            }

            int movePoints = static_cast<int>(graphWorkData.movePercent * static_cast<float>(graphWorkData.pointsPerPage) / 100.0);


            prevCoef = ceil(graphWorkData.totalPKDSize / 30000.0);

            if( graphWorkData.currentZoom > 0 )
            {
                totalPKDSCur = static_cast<int>(floor((graphWorkData.currentZoom / 5) * 7500) + 30000);
                totalPkds = ceil(graphWorkData.totalPKDSize / (float)totalPKDSCur );
            }

            int prevPKDS = graphWorkData.pointsPerPage;

            if( totalPKDSCur < graphWorkData.pointsPerPage )
            {
                graphWorkData.pointsPerPage = totalPKDSCur;
            }

            if( graphWorkData.currentPos < 0 )
                graphWorkData.currentPos = 0;

            graphWorkData.prevPos = graphWorkData.currentPos;

            if( graphWorkData.isMoveRight )
            {
                if( graphWorkData.currentPos + movePoints > ceil(graphWorkData.totalPKDSize / coef ) - graphWorkData.pointsPerPage )
                {
                    graphWorkData.currentPos = static_cast<int>(ceil(graphWorkData.totalPKDSize / coef ) - graphWorkData.pointsPerPage);
                }
                else
                    graphWorkData.currentPos = graphWorkData.currentPos + movePoints;
            }
            else
            {
                graphWorkData.currentPos = graphWorkData.currentPos - movePoints;

                if( graphWorkData.currentPos - movePoints < 0 )
                {
                    graphWorkData.currentPos = 0;
                }
            }


            if( graphWorkData.currentPos < 0 )
                graphWorkData.currentPos = 0;


            if( graphWorkData.currentZoom == 0 )
            {
                graphWorkData.currentPos = 0;
            }

            varLIst.clear();
            nextSessionPoints.clear();
            pointCounter = 0;

            auto search = zoomPages.find(coef);
            if (search == zoomPages.end())
            {
                for(auto it: zoomPages )
                {
                    coef = it.second.zoomCoef;
                }
                totalPkds = totalPKDSPrev;
//                graphWorkData.state = GRAPH_IDLE;
//                this->userManipulators->hideLoadingWidget();
//                continue;
            }

            int coefByZoom = coef;

            graphWorkData.isStartZoom = false;



            GraphZoom &graphZ = zoomPages[coefByZoom];

            if( graphWorkData.currentPos < 0 )
                graphWorkData.currentPos = 0;
            if( graphWorkData.currentPos > graphZ.pages.size() )
                graphWorkData.currentPos = static_cast<int>(graphZ.pages.size() - graphWorkData.pointsPerPage - 1);

            std::cerr << "DRAW MOVE FROM " << graphWorkData.currentPos<< " TILL " <<  graphWorkData.currentPos + graphWorkData.pointsPerPage <<
                      " ALL = " << graphZ.pages.size() << " COEF " << coefByZoom << " ZOOM " << graphWorkData.currentZoom <<  std::endl;

            uint64_t startPageTime = 0, endPageTime = 0;



            uint32_t sliderPoint = static_cast<uint32_t>(static_cast<float>(graphWorkData.sliderProc) *
                                                        graphZ.pages.size()/ 100.0);


            uint32_t pointsMaxPos = graphWorkData.currentPos + graphWorkData.pointsPerPage - graphWorkData.currentPos;

            if( graphZ.pages.size() < graphWorkData.pointsPerPage )
            {
                pointsMaxPos = graphZ.pages.size();
            }
            uint32_t sliderPointByMax = 0;

            if( graphWorkData.currentPos < sliderPoint  )
            {
                if((sliderPoint - graphWorkData.currentPos) <  pointsMaxPos)
                    sliderPointByMax = sliderPoint - graphWorkData.currentPos;
            }

            float sliderProc = static_cast<float>(sliderPointByMax) * 100.0 / static_cast<float>(pointsMaxPos);

            int pos = sliderProc * GRAPH_SIZE / 100.0 ;

            if( sliderPoint < graphWorkData.currentPos )
            {
                emit signalMoveSliders(pos, sliderProc, false);
            }
            else if ( sliderPoint > graphWorkData.currentPos + pointsMaxPos )
                    emit signalMoveSliders(pos, 100, false);
            else
                    emit signalMoveSliders(pos, sliderProc, true);

//            std::cerr << "sliderPoint = " << sliderPoint << std::endl;
//            std::cerr << "SLIDER PROC = " << sliderProc << std::endl;
//            std::cerr << "pointsMaxPos = " << pointsMaxPos << std::endl;
//            std::cerr << "sliderPointByMax = " << sliderPointByMax << std::endl;
//
//
//
//            std::cerr << "pos = " << pos << std::endl;


            for( int i = graphWorkData.currentPos; i < graphWorkData.currentPos + graphWorkData.pointsPerPage; i++ )
            {
                if( i > graphZ.pages.size() - 1 )
                {
                    break;
                }
                ChannelPkdLite &lite = graphZ.pages.at(i);

                if( i == graphWorkData.currentPos )
                {
                    startPageTime = lite.endTime;
                }

                if( lite.endTime > 0 )
                {
                    endPageTime = lite.endTime;
                }

                varLIst.push_back(QVariant::fromValue((int)lite.peak));

                pointCounter++;
                if(lite.id)
                {
                    nextSessionPoints.push_back(QVariant::fromValue(pointCounter));
                }
            }


            auto scroll = calculateScrollPos(graphZ.pages.size(), graphWorkData.currentPos);
            currentStartPageTime = startPageTime;
            currentEndPageTime = endPageTime;

            emit signalChangeCurrentPageTime(QVariant::fromValue(QDateTime::fromSecsSinceEpoch(startPageTime).toString("hh:mm:ss dd:MM:yyyy")),
                                             QVariant::fromValue(QDateTime::fromSecsSinceEpoch(endPageTime).toString("hh:mm:ss dd:MM:yyyy")));
            emit signalDrawPoints(QVariant::fromValue(varLIst),
                                  QVariant::fromValue(scroll.first),
                                  QVariant::fromValue(scroll.second),
                                  QVariant::fromValue(graphWorkData.currentZoom),
                                  QVariant::fromValue(nextSessionPoints));


            float pointProc = 0;
            float pointEndProc = 0;


            auto ret = getCurrentSessionHighlight(graphZ, sliderPoint, graphWorkData.currentPos, graphWorkData.pointsPerPage);

            pointProc = ret.first;
            pointEndProc = ret.second;

            emit signalHighlightCurrentSession(pointProc, pointEndProc);

        }
        else if( graphWorkData.state == GRAPH_ZOOM )
        {
            std::cerr << "ZOOM ------------------------------------------------------" << std::endl;
//            lastTT = 0;
            if( graphWorkData.currentZoom >= 0 && graphWorkData.zoomMovePercent > 1  )
            {
                int currentCoef = ceil(graphWorkData.totalPKDSize / 30000.0);
                prevCoef = ceil(graphWorkData.totalPKDSize / 30000.0);
                int totalPKDSCur = static_cast<int>(graphWorkData.totalPKDSize / currentCoef);
                int totalPKDSPrev = static_cast<int>(graphWorkData.totalPKDSize / prevCoef);
                int totalPkds = ceil(graphWorkData.totalPKDSize / currentCoef );


                totalPKDSCur = static_cast<int>(floor((graphWorkData.currentZoom / 5) * 7500) + 30000);
                totalPKDSPrev = static_cast<int>(floor((graphWorkData.prevZoom / 5) * 7500) + 30000);

                totalPkds = ceil(graphWorkData.totalPKDSize / (float)totalPKDSCur );
                prevCoef = ceil(graphWorkData.totalPKDSize / (float)totalPKDSPrev);


                prevCoefByZoom = ceil(static_cast<float>((100 - graphWorkData.prevZoom)) * static_cast<float>(prevCoef) / 100.0);
                coef = ceil(static_cast<float>((100 - graphWorkData.currentZoom)) * static_cast<float>(totalPkds) / 100.0 );

                if( prevCoefByZoom <= 1 )
                    prevCoefByZoom = 1;


                auto search = zoomPages.find(coef);
                if (search == zoomPages.end() || coef == 1)
                {
                    int prevSS = 1000;
                    for(auto it: zoomPages )
                    {
                        if( prevSS >  it.second.zoomCoef)
                        {
                            if( it.second.zoomCoef > 0 )
                            {
                                prevSS = it.second.zoomCoef;
                            }
                        }
                    }
                    coef = prevSS;
                    totalPkds = prevCoef;
                    prevCoefByZoom = prevSS;
//                    graphWorkData.state = GRAPH_IDLE;
//                    this->userManipulators->hideLoadingWidget();
//                    continue;
                }

                if( prevCoefByZoom == 1 )
                    prevCoefByZoom = 2;

                //CHECK
                GraphZoom &graphZ = zoomPages[coef];

                prevPkdsCount = totalPKDSPrev;

                int prvMiddlePoint = static_cast<int>(round(static_cast<float>(graphWorkData.zoomMovePercent) *
                        static_cast<float>(graphWorkData.pointsPerPage) / 100.0));


                double prvMiddleProc = static_cast<float>((graphWorkData.currentPos + prvMiddlePoint)) * 100.0 /
                        floor(graphWorkData.totalPKDSize / prevCoefByZoom);

//                double prvMiddleProc = static_cast<float>((prvMiddlePoint)) * 100.0 /
//                        floor(graphWorkData.totalPKDSize / prevCoefByZoom);

                if( graphWorkData.currentZoom == 0 )
                    pointsPerPage = POINTS_PER_PAGE;

                int zoomLevel = 100 - graphWorkData.currentZoom;
                int zoomPointsMax = static_cast<int>(round(static_cast<float>(zoomLevel) * static_cast<float>(pointsPerPage) / 100.0));

                std::cerr << "POINTS PER PAGE !!!!!!!!! " << zoomPointsMax << std::endl;
                std::cerr << "ZOOOM LEVEL = " << graphWorkData.currentZoom << std::endl;

                if( zoomPointsMax <= 0 )
                {
//                    userManipulators->hideLoadingWidget();
                    showLoadingScreen(false);
                    graphWorkData.state = GRAPH_IDLE;
                    graphWorkData.currentZoom = 100 - (zoomLevel - 5);
                    std::cerr << "CONTINUEEEE " << std::endl;
                    continue;
                }
                pointsPerPage = zoomPointsMax;
                graphWorkData.pointsPerPage = zoomPointsMax;


                std::cerr << "ZOOO MOVE PERCENT " << graphWorkData.zoomMovePercent << std::endl;


                if( graphWorkData.zoomMovePercent > 1 )
                {
                    int ourPoint = static_cast<int>(round(static_cast<float>(prvMiddleProc) * graphZ.pages.size() / 100.0));
                    int freshMovePoints = static_cast<int>(round(static_cast<float>(graphWorkData.zoomMovePercent) *
                            static_cast<float>(graphWorkData.pointsPerPage) / 100.0));

                    int moveVV = static_cast<int>(totalPKDSCur - (zoomLevel * totalPKDSCur/ 100.0));
                    moveVV = static_cast<int>(graphWorkData.zoomMovePercent * moveVV / 100.0);

                    graphWorkData.currentPos = ourPoint - freshMovePoints;

                    uint32_t sliderPoint = static_cast<uint32_t>(static_cast<float>(graphWorkData.sliderProc) *
                                                                graphZ.pages.size()/ 100.0);

//                    std::cerr << " graphWorkData.sliderProc = " << graphWorkData.sliderProc << std::endl;
//                    std::cerr << " sliderPoint = " << sliderPoint << std::endl;
//                    std::cerr << " graphWorkData.currentPos = " << graphWorkData.currentPos << std::endl;
//                    std::cerr << " ourPoint = " << ourPoint << std::endl;


                    if( graphWorkData.currentZoom == 0 )
                    {
                        graphWorkData.currentPos = 0;
                    }

                    std::cerr << "coef" << coef << std::endl;

                    varLIst.clear();
                    nextSessionPoints.clear();
                    pointCounter = 0;

                    if( coef <= 1 )
                    {
                        int prevSS = 1000;
                        for(auto it3: zoomPages )
                        {
                            if( prevSS >  it3.second.zoomCoef)
                            {
                                if( it3.second.zoomCoef > 0 )
                                {
                                    prevSS = it3.second.zoomCoef;
                                }
                            }
                        }
                        coef = prevSS;
                        totalPkds = prevCoef;
//                        userManipulators->hideLoadingWidget();
//                        graphWorkData.state = GRAPH_IDLE;
//                        continue;
                    }

                    std::cerr << "coef" << coef << std::endl;

                    graphWorkData.isStartZoom = false;

                    for(auto it : zoomPages )
                    {
                        std::cerr << "Zoom pages = " << it.first << std::endl;
                        std::cerr << "Zoom pages size = " << it.second.pages.size() << std::endl;
                    }

                    if( graphWorkData.currentPos < 0 )
                        graphWorkData.currentPos = 0;
                    if( graphWorkData.currentPos + graphWorkData.pointsPerPage > graphZ.pages.size() )
                        graphWorkData.currentPos = graphZ.pages.size() - graphWorkData.pointsPerPage - 1;
                    if( graphWorkData.currentPos < 0 )
                        graphWorkData.currentPos = 0;

                    std::cerr << "DRAW FROM " << graphWorkData.currentPos << " TILL " <<  graphWorkData.currentPos + graphWorkData.pointsPerPage <<
                    " ALL = " << graphZ.pages.size() << " COEF " << coef << " ZOOM " << 100 - graphWorkData.currentZoom  <<
                    " zoom move = " <<  graphWorkData.zoomMovePercent << std::endl;

                    uint32_t pointsMaxPos = graphWorkData.currentPos + graphWorkData.pointsPerPage - graphWorkData.currentPos;

                    if( graphZ.pages.size() < graphWorkData.pointsPerPage )
                    {
                        pointsMaxPos = graphZ.pages.size();
                    }
                    uint32_t sliderPointByMax = 0;

                    if( graphWorkData.currentPos < sliderPoint  )
                    {
                        if((sliderPoint - graphWorkData.currentPos) <  pointsMaxPos)
                            sliderPointByMax = sliderPoint - graphWorkData.currentPos;
                    }



                    uint64_t startPageTime = 0, endPageTime = 0;
                    int lastI = 0;

                    if( graphZ.pages.empty() )
                    {
//                        this->userManipulators->hideLoadingWidget();
                        showLoadingScreen(false);
                        this->userManipulators->showMessage("Ошибка зума");
                        graphWorkData.state = GRAPH_IDLE;
                        continue;
                    }

                    bool startSearchPoint = false;

                    //TODO COPY FROM POS TO POS to varLis and get time by index
                    for( int i = graphWorkData.currentPos; i < graphWorkData.currentPos + graphWorkData.pointsPerPage; i++ )
                    {
                        if( i > graphZ.pages.size() - 1 )
                        {
                            break;
                        }

                        ChannelPkdLite &lite = graphZ.pages.at(i);
                        if( i == graphWorkData.currentPos )
                        {
                            startPageTime = lite.endTime;
                        }


                        if( lite.endTime > 0 )
                        {
                            endPageTime = lite.endTime;
                        }

                        varLIst.push_back(QVariant::fromValue((int)lite.peak));

                        pointCounter++;
                        if(lite.id)
                        {
                            nextSessionPoints.push_back(QVariant::fromValue(pointCounter));
                        }
                    }


                    float sliderProc = static_cast<float>(sliderPointByMax) * 100.0 / static_cast<float>(pointsMaxPos);

//                    std::cerr << "SLIDER PROC = " << sliderProc << std::endl;
//                    std::cerr << "sliderPoint = " << sliderPoint << std::endl;
//                    std::cerr << "graphWorkData.currentPos = " << graphWorkData.currentPos << std::endl;
//                    std::cerr << "pointsMaxPos = " << pointsMaxPos << std::endl;
//                    std::cerr << "sliderPointByMax = " << sliderPointByMax << std::endl;


                    int pos = sliderProc * GRAPH_SIZE / 100.0 ;


                    if( sliderPoint < graphWorkData.currentPos  )
                    {
                        emit signalMoveSliders(pos, sliderProc, false);
                    }
                    else if ( sliderPoint > graphWorkData.currentPos + pointsMaxPos )
                            emit signalMoveSliders(pos, 100, false);
                    else
                            emit signalMoveSliders(pos, sliderProc, true);



                    auto scroll = calculateScrollPos(graphZ.pages.size(), graphWorkData.currentPos);

                    currentStartPageTime = startPageTime;
                    currentEndPageTime = endPageTime;
                    emit signalChangeCurrentPageTime(QVariant::fromValue(QDateTime::fromSecsSinceEpoch(startPageTime).toString("hh:mm:ss dd:MM:yyyy")),
                                                     QVariant::fromValue(QDateTime::fromSecsSinceEpoch(endPageTime).toString("hh:mm:ss dd:MM:yyyy")));
                    emit signalDrawPoints(QVariant::fromValue(varLIst),
                                          QVariant::fromValue(scroll.first),
                                          QVariant::fromValue(scroll.second),
                                          QVariant::fromValue(graphWorkData.currentZoom),
                                          QVariant::fromValue(nextSessionPoints));


                    float pointProc = 0;
                    float pointEndProc = 0;


                    auto ret = getCurrentSessionHighlight(graphZ, sliderPoint, graphWorkData.currentPos, graphWorkData.pointsPerPage);

                    pointProc = ret.first;
                    pointEndProc = ret.second;

                    emit signalHighlightCurrentSession(pointProc, pointEndProc);

//                    std::cerr << "Point start proc = " << pointProc << std::endl;
//                    std::cerr << "Point end proc = " << pointEndProc << std::endl;
                }
            }
//            userManipulators->hideLoadingWidget();
            showLoadingScreen(false);
        }

        graphWorkData.state = GRAPH_IDLE;
    }
}

void PlayBackGraph::slotGraphDrag(QVariant pix, QVariant startPix, QVariant isRight)
{
    if( graphWorkData.state == GRAPH_IDLE )
    {
        int onMovePixel = 0;
        if( graphWorkData.startMovePixel != startPix.toInt() )
        {
            graphWorkData.startMovePixel = startPix.toInt();
            graphWorkData.pixelMoved = 0;
            onMovePixel = pix.toInt();
            graphWorkData.pixelMoved += onMovePixel;
            graphWorkData.isMoveRight = isRight.toBool();
        }
        else
        {
            if( graphWorkData.isMoveRight != isRight.toBool() )
            {
                return;
            }
            else
            {
                onMovePixel = pix.toInt() - graphWorkData.pixelMoved;
                graphWorkData.pixelMoved += onMovePixel;
            }

        }

        graphWorkData.movePercent = onMovePixel * 100.0 / GRAPH_SIZE;
        graphWorkData.state = GRAPH_MOVE;
        conditionWork = true;

        cv.notify_one();
    }
}

int PlayBackGraph::roundUp(int numToRound, int multiple)
{
    if(multiple == 0)
    {
        return numToRound;
    }

    int roundDown = ( (int) (numToRound) / multiple) * multiple;
    int roundUp = roundDown + multiple;
    int roundCalc = roundUp;
    return (roundCalc);
}


void PlayBackGraph::slotGraphZoom(QVariant zoom, QVariant moveProc, QVariant isReleased)
{
    if( !isReleased.toBool() )
    {
        return;
    }

    if( graphWorkData.state == GRAPH_IDLE )
    {
        int freshZoom =  roundUp(zoom.toInt(), 5) ; //Градация зума с шагом по 5


        if( (100 - freshZoom ) > 95 )
        {
            freshZoom = 5;
        }

        if( zoom.toInt() < 5 )
            freshZoom = 0;

        showLoadingScreen(true);

        graphWorkData.state = GRAPH_ZOOM;
        graphWorkData.prevZoom = graphWorkData.currentZoom;
        graphWorkData.zoomMovePercent = moveProc.toInt();

        graphWorkData.currentZoom = freshZoom;
        if( graphWorkData.currentZoom < 3 )
            graphWorkData.currentZoom = 0;
        else if( graphWorkData.currentZoom > 95 )
            graphWorkData.currentZoom = 95;

        if(graphWorkData.totalPKDSize < POINTS_PER_PAGE)
        {
            graphWorkData.currentZoom = 0;
        }

        graphWorkData.isStartZoom = true;
    }
}

void PlayBackGraph::slotDrawPoints(QVariant list, QVariant scrollSize, QVariant scrollPos, QVariant zoomProc, QVariant separatorPoints)
{
    QMetaObject::invokeMethod(this->graphItem, "updatePoints",
                              Q_ARG(QVariant, list),
                              Q_ARG(QVariant, zoomProc),
                              Q_ARG(QVariant, separatorPoints));
    QMetaObject::invokeMethod(currentItem, "updateScroll",
                              Q_ARG(QVariant, scrollSize),
                              Q_ARG(QVariant, scrollPos));
    showLoadingScreen(false);
}

void PlayBackGraph::slotChangeCurrentPageTime(QVariant startTime, QVariant endTime)
{
    QMetaObject::invokeMethod(this->currentItem, "updateTime",
                              Q_ARG(QVariant, startTime),
                              Q_ARG(QVariant, endTime));
}

void PlayBackGraph::slotChangeMaxZoom(QVariant maxZoom)
{
    QMetaObject::invokeMethod(this->currentItem, "updateMaxZoom",
                              Q_ARG(QVariant, maxZoom));
}

void PlayBackGraph::refillGraph()
{
    int countSessions = 0;
    std::cerr << "Start refill graph" << std::endl;

    playProcessData = {};

    if( !isPlayerPlay )
    {

        QMetaObject::invokeMethod(this->currentItem, "setElapsedTimeIndicatorPos",
                                  Q_ARG(QVariant, QVariant::fromValue(0)),
                                  Q_ARG(QVariant, QVariant::fromValue(true)));
        QMetaObject::invokeMethod(this->currentItem, "setSliderPos",
                                  Q_ARG(QVariant, QVariant::fromValue(0)));
    }
    for( auto &it: sessions )
    {
        if( it.second.isSelected )
        {
            if( countSessions == 0 )
            {
                if( !isPlayerPlay )
                {
                    playProcessData.currentSession = it.second.sessionID;
                    playProcessData.currentSec = it.second.startTime;
                }
            }
            SessionGraph &graph = sessions[it.first];
            graph.pkds.clear();
            graph.size = 0;

            pkdWorker->getPKDSessionSize(it.first, graph.size);
            graphWorkData.totalPKDSize += graph.size;
            graphWorkData.currentZoom = 0;
            graphWorkData.currentPos = 0;
            countSessions++;
        }
    }
    std::cerr << "Start refill graph 1" << std::endl;

    if( countSessions == 0 )
    {
        showLoadingScreen(false);
        return;
    }

    dataMutex.lock();
    zoomPages.clear();
    dataMutex.unlock();

    int coef = ceil(graphWorkData.totalPKDSize / 30000.0);
    int totalSumm = 0;
    int maxCoef = 0;
    int prevCoef = 0;
    int maxZoom = 0;
    int totalPointsPerPage = 30000.0;

    if(graphWorkData.totalPKDSize < POINTS_PER_PAGE)
    {
        totalPointsPerPage = graphWorkData.totalPKDSize;
    }

    graphWorkData.pointsPerPage = POINTS_PER_PAGE;


    int coefsCount = 0;
    for (int i = 100; i > 0; i -= 5)
    {
        int coefByZoom = ceil( i * (float)coef / 100.0);

        if( prevCoef == coefByZoom || coefByZoom < 2 )
        {
            continue;
        }

        graphWorkData.maxZoom = i;
        totalPointsPerPage += 7500;
        coef = ceil(graphWorkData.totalPKDSize / ((float)totalPointsPerPage));
        if( i == 100 )
            maxCoef = coefByZoom;

        int total = graphWorkData.totalPKDSize;
        int numPKDS = ceil((float)total / (float)coefByZoom);
        if( i == 1 )
            break;
        totalSumm += numPKDS;

        maxZoom = i;
        coefsCount++;

        prevCoef = coefByZoom;
        GraphZoom zoomPage = {};
        zoomPage.zoomCoef = coefByZoom;
        zoomPages[coefByZoom] = zoomPage;
        std::cerr << "ZOOM = " << i << " | coefByZoom " << coefByZoom << " | Num PKDS " << numPKDS << " PER PAGE " << totalPointsPerPage - 7500 << std::endl;
    }

    if( graphWorkData.maxZoom == 0 )
    {
        GraphZoom zoomPage = {};
        zoomPage.zoomCoef = 1;
        maxCoef = 1;
        zoomPages[1] = zoomPage;
    }

//    std::cerr << "Start refill graph 2" << std::endl;

//    std::cerr << "ZOOOM PAGE COUNT = " << zoomPages.size() << std::endl;

//    for(auto it : zoomPages )
//    {
//        std::cerr << "Zoom pages coef = " << it.first << std::endl;
//    }



    std::cerr << "Max coef = " << maxCoef << std::endl;
    for( auto &it: sessions ) // Грузим самый первый зум
    {
        if( it.second.isSelected )
        {
            if( !pkdWorker->getPKDByGroup( it.first,
                                           0, 0, maxCoef, zoomPages) )
            {
                std::cerr << "ABORT!!" << std::endl;
                abort();
            }
        }
    }

    std::cerr << "Start refill graph 3" << std::endl;
    uint64_t startPageTime = 0, endPageTime = 0;


    QVariantList varLIst;
    QVariantList nextSessionPoints;
    int pointCounter = 0;
    int count = 0;
    int totalSize = 0;
    uint64_t lastPkdTime = 0;
    int repeatPkdTimeCounter = 0;
    for(auto it: zoomPages.at(maxCoef).pages)
    {
        if( count == 0 )
            startPageTime = it.endTime;
        if( it.endTime > 0 )
            endPageTime = it.endTime;

        varLIst.push_back(QVariant::fromValue((int)it.peak));
        count++;

        pointCounter++;
        if(it.id)
        {
            nextSessionPoints.push_back(QVariant::fromValue(pointCounter));
        }
    }

    playProcessData.currentSec += maxCoef;

    totalSize = zoomPages.at(maxCoef).pages.size();

    currentStartPageTime = startPageTime;
    currentEndPageTime = endPageTime;

    emit signalChangeCurrentPageTime(QVariant::fromValue(QDateTime::fromSecsSinceEpoch(startPageTime).toString("hh:mm:ss dd:MM:yyyy")),
                                     QVariant::fromValue(QDateTime::fromSecsSinceEpoch(endPageTime).toString("hh:mm:ss dd:MM:yyyy")));

    emit signalChangeMaxZoom(maxZoom);

    std::cerr << "Start refill graph 4" << std::endl;
    updateWorker = std::async(std::launch::async, [this, maxCoef] () mutable
    {
        try
        {
            dataMutex.lock();
            GraphZoom &firstZoom = zoomPages.at(0);

            int peakIdCOunter = 0;
            for(auto &peak: firstZoom.peak)
            {
                if( peak.id )
                {
                    peakIdCOunter++;
                }
                for( auto &it : zoomPages )
                {
                    if( it.first == 0 || it.first == maxCoef )
                    {
                        continue;
                    }
                    it.second.insertPeak(peak.peak, peak.endTime, peak.id);
                }
            }

            std::cerr << "Peak id counter = " << peakIdCOunter << std::endl;

            for( auto &it : zoomPages )
            {
                if( it.first == 0 )
                    continue;
                it.second.insertPeak(0,0, false); //DOPUSTIM COMMENT ESLI MALO TO4EK GAVNO
            }

            for( auto &it : zoomPages )
            {
                int splitCounter = 0;
                if( it.second.pages.size() > 0 )
                {
                    for( auto it2 : it.second.pages )
                    {
                        if( it2.id )
                        {
                            splitCounter++;
                        }
                    }
                }


                if( it.first == 0 )
                {
                    continue;
                }
            }

            dataMutex.unlock();
        }
        catch (std::system_error &except)
        {
            dataMutex.unlock();
        }

//        for(auto it : zoomPages )
//        {
//            std::cerr << "Zoom pages = " << it.first << std::endl;
//            std::cerr << "Zoom pages size async = " << it.second.pages.size() << std::endl;
//        }
    });
    updateWorker.wait_for(std::chrono::seconds(0));



    auto scroll = calculateScrollPos(totalSize, graphWorkData.currentPos);


    std::cerr << "Var list size = " << varLIst.size() << std::endl;
    emit signalDrawPoints(QVariant::fromValue(varLIst),
                          QVariant::fromValue(scroll.first),
                          QVariant::fromValue(scroll.second),
                          QVariant::fromValue(100),
                          QVariant::fromValue(nextSessionPoints));



    graphWorkData.state = GRAPH_IDLE;
}

std::pair<int, int> PlayBackGraph::calculateScrollPos(int totalPkdSize, int curPos)
{
    double scrollProc = graphWorkData.pointsPerPage * GRAPH_SIZE / totalPkdSize;

    if( scrollProc < 2 )
    {
        scrollProc = 2;
    }
    if( scrollProc > GRAPH_SIZE )
        scrollProc = GRAPH_SIZE;

    double scrollPosProc = curPos * 100.0 / totalPkdSize;
    double scrollPos = scrollPosProc * GRAPH_SIZE / 100;

    return std::make_pair(scrollProc, scrollPos);
}

void PlayBackGraph::currentPlayTimeChanged(int sessionID, uint64_t currentPlayTime)
{
    if(currentPlayTime != 0)
        sliderResetId = -1;

    if( !isGraphShown || graphWorkData.state != GRAPH_IDLE || playProcessData.isSliderPressed )
        return;

    //При стопе повторном мы переходим на 1 сессию, нам нужно сбросить слайдер повторно до 0
    bool seekSliderOnStop = false;
    if( playProcessData.currentSec == 0 && !isPlayerPlay && playProcessData.currentSession != sessionID )
    {
        seekSliderOnStop = true;
    }

    if( !isPlayerPlay && playProcessData.currentSec == 0 && playProcessData.currentSession == sessionID )
    {
        std::cerr << "Playback graph player not play " << sessionID << " time = " << currentPlayTime << std::endl;
        return;
    }

    std::cerr << "Change current play time changed !!!!!!!!!!!!!!!!!!!!!!!!!! " << std::endl;
    static bool seekSend = false;
    static uint64_t lastPlayTime = 0;

    if( isDataToSeek )
    {
        try
        {
            isDataToSeek = false;

            if( graphDataForSeek.sessionID >= 0 )
            {

                this->playBackGraphInterface.seekAudioToPosFromGraph(
                        graphDataForSeek.sessionID,  graphDataForSeek.sessionSec);
                seekSend = true;
            }
            else
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "We do not have such session id for seek slider");
            }

        }
        catch (std::exception &except)
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Invalid session id for slider seek");
        }

        isDataToSeek = false;
        return;
    }


    if( seekSend )
    {
        if( sessionID != graphDataForSeek.sessionID )
            return;
        std::cerr << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
        seekSend = false;
    }

    lastPlayTime = currentPlayTime;
    playProcessData.currentSec = currentPlayTime;

    std::cerr << "==== currentPlayTimeChanged: sessionID = " << sessionID << " currentPlayTime = " << currentPlayTime << std::endl;

    //Расчитываем не по секундам, тк у нас могут сессии не идти по порядку и даты сильно прыгать, для этого идет расчет по
    //позиции пкд + сек и все это зависит от коэф зума

    std::cerr << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! current zoom = " << graphWorkData.currentZoom << std::endl;

    if( playProcessData.currentSession != sessionID  || playProcessData.currentZoom != graphWorkData.currentZoom )
    {
        //TODO убрать по финалу
//        std::cerr << "==== currentPlayTimeChanged STAGE 2 ====" << std::endl;
//        std::cerr << "sessionID =" << sessionID << std::endl;
//        std::cerr << "playProcessData.currentSession =" << playProcessData.currentSession << std::endl;
//        std::cerr << "playProcessData.currentZoom =" << playProcessData.currentZoom << std::endl;
//        std::cerr << "graphWorkData.currentZoom =" << graphWorkData.currentZoom << std::endl;
//        std::cerr << "graphWorkData.currentPos =" << graphWorkData.currentPos << std::endl;


        if(currentPlayTime == 0)
        {
            std::cerr << "currentPlayTime == 0 return!" << std::endl;
            //return;
        }

        uint64_t sessionsPadding = 0;

        SessionGraph currPage;

        //Считаем падинг до нашей сессии в пкд
        for( auto it: sessions )
        {
            if( it.second.isSelected )
            {
                if( it.second.sessionID < sessionID )
                    sessionsPadding += it.second.size;

                if( it.second.sessionID == sessionID )
                {
                    currPage = it.second;
                }
            }
        }

        //Мы знаем падинг до сессиии а размер = секунды.

        //Нам нужен коэф относительно зума
        int totalPointsPerPage = 30000.0;

        if( graphWorkData.totalPKDSize < POINTS_PER_PAGE )
        {
            totalPointsPerPage = graphWorkData.totalPKDSize;
        }

        if( graphWorkData.currentZoom > 0 )
            totalPointsPerPage += 7500 * ((graphWorkData.currentZoom ) / 5 );

//        int coef = ceil(graphWorkData.totalPKDSize / ((float)totalPointsPerPage));
        float coef = graphWorkData.totalPKDSize / ((float)totalPointsPerPage);

        if(graphWorkData.currentZoom != 0) //if not min zoom we need work with integer
        {
            coef = ceil(coef);
        }

        int coefByZoom = ceil( ( 100 - graphWorkData.currentZoom ) * (float)coef / 100.0);
        //float coefByZoom =  ( 100 - graphWorkData.currentZoom ) * (float)coef / 100.0;

        if(graphWorkData.currentZoom != 0) //if not min zoom we need work with integer
        {
            coefByZoom = ceil(coefByZoom);
        }

        if( coefByZoom <= 1 && graphWorkData.totalPKDSize > POINTS_PER_PAGE)
            coefByZoom = 2;


        playProcessData.coefByZoom = coefByZoom;

        if(graphWorkData.totalPKDSize < POINTS_PER_PAGE)
        {
            coefByZoom = 2;
            playProcessData.coefByZoom = 2;
        }

        int diffOurSession = currentPlayTime - currPage.startTime;

        if(currentPlayTime == 0)
        {
            diffOurSession = 0;
        }


        uint32_t ourCurPos = floor((double)sessionsPadding / (double)coefByZoom) + floor(diffOurSession / coefByZoom);

        std::cerr << "New pos = " << ourCurPos << std::endl;
        int curPKDSEnd = graphWorkData.currentPos + graphWorkData.pointsPerPage;
        std::cerr << "curPKDSEnd " << curPKDSEnd << std::endl;
        std::cerr << "graphWorkData.currentPos " << graphWorkData.currentPos << std::endl;

        {
            int32_t startSessionPos = floor((double)sessionsPadding / (double)coefByZoom);

            graphWorkData.currentPos = startSessionPos +
                    floor(diffOurSession / coefByZoom) - graphWorkData.pointsPerPage / 2;

            if( (graphWorkData.currentPos + graphWorkData.pointsPerPage) < startSessionPos || graphWorkData.currentPos < 0  )
            {
                graphWorkData.currentPos = startSessionPos;
            }
            //TODO убрать по финалу
//            std::cerr << "!!!!!!!!!!!! Lim = " << (graphWorkData.currentPos + graphWorkData.pointsPerPage) << std::endl;
//            std::cerr << "!!!!!!!!!!!! DIFF OUR SESSION  = " << floor(diffOurSession / coefByZoom) << std::endl;
//            std::cerr << "!!!!!!!!!!!! graphWorkData.pointsPerPage / 2 = " << graphWorkData.pointsPerPage / 2 << std::endl;
//            std::cerr << "!!!!!!!!!!!! startSessionPos = " << startSessionPos << std::endl;
//            std::cerr << "!!!!!!!!!!!! CUR POS = " << graphWorkData.currentPos << std::endl;
//            std::cerr << "!!!!!!!!!!!! DIFF = " << floor(diffOurSession / coefByZoom) << std::endl;
//            std::cerr << "!!!!!!!!!!!! coefByZoom = " << coefByZoom << std::endl;
//            std::cerr << "!!!!!!!!!!!! START TIME = " << currPage.startTime << std::endl;


            playProcessData.diffByOurSessions = floor(diffOurSession / coefByZoom);
            playProcessData.currentPKDSessionPadding = startSessionPos;
            playProcessData.currentZoom = graphWorkData.currentZoom;
            playProcessData.currentSession = sessionID;

            if( graphWorkData.state == GRAPH_IDLE )
            {
                graphWorkData.movePercent = 0;
                graphWorkData.state = GRAPH_MOVE;
                conditionWork = true;
            }

            //Нам нужно расчитать позицию слайдера при стопе
            if( seekSliderOnStop )
                goto seekOnStop;
        }
        return;
    }

    seekOnStop:
    if(graphWorkData.totalPKDSize < POINTS_PER_PAGE )
    {
        playProcessData.coefByZoom = 2;
    }
    //TODO убрать по финалу
//    std::cerr << "==== currentPlayTimeChanged STAGE 1 ====" << std::endl;
//    std::cerr << "playProcessData.currentSession =" << playProcessData.currentSession << std::endl;
//    std::cerr << "playProcessData.currentZoom =" << playProcessData.currentZoom << std::endl;
//    std::cerr << "graphWorkData.currentZoom =" << graphWorkData.currentZoom << std::endl;
//    std::cerr << "graphWorkData.currentPos =" << graphWorkData.currentPos << std::endl;
//    std::cerr << "playProcessData.currentPKDSessionPadding =" << playProcessData.currentPKDSessionPadding << std::endl;

    SessionGraph currPage = sessions.at(sessionID);

    uint64_t timeWithCoef = currentPlayTime +  playProcessData.coefByZoom;

    if(currentPlayTime == 0)
        timeWithCoef = currPage.startTime +  playProcessData.coefByZoom;

    std::cerr << "TIME WITH COEF = " << timeWithCoef << std::endl;

    QMetaObject::invokeMethod(this->currentItem, "setCurrentTime",
                              Q_ARG(QVariant, QVariant::fromValue(QDateTime::fromSecsSinceEpoch(currentPlayTime).toString("hh:mm:ss dd:MM:yyyy"))));

    int diffOurSession = currentPlayTime - currPage.startTime;
    if(currentPlayTime == 0)
        diffOurSession = 0;

    std::cerr << "Diff our session without delete " << diffOurSession << std::endl;
    int diffPKDS = floor((float)diffOurSession / (float)playProcessData.coefByZoom ); //Cколько пкд относительно нашей сессии начала
    int curPKDSStart = graphWorkData.currentPos;
    int curPKDSEnd = graphWorkData.currentPos + graphWorkData.pointsPerPage;

    if( graphWorkData.totalPKDSize / playProcessData.coefByZoom < POINTS_PER_PAGE && graphWorkData.currentZoom == 0 )
    {
        curPKDSEnd = graphWorkData.totalPKDSize/ playProcessData.coefByZoom ;
    }
    int ourCurPos = playProcessData.currentPKDSessionPadding + diffPKDS; //Наша позиция ПКД относительно начала сессии

    playProcessData.currentOurPos = ourCurPos;


    std::cerr << "!!!!!!!!!!!! DIFF OUR SESSION  = " << diffPKDS<< std::endl;


    std::cerr << "currentPlayTime " << currentPlayTime << std::endl;
    std::cerr << "currPage.startTime " << currPage.startTime << std::endl;

//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "Current play time  = %li", currentPlayTime);
//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "Page start time play time  = %li", currPage.startTime);
//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "ourCurPos = %i", ourCurPos);
//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "CUR POS REAL  = %i", graphWorkData.currentPos);
//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "curPKDSStart  = %i", curPKDSStart);
//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "curPKDSEnd  = %i", curPKDSEnd);
//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "diffPKDS  = %i", diffPKDS);
//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "diff by our  = %i", playProcessData.diffByOurSessions);
//    this->logger->Message(rosslog::LOG_ERR,__LINE__,  __func__, "graphWorkData.pointsPerPage  = %i", graphWorkData.pointsPerPage);


    //TODO убрать по финалу
//    std::cerr << "COEF BY ZOOM = "  << playProcessData.coefByZoom << std::endl;
//    std::cerr << "CUR POS START OUR = "  << ourCurPos << std::endl;
//    std::cerr << "CUR POS REAL = "  << graphWorkData.currentPos << std::endl;
//    std::cerr << "CUR POS START  REAL = "  << curPKDSStart << std::endl;
//    std::cerr << "CUR POS END  REAL = "  << curPKDSEnd << std::endl;
//    std::cerr << "CUR POS PKD OUR DIFF = "  << diffPKDS << std::endl;
//    std::cerr << "DIFF BY OUR SESSION stored" << playProcessData.diffByOurSessions << std::endl;
//    std::cerr << "!!!!!!!!!!!! START TIME = " << currPage.startTime << std::endl;


    if(currentPlayTime == 0)
    {
        if(sliderResetId != sessionID)
        {
            sliderResetId = sessionID;
            std::cerr << "==== reset slider ====" << std::endl;
            int diff = ourCurPos - graphWorkData.currentPos;
            int length = curPKDSEnd - graphWorkData.currentPos;
            double proc = diff * 100.0 / length;

            int xPos = proc * GRAPH_SIZE / 100.0;

            QMetaObject::invokeMethod(this->currentItem, "setElapsedTimeIndicatorPos",
                                      Q_ARG(QVariant, QVariant::fromValue(xPos)),
                                      Q_ARG(QVariant, QVariant::fromValue(true)));
            QMetaObject::invokeMethod(this->currentItem, "setSliderPos",
                                      Q_ARG(QVariant, QVariant::fromValue(proc)));

        }
    }
    else if(  ourCurPos > graphWorkData.currentPos  && ourCurPos < curPKDSEnd )
    {
        int diff = ourCurPos - graphWorkData.currentPos;
        int length = curPKDSEnd - graphWorkData.currentPos;
        double proc = diff * 100.0 / length;
        int xPos = proc * GRAPH_SIZE / 100.0;

        std::cerr << "Proc = " << proc << std::endl;

        QMetaObject::invokeMethod(this->currentItem, "setElapsedTimeIndicatorPos",
                                  Q_ARG(QVariant, QVariant::fromValue(xPos)),
                                  Q_ARG(QVariant, QVariant::fromValue(true)));
        QMetaObject::invokeMethod(this->currentItem, "setSliderPos",
                                  Q_ARG(QVariant, QVariant::fromValue(proc)));


        std::cerr << "---  Screen pos BY PKD " << xPos << std::endl;
//        graphWorkData.movePercent = 0;
//        graphWorkData.state = GRAPH_MOVE;
    }
    else if( curPKDSEnd < ourCurPos   )
    {
        int pkdsTillNext = ourCurPos - curPKDSEnd + graphWorkData.pointsPerPage;

        graphWorkData.currentPos = graphWorkData.currentPos + pkdsTillNext;

        diffOurSession = currentPlayTime - currPage.startTime;
        playProcessData.diffByOurSessions = floor(diffOurSession / playProcessData.coefByZoom);

        graphWorkData.movePercent = 0;
        graphWorkData.state = GRAPH_MOVE;
        conditionWork = true;

        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__, "PREV PAGE AUTO DRAG");
    }
    else if( graphWorkData.currentPos > ourCurPos )
    {
        int pkdsTillNext = graphWorkData.currentPos - ourCurPos + graphWorkData.pointsPerPage;
        graphWorkData.currentPos = graphWorkData.currentPos - pkdsTillNext;
        diffOurSession = currentPlayTime - currPage.startTime;
        playProcessData.diffByOurSessions = floor(diffOurSession / playProcessData.coefByZoom);

        graphWorkData.movePercent = 0;
        graphWorkData.state = GRAPH_MOVE;
        conditionWork = true;
        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__, "NEXT PAGE AUTO DRAG");
    }

}

void PlayBackGraph::setPlayerState(bool state)
{
    isPlayerPlay = state;
}

void PlayBackGraph::slotSliderSeekTo(QVariant procent)
{
}

void PlayBackGraph::slotSliderSeekValueChanged(QVariant procent, QVariant relesed)
{
    int xPos = procent.toInt() * GRAPH_SIZE / 100.0;

    int currentCoef = ceil(graphWorkData.totalPKDSize / 30000.0);
    int totalPKDSCur = static_cast<int>(graphWorkData.totalPKDSize / currentCoef);
    int totalPkds = ceil(graphWorkData.totalPKDSize / currentCoef );


    totalPKDSCur = static_cast<int>(floor((graphWorkData.currentZoom / 5) * 7500) + 30000);
    totalPkds = ceil(graphWorkData.totalPKDSize / (float)totalPKDSCur );


    int coef = ceil(static_cast<float>((100 - graphWorkData.currentZoom)) * static_cast<float>(totalPkds) / 100.0 );


    std::cerr << "While seek coef = " << coef << std::endl;
    if( coef == 1 )
    {
        coef = 2;
    }

//    if(graphWorkData.totalPKDSize < POINTS_PER_PAGE)
//    {
//        coef = 1;
//    }


    playProcessData.coefByZoom = coef;

    int totalPKDSCurReserv = graphWorkData.totalPKDSize / coef;

//    std::cerr << "Total pkds size cur = " << graphWorkData.totalPKDSize << std::endl;
//    std::cerr << "Total pkds size cur 2 = " << graphWorkData.totalPKDSize/ coef << std::endl;


//    std::cerr << "Total pkds size cur graphWorkData.pointsPerPage = " << graphWorkData.pointsPerPage << std::endl;
//    std::cerr << "Total pkds size cur graphWorkData.totalPKDSize = " << graphWorkData.totalPKDSize << std::endl;


    int currentSeekPKDS = procent.toFloat() * graphWorkData.pointsPerPage / 100.0;

    if(graphWorkData.totalPKDSize / coef < POINTS_PER_PAGE && graphWorkData.currentZoom == 0 )
    {
        currentSeekPKDS = procent.toFloat() * (graphWorkData.totalPKDSize/ coef) / 100.0;
    }
    int clearPKDSByStartSes = graphWorkData.currentPos + currentSeekPKDS;


//    if( clearPKDSByStartSes > totalPKDSCur )
//    {
//        clearPKDSByStartSes = totalPKDSCur;
//    }

    float procByPos = (float)clearPKDSByStartSes * 100.0 / totalPKDSCurReserv;



    graphWorkData.sliderProc = procByPos;



    int sessionID = -1;
    double sessionPadding = 0;
    uint64_t seekSec = 0;

    for( auto it: sessions )
    {

        if( it.second.isSelected )
        {
            double sessionSize = it.second.size / playProcessData.coefByZoom;
            if( clearPKDSByStartSes > sessionPadding  && clearPKDSByStartSes <= (sessionPadding + sessionSize))
            {

                int diff = clearPKDSByStartSes - sessionPadding;

                seekSec =  diff * playProcessData.coefByZoom;
                seekSec = seekSec + it.second.startTime;
                sessionID = it.second.sessionID;
                break;
            }

            sessionPadding = sessionPadding + sessionSize;
        }
    }


    if( relesed.toBool() && isPlayerPlay )
    {
        try
        {
            if( sessionID >= 0 )
            {
                std::cerr << "SEND PLAYER VAL !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
                this->playBackGraphInterface.seekAudioToPosFromGraph(
                        sessionID,  seekSec);
            }
            else
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "We do not have such session id for seek slider");
            }

        }
        catch (std::exception &except)
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Invalid session id for slider seek");
        }
    }
    else
    {
        if( sessionID >= 0 )
        {
            graphDataForSeek.sessionID = sessionID;
            graphDataForSeek.sessionSec = seekSec;
            isDataToSeek = true;
        }
        else
            isDataToSeek = false;
    }


    QMetaObject::invokeMethod(this->currentItem, "setCurrentTime",
                              Q_ARG(QVariant, QVariant::fromValue(QDateTime::fromSecsSinceEpoch(seekSec).toString("hh:mm:ss dd:MM:yyyy"))));

    graphWorkData.currentSecond = seekSec;
    std::cerr << "SEEK SEC = "<< seekSec << " " << procByPos << std::endl;


}

void PlayBackGraph::slotSliderSeekPress(QVariant isPressed)
{
    playProcessData.isSliderPressed = isPressed.toBool();
}

void PlayBackGraph::slotMoveSliders(QVariant pos, QVariant proc, QVariant isTimeIndicatorVisible)
{
    QMetaObject::invokeMethod(this->currentItem, "setElapsedTimeIndicatorPos",
                              Q_ARG(QVariant, QVariant::fromValue(pos)),
                              Q_ARG(QVariant, QVariant::fromValue(isTimeIndicatorVisible)));

    QMetaObject::invokeMethod(this->currentItem, "setSliderPos",
                              Q_ARG(QVariant, QVariant::fromValue(proc)));
}

void PlayBackGraph::showLoadingScreen(bool isShow)
{
    QMetaObject::invokeMethod(this->currentItem, "setBusy",
                              Q_ARG(QVariant, isShow));
}

bool PlayBackGraph::isVisible()
{
    QVariant visible = QQmlProperty(currentItem, "visible").read();
    return visible.toBool();
}

std::pair<float, float> PlayBackGraph::getCurrentSessionHighlight(const GraphZoom &graphZ, const uint32_t &sliderPoint,
                                                              const uint32_t &startPos, const uint32_t &pointsMax)
{
    float pointProc = 0;
    float pointEndProc = 0;

    int separatorIndexStart = 0;
    for( auto it: graphZ.separatorPos )
    {
        if( it > sliderPoint )
        {
            if( separatorIndexStart != 0 )
                separatorIndexStart--;
            break;
        }
        separatorIndexStart++;
    }

    //std::cerr << "Separator index = " << separatorIndexStart  << "size " <<  graphZ.separatorPos.size() << std::endl;

    try
    {
        uint32_t separatorStartPoint = graphZ.separatorPos.at(separatorIndexStart);


        if( separatorIndexStart + 1 >= graphZ.separatorPos.size() )
        {
            if( separatorStartPoint >= startPos && separatorStartPoint <= startPos + pointsMax)
            {
                uint32_t point = separatorStartPoint - startPos;
                pointProc = (float)point * 100 / (float)pointsMax;
            }
            else if( separatorStartPoint < startPos )
            {
                pointProc = 0;
            }
            else if( separatorStartPoint > startPos + pointsMax )
            {
                pointProc = 100;
            }

            pointEndProc = 100;
        }
        else
        {
            uint32_t separatorEndPoint = graphZ.separatorPos.at(separatorIndexStart + 1);

            if( separatorStartPoint >= startPos && separatorStartPoint <= startPos + pointsMax )
            {
                uint32_t point = separatorStartPoint - startPos;
                pointProc = (float)point * 100 / (float)pointsMax;
            }
            else if( separatorStartPoint < startPos )
            {
                pointProc = 0;
            }
            else if( separatorStartPoint > startPos + pointsMax )
            {
                pointProc = 100;
            }

            if( separatorEndPoint > startPos && separatorEndPoint <= startPos + pointsMax )
            {
                uint32_t pointEnd = separatorEndPoint - startPos;
                pointEndProc = (float)pointEnd * 100 / (float)pointsMax;
            }
            else if( separatorEndPoint >= startPos + pointsMax )
            {
                pointEndProc = 100;
            }
            else if( separatorEndPoint <= startPos )
            {
                pointEndProc = 0;
            }

        }

        return std::make_pair(pointProc, pointEndProc);
    }
    catch(std::exception &except)
    {
        return std::make_pair(0, 0);
    }

}

void PlayBackGraph::slotHighlightCurrentSession(QVariant proc, QVariant procEnd)
{
    QMetaObject::invokeMethod(this->graphItem, "highlightCurrentSession",
                              Q_ARG(QVariant, QVariant::fromValue(proc)),
                              Q_ARG(QVariant, QVariant::fromValue(procEnd)));
}


