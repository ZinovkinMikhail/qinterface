//
// Created by ivan on 27.01.2021.
//

#ifndef CM_INTERFACE_GRAPHZOOM_H
#define CM_INTERFACE_GRAPHZOOM_H

#include "../database/channelparamsmodel.h"

class GraphZoom
{
public:
    GraphZoom() = default;


    void insertPeak(int peak, uint64_t time, bool id);


    std::vector<ChannelPkdLite> pages;
    std::vector<ChannelPkdLite> peak;
    std::vector<size_t> separatorPos;
    int zoomCoef;
    int currentPage;

private:
    int averageCount;
    int averageSumm;
    uint64_t lastTime;
    bool lastId {false};
//    int splitCounter{0};

};


#endif //CM_INTERFACE_GRAPHZOOM_H
