//
// Created by ivan on 22.02.2021.
//

#ifndef CM_INTERFACE_IPLAYBACKGRAPH_H
#define CM_INTERFACE_IPLAYBACKGRAPH_H

#include <cstdint>

class IPlayBackGraph
{
public:
    virtual bool seekAudioToPosFromGraph(int sessionID, uint64_t seekSec) = 0;
};

#endif //CM_INTERFACE_IPLAYBACKGRAPH_H
