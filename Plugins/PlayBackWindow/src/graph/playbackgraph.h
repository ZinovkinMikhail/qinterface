//
// Created by ivan on 23.10.2020.
//

#ifndef CM_INTERFACE_PLAYBACKGRAPH_H
#define CM_INTERFACE_PLAYBACKGRAPH_H

#include <memory>
#include <string>
#include <utility>
#include <iterator>
#include <future>
#include <thread>
#include <math.h>
#include <unistd.h>
#include <utility>
#include <random>

#include <QObject>
#include <QQuickItem>
#include <QDateTime>

#include <rosslog/log.h>
#include <iqmlengine.h>

#include <iusermanipulators.h>
#include "src/database/audiopkdworker.h"
#include "../sessions/sessionsmodel.h"
#include "iplaybackgraph.h"

#define GRAPH_QML_PARH "qrc:/PlayBackWindow/PlayBackGraph.qml"

#define POINTS_PER_PAGE 30000
#define GRAPH_SIZE 1120.0

typedef struct
{
    bool isSelected;
    int sessionID;
    std::vector<ChannelPkdLite> pkds;
    size_t size;
    uint64_t startTime;
    uint64_t endTime;
} SessionGraph;



typedef enum
{
    GRAPH_IDLE,
    GRAPH_REFILL,
    GRAPH_ZOOM,
    GRAPH_MOVE
} GraphState;

typedef struct
{
    uint32_t sessionID;
    uint64_t sessionSec;
} GraphDataForSeek;

typedef struct
{
    size_t totalPKDSize;
    int prevZoom;
    int currentPos; //Page start pos
    int currentZoom;
    int totalPages;
    int currentPage;
    GraphState state;
    double movePercent;
    bool isMoveRight;
    int zoomMovePercent;
    int pointsPerPage;
    bool isStartZoom;
    int startMovePixel;
    int pixelMoved;
    int prevPos;
    int maxZoom;
    uint64_t currentSecond;
    float sliderProc;
} GaphWorkData;

typedef struct
{
    int currentZoom;
    int currentSession;
    int coefByZoom;
    int currentPKDSessionPadding;
    int diffByOurSessions;
    uint64_t currentOurPos;
    bool isSliderPressed;
    uint64_t currentSec;
} GraphPlayProcessData;

class PlayBackGraph: public QObject, public std::enable_shared_from_this<PlayBackGraph>
{
    Q_OBJECT
public:
    explicit PlayBackGraph(std::shared_ptr<IUserManipulators> userManipulators,
    std::shared_ptr<IQmlEngine> qmlEngine,
    std::shared_ptr<rosslog::Log> log,
    std::shared_ptr<AudioPKDWorker> pkdWorker,
    IPlayBackGraph &playBackGraphInterface,
    QObject *parent = nullptr);

    ~PlayBackGraph();

    void init();
    void show();
    void hide();
    bool isVisible();

    std::vector<ChannelPkdLite> litePKDTEST;

    void sessionSelectedStateChanged(SessionsModelNS::SessionData sessionData, bool state);
    void currentPlayTimeChanged(int sessionID, uint64_t currentPlayTime);
    void setPlayerState(bool state);

private:
    void threadFunction();
    int roundUp(int numToRound, int multiple);
    void refillGraph();
    void showLoadingScreen(bool isShow);
    std::pair<int, int> calculateScrollPos(int totalPkdSize, int curPos);
    std::pair<float, float> getCurrentSessionHighlight(const GraphZoom &graphZ, const uint32_t &sliderPoint,
                                                   const uint32_t &startPos, const uint32_t &pointsMax);

    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<AudioPKDWorker> pkdWorker;
    IPlayBackGraph &playBackGraphInterface;
    QQuickItem *currentItem;
    QQuickItem *graphItem{};
    std::thread graphThread;
    std::condition_variable cv;
    std::mutex cv_m;
    std::mutex dataMutex;
    std::mutex dataMutexLeft;
    bool threadWork;
    bool conditionWork;
    std::future<void> updateWorker;
    std::future<void> updateWorkerLeft;
    std::shared_ptr<IUserManipulators> userManipulators;
    GaphWorkData graphWorkData{};
    int pointsPerPage;
    int sliderResetId;
    double lastTT;
    double lastTTZ;
    bool isPlayerPlay{false};
    bool isDataToSeek{false};
    GraphDataForSeek graphDataForSeek;

    std::vector<GraphPage> sessionsPages;
    std::map<int, SessionGraph> sessions;
    std::map<int, GraphZoom> zoomPages;

    uint64_t currentStartPageTime;
    uint64_t currentEndPageTime;

    bool isGraphShown;
    GraphPlayProcessData playProcessData;

signals:
    void signalDrawPoints(QVariant list, QVariant scrollSize, QVariant scrollPos, QVariant zoomProc, QVariant separatorPoints);
    void signalChangeCurrentPageTime(QVariant startTime, QVariant endTime);
    void signalChangeMaxZoom(QVariant maxZoom);
    void signalMoveSliders(QVariant pos, QVariant proc, QVariant isTimeIndicatorVisible);
    void signalHighlightCurrentSession(QVariant proc, QVariant procEnd);

public slots:
    void slotGraphDrag(QVariant pix, QVariant startPix, QVariant isRight);
    void slotGraphZoom(QVariant zoom, QVariant moveProc, QVariant isReleased);
    void slotDrawPoints(QVariant list, QVariant scrollSize, QVariant scrollPos, QVariant zoomProc, QVariant separatorPoints);
    void slotChangeCurrentPageTime(QVariant startTime, QVariant endTime);
    void slotChangeMaxZoom(QVariant maxZoom);
    void slotSliderSeekTo(QVariant procent);
    void slotSliderSeekValueChanged(QVariant procent, QVariant relesed);
    void slotSliderSeekPress(QVariant isPressed);
    void slotMoveSliders(QVariant pos, QVariant proc, QVariant isTimeIndicatorVisible);
    void slotHighlightCurrentSession(QVariant proc, QVariant procEnd);
};


#endif //CM_INTERFACE_PLAYBACKGRAPH_H
