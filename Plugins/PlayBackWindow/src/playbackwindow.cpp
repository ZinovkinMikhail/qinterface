#include <assert.h>

#include <QQmlComponent>
#include <QTextCodec>
#include <sys/time.h>

#include "playbackwindow.h"

using namespace rosslog;
using namespace recorder_status_ctrl;
using namespace SessionsModelNS;

PlayBackWindow::PlayBackWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    dataProvider(nullptr),
    userManipulators(nullptr),
    sessionsSection(nullptr),
    graphSection(nullptr),
    filterSection(nullptr),
    copySection(nullptr),
    currentSectionShown(SESSION_SECTION),
    audioPKDWorker(nullptr),
    recorderStatusControl(nullptr),
    recorderSettingsControl(nullptr),
    copyProcessWidget(nullptr),
    conditionWait(true)
{
    qRegisterMetaType<size_t>("size_t");
    qRegisterMetaType<std::string>("std::string");
    qRegisterMetaType<std::shared_ptr<IProcessWidget>>("std::shared_ptr<IProcessWidget>");
    isMainWindowVisible = false;
    blockScreen = false;
    initCheck = false;  //for check recordStatus.PlayStatus and send stop if recorder play on start App
    maskData = {};
    memset(&filterData, 0, sizeof(SessionFilter));
//    memset(&maskData, 0, sizeof(SessionMask));

    memset(&recordSettings, 0, sizeof(SoftRecorderSettingsStruct_t));
    memset(&recordStatus, 0, sizeof(SoftRecorderStatusStruct_t));
}

std::shared_ptr<IUserManipulators> PlayBackWindow::initWindow(EWindSizeType winSizeType,
                                                                      std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                      std::shared_ptr<IStateWidget> stateWidget,
                                                                      std::shared_ptr<IUserManipulators> userManipulators,
                                                                      std::shared_ptr<IQmlEngine> engine,
                                                                      std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    Q_UNUSED(log);
    assert(engine);
    assert(dataProvider);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;
    this->stateWidget = stateWidget;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(NETWORK_PARAMETERS_WINDOW_QML_PATH);

        assert(windowComponent);
        connect(windowComponent, SIGNAL(showGraphClicked()),
                this, SLOT(slotShowGraphClicked()));
        connect(windowComponent, SIGNAL(showFilterClicked()),
                this, SLOT(slotShowFilterClicked()));
        connect(windowComponent, SIGNAL(showCopyClicked()),
                this, SLOT(slotShowCopyClicked()));

        connect(this, SIGNAL(signalStartTimer()), this, SLOT(slotStartTimer()));
        connect(this, SIGNAL(signalParseFileFinished(bool, std::string)),
                this, SLOT(slotParseFileFinished(bool, std::string)), Qt::QueuedConnection);
        connect(this, SIGNAL(signalParseFileProgress(int)),
                this, SLOT(slotParseFileProgress(int)), Qt::QueuedConnection);
        connect(this, SIGNAL(signalRecorderStatusUpdated()),
                this, SLOT(slotRecorderStatusUpdated()));
        connect(this, SIGNAL(signalErasureStatusUpdated()),
                this, SLOT(slotErasureStatusUpdated()));
        connect(windowComponent, SIGNAL(updateDatabase()),
                this, SLOT(slotUpdateDatabase()));

    }

    initCopyProcessWidget();
    initAudioPKDWorker();
    initAdapterSettingsControl();
    initSessionsSection();
    initGraphSection();
    initFilterSection();
    initCopySection();
    initRecorderStatusControl();
    initRecorderSettingsControl();
    initPlayBackParser();
    initPlayBackProcess();


    connect(copyProcessWidget.get(), SIGNAL(signalSwipeSelected(std::string, std::shared_ptr<IProcessWidget>)),
            this, SLOT(slotOnProcessWidgetSwipeSelected(std::string, std::shared_ptr<IProcessWidget>)));
    connect(copyProcessWidget.get(), SIGNAL(signalSwipeDeselected(std::string,std::shared_ptr<IProcessWidget>)),
            this, SLOT(slotOnProcessWidgetSwipeDeSelected(std::string, std::shared_ptr<IProcessWidget>)));

//    updateErasureStatus();
#ifdef DEBUG_NO_ADAPTER
    //!!Add ifdef DEBUG
    this->playBackParser->parsePKDData(repeatAllParseOnFail);
    //!!
#else
    updatePKDFileData(true);
#endif

    threadWork = true;
    audioThreadWork = true; //Будет работать при воспроизведении

    playThread = std::thread(&PlayBackWindow::audioPlayThreadFunction, this);

    stateWidget->addState("DATABASE_WORK_PROGRESS", PROCESS, QPixmap(":/icons/database.png"), false);
    stateWidget->addState("COPY_WORK_PROGRESS", PROCESS, QPixmap(":/icons/i1.png"), false);

    return nullptr;
}

QObject *PlayBackWindow::getComponentObject()
{
    return windowComponent;
}

std::string PlayBackWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string PlayBackWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string PlayBackWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string PlayBackWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string PlayBackWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap PlayBackWindow::getIcon()
{
    return QPixmap(":/PlayBackWindow/icons/i26.png");
}

std::pair<bool, QPixmap> PlayBackWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/PlayBackWindow/icons/i18.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > PlayBackWindow::getProcessWidget()
{
    return std::make_pair(false, nullptr);
}

bool PlayBackWindow::showWindow()
{
    playBackProcess->getProcessWidget()->show();

    isMainWindowVisible = true;

    //Нужно запросить код для воспроизведения
    getRecorderSettingsData();

    if( playBackParser->getParseState() )
    {
        return true;
    }

    updatePKDFileData(false);

    return true;
}

bool PlayBackWindow::hideWindow()
{
    isMainWindowVisible = false;
    if( playBackProcess->getOurState() == AUDIO_STOP_STATE || playBackProcess->getOurState() == AUDIO_NONE_STATE )
    {
        if( sessionsSection != nullptr )
            sessionsSection->deselectAllSessions();
    }
//    GRAPH_SECTION,
//    FILTER_SECTION
    if( currentSectionShown == GRAPH_SECTION )
    {
        slotShowGraphClicked();
    }
    else if( currentSectionShown == FILTER_SECTION )
    {
        slotShowFilterClicked();
    }

    return  true;
}

EWindSizeType PlayBackWindow::getWinSizeType()
{
    return NORMALSIZE;
}

void PlayBackWindow::initAdapterSettingsControl()
{
}

void PlayBackWindow::getInfoFromAdapter()
{
//    this->userManipulators->showLoadingWidget();
    showLoadingScreen(true);

    try
    {
        emit signalRecivedAdapterData();
//        this->userManipulators->hideLoadingWidget();
        showLoadingScreen(false);
    }
    catch (std::system_error &systemErr)
    {
//        this->userManipulators->hideLoadingWidget();
        showLoadingScreen(false);
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        userManipulators->showMessage(std::string("Не удалось получить данные.\nКод ошибки: ").append(
                std::to_string(systemErr.code().value())));
    }
}

void PlayBackWindow::initSessionsSection()
{
    if( sessionsSection == nullptr )
    {
        sessionsSection = std::make_shared<PlayBackSessions>(userManipulators, engine,
                                                             logger, audioPKDWorker,
                                                             *this, windowComponent);
        sessionsSection->init();
    }
}

void PlayBackWindow::initGraphSection()
{
    if( graphSection == nullptr )
    {
        graphSection = std::make_shared<PlayBackGraph>(userManipulators, engine,
                                                             logger, audioPKDWorker, *this, windowComponent);
        graphSection->init();
    }
}

void PlayBackWindow::initFilterSection()
{
    if( filterSection == nullptr )
    {
        filterSection = std::make_shared<FilterMaskingTab>(userManipulators, engine,
                                                             logger, *this, dataProvider, windowComponent);
        filterSection->init();
    }
}

void PlayBackWindow::initCopySection()
{
    if( copySection == nullptr )
    {
        copySection = std::make_shared<CopyWindow>(userManipulators, engine,
                                                             logger, dataProvider, *this, stateWidget, windowComponent);
        copySection->init();
    }
}

void PlayBackWindow::slotShowGraphClicked()
{     
    QMetaObject::invokeMethod(windowComponent, "updateButtonSetActive",
                              Q_ARG(QVariant, false));
    showGraph();
}

void PlayBackWindow::slotShowFilterClicked()
{
    uint64_t startDate, endDate = 0;

    if( this->getSessionsDateInterval(startDate, endDate) )
    {
        this->filterSection->setDateInterval(startDate, endDate);
    }

    if( filterSection != nullptr )
    {
        switch(currentSectionShown)
        {
            case SESSION_SECTION :
                sessionsSection->hide();
                filterSection->show();
                this->setHighligthButton(FILTER_BUTTON, true);
                this->setBackgroundTitle(filterSection->getWindowTitle());
                currentSectionShown = FILTER_SECTION;
                QMetaObject::invokeMethod(windowComponent, "updateButtonSetActive",
                                          Q_ARG(QVariant, false));
                break;
            case GRAPH_SECTION :
                graphSection->hide();
                filterSection->show();
                this->setHighligthButton(FILTER_BUTTON, true);
                this->setBackgroundTitle(filterSection->getWindowTitle());
                currentSectionShown = FILTER_SECTION;
                QMetaObject::invokeMethod(windowComponent, "updateButtonSetActive",
                                          Q_ARG(QVariant, false));

                break;
            case FILTER_SECTION :
                filterSection->hide();
                sessionsSection->show();
                this->setHighligthButton(FILTER_BUTTON, false);
                this->setBackgroundTitle(this->getWindowTitle());
                currentSectionShown = SESSION_SECTION;
                QMetaObject::invokeMethod(windowComponent, "updateButtonSetActive",
                                          Q_ARG(QVariant, true));
                break;
        }
    }
}

void PlayBackWindow::slotShowCopyClicked()
{
    showCopy();
}

void PlayBackWindow::setBackgroundTitle(std::string newtitle)
{
    QMetaObject::invokeMethod(windowComponent, "setBackgroundTitle",
                              Q_ARG(QVariant, QString::fromStdString(newtitle)));
}

void PlayBackWindow::initAudioPKDWorker()
{
    if( audioPKDWorker == nullptr )
    {
        audioPKDWorker = std::make_shared<AudioPKDWorker>(logger, *this);
    }
}

void PlayBackWindow::pkdFileParseFinished(bool isError, const std::string &msg)
{
    //switch thread
//    isPkdUpdating = false;
    emit signalParseFileFinished(isError, msg);
}

void PlayBackWindow::pkdFileParseProgress(int procent)
{
    //switch thread
    std::cerr << "STILL WORK " << std::endl;
    emit signalParseFileProgress(procent);
}

void PlayBackWindow::updatePKDFileData(bool repeatOnFail)
{
//    if( isPkdUpdating )
//    {
//        std::cerr << "Pkd is updating now " << std::endl;
//        return;
//    }

//    isPkdUpdating = true;
//    if( !this->playBackParser->parsePKDData(isFirstTime) )
//        isPkdUpdating = false;

    waitForStartUpdatePkd = true;
    repeatAllParseOnFail = repeatOnFail;
    updateErasureStatus(); //playBackParser->parsePKDData будет запущена по результатам проверки на стирание.

//    this->playBackParser->parsePKDData();
}

bool PlayBackWindow::sessionSelectedStateChanged(SessionData sessionData, bool state)
{
    if( playBackProcess->sessionSelectedStateChanged(sessionData, state) )
    {
        graphSection->sessionSelectedStateChanged(sessionData, state);
        return true;
    }
    return false;
}

void PlayBackWindow::initPlayBackProcessWidget()
{

}

void PlayBackWindow::recieveData(int command, std::vector<uint8_t> data, size_t size)
{}



void PlayBackWindow::initRecorderStatusControl()
{
    if( recorderStatusControl == nullptr )
    {
        recorderStatusControl = std::make_shared<RecorderStatusControl>(dataProvider, *logger.get());
    }
}

void PlayBackWindow::initPlayBackParser()
{
    if( playBackParser == nullptr )
    {
        playBackParser = std::make_shared<PlayBackParser>(audioPKDWorker,
                                                          recorderStatusControl,
                                                          userManipulators,
                                                          logger,
                                                          *this,
                                                          dataProvider);
        playBackParser->init();
    }
}


void PlayBackWindow::slotParseFileFinished(bool isError, string error)
{

//    isPkdUpdating = false;
    playBackParser->setParseState(false);
    if( isError )
        userManipulators->showMessage(error);
    else
    {
        this->sessionsSection->updateSessionList(recordStatus.FlashUsed, recordStatus.FlashCapacity, filterData);
    }
    showLoadingScreen(false);
    stateWidget->hideState("DATABASE_WORK_PROGRESS");


#ifndef DEBUG_NO_ADAPTER    
    playBackParser->returnSDCard();
#endif
}

void PlayBackWindow::slotParseFileProgress(int procent)
{
    if( stateWidget != nullptr )
        stateWidget->showState("DATABASE_WORK_PROGRESS", std::to_string(procent) + "%");
}

void PlayBackWindow::playSelectedSessions(const QList<SessionsModelNS::SessionData> &listData)
{
    Q_UNUSED(listData);
}


void PlayBackWindow::audioPlayThreadFunction()
{
    struct timeval tv;
    __time_t lastTime = 0;

    while(threadWork)
    {
//        std::cerr << "SEND SELECT TIME FROM AUDIO PLAYER AUDIO PLAY THREAD !!!!!!!!!!!! " << audioThreadWork << std::endl;

        if( audioThreadWork || !initCheck)
        {
            //При воспроизведении опрашиваем статус, на какой мы секунде и тд.
//            std::unique_lock<std::mutex> lk(this->cv_m);
//            cv.wait_for(lk, std::chrono::seconds(1), [this]{return conditionWork;});
//            conditionWork = false;

             gettimeofday(&tv, nullptr);
             if( tv.tv_sec >= (lastTime + 1) || !conditionWait || tv.tv_sec < lastTime )
             {
                 conditionWait = true;
                 lastTime = tv.tv_sec;

     //            this->logger->Message(LOG_ERR, __LINE__, __func__, "+++ START UPDATE");
                 if( playBackProcess->getProcessWidgetState() || isMainWindowVisible || !initCheck)
                 {
                     //if( testFlagAudioPlay )
                     {
                         try
                         {
#ifndef DEBUG_TEST
                             if( dataMutex.try_lock() )
                             {
                                 try
                                 {
                                     recorderStatusControl->updateStatus();
                                     recordStatus = recorderStatusControl->getRecorderStatus();
                                     dataMutex.unlock();
                                 }
                                 catch( std::system_error &except )
                                 {
                                     dataMutex.unlock();
                                     this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to get audio pos");
                                     continue;
                                 }
                             }
                             else
                             {
                                 this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Update Recorder status mutex is busy");
                                 continue;
                             }

#endif

                             std::cerr << QDateTime::fromSecsSinceEpoch(
                                     recordStatus.PlayTime).toString("dd.MM.yyyy hh:mm:ss").toStdString() << std::endl;
                             std::cerr << "Audio play sec " << recordStatus.PlayTime << std::endl;
                             std::cerr << "Audio play status " << recordStatus.PlayStatus << std::endl;
                             std::cerr << "Audio play pos " << recordStatus.PlayPosition << std::endl;
                             std::cerr << "OUR audio status " << static_cast<int>(playBackProcess->getOurState()) << std::endl;


                             emit signalRecorderStatusUpdated();
                         }
                         catch (std::system_error &run_except)
                         {
                             dataMutex.unlock();
                             this->logger->Message(LOG_ERR, __LINE__,
                                                   __func__ , "Error while getting audio play staus");
                         }
                     }
                 }
                 continue;
             }
             else
             {
                 usleep(10000);
             }
        }
        usleep(10000);
    }
}



void PlayBackWindow::slotRecorderStatusUpdated()
{

    //if( initCheck == false )
    {
        if( playBackProcess->getOurState() != recordStatus.PlayStatus  && playBackProcess->getOurState() == AUDIO_NONE_STATE && recordStatus.PlayStatus != AUDIO_STOP_STATE )
        {
            this->playBackProcess->stopAudioImidiatly();
        }
    }
    initCheck = true;
    try
    {
        std::cerr << "OUR STATUS == " << (int)playBackProcess->getOurState() << " RECORD STATUS = " << recordStatus.PlayStatus << std::endl;

        if( playBackProcess->getOurState() == AUDIO_PLAY_STATE ||
                playBackProcess->getOurState() == AUDIO_LOOP_STATE )
            this->graphSection->setPlayerState(true);
        else
            this->graphSection->setPlayerState(false);


        if( static_cast<AudioPlayState>(playBackProcess->getOurState()) == AUDIO_STOP_STATE )
        {
            this->audioPlayStopped();
        }


        playBackProcess->updatePlayProcessData(recordStatus);

        erasureStatusChecker();
    }
    catch (std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__ , "Error work update recorder status");
    }
}





bool PlayBackWindow::seekAudioToPosFromGraph(int sessionID, uint64_t seekSec)
{
    if( playBackProcess != nullptr )
    {
        return playBackProcess->seekAudioToPosFromGraph(sessionID, seekSec);
    }
    return false;
}

std::string PlayBackWindow::getWindowTitle()
{
    return PLAYBACK_WINDOW_TITLE;
}

bool PlayBackWindow::getSessionsDateInterval(uint64_t &startDate, uint64_t &endDate)
{
    auto &sessionsList = sessionsSection->getSessionsList();

    if( !sessionsList.empty() )
    {
        startDate = sessionsList.first().startTime;
        endDate = sessionsList.last().endTime;
    }
    else
        return false;
    return true;
}

bool PlayBackWindow::filterSessionsData(SessionFilter filterData)
{
    this->filterData = filterData;
    this->sessionsSection->updateSessionList(recordStatus.FlashUsed, recordStatus.FlashCapacity, this->filterData);
    return true;
}

bool PlayBackWindow::getAllSessions(std::vector<SessionsModelNS::SessionData> &sessions)
{
    for( auto session: sessionsSection->getSessionsList() )
    {
        sessions.push_back(session);
    }
    return true;
}

bool PlayBackWindow::getSelectedSessions(std::vector<SessionsModelNS::SessionData> &sessions)
{
    if( playBackProcess != nullptr )
        playBackProcess->getSelectedSessions(sessions);
}

void PlayBackWindow::setHighligthButton(int button, bool activeState)
{
    QMetaObject::invokeMethod(windowComponent, "setHighlightButton",
                              Q_ARG(QVariant, button),
                              Q_ARG(QVariant, activeState));
}

bool PlayBackWindow::filterMaskData(SessionMask maskData)
{
    this->maskData = maskData;
    if( playBackProcess != nullptr )
        playBackProcess->updateFilterMaskData(maskData);
    return false;
}

void PlayBackWindow::getRecorderSettingsData()
{
    updateWorker = std::async(std::launch::async, [this] () mutable
    {
        try
        {
            recorderSettingsControl->updateStatus();
            recorderDataUpdate.lock();
            recordSettings = recorderSettingsControl->getRecordSettings();
            if( playBackProcess != nullptr )
                playBackProcess->updateRecorderSettingsControl(recordSettings);
            recorderDataUpdate.unlock();
//            emit signalRecorderSettingsUpdated();
        }
        catch (std::system_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            userManipulators->showMessage(std::string("Не удалось обновить данные.\nКод ошибки: ").append(
                    std::to_string(except.code().value())));
            recorderDataUpdate.unlock();
            showLoadingScreen(false);
        }
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}

void PlayBackWindow::initRecorderSettingsControl()
{
    if( recorderSettingsControl == nullptr )
    {
        recorderSettingsControl = std::make_shared<RecorderSettingsContol>(dataProvider, *logger.get());
    }
}

void PlayBackWindow::slotRecorderSettingsUpdated()
{

}

void PlayBackWindow::updateSessionListFinished()
{
    showLoadingScreen(false);
}

void PlayBackWindow::showLoadingScreen(bool isShow)
{
    sessionsSection->showLoadingScreen(isShow);

    if(!blockScreen)
        enableSqareButtons(!isShow);
}

void PlayBackWindow::enableSqareButtons(bool isEnable)
{
    QMetaObject::invokeMethod(windowComponent, "squareButtonsSetActive",
                              Q_ARG(QVariant, isEnable));
}

void PlayBackWindow::copyProcessData(int percent, uint32_t timeLeft, int speed)
{
    if( percent == -1 )
    {
        stateWidget->hideState("COPY_WORK_PROGRESS");
        copyProcessWidget->updateCopyData(0, 0, 0);
        return;
    }

    copyProcessWidget->updateCopyData(percent, timeLeft, speed);
    stateWidget->showState("COPY_WORK_PROGRESS", std::to_string(percent) + "%");
}

void PlayBackWindow::initCopyProcessWidget()
{
    if( copyProcessWidget == nullptr )
    {
        copyProcessWidget = std::shared_ptr<CopyProcessWidget>(
                new CopyProcessWidget(engine, COPY_PROCESS_WIDGET_NAME, *logger));

        copyProcessWidget->init();
        processWidgets.push_back(copyProcessWidget);
    }
}

void PlayBackWindow::initPlayBackProcess()
{
    if( playBackProcess == nullptr )
    {
        playBackProcess = std::make_shared<PlayBackProcess>( audioPKDWorker, this->engine,
                                                            recorderStatusControl,
                                                            logger,
                                                            userManipulators,
                                                             *this);
        playBackProcess->init();
        processWidgets.push_back(playBackProcess->getProcessWidget());
    }
}

void PlayBackWindow::copyStarted()
{
    copyProcessWidget->show();
}

std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> PlayBackWindow::getProcessWidgetsVector()
{
    std::vector<std::shared_ptr<IProcessWidget>> processWidgetsVector;

    for(auto it: processWidgets )
        processWidgetsVector.push_back(it);

    return std::make_pair(true, processWidgetsVector);
}

bool PlayBackWindow::prepareToDelete()
{
    if( copySection != nullptr )
    {
        if( copySection->getCopyState() == COPY_STARTED )
        {
            return false;
        }
    }

    return true;

}

PlayBackWindow::~PlayBackWindow()
{
    std::cerr << "DElete playbackwindow" << std::endl;
    threadWork = false;
    if(playThread.joinable() )
        playThread.join();
    std::cerr << "Play thread stopped" << std::endl;
    audioPKDWorker->stopPKDParse();
    audioPKDWorker = nullptr;
    std::cerr << "audioPKDWorker removed" << std::endl;

    std::cerr << "PLayBackParser start remove" << std::endl;
    playBackParser->prepareToDelete();
    playBackParser = nullptr;
    std::cerr << "PLayBackParser removed" << std::endl;

    sessionsSection = nullptr;
    std::cerr << "PlayBackSessions removed" << std::endl;

    copySection = nullptr;
    std::cerr << "CopySesction removed" << std::endl;

    filterSection = nullptr;
    std::cerr << "FilterSection removed" << std::endl;


    graphSection = nullptr;
    std::cerr << "PLayBackGraph removed" << std::endl;

    playBackProcess = nullptr;
    std::cerr << "PlayBackProcess removed" << std::endl;
}

bool PlayBackWindow::checkAnySessionSelected()
{
    std::vector<SessionsModelNS::SessionData> sessionsForCheck;

    getAllSessions(sessionsForCheck);

    for( auto session : sessionsForCheck )
    {
        if(session.selected)
        {
            return true;
        }
    }
    return false;
}

SessionFilter PlayBackWindow::getFilterData()
{
    return filterData;
}

SessionMask PlayBackWindow::getMaskData()
{
    return maskData;
}

void PlayBackWindow::showGraph()
{
    if(checkAnySessionSelected() || graphSection->isVisible())
    {
        if( graphSection != nullptr )
        {
            switch(currentSectionShown)
            {
                case SESSION_SECTION :
                    sessionsSection->hide();
                    graphSection->show();
                    this->setHighligthButton(GRAPH_BUTTON, true);
                    currentSectionShown = GRAPH_SECTION;
                    QMetaObject::invokeMethod(windowComponent, "updateButtonSetActive",
                                              Q_ARG(QVariant, false));
                    break;
                case GRAPH_SECTION :
                    graphSection->hide();
                    this->setHighligthButton(GRAPH_BUTTON, false);
                    sessionsSection->show();
                    currentSectionShown = SESSION_SECTION;
                    QMetaObject::invokeMethod(windowComponent, "updateButtonSetActive",
                                              Q_ARG(QVariant, true));
                    break;
                case FILTER_SECTION :
                    filterSection->hide();
                    this->setBackgroundTitle(this->getWindowTitle());
                    graphSection->show();
                    this->setHighligthButton(GRAPH_BUTTON, true);
                    currentSectionShown = GRAPH_SECTION;
                    QMetaObject::invokeMethod(windowComponent, "updateButtonSetActive",
                                              Q_ARG(QVariant, false));
                    break;
            }
        }
    }
    else
    {
        this->userManipulators->showMessage("Сессии не выбраны");
    }
}

void PlayBackWindow::showCopy()
{
    copySection->show(recordStatus.FlashCapacity);
}

void PlayBackWindow::tapMenuPlay()
{
    if( playBackProcess != nullptr )
        playBackProcess->tapMenuPlay();
}


void PlayBackWindow::updateErasureStatus()
{
    showLoadingScreen(true);

    updateWorker = std::async( std::launch::async,[this]
    {
        try
        {
            recorderStatusControl->updateStatus();

            recordStatus = recorderStatusControl->getRecorderStatus();

            emit signalErasureStatusUpdated();
        }
        catch (std::system_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            userManipulators->showMessage(std::string("Не удалось получть данные.\nКод ошибки: ").append(
                    std::to_string(except.code().value())));
            showLoadingScreen(false);
        }

        return;
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}


void PlayBackWindow::slotErasureStatusUpdated()
{
    showLoadingScreen(false);

    if(static_cast<FLASH_STATUS>(recordStatus.FlashStatus) != fsFlashErasing)
    {
        showBlockScreen(false);
        if( playBackProcess->getOurState() == AUDIO_STOP_STATE || playBackProcess->getOurState() == AUDIO_NONE_STATE )
        {
            if(waitForStartUpdatePkd)
            {
                waitForStartUpdatePkd = false;
                this->playBackParser->parsePKDData(repeatAllParseOnFail);
            }
        }
    }
    else
    {
        eraseIsRunning = true;
        showBlockScreen(true);
        enableSqareButtons(false);
        userManipulators->showMessage(std::string("Идет стирание.\nДоступ к памяти запрещен."));
    }
}


void PlayBackWindow::erasureStatusChecker()
{
    if(static_cast<FLASH_STATUS>(recordStatus.FlashStatus) == fsFlashErasing)
    {
        if(!eraseIsRunning)
        {
            eraseIsRunning = true;
            showBlockScreen(true);
            enableSqareButtons(false);
            userManipulators->showMessage(std::string("Запущено стирание.\nДоступ к памяти запрещен."));
        }
    }
    else
    {
        if(eraseIsRunning)
        {
            eraseIsRunning = false;
            showBlockScreen(false);
            enableSqareButtons(true);

            if(playBackParser->getParseState())
            {
                userManipulators->showMessage(std::string("Ошибка обновления списка воспроизведения. (Обновление уже запущено)"));
                return;
            }

            if( playBackProcess->getOurState() == AUDIO_STOP_STATE || playBackProcess->getOurState() == AUDIO_NONE_STATE )
            {
                sessionsSection->deselectAllSessions();
                if(audioPKDWorker->removeDatabase())
                {
                    std::cerr << "Start update pkd file data" << std::endl;
                    waitForStartUpdatePkd = false;
                    this->playBackParser->parsePKDData(false);
                }
            }
            else
            {
                userManipulators->showMessage(std::string("Ошибка обновления списка воспроизведения."));
                return;
            }
        }
    }
}


void PlayBackWindow::slotUpdateDatabase()
{
    if(playBackParser->getParseState())
    {
        userManipulators->showMessage(std::string("Обновление списка воспроизведения уже запущено."));
        return;
    }

    if( this->userManipulators->requestUserConfirm("Вы уверены что хотите обновить данные сессий?"))
    {
        if( playBackProcess->getOurState() == AUDIO_STOP_STATE || playBackProcess->getOurState() == AUDIO_NONE_STATE )
        {
            sessionsSection->deselectAllSessions();
            if(audioPKDWorker->removeDatabase())
            {
                std::cerr << "Start update pkd file data" << std::endl;
                waitForStartUpdatePkd = false;
                this->playBackParser->parsePKDData(false);
            }
        }
        else
        {
            userManipulators->showMessage(std::string("Остановите воспроизведение для продолжения."));
            return;
        }
    }
}

void PlayBackWindow::showBlockScreen(bool isShow)
{
    blockScreen = isShow;
    sessionsSection->showBlockScreen(isShow);
}


//IPlayBackParser
void PlayBackWindow::setSectionLoadingScreen(bool isShown)
{
    this->showLoadingScreen(isShown);
}

void PlayBackWindow::updateSessionsList(size_t recordedSize,
                                        size_t recordedCapacity)
{
    this->sessionsSection->updateSessionList(recordedSize, recordedCapacity, this->filterData);
}

void PlayBackWindow::currentPlayTimeChanged(int sessionID, uint64_t currentPlayTime)
{
    this->graphSection->currentPlayTimeChanged(sessionID, currentPlayTime);

    if( currentPlaySessionId != sessionID && playBackProcess->getOurState() != AUDIO_STOP_STATE )
    {
        this->sessionsSection->currentPlaySessionChanged(currentPlaySessionId, false);
        this->sessionsSection->currentPlaySessionChanged(sessionID, true);
        currentPlaySessionId = sessionID;
    }
}

void PlayBackWindow::audioPlayStopped()
{
    if( currentPlaySessionId != -1 )
    {
        this->sessionsSection->currentPlaySessionChanged(currentPlaySessionId, false);
        currentPlaySessionId = -1;
    }
}

//**IPlayBackParser



