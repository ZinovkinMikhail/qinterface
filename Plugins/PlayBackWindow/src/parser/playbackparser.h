//
// Created by ivan on 09.11.2020.
//

#ifndef CM_INTERFACE_PLAYBACKPARSER_H
#define CM_INTERFACE_PLAYBACKPARSER_H

#include <memory>
#include <future>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fstream>

#include <QObject>
#include <QTimer>


#include <iemptyplacewindow.h>

#include <dataprovider/recorderstatuscontrol.h>

#include "src/database/audiopkdworker.h"

#include "iplaybackparser.h"

#define UMS_LOCK_PATH "/tmp/UMSStoplock"

//#define DEBUG_NO_ADAPTER
//#define DEBUG_RECORDED_BYTES 8334336  //2415423488 //cff 11 2415423488  //cff 10 2413382656  // ON DEVICE
//data1: 187200512, data2:11428352, data3: 93700096, data4:146121728, data5:191511552
//#define DEBUG_CAPACITY 234881024 //5268045824

using namespace recorder_status_ctrl;

//Объект контролирует процесс парсинга и обрабатывает результат

class PlayBackParser: public QObject, public IDataWaiter,
        public std::enable_shared_from_this<PlayBackParser>
{
    Q_OBJECT
public:
    explicit PlayBackParser(std::shared_ptr<AudioPKDWorker> audioPkdWorker,
                            std::shared_ptr<RecorderStatusControl> recorderStatusCtrl,
                            std::shared_ptr<IUserManipulators> iUserManipulators,
                            std::shared_ptr<rosslog::Log> logger,
                            IPlayBackParser &iPlayBackParser,
                            std::shared_ptr<data_provider::DataProvider> dataProvider,
                            QObject *parent = nullptr);

    void init();
    bool parsePKDData(bool repeatAllOnFail);
    void returnSDCard();
    void setParseState(bool isUpdating);
    bool getParseState();
    void prepareToDelete();

    ~PlayBackParser();

protected:

    void recieveData(int command, std::vector<uint8_t> data,
                        size_t size);


private:
    bool checkEmmcInSystem();
    bool checkUMSStopLock();

    std::shared_ptr<AudioPKDWorker> audioPkdWorker{nullptr};
    std::shared_ptr<RecorderStatusControl> recorderStatusCtrl{nullptr};
    std::shared_ptr<IUserManipulators> iUserManipulators{nullptr};
    std::shared_ptr<rosslog::Log> logger{nullptr};
    std::shared_ptr<data_provider::DataProvider> dataProvider{nullptr};
    IPlayBackParser &iPlayBackParser;

    int timerCounter{0};
    int repeatAllTimerCounter{0};
    int flashUsedErrorCounter{0};
    bool repeatAllOnFail{false};

    bool isPkdUpdating{false};
    bool fileRead{false};
    bool isSdRequested{false};

    std::future<void> updateWorker;


    QTimer *parseProcessTimer{nullptr};
    QTimer *repeatAllparseTimer{nullptr};
    SoftRecorderStatusStruct_t recordStatus;



signals:
    void signalStartTimer();
    void signalStartRepeatTimer();
    void signalUpdateSessionsList(size_t recordedSize,
                                  size_t recordedCapacity);

public slots:
    void slotTimerTimeout();
    void slotStartTimer();
    void slotRepeatTimerTimeout();
    void slotStartRepeatTimer();
    void slotUpdateSessionsList(size_t recordedSize,
                                size_t recordedCapacity);

};


#endif
