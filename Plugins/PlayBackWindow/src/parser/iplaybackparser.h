#ifndef CM_INTERFACE_IPLAYBACKPARSER_H
#define CM_INTERFACE_IPLAYBACKPARSER_H

#include <unistd.h>

class IPlayBackParser
{
public:
    virtual void setSectionLoadingScreen(bool isShown) = 0;
    //В случае если парсер на этапе подготовки определил, что нет изменений по пкд
    //просто обновляем таблицу с сессиями
    virtual void updateSessionsList(size_t recordedSize,
                                    size_t recordedCapacity) = 0;
};

#endif //CM_INTERFACE_IPLAYBACKPARSER_H
