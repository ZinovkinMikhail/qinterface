#include "playbackparser.h"

#include <cassert>


using namespace recorder_status_ctrl;
using namespace rosslog;

PlayBackParser::PlayBackParser(std::shared_ptr<AudioPKDWorker> audioPkdWorker,
                               std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusCtrl,
                               std::shared_ptr<IUserManipulators> iUserManipulators,
                               std::shared_ptr<rosslog::Log> logger,
                               IPlayBackParser &iPlayBackParser,
                               std::shared_ptr<data_provider::DataProvider> dataProvider,
                               QObject *parent) :
    audioPkdWorker(audioPkdWorker),
    recorderStatusCtrl(recorderStatusCtrl),
    iUserManipulators(iUserManipulators),
    logger(logger),
    iPlayBackParser(iPlayBackParser),
    dataProvider(dataProvider),
    QObject(parent)
{
    qRegisterMetaType<size_t>("size_t");
}

void PlayBackParser::init()
{
    if( parseProcessTimer == nullptr )
    {
        parseProcessTimer = new QTimer();
        connect(parseProcessTimer, SIGNAL(timeout()),
                this, SLOT(slotTimerTimeout()));
        connect(this, SIGNAL(signalStartTimer()),
                this, SLOT(slotStartTimer()));
        connect(this, SIGNAL(signalUpdateSessionsList(size_t, size_t)),
                this, SLOT(slotUpdateSessionsList(size_t, size_t)));
        dataProvider->addDataWaiter(AXIS_INTERFACE_EVENT_STORAGE, shared_from_this());
    }

    if( repeatAllparseTimer == nullptr )
    {
        repeatAllparseTimer = new QTimer();
        connect(repeatAllparseTimer, SIGNAL(timeout()),
                this, SLOT(slotRepeatTimerTimeout()));
        connect(this, SIGNAL(signalStartRepeatTimer()),
                this, SLOT(slotStartRepeatTimer()));
    }
}

bool PlayBackParser::parsePKDData(bool repeatOnFail)
{
    std::cout << "parsePKDData.. repeatOnFail = " << repeatOnFail << std::endl;

    repeatAllOnFail = repeatOnFail;

    if( isPkdUpdating )
    {
        iUserManipulators->showMessage(std::string("Обновление списка воспроизведения уже запущено."));
        return false;
    }

    isPkdUpdating = true;
#ifdef  DEBUG_TEST
    //Тестовое заполнение базы пкдшками
    audioPkdWorker->startPKDParseTest();
    return;
#endif
    bool isDatabasePresent = audioPkdWorker->checkDatabaseExists();
    uint64_t lastBytesRecorded = 0;

    std::cout << "isDatabasePresent = " << isDatabasePresent << std::endl;

    if( isDatabasePresent )
    {
        if( !audioPkdWorker->getLastRecordedInfo(lastBytesRecorded) )
        {
            //TODO clean database and refill from begining
            isPkdUpdating = false;
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while getting last parsed data from database");
            return false;
        }
    }

//    if( !isFirst )
        iPlayBackParser.setSectionLoadingScreen(true);


    updateWorker = std::async( std::launch::async,[this, isDatabasePresent, lastBytesRecorded]
    {
        try
        {

#ifdef DEBUG_NO_ADAPTER
            recordStatus.FlashUsed = DEBUG_RECORDED_BYTES;
            recordStatus.FlashCapacity = DEBUG_CAPACITY;
#else

            recorderStatusCtrl->updateStatus();

            recordStatus = recorderStatusCtrl->getRecorderStatus();

            this->logger->Message(LOG_INFO, __LINE__, __func__ , "FLASH USED %li FLASH CAPACITY %li \n",
                                  recordStatus.FlashUsed,
                                  recordStatus.FlashCapacity);
#endif
            if( isDatabasePresent )
            {
                this->logger->Message(LOG_INFO, __LINE__, __func__ , "Last recorded %li flash used %li\n",
                                      lastBytesRecorded,
                                      recordStatus.FlashUsed);
                if( lastBytesRecorded != recordStatus.FlashUsed )
                {
                    //Начинаем
#ifndef DEBUG_NO_ADAPTER                  
                    if(!checkEmmcInSystem())
                    {
                        if(!checkUMSStopLock())
                        {
                            recorderStatusCtrl->requestSDCard();
                        }
                        else
                        {
                            iPlayBackParser.setSectionLoadingScreen(false);
                            iUserManipulators->showMessage(std::string("Нет доступа к EMMC (UMSStopLock)"));
                            isPkdUpdating = false;
                            return;
                        }
                    }
                    isSdRequested = true;

#endif
//                    if( !isFirst )
                        iPlayBackParser.setSectionLoadingScreen(true);

                    timerCounter = 0;
                    emit signalStartTimer();
                }
                else
                {
//                    if( !isFirst )
                        iPlayBackParser.setSectionLoadingScreen(true);
                    this->logger->Message(LOG_INFO, __LINE__, __func__ , "We have actual info about pkd!");

                    //move from main thread
                    emit signalUpdateSessionsList(recordStatus.FlashUsed, recordStatus.FlashCapacity);
                    repeatAllOnFail = false;
                }
            }
            else
            {
#ifndef DEBUG_NO_ADAPTER
                if(!checkEmmcInSystem())
                {
                    if(!checkUMSStopLock())
                    {
                        recorderStatusCtrl->requestSDCard();
                    }
                    else
                    {
                        iPlayBackParser.setSectionLoadingScreen(false);
                        iUserManipulators->showMessage(std::string("Нет доступа к EMMC (UMSStopLock)"));
                        isPkdUpdating = false;
                        return;
                    }
                }
                isSdRequested = true;
#endif
//                if( !isFirst )
                    iPlayBackParser.setSectionLoadingScreen(true);

                timerCounter = 0;
                emit signalStartTimer();
            }

        }
        catch (std::system_error &except)
        {
            iPlayBackParser.setSectionLoadingScreen(false);
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            iUserManipulators->showMessage(std::string("Не удалось выполнить команду.\nКод ошибки: ").append(
                    std::to_string(except.code().value())));
            isPkdUpdating = false;
        }

        return;
    });
    updateWorker.wait_for(std::chrono::seconds(0));
    return true;
}

PlayBackParser::~PlayBackParser()
{
    if( parseProcessTimer != nullptr )
    {
        parseProcessTimer->stop();
        delete parseProcessTimer;
    }

    if( repeatAllparseTimer != nullptr )
    {
        repeatAllparseTimer->stop();
        delete repeatAllparseTimer;
    }

    if( audioPkdWorker != nullptr )
    {
        audioPkdWorker->stopPKDParse();
    }

    if(isSdRequested)
        returnSDCard();
}

void PlayBackParser::recieveData(int command, std::vector<uint8_t> data, size_t size)
{
    if( command == AXIS_INTERFACE_EVENT_STORAGE )
    {
        std::cerr << "Ща отберут SDDD 2 " << std::endl;
    }
}

bool PlayBackParser::checkEmmcInSystem()
{
    struct statvfs path_stat;
    int ret = statvfs(PKD_FILE_PATH, &path_stat);
    FILE *fp = NULL;
    fp = fopen(PKD_FILE_PATH, "r");


    if (fp == nullptr )
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ ,"failed to fopen %s\n", PKD_FILE_PATH);
        return false;
    }

    if (fseek(fp, 0, SEEK_END) == -1)
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ ,"failed to fseek %s\n", PKD_FILE_PATH);
        fclose(fp);
        return false;
    }

    long int off = ftell(fp);
    std::cerr << "EMMC SIZE = " << off << std::endl;

    if (off == (long)-1)
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ ,"failed to ftell %s\n", PKD_FILE_PATH);
        fclose(fp);
        return false;
    }

    fclose(fp);

    if( ret >= 0 && off > 17068792320 )
    {
        return true;
    }
    return false;
}

bool PlayBackParser::checkUMSStopLock()
{
    for (int i = 0; i < 5; i++)
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ , "Check UMS stop lock file");
        if (FILE *file = fopen(UMS_LOCK_PATH, "r"))
        {
            fclose(file);
            this->logger->Message(LOG_INFO, __LINE__, __func__ , "UMS stop lock file exist!");
        }
        else
        {
            this->logger->Message(LOG_INFO, __LINE__, __func__ , "UMS stop lock file does't exist");
            return false;
        }

        sleep(1);
    }

    return true;
}

void PlayBackParser::slotTimerTimeout()
{
    timerCounter++;

    if(timerCounter > 5)
    {
        parseProcessTimer->stop();
        flashUsedErrorCounter = 0;
        iUserManipulators->showMessage(std::string("EMMC не найдено"));
        if(isSdRequested)
            returnSDCard();
        this->iPlayBackParser.setSectionLoadingScreen(false);
        isPkdUpdating = false;

        if(repeatAllOnFail)
        {
            repeatAllTimerCounter++;

            if(repeatAllTimerCounter > 5)
            {
                repeatAllOnFail = false;
                repeatAllTimerCounter = 0;
            }
            else
                emit signalStartRepeatTimer();
        }

        return;
    }
#ifndef DEBUG_NO_ADAPTER
    bool isEmmcInSystem = checkEmmcInSystem();

    //Добавим таймаут проверки , не будем же мы тут висеть вченость
    if( isEmmcInSystem )
    {
        if(fileRead)
        {
            std::cout << "Fix when EMMC not avalible after record"  << std::endl;
            fileRead = false;
        }

        if( !fileRead )
        {
            try
            {
                fileRead = true;
                parseProcessTimer->stop();

                sync();

                int ret = system("echo 1 > /proc/sys/vm/drop_caches");

                if( ret < 0 )
                {
                    std::cerr << "Error while drop caches 1" << std::endl;
                }

                ret = system("echo 2 > /proc/sys/vm/drop_caches");

                if( ret < 0 )
                {
                    std::cerr << "Error while drop caches 2" << std::endl;
                }

                ret = system("echo 3 > /proc/sys/vm/drop_caches");

                if( ret < 0 )
                {
                    std::cerr << "Error while drop caches 3" << std::endl;
                }

                flashUsedErrorCounter = 0;

                if( !audioPkdWorker->startPKDParse(recordStatus.FlashUsed, recordStatus.FlashCapacity) )
                {
                    isPkdUpdating = false;
                    iUserManipulators->showMessage(std::string("Ошибка построения таблицы сеансов."));
                }


            }
            catch (std::system_error &systemErr)
            {
                this->logger->Message(LOG_ERR, __LINE__,
                                      __func__,
                                      dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
                flashUsedErrorCounter++;

                if( flashUsedErrorCounter > 3 )
                {
                    parseProcessTimer->stop();
                    iUserManipulators->showMessage(std::string("Не удалось получить данные.\nКод ошибки: ").append(
                            std::to_string(systemErr.code().value())));
                    flashUsedErrorCounter = 0;
                }
            }
            return;
        }
    }
    else{
        isPkdUpdating = false;
    }
#endif

#ifdef DEBUG_NO_ADAPTER
        if( !audioPkdWorker->startPKDParse(DEBUG_RECORDED_BYTES, DEBUG_CAPACITY) )
        {
            iUserManipulators->showMessage(std::string("Ошибка построения таблицы сеансов."));
        }
        else
            parseProcessTimer->stop();

#endif

}

void PlayBackParser::slotRepeatTimerTimeout()
{
    repeatAllparseTimer->stop();
    this->logger->Message(LOG_INFO, __LINE__, __func__ , "parsePKDData repeat all!");
    parsePKDData(true);
}

void PlayBackParser::slotStartTimer()
{
    parseProcessTimer->start(2000);
}

void PlayBackParser::slotStartRepeatTimer()
{
    repeatAllparseTimer->start(2000);
}

void PlayBackParser::slotUpdateSessionsList(size_t recordedSize,
                                            size_t recordedCapacity)
{
    isPkdUpdating = false;
    this->iPlayBackParser.updateSessionsList(recordedSize, recordedCapacity);
}


void PlayBackParser::returnSDCard()
{
    updateWorker = std::async( std::launch::async,[this]
    {
        try
        {
            recorderStatusCtrl->returnSDCard();
            isSdRequested = false;
            std::cout << "RETURN SD" << std::endl;
        }
        catch (std::system_error &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__,
                                  dpr::ProviderError::getInstance().getErrorString(except).c_str());
            iUserManipulators->showMessage(std::string("Не удалось выполнить команду (Возврат памяти).\nКод ошибки: ").append(
                    std::to_string(except.code().value())));
        }
    });
    updateWorker.wait_for(std::chrono::seconds(0));
}

void PlayBackParser::setParseState(bool isUpdating)
{
    isPkdUpdating = isUpdating;
    if(!isUpdating)
    {
        repeatAllOnFail = false;
        repeatAllTimerCounter = 0;
    }
}

bool PlayBackParser::getParseState()
{
    if(isPkdUpdating || repeatAllOnFail)
        return true;
    else
        return false;
}

void PlayBackParser::prepareToDelete()
{
    this->dataProvider->removeDataWaiter(AXIS_INTERFACE_EVENT_STORAGE, shared_from_this());
}

