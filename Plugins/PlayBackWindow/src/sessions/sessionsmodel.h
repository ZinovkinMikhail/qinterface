//
// Created by ivan on 23.10.2020.
//

#ifndef CM_INTERFACE_SESSIONSMODEL_H
#define CM_INTERFACE_SESSIONSMODEL_H

#include <string>
#include <cstdint>

namespace SessionsModelNS
{
    typedef struct
    {
        uint64_t startTime;
        uint64_t endTime;
        int duration;
        double size;
        uint8_t channelType;
        uint8_t channelMode;
        int selected;
        int sessionID;
        uint64_t startRecAddress;
        uint64_t endRecAddress;
        bool currentPlay;
    } SessionData;
}


#endif //CM_INTERFACE_SESSIONSMODEL_H
