//
// Created by ivan on 23.10.2020.
//

#ifndef CM_INTERFACE_PLAYERLISTMODEL_H
#define CM_INTERFACE_PLAYERLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QStringList>

#include "sessionsmodel.h"

namespace PlayBackListModelNS
{
    enum Roles
    {
        StartTime = Qt::UserRole + 1,
        Index,
        EndTime,
        Duration,
        Size,
        Type,
        Selected,
        CurentPlay
    };
    class PlayBackListModel : public QAbstractListModel
    {
        Q_OBJECT
    public:
        explicit PlayBackListModel(QObject *parent = nullptr);

        std::pair<bool, SessionsModelNS::SessionData> setProperty(int index, QString type, int val);
        void appendModel(SessionsModelNS::SessionData data);
        void clearModel();
        void updateModel();
        void selectAllRows();
        void deselectAllRows();
        const QList<SessionsModelNS::SessionData> &getSessionsList();

        Q_INVOKABLE bool getSelectedState(int index) const;

    protected:
        virtual int rowCount(const QModelIndex &parent) const;
        virtual QVariant data(const QModelIndex &index, int role) const;
        virtual QHash<int, QByteArray> roleNames() const;



    private:
        QList<SessionsModelNS::SessionData> listData;
    };
}




#endif //CM_INTERFACE_PLAYERLISTMODEL_H
