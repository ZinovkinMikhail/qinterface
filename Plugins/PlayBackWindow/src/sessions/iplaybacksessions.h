//
// Created by ivan on 07.12.2020.
//

#ifndef CM_INTERFACE_IPLAYBACKSESSIONS_H
#define CM_INTERFACE_IPLAYBACKSESSIONS_H

#include "sessionsmodel.h"

class IPlayBackSessions
{
public:
    virtual bool sessionSelectedStateChanged(SessionsModelNS::SessionData sessionData, bool state) = 0;
    virtual void playSelectedSessions(const QList<SessionsModelNS::SessionData> &listData) = 0;
    virtual void updateSessionListFinished() = 0;
    virtual void showGraph() = 0;
    virtual void showCopy() = 0;
    virtual void tapMenuPlay() = 0;
};

#endif //CM_INTERFACE_IPLAYBACKSESSIONS_H
