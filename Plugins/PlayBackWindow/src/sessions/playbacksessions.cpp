//
// Created by ivan on 23.10.2020.
//

#include <cassert>

#include <QQmlProperty>

#include "playbacksessions.h"

using namespace PlayBackListModelNS;

PlayBackSessions::PlayBackSessions(std::shared_ptr<IUserManipulators> userManipulators,
                                   std::shared_ptr<IQmlEngine> qmlEngine,
                                   std::shared_ptr<rosslog::Log> log,
                                   std::shared_ptr<AudioPKDWorker> pkdWorker,
                                   IPlayBackSessions &playBackSessionsInterface,
                                   QObject *parent) :
        QObject(parent),
        qmlEngine(qmlEngine),
        logger(log),
        currentItem(nullptr),
        pkdWorker(std::move(pkdWorker)),
        playBackSessionsInterface(playBackSessionsInterface),
        allSelected(false)
{
    assert(qmlEngine);
    assert(logger);
    assert(parent);
    assert(this->pkdWorker);
}

PlayBackSessions::~PlayBackSessions()
{
    std::cerr << "Delete PlayBackSessions" << std::endl;
}

void PlayBackSessions::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(
                qmlEngine->createComponent(SESSIONS_LIST_QML_PARH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

        initSessionsListModel();

        QMetaObject::invokeMethod(currentItem,"setListModel",
                                  Q_ARG(QVariant, QVariant::fromValue(listModel.get())));
        connect(currentItem, SIGNAL(rowChanged(int,QString,int)),
                this, SLOT(slotRowChanged(int,QString,int)));
        connect(currentItem, SIGNAL(selectAll()), this, SLOT(slotSelectAll()));
        connect(currentItem, SIGNAL(play()), this, SLOT(slotPlay()));
        connect(currentItem, SIGNAL(copy()), this, SLOT(slotCopy()));
        connect(currentItem, SIGNAL(select(QVariant)), this, SLOT(slotSelect(QVariant)));
        connect(currentItem, SIGNAL(graph()), this, SLOT(slotGraph()));
    }
}

void PlayBackSessions::show()
{
    QQmlProperty(currentItem, "visible").write(true);
}

void PlayBackSessions::hide()
{
    QQmlProperty(currentItem, "visible").write(false);
}

void PlayBackSessions::initSessionsListModel()
{
    if( listModel == nullptr )
    {
        listModel = std::make_shared<PlayBackListModel>();
    }
}

void PlayBackSessions::slotRowChanged(int index, QString row, int value)
{
    auto ret = listModel->setProperty(index, row, value);

    if( ret.first )
    {
        if( allSelected )
        {
            allSelected = false;
            QMetaObject::invokeMethod(currentItem,"reload");
        }
        if( ! playBackSessionsInterface.sessionSelectedStateChanged(ret.second , value) )
            listModel->setProperty(index, row, !value );
    }

    listModel->updateModel();
}

void PlayBackSessions::updateSessionList(size_t recordedSize, size_t recordedCapacity, SessionFilter filterData)
{
    listModel->clearModel();
    std::vector<ChannelParams> sessions;

    if( filterData.type == DATE_FILTER )
    {
        if( !pkdWorker->getSessionsByDate(filterData.startDate, filterData.endDate, sessions) )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while get sessions by date");
            //TODO do show message for user
            return;
        }
    }
    else if( filterData.type == CHANNEL_FILTER )
    {
        uint8_t channelType;
        uint8_t channel;

        switch (filterData.channelType)
        {
            case MONO_1:
            {
                channelType = 1;
                channel = 0;
                break;
            }
            case MONO_2:
            {
                channelType = 1;
                channel = 1;
                break;
            }
            case MONO_3:
            {
                channelType = 1;
                channel = 2;
                break;
            }
            case MONO_4:
            {
                channelType = 1;
                channel = 3;
                break;
            }
            case STEREO_1:
            {
                channelType = 0;
                channel = 0;
                break;
            }
            case STEREO_2:
            {
                channelType = 0;
                channel = 2;
                break;
            }
        }

        if( !pkdWorker->getSessionsByChannelType(channelType, channel, sessions) )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while get sessions by channel");
            //TODO do show message for user
            return;
        }
    }
    else if( filterData.type == NONE_FILTER )
    {
        pkdWorker->getSessions(sessions);
    }

    int previousSessionByteShift = 0;

    std::cerr << "Recorder size = " << recordedSize << std::endl;
    std::cerr << "Recorder capacity = " << recordedCapacity << std::endl;



    for(auto it: sessions )
    {
        int shift = it.sessionByteShift;
        double size = 0;


        std::cerr << "Session id = " << it.id << std::endl;
        std::cerr << "Session start Addr " << it.startAddress << std::endl;
        std::cerr << "Bytes shift " << it.sessionByteShift << std::endl;

        std::cerr << "Posible 1/64 Start addr rec " << it.startAddress * 64 << std::endl;
        std::cerr << "Posible 1/64 rec " << it.sessionByteShift * 64 << std::endl;

        uint64_t packs = ((static_cast<uint64_t>(it.packetsCount)) * 8 * 64);

        size = packs/ 1000.0 / 1000.0;

        SessionsModelNS::SessionData data;
        data.startRecAddress = it.startAddress * 64;
        data.endRecAddress = it.sessionByteShift * 64;
        data.channelType = it.channel + 1;
        data.duration = it.endTime - it.startTime;
        data.endTime = it.endTime;
        data.startTime = it.startTime;
        data.selected = 0;
        data.size = size;
        data.sessionID = it.id;
        data.channelMode = it.type;
        data.currentPlay = false;

        listModel->appendModel(data);
    }

    //TODO It was made only for test

    listModel->updateModel();

    playBackSessionsInterface.updateSessionListFinished();
}

void PlayBackSessions::currentPlaySessionChanged(uint64_t sessionID, bool state)
{
    int index = 0;
    for( auto &it : listModel->getSessionsList() )
    {
        if( it.sessionID == sessionID )
        {
            auto ret = listModel->setProperty(index, "current_play", state);
            listModel->updateModel();
            //TODO AUTOSCROLL
            QMetaObject::invokeMethod(currentItem, "setLastTablePos");
            break;
        }
        index++;
    }
}

void PlayBackSessions::slotPlay()
{
//    if( allSelected )
//        playBackSessionsInterface.playSelectedSessions(listModel->getSessionsList());
//    else
//    {
//        QList<SessionsModelNS::SessionData> list;
//        for(auto it : listModel->getSessionsList())
//        {
//            if( it.selected )
//            {
//                list.push_back(it);
//            }
//        }
//        playBackSessionsInterface.playSelectedSessions(list);
//    }

    playBackSessionsInterface.tapMenuPlay();
}

void PlayBackSessions::slotCopy()
{
    playBackSessionsInterface.showCopy();
}

void PlayBackSessions::slotSelect(QVariant row)
{

}

void PlayBackSessions::slotSelectAll()
{
    if( allSelected )
    {
        allSelected = false;
        listModel->deselectAllRows();

        for( auto &it : listModel->getSessionsList() )
        {
            playBackSessionsInterface.sessionSelectedStateChanged(it , false);
        }
        return;
    }
    allSelected = true;
    listModel->selectAllRows();

    for( auto &it : listModel->getSessionsList() )
    {
        playBackSessionsInterface.sessionSelectedStateChanged(it , true);
    }
}

void PlayBackSessions::slotGraph()
{
    playBackSessionsInterface.showGraph();
}

const QList<SessionsModelNS::SessionData> &PlayBackSessions::getSessionsList()
{
    return  listModel->getSessionsList();
}


void PlayBackSessions::showLoadingScreen(bool isShow)
{
    QMetaObject::invokeMethod(currentItem, "setBusy",
                              Q_ARG(QVariant, isShow));
}

void PlayBackSessions::deselectAllSessions()
{
    allSelected = false;
    listModel->deselectAllRows();

    for( auto &it : listModel->getSessionsList() )
    {
        playBackSessionsInterface.sessionSelectedStateChanged(it , false);
    }
}

void PlayBackSessions::showBlockScreen(bool isShow)
{
    QMetaObject::invokeMethod(currentItem, "setBlockScreen",
                              Q_ARG(QVariant, isShow));
}


