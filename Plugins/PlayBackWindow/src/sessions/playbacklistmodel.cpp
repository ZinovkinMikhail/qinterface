//
// Created by ivan on 23.10.2020.
//
#include <QDateTime>

#include "playbacklistmodel.h"


using namespace PlayBackListModelNS;
using namespace SessionsModelNS;

PlayBackListModel::PlayBackListModel(QObject *parent) : QAbstractListModel(parent)
{

}

int PlayBackListModel::rowCount(const QModelIndex &parent) const
{
    return listData.count();
}

QVariant PlayBackListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    SessionData data = listData.at(index.row());

    switch (role)
    {
        case StartTime:
            return QDateTime::fromTime_t(data.startTime).toUTC().toString("dd.MM.yyyy hh:mm");
        case EndTime:
            return QDateTime::fromTime_t(data.endTime).toUTC().toString("dd.MM.yyyy hh:mm");
        case Type:
        {
            QString channelTypeStr;

            if(data.channelMode == 0)
            {
                channelTypeStr = "Стерео - ";
            }
            else if(data.channelMode == 1)
            {
                channelTypeStr = "Моно - ";
            }

            if(data.channelMode == 0 && data.channelType == 3)
            {
                return channelTypeStr + QString::number(2);
            }
            else
            {
                return channelTypeStr + QString::number(data.channelType);
            }
        }
//            return data.channelType; //TODO get string from model
        case Duration:
        {
            QString res;
            int secsRange = data.duration;
            int hours = secsRange/3600;
            secsRange = secsRange - (hours * 3600);
            int minutes = secsRange/60;
            secsRange = secsRange - (minutes * 60);
            int seconds = secsRange/60;
            return res.sprintf("%02d ч. %02d м. %02d с.", hours, minutes, secsRange);
        }
        case Selected:
            return data.selected;
        case Size:
            return  (double)data.size;
        case Index:
            return index.row();
        case CurentPlay:
            return data.currentPlay;
        default:
            return QVariant();
    }
    return QVariant();
}

QHash<int, QByteArray> PlayBackListModel::roleNames() const
{
    QHash<int, QByteArray> roles =  QAbstractListModel::roleNames();
    roles[StartTime] = "time_start";
    roles[EndTime] = "time_end";
    roles[Duration] = "duration";
    roles[Size] = "size";
    roles[Type] = "type";
    roles[Selected] = "selected";
    roles[Index] = "index";
    roles[CurentPlay] = "current_play";
    return roles;
}

void PlayBackListModel::appendModel(SessionData data)
{
    listData.append(data);
}

void PlayBackListModel::clearModel()
{
    beginResetModel();
    listData.clear();
    endResetModel();
}

void PlayBackListModel::updateModel()
{
    beginResetModel();
    endResetModel();
}

/*
 *  ret session ID
 */
std::pair<bool, SessionsModelNS::SessionData> PlayBackListModel::setProperty(int index, QString type, int val)
{
    if( type == "selected")
    {
        listData[index].selected = val;
        return std::make_pair(true, listData[index]);
    }
    else if( type == "current_play")
    {
        if( index >= 0 && index < listData.size() )
        {
            listData[index].currentPlay = static_cast<bool>(val);
            return std::make_pair(true, listData[index]);
        }
    }
    return std::make_pair(false, SessionData());
}

void PlayBackListModel::selectAllRows()
{
    for(auto &it : listData )
    {
        it.selected = true;
    }
    beginResetModel();
    endResetModel();
}

void PlayBackListModel::deselectAllRows()
{
    for(auto &it : listData )
    {
        if( it.currentPlay )
            continue;
        it.selected = false;
    }
    beginResetModel();
    endResetModel();
}

const QList<SessionsModelNS::SessionData> &PlayBackListModel::getSessionsList()
{
    return listData;
}

bool PlayBackListModel::getSelectedState(int index) const
{
    return listData.at(index).selected;
}
