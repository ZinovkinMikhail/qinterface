//
// Created by ivan on 23.10.2020.
//

#ifndef CM_INTERFACE_PLAYBACKSESSIONS_H
#define CM_INTERFACE_PLAYBACKSESSIONS_H

#include <memory>
#include <string>
#include <utility>
#include <iterator>

#include <QObject>
#include <QQuickItem>

#include <rosslog/log.h>
#include <iqmlengine.h>

#include <iusermanipulators.h>

#include "playbacklistmodel.h"
#include "src/database/audiopkdworker.h"
#include "iplaybacksessions.h"
#include "src/filter/filtermodel.h"

#define SESSIONS_LIST_QML_PARH "qrc:/PlayBackWindow/PlayBackList.qml"

class PlayBackSessions: public QObject, public std::enable_shared_from_this<PlayBackSessions>
{
    Q_OBJECT
public:
    explicit PlayBackSessions(std::shared_ptr<IUserManipulators> userManipulators,
            std::shared_ptr<IQmlEngine> qmlEngine,
            std::shared_ptr<rosslog::Log> log,
            std::shared_ptr<AudioPKDWorker> pkdWorker,
            IPlayBackSessions &playBackSessionsInterface,
            QObject *parent = nullptr);
    ~PlayBackSessions();

    void init();
    void show();
    void hide();
    void showLoadingScreen(bool isShow);
    void showBlockScreen(bool isShow);
    void deselectAllSessions();
    void updateSessionList(size_t recordedSize, size_t recordedCapacity, SessionFilter filterData);
    void currentPlaySessionChanged(uint64_t sessionID, bool state);
    const QList<SessionsModelNS::SessionData> &getSessionsList();

private:
    void initSessionsListModel();

    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<rosslog::Log> logger;
    QQuickItem *currentItem;
    std::shared_ptr<PlayBackListModelNS::PlayBackListModel> listModel;
    std::shared_ptr<AudioPKDWorker> pkdWorker;
    IPlayBackSessions &playBackSessionsInterface;

    bool allSelected;

public slots:
    void slotRowChanged(int index, QString row, int value);
    void slotPlay();
    void slotCopy();
    void slotSelect(QVariant row);
    void slotSelectAll();
    void slotGraph();

};


#endif //CM_INTERFACE_PLAYBACKSESSIONS_H
