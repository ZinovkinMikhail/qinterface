//
// Created by ivan on 27.02.2021.
//
#include <assert.h>

#include "usbbusservice.h"

UsbBusService::UsbBusService(std::shared_ptr<data_provider::DataProvider> dataProvider,
                             std::shared_ptr<rosslog::Log> log) :
        dataProvider(dataProvider),
        logger(log),
        currentBusState(BUS_STATE_NONE),
        adapterStatusControl(nullptr)
{
    assert(dataProvider);
    assert(log);

    initAdapterStatusControl();
}


BusState UsbBusService::checkConnection()
{
    switch (static_cast<int>(currentBusState) )
    {
        case BUS_STATE_FOUND:
        {
            if( !checkDevAttach(foundedDevName) )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "DEVICE NOT LONGER ATTACHED");
                currentBusState = BUS_STATE_NOT_FOUND;
            }
            break;
        }

    }
    return currentBusState;
}

BusState UsbBusService::getCurrentBusState()
{
    return currentBusState;
}

int UsbBusService::requestDevice()
{
#ifdef  DEBUG_COPY_TEST
    currentBusState = BUS_STATE_SWITCHED;
    return 1;
#endif
    if( busMutex.try_lock() )
    {
        busWorker = std::async( std::launch::async,[this]
        {
            try
            {
                adapterStatusControl->requestUSB(dataProvider);
                currentBusState = BUS_STATE_SWITCHED;
            }
            catch (std::system_error &except )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ ,
                                      " Error while request USB %s", except.what());
                busMutex.unlock();
                return;
            }
            this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__ ,
                                  "Bus USB device requested");
            busMutex.unlock();
        });
        busWorker.wait_for(std::chrono::seconds(0));
    }
    else
    {
        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__ ,
                              "Request emmc bus device is busy!");
        return 0;
    }
    return 1;
}

void UsbBusService::initAdapterStatusControl()
{
    if(adapterStatusControl == nullptr)
    {
        adapterStatusControl = std::make_shared<adapter_status_ctrl::AdapterStatusControl>(dataProvider, *logger.get());
    }
}

int UsbBusService::returnDevice()
{
#ifdef  DEBUG_COPY_TEST
    currentBusState = BUS_STATE_NONE;
    return 1;
#endif
    if( busMutex.try_lock() )
    {
        busWorker = std::async( std::launch::async,[this]
        {
            try
            {
                adapterStatusControl->returnUSB(dataProvider);
                currentBusState = BUS_STATE_NONE;
            }
            catch (std::system_error &except )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ ,
                                      " Error while return USB %s", except.what());
                busMutex.unlock();
                return;
            }
            this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__ ,
                                  "Bus USB device returned");
            busMutex.unlock();
        });
        busWorker.wait_for(std::chrono::seconds(0));
    }
    else
    {
        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__ ,
                              "Request emmc bus device is busy!");
        return 0;
    }
    return 1;
}

bool UsbBusService::checkDevAttach(const std::string &devName)
{
    static QRegExp reIsDev( R"(sd?)" );
#if 0
    std::string fn_removable = std::string ("/sys/block/") + devName + "/removable";
    std::ifstream  ifs(  fn_removable.c_str() );
    bool is_removable = false;
    ifs >> is_removable;
    ifs.close();
    return is_removable;
#else //
    QString locDev = devName.c_str();;
    bool res = locDev.contains( reIsDev );
    
    return res;
#endif
}

int UsbBusService::getDeviceName(std::string &deviceName)
{
    if( currentBusState == BUS_STATE_FOUND )
    {
        deviceName = foundedDevName;
        return 0;
    }

    DIR * dp;
    if (NULL == (dp = opendir( "/sys/block" )))
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__  ,
                              "Error while openning dir /sys/block");
        return -1;
    }

    std::vector<std::string> devices;
    struct dirent *ent;

    while( NULL != (ent = readdir(dp)) )
    {
        if( ::strcmp(ent->d_name, "." ) == 0 ) continue;
        if( ::strcmp(ent->d_name, "..") == 0 ) continue;
        if( (ent->d_type == DT_DIR)  || (ent->d_type == DT_LNK) )
        {
            char sd_char;
            int res = sscanf(ent->d_name, "sd%c", &sd_char);
            if(res == 1 )
            {
                devices.push_back( ent->d_name);
            }
        }
    }
    if(dp) closedir(dp);

    for(auto &it : devices )
    {
        if( checkDevSize(it) > 0 )
        {
            if( checkDevAttach(it) )
            {
                deviceName = it;
                foundedDevName = deviceName;
                currentBusState = BUS_STATE_FOUND;
                break;
            }
        }
    }
    return 1;
}

bool UsbBusService::checkDevAttach(int dev)
{
    std::string ifname = ATTACH_DEV_FILE_N + std::to_string(dev);
    std::ifstream  ifs;
    ifs.open(ifname.c_str());
    if(!ifs)  return false;
    ifs.close();
    return true;
}

int UsbBusService::openDevice()
{
    this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "We can't open usb device DEPRECATED");
    return -1;
}

int UsbBusService::cloceDevice()
{
    this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "We can't close usb device DEPRECATED");
    return -1;
}

size_t UsbBusService::checkDevSize(const string &devName)
{
    std::string fn_size = std::string ("/sys/block/") + devName + "/size";
    std::ifstream  ifs(  fn_size.c_str() );
    size_t size = false;
    ifs >> size;
    ifs.close();
    return size;
}


