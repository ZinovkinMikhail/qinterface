//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_EMMCBUSSERVICE_H
#define CM_INTERFACE_EMMCBUSSERVICE_H

#include <memory>
#include <mutex>
#include <fcntl.h>

#include <dataprovider/dataprovider.h>
#include <dataprovider/recorderstatuscontrol.h>
#include <rosslog/log.h>

#include "ibusservice.h"

#define EMMC_DEV_NAME "mmcblk1"
#define EMMC_DEV_PATH "/dev/mmcblk1"
#define EMMC_MIN_SIZE 17068792320
#define UMS_LOCK_PATH "/tmp/UMSStoplock"

class EmmcBusService : public IBusService
{
public:
    EmmcBusService(std::shared_ptr<data_provider::DataProvider> dataProvider,
                   std::shared_ptr<rosslog::Log> log);

protected:
    BusState checkConnection() override;
    BusState getCurrentBusState() override;
    int requestDevice() override;
    int returnDevice() override;
    int getDeviceName(std::string &deviceName) override;
    int openDevice() override;
    int cloceDevice() override;

private:
    void initRecorderStatusControl();
    bool checkEmmcDevice();
    bool checkUMSStopLock();

    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusControl;
    BusState currentBusState;
    std::mutex busMutex;
    std::future<void> busWorker;

    int fd;
};


#endif //CM_INTERFACE_EMMCBUSSERVICE_H
