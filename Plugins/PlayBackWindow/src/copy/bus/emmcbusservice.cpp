//
// Created by ivan on 27.02.2021.
//

#include <assert.h>

#include "emmcbusservice.h"

using namespace rosslog;

EmmcBusService::EmmcBusService(std::shared_ptr<data_provider::DataProvider> dataProvider,
                               std::shared_ptr<rosslog::Log> log) :
       dataProvider(dataProvider),
       logger(log),
       recorderStatusControl(nullptr),
       currentBusState(BUS_STATE_NONE)
{
    assert(dataProvider);
    assert(log);

    fd = 0;

    initRecorderStatusControl();
}

BusState EmmcBusService::checkConnection()
{
    switch (static_cast<int>(currentBusState))
    {
        case BUS_STATE_SWITCHED:
        case BUS_STATE_FOUND:
        case BUS_STATE_NOT_FOUND:
        {
            if( checkEmmcDevice() )
                currentBusState = BUS_STATE_FOUND;
            else
                currentBusState = BUS_STATE_NOT_FOUND;
            break;
        }
    }
    return currentBusState;
}

BusState EmmcBusService::getCurrentBusState()
{
    return currentBusState;
}

int EmmcBusService::requestDevice()
{
#ifdef  DEBUG_COPY_TEST
    currentBusState = BUS_STATE_SWITCHED;
    return 1;
#endif
    if( busMutex.try_lock() )
    {
        busWorker = std::async( std::launch::async,[this]
        {
            try
            {
                if( checkEmmcDevice() )
                    currentBusState = BUS_STATE_FOUND;
                else
                {
                    if(!checkUMSStopLock())
                    {
                        this->recorderStatusControl->requestSDCard();
                        currentBusState = BUS_STATE_SWITCHED;
                    }
                    else
                    {
                        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Cant't request SD Card. UMSStopLock file exist!");
                        busMutex.unlock();
                        return;
                    }
                }
            }
            catch (std::system_error &except)
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while request SD Card");
                busMutex.unlock();
                return;
            }
            this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__ ,
                                  "Bus emmc device requested");
            busMutex.unlock();
        });
        busWorker.wait_for(std::chrono::seconds(0));
    }
    else
    {
        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__ ,
                              "Request emmc bus device is busy!");
        return 0;
    }
    return 1;
}

int EmmcBusService::returnDevice()
{
#ifdef  DEBUG_COPY_TEST
    currentBusState = BUS_STATE_NONE;
    return 1;
#endif
    if( busMutex.try_lock() )
    {
        busWorker = std::async( std::launch::async,[this]
        {
            try
            {
                this->recorderStatusControl->returnSDCard();
                currentBusState = BUS_STATE_NONE;
            }
            catch (std::system_error &except)
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while return SD Card");
                busMutex.unlock();
                return;
            }
            this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__ ,
                                  "Bus emmc device returned");
            busMutex.unlock();
        });
        busWorker.wait_for(std::chrono::seconds(0));
    }
    else
    {
        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__ ,
                              "Return emmc bus device is busy!");
        return 0;
    }
    return 1;
}

int EmmcBusService::getDeviceName(std::string &deviceName)
{
    return 0;
}

void EmmcBusService::initRecorderStatusControl()
{
    if( recorderStatusControl == nullptr )
    {
        recorderStatusControl = std::make_shared<recorder_status_ctrl::RecorderStatusControl>(dataProvider, *logger.get());
    }
}

bool EmmcBusService::checkEmmcDevice()
{
    FILE *fp = NULL;

    fp = fopen(EMMC_DEV_PATH, "r");

    if (fp == NULL)
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ ,"failed to fopen %s\n", EMMC_DEV_PATH);
        return false;
    }

    if (fseek(fp, 0, SEEK_END) == -1)
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ ,"failed to fseek %s\n", EMMC_DEV_PATH);
        fclose(fp);
        return false;
    }

    long int off = ftell(fp);

    if (off == (long)-1)
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ ,"failed to ftell %s\n", EMMC_DEV_PATH);
        fclose(fp);
        return false;
    }

    if( off > EMMC_MIN_SIZE )
        return true;

    return false;
}

int EmmcBusService::openDevice()
{
    std::cerr << "BUS STATE OPEN DEVICE = " << currentBusState << std::endl;
    if( currentBusState != BUS_STATE_FOUND ){
        this->logger->Message(LOG_ERR, __LINE__, __func__, "We do not have emmc device!!");
        return -1;
    }
    
    if( fd > 0 ){ //===== может закрыть и открыть?
        this->logger->Message(LOG_INFO, __LINE__, __func__ ,"Device already opened or not closed ");
        return fd;
    }

//    fd = open64(EMMC_DEV_PATH, O_RDONLY | __O_LARGEFILE);
    fd = open(EMMC_DEV_PATH, O_RDONLY);

    if( fd <= 0 ){
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error emmc device open");
        return -1;
    }
    return fd;
}

int EmmcBusService::cloceDevice()
{
    if( fd <= 0 )
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ , "Device already closed");
        return 0;
    }

    int ret = close(fd);
    fd = 0;

    if(ret != 0)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Error while closing emmc device %i", errno);
        return -1;
    }

    return 1;
}

bool EmmcBusService::checkUMSStopLock()
{
    for (int i = 0; i < 5; i++)
    {
        this->logger->Message(LOG_INFO, __LINE__, __func__ , "Check UMS stop lock file");
        if (FILE *file = fopen(UMS_LOCK_PATH, "r"))
        {
            fclose(file);
            this->logger->Message(LOG_INFO, __LINE__, __func__ , "UMS stop lock file exist!");
        }
        else
        {
            this->logger->Message(LOG_INFO, __LINE__, __func__ , "UMS stop lock file does't exist");
            return false;
        }

        sleep(1);
    }

    return true;
}
