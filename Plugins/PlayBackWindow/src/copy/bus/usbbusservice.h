//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_USBBUSSERVICE_H
#define CM_INTERFACE_USBBUSSERVICE_H

#include <memory>
#include <fstream>
#include <dirent.h>
#include <string>
#include <future>
#include <mutex>

#include <rosslog/log.h>
#include <dataprovider/dataprovider.h>
#include <dataprovider/adapterstatuscontrol.h>


#include "ibusservice.h"

#define ATTACH_DEV_FILE_N "/proc/scsi/usb-storage/"

class UsbBusService: public IBusService
{
public:
    UsbBusService(std::shared_ptr<data_provider::DataProvider> dataProvider,
    std::shared_ptr<rosslog::Log> log);

protected:
    BusState checkConnection() override;
    BusState getCurrentBusState() override;
    int requestDevice() override;
    int returnDevice() override;
    int getDeviceName(std::string &deviceName) override;
    int openDevice() override;
    int cloceDevice() override;

private:
    void initAdapterStatusControl();
    bool checkDevAttach(int dev);
    bool checkDevAttach(const std::string &devName);
    size_t checkDevSize(const std::string &devName);


    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusControl;
    BusState currentBusState;
    std::string foundedDevName;
    std::mutex busMutex;
    std::future<void> busWorker;


};


#endif //CM_INTERFACE_USBBUSSERVICE_H
