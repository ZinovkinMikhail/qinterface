//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_IBUSSERVICE_H
#define CM_INTERFACE_IBUSSERVICE_H

//#define DEBUG_COPY_TEST

typedef enum
{
    BUS_STATE_NONE,
    BUS_STATE_SWITCHED,
    BUS_STATE_NOT_FOUND,
    BUS_STATE_FOUND
} BusState;


class IBusService
{
public:
    virtual BusState checkConnection() = 0;
    virtual BusState getCurrentBusState() = 0;

    virtual int requestDevice() = 0;
    virtual int returnDevice() = 0;
    virtual int getDeviceName(std::string &deviceName) = 0;
    virtual int openDevice() = 0;
    virtual int cloceDevice() = 0;
};

#endif //CM_INTERFACE_IBUSSERVICE_H
