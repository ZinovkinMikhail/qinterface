//
// Created by ivan and yaroslav on 23.10.2020.
//

#include <cassert>
#include <QQmlProperty>
#include "copywindow.h"



CopyWindow::CopyWindow(std::shared_ptr<IUserManipulators> userManipulators,
                             std::shared_ptr<IQmlEngine> qmlEngine,
                             std::shared_ptr<rosslog::Log> log,
                             std::shared_ptr<data_provider::DataProvider> dataProvider,
                             IPlayBackCopy &playBackCopy,
                             std::shared_ptr<IStateWidget> stateWidget,
                             QObject *parent) :
        QObject(parent),
        userManipulators(userManipulators),
        qmlEngine(qmlEngine),
        logger(log),
        dataProvider(dataProvider),
        playBackCopyInterface(playBackCopy),
        currentItem(nullptr),
        copyY4CWorker(nullptr)
{
    assert(qmlEngine);
    assert(logger);
    assert(parent);
    assert(dataProvider);

    this->stateWidget = stateWidget;
    flashCapacity = 0;


    isShown = false;
}

CopyY4CState CopyWindow::getCopyState()
{
    if( copyY4CWorker != nullptr )
        return copyY4CWorker->getCurrentCopyState();
    return COPY_NONE;
}


void CopyWindow::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(
                qmlEngine->createComponent(COPY_QML_PARH));
        assert(currentItem);

        selectedToCopyText = currentItem->findChild<QQuickItem*>("SelectedToCopyText");
        assert(selectedToCopyText);

        selectedDurationText = currentItem->findChild<QQuickItem*>("SelectedDurationText");
        assert(selectedDurationText);

        externalStorageIcon = currentItem->findChild<QQuickItem*>("ExternalStorageIcon");
        assert(externalStorageIcon);

        storageFoundText = currentItem->findChild<QQuickItem*>("StorageFoundText");
        assert(storageFoundText);

        freeSpaceText = currentItem->findChild<QQuickItem*>("FreeSpaceText");
        assert(freeSpaceText);

        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));


        connect(currentItem, SIGNAL(copyButtonClicked()),
                this, SLOT(slotCopyButtonClicked()));
        connect(currentItem, SIGNAL(cancelButtonClicked()),
                this, SLOT(slotCancelButtonClicked()));
        connect(currentItem, SIGNAL(copySessionsChecked()),
                this, SLOT(slotCopySessionsChecked()));
        connect(currentItem, SIGNAL(copyAllChecked()),
                this, SLOT(slotCopyAllChecked()));
        connect(currentItem, SIGNAL(searchButtonClicked()),
                this, SLOT(slotSearchButtonClicked()));
        connect(currentItem, SIGNAL(closeButtonClicked()),
                this, SLOT(slotCloseButtonClicked()));
        connect(&copyWindowTimer, SIGNAL(timeout()),
                this, SLOT(slotCopyWindowTimeout()));

        connect(currentItem, SIGNAL(copyLogsClicked()),
                this, SLOT(slotCopyLogsClicked()));


        stateWidget->addState("USB_STORAGE_CONNECTED", INDICATOR, QPixmap(":/icons/flash-drive.png"), false);
        dataProvider->addDataWaiter(AXIS_INTERFACE_EVENT_STORAGE, shared_from_this());

    }

    initCopyY4CWorker();
}


void CopyWindow::show(uint64_t flashCapacity)
{   
    QQmlProperty(currentItem, "visible").write(true);

    if( !copyWindowTimer.isActive() )
    {
        copyWindowTimer.start(2000);
    }
    isShown = true;

    if( copyY4CWorker->getCurrentCopyState() != COPY_STARTED )
    {
        this->flashCapacity = flashCapacity;
        slotCopySessionsChecked();
        QMetaObject::invokeMethod(currentItem, "selectSessionsRadioButton");
        startDestDeviceSearch();
        QMetaObject::invokeMethod(currentItem,"setGuiByCopyEndState");
    }
    else
    {
        QMetaObject::invokeMethod(currentItem,"setGuiByCopyState");
    }
}


void CopyWindow::hide()
{
    copyWindowTimer.stop();
    isShown = false;

    if( copyY4CWorker->getCurrentCopyState() != COPY_STARTED )
    {
        if( copyY4CWorker != nullptr )
            copyY4CWorker->disconnectSearchedDevice();
    }
    else
    {

    }

    QQmlProperty(currentItem, "visible").write(false);
}

CopyWindow::~CopyWindow()
{
    if( copyY4CWorker != nullptr )
        copyY4CWorker->stopCopy();
}


void CopyWindow::sessionSelectedStateChanged(int sessionID, bool state)
{
    auto it = sessions.find(sessionID);
    if( it != sessions.end() )
    {
        sessions[sessionID].isSelected = state;
    }
    else
    {
        if( state )
        {
            SessionCopy _copy;
            _copy.isSelected = state;
            _copy.sessionID = sessionID;
            sessions[sessionID] = _copy;
        }
    }
}


void CopyWindow::setSelectedToCopyInfo(QQuickItem *item, QString selectedSize, QString selectedDuration)
{
    QQmlProperty(selectedToCopyText, "text").write(selectedSize);
    QQmlProperty(selectedDurationText, "text").write(selectedDuration);
}

void CopyWindow::setStorageFoundInfo(QQuickItem *item, bool storageFound, QString storageSize, QString freeSpace)
{
    QQmlProperty(externalStorageIcon, "visible").write(storageFound);
    QQmlProperty(storageFoundText, "text").write(storageSize);
    QQmlProperty(freeSpaceText, "text").write(freeSpace);
}


void CopyWindow::slotCopyButtonClicked()
{
    if( copyTypeSelected == COPY_TYPE_SESSIONS)
        copyY4CWorker->startCopy(sessionsForCopy, flashCapacity);
    else
    {
        std::vector<std::string> logs;
        logs.push_back(MAIN_LOG_NAME);
        logs.push_back(MAIN_LOG_BACK_NAME);
        logs.push_back(MAIN_ADAPTER_LOG_NAME);
        copyY4CWorker->startCopyLogs(logs, MAIN_LOG_PATH);
    }
}

void CopyWindow::slotCancelButtonClicked()
{
    //hide();
    if( this->userManipulators->requestUserConfirm("Остановить копирование информации?") )
    {
        QMetaObject::invokeMethod(currentItem,"setGuiByCopyStopState");
        copyY4CWorker->stopCopy();
    }
}

void CopyWindow::slotSearchButtonClicked()
{
    if( copyY4CWorker != nullptr )
    {
        searchStateVisual(true);
        copyY4CWorker->startSearchDestDevice();
    }
}

void CopyWindow::slotCloseButtonClicked()
{
    hide();
}

void CopyWindow::slotCopyWindowTimeout()
{
    std::cerr << "Copy state = " << copyY4CWorker->getCurrentCopyState() << std::endl;
    switch(copyY4CWorker->getCurrentCopyState() )
    {
        case COPY_END:
        {
            QMetaObject::invokeMethod(currentItem,"setGuiByCopyEndState");
            break;
        }
        case COPY_STOP:
        {
            QMetaObject::invokeMethod(currentItem,"setGuiByCopyStopState");
            break;
        }
    }
}

void CopyWindow::slotCopySessionsChecked()
{
    copyTypeSelected = COPY_TYPE_SESSIONS;

    sessionsForCopy.clear();
    playBackCopyInterface.getSelectedSessions(sessionsForCopy);

    uint64_t size = 0;
    uint64_t timeRec = 0;


    if( copyY4CWorker != nullptr )
    {
        copyY4CWorker->calculateSessionsSize(sessionsForCopy, size, timeRec);
    }

    double sizeDoub = size /1000.0 / 1000.0 / 1000.0;
    setSelectedToCopyInfo(currentItem,
                          ("Выбрано " + QString::number(sizeDoub, 'f', 2) + " Гб."), convertSecondsToHMS(timeRec) );
}

void CopyWindow::slotCopyAllChecked()
{
    copyTypeSelected = COPY_TYPE_SESSIONS;
    sessionsForCopy.clear();
    playBackCopyInterface.getAllSessions(sessionsForCopy);

    uint64_t size = 0;
    uint64_t timeRec = 0;


    if( copyY4CWorker != nullptr )
    {
        copyY4CWorker->calculateSessionsSize(sessionsForCopy, size, timeRec);
    }

    double sizeDoub = size /1000.0 / 1000.0 / 1000.0;
    setSelectedToCopyInfo(currentItem,
                          ("Выбрано " + QString::number(sizeDoub, 'f', 2) + " Гб."), convertSecondsToHMS(timeRec) );
}

QString CopyWindow::convertSecondsToHMS(uint64_t value)
{
    QString res;
    int seconds = (int) (value % 60);
    value /= 60;
    int minutes = (int) (value % 60);
    value /= 60;
    int hours = (int) (value);
    return res.sprintf("%02d час. %02d мин. %02d сек.", hours, minutes, seconds);
}

void CopyWindow::initCopyY4CWorker()
{
    if( copyY4CWorker == nullptr )
    {
        copyY4CWorker = std::make_shared<CopyY4CWorker>(this->dataProvider, this->logger, *this);
        copyY4CWorker->init();

        //copyY4CWorker = std::dynamic_pointer_cast<ICopyService>(copyWorker);
    }
}

void CopyWindow::copyProcessProcent(int procent, uint32_t timeLeft, int speed)
{
    this->playBackCopyInterface.copyProcessData(procent, timeLeft, speed);
}

void CopyWindow::copyProcessMessage(std::string message)
{
    this->userManipulators->showMessage(message);
}

void CopyWindow::copyProcessMessageConfirm(std::string message, int confirmState)
{

}

void CopyWindow::startDestDeviceSearch()
{
    if( copyY4CWorker != nullptr )
    {
        searchStateVisual(true);
        copyY4CWorker->startSearchDestDevice();
    }
}

void CopyWindow::destDeviceWasFound(const DeviceData deviceInfo)
{
    std::cerr << "Device was found " << deviceInfo.fsType << std::endl;

    double fullSize = deviceInfo.fullSize / 1000.0 / 1000.0 /1000.0;
    double emptySize = deviceInfo.emptySpace / 1000.0 / 1000.0 /1000.0;

    QString found = ("Найден внешний накопитель " + QString::number(fullSize, 'f', 2) + " Гб.");
    QString empty = ("Свободно " + QString::number(emptySize, 'f', 2) + " Гб.");
    setStorageFoundInfo(currentItem, true, found, empty);
    searchStateVisual(false);
    stateWidget->showState("USB_STORAGE_CONNECTED", "Внешний накопитель " + QString::number(fullSize, 'f', 2).toStdString() + " Гб.");
}

void CopyWindow::searchStateVisual(bool isShow)
{
    if(isShow)
    {
        QMetaObject::invokeMethod(currentItem, "searchStateON",
                                  Q_ARG(QVariant, "Определение внешнего накопителя..."));
    }
    else
    {
        QMetaObject::invokeMethod(currentItem,"searchStateOFF");
    }
}

void CopyWindow::copyWasStarted()
{
    QQmlProperty(currentItem, "visible").write(false);
    isShown = false;
    playBackCopyInterface.copyStarted();
}

void CopyWindow::copyEnded()
{
    if( !isShown )
        return;

//    if( copyY4CWorker != nullptr )
//    {
    QMetaObject::invokeMethod(currentItem,"notFoundState");
        //searchStateVisual(false);
//        copyY4CWorker->startSearchDestDevice();
//    }
}

void CopyWindow::deviceUmount()
{
    stateWidget->hideState("USB_STORAGE_CONNECTED");
}

void CopyWindow::recieveData(int command, std::vector<uint8_t> data, size_t size)
{
    if( command == AXIS_INTERFACE_EVENT_STORAGE )
    {
        std::cerr << "Ща отберут SDDD" << std::endl;
    }
}

void CopyWindow::showNotFoundState()
{
    QMetaObject::invokeMethod(currentItem,"notFoundState");
}

void CopyWindow::destDeviceNotFound()
{
    showNotFoundState();
}


void CopyWindow::slotCopyLogsClicked()
{
    copyTypeSelected = COPY_TYPE_LOGS;
}
