#ifndef COPYWINDOW_H
#define COPYWINDOW_H

#include <memory>
#include <string>
#include <utility>
#include <iterator>

#include <QObject>
#include <QQuickItem>

#include <rosslog/log.h>
#include <iqmlengine.h>

#include <iusermanipulators.h>
#include "src/database/audiopkdworker.h"
#include "icopyserviceprocess.h"
#include "copyy4cworker.h"
#include "icopyservice.h"
#include "iplaybackcopy.h"
#include "src/process/copyprocesswidget.h"
#include "istatewidget.h"


typedef enum
{
    COPY_TYPE_SESSIONS,
    COPY_TYPE_LOGS
} CopyTypeSelected;

#define COPY_QML_PARH "qrc:/PlayBackWindow/CopyWindow.qml"
#define COPY_PROCESS_WIDGET_NAME "Копирование"

#define MAIN_LOG_NAME "cm_interface"
#define MAIN_LOG_BACK_NAME "cm_interface.back"
#define MAIN_ADAPTER_LOG_NAME "adapter.log"
#define MAIN_LOG_PATH "/tmp"

typedef struct
{
    bool isSelected;
    int sessionID;
} SessionCopy;

class CopyWindow: public QObject, public IDataWaiter,
        public std::enable_shared_from_this<CopyWindow>,
        public ICopyServiceProcess
{
    Q_OBJECT
public:
    explicit CopyWindow(std::shared_ptr<IUserManipulators> userManipulators,
    std::shared_ptr<IQmlEngine> qmlEngine,
    std::shared_ptr<rosslog::Log> log,
    std::shared_ptr<data_provider::DataProvider> dataProvider,
    IPlayBackCopy &playBackCopy,
    std::shared_ptr<IStateWidget> stateWidget,
    QObject *parent = nullptr);

    CopyY4CState getCopyState();

    void init();
    void show(uint64_t flashCapacity);
    void hide();

    ~CopyWindow();

    void sessionSelectedStateChanged(int sessionID, bool state);

protected:
    void copyProcessProcent(int procent, uint32_t timeLeft, int speed) override;
    void copyProcessMessage(std::string message) override;
    void copyProcessMessageConfirm(std::string message, int confirmState) override;
    void destDeviceWasFound(const DeviceData deviceInfo) override;
    void destDeviceNotFound() override;
    void copyWasStarted() override;
    void copyEnded() override;
    void deviceUmount() override;

    void recieveData(int command, std::vector<uint8_t> data,
                        size_t size);

private:
    void initCopyY4CWorker();
    void setSelectedToCopyInfo(QQuickItem *item, QString selectedSize, QString selectedDuration);
    void setStorageFoundInfo(QQuickItem *item, bool storageFound, QString storageSize, QString freeSpace);
    void startDestDeviceSearch();
    void searchStateVisual(bool isShow);
    void showNotFoundState();
    QString convertSecondsToHMS(uint64_t value);

    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<IUserManipulators> userManipulators;
    QQuickItem *currentItem;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::map<int, SessionCopy> sessions;
    std::shared_ptr<CopyY4CWorker> copyY4CWorker;
    IPlayBackCopy &playBackCopyInterface;
    std::shared_ptr<IStateWidget> stateWidget;

    std::vector<SessionsModelNS::SessionData> sessionsForCopy;

    QQuickItem *selectedToCopyText;
    QQuickItem *selectedDurationText;
    QQuickItem *externalStorageIcon;
    QQuickItem *storageFoundText;
    QQuickItem *freeSpaceText;

    bool isShown;
    uint64_t flashCapacity;

    QTimer copyWindowTimer;

    CopyTypeSelected copyTypeSelected{COPY_TYPE_SESSIONS};



public slots:
//    void slotShowDateComboDrum(int type);
    void slotCopyButtonClicked();
    void slotCancelButtonClicked();
    void slotCopySessionsChecked();
    void slotCopyAllChecked();
    void slotSearchButtonClicked();
    void slotCloseButtonClicked();
    void slotCopyWindowTimeout();
    void slotCopyLogsClicked();

};


#endif //COPYWINDOW_H
