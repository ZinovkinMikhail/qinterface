//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_ICOPYSERVICE_H
#define CM_INTERFACE_ICOPYSERVICE_H

#include <QList>

#include "src/sessions/sessionsmodel.h"

class ICopyService
{
public:
    virtual bool startCopy(std::vector<SessionsModelNS::SessionData> sessions, uint64_t flashCapacity) = 0;
};


#endif //CM_INTERFACE_ICOPYSERVICE_H
