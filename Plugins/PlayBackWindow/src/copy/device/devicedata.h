//
// Created by ivan on 01.03.2021.
//

#ifndef CM_INTERFACE_DEVICEDATA_H
#define CM_INTERFACE_DEVICEDATA_H

#include <stdint.h>

typedef enum
{
    FS_FAT,
    FS_EXT2,
    FS_EXT3,
    FS_EXT4,
    FS_NTFS,
    FS_EXFAT,
    FS_FUSE
} FileSystemType;

typedef struct
{
    size_t fullSize;
    size_t emptySpace;
    FileSystemType fsType;
} DeviceData;

#endif //CM_INTERFACE_DEVICEDATA_H
