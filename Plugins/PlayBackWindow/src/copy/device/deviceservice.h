//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_DEVICESERVICE_H
#define CM_INTERFACE_DEVICESERVICE_H

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fstream>
#include <string>
#include <memory>
#include <sstream>
#include <mntent.h>

#include <QObject>
#include <QDir>
#include <QFile>
#include <QStorageInfo>
#include <QDateTime>

#include <rosslog/log.h>

#include "devicedata.h"

#define FS_TYPE_NTFS "ntfs"
#define FS_TYPE_FAT "fat"
#define FS_TYPE_EXT2 "ext"
#define FS_TYPE_EXT3 "ext3"
#define FS_TYPE_EXT4 "ext4"
#define FS_TYPE_EXFAT "exfat"
#define FS_TYPE_FUSE "fuseblk"

#define DEVICE_MOUNT_PATH "/tmp/deviceMountPath"
#define DEFAULT_REC_FOLDER "REC"


class DeviceService : public QObject
{
    Q_OBJECT
public:
    explicit DeviceService(std::shared_ptr<rosslog::Log> log, QObject *parent = nullptr);

    int mountDevice(const std::string &deviceName);
    int umountDevice(const std::string &deviceName);
    std::pair<bool, DeviceData> getDeviceInfo(const std::string &deviceName);


private:
    int checkDevMount(const std::string &devPath, std::string &mountedPath);
    int umountPath(const std::string &mountPath);

    std::shared_ptr<rosslog::Log> logger;
    std::string deviceMountFS;

    QFile *file;
};


#endif //CM_INTERFACE_DEVICESERVICE_H
