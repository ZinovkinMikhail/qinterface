//
// Created by ivan on 27.02.2021.
//

#include <assert.h>
#include <iostream>

#include "deviceservice.h"

DeviceService::DeviceService(std::shared_ptr<rosslog::Log> log, QObject *parent) :
    QObject(parent),
    logger(log),
    deviceMountFS("")
{
    assert(log);
    file = new QFile();
}

int DeviceService::mountDevice(const std::string &deviceName)
{
    //У нас только одна партиция с другими не работаем
    struct stat st = {0};
    int ret = 0;

    std::string devPath = "/dev/" + deviceName + "1";
    std::string mountedPath;

    if( checkDevMount(devPath, mountedPath) )
    {
        if( std::string(mountedPath).compare(DEVICE_MOUNT_PATH) != 0 && !mountedPath.empty())
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Device mounted not in our location");
            if( umountPath(mountedPath) <= 0 )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "We can't umount device");
                return -1;
            }
        }
        else
        {
            std::cerr << "Device already mounted!!" << std::endl;
            //Have to check that mounted at our path?
            this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__, "Device already mounted");
            return 1;
        }
    }



    if( stat(devPath.c_str(), &st) == -1 )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Partition doesn't exist %s" , devPath.c_str());
        return -1;
    }

    if (stat(DEVICE_MOUNT_PATH, &st) == -1)
    {
        ret = mkdir(DEVICE_MOUNT_PATH, 0700);
        if( ret < 0 )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Can't create mount dir %s" , DEVICE_MOUNT_PATH);
            return -1;
        }
    }
    sync();

    usleep(500);

    ret = system(std::string("mount " + devPath + " " + DEVICE_MOUNT_PATH).c_str() );
    if( ret < 0 || ret == 256 )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Can't mount dev %s" , devPath.c_str());
        return -1;
    }
    return 1;
}

int DeviceService::checkDevMount(const std::string &devPath, std::string &mountedPath)
{
    if(devPath.empty())
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error can't check mount path devPath is empty!");
        return -1;
    }

    struct mntent *ent;
    FILE *aFile;

    aFile = setmntent("/proc/mounts", "r");
    if (aFile == NULL) {
        perror("setmntent");
        return 0;
    }
    while (NULL != (ent = getmntent(aFile)))
    {
        if( devPath == std::string(ent->mnt_fsname ) )
        {
            mountedPath = ent->mnt_dir;
            this->deviceMountFS = ent->mnt_type;
            printf("Found %s\n", ent->mnt_fsname);
            printf("Found %s\n", mountedPath.c_str());
            printf("Found %s\n", this->deviceMountFS.c_str());;
            endmntent(aFile);
            return 1;
        }
    }
    endmntent(aFile);

    return 0;
}

int DeviceService::umountDevice(const std::string &deviceName)
{
    std::string devPath = "/dev/" + deviceName + "1";

//    int ret = system(std::string("umount " + devPath ).c_str() );
//    if( !ret )
//    {
//        std::cerr << "CLOSE ERRNO =  " << errno << std::endl;
//        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Can't umount dev %s" , devPath.c_str());
//        return -1;
//    }

    int _counter = 0;
    while (system(std::string("umount " + devPath ).c_str()) != 0)
    {
        int _errsv = errno;
        if(_errsv == EBUSY)
        {
            _counter++;
            std::cerr << devPath.c_str() << " is busy. Try umount count = " << _counter << std::endl;
            usleep(2000000);

            if(_counter == 10)
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Can't umount dev %s" , devPath.c_str());
                return -1;
            }
        }
        else
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Can't umount dev %s" , devPath.c_str());
            return -1;
        }

    }

    std::cerr << "Device umount OK " << devPath.c_str() << std::endl;

    return 1;
}

std::pair<bool, DeviceData> DeviceService::getDeviceInfo(const std::string &deviceName)
{
    struct statvfs buf;
    DeviceData data;
    memset(&data, 0, sizeof(DeviceData));
    int ret =  statvfs ( DEVICE_MOUNT_PATH, &buf );

    if ( ret < 0 )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while getting info about device fs");
        return std::make_pair(false, data);
    }

    std::string devPath = "/dev/" + deviceName + "1";
    std::string mountedPath;

    if( checkDevMount( devPath, mountedPath ) <= 0 )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while check dev mount");
        return std::make_pair(false, data);
    }

    data.emptySpace = buf.f_bfree * buf.f_bsize;
    data.fullSize = buf.f_blocks * buf.f_bsize;

    if( !deviceMountFS.empty() )
    {
            if( deviceMountFS == FS_TYPE_NTFS )
                data.fsType = FS_NTFS;
            else if( deviceMountFS == FS_TYPE_FAT )
                data.fsType = FS_FAT;
            else if( deviceMountFS == FS_TYPE_EXT2 )
                data.fsType = FS_EXT2;
            else if( deviceMountFS == FS_TYPE_EXT3 )
                    data.fsType = FS_EXT3;
            else if( deviceMountFS == FS_TYPE_EXT4 )
                    data.fsType = FS_EXT4;
            else if( deviceMountFS == FS_TYPE_EXFAT )
                    data.fsType = FS_EXFAT;
            else if( deviceMountFS == FS_TYPE_FUSE )
                    data.fsType = FS_FUSE;
    }

    return std::make_pair(true, data);
}

int DeviceService::umountPath(const std::string &mountPath)
{
    int ret = system(std::string("umount " + mountPath ).c_str() );
    if( ret < 0 )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Can't umount dev %s" , mountPath.c_str());
        return -1;
    }
    return 1;
}


