//
// Created by ivan on 27.02.2021.
//
#include "senddataservice.h"

#include <assert.h>
#include <string.h>
#include <sys/select.h>
#include <fcntl.h>

SendDataService::SendDataService(std::shared_ptr<rosslog::Log> log) :
    logger(log)
{
    assert(log);
    memset(&buff, 0 , BUFF_SIZE);

}



int SendDataService::sendData(int fromFD, int toFD, size_t nbytes)
{
    if(nbytes > BUFF_SIZE){
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Bytes for copy > BUFF_SIZE");
        return -1;
    }


    fd_set read_fds;
    fd_set write_fds;

    struct timeval tv;

    FD_ZERO(&read_fds);
    FD_SET(fromFD, &read_fds);

    FD_ZERO(&write_fds);
    FD_SET(toFD, &write_fds);


    tv.tv_usec = 0;
    tv.tv_sec = 1;

    int activity = select(fromFD + 1, &read_fds, NULL, NULL, &tv);

    if( activity <= 0 ){ //==== Re: 0 == timeout
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "SELECT read ERROR : %s", strerror(errno) );
        return -1;
    }

    if (FD_ISSET(fromFD, &read_fds)){

        int readRet = read(fromFD, buff, nbytes);

        if( readRet <= 0 ){
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error read file id=%d nbytes=%u: %s", fromFD, nbytes, strerror(errno) );
            return readRet;
        }

        activity = select(toFD + 1, NULL, &write_fds, NULL, &tv);

        if( activity <= 0 ){ //==== Re: 0 == timeout
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "SELECT write Error: %s", strerror(errno) );
            return -1;
        }

        if (FD_ISSET(toFD, &write_fds) ){

            int writeRet = write(toFD, buff, readRet);

            if( writeRet <= 0 ){
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error write file id=%d: %s", toFD, strerror(errno) );
            }
            return writeRet;
        }
    }

    return -1;
}
