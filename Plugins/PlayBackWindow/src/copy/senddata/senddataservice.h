//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_SENDDATASERVICE_H
#define CM_INTERFACE_SENDDATASERVICE_H

#include <unistd.h>
#include <memory>
#include <rosslog/log.h>

#define BUFF_SIZE 4096


class SendDataService
{

public:
    explicit SendDataService(std::shared_ptr<rosslog::Log> log);

    int sendData(int fromFD, int toFD, size_t nbytes);


private:
    std::shared_ptr<rosslog::Log> logger;

    char buff[BUFF_SIZE];


};


#endif //CM_INTERFACE_SENDDATASERVICE_H
