//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_ICOPYSERVICEPROCESS_H
#define CM_INTERFACE_ICOPYSERVICEPROCESS_H

#include <cstdint>
#include <string>

#include "device/devicedata.h"

class ICopyServiceProcess
{
public:
    virtual void copyProcessProcent(int procent, uint32_t timeLeft, int speed) = 0;
    virtual void copyProcessMessage(std::string message) = 0;
    virtual void copyProcessMessageConfirm(std::string message, int confirmState) = 0;
    virtual void destDeviceWasFound(const DeviceData deviceInfo) = 0;
    virtual void destDeviceNotFound() = 0;
    virtual void deviceUmount() = 0;
    virtual void copyWasStarted() = 0;
    virtual void copyEnded() = 0;
};

#endif //CM_INTERFACE_ICOPYSERVICEPROCESS_H
