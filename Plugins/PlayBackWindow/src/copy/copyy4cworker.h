//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_COPYY4CWORKER_H
#define CM_INTERFACE_COPYY4CWORKER_H

#include <fstream>
#include <sys/stat.h>
#include <ios>
#include <fcntl.h>
#include <unistd.h>
#include <mutex>

#include <sys/types.h>

#include <unistd.h>
#include <thread>

#include <sys/sendfile.h>

#include <QObject>
#include <QTimer>

#include <dataprovider/dataprovider.h>
#include <dataprovider/adapterstatuscontrol.h>
#include <rosslog/log.h>

#include "icopyservice.h"
#include "icopyserviceprocess.h"
#include "bus/usbbusservice.h"
#include "bus/emmcbusservice.h"
#include "bus/ibusservice.h"
#include "device/deviceservice.h"
#include "file/fileservice.h"
#include "senddata/senddataservice.h"


typedef enum
{
    COPY_NONE,
    COPY_SEARCH_SRC_DEVICE,
    COPY_TRY_START_COPY,
    COPY_STARTED,
    COPY_ERROR,
    COPY_END,
    COPY_STOP
} CopyY4CState;

typedef enum
{
    COPY_PROCESS_OK,
    COPY_PROCESS_REOPEN_DEST,
    COPY_PROCESS_REOPEN_SRC,
    COPY_PROCESS_RECALL_SD,
    COPY_PROCESS_REMOUNT_DEST,
    COPY_PROCESS_RECALL_USB,
    COPY_CRITICAL_ERROR
} CopyY4CProcessState;

typedef enum
{
    COPY_ERROR_NONE,
    COPY_ERROR_FIND_SRC_DEV,
    COPY_ERROR_FIND_DST_DEV,
    COPY_ERROR_THREAD,
    COPY_ERROR_SENDFILE,
    COPY_ERROR_PREPARE_DIRECTORY,
    COPY_ERROR_PREPARE_FILE,
    COPY_ERROR_OPEN_SRC_DEV,
    COPY_ERROR_OPEN_DST_FILE,
    COPY_ERROR_PREPARE_SESSIONS,
    COPY_ERROR_SEEK
} CopyY4CErrorCodes;

typedef struct
{
    std::string destFileName;
    uint64_t lastSyncPos;
    uint64_t writedData;
    CopyY4CErrorCodes errorCode;
    CopyY4CProcessState processState;
    int copyErrorCounter;
    int sessionStopPos;
    uint64_t sessionTotalToWrite;
    int totalSplitFs;
    bool relaunch;
    int searchDestCounter;
} CopyY4CData;

#define COPY_START_ERROR_TIMES 5 //Seconds
#define COPY_BLOCK_SIZE 4096
#define __USE_FILE_OFFSET64 1
#define __USE_LARGEFILE64 1
#define DEST_FILE_SIZE 4147483648
#define SYNC_PERIOD 419430400

class CopyY4CWorker : public QObject, public ICopyService
{
    Q_OBJECT
public:
    CopyY4CWorker(std::shared_ptr<data_provider::DataProvider> dataProvider,
                  std::shared_ptr<rosslog::Log> log,
                  ICopyServiceProcess &copyProcessInterface,
                  QObject *parent = nullptr);

    void init();
    void startCopy();
    void startSearchDestDevice();
    void disconnectSearchedDevice();

    bool stopCopy();
    bool startCopy(std::vector<SessionsModelNS::SessionData> sessions, uint64_t flashCapacity) override;
    bool startCopyLogs(std::vector<std::string> logs, std::string logPath);
    void calculateSessionsSize(const std::vector<SessionsModelNS::SessionData> &sessions,
                                uint64_t &totalBytesForCopy, uint64_t &recTime);

    bool doError();
    CopyY4CState getCurrentCopyState();

    void prepareToDelete();

    ~CopyY4CWorker();

private:
    void initBusServices();
    void initDeviceService();
    void initFileService();
    void initSendDataService();
    void closeDevices();
    int umountDevice(std::string deviceName);
    void stopCopyOnError(const std::string textRus);
    bool prepareDestRootFolders(std::string &serialFolder);
    int prepareFileForCopy(const std::string &folderPath, const uint64_t &startTime, CopyY4CData &currentData);
    void copyThreadFunc();
    bool relaunchCopy();
    void startCopyLogsThread();

    void updateTimeLeftAndSpeed ( uint64_t totalSizeToCopy, uint64_t curCopied,
            const std::chrono::time_point<std::chrono::system_clock> &timeStart );
    void updatePercent ( uint64_t totalSizeToCopy, uint64_t curCopied );
    std::string format_duration( std::chrono::milliseconds ms );
    std::pair<bool, std::string> getAdapterSerial();
    void prepareSessionsForCopy(std::vector<SessionsModelNS::SessionData> &copySessionsNEW,
                                uint64_t &totalBytesForCopy);
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    ICopyServiceProcess &copyProcessInterface;


    int errorCOunt = 0; //FOR TEST
    int errorCopyCount;

    QTimer copySearchDestTimer;
    QTimer prepareCopyTimer;
    QTimer prepareCopyLogsTimer;

    std::string deviceName;

    std::shared_ptr<IBusService> srcBusService;
    std::shared_ptr<IBusService> destBusService;
    std::shared_ptr<DeviceService> deviceService;
    std::shared_ptr<FileService> fileService;
    std::shared_ptr<SendDataService> sendDataServiceService;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusControl;
    SoftAdapterDeviceCurrentStatus_t adapterStatus;

    CopyY4CState currentState;

    bool isThreadWorking;
    std::thread copyThread;

    int copyStarterrorCounter;
    int lastPercent;
    bool emulateError{false};

    off64_t totalToWrite;

    DeviceData destDeviceData;
    CopyY4CData currentCopyData{};

    std::vector<SessionsModelNS::SessionData> copySessions;
    std::vector<std::string> copyLogs;
    std::string logMainPath;

    std::atomic<float> percent;
    std::atomic<float> speed;
    std::atomic<uint32_t> timeLeft;

//    std::chrono::steady_clock::time_point copyStartTime;
    std::chrono::steady_clock::time_point copyStartTime;
    uint64_t flashCapacity;

    std::future<void> copyLogsWorker;
    size_t copyLogsTotalSize{0};


signals:
    void signalCopyPercentUpdate(int percent, int timeLeft, int speed);
    void signalStopCopy(bool isError, std::string errorMsg);
    void signalShowMessage(std::string msg);

public slots:
    void slotSearchDestTimerTimeout();
    void slotPrepareCopyTimerTimeout();
    void slotCheckBusesTimerTimeout();
    void slotCopyPercentUpdate(int percent, int timeLeft, int speed);
    void slotStopCopy(bool isError, std::string errorMsg);
    void slotShowMessage(std::string msg);
    void slotPrepareCopyLogsTimerTimeout();
};


#endif //CM_INTERFACE_COPYY4CWORKER_H
