//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_FILESERVICE_H
#define CM_INTERFACE_FILESERVICE_H

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fstream>
#include <string>
#include <memory>
#include <iostream>

#include <QDir>
#include <QFile>
#include <QStorageInfo>
#include <QDateTime>

#include <rosslog/log.h>

class FileService : public QObject
{
    Q_OBJECT
public:
    explicit FileService(std::shared_ptr<rosslog::Log> log, QObject *parent = nullptr);

    int checkCopyFolderExist(const std::string &dirPath);
    int createFileForCopy(const std::string &dirPath, const uint64_t &timeStart, std::string &filePath);
    int createLogFileForCopy(const std::string &dirPath, const std::string fileName, std::string &filePath);
    int createDir(const std::string &dirPath);
    int openFile(const std::string &filePath);

private:
    int checkFilePathExist(std::string filePath);

    std::shared_ptr<rosslog::Log> logger;
};


#endif //CM_INTERFACE_FILESERVICE_H
