//
// Created by ivan on 27.02.2021.
//

#include <cassert>

#include "fileservice.h"

FileService::FileService(std::shared_ptr<rosslog::Log> log, QObject *parent) :
    QObject(parent),
    logger(log)
{
    assert(log);
}

int FileService::checkCopyFolderExist(const std::string &dirPath)
{
    if( dirPath.empty() )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Dir path is empty");
        return -1;
    }
    QDir dir = QDir(QString::fromStdString(dirPath));

    if( dir.exists() )
    {
        return 1;
    }
    return 0;
}

int FileService::createFileForCopy(const std::string &dirPath, const uint64_t &timeStart, std::string &filePath)
{
    if( dirPath.empty() )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Dir path is empty");
        return -1;
    }
    QFile file;
    QDateTime dateTime = QDateTime::fromSecsSinceEpoch(timeStart);

    std::string timeStr = dateTime.toUTC().toString("yyyy.MM.dd_hh-mm-ss").toStdString();
    filePath = dirPath + "/" + "rec_" + timeStr + ".ctf";

    if(checkFilePathExist(filePath))
    {
        for( int i = 1;  i < 120 ; i++)
        {
            if( checkFilePathExist((dirPath + "/" + "rec_" + timeStr + "(" + std::to_string(i) + ")" + ".ctf")) )
                continue;
            else
            {
                filePath = (dirPath + "/" + "rec_" + timeStr + "(" + std::to_string(i) + ")" + ".ctf");
                file.setFileName(QString::fromStdString((filePath)));

//
//                if( !file->open(QIODevice::ReadWrite) )
//                {
//                    this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while file create");
//                    return -1;
//                }
//                file->close();
                return 1;
            }
        }
    }
    else
    {
        file.setFileName(QString::fromStdString((filePath)));
        if( !file.open(QIODevice::ReadWrite) )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while file create");
            return -1;
        }
        file.close();
    }
    return 1;
}

int FileService::createLogFileForCopy(const std::string &dirPath, const std::string fileName, std::string &filePath)
{
    if( dirPath.empty() )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Dir path is empty");
        return -1;
    }
    QFile file;

    filePath = dirPath + "/" + fileName + ".log";

    if(checkFilePathExist(filePath))
    {
        for( int i = 1;  i < 120 ; i++)
        {
            if( checkFilePathExist((dirPath + "/" + fileName + "(" + std::to_string(i) + ")" +  ".log")) )
                continue;
            else
            {
                filePath = (dirPath + "/" + fileName + "(" + std::to_string(i) + ")" +  ".log");
                file.setFileName(QString::fromStdString((filePath)));

//
//                if( !file->open(QIODevice::ReadWrite) )
//                {
//                    this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while file create");
//                    return -1;
//                }
//                file->close();
                return 1;
            }
        }
    }
    else
    {
        file.setFileName(QString::fromStdString((filePath)));
        if( !file.open(QIODevice::ReadWrite) )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while file create");
            return -1;
        }
        file.close();
    }
    return 1;
}

int FileService::createDir(const std::string &dirPath)
{
    if( dirPath.empty() )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Dir path is empty");
        return -1;
    }
    if( QDir().mkdir(QString::fromStdString(dirPath)) )
    {
        return 1;
    }
    return -1;
}

int FileService::openFile(const std::string &filePath)
{
//    int fd = open(filePath.c_str(), O_RDWR  | O_CREAT,
//                  S_IROTH | S_IWOTH );

    int fd = open(filePath.c_str(), O_RDWR  | O_CREAT); //TRY ADD 0644 mode

    if( fd <= 0 )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "ERROR while open file for copy errno = %i", errno);
        return -1;
    }
    return fd;
}

int FileService::checkFilePathExist(std::string filePath)
{
    QString filePathStr = QString::fromStdString(filePath);

    if( filePathStr.isEmpty() )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error file path is empty");
        return 0;
    }

    //file->setFileName(filePathStr);

    if( QFile::exists(filePathStr))
    {
        return 1;
    }
    return 0;
}
