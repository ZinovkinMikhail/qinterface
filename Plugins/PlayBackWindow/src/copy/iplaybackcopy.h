//
// Created by ivan on 27.02.2021.
//

#ifndef CM_INTERFACE_IPLAYBACKCOPY_H
#define CM_INTERFACE_IPLAYBACKCOPY_H

#include <vector>

#include "src/sessions/sessionsmodel.h"

class IPlayBackCopy
{
public:
    virtual bool getAllSessions(std::vector<SessionsModelNS::SessionData> &sessions) = 0;
    virtual bool getSelectedSessions(std::vector<SessionsModelNS::SessionData> &sessions) = 0;
    virtual void copyProcessData(int percent, uint32_t timeLeft, int speed) = 0;
    virtual void copyStarted() = 0;
};

#endif //CM_INTERFACE_IPLAYBACKCOPY_H
