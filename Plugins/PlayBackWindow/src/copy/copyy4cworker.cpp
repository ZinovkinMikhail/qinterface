//
// Created by ivan on 27.02.2021.
//

#include <assert.h>

#include "copyy4cworker.h"

#include <errormessageformat.h>
#include <string.h>

using namespace adapter_status_ctrl;

CopyY4CWorker::CopyY4CWorker(std::shared_ptr<data_provider::DataProvider> dataProvider,
                             std::shared_ptr<rosslog::Log> log,
                             ICopyServiceProcess &copyProcessInterface,
                             QObject *parent) :
     QObject(parent),
     dataProvider(dataProvider),
     logger(log),
     copyProcessInterface(copyProcessInterface),
     srcBusService(nullptr),
     destBusService(nullptr),
     deviceService(nullptr),
     fileService(nullptr),
     sendDataServiceService(nullptr),
     currentState(COPY_NONE)
{
    assert(dataProvider);
    assert(log);

    isThreadWorking = false;
    copyStarterrorCounter = 0;
    timeLeft = 0;
    speed = 0;
    flashCapacity = 0;

    qRegisterMetaType<std::string>("std::string");

}

void CopyY4CWorker::init()
{
    connect(&copySearchDestTimer, SIGNAL(timeout()),
                     this, SLOT(slotSearchDestTimerTimeout()));
    connect(&prepareCopyTimer, SIGNAL(timeout()),
            this, SLOT(slotPrepareCopyTimerTimeout()));
    connect(this, SIGNAL(signalCopyPercentUpdate(int, int, int)),
                         this, SLOT(slotCopyPercentUpdate(int, int, int)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalStopCopy(bool, std::string)),
            this, SLOT(slotStopCopy(bool, std::string)),Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(signalShowMessage(std::string)),
            this, SLOT(slotShowMessage(std::string)),Qt::BlockingQueuedConnection);

    adapterStatusControl = std::make_shared<AdapterStatusControl>(dataProvider, *logger.get());
    connect(&prepareCopyLogsTimer, SIGNAL(timeout()),
            this, SLOT(slotPrepareCopyLogsTimerTimeout()));

    initBusServices();
    initDeviceService();
    initFileService();
    initSendDataService();
}

bool CopyY4CWorker::startCopy(std::vector<SessionsModelNS::SessionData> sessions, uint64_t flashCapacity)
{
    copyStarterrorCounter = 0;
    lastPercent = 0;
    currentCopyData = {};
    this->flashCapacity = flashCapacity;

    if( sessions.empty() )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Sessions for copy is empty");
        this->copyProcessInterface.copyProcessMessage("Не выбраны сессии для копирования");
        return false;
    }

    copySessions = sessions;

    if( currentState == COPY_SEARCH_SRC_DEVICE  || currentState == COPY_STARTED )
    {
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Копирование уже запущено, дождитесь завершения",
                                                                                       Y4C_COPY_ERROR_ALREADY_RUN_START, 0));
        return false;
    }


    if( destBusService->checkConnection() != BUS_STATE_FOUND )
    {
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Внешний накопитель не найден", Y4C_COPY_ERROR_EXT_DEVICE_NOT_FOUND, 0));
        this->copyProcessInterface.destDeviceNotFound();
        currentState = COPY_ERROR;
        return false;
    }


    //Check if USB have free space for copy
    uint64_t sessionsSize = 0;
    uint64_t timeRec = 0;


    calculateSessionsSize(sessions, sessionsSize, timeRec);


    auto _ret = deviceService->getDeviceInfo(deviceName);

    if( _ret.first )
    {
        if((int64_t)(_ret.second.emptySpace - sessionsSize) <= 0)
        {
            this->copyProcessInterface.copyProcessMessage("Недостаточно свободного места на внешнем накопителе");
            return false;
        }
    }
    ////

    currentState = COPY_SEARCH_SRC_DEVICE;

    if( !prepareCopyTimer.isActive() )
        prepareCopyTimer.start(1000);
    return true;
}

bool CopyY4CWorker::startCopyLogs(std::vector<std::string> logs, std::string logPath)
{
    lastPercent = 0;
    currentCopyData = {};
    copyLogs.clear();

    this->logMainPath = logPath;
    if( logs.empty() )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Logs list for copy is empty");
        this->copyProcessInterface.copyProcessMessage("Нет логов для копирования");
        return false;
    }

    if( currentState == COPY_SEARCH_SRC_DEVICE  || currentState == COPY_STARTED )
    {
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Копирование уже запущено, дождитесь завершения",
                                                                                       Y4C_COPY_ERROR_ALREADY_RUN_START, 0));
        return false;
    }


    if( destBusService->checkConnection() != BUS_STATE_FOUND )
    {
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Внешний накопитель не найден", Y4C_COPY_ERROR_EXT_DEVICE_NOT_FOUND, 0));
        this->copyProcessInterface.destDeviceNotFound();
        currentState = COPY_ERROR;
        return false;
    }

    //Calculate logs size

    size_t logsSize = 0;

    for(auto log : logs )
    {
        struct stat stat_buf;
        std::cerr << "Try file " << log << std::endl;


        std::string completeLogPath = logPath + ("/") + (log);
        std::cerr << "Try file " << completeLogPath << std::endl;

        int rc = stat(completeLogPath.c_str(), &stat_buf);

        std::cerr << "RET file " << rc << std::endl;

        if( rc == 0 )
        {

            logsSize+= stat_buf.st_size;
            copyLogs.push_back(log);
        }
    }

    auto _ret = deviceService->getDeviceInfo(deviceName);

    if( _ret.first )
    {
        if((int64_t)(_ret.second.emptySpace - logsSize) <= 0)
        {
            this->copyProcessInterface.copyProcessMessage("Недостаточно свободного места на внешнем накопителе");
            return false;
        }
    }
    ////

    copyLogsTotalSize = logsSize;

    currentState = COPY_SEARCH_SRC_DEVICE;

    if( !prepareCopyLogsTimer.isActive() )
        prepareCopyLogsTimer.start(1000);
    return true;
}

void CopyY4CWorker::calculateSessionsSize(const std::vector<SessionsModelNS::SessionData> &sessions, uint64_t &totalBytesForCopy, uint64_t &recTime)
{
    uint64_t lastStart = 0;
    uint64_t lastEnd = 0;

    for( auto &sess : sessions )
    {
        recTime += (sess.endTime - sess.startTime);

        std::cerr << "---" << std::endl;
        std::cerr << "sess.sessionID = " << sess.sessionID << std::endl;
        std::cerr << "sess.startRecAddress = " << sess.startRecAddress << std::endl;
        std::cerr << "sess.endRecAddress = " << sess.endRecAddress << std::endl;

        if(sess.startRecAddress >= lastStart && sess.endRecAddress <= lastEnd) //ignore sess (alrady in last range)
        {
            continue;
        }

        if(sess.startRecAddress < lastEnd && sess.endRecAddress > lastEnd) //update last session endAdress
        {
            lastEnd = sess.endRecAddress;
            totalBytesForCopy += sess.endRecAddress - lastEnd;
            continue;
        }


        lastStart = sess.startRecAddress;
        lastEnd = sess.endRecAddress;
        totalBytesForCopy += sess.endRecAddress - sess.startRecAddress;
    }

}

bool CopyY4CWorker::doError()
{
    emulateError = true;
}

CopyY4CState CopyY4CWorker::getCurrentCopyState()
{
    return currentState;
}

void CopyY4CWorker::prepareToDelete()
{
    return;
}

CopyY4CWorker::~CopyY4CWorker()
{
    isThreadWorking = false;
    if( copyThread.joinable() )
        copyThread.join();

    if( currentState != COPY_END && currentState != COPY_STOP && currentState != COPY_NONE )
        slotStopCopy(false,"");
}

void CopyY4CWorker::initBusServices()
{
    if( srcBusService == nullptr )
    {
        srcBusService = std::make_shared<EmmcBusService>(dataProvider, logger);
    }

    if( destBusService == nullptr )
    {
        destBusService = std::make_shared<UsbBusService>(dataProvider, logger);
    }
}

void CopyY4CWorker::startCopy()
{

}

void CopyY4CWorker::slotSearchDestTimerTimeout()
{
    switch (static_cast<int>(destBusService->checkConnection()))
    {
        case BUS_STATE_NONE:
        {
            std::cerr << "BUS STATE NONE SWITCH " << std::endl;

            if( !destBusService->requestDevice() )
            {
                copySearchDestTimer.stop();
                this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                                  "Ошибка запроса флеш носителя", Y4C_COPY_ERROR_PREPARE_DEST_CHECK_CONNECTION, 0));
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while request dest device");
                break;
            }
            break;
        }
        case BUS_STATE_SWITCHED:
        {
            std::string devices;

            int ret = destBusService->getDeviceName(devices);

            if( ret )
            {
                deviceName = devices;
                std::cerr << "DEVICE NAME = " << devices << std::endl;
            }
            std::cerr << "BUS STATE SWITCH START SEARCH USB!!! " << std::endl;
            currentCopyData.searchDestCounter++;

            if( currentCopyData.searchDestCounter > 10 )
            {
                currentCopyData.searchDestCounter = 0;

                if( currentCopyData.copyErrorCounter > 0 )
                {
                    copySearchDestTimer.stop();
                    slotStopCopy(true, "Ошибка подключения USB носителя");
                    return;
                }
                this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                                  "Ошибка подключения USB носителя",
                                                                  Y4C_COPY_ERROR_EXT_DEVICE_NOT_FOUND, 0));
                copySearchDestTimer.stop();
                this->copyProcessInterface.destDeviceNotFound();
            }
            break;
        }
        case BUS_STATE_FOUND:
        {
            std::cerr << "BUS DEVICE LIST FOUND !!!" << std::endl;

            if( deviceService->mountDevice(deviceName) > 0 ) //GET HERE MOUNT TYPE
            {
                std::cerr << "DEVICE READY FOR COPY" << std::endl;
                auto ret = deviceService->getDeviceInfo(deviceName);

                if( ret.first )
                {
                    destDeviceData = ret.second;
                    std::cerr << "FS TYPE = " << ret.second.fsType << std::endl;
                    std::cerr << "USB SIZE = " << ret.second.fullSize << std::endl;
                    std::cerr << "EMPTY SIZE = " << ret.second.emptySpace << std::endl;
                }
                else
                {
                    this->copyProcessInterface.copyProcessMessage("Ошибка подключения USB носителя");
                    copySearchDestTimer.stop();
                    this->copyProcessInterface.destDeviceNotFound();
                    break;
                }

                if( currentCopyData.copyErrorCounter > 0 )
                {
                    //Значит перезапускаемся
                    copySearchDestTimer.stop();
                    relaunchCopy();
                }
                else
                {
                    this->copyProcessInterface.destDeviceWasFound(ret.second);
                    copySearchDestTimer.stop();
                }
            }
            break;
        }
        case BUS_STATE_NOT_FOUND:
        {
            std::cerr << "BUS STATE NO LONGER FOUND !!!" << std::endl;

            this->copyProcessInterface.destDeviceNotFound();

            //TODO UNMOUNT
            int ret = umountDevice(deviceName);
            if( ret < 0 )
            {
                this->copyProcessInterface.copyProcessMessage("Ошибка при размонтировании флеш носителя");
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Unable to umount device");
            }

            if( !destBusService->returnDevice() )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while return dest device");
                break;
            }
            copySearchDestTimer.stop();
            break;
        }
    }
}

void CopyY4CWorker::startSearchDestDevice()
{
    if( currentState == COPY_STARTED )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ ,
                              "Copy already started! we will not start search dest device");
        this->copyProcessInterface.copyProcessMessage("Копирование запущенно!");
        return;
    }
    memset(&destDeviceData, 0, sizeof(destDeviceData));
    if( copySearchDestTimer.isActive() )
        return;

    copySearchDestTimer.start(1000);
    currentCopyData.searchDestCounter = 0;
}

void CopyY4CWorker::initDeviceService()
{
    if(deviceService == nullptr )
    {
        deviceService = std::make_shared<DeviceService>(logger);
    }
}


void CopyY4CWorker::slotCheckBusesTimerTimeout()
{
    if( destBusService->checkConnection() == BUS_STATE_NOT_FOUND )
    {
        std::cerr << "ERROR BUS STATE NOT FOUND " << std::endl;
        return;
    }
    if( srcBusService->checkConnection() == BUS_STATE_NOT_FOUND )
    {
        std::cerr << "ERROR BUS EMMC STATE NOT FOUND " << std::endl;
    }
}

void CopyY4CWorker::slotPrepareCopyTimerTimeout()
{
    if( copyStarterrorCounter > COPY_START_ERROR_TIMES )
    {
        //Say user that error
        //Unmount all mounted and return SD / USB
        //TODO UMOUNT ALL
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Ошибка запроса emmc",
                                                          Y4C_COPY_ERROR_PREPARE_ERROR_TIMES, 0));
        closeDevices();
        prepareCopyTimer.stop();
        return;
    }

    if( destBusService->checkConnection() == BUS_STATE_NOT_FOUND )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error we do not have dest umount all");
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Ошибка внешний накопитель не найден",
                                                          Y4C_COPY_ERROR_EXT_DEVICE_NOT_FOUND, 0));
        //TODO UMOUNT ALL
        prepareCopyTimer.stop();
        return;
    }

    BusState srcBusState = srcBusService->checkConnection();

    std::cerr << "BUS STATE = " << srcBusState << std::endl;
    if( srcBusState == BUS_STATE_NONE )
    {
        std::cerr << "REQUEST SOURCE DEVUICE!!!" << std::endl;
        if( !srcBusService->requestDevice() )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while request src device");
//            this->copyProcessInterface.copyProcessMessage("Ошибка запроса emmc");
            //will try next time errorCount++
            copyStarterrorCounter++;
            return;
        }
        return;
    }//TODO Uncomment !
    else if( srcBusState != BUS_STATE_FOUND )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Src device still not found");
        copyStarterrorCounter++;
        return;
    }


    if( currentState == COPY_END || currentState == COPY_ERROR )
    {
        prepareCopyTimer.stop();
        currentState = COPY_NONE;
        return;
    }

    if( !isThreadWorking && currentState != COPY_STARTED )
    {
        std::cerr << "BUS STATE START COPY = " << srcBusState << std::endl;
        isThreadWorking = false;
        if( copyThread.joinable() )
            copyThread.join();
        isThreadWorking = true;
        prepareCopyTimer.stop();
        currentState = COPY_STARTED;
        copyThread = std::thread(&CopyY4CWorker::copyThreadFunc, this);
        this->copyProcessInterface.copyWasStarted();
    }
    else if( isThreadWorking && currentState == COPY_STARTED )
    {

    }
    else
    {
        currentState = COPY_ERROR;
        closeDevices();
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Ошибка копировния",
                                                          Y4C_COPY_ERROR_PREPARE_START_THREAD, 0));
        prepareCopyTimer.stop();
    }
}


void CopyY4CWorker::copyThreadFunc()
{
    while(isThreadWorking)
    {
        if( currentState == COPY_STARTED )
        {
            int fpSrc;
            int dstSrc;

            fpSrc = srcBusService->openDevice();

#ifdef DEBUG_COPY_TEST
            fpSrc = open("/media/ivan/yocto/testfile", O_RDONLY);

            if( fpSrc <= 0 )
            {
                std::cerr << "Unable to open test src file" << std::endl;
                currentState = COPY_ERROR;
                return;
            }
#else
            if( fpSrc <= 0 )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error emmc device open");
                currentCopyData.errorCode = COPY_ERROR_OPEN_SRC_DEV;
                stopCopyOnError(ErrorMessageFormat::formatString("Не удалось открыть источник!",
                                                                 Y4C_COPY_ERROR_PROCESS_OPEN_SRC_DEV, errno));
                return;
            }
#endif

            std::string serialFolder;

            if( !prepareDestRootFolders(serialFolder) )
            {
                currentCopyData.errorCode = COPY_ERROR_PREPARE_DIRECTORY;
                stopCopyOnError(ErrorMessageFormat::formatString("Не удалось подготовить директории на устройстве назначения!",
                                                                 Y4C_COPY_ERROR_PROCESS_PREPARE_FOLDER, errno));
                return;
            }

            uint64_t totalBytesForCopy = 0;
            std::vector<SessionsModelNS::SessionData> copySessionsNEW;

            prepareSessionsForCopy(copySessionsNEW, totalBytesForCopy);

            bool fileWasCreated = false;

            auto timeStart = std::chrono::high_resolution_clock::now();
            copyStartTime = std::chrono::steady_clock::now();

            totalToWrite = 0;
            std::string filePath;

            uint64_t writeData = 0;
            uint64_t writed = 0;
            uint64_t wrap = 0;

//****FOR TEST
            std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
//****

            int sessionCounter = 0;
            off64_t haveToWrite = 0;
            off64_t writ = 0;
            for( auto &sess : copySessionsNEW )
            {
                if( currentCopyData.copyErrorCounter > 0 && currentCopyData.relaunch )
                {
                    std::cerr << "Restore session position after error" << std::endl;
                    if( sessionCounter < currentCopyData.sessionStopPos )
                    {
                        sessionCounter++;
                        continue;
                    }

                    //Файл уже должен быть создан нам нужно его открыть и  продолжить писать
                    //Если только ошибка не была при создании файла

                    if( currentCopyData.errorCode == COPY_ERROR_PREPARE_FILE )
                    {
                        if( !fileWasCreated )
                        {
                            dstSrc = prepareFileForCopy(serialFolder, sess.startTime, currentCopyData);
                            if( dstSrc < 0 )
                            {
                                //Внутри поток будет остановлен и напечатает ошибку *для интуитивной ошибки
                                currentCopyData.errorCode = COPY_ERROR_PREPARE_FILE;
                                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                                                      "Error while prepare file for copy");
                                return;
                            }
                            errorCopyCount = 0;
                            fileWasCreated = true;
                        }
                    }
                    else
                    {
                        std::cerr << "File restore name = " << currentCopyData.destFileName << std::endl;

                        dstSrc = fileService->openFile(currentCopyData.destFileName);

                        if( dstSrc <= 0)
                        {
                            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while open file for copy");
                            stopCopyOnError(ErrorMessageFormat::formatString("Не удалось открыть файл назначения!",
                                                                             Y4C_COPY_ERROR_PROCESS_OPEN_DEST_FILE, errno));
                            return ;
                        }

                        //uint64_t haveToWriteBySess = currentCopyData.sessionTotalToWrite - currentCopyData.writedData;

                        off64_t curPosDST = lseek64(dstSrc, currentCopyData.writedData , SEEK_SET);

                        if( curPosDST < 0 )
                        {
                            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                                                  "FAIL TO SEEK DST");
                            currentCopyData.errorCode = COPY_ERROR_SEEK;
                            stopCopyOnError(ErrorMessageFormat::formatString("Ошибка работы с файлом.", Y4C_COPY_ERROR_PROCESS_DEST_FILE_SEEK, errno));
                            return;
                        }

                        curPosDST = lseek64(fpSrc, sess.startRecAddress + currentCopyData.writedData , SEEK_SET);

                        if( curPosDST < 0 )
                        {
                            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                                                  "FAIL TO SEEK SRC");
                            currentCopyData.errorCode = COPY_ERROR_SEEK;
                            stopCopyOnError(ErrorMessageFormat::formatString(
                                                "Ошибка работы с файлом.",
                                                Y4C_COPY_ERROR_PROCESS_SRC_SEEK_AFTER_ERROR, errno));
                            return;
                        }
                        writed = currentCopyData.writedData;
                        wrap = writed;
                        writeData = writed;
                        errorCopyCount = 0;
                        currentCopyData.relaunch = false;
                        haveToWrite = sess.endRecAddress - sess.startRecAddress - writed;
                    }
                }
                else
                {
                    //Create File
                    if( !fileWasCreated )
                    {
                        dstSrc = prepareFileForCopy(serialFolder, sess.startTime, currentCopyData);
                        if( dstSrc < 0 )
                        {
                            //Внутри поток будет остановлен и напечатает ошибку *для интуитивной ошибки
                            currentCopyData.errorCode = COPY_ERROR_PREPARE_FILE;
                            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                                                  "Error while prepare file for copy");
                            return;
                        }
                        errorCopyCount = 0;
                        fileWasCreated = true;
                    }

                    off64_t curPosDST = lseek64(fpSrc, sess.startRecAddress , SEEK_SET);

                    if( curPosDST < 0 )
                    {
                        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                                              "FAIL TO SEEK");
                        currentCopyData.errorCode = COPY_ERROR_SEEK;
                        stopCopyOnError("Ошибка работы с файлом.");
                        stopCopyOnError(ErrorMessageFormat::formatString(
                                            "Ошибка работы с файлом.",
                                            Y4C_COPY_ERROR_PROCESS_SRC_SEEK, errno));
                        return;
                    }

                    haveToWrite = sess.endRecAddress - sess.startRecAddress;
                    totalToWrite += haveToWrite;

                }

                writ = sess.startRecAddress;
                currentCopyData.sessionTotalToWrite = haveToWrite;

                std::cerr << "((((((((((((((((((((((((((((( total to write = " << totalToWrite << std::endl;

                while(haveToWrite > 0)
                {
                    if( currentState == COPY_STOP )
                    {
                        if( dstSrc > 0 )
                        {
                            syncfs(dstSrc);
                            close(dstSrc);
                            isThreadWorking = false;
                            emit signalStopCopy(false, "");
                            emit signalShowMessage("Копирование принудительно завершено");
                            return;
                        }
                    }

                    if( (wrap + 4096) >= SYNC_PERIOD)
                    {
                        wrap = 0;
                        syncfs(dstSrc);
                        currentCopyData.lastSyncPos = writed;
                        currentCopyData.writedData = haveToWrite;
                    }

                    if( destDeviceData.fsType == FS_FAT || destDeviceData.fsType == FS_EXT2 )
                    {
                        if( (writed + 4096) >= DEST_FILE_SIZE)
                        {
                            syncfs(dstSrc);
                            close(dstSrc);

                            uint64_t secPadding = (writ - sess.startRecAddress) / 512;
                            uint64_t startTime = sess.startTime + secPadding;


                            this->logger->Message(rosslog::LOG_INFO, "NEW FILE time %li path = %s rec address %li, diff = %li, writ = %li secPad %li",
                                                  startTime, filePath.c_str(), sess.startRecAddress, (writ - sess.startRecAddress), writ, secPadding);

                            dstSrc = prepareFileForCopy(serialFolder, startTime, currentCopyData);
                            if( dstSrc < 0 )
                            {
                                //Внутри поток будет остановлен и напечатает ошибку *для интуитивной ошибки
                                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                                                      "Error while prepare file for copy");
                                return;
                            }
                            errorCopyCount = 0;
                            writed = 0;
                        }
                    }

                    if( errorCopyCount > 5 || emulateError )
                    {
                        std::cerr << "Writ = " << writ << std::endl;
                        std::cerr << "Writed = " << writeData << std::endl;
                        std::cerr << "Diff = " << (writ - sess.startRecAddress) << std::endl;
                        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while copy stoping");
                        stopCopyOnError(ErrorMessageFormat::formatString("Ошибка копирования!",
                                                                         Y4C_COPY_ERROR_PROCESS_ERROR_TIMES, 0));
                        if( dstSrc > 0 )
                            close(dstSrc);
                        return;
                    }

                    int ret = 0;
                    int bytesForCopy = 4096;

                    if(haveToWrite < 4096)
                    {
                        bytesForCopy = haveToWrite;
                    }

                    ret = sendDataServiceService->sendData(fpSrc, dstSrc, bytesForCopy);

                    if( ret <= 0){
                        currentCopyData.errorCode = COPY_ERROR_SENDFILE;
                        fprintf(stderr, "error from sendfile: %s\n", strerror(errno));
                        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while send file: %s", strerror(errno));
                        errorCopyCount++;
//                        if( dstSrc > 0 )
//                            close(dstSrc);
                        sleep(1);
                        continue;
                    }

                    writed += ret;
                    errorCopyCount = 0;
                    haveToWrite -= ret;
                    writeData +=ret;
                    wrap += ret;

                    updateTimeLeftAndSpeed(totalBytesForCopy, writeData, timeStart);
                    updatePercent(totalBytesForCopy, writeData);
                }
                sessionCounter++;
                currentCopyData.sessionStopPos++;
                std::cerr << "))))))))))))))))))))))))))) haveToWrite = " << haveToWrite << std::endl;
                std::cerr << "))))))))))))))))))))))))))) writeData = " << writeData << std::endl;

            }
//****FOR TEST
            end = std::chrono::steady_clock::now();

            std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "[ms]" << std::endl;
            std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
            std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() << "[ns]" << std::endl;
//****
            if( dstSrc > 0 )
                syncfs(dstSrc);

            std::cerr << "DST FP = " << dstSrc << std::endl;
            if( dstSrc > 0 )
            {
                int ret = close(dstSrc);
                std::cerr << "CLOSE RET =  " << ret << std::endl;
                std::cerr << "CLOSE ERRNO =  " << errno << std::endl;
            }

            currentState = COPY_END;
            emit signalStopCopy(false, "");
            isThreadWorking = false;

            break;
        }
        sleep(1);
    }

}

bool CopyY4CWorker::relaunchCopy()
{
    copyStarterrorCounter = 0;

    if( copySessions.empty() )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Sessions for copy is empty");
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Не выбраны сессии для копирования",
                                                          Y4C_COPY_ERROR_RELAUNCH_SESSIONS, 0));
        return false;
    }

    if( currentState == COPY_SEARCH_SRC_DEVICE  || currentState == COPY_STARTED )
    {
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Копирование уже запущено, дождитесь завершения",
                                                          Y4C_COPY_ERROR_RELAUNCH_COPY_STARTED, 0));
        return false;
    }


    if( destBusService->checkConnection() != BUS_STATE_FOUND )
    {
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Внешний накопитель не найден",
                                                          Y4C_COPY_ERROR_RELAUNCH_DEST_NOT_FOUND, 0));
        this->copyProcessInterface.destDeviceNotFound();
        return false;
    }

    currentCopyData.relaunch = true;
    currentState = COPY_SEARCH_SRC_DEVICE;

    if( !prepareCopyTimer.isActive() )
        prepareCopyTimer.start(1000);
}

void CopyY4CWorker::startCopyLogsThread()
{
    this->copyProcessInterface.copyWasStarted();

    copyLogsWorker = std::async( std::launch::async,[this]
    {
        auto timeStart = std::chrono::high_resolution_clock::now();
        copyStartTime = std::chrono::steady_clock::now();

        totalToWrite = copyLogsTotalSize;

        std::string serialFolder;

        if( !prepareDestRootFolders(serialFolder) )
        {
            currentCopyData.errorCode = COPY_ERROR_PREPARE_DIRECTORY;
            stopCopyOnError(ErrorMessageFormat::formatString("Не удалось подготовить директории на устройстве назначения!",
                                                             Y4C_COPY_ERROR_PROCESS_PREPARE_FOLDER, errno));
            return;
        }

        bool stopOnError = false;
        size_t writedData = 0;
        for( auto logName: copyLogs )
        {
            std::string filePath;

            std::cerr << "Try file to copy " << logName << std::endl;

            if( fileService->createLogFileForCopy(serialFolder, logName, filePath)  < 0 )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while prepare log file");
                continue;
            }

            std::string srcLogPath = this->logMainPath + "/" + logName;
            int fdSrc = open(srcLogPath.c_str(), O_RDONLY);
            if( fdSrc < 0 )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while open src log file");
                continue;
            }

            struct stat stat_buf;
            size_t haveToWrite = 0;
            int rc = stat(srcLogPath.c_str(), &stat_buf);
            if( rc == 0 )
            {
                haveToWrite = stat_buf.st_size;
            }
            else
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while get src log file size %s", srcLogPath.c_str());
                continue;
            }

            std::cerr << "File dest copy = " << filePath << std::endl;
            int fdDst = fileService->openFile(filePath);

            if( fdDst < 0 )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while open dst log file %s", filePath.c_str());
                continue;
            }

            while( haveToWrite > 0 )
            {
                size_t bytesForCopy = 4096;
                if(haveToWrite < 4096)
                {
                    bytesForCopy = haveToWrite;
                }

                int ret = sendDataServiceService->sendData(fdSrc, fdDst, bytesForCopy);

                if( errorCopyCount > 5 )
                {
                    currentCopyData.copyErrorCounter = 3; //Нам не нужно пробовать еще раз
                    stopCopyOnError(ErrorMessageFormat::formatString("Ошибка копирования логов!",
                                                                     Y4C_COPY_ERROR_PROCESS_ERROR_TIMES, 0));
                    stopOnError = true;
                    break;
                }
                std::cerr << "Copy data = " << ret << std::endl;


                if( ret <= 0)
                {
                    currentCopyData.errorCode = COPY_ERROR_SENDFILE;
                    fprintf(stderr, "error from sendfile: %s\n", strerror(errno));
                    this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while send file: %s", strerror(errno));
                    errorCopyCount++;
                    sleep(1);
                    continue;
                }

                writedData += ret;
                errorCopyCount = 0;
                haveToWrite -= ret;
                std::cerr << "CopyLogsTOtal size = " << copyLogsTotalSize << std::endl;
                updateTimeLeftAndSpeed(copyLogsTotalSize, writedData, timeStart);
                updatePercent(copyLogsTotalSize, writedData);
            }

            if( fdSrc > 0 )
            {
                close(fdSrc);
            }
            if( fdDst > 0 )
            {
                close(fdDst);
            }
        }
        if( !stopOnError )
        {
            emit signalStopCopy(false,"");
        }
        return;
    });
    copyLogsWorker.wait_for(std::chrono::seconds(0));
}

void CopyY4CWorker::initFileService()
{
    if( fileService == nullptr )
    {
        fileService = std::make_shared<FileService>(logger);
    }
}

void CopyY4CWorker::initSendDataService()
{
    if( sendDataServiceService == nullptr )
    {
        sendDataServiceService = std::make_shared<SendDataService>(logger);
    }
}

void CopyY4CWorker::updateTimeLeftAndSpeed(uint64_t totalSizeToCopy, uint64_t curCopied,
                                           const chrono::time_point<std::chrono::system_clock> &timeStart)
{
    auto curTime = std::chrono::high_resolution_clock::now();
    auto diffTime = curTime - timeStart;
    auto sizeCopiedinMB = static_cast<float>(curCopied) / 1024.f / 1024.f;
    auto totalSizeinMB = static_cast<float>(totalSizeToCopy / 1024.f / 1024.f);
    auto secondsAfterStart = std::chrono::duration_cast<std::chrono::seconds>(diffTime).count();

    if(static_cast<float>(secondsAfterStart) <= 1.f)
        return;

    speed = sizeCopiedinMB / static_cast<float>(secondsAfterStart);
    timeLeft = static_cast<float>(totalSizeinMB - sizeCopiedinMB) / speed;

//    std::cerr << "Time Left " << timeLeft << std::endl;
//    std::cerr << "Speed " << speed << std::endl;
}

void CopyY4CWorker::updatePercent(uint64_t totalSizeToCopy, uint64_t curCopied)
{
#if 1
    percent = (100. * static_cast<double>(curCopied) ) /  static_cast<double>(totalSizeToCopy);
    if( percent > 100.) percent = 100.;
#else
    percent = (1.f /  static_cast<float>(totalSizeToCopy) * static_cast<float>(curCopied)) * 100;
#endif


    if( (lastPercent + 1) <= (int)percent  || percent == 0)
    {
//        std::cerr << "PERCENT = " << percent << std::endl;
        lastPercent = percent;
        emit signalCopyPercentUpdate(percent, timeLeft, speed);
    }
}

void CopyY4CWorker::slotCopyPercentUpdate(int percent, int timeLeft, int speed)
{
    this->copyProcessInterface.copyProcessProcent((percent), timeLeft, speed);
}

void CopyY4CWorker::slotStopCopy(bool isError, std::string errorMsg)
{
    std::cerr << "SLOT IS WORKING " << std::endl;
    if( isError )
    {
        //Если каунтер ошибки < 2
        //Перезапускаем копирование с последней точки

        if( currentCopyData.copyErrorCounter > 2 )
        {
            currentCopyData = {};
            closeDevices();
            this->copyProcessInterface.copyProcessMessage(errorMsg);
        }
        else
        {
            //TODO reset copy

            try
            {
                if( destBusService->checkConnection() != BUS_STATE_FOUND )
                {
                    destBusService->cloceDevice();
                    umountDevice(deviceName);
                    destBusService->returnDevice();


                    if( srcBusService->checkConnection() == BUS_STATE_FOUND )
                    {
                        srcBusService->cloceDevice();
                    }

                    srcBusService->returnDevice();


                    currentCopyData = {};
                    this->copyProcessInterface.copyEnded();
                    this->copyProcessInterface.copyProcessMessage(errorMsg);

                    currentState = COPY_END;
                    this->copyProcessInterface.copyProcessProcent((-1), -1, -1);
                    return;
                }

                if( srcBusService->checkConnection() == BUS_STATE_FOUND )
                {
                    srcBusService->cloceDevice();
                }

                srcBusService->returnDevice();
            }
            catch (std::runtime_error &except)
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while disconnectiong all devices");
            }

            currentCopyData.copyErrorCounter++;
            startSearchDestDevice();
            emulateError = false;
            return;
        }
    }
    else
    {
        closeDevices();
        currentCopyData = {};
        std::chrono::steady_clock::time_point copyEndTime = std::chrono::steady_clock::now();

        this->copyProcessInterface.copyEnded();


        int mbSEC = (totalToWrite / std::chrono::duration_cast<std::chrono::milliseconds>(copyEndTime - copyStartTime).count())/1000;
        std::string message = "Копирование завершено\n(Время:  "
                + format_duration(std::chrono::duration_cast<std::chrono::milliseconds>(copyEndTime - copyStartTime))
                + ")\n(Скорость:  "
                + std::to_string(mbSEC)
                + " мб/с.)";
        this->copyProcessInterface.copyProcessMessage(message);
    }
    currentState = COPY_END;
    this->copyProcessInterface.copyProcessProcent((-1), -1, -1);
}

std::string CopyY4CWorker::format_duration( std::chrono::milliseconds ms )
{
    using namespace std::chrono;
    auto secs = duration_cast<seconds>(ms);
    ms -= duration_cast<milliseconds>(secs);
    auto mins = duration_cast<minutes>(secs);
    secs -= duration_cast<seconds>(mins);
    auto hour = duration_cast<hours>(mins);
    mins -= duration_cast<minutes>(hour);

    std::stringstream ss;
    ss << hour.count() << " ч.  " << mins.count() << " мин.  " << secs.count() << "." << ms.count() << " сек.";
    return ss.str();
}

void CopyY4CWorker::closeDevices()
{
    try
    {
        if( destBusService->checkConnection() == BUS_STATE_FOUND )
        {
            destBusService->cloceDevice();
            umountDevice(deviceName);
        }

        if( srcBusService->checkConnection() == BUS_STATE_FOUND )
        {
            srcBusService->cloceDevice();
        }

        destBusService->returnDevice();
        srcBusService->returnDevice();
    }
    catch (std::runtime_error &except)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while disconnectiong all devices");
    }
}

void CopyY4CWorker::disconnectSearchedDevice()
{
    try
    {
        if( destBusService->checkConnection() == BUS_STATE_FOUND || destBusService->checkConnection() == BUS_STATE_SWITCHED )
        {
            if( copySearchDestTimer.isActive() )
                copySearchDestTimer.stop();
            if( !umountDevice(deviceName) )
                copyProcessInterface.copyProcessMessage("Неу далось размонтировать внешний накопитель");
            destBusService->returnDevice();
        }
        else
        {
            if( copySearchDestTimer.isActive() )
                copySearchDestTimer.stop();

            copyProcessInterface.deviceUmount();
        }
    }
    catch (std::runtime_error &except)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while disconnectiong device");
    }
}

bool CopyY4CWorker::stopCopy()
{
    if( currentState == COPY_STARTED )
    {
        currentState = COPY_STOP;
    }
}

int CopyY4CWorker::umountDevice(std::string deviceName)
{
    int ret = deviceService->umountDevice(deviceName);

    copyProcessInterface.deviceUmount();

    return ret;
}

void CopyY4CWorker::stopCopyOnError(const std::string textRus)
{
    isThreadWorking = false;
    currentState = COPY_ERROR;
    emit signalStopCopy(true, textRus);
    //emit signalShowMessage(textRus);
}

bool CopyY4CWorker::prepareDestRootFolders(std::string &serialFolder)
{

    std::string rootDirPath = std::string(DEVICE_MOUNT_PATH) + "/" + DEFAULT_REC_FOLDER;

    if( fileService->checkCopyFolderExist(rootDirPath) <= 0 )
    {
        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__,
                              "Root rec folder doesn't exist will create %s", rootDirPath.c_str());

        if( fileService->createDir(rootDirPath) <= 0 )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ ,
                                  " Error while create root rec folder %s ", rootDirPath.c_str());
            return false;
        }
    }


    auto res = getAdapterSerial();
    if( !res.first )
    {
        return false;
    }
    serialFolder = rootDirPath + "/" + res.second; // Serial number get from recorder

    if( fileService->checkCopyFolderExist(serialFolder) <= 0)
    {
        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__,
                              "Serial rec folder doesn't exist will create %s", serialFolder.c_str());
        if( fileService->createDir(serialFolder) <= 0 )
        {
            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ ,
                                  " Error while create serial rec folder %s ", serialFolder.c_str());
            return false;
        }
    }
    return true;
}

int CopyY4CWorker::prepareFileForCopy(const string &folderPath, const uint64_t &startTime, CopyY4CData &currentData)
{
    int dstSrc = 0;
    std::string filePath;

    if( fileService->createFileForCopy(folderPath, startTime, filePath) <= 0 )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while creating file for copy");
        stopCopyOnError("Не удалось создать файл назначения!");
        return -1;
    }

    currentData.destFileName = filePath;


    dstSrc = fileService->openFile(filePath);

    if( dstSrc <= 0)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error while open file for copy");
        stopCopyOnError("Не удалось открыть файл назначения!");
        return -1;
    }
    return dstSrc;
}

void CopyY4CWorker::slotShowMessage(std::string msg)
{
    this->copyProcessInterface.copyProcessMessage(msg);
}

void CopyY4CWorker::slotPrepareCopyLogsTimerTimeout()
{
    errorCopyCount = 0;
    if( copyStarterrorCounter > COPY_START_ERROR_TIMES )
    {
        //Say user that error
        //Unmount all mounted and return SD / USB
        //TODO UMOUNT ALL
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Ошибка копирования логов.",
                                                          Y4C_COPY_ERROR_PREPARE_ERROR_TIMES, 0));
        closeDevices();
        prepareCopyLogsTimer.stop();
        return;
    }

    if( destBusService->checkConnection() == BUS_STATE_NOT_FOUND )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error we do not have dest umount all");
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Ошибка внешний накопитель не найден",
                                                          Y4C_COPY_ERROR_EXT_DEVICE_NOT_FOUND, 0));
        //TODO UMOUNT ALL
        prepareCopyLogsTimer.stop();
        return;
    }


    if( currentState == COPY_END || currentState == COPY_ERROR )
    {
        prepareCopyLogsTimer.stop();
        currentState = COPY_NONE;
        return;
    }

    if( currentState != COPY_STARTED )
    {
        currentState = COPY_STARTED;
        startCopyLogsThread();
        prepareCopyLogsTimer.stop();
    }
    else if( currentState == COPY_STARTED )
    {

    }
    else
    {
        currentState = COPY_ERROR;
        closeDevices();
        this->copyProcessInterface.copyProcessMessage(ErrorMessageFormat::formatString(
                                                          "Ошибка копировния логов.",
                                                          Y4C_COPY_ERROR_PREPARE_START_THREAD, 0));
        prepareCopyLogsTimer.stop();
    }
}

std::pair<bool, std::string> CopyY4CWorker::getAdapterSerial()
{
    char adapterSerial[64];
    memset(&adapterSerial, 0, 64);
    try
    {
        adapterStatusControl->updateStatus();
        adapterStatus = adapterStatusControl->getAdapterStatus();
        sprintf(adapterSerial,"%016lX", adapterStatus.sn);
    }
    catch(std::runtime_error &except)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ ,
                              " Error get serial num %s ", except.what());
        return std::make_pair(false, "");
    }

    return std::make_pair(true, adapterSerial);
}

void CopyY4CWorker::prepareSessionsForCopy(std::vector<SessionsModelNS::SessionData> &copySessionsNEW, uint64_t &totalBytesForCopy)
{
    uint64_t lastStart = 0;
    uint64_t lastEnd = 0;

    for( auto &sess : copySessions )
    {
        std::cerr << "---" << std::endl;
        std::cerr << "sess.sessionID = " << sess.sessionID << std::endl;
        std::cerr << "sess.startRecAddress = " << sess.startRecAddress << std::endl;
        std::cerr << "sess.endRecAddress = " << sess.endRecAddress << std::endl;

        if(sess.startRecAddress >= lastStart && sess.endRecAddress <= lastEnd) //ignore sess (alrady in last range)
        {
            continue;
        }

        if(sess.startRecAddress < lastEnd && sess.endRecAddress > lastEnd) //update last session endAdress
        {
            copySessionsNEW.back().endRecAddress = sess.endRecAddress;
            lastEnd = sess.endRecAddress;
            totalBytesForCopy += sess.endRecAddress - lastEnd;
            continue;
        }


        lastStart = sess.startRecAddress;
        lastEnd = sess.endRecAddress;

        copySessionsNEW.push_back(sess);

        totalBytesForCopy += sess.endRecAddress - sess.startRecAddress;
    }

    std::cerr << "copySessionsNEW count = " << copySessionsNEW.size() << std::endl;
    std::cerr << "totalBytesForCopy = " << totalBytesForCopy << std::endl;
}

