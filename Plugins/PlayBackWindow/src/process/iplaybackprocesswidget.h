
#ifndef CM_INTERFACE_IPLAYBACKPROCESSWIDGET_H
#define CM_INTERFACE_IPLAYBACKPROCESSWIDGET_H

class IPlayBackProcessWidget
{
public:
    virtual bool playAudioData(int currentIndex, uint64_t playPos) = 0;
    virtual bool nextAudioData(int currentIndex) = 0;
    virtual bool previousAudioData(int currentIndex) = 0;
    virtual bool stopAudioPlay() = 0;
    virtual bool pauseAudioPlay(int currentIndex) = 0;
    virtual bool loopAudioPlay(int currentIndex, uint64_t playPos, bool isLoopPlay) = 0;
    virtual bool seekForwardAudioPlay(int currentIndex, uint64_t playPos) = 0;
    virtual bool seekBackAudioPlay(int currentIndex, uint64_t playPos) = 0;
    virtual bool seekToPos(int currentIndex, uint64_t playPos) = 0;
};

#endif //CM_INTERFACE_IPLAYBACKPROCESSWIDGET_H
