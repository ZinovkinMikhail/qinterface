//
// Created by ivan on 14.12.2020.
//

#ifndef CM_INTERFACE_IPLAYBACKPROCESS_H
#define CM_INTERFACE_IPLAYBACKPROCESS_H

class IPlayBackProcess
{
public:
    virtual void currentPlayTimeChanged(int sessionID, uint64_t currentPlayTime) = 0;
};

#endif //CM_INTERFACE_IPLAYBACKPROCESS_H
