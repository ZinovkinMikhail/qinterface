//
// Created by ivan on 09.11.2020.
//

#ifndef CM_INTERFACE_PLAYBACKPROCESS_H
#define CM_INTERFACE_PLAYBACKPROCESS_H

#include <future>
#include <mutex>

#include <QObject>
#include <QTimer>
#include <QList>
#include <QTextCodec>
#include <QVariant>

#include <iemptyplacewindow.h>

#include <dataprovider/recorderstatuscontrol.h>
#include <dataprovider/recordersettingscontol.h>

#include "src/sessions/sessionsmodel.h"
#include "src/database/audiopkdworker.h"
#include "src/filter/filtermodel.h"
#include "iplaybackprocess.h"
#include "iplaybackprocesswidget.h"
#include "playbackcontrolsprocesswidget.h"


#define PLAYBACK_PROCES_WIDGET_NAME "PlayBackProcessWidget"
#define AUDIO_PLAY_SEEK_SEC 7
#define AUDIO_PLAY_REPEAT_SEC 7
#define VENDOR_PASSWORD "h2cHm2H7oMlA2b8FG5xM3PzYus3Cy7V5"


using namespace SessionsModelNS;
using namespace recorder_status_ctrl;
using namespace rosslog;

class PlayBackProcess: public QObject, public IPlayBackProcessWidget
{
    Q_OBJECT
public:
    explicit PlayBackProcess(std::shared_ptr<AudioPKDWorker> audioPKDWorker,
                             std::shared_ptr<IQmlEngine> qmlEngine,
                             std::shared_ptr<RecorderStatusControl> recorderStatusCtrl,
                             std::shared_ptr<rosslog::Log> logger,
                             std::shared_ptr<IUserManipulators> iUserManipulators,
                             IPlayBackProcess &iPlayBackProcess,
                             QObject *parent = nullptr);

    ~PlayBackProcess();

    void init();

    bool sessionSelectedStateChanged(const SessionData &sessionData, const bool state );
    bool getSelectedSessions(std::vector<SessionsModelNS::SessionData> &sessions);
    void tapMenuPlay();
    void updatePlayProcessData(SoftRecorderStatusStruct_t recordStatus);
    void updateRecorderSettingsControl(SoftRecorderSettingsStruct_t recorderSettings);
    void updateFilterMaskData(SessionMask maskData);
    bool seekAudioToPosFromGraph(int sessionID, uint64_t seekSec);
    uint8_t getOurState();
    bool getProcessWidgetState();
    std::shared_ptr<PlayBackControlsProcessWidget> getProcessWidget();


    void stopAudioImidiatly();
protected:
    //IPlayBackProcess
    bool playAudioData(int currentIndex, uint64_t playPos) override;
    bool nextAudioData(int currentIndex) override;
    bool previousAudioData(int currentIndex) override;
    bool pauseAudioPlay(int currentIndex) override;
    bool loopAudioPlay(int currentIndex, uint64_t playPos, bool isLoopPlay) override;
    bool seekForwardAudioPlay(int currentIndex, uint64_t playPos) override;
    bool seekBackAudioPlay(int currentIndex, uint64_t playPos) override;
    bool seekToPos(int currentIndex, uint64_t playPos) override;
    bool stopAudioPlay() override;

private:
    void initPlayBackProcessWidget();
    int checkSessionInList(int sessionId, bool isAppend);


    bool playNextAudioSession();
    bool playPrevAudioSession();
    bool continuePlayFromPos(int currentIndex, uint64_t playPos);
    void playSessFromPos(int sessIndex, uint64_t playPos);
    void prepareMaskCode(char *code);

    //Not used now
    void compareRecordAndOurPos();

    std::shared_ptr<AudioPKDWorker> audioPKDWorker{nullptr};
    std::shared_ptr<IQmlEngine> qmlEngine{nullptr};
    std::shared_ptr<RecorderStatusControl> recorderStatusCtrl{nullptr};
    std::shared_ptr<rosslog::Log> logger{nullptr};
    std::shared_ptr<IUserManipulators> iUserManipulators{nullptr};
    IPlayBackProcess &iPlayBackProcess;

    std::shared_ptr<PlayBackControlsProcessWidget> playBackProcessWidget{nullptr};

    SoftRecorderStatusStruct_t recordStatus;
    SoftRecorderSettingsStruct_t recordSettings;

    std::thread playThread;
    std::mutex playerFuncMutex;
    std::mutex sessionSelectMutex;
    std::mutex dataMutex;
    std::mutex recorderDataUpdate;

    std::future<void> playerFuncWorker;

    QList<int> selectedSessions;

    uint8_t previousRecordStatus{0};
    uint8_t ourStatus{0};
    uint64_t previousRecordTime{0};

    std::map<int, SessionsModelNS::SessionData> selectedSessionsMap;

    bool isProcessWidgetVisible{false};

    int currentPlayAudioIndex{0};
    int futurePlayAudioIndex{0};

    SessionMask maskData;


signals:
    void signalSeekPos(int newIndex, size_t playPos);

public slots:
    void slotOnProcessWidgetSwipeSelected(std::string name,
                                          std::shared_ptr<IProcessWidget> object);
    void slotOnProcessWidgetSwipeDeSelected(std::string name,
                                            std::shared_ptr<IProcessWidget> object);
    void slotSeekPos(int newIndex, size_t playPos);


};

#endif
