//
// Created by ivan on 14.12.2020.
//

#ifndef CM_INTERFACE_PLAYBACKCONTROLSPROCESSWIDGET_H
#define CM_INTERFACE_PLAYBACKCONTROLSPROCESSWIDGET_H

#include <QObject>
#include <QQuickItem>

#include <iqmlengine.h>
#include <iprocesswidget.h>

#include "iplaybackprocesswidget.h"
#include "src/sessions/sessionsmodel.h"
#include "iusermanipulators.h"

#define PLAY_BUTTON_CONTROL_NAME "PlayButt"
#define LOOP_BUTTON_CONTROL_NAME "RepeatButt"
#define SEEKBACK_BUTTON_CONTROL_NAME "SeekBackButt"
#define SEEKFORWARD_BUTTON_CONTROL_NAME "SeekForwartButt"

typedef enum
{
    AUDIO_NONE_STATE,
    AUDIO_PLAY_STATE,
    AUDIO_PAUSE_STATE,
    AUDIO_BACK_STATE,
    AUDIO_LOOP_STATE,
    AUDIO_STOP_STATE,
    AUDIO_ERROR_STATE
} AudioPlayState;

#define PLAYBACK_PROCESS_COMPONENT_PATH "qrc:/PlayBackWindow/PlayBackControls.qml"

class PlayBackControlsProcessWidget : public QObject,
                                      public IProcessWidget,
                                      public std::enable_shared_from_this<PlayBackControlsProcessWidget>
{
    Q_OBJECT
    Q_INTERFACES(IProcessWidget)
public:
    explicit PlayBackControlsProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                           std::string name,
                                           std::shared_ptr<rosslog::Log> log,
                                           IPlayBackProcessWidget &playBackInterface,
                                           std::shared_ptr<IUserManipulators> userManipulators,
                                           QObject *parent = nullptr);
    void init();
    void show();

    bool isWidgetActive() override;
    std::string getWidgetName() override;
    QObject *getComponent();

    //We have to know if widget selected for update info
    bool isProcessWidgetShown();

    void setFreshSessionsData(int countSessions, SessionsModelNS::SessionData currentSession, int currentIndex);
    void updateElapsedTime(int currentIndex, uint64_t time);
    void changeAudioProcessState(AudioPlayState state);
    const AudioPlayState &getAudioPlayState();
    const bool &getLoopPlayState();
    int getCurrentAudioID();
    void controlCommand(QString name);

private:
    QString getLengthString(const uint64_t time);


    std::shared_ptr<IQmlEngine> qmlEngine;
    std::string name;
    std::shared_ptr<rosslog::Log> logger;
    QQuickItem *currentItem;
    QQuickItem *playButtonItem;
    QQuickItem *loopButtonItem;
    QQuickItem *seekBackButtonItem;
    QQuickItem *seekForwardButtonItem;
    QQuickItem *sliderItem;
    bool isWidgetSelected;
    IPlayBackProcessWidget &playBackInterface;
    int currentPlayingIndex;
    SessionsModelNS::SessionData currentSession;
    uint32_t elapsedTime;
    int countSelectedSessions;
    AudioPlayState currentAudioState;
    std::shared_ptr<IUserManipulators> userManipulators;
    bool repeatMode;

signals:
    void signalShowMe(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalSwipeSelected(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalSwipeDeselected(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalMakeFullSized();


public slots:
    void slotProcessWidgetActive();
    void slotProcessWidgetUnActive();
    void slotControlsButtonPressed(QString buttonName);
    void slotSliderSeekPos(QVariant posProc);
    void slotMakeFullSizedControll();

};


#endif //CM_INTERFACE_PLAYBACKCONTROLSPROCESSWIDGET_H
