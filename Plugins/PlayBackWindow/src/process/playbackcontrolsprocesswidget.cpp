//
// Created by ivan on 14.12.2020.
//

#include "playbackcontrolsprocesswidget.h"

#include <utility>
#include <iostream>

#include <QDateTime>

using namespace rosslog;

PlayBackControlsProcessWidget::PlayBackControlsProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                                             std::string name,
                                                             std::shared_ptr<rosslog::Log> log,
                                                             IPlayBackProcessWidget &playBackInterface,
                                                             std::shared_ptr<IUserManipulators> userManipulator,
                                                             QObject *parent) :
     QObject(parent),
     qmlEngine(std::move(qmlEngine)),
     name(std::move(name)),
     logger(std::move(log)),
     currentItem(nullptr),
     playButtonItem(nullptr),
     sliderItem(nullptr),
     isWidgetSelected(false),
     playBackInterface(playBackInterface),
     currentPlayingIndex(0),
     elapsedTime(0),
     countSelectedSessions(0),
     currentAudioState(AUDIO_STOP_STATE),
     userManipulators(userManipulator),
     repeatMode(false)

{
    Q_ASSERT(this->qmlEngine);
    Q_ASSERT(this->logger);
    memset(&currentSession, 0 , sizeof(SessionsModelNS::SessionData));
}

void PlayBackControlsProcessWidget::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(qmlEngine->createComponent(PLAYBACK_PROCESS_COMPONENT_PATH));
        Q_ASSERT(currentItem);

        auto *controlsRow = currentItem->findChild<QQuickItem*>("ButtonsRow");
        Q_ASSERT(controlsRow);

        for( auto item : controlsRow->childItems() )
        {
            if( item->objectName().compare(PLAY_BUTTON_CONTROL_NAME) == 0 )
            {
                playButtonItem = item;
            }
            if( item->objectName().compare(LOOP_BUTTON_CONTROL_NAME) == 0 )
            {
                loopButtonItem = item;
            }
            if( item->objectName().compare(SEEKBACK_BUTTON_CONTROL_NAME) == 0 )
            {
                seekBackButtonItem = item;
            }
            if( item->objectName().compare(SEEKFORWARD_BUTTON_CONTROL_NAME) == 0 )
            {
                seekForwardButtonItem = item;
            }
        }

        Q_ASSERT(playButtonItem);
        Q_ASSERT(loopButtonItem);

        sliderItem = currentItem->findChild<QQuickItem*>("SliderItem");

        Q_ASSERT(sliderItem);

        connect(qobject_cast<QObject*>(currentItem), SIGNAL(swipeSelectedSignal()),
                this, SLOT(slotProcessWidgetActive()));
        connect(qobject_cast<QObject*>(currentItem), SIGNAL(swipeDeselectedSignal()),
                this, SLOT(slotProcessWidgetUnActive()));
        connect(currentItem , SIGNAL(controlsButtonPressed(QString)),
                this, SLOT(slotControlsButtonPressed(QString)));
        connect(currentItem , SIGNAL(setSliderSeekPos(QVariant)),
                this, SLOT(slotSliderSeekPos(QVariant)));
        connect(qobject_cast<QObject*>(currentItem), SIGNAL(makeFullSizedControll()),
                this, SLOT(slotMakeFullSizedControll()));
    }
}

void PlayBackControlsProcessWidget::show()
{
    emit signalShowMe(name, shared_from_this());
}

bool PlayBackControlsProcessWidget::isWidgetActive()
{
    return isWidgetSelected;
}

void PlayBackControlsProcessWidget::slotProcessWidgetActive()
{
    isWidgetSelected = true;
    emit signalSwipeSelected(name, shared_from_this());
}

void PlayBackControlsProcessWidget::slotProcessWidgetUnActive()
{
    isWidgetSelected = false;
    emit signalSwipeDeselected(name, shared_from_this());
}

std::string PlayBackControlsProcessWidget::getWidgetName()
{
    return name;
}

QObject *PlayBackControlsProcessWidget::getComponent()
{
    return qobject_cast<QObject*>(currentItem);
}

void PlayBackControlsProcessWidget::slotControlsButtonPressed(QString name)
{
    logger->Message(rosslog::LOG_DEBUG, __LINE__, __func__ ,
                    "Button pressed name: %s", name.toStdString().c_str());

    controlCommand(name);
}

void PlayBackControlsProcessWidget::controlCommand(QString name)
{
    if( name.compare("PreviousButt") == 0 )
    {
        if(elapsedTime < 5 || currentAudioState == AUDIO_PAUSE_STATE)
        {
            playBackInterface.previousAudioData(currentPlayingIndex);
        }
        else
        {
            playBackInterface.seekToPos(currentPlayingIndex, 0);
        }
    }
    else if( name.compare("PlayButt") == 0 )
    {
        if( currentAudioState == AUDIO_PLAY_STATE || currentAudioState == AUDIO_LOOP_STATE)
            playBackInterface.pauseAudioPlay(currentPlayingIndex);
        else
            playBackInterface.playAudioData(currentPlayingIndex, elapsedTime);
    }
    else if( name.compare("RepeatButt") == 0 )
    {
        if( currentAudioState == AUDIO_LOOP_STATE )
            playBackInterface.loopAudioPlay(currentPlayingIndex, elapsedTime, true);
        else if( currentAudioState == AUDIO_PLAY_STATE )
            playBackInterface.loopAudioPlay(currentPlayingIndex, elapsedTime, false);
    }
    else if( name.compare("SeekBackButt") == 0 )
    {
        playBackInterface.seekBackAudioPlay(currentPlayingIndex, elapsedTime);
    }
    else if( name.compare("StopButt") == 0 )
    {
        playBackInterface.stopAudioPlay();
    }
    else if( name.compare("SeekForwartButt") == 0 )
    {
        playBackInterface.seekForwardAudioPlay(currentPlayingIndex, elapsedTime);
    }
    else if( name.compare("NextButt") == 0 )
    {
        playBackInterface.nextAudioData(currentPlayingIndex);
    }
//    else if( name.compare("PauseButt") == 0 )
//    {
//        playBackInterface.pauseAudioPlay(currentPlayingIndex);
//    }
}

bool PlayBackControlsProcessWidget::isProcessWidgetShown()
{
    return isWidgetSelected;
}

void
PlayBackControlsProcessWidget::setFreshSessionsData(int countSessions,
                                                    SessionsModelNS::SessionData currentSession,
                                                    int currentIndex)
{
    this->countSelectedSessions = countSessions;
    this->currentSession = currentSession;
    currentPlayingIndex = currentIndex;
    elapsedTime = 0;


    QMetaObject::invokeMethod(currentItem,"setNewData",
                              Q_ARG(QVariant, QVariant::fromValue(currentPlayingIndex + 1)),
                              Q_ARG(QVariant, QVariant::fromValue(countSessions)),
                              Q_ARG(QVariant, "00:00:00"),
                              Q_ARG(QVariant, getLengthString(currentSession.endTime - currentSession.startTime)),
                              Q_ARG(QVariant, ("")),
                              Q_ARG(QVariant, ("")));

    sliderItem->setProperty("value", 0);

}

void PlayBackControlsProcessWidget::updateElapsedTime(int currentIndex, uint64_t time)
{

    uint64_t length = currentSession.endTime - currentSession.startTime;
    uint64_t elapsed = time - currentSession.startTime;

    double proc = elapsed * 100.0 / static_cast<double>(length);

    QDateTime dt = QDateTime::fromSecsSinceEpoch(currentSession.startTime +
                                                 (time - currentSession.startTime));

    QDateTime dtLen = QDateTime::fromSecsSinceEpoch(currentSession.endTime);


    QMetaObject::invokeMethod(currentItem,"setNewData",
                              Q_ARG(QVariant, QVariant::fromValue(currentPlayingIndex + 1)),
                              Q_ARG(QVariant, QVariant::fromValue(this->countSelectedSessions)),
                              Q_ARG(QVariant, getLengthString(time - currentSession.startTime)),
                              Q_ARG(QVariant, getLengthString(length)),
                              Q_ARG(QVariant, (dt.toString("(hh:mm:ss)"))),
                              Q_ARG(QVariant, (dtLen.toString("(hh:mm:ss)"))));

    elapsedTime = elapsed;

    if( !sliderItem->property("pressed").toBool() )
        sliderItem->setProperty("value", proc);
}

QString PlayBackControlsProcessWidget::getLengthString(const uint64_t time)
{
    QString res;
    int secsRange = time;
    int hours = secsRange/3600;
    secsRange = secsRange - (hours * 3600);
    int minutes = secsRange/60;
    secsRange = secsRange - (minutes * 60);
    int seconds = secsRange/60;

    return res.sprintf("%02d:%02d:%02d", hours, minutes, secsRange);
}

void PlayBackControlsProcessWidget::changeAudioProcessState(AudioPlayState state)
{
    if( currentAudioState == state )
        return;

    switch (static_cast<int>(state))
    {
        case AUDIO_PLAY_STATE:
        {
            QMetaObject::invokeMethod(playButtonItem,"setState", Q_ARG(QVariant, 2));
            QMetaObject::invokeMethod(loopButtonItem,"setState", Q_ARG(QVariant, 1));
            repeatMode = false;
//            if( currentAudioState == AUDIO_LOOP_STATE )
//            {
//                QMetaObject::invokeMethod(loopButtonItem,"setState", Q_ARG(QVariant, 1));
//                repeatMode = false;
//            }
            break;
        }
        case AUDIO_PAUSE_STATE:
        {
            QMetaObject::invokeMethod(playButtonItem,"setState", Q_ARG(QVariant, 1));
            break;
        }
        case AUDIO_STOP_STATE:
        {
            QMetaObject::invokeMethod(playButtonItem,"setState", Q_ARG(QVariant, 1));
            QMetaObject::invokeMethod(loopButtonItem,"setState", Q_ARG(QVariant, 1));
            repeatMode = false;
            break;
        }
        case AUDIO_LOOP_STATE:
        {
            QMetaObject::invokeMethod(loopButtonItem,"setState", Q_ARG(QVariant, 2));
            repeatMode = true;
            if( currentAudioState == AUDIO_PAUSE_STATE )
            {
                QMetaObject::invokeMethod(playButtonItem,"setState", Q_ARG(QVariant, 2));
            }
            break;
        }
    }

    currentAudioState = state;
}

const AudioPlayState &PlayBackControlsProcessWidget::getAudioPlayState()
{
    return currentAudioState;
}

const bool &PlayBackControlsProcessWidget::getLoopPlayState()
{
    return repeatMode;
}

void PlayBackControlsProcessWidget::slotSliderSeekPos(QVariant posProc)
{
    uint64_t length = currentSession.endTime - currentSession.startTime;
    auto elapsTime = static_cast<uint64_t>(posProc.toFloat() * length / 100.0);
    playBackInterface.seekToPos(currentPlayingIndex, elapsTime);
}

int PlayBackControlsProcessWidget::getCurrentAudioID()
{
    //TODO CHECK IF WE CAN
    return currentSession.sessionID;
}


void PlayBackControlsProcessWidget::slotMakeFullSizedControll()
{
    if(countSelectedSessions == 0)
    {
        this->userManipulators->showMessage("Сессии не выбраны");
    }
    else
    {
        emit signalMakeFullSized();
    }
}



