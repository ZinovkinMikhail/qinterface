#ifndef COPYPROCESSWIDGET_H
#define COPYPROCESSWIDGET_H

#include <QObject>

#include <iostream>

#include "iprocesswidget.h"
#include "iqmlengine.h"

#define COPY_PROCESS_QML_CLASS "itemClass"
#define COPY_PROCESS_COMPONENT_PATH "qrc:/PlayBackWindow/CopyProcessWidget.qml"

class CopyProcessWidget : public QObject, public IProcessWidget,
        public std::enable_shared_from_this<CopyProcessWidget>
{
    Q_OBJECT
public:
    explicit CopyProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                               std::string name, rosslog::Log &log, QObject *parent = nullptr);

    void init();
    void show();
    void updateCopyData(uint16_t progress, int time, int speed);
    bool isWidgetActive();
    std::string getWidgetName();
    QObject *getComponent();

private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::string name;
    rosslog::Log *logger;
    QObject *widgetComponent;
    bool isWidgetSelected;

    QString getLengthString(const uint64_t time);

signals:
    void signalShowMe(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalSwipeSelected(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalSwipeDeselected(std::string name, std::shared_ptr<IProcessWidget> object);
    void signalMakeFullSized();

public slots:
    void slotProcessWidgetActive();
    void slotProcessWidgetUnActive();

};

#endif // COPYPROCESSWIDGET_H
