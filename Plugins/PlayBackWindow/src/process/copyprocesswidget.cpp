#include <assert.h>

#include "copyprocesswidget.h"

using namespace rosslog;

CopyProcessWidget::CopyProcessWidget(std::shared_ptr<IQmlEngine> qmlEngine,
                                     std::string name,
                                     rosslog::Log &log, QObject *parent) :
    QObject(parent),
    qmlEngine(qmlEngine),
    name(name),
    logger(&log),
    widgetComponent(nullptr),
    isWidgetSelected(false)

{
    assert(logger);
    assert(qmlEngine);
}

void CopyProcessWidget::init()
{
    if( widgetComponent == nullptr )
    {
        widgetComponent = qmlEngine->createComponent(COPY_PROCESS_COMPONENT_PATH);
        assert(widgetComponent);
        connect(widgetComponent, SIGNAL(swipeSelectedSignal()),
                this, SLOT(slotProcessWidgetActive()));
        connect(widgetComponent, SIGNAL(swipeDeselectedSignal()),
                this, SLOT(slotProcessWidgetUnActive()));
    }
}

void CopyProcessWidget::show()
{
    emit signalShowMe(name, shared_from_this());
}

void CopyProcessWidget::updateCopyData(uint16_t progress, int time, int speed)
{
    std::cerr << "UPDATE COPY DAYTA " << progress << " time " << time << " speed " << speed << std::endl;
    QMetaObject::invokeMethod(widgetComponent, "setCopyData",
                              Q_ARG(QVariant, progress),
                              Q_ARG(QVariant, QString::number(progress)),
                              Q_ARG(QVariant, QString::number(speed)),
                              Q_ARG(QVariant, getLengthString(time)));
}

bool CopyProcessWidget::isWidgetActive()
{
    return isWidgetSelected;
}

std::string CopyProcessWidget::getWidgetName()
{
    return name;
}

QString CopyProcessWidget::getLengthString(const uint64_t time)
{
    QString res;
    int secsRange = time;
    int hours = secsRange/3600;
    secsRange = secsRange - (hours * 3600);
    int minutes = secsRange/60;
    secsRange = secsRange - (minutes * 60);
    int seconds = secsRange/60;

    return res.sprintf("%02d:%02d:%02d", hours, minutes, secsRange);
}


QObject *CopyProcessWidget::getComponent()
{
    assert(widgetComponent);
    return widgetComponent;
}


void CopyProcessWidget::slotProcessWidgetActive()
{
    isWidgetSelected = true;
    emit signalSwipeSelected(name, shared_from_this());
}

void CopyProcessWidget::slotProcessWidgetUnActive()
{
    isWidgetSelected = false;
    emit signalSwipeDeselected(name, shared_from_this());
}
