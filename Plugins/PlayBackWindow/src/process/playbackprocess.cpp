
#include "playbackprocess.h"

#include <cassert>
#include <iostream>

using namespace recorder_status_ctrl;
using namespace rosslog;
using namespace SessionsModelNS;

PlayBackProcess::PlayBackProcess(std::shared_ptr<AudioPKDWorker> audioPKDWorker,
                                 std::shared_ptr<IQmlEngine> qmlEngine,
                                 std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusCtrl,
                                 std::shared_ptr<rosslog::Log> logger,
                                 std::shared_ptr<IUserManipulators> iUserManipulators,
                                 IPlayBackProcess &iPlayBackProcess,
                                 QObject *parent) :
    audioPKDWorker(audioPKDWorker),
    qmlEngine(qmlEngine),
    recorderStatusCtrl(recorderStatusCtrl),
    logger(logger),
    iUserManipulators(iUserManipulators),
    iPlayBackProcess(iPlayBackProcess),
    QObject(parent)
{
    assert(this->audioPKDWorker);
    assert(this->qmlEngine);
    assert(this->recorderStatusCtrl);
    assert(this->logger);
    assert(this->iUserManipulators);

    qRegisterMetaType<std::string>("std::string");
    qRegisterMetaType<std::shared_ptr<IProcessWidget>>("std::shared_ptr<IProcessWidget>");
}

PlayBackProcess::~PlayBackProcess()
{}

void PlayBackProcess::init()
{
    initPlayBackProcessWidget();
    connect(this, SIGNAL(signalSeekPos(int, size_t)),
            this, SLOT(slotSeekPos(int, size_t)),Qt::QueuedConnection );
}


void PlayBackProcess::initPlayBackProcessWidget()
{
    if( playBackProcessWidget == nullptr )
    {
        playBackProcessWidget = std::make_shared<PlayBackControlsProcessWidget>(this->qmlEngine,
                                                                                PLAYBACK_PROCES_WIDGET_NAME,
                                                                                logger,
                                                                                *this,
                                                                                iUserManipulators);
        playBackProcessWidget->init();

        connect(playBackProcessWidget.get(), SIGNAL(signalSwipeSelected(std::string, std::shared_ptr<IProcessWidget>)),
                this, SLOT(slotOnProcessWidgetSwipeSelected(std::string, std::shared_ptr<IProcessWidget>)));
        connect(playBackProcessWidget.get(), SIGNAL(signalSwipeDeselected(std::string,std::shared_ptr<IProcessWidget>)),
                this, SLOT(slotOnProcessWidgetSwipeDeSelected(std::string, std::shared_ptr<IProcessWidget>)));
    }
}

bool variantLessThan(const QVariant &v1, const QVariant &v2)
{
    return v1.toInt() < v2.toInt();
}

bool PlayBackProcess::sessionSelectedStateChanged(const SessionData &sessionData, const bool state)
{
    std::cerr << "Session seleted state changed = " << sessionData.sessionID << " state = " << state << std::endl;
    uint8_t curStatus = recordStatus.PlayStatus;

    int currentAudioID = playBackProcessWidget->getCurrentAudioID();   

    if( curStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PLAY ||
            curStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PAUSE)
    {
        if( currentAudioID == sessionData.sessionID )
        {
            this->iUserManipulators->showMessage("Сессия в данный момент проигрывается. Поставьте на стоп, чтобы отменить выбор");
            return false;
        }
    }


    //TODO NOT READYYY YET!!!!!!!!!!!!!!!!!!
    auto it = selectedSessionsMap.find(sessionData.sessionID);
    if( it != selectedSessionsMap.end() )
    {
        if( !state )
        {
            std::cout << "UNselect 1 " << selectedSessionsMap.size() << std::endl;
            selectedSessionsMap.erase(sessionData.sessionID);

            SessionData data = selectedSessionsMap.find(currentAudioID)->second;

            auto it2 = selectedSessionsMap.find(sessionData.sessionID);
            if( it2 != selectedSessionsMap.end() )
            {
                return false;
            }

            checkSessionInList(sessionData.sessionID, false);

            if( currentAudioID == sessionData.sessionID )
            {
                if( selectedSessionsMap.empty() )
                {
                    playBackProcessWidget->setFreshSessionsData(selectedSessionsMap.size(), SessionData(), -1);
                    return true;
                }
                for(auto itFirst: selectedSessionsMap )
                {
                    std::cout << "Reset play index" << std::endl;
                    currentPlayAudioIndex = 0;
                    futurePlayAudioIndex = 0;
                    playBackProcessWidget->setFreshSessionsData(selectedSessionsMap.size(), itFirst.second, 0);
                    break;
                }
            }
            else
            {
                std::cout << "UNselect 2 " << selectedSessionsMap.size() << std::endl;
                std::cout << "UNselect 2 " << selectedSessions.size() << std::endl;

                sessionSelectMutex.lock();

                int index = selectedSessions.indexOf(currentAudioID, 0);

                if( index ==  -1 )
                {
                    sessionSelectMutex.unlock();
                    std::cerr << "ERRORR ONDEX " << currentAudioID  << std::endl;
                    return true;
                }

                currentPlayAudioIndex = index;
                futurePlayAudioIndex = index;
                sessionSelectMutex.unlock();

                playBackProcessWidget->setFreshSessionsData(selectedSessionsMap.size(), data, index);
            }
        }
    }
    else
    {
        if( state )
        {
            std::cout << "Select 1 " << selectedSessionsMap.size() << std::endl;
            std::cout << "Select 1 " << selectedSessions.size() << std::endl;
            selectedSessionsMap[sessionData.sessionID] = sessionData;
            //Если уже есть и идет плей текущей или пауза, то берем текущий айди и выставим с новым списком
           // dataMutex.lock();
            uint8_t curStatus = recordStatus.PlayStatus;
           // dataMutex.unlock();
            if( curStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PLAY || curStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PAUSE)
            {
                sessionSelectMutex.lock();
                int currentAudioID = playBackProcessWidget->getCurrentAudioID();
                SessionData data = selectedSessionsMap.find(currentAudioID)->second;
                checkSessionInList(sessionData.sessionID, true);

                int index = selectedSessions.indexOf(currentAudioID, 0);

                if( index ==  -1 )
                {
                    sessionSelectMutex.unlock();
                    return true;
                }

                playBackProcessWidget->setFreshSessionsData(selectedSessionsMap.size(),
                                                            data,
                                                            index);
                currentPlayAudioIndex = index;
                futurePlayAudioIndex = index;
                sessionSelectMutex.unlock();
            }
            else
            {
                std::cout << "Select 2 " << selectedSessionsMap.size() << std::endl;
                std::cout << "Select 2 " << selectedSessions.size() << std::endl;
//                std::cerr << "currentPlayAudioIndex = " << currentPlayAudioIndex << std::endl;

                sessionSelectMutex.lock();

                int currentAudioID = playBackProcessWidget->getCurrentAudioID();

                if(sessionData.sessionID < currentAudioID)
                {
                    currentPlayAudioIndex ++;
                    futurePlayAudioIndex ++;
                }

                sessionSelectMutex.unlock();
                for(auto itFirst: selectedSessionsMap )
                {
                    int freshIndex = checkSessionInList(sessionData.sessionID, true);
                    SessionData data = selectedSessionsMap.find(itFirst.second.sessionID)->second;
                    playBackProcessWidget->setFreshSessionsData(selectedSessionsMap.size(), data, 0);
                    break;
                }
            }
        }
    }

    return true;
}

bool PlayBackProcess::getSelectedSessions(std::vector<SessionData> &sessions)
{
    for( auto session: selectedSessions )
    {
        sessions.push_back(selectedSessionsMap[session]);
    }
    return true;
}

void PlayBackProcess::tapMenuPlay()
{
    if(selectedSessionsMap.size() == 0)
    {
        iUserManipulators->showMessage("Сессии не выбраны");
    }
    else if (playBackProcessWidget->getAudioPlayState() == AUDIO_PLAY_STATE)
    {
        iUserManipulators->showMessage("Воспроизведение уже запущено");
    }
    else
    {
        playBackProcessWidget->controlCommand("PlayButt");
    }
}


int PlayBackProcess::checkSessionInList(int sessionId, bool isAppend)
{
    int freshIndex = -1;
    if( isAppend )
    {
        if( selectedSessions.isEmpty() )
        {
            selectedSessions.push_back(sessionId);
        }
        else
        {
            {
                selectedSessions.push_back(sessionId);
                qSort(selectedSessions.begin(), selectedSessions.end(), variantLessThan);
            }
        }
    }
    else
    {
        if( selectedSessions.isEmpty() )
        {
            return - 1;
        }
        else
        {
            int index = selectedSessions.indexOf(sessionId, 0);
            if( index !=  -1 )
            {
                selectedSessions.removeAt(index);
                qSort(selectedSessions.begin(), selectedSessions.end(), variantLessThan);
            }
        }
    }

    return  freshIndex;
}



void PlayBackProcess::updatePlayProcessData(SoftRecorderStatusStruct_t recordStatus)
{
  //  dataMutex.lock();
    this->recordStatus = recordStatus;
  //  dataMutex.unlock();


    if( selectedSessions.isEmpty() )
    {
        if( ourStatus == AUDIO_STOP_STATE && recordStatus.PlayStatus != AUDIO_STOP_STATE
            && recordStatus.PlayStatus != AUDIO_NONE_STATE )
        {
            this->stopAudioPlay();
        }
        return;
    }

    if( !sessionSelectMutex.try_lock() )
    {
        return;
    }

    sessionSelectMutex.unlock();

    //Если мы изменили статус и отправили, и рекордер подтвердил изменения, то меняем наш стейт контролов


    if( ourStatus == recordStatus.PlayStatus )
    {
        //Рекордер подтвердил остановку и мы сошлись, сбрасываем визуал
        if( ourStatus == AUDIO_STOP_STATE )
        {
            this->playBackProcessWidget->changeAudioProcessState(AUDIO_STOP_STATE);

            if( !selectedSessions.isEmpty() )
            {
                if( currentPlayAudioIndex < selectedSessions.size() )
                {
                    auto it = selectedSessionsMap.find(selectedSessions[currentPlayAudioIndex]); //TODO: do better
                    SessionsModelNS::SessionData curAudio = it->second;
                    this->iPlayBackProcess.currentPlayTimeChanged(curAudio.sessionID, 0);
                }
                else
                {
                    this->logger->Message(LOG_ERR, __LINE__, __func__, "Current index session is invalid. stoping.. at index 0 ");
                    currentPlayAudioIndex = 0;
                    futurePlayAudioIndex = 0;
                }
                auto it = selectedSessionsMap.find(selectedSessions[currentPlayAudioIndex]);

                if( it == selectedSessionsMap.end() )
                {
                    this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at AUDIO STATE STOP");
                    return;
                }


                this->playBackProcessWidget->setFreshSessionsData(selectedSessions.size(),
                                                                  it->second,
                                                                  currentPlayAudioIndex);
            }
            return;
        }
        this->playBackProcessWidget->changeAudioProcessState(static_cast<AudioPlayState>(ourStatus));
    }

    if( ourStatus == AUDIO_PLAY_STATE && recordStatus.PlayStatus == AUDIO_PAUSE_STATE )
    {
        ourStatus = AUDIO_PAUSE_STATE;
        this->playBackProcessWidget->changeAudioProcessState(static_cast<AudioPlayState>(ourStatus));
    }


    if( ourStatus == AUDIO_PAUSE_STATE && recordStatus.PlayStatus == AUDIO_STOP_STATE )
    {
        ourStatus = AUDIO_STOP_STATE;
        this->playBackProcessWidget->changeAudioProcessState(static_cast<AudioPlayState>(ourStatus));
    }


    this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Current play index = %i, size = %i",
                          currentPlayAudioIndex, selectedSessions.size());

    //Проверяем что до этого плеер работал и остановился, мы должны проверить что мы готовы проиграть следующую сессию
    if( previousRecordStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PLAY &&
    recordStatus.PlayStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__STOP &&
    playBackProcessWidget->getAudioPlayState() == AUDIO_PLAY_STATE )
    {
        this->logger->Message(LOG_DEBUG, __LINE__, __func__, "----- Try play next song");

        if( this->playNextAudioSession() )
        {
            return;
        }
        else
        {
            this->logger->Message(LOG_INFO, __LINE__, __func__, "We played all selected sessions" );
            ourStatus = AUDIO_STOP_STATE;
            //У нас больше нет аудио для воспроизведения возможно нужно показать это пользователю. Сменим статус визуала
        }
    }

    auto it = selectedSessionsMap.find(selectedSessions[currentPlayAudioIndex]);

    if( it == selectedSessionsMap.end() )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at Current audio STATE ");
        return;
    }

    SessionsModelNS::SessionData curAudio = it->second;


    //Проверяем прошел ли рекордер на следующую сессию записи
    if( recordStatus.PlayTime > curAudio.endTime && recordStatus.PlayStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PLAY )
    {
//            if( futurePlayAudioIndex > currentPlayAudioIndex ) //Это условие мешает работе если перемотать из гафика на предыдущию смежную сессию.
//            {
            //Проверяем что мы можем себе это позволить;
            if( futurePlayAudioIndex < selectedSessions.size() )
            {
                this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Recorder said next session %li ",
                                      recordStatus.PlayTime);

                auto it = selectedSessionsMap.find(selectedSessions[futurePlayAudioIndex]);

                if( it == selectedSessionsMap.end() )
                {
                    this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at Future play next STATE ");
                    return;
                }

                this->playBackProcessWidget->setFreshSessionsData(selectedSessions.size(),
                                                                  it->second,
                                                                  futurePlayAudioIndex);
                currentPlayAudioIndex = futurePlayAudioIndex;
            }
            else
            {
                //Рекордер пошел дальще, а у нас нет такого, надо его стопорить.
                this->stopAudioPlay();
            }
//            }
//            else
//            {
//                this->logger->Message(LOG_ERR, __LINE__, __func__,
//                                      "Recorder said next session but we not ready ERROR %li future index %i, cur index %i",
//                                      recordStatus.PlayTime, futurePlayAudioIndex, currentPlayAudioIndex);
//                this->stopAudioPlay();
//            }
        return;
    } // При изменении аудио сессии назад, мы должны среагировать на это
    else if( recordStatus.PlayTime < curAudio.startTime && recordStatus.PlayStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PLAY )
    {
        if( currentPlayAudioIndex == 0 )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Recorder said prev session but we can't %li ",
                                  recordStatus.PlayTime);
            this->stopAudioPlay();
        }
        if( futurePlayAudioIndex < selectedSessions.size()  )
        {
            this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Recorder said prev session %li ",
                                  recordStatus.PlayTime);

            auto it = selectedSessionsMap.find(selectedSessions[futurePlayAudioIndex]);

            if( it == selectedSessionsMap.end() )
            {
                this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at Future play prev STATE ");
                return;
            }

            this->playBackProcessWidget->setFreshSessionsData(selectedSessions.size(),
                                                              it->second,
                                                              futurePlayAudioIndex);
            currentPlayAudioIndex = futurePlayAudioIndex;
        }
        else
        {
            //Рекордер пошел дальще, а у нас нет такого, надо его стопорить.
            this->stopAudioPlay();
        }
    }
    else if( recordStatus.PlayTime <= curAudio.endTime &&
            recordStatus.PlayTime >= curAudio.startTime &&
            recordStatus.PlayStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PLAY ) //Проверяем на изменение канала.
    {
//            if( recordStatus.PlayTime < previousRecordTime - 2 && currentPlayAudioIndex != futurePlayAudioIndex )
//            {
          if(currentPlayAudioIndex != futurePlayAudioIndex )
          {
            //Похоже что сменили Аудио но канал тот же
            if( futurePlayAudioIndex < selectedSessions.size()  && futurePlayAudioIndex >= 0 )
            {
                this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Recorder said play another channel session with same time %li ",
                                      recordStatus.PlayTime);

                auto it = selectedSessionsMap.find(selectedSessions[futurePlayAudioIndex]);

                if( it == selectedSessionsMap.end() )
                {
                    this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at Future play same time STATE ");
                    return;
                }

                this->playBackProcessWidget->setFreshSessionsData(selectedSessions.size(),
                                                                  it->second,
                                                                  futurePlayAudioIndex);
                currentPlayAudioIndex = futurePlayAudioIndex;
            }
            else
            {
                //Рекордер пошел дальще, а у нас нет такого, надо его стопорить.
                this->stopAudioPlay();
            }
        }
    }

    //Как мы реагируем если он уже играет?
    if( recordStatus.PlayStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__PLAY ||
            recordStatus.PlayStatus == AXIS_SOFTRECORDER_PLAYER_COMMAND__LOOP ) //ourStatus == AUDIO_PLAY_STATE
    {

        auto it = selectedSessionsMap.find(selectedSessions[currentPlayAudioIndex]);

        if( it != selectedSessionsMap.end() )
        {
            if( it->second.startTime > recordStatus.PlayTime )
            {
                this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Invalid time in status pack %li  out start time %li ",
                                      recordStatus.PlayTime, it->second.startTime);
                return;
            }
        }
        this->playBackProcessWidget->updateElapsedTime(currentPlayAudioIndex, recordStatus.PlayTime);
        this->iPlayBackProcess.currentPlayTimeChanged(curAudio.sessionID, recordStatus.PlayTime);
    }

    previousRecordTime = recordStatus.PlayTime;
    previousRecordStatus = recordStatus.PlayStatus;


}

void PlayBackProcess::updateRecorderSettingsControl(SoftRecorderSettingsStruct_t recorderSettings)
{
    recorderDataUpdate.lock();
    this->recordSettings = recorderSettings;
    recorderDataUpdate.unlock();
}

void PlayBackProcess::updateFilterMaskData(SessionMask maskData)
{
    this->maskData = maskData;
}

bool PlayBackProcess::seekAudioToPosFromGraph(int sessionID, uint64_t seekSec)
{
    //    logger->Message(LOG_DEBUG, __LINE__, __func__, "+++ emit signalTest1");
    //    emit signalTest1(sessionID, seekSec);
        std::cerr << "------------seekAudioToPosFromGraph-----------" << std::endl;
        std::cerr << "sessionID = " << sessionID << std::endl;
        std::cerr << "seekSec = " << seekSec << std::endl;
        try
        {
            SessionData &curSession = selectedSessionsMap.at(sessionID);

            if (curSession.startTime <= seekSec && curSession.endTime >= seekSec)
            {
    #ifdef DEBUG_TEST
                int index = selectedSessions.indexOf(sessionID, 0);
                if( index ==  -1 )
                {
                    this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid session list index!");
                    return false;
                }
                currentPlayAudioIndex = index;
                recordStatus.PlayTime = seekSec;
    #else
    #endif
                if(selectedSessions.contains(sessionID))
                {
                    int sessIndex = 0;
                    for( int i = 0; i < selectedSessions.length(); i++)
                    {
                        if(selectedSessions[i] == sessionID)
                        {
                            sessIndex = i;
                        }
                    }

                    if(sessionID != playBackProcessWidget->getCurrentAudioID())
                    {
                        playSessFromPos(sessIndex, seekSec - curSession.startTime);
                    }
                    else
                    {
                        seekToPos(sessIndex, seekSec - curSession.startTime);
                    }
                }
                else
                {
                    this->logger->Message(LOG_ERR, __LINE__, __func__, "Session id not found in selected sessions");
                }

            }
            else
            {
                this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid time to seek start %li, end %li, set %li!",
                                      curSession.startTime, curSession.endTime, seekSec);
                this->iUserManipulators->showMessage("Неверно задано время для прокрутки");
            }
        }
        catch (std::exception &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid seek session id from graph !");
        }

        return false;
}

uint8_t PlayBackProcess::getOurState()
{
    return ourStatus;
}

bool PlayBackProcess::getProcessWidgetState()
{
    return isProcessWidgetVisible;
}

std::shared_ptr<PlayBackControlsProcessWidget> PlayBackProcess::getProcessWidget()
{
    return playBackProcessWidget;
}

void PlayBackProcess::stopAudioImidiatly()
{
    try
    {
        this->logger->Message(LOG_DEBUG, __LINE__, __func__ , "------- Set stop Audio");

        if( playerFuncMutex.try_lock() )
        {
            playerFuncWorker = std::async( std::launch::async,[this]
            {
                try
                {
                    this->logger->Message(LOG_INFO, __LINE__, __func__, "Send STOP audio play");
                    recorderStatusCtrl->stopPlayAudio();
                    ourStatus = AUDIO_STOP_STATE;
                }
                catch (std::system_error &except)
                {
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__ , "Error while setting audio control state %s", except.what());
                }
                playerFuncMutex.unlock();
            });
            playerFuncWorker.wait_for(std::chrono::seconds(0));
        }
        else
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
        }
    }
    catch (std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__ , " Error while stop audio %s", except.what());
    }
}


bool PlayBackProcess::playNextAudioSession()
{
    if( selectedSessions.size() > currentPlayAudioIndex + 1 )
    {
        auto it = selectedSessionsMap.find(selectedSessions[currentPlayAudioIndex+1]);

        if( it == selectedSessionsMap.end() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at playNextAudioSession Play");
            return false;
        }


        SessionsModelNS::SessionData nextAudio = it->second;

        if( playerFuncMutex.try_lock() )
        {
            playerFuncWorker = std::async( std::launch::async,[this, nextAudio]
            {
                try
                {

                    char code[AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE] = "";

                    prepareMaskCode(code);

                    this->logger->Message(LOG_INFO, __LINE__, __func__ , "SEND Play next audio address start %li , address end = %li , pos = %li channel type = %i ",
                                          nextAudio.startRecAddress, nextAudio.endRecAddress, nextAudio.startRecAddress, nextAudio.channelType);

                    this->recorderStatusCtrl->startPlayAudio(nextAudio.channelType, 0, nextAudio.startRecAddress,
                                                                nextAudio.endRecAddress, nextAudio.startRecAddress, code);
                    ourStatus = AUDIO_PLAY_STATE;
                }
                catch (std::system_error &except)
                {
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__ , "Error while setting audio control state %s", except.what());
                }
                playerFuncMutex.unlock();

            });
            playerFuncWorker.wait_for(std::chrono::seconds(0));
        }
        else
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
        }

        futurePlayAudioIndex = currentPlayAudioIndex + 1;
        return true;
    }
    else
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__ , "Index out of range selected sessions");
    }
    return false;
}

bool PlayBackProcess::playPrevAudioSession()
{
    if( selectedSessions.size() > currentPlayAudioIndex - 1 && currentPlayAudioIndex - 1 >= 0)
    {
        auto it = selectedSessionsMap.find(selectedSessions[currentPlayAudioIndex-1]);

        if( it == selectedSessionsMap.end() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at playPrevAudioSession Play");
            return false;
        }

        SessionsModelNS::SessionData nextAudio = it->second;

        if( playerFuncMutex.try_lock() )
        {
            playerFuncWorker = std::async( std::launch::async,[this, nextAudio]
            {
                try
                {
                    char code[AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE] = "";

                    prepareMaskCode(code);
                    this->logger->Message(LOG_INFO, __LINE__, __func__ , "SEND Play prev audio address start %li , address end = %li , pos = %li",
                                          nextAudio.startRecAddress, nextAudio.endRecAddress, nextAudio.startRecAddress);

                    this->recorderStatusCtrl->startPlayAudio(nextAudio.channelType, 0, nextAudio.startRecAddress,
                                                                nextAudio.endRecAddress, nextAudio.startRecAddress, code);
                    ourStatus = AUDIO_PLAY_STATE;
                }
                catch (std::system_error &except)
                {
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__ , "Error while setting audio control state %s", except.what());
                }
                playerFuncMutex.unlock();
            });
            playerFuncWorker.wait_for(std::chrono::seconds(0));
        }
        else
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
        }

                            //TODO CODE;
        futurePlayAudioIndex = currentPlayAudioIndex - 1;
        return  true;
    }
    return false;
}

bool PlayBackProcess::continuePlayFromPos(int currentIndex, uint64_t playPos)
{
    if( currentIndex < selectedSessions.size() )
    {
        auto it = selectedSessionsMap.find(selectedSessions[currentIndex]);

        if( it == selectedSessionsMap.end() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at Future play prev STATE ");
            return false;
        }

        SessionsModelNS::SessionData curAudio = it->second;
        uint32_t curAddress = 0;

        if(curAudio.startTime + playPos < curAudio.endTime )
        {
            if( audioPKDWorker->getPKDAddress(curAudio.sessionID, curAudio.startTime + playPos , curAudio.channelType, curAddress) )
            {
                this->logger->Message(LOG_DEBUG, __LINE__, __func__ ,
                                      "Seek audio to time %li to address %li",
                                      curAudio.startTime + playPos, curAddress * 64);

#ifndef DEBUG_TEST
                try
                {
                    std::cerr << "----------- CONTINUE TO PLAY " << curAudio.startRecAddress  << " POS " << curAddress * 64 << std::endl;

                    if( playerFuncMutex.try_lock() )
                    {
                        playerFuncWorker = std::async( std::launch::async,[this, curAudio, curAddress]
                        {
                            char code[AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE] = "";
                            prepareMaskCode(code);
                            try
                            {
                                this->logger->Message(LOG_INFO, __LINE__, __func__,
                                                      "SEND Continue play audio start address %li, end address %li, pos %li, channel %i",
                                                      curAudio.startRecAddress, curAudio.endRecAddress, curAudio.startRecAddress, curAudio.channelType);
                                this->recorderStatusCtrl->startPlayAudio(curAudio.channelType, 0, curAudio.startRecAddress,
                                                                            curAudio.endRecAddress, curAddress * 64, code);
                            }
                            catch (std::system_error &except)
                            {
                                this->logger->Message(LOG_ERR, __LINE__,
                                                      __func__ , "Error while setting audio control state %s", except.what());
                            }
                            playerFuncMutex.unlock();
                        });
                        playerFuncWorker.wait_for(std::chrono::seconds(0));
                    }
                    else
                    {
                        logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
                    }
                }
                catch( std::system_error &except )
                {
                    this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to play audio from pos");
                    this->iUserManipulators->showMessage("Не удалось воспроизвести аудио с позиции аудио");
                    return false;
                }
#else
                recordStatus.PlayTime = curAudio.startTime + playPos;
#endif
            }
            else
            {
                this->logger->Message(LOG_ERR, __LINE__, __func__ , "Error while get pkd address");
                this->iUserManipulators->showMessage("Ошибка выбора адреса пкд");
                return false;
            }
        }
    }
    return true;
}

void PlayBackProcess::playSessFromPos(int sessIndex, uint64_t playPos)
{
    std::cerr << "------------playSessFromPos-----------" << std::endl;
    std::cerr << "sessIndex = " << sessIndex << std::endl;
    std::cerr << "playPos = " << playPos << std::endl;

    if( sessIndex < selectedSessions.size() )
    {
        auto it = selectedSessionsMap.find(selectedSessions[sessIndex]);

        if( it == selectedSessionsMap.end() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key!");
            return;
        }

        SessionsModelNS::SessionData nextAudio = it->second;

        if( playerFuncMutex.try_lock() )
        {
            playerFuncWorker = std::async( std::launch::async,[this, nextAudio, sessIndex, playPos]
            {
                try
                {
                    char code[32] = "";
                    prepareMaskCode(code);

//                    std::cerr << "Send command startPlayAudio " << std::endl;
//                    logger->Message(LOG_DEBUG, __LINE__, __func__, "+++ startPlayAudio");
                    this->recorderStatusCtrl->startPlayAudio(nextAudio.channelType, 0, nextAudio.startRecAddress,
                                                                nextAudio.endRecAddress, nextAudio.startRecAddress, code);
                    ourStatus = AUDIO_PLAY_STATE;
                    std::cerr << "Sess index = " << sessIndex << " Play pos " << playPos << std::endl;
                }
                catch (std::system_error &except)
                {
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__ , "Error while setting audio control state %s", except.what());
                }
                playerFuncMutex.unlock();
//                logger->Message(LOG_DEBUG, __LINE__, __func__, "+++ EMIT signalSeekPos");
                emit signalSeekPos(sessIndex, playPos);
            });
            playerFuncWorker.wait_for(std::chrono::seconds(0));
        }
        else
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "THREAD playerFuncWorker STILL BUSY!! ");
            return;
        }

//        logger->Message(LOG_DEBUG, __LINE__, __func__, "+++ futurePlayAudioIndex = sessIndex");
        futurePlayAudioIndex = sessIndex;
    }
    else
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__ , "Index out of range selected sessions");
    }
}

void PlayBackProcess::prepareMaskCode(char *code)
{
    if( maskData.isActive )
    {
        if( maskData.code.length() > 0 )
        {
            char chPassword[33];
            char chUser[33];
            QTextCodec *codec = QTextCodec::codecForName("CP1251");

            strncpy(chPassword, VENDOR_PASSWORD, AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE);
            strncpy(chUser, codec->fromUnicode(QString::fromStdString(maskData.code)).toStdString().c_str(), AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE);

            for(int i = 0; i < AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE; i++)
            {
                chPassword[i] = chPassword[i] ^ chUser[i];
                if(chPassword[i] == 0)
                {
                    chPassword[i] = 0x5a;
                }
                printf("%x", (uint8_t)chPassword[i]);
            }
            printf("\n");

            memcpy(code, &chPassword, AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE);
        }
//        std::cerr << "SET CODE MASK CODEPTR " << code << std::endl;
    }
    else
    {
        recorderDataUpdate.lock();
        //recorderSettings.SecurityCode[31] = 0x00;
        //code = (char*)recordSettings.SecurityCode;
        memcpy(code, (char*)recordSettings.SecurityCode, AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE);
        recorderDataUpdate.unlock();

        for(int i = 0; i < AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE; i++)
        {
            printf("%x", (uint8_t)recordSettings.SecurityCode[i]);
        }

//        printf("Sec code = %s \n", (char*)recordSettings.SecurityCode);
//        std::cerr << "SET CODE CODEPTR " << code << std::endl;
    }
}

static int errorCounter = 0;

void PlayBackProcess::compareRecordAndOurPos()
{
    auto it = selectedSessionsMap.find(selectedSessions[currentPlayAudioIndex]);

    if( it == selectedSessionsMap.end() )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at Future play prev STATE ");
        return;
    }

    SessionsModelNS::SessionData curAudio = it->second;

    uint32_t curAddress;

    if( audioPKDWorker->getPKDAddress(curAudio.sessionID, recordStatus.PlayTime , curAudio.channelType, curAddress) )
    {
        this->logger->Message(LOG_DEBUG, __LINE__, __func__,
                              "CHECK TIME %li to address %li",
                              recordStatus.PlayTime , curAddress * 64);
        if( curAddress * 64 != recordStatus.PlayPosition && recordStatus.PlayPosition > 0 )
        {
            std::cerr << "WE HAVE ERROR PLAY ADDRESS BY IGOR = " << recordStatus.PlayPosition << " our address " << curAddress * 64 << " ERRORS " << errorCounter << std::endl;
            errorCounter++;
        }
    }
    else
    {
        std::cerr << "Error while get from DB time " << std::endl;
    }
}


//IPlayBackProcess
bool PlayBackProcess::playAudioData(int currentIndex, uint64_t playPos)
{
    this->logger->Message(LOG_DEBUG, __LINE__, __func__, "Play button clicked!");

    if( !(recordStatus.ExtStatus & RECORDER_EXT_STATUS__PLAY_PHONE) )
    {
        this->iUserManipulators->showMessage("Запуск воспроизведения недоступен. Подключите устройство прослушивания.");
        return false;
    }

    try
    {
        if( !selectedSessions.isEmpty() && !selectedSessionsMap.empty() )
        {
#ifdef DEBUG_TEST
            currentIndex = 0;
#endif
            SessionsModelNS::SessionData &session = selectedSessionsMap[selectedSessions[currentIndex]];

            std::cerr << "Session id = " << session.sessionID << std::endl;
            std::cerr << "Start addr = " << session.startRecAddress << " (" << session.startRecAddress / 512 << ")" << std::endl;
            std::cerr << "End addr = " << session.endRecAddress << " (" << session.endRecAddress / 512 << ")" << std::endl;

            std::cerr << "AUDIO STATE OUR = " << playBackProcessWidget->getAudioPlayState() << std::endl;

            //Если мы были на паузе то продолжим с текущего положения
            if( playBackProcessWidget->getAudioPlayState() == AUDIO_PAUSE_STATE )
            {
                std::cerr << "---------- CONTINUE FROM POS PLAY " << currentIndex << " " << playPos << std::endl;
                if( !continuePlayFromPos(currentIndex, playPos) )
                    return false;
                else if(playBackProcessWidget->getLoopPlayState())
                    ourStatus = AUDIO_LOOP_STATE;
                else
                    ourStatus = AUDIO_PLAY_STATE;
            }
            else
            {
#ifdef DEBUG_TEST
                ourStatus = AUDIO_PLAY_STATE;
                recordStatus.PlayTime = session.startTime;
#else

                if( playerFuncMutex.try_lock() )
                {
                    playerFuncWorker = std::async( std::launch::async,[this, session]
                    {
                        try
                        {
                            this->logger->Message(LOG_INFO, __LINE__, __func__,
                                                  "SEND Start play audio start address %li, end address %li, pos %li, channel %i",
                                                  session.startRecAddress, session.endRecAddress, session.startRecAddress, session.channelType);
                            char code[AXIS_SOFTRECORDER_SETTINGS__SECURITY_CODE_SIZE] = "";

                            prepareMaskCode(code);
                            recorderStatusCtrl->startPlayAudio(session.channelType, 0, session.startRecAddress,
                                                                  session.endRecAddress, session.startRecAddress, code);
                            ourStatus = AUDIO_PLAY_STATE;
                        }
                        catch (std::system_error &except)
                        {
                            this->logger->Message(LOG_ERR, __LINE__,
                                                  __func__ , "Error while setting audio control state %s", except.what());
                        }
                        playerFuncMutex.unlock();

                    });
                    playerFuncWorker.wait_for(std::chrono::seconds(0));
                }
                else
                {
                    logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
                }
#endif
                currentPlayAudioIndex = currentIndex;
            }
        }

    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to set audio pos");
        this->iUserManipulators->showMessage("Не удалось перемотать аудио");
    }
    return true;
}

bool PlayBackProcess::nextAudioData(int currentIndex)
{
    try
    {
        // Мы и так уже сделали переключение но оно пока не сработало
        if( futurePlayAudioIndex != currentPlayAudioIndex )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "We did select next/prev session wait for set");
            return false;
        }
        if( !playNextAudioSession() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Can't set next audio session");
            this->iUserManipulators->showMessage("Не удалось выбрать следующую аудио сессию");
        }
    }
    catch (std::system_error &error)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Can't set next audio session");
        this->iUserManipulators->showMessage("Не удалось выбрать следующую аудио сессию");
    }
    return false;
}

bool PlayBackProcess::previousAudioData(int currentIndex)
{
    try
    {
        // Мы и так уже сделали переключение но оно пока не сработало
        if( futurePlayAudioIndex != currentPlayAudioIndex )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "We did select next/prev session wait for set");
            return false;
        }
        if( !playPrevAudioSession() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Can't set prev audio session");
            this->iUserManipulators->showMessage("Не удалось выбрать предыдущую аудио сессию");
        }
    }
    catch (std::system_error &error)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Can't set prev audio session");
        this->iUserManipulators->showMessage("Не удалось выбрать предыдущую аудио сессию");
    }
    return false;
}

bool PlayBackProcess::stopAudioPlay()
{
    if(ourStatus == AUDIO_STOP_STATE)
    {
        futurePlayAudioIndex = 0;
        currentPlayAudioIndex = 0;
    }

    try
    {
        this->logger->Message(LOG_DEBUG, __LINE__, __func__ , "------- Set stop Audio");

        if( playerFuncMutex.try_lock() )
        {
            playerFuncWorker = std::async( std::launch::async,[this]
            {
                try
                {
                    this->logger->Message(LOG_INFO, __LINE__, __func__, "Send STOP audio play");
                    recorderStatusCtrl->stopPlayAudio();
                    ourStatus = AUDIO_STOP_STATE;
                }
                catch (std::system_error &except)
                {
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__ , "Error while setting audio control state %s", except.what());
                }
                playerFuncMutex.unlock();
            });
            playerFuncWorker.wait_for(std::chrono::seconds(0));
        }
        else
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
        }
    }
    catch (std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__ , " Error while stop audio %s", except.what());
    }
    return false;
}

bool PlayBackProcess::pauseAudioPlay(int currentIndex)
{
    try
    {
        this->logger->Message(LOG_DEBUG, __LINE__, __func__ , "------- Set pause Audio");
        if( playerFuncMutex.try_lock() )
        {
            playerFuncWorker = std::async( std::launch::async,[this]
            {
                try
                {
                    this->logger->Message(LOG_INFO, __LINE__, __func__, "Send PAUSE audio play");
                    this->recorderStatusCtrl->pausePlayAudio();
                    ourStatus = AUDIO_PAUSE_STATE;
                }
                catch (std::system_error &except)
                {
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__ , "Error while setting audio control state %s", except.what());
                }
                playerFuncMutex.unlock();
            });
            playerFuncWorker.wait_for(std::chrono::seconds(0));
        }
        else
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
        }
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to set pause audio play");
        this->iUserManipulators->showMessage("Не удалось установить паузу");
    }
    return false;
}

bool PlayBackProcess::loopAudioPlay(int currentIndex, uint64_t playPos, bool isLoopPlay)
{
    if( currentIndex < selectedSessions.size() && !selectedSessionsMap.empty() )
    {
        auto it = selectedSessionsMap.find(selectedSessions[currentIndex]);

        if( it == selectedSessionsMap.end() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at loopAudio Play");
            return false;
        }
        SessionsModelNS::SessionData curAudio = it->second;

        if( playerFuncMutex.try_lock() )
        {
            playerFuncWorker = std::async( std::launch::async,[this, curAudio, playPos, isLoopPlay]
            {
                try
                {
                    if( isLoopPlay )
                    {
                        this->logger->Message(LOG_INFO, __LINE__, __func__, "Send STOP loop audio play");
                        this->recorderStatusCtrl->stopLoopPlayAudio();
                        ourStatus = AUDIO_PLAY_STATE;
                    }
                    else
                    {
                        uint32_t curAddress = 0;
                        uint32_t fromAddress = 0;
                        uint64_t timeFrom = curAudio.startTime + playPos - AUDIO_PLAY_REPEAT_SEC;

                        if(timeFrom <= curAudio.startTime)
                        {
                            timeFrom = curAudio.startTime + 1;
                        }

                        if( audioPKDWorker->getPKDAddress(curAudio.sessionID, curAudio.startTime + playPos , curAudio.channelType, curAddress) &&
                            audioPKDWorker->getPKDAddress(curAudio.sessionID, timeFrom , curAudio.channelType, fromAddress))
                        {
                            this->logger->Message(LOG_INFO, __LINE__, __func__ ,
                                                  "SEND LOOP Play audio address start %li , address end = %li , pos = %li",
                                                  fromAddress * 64, curAddress * 64, curAddress * 64);

                            this->recorderStatusCtrl->loopPlayAudio(fromAddress * 64, curAddress * 64,
                                                                       curAddress * 64 - 512);
                        }
                        else
                        {
                            std::cerr << "Error while get from DB time " << std::endl;
                            this->logger->Message(LOG_ERR, __LINE__, __func__, "Can't get db address");
                        }


                        ourStatus = AUDIO_LOOP_STATE;
                    }
                }
                catch (std::system_error &except)
                {
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__ , "Error while setting audio control state %s", except.what());
                }
                playerFuncMutex.unlock();
            });
            playerFuncWorker.wait_for(std::chrono::seconds(0));
        }
        else
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
        }
    }
    return false;
}

bool PlayBackProcess::seekForwardAudioPlay(int currentIndex, uint64_t playPos)
{
    if( currentIndex < selectedSessions.size() && !selectedSessionsMap.empty() )
    {
        auto it = selectedSessionsMap.find(selectedSessions[currentIndex]);

        if( it == selectedSessionsMap.end() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at seekForward Play");
            return false;
        }
        SessionsModelNS::SessionData curAudio = it->second;

        if(curAudio.startTime + playPos + AUDIO_PLAY_SEEK_SEC < curAudio.endTime )
        {
            seekToPos(currentIndex,  playPos + AUDIO_PLAY_SEEK_SEC);
        }
    }
    return false;
}

bool PlayBackProcess::seekBackAudioPlay(int currentIndex, uint64_t playPos)
{
    if( currentIndex < selectedSessions.size() && !selectedSessionsMap.empty() )
    {
        auto it = selectedSessionsMap.find(selectedSessions[currentIndex]);

        if( it == selectedSessionsMap.end() )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at seekBackAudioPlay Play");
            return false;
        }

        SessionsModelNS::SessionData curAudio = it->second;
        uint32_t curAddress = 0;
        try
        {
            if (curAudio.startTime != playPos && curAudio.startTime + playPos - AUDIO_PLAY_SEEK_SEC > curAudio.startTime)
            {
                seekToPos(currentIndex, playPos - AUDIO_PLAY_SEEK_SEC);
            }
            else if (curAudio.startTime != playPos)
            {
                seekToPos(currentIndex, 0);
            }
        }
        catch( std::system_error &except )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Unable to set audio pos");
            this->iUserManipulators->showMessage("Не удалось перемотать аудио");
            return false;
        }
    }
    return false;
}

bool PlayBackProcess::seekToPos(int currentIndex, uint64_t playPos)
{
    //    logger->Message(LOG_DEBUG, __LINE__, __func__, "+++ ------------seekToPos-----------");
        std::cerr << "------------seekToPos-----------" << std::endl;
        std::cerr << "currentIndex = " << currentIndex << std::endl;
        std::cerr << "playPos = " << playPos << std::endl;
        if(playPos == 0)
            playPos = 1;

        if( currentIndex < selectedSessions.size() )
        {
            auto it = selectedSessionsMap.find(selectedSessions[currentIndex]);

            if( it == selectedSessionsMap.end() )
            {
                this->logger->Message(LOG_ERR, __LINE__, __func__, "Sessions invalid key at seekToPos Play");
                return false;
            }

            SessionsModelNS::SessionData curAudio = it->second;
            uint32_t curAddress = 0;

            std::cerr << "curAudio.startTime + playPos = " << curAudio.startTime + playPos << std::endl;
            std::cerr << "curAudio.endTime = " << curAudio.endTime << std::endl;

            if(curAudio.startTime + playPos < curAudio.endTime )
            {
                if( audioPKDWorker->getPKDAddress(curAudio.sessionID, curAudio.startTime + playPos , curAudio.channelType, curAddress) )
                {
                    this->logger->Message(LOG_INFO, __LINE__, __func__ ,
                                          "Seek audio to time %li to address %li",
                                          curAudio.startTime + playPos, (uint64_t)curAddress * 64);

                    if( recordStatus.PlayStatus == AUDIO_STOP_STATE )
                    {
                        return true;
                    }
                    if( playerFuncMutex.try_lock() )
                    {
                        playerFuncWorker = std::async( std::launch::async,[this, curAddress]
                        {
    //                        logger->Message(LOG_DEBUG, __LINE__, __func__, "+++ playerFuncWorker");
                            try
                            {
                                std::cerr << "Play sector = " << ((uint64_t)curAddress * 64)/512 << std::endl;
                                this->recorderStatusCtrl->setPosPlayAudio((uint64_t)curAddress * 64);
                            }
                            catch (std::system_error &except)
                            {
                                this->logger->Message(LOG_ERR, __LINE__,
                                                      __func__ , "Unable to set audio pos %s", except.what());
                            }
                            playerFuncMutex.unlock();
                        });
                        playerFuncWorker.wait_for(std::chrono::seconds(0));
                    }
                    else
                    {
                        logger->Message(LOG_ERR, __LINE__, __func__, "THREAD Player send command STILL BUSY!! ");
                    }

                }
                else
                {
                    this->logger->Message(LOG_ERR, __LINE__, __func__ , "Error while get pkd address");
                    this->iUserManipulators->showMessage("Ошибка выбора адреса пкд");
                }
            }
            else
            {
                this->logger->Message(LOG_ERR, __LINE__, __func__ , "Out of range session time");
            }
        }
        else
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__ , "Index out of range selected sessions");
        }
        return true;
}


//**IPlayBackProcess


void PlayBackProcess::slotSeekPos(int newIndex, size_t playPos)
{
    seekToPos(newIndex, playPos);
}

//ProcessWidget
void PlayBackProcess::slotOnProcessWidgetSwipeSelected(std::string name, std::shared_ptr<IProcessWidget> object)
{
    Q_UNUSED(object);
    if( name == PLAYBACK_PROCES_WIDGET_NAME )
        isProcessWidgetVisible = true;
}

void PlayBackProcess::slotOnProcessWidgetSwipeDeSelected(std::string name, std::shared_ptr<IProcessWidget> object)
{
    Q_UNUSED(object);
    if( name == PLAYBACK_PROCES_WIDGET_NAME )
        isProcessWidgetVisible = false;
}

//**ProcessWidget
