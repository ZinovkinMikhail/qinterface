//
// Created by ivan on 10.11.2020.
//

#include "databasecreator.h"

#include <QFile>

DatabaseCreator::DatabaseCreator(std::shared_ptr<rosslog::Log> log) :
    logger(log)
{

}

sqlite3* DatabaseCreator::createDatabase(bool dropTables, std::string dbPath)
{
    int rc;
    const char * tail = 0;

    this->dbPath = dbPath;
    rc = sqlite3_open_v2(dbPath.c_str(),  &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE ,  nullptr);
    sqlite3_config(SQLITE_CONFIG_MULTITHREAD);
//   sqlite3_config(SQLITE_OPEN_FULLMUTEX);


    int res;
    if( rc )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while opening database %s",
                              sqlite3_errmsg(db));
        return nullptr;
    }
    sqlite3_exec(db, "PRAGMA journal_mode = MEMORY", NULL, NULL, nullptr);
    sqlite3_exec(db, "PRAGMA journal_mode = MEMORY", NULL, NULL, nullptr);

    std::string request = "DROP TABLE IF EXISTS yantra_sessions";

    if( dropTables )
    {
        if( !runRequest(request) )
        {
            return nullptr;
        }
    }

    request = "CREATE TABLE IF NOT EXISTS yantra_sessions ( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"
              ", start_time INTEGER, end_time INTEGER, channel INTEGER, byte_shift INTEGER, type INTEGER, startAddress INTEGER, packets INTEGER)";

    if( !runRequest(request) )
    {
        return nullptr;
    }

    request = "DROP TABLE IF EXISTS yantra_pkd";

    if( dropTables )
    {
        if (!runRequest(request))
        {
            return nullptr;
        }
    }

    request =  "CREATE TABLE IF NOT EXISTS yantra_pkd (  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
               " session_id INTEGER, peak INTEGER, time INTEGER, byte_shift INTEGER)";

    if( !runRequest(request) )
    {
        return nullptr;
    }

    request =  "CREATE TABLE IF NOT EXISTS yantra_data ( bytesUsed INTEGER, lastPKDPos INTEGER, ring BOOL, dwTime INTEGER,"
               " ring_count INTEGER, data_shift INTEGER)";

    if( !runRequest(request) )
    {
        return nullptr;
    }

    return db;
}

bool DatabaseCreator::runRequest(const std::string &request)
{
    sqlite3_stmt * stmt;
    const char * tail = 0;
    int rc;

    if( this->db == nullptr )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error database is not inited %s", sqlite3_errmsg(db));
        return false;
    }

    rc = sqlite3_prepare_v2(this->db,  request.c_str(), -1, &stmt, &tail);

    if( rc != SQLITE_OK )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while prepare request database %s",
                              sqlite3_errmsg(db));
        return false;
    }

    rc = sqlite3_step(stmt);

    if( rc != SQLITE_DONE )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__ , "Error while step request database %s",
                              sqlite3_errmsg(db));
        return false;
    }
    sqlite3_reset(stmt);
    return true;
}

bool DatabaseCreator::startTransaction()
{
    int rc;
    char *zErrMsg = 0;
    rc = sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &zErrMsg);

    if( rc != SQLITE_DONE )
        return false;
    return true;
}

bool DatabaseCreator::endTransaction()
{
    int rc;
    char *zErrMsg = 0;
    rc = sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &zErrMsg);
    //sqlite3_exec(db, "CREATE  INDEX 'TTC_start_time_Index' ON 'yantra_sessions' ('start_time, end_time, id, channel')", NULL, NULL, nullptr);
    //sqlite3_exec(db, "CREATE  INDEX 'TTC_peak_Index' ON 'yantra_pkd' ('peak, id, start_time')", NULL, NULL, nullptr);

    if( rc != SQLITE_DONE )
        return false;
    return true;
}

bool DatabaseCreator::clearDatabase()
{
    if(db != nullptr )
    {
        sqlite3_close(db);
    }
    QFile file(QString::fromStdString(dbPath));

    if( file.exists() )
    {
        return file.remove();
    }
    else
        return true;

    return false;
}

bool DatabaseCreator::createPKDIndex()
{
    std::string request = "CREATE INDEX IF NOT EXISTS index_name ON yantra_pkd (session_id);";

    if (!runRequest(request))
    {
        return false;
    }
    return true;
}

bool DatabaseCreator::removePKDIndex()
{
    std::string request = "DROP INDEX IF EXISTS index_name;";

    if (!runRequest(request))
    {
        return false;
    }
    return true;
}


