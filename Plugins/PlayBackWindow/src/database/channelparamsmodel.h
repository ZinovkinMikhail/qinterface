//
// Created by ivan on 09.11.2020.
//

#ifndef CM_INTERFACE_CHANNELPARAMSMODEL_H
#define CM_INTERFACE_CHANNELPARAMSMODEL_H

#include <vector>
#include <cstdint>

#define MAX_PKD_PACK 500

typedef enum
{
    MONO_1,
    MONO_2,
    MONO_3,
    MONO_4,
    STEREO_1,
    STEREO_2
}ChannelType;

typedef enum
{
    STEREO,
    MONO
} PKDType;

typedef struct
{
    uint64_t mid;
    int bytesShift;
    uint64_t endTime;
    uint32_t peak;
    int sessionID;
} ChannelPkd;

typedef struct
{
    uint64_t endTime;
    uint16_t peak;
    bool id;
} ChannelPkdLite;

typedef struct
{
    uint64_t startTime;
    uint64_t endTime;
    bool isWrited;
    int packetsCount;
    int channel;
    int id;
    uint16_t pkdL;
    uint16_t pkdR;
    uint64_t midPkdL;
    uint64_t midPkdR;
    int pkdCount;
    uint64_t sessionByteShift;
    std::vector<ChannelPkd> pkds;
    bool isSend;
    int type;
    int pkdShift;
    uint64_t startAddress;
    bool isOnRecordSessionChecked;
    bool isRecordAlready;
    uint64_t alreadyRecordByteShift;
} ChannelParams;

typedef struct
{
    int startPoint;
    int endPoint;
    int pointCount;
    int sessionID;
    int sessionStartPoint;
    int sessionEndPoint;
} GraphSegment;

typedef struct
{
    bool isActual;
    int segmentsCount;
    int totalPoints;
    std::vector<GraphSegment> segments;
    int startPoint;
    int endPoint;
    std::vector<ChannelPkdLite> pkds;
} GraphPage;



#endif //CM_INTERFACE_CHANNELPARAMSMODEL_H
