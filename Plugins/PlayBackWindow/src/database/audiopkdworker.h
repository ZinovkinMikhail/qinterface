//
// Created by ivan on 09.11.2020.
//

#ifndef CM_INTERFACE_AUDIOPKDWORKER_H
#define CM_INTERFACE_AUDIOPKDWORKER_H

#include <memory>
#include <vector>
#include <fstream>
#include <thread>
#include <ios>
#include <sys/stat.h>

#include <rosslog/log.h>

#include "databasepkdworker.h"
#include "filepkdworker.h"
#include "databasecreator.h"
#include "ifilepkdworker.h"
#include "iaudiopkdworker.h"

//#define DEBUG_NO_ADAPTER
#define DATABASE_PATH "/tmp/yantra.db"
#define PKD_FILE_PATH "/dev/mmcblk1"
//#define ISRING
//#define RCOUNTER 0
//#define PKD_FILE_PATH "/home/yaroslav/proj/cm-interface_PC/DEBUG_DATA/data8/flash.cff"

#define SETUINT16(lob, hib)      (((uint16_t)(lob) & 0xffu) | (((uint16_t)(hib) & 0xffu) << 8))
#define SETUINT32(low, hiw)     (((uint32_t)(low) & 0xffffu) | (((uint32_t)(hiw) & 0xfffful) << 16))

typedef struct
{
    bool isRing;
    uint64_t currentDWTime;
    int rCounter;
    int lastRCounter;
    bool databaseState;
    uint64_t bytesUsed;
    uint64_t pkdBytePos;
    uint64_t flashCapacity;
    bool lastIsRing;
    uint64_t lastDWTime;
    uint64_t lastPkdPos;
    uint64_t lastRecordBytesUsed;
    uint32_t lastDataShift;
} ParseCommonInfo;

typedef struct
{
    uint32_t pkdPos;
    bool dataBaseState;
    ParseType parseType;
} ParseResultInfo;


class AudioPKDWorker : public IFIlePKDWorker, public std::enable_shared_from_this<AudioPKDWorker>
{
public:
    explicit AudioPKDWorker(std::shared_ptr<rosslog::Log> log,
                            IAudioPKDWorker &audioWorkerInterface);

    ~AudioPKDWorker();

    bool startPKDParse(uint64_t bytesUsed, uint64_t flashCapacity);
    bool startPKDParseTest();
    bool getSessions(std::vector<ChannelParams> &sessions);
    bool getSessionsByDate(const uint64_t startDate, const uint64_t endDate, std::vector<ChannelParams> &sessions);
    bool getSessionsByChannelType(uint8_t channelType, uint8_t channel, std::vector<ChannelParams> &sessions);
    bool getPKDBySession(int channel, std::vector<ChannelPkd> &pkds);
    bool getPKDSessionSize(const int channel, size_t &size);
    bool checkDatabaseExists();
    bool getLastRecordedInfo(uint64_t &lastBytesRecorded);
    bool getPKDByGroup(int sessionID, int limit,
                             int offset, int group, std::vector<ChannelPkdLite> &pkds);
    bool getPKDByGroup(int sessionID, int limit,
                       int offset, int group, std::map< int , GraphZoom> &zoomPage);
    bool getPKDAddress(int sessionID, uint64_t time, int channel, uint32_t &address);
    bool removeDatabase();
    void stopPKDParse();

protected:
    void fileLoadingFinished(int index, bool isError, const std::string &msg) override;
    void fileParseProcessState(int procent) override;

private:
    void initDatabaseCreator();
    bool checkRecordForRing(int &rCounter);
    uint32_t getCurDwTime();

    bool workOnParseType1(const ParseCommonInfo &parseCommonInfo, ParseResultInfo &resultInfo);
    bool workOnParseType2(const ParseCommonInfo &parseCommonInfo, ParseResultInfo &resultInfo);
    bool workOnParseType3(const ParseCommonInfo &parseCommonInfo, ParseResultInfo &resultInfo);
    bool workOnParseCheckTimeAndResize(const ParseCommonInfo &parseCommonInfo, ParseResultInfo &parseResultInfo, std::shared_ptr<FilePKDWorker> filePKDWorker);


    bool clearDatabase();

    std::thread mainThread;
    QFile *fileInput;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<DatabaseCreator> databaseCreator;
    std::vector<std::shared_ptr<FilePKDWorker>> fileParsers;
    std::vector<std::shared_ptr<DatabasePKDWorker>> connections;
    std::vector<std::shared_ptr<DatabaseCreator>> dbCreators;
    std::shared_ptr<DatabasePKDWorker> pkdDb;
    IAudioPKDWorker &audioWorkerInterface;

};


#endif //CM_INTERFACE_AUDIOPKDWORKER_H
