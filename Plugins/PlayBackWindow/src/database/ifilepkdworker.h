//
// Created by ivan on 10.11.2020.
//

#ifndef CM_INTERFACE_IFILEPKDWORKER_H
#define CM_INTERFACE_IFILEPKDWORKER_H

#include <string>

class IFIlePKDWorker
{
public:
    virtual void fileLoadingFinished(int index, bool isError, const std::string &msg) = 0;
    virtual void fileParseProcessState(int procent) = 0;
};

#endif //CM_INTERFACE_IFILEPKDWORKER_H
