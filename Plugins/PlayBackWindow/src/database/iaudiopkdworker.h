//
// Created by ivan on 07.12.2020.
//

#ifndef CM_INTERFACE_IAUDIOPKDWORKER_H
#define CM_INTERFACE_IAUDIOPKDWORKER_H

#include <string>

class IAudioPKDWorker
{
public:
    virtual void pkdFileParseFinished(bool isError, const std::string &msg) = 0;
    virtual void pkdFileParseProgress(int procent) = 0;
};

#endif //CM_INTERFACE_IAUDIOPKDWORKER_H
