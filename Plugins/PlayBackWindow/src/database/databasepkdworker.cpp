//
// Created by ivan on 09.11.2020.
//

#include "databasepkdworker.h"

DatabasePKDWorker::DatabasePKDWorker(std::shared_ptr<rosslog::Log> log, sqlite3 *db) :
    logger(log),
    db(db)
{

}

DatabasePKDWorker::~DatabasePKDWorker()
{
    std::cerr << "Remove database pkdWorker" << std::endl;
}

bool DatabasePKDWorker::initDatabase(bool dropTables)
{
    int rc;
    const char * tail = 0;
    int res;

    std::string insert = "INSERT into yantra_sessions (start_time, end_time, channel, type, startAddress, packets) VALUES(?,?,?,?,?,?);";
    res = sqlite3_prepare_v2(db,  insert.c_str(), -1, &insertStmt, &tail);

    std::string update = "UPDATE yantra_sessions SET end_time = ?, byte_shift = ?, packets = ? WHERE start_time = ? AND channel = ?;";
    res = sqlite3_prepare_v2(db,  update.c_str(), -1, &updateStmt, &tail);

    std::string insertPKD = "INSERT into yantra_pkd (session_id, peak, time, byte_shift) VALUES(?,?,?,?);";
    res = sqlite3_prepare_v2(db,  insertPKD.c_str(), -1, &insertPKDStmt, &tail);

    std::string select = "SELECT start_time, end_time from yantra_sessions WHERE id = ?";
    //res = sqlite3_prepare_v2(db,  select.c_str(), -1, &selectStmt, &tail);

    std::string selectSessionsStr = "SELECT * from yantra_sessions;";
    res = sqlite3_prepare_v2(db,  selectSessionsStr.c_str(), -1, &selectSessionsStmt, &tail);

    std::string selectPKDBySesStr = "SELECT * from yantra_pkd WHERE session_id = ? ORDER BY id ASC;";
    res = sqlite3_prepare_v2(db,  selectPKDBySesStr.c_str(), -1, &selectPKDBySesStmt, &tail);

    std::string insertYantraData = "INSERT into yantra_data (bytesUsed, lastPKDPos, ring, dwTime, ring_count, data_shift) VALUES(?,?,?,?,?,?);";
    res = sqlite3_prepare_v2(db,  insertYantraData.c_str(), -1, &setYantraDataStmt, &tail);

    std::string updateYantraData = "UPDATE yantra_data SET bytesUsed = ?, lastPKDPos = ?, ring = ?, dwTime = ?, ring_count = ?, data_shift = ?;";
    res = sqlite3_prepare_v2(db,  updateYantraData.c_str(), -1, &updateYantraDataStmt, &tail);

    std::string getYantraData = "SELECT bytesUsed, lastPKDPos, ring, dwTime, ring_count, data_shift from yantra_data;";
    res = sqlite3_prepare_v2(db,  getYantraData.c_str(), -1, &getYantraDataStmt, &tail);

    std::string selectSessionsForRemoveStr = "SELECT id from yantra_sessions WHERE byte_shift >= ? AND byte_shift <= ?;";
    res = sqlite3_prepare_v2(db,  selectSessionsForRemoveStr.c_str(), -1, &selectSessionsForRemStmt, &tail);

    std::string removeSessionByIDStr = "DELETE from yantra_sessions WHERE id = ?;";
    res = sqlite3_prepare_v2(db,  removeSessionByIDStr.c_str(), -1, &removeSessionByIDStmt, &tail);

    std::string request = "DELETE from yantra_pkd WHERE session_id = ?;";
    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &removePKDSByIDStmt, &tail);

    request = "SELECT count(*) from yantra_pkd where session_id = ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectSessionPKDSize, &tail);
    if( res != SQLITE_OK )
            abort();

    //request = "SELECT round(id/? + 0.49, 0) as grp, avg(peak), count(*) FROM yantra_pkd WHERE session_id = ? group by round(time/? + 0.49, 0) LIMIT ? OFFSET ?";
    //request = "SELECT time, peak FROM yantra_pkd WHERE session_id = ? LIMIT ? OFFSET ?";
    request = "SELECT time, peak FROM yantra_pkd WHERE session_id = ? ORDER BY ID ASC";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectPKDByGroupStmt, &tail);
    if( res != SQLITE_OK )
        abort();

    request = "SELECT * from yantra_pkd where session_id = ? AND byte_shift >= ? AND byte_shift <= ? LIMIT ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectPKDStmt, &tail);
    if( res != SQLITE_OK )
        abort();


    request = "SELECT byte_shift from yantra_pkd where session_id = ? AND time = ? LIMIT 1;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectPKDAddress, &tail);
    if( res != SQLITE_OK )
        abort();

    request = "SELECT * from yantra_sessions where start_time >= ? AND end_time <= ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectSessionsByDateStmt, &tail);
    if( res != SQLITE_OK )
        abort();

    request = "SELECT * from yantra_sessions where start_time >= ? AND end_time <= ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectSessionsByDateStmt, &tail);
    if( res != SQLITE_OK )
        abort();

    request = "SELECT * from yantra_sessions where channel = ? AND type = ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectSessionsByChannelTypeStmt, &tail);
    if( res != SQLITE_OK )
        abort();


    request = "SELECT * from yantra_sessions where start_time <= ? AND end_time >= ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectSessionByTime, &tail);
    if( res != SQLITE_OK )
        abort();


    request = "SELECT * from yantra_sessions where startAddress <= ? AND byte_shift > ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectSessionWithReused, &tail);
    if( res != SQLITE_OK )
        abort();

    request = "SELECT * from yantra_sessions where startAddress < ? and byte_shift > ? and byte_shift <= ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectSessionReused, &tail);
    if( res != SQLITE_OK )
        abort();


    request = "UPDATE yantra_sessions SET startAddress = ?, start_time = ? WHERE id = ? AND channel = ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &sessionFullUpdate, &tail);
    if( res != SQLITE_OK )
        abort();


    request = "DELETE from yantra_pkd WHERE session_id = ? AND byte_shift < ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &removePkdsLowerByteShift, &tail);
    if( res != SQLITE_OK )
        abort();


    request = "SELECT * from yantra_sessions where channel = ? AND end_time >= ? AND end_time <= ?;";

    res = sqlite3_prepare_v2(db,  request.c_str(), -1, &selectSessionByEndTime, &tail);
    if( res != SQLITE_OK )
        abort();



    return true;
}



bool DatabasePKDWorker::startTransaction()
{
    int rc;
    char *zErrMsg = 0;
    rc = sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &zErrMsg);

    if( rc != SQLITE_DONE )
        return false;
    return true;
}

bool DatabasePKDWorker::endTransaction()
{
    int rc;
    char *zErrMsg = 0;
    rc = sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &zErrMsg);

    if( rc != SQLITE_DONE )
        return false;
    return true;
}

int DatabasePKDWorker::sendInsertCommand(uint64_t startTime, uint64_t endTime, int channel, int type,
                                         uint64_t startAddress, ChannelParams &params, uint32_t packets)
{
    int res;
    int id;

    res = sqlite3_bind_int64(insertStmt, 1 ,startTime);
    res = sqlite3_bind_int64(insertStmt, 2 , endTime);
    res = sqlite3_bind_int64(insertStmt, 3 , channel);
    res = sqlite3_bind_int64(insertStmt, 4 , type);
    res = sqlite3_bind_int64(insertStmt, 5 , startAddress);
    res = sqlite3_bind_int64(insertStmt, 6 , packets);

    res = sqlite3_step(insertStmt);

    sqlite3_clear_bindings(insertStmt);
    sqlite3_reset(insertStmt);

    id = sqlite3_last_insert_rowid(db);

    return id;
}

int DatabasePKDWorker::sendUpdateCommand(int id, uint64_t startTime, uint64_t endTime, int channel, int byteShift, uint32_t packets)
{
    int res;
    const char * tail = 0;

    sqlite3_bind_int64(updateStmt, 1 , endTime);
    sqlite3_bind_int64(updateStmt, 2 , byteShift);
    sqlite3_bind_int64(updateStmt, 3 , packets);
    sqlite3_bind_int64(updateStmt, 4 , startTime);
    sqlite3_bind_int64(updateStmt, 5 , channel);


    res = sqlite3_step(updateStmt);

    sqlite3_clear_bindings(updateStmt);
    sqlite3_reset(updateStmt);
    return 0;
}

int DatabasePKDWorker::sendInsertPKDCommand(int sessionID, uint64_t pkd, uint64_t time, int byteShift)
{
    int res;
    const char * tail = 0;

    sqlite3_bind_int64(insertPKDStmt, 1 , sessionID);
    sqlite3_bind_int64(insertPKDStmt, 2 , pkd);
    sqlite3_bind_int64(insertPKDStmt, 3 , time);
    sqlite3_bind_int64(insertPKDStmt, 4 , byteShift);
    res = sqlite3_step(insertPKDStmt);

    sqlite3_clear_bindings(insertPKDStmt);
    sqlite3_reset(insertPKDStmt);
    return 0;
}

int DatabasePKDWorker::sendSelectSessionCommand(int sessionID, ChannelParams &params)
{
    int res;
    const char * tail = 0;

    sqlite3_bind_int64(selectStmt, 1 , sessionID);

    res = sqlite3_step(selectStmt);

    params.startTime   = sqlite3_column_int( selectStmt, 0);
    params.endTime   = sqlite3_column_int( selectStmt, 1);


    sqlite3_reset(selectStmt);
    return 0;
}

bool DatabasePKDWorker::finalize()
{
    sqlite3_finalize( insertPKDStmt );
    sqlite3_finalize( updateStmt );
    sqlite3_finalize( insertStmt );
    return false;
}

int DatabasePKDWorker::sendSelectSessions(std::vector<ChannelParams> &sessions)
{
    int res;
    const char * tail = 0;
    int count = 0;

    do
    {
        res = sqlite3_step(selectSessionsStmt);
        switch( res )
        {
            /** No more data */
            case SQLITE_DONE:
                break;
                /** New data */
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectSessionsStmt );

                if(size == 8 ) // should always be one
                {
                    ChannelParams session = {};
//                    memset(&session, 0, sizeof(session));
                    session.id = sqlite3_column_int( selectSessionsStmt, 0);
                    session.startTime = sqlite3_column_int( selectSessionsStmt, 1);
                    session.endTime = sqlite3_column_int( selectSessionsStmt, 2);
                    session.channel = sqlite3_column_int( selectSessionsStmt, 3);
                    session.sessionByteShift = sqlite3_column_int( selectSessionsStmt, 4);
                    session.type = sqlite3_column_int( selectSessionsStmt, 5);
                    session.startAddress = sqlite3_column_int( selectSessionsStmt, 6);
                    session.packetsCount = sqlite3_column_int( selectSessionsStmt, 7);
                    sessions.push_back(session);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );


    sqlite3_reset(selectSessionsStmt);
    return 0;
}

int DatabasePKDWorker::sendSelectPKDBySession(int sessionID, std::vector<ChannelPkd> &pkds)
{
    int res;
    const char * tail = 0;
    do
    {
        sqlite3_bind_int64(selectPKDBySesStmt, 1 , sessionID);
        res = sqlite3_step(selectPKDBySesStmt);
        switch( res )
        {
            case SQLITE_DONE:
                break;
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectPKDBySesStmt );

                if(size == 5 ) // should always be one
                {
                    ChannelPkd pkd;
                    memset(&pkd, 0, sizeof(pkd));
                    pkd.sessionID = sqlite3_column_int( selectPKDBySesStmt, 1);
                    pkd.peak = sqlite3_column_int( selectPKDBySesStmt, 2);
                    pkd.endTime = sqlite3_column_int( selectPKDBySesStmt, 3);
                    pkd.bytesShift = sqlite3_column_int( selectPKDBySesStmt, 4);
                    pkds.push_back(pkd);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );


    sqlite3_clear_bindings(selectPKDBySesStmt);
    sqlite3_reset(selectPKDBySesStmt);

    return 0;
}

int DatabasePKDWorker::sendYantraData(uint64_t bytesUsed, uint64_t pkdPos, bool isRing, uint64_t dwTime, int rCounter, uint32_t dataShift)
{
    int res;

    sqlite3_bind_int64(setYantraDataStmt , 1 , bytesUsed);
    sqlite3_bind_int64(setYantraDataStmt, 2 , pkdPos);
    sqlite3_bind_int64(setYantraDataStmt, 3 , isRing);
    sqlite3_bind_int64(setYantraDataStmt, 4 , dwTime);
    sqlite3_bind_int64(setYantraDataStmt, 5 , rCounter);
    sqlite3_bind_int64(setYantraDataStmt, 6 , dataShift);

    res = sqlite3_step(setYantraDataStmt);

    sqlite3_clear_bindings(setYantraDataStmt);
    sqlite3_reset(setYantraDataStmt);

    if( res == SQLITE_ERROR )
        return -1;

    return 0;
}

int DatabasePKDWorker::getYantraData(uint64_t &bytesUsed, uint64_t &pkdPos, bool &isRing, uint64_t &dwTime, int &rCounter, uint32_t &dataShift)
{
    int res;
    do
    {
        res = sqlite3_step(getYantraDataStmt);

        switch( res )
        {
            case SQLITE_DONE:
                break;
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( getYantraDataStmt );

                if(size == 6 ) // should always be one
                {
                    bytesUsed = sqlite3_column_int64( getYantraDataStmt, 0);
                    pkdPos = sqlite3_column_int64( getYantraDataStmt, 1);
                    isRing = sqlite3_column_int( getYantraDataStmt, 2);
                    dwTime = sqlite3_column_int64( getYantraDataStmt, 3);
                    rCounter = sqlite3_column_int64( getYantraDataStmt, 4);
                    dataShift = sqlite3_column_int64( getYantraDataStmt, 5);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );


    sqlite3_clear_bindings(getYantraDataStmt);
    sqlite3_reset(getYantraDataStmt);

    if( res == SQLITE_ERROR )
        return -1;

    return 0;
}

int DatabasePKDWorker::updateYantraData(uint64_t bytesUsed, uint64_t pkdPos, bool isRing, uint64_t dwTime, int rCounter, uint32_t dataShift)
{
    int res;

    sqlite3_bind_int64(updateYantraDataStmt, 1 , bytesUsed);
    sqlite3_bind_int64(updateYantraDataStmt, 2 , pkdPos);
    sqlite3_bind_int64(updateYantraDataStmt, 3 , isRing);
    sqlite3_bind_int64(updateYantraDataStmt, 4 , dwTime);
    sqlite3_bind_int64(updateYantraDataStmt, 5 , rCounter);
    sqlite3_bind_int64(updateYantraDataStmt, 6 , dataShift);



    res = sqlite3_step(updateYantraDataStmt);

    sqlite3_clear_bindings(updateYantraDataStmt);
    sqlite3_reset(updateYantraDataStmt);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

int DatabasePKDWorker::clearPKDData(uint64_t from, uint64_t to)
{
    return 0;
}

int DatabasePKDWorker::selectSessionsForRemove(std::vector<int> &sessions, uint64_t startPos, uint64_t endPos)
{
    int res;
    const char * tail = 0;
    do
    {
        sqlite3_bind_int64(selectSessionsForRemStmt, 1 , startPos);
        sqlite3_bind_int64(selectSessionsForRemStmt, 2 , endPos);
        res = sqlite3_step(selectSessionsForRemStmt);
        switch( res )
        {
            case SQLITE_DONE:
                break;
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectSessionsForRemStmt );

                if(size == 1 ) // should always be one
                {
                    int id;
                    id = sqlite3_column_int( selectSessionsForRemStmt, 0);
                    sessions.push_back(id);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );


    sqlite3_clear_bindings(selectSessionsForRemStmt);
    sqlite3_reset(selectSessionsForRemStmt);

    if( res == SQLITE_ERROR )
        return -1;

    return 0;
}

int DatabasePKDWorker::removeSessionAndPkds(int sessionID)
{
    int res;

    sqlite3_bind_int64(removePKDSByIDStmt, 1 , sessionID);

    res = sqlite3_step(removePKDSByIDStmt);

    sqlite3_clear_bindings(removePKDSByIDStmt);
    sqlite3_reset(removePKDSByIDStmt);

    if( res == SQLITE_ERROR )
        return -1;

    sqlite3_bind_int64(removeSessionByIDStmt, 1 , sessionID);

    res = sqlite3_step(removeSessionByIDStmt);

    sqlite3_clear_bindings(removeSessionByIDStmt);
    sqlite3_reset(removeSessionByIDStmt);

    if( res == SQLITE_ERROR )
        return -1;


    return 0;
}

int DatabasePKDWorker::sendSelectSessionPKDSize(size_t &size, const int sessionID)
{
    int res;
    const char * tail = 0;
    do
    {
        sqlite3_bind_int64(selectSessionPKDSize, 1 , sessionID);

        res = sqlite3_step(selectSessionPKDSize);
        switch( res )
        {
            case SQLITE_DONE:
                    break;
            case SQLITE_ROW:
            {
                uint16_t colSize = sqlite3_column_count( selectSessionPKDSize );

                if(colSize == 1 )
                {
                        size = sqlite3_column_int( selectSessionPKDSize, 0);
                }
                break;
            }
            default:
                    break;
        }
    } while( res == SQLITE_ROW );


    sqlite3_clear_bindings(selectSessionPKDSize);
    sqlite3_reset(selectSessionPKDSize);

    if( res == SQLITE_ERROR )
            return -1;

    return 0;
}

int DatabasePKDWorker::sendSelectPKDByGroup(int sessionID, int limit,
                                            int offset, int group,
                                            std::vector<ChannelPkdLite> &pkds)
{
    int res;
    const char * tail = 0;

    int averageCount  = 0;
    uint64_t averageSum = 0;

    do
    {
//        sqlite3_bind_int64(selectPKDByGroupStmt, 1 , group);
        sqlite3_bind_int64(selectPKDByGroupStmt, 1 , sessionID);
//        sqlite3_bind_int64(selectPKDByGroupStmt, 3 , group);
//        sqlite3_bind_int64(selectPKDByGroupStmt, 2 , limit);
//        sqlite3_bind_int64(selectPKDByGroupStmt, 3 , offset);

        res = sqlite3_step(selectPKDByGroupStmt);
        switch( res )
        {
            case SQLITE_DONE:
                break;
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectPKDByGroupStmt );

                if(size == 2 ) // should always be one
                {
                    ChannelPkdLite pkd;
                    memset(&pkd, 0, sizeof(pkd));
                    int peak = sqlite3_column_int( selectPKDByGroupStmt, 1);
                    uint64_t time = sqlite3_column_int( selectPKDByGroupStmt, 0);
                    if( averageCount == group  )
                    {
                        pkd.peak = averageSum / averageCount;
                        pkd.endTime = time;
                        averageCount = 0;
                        averageSum = 0;
                        pkds.push_back(pkd);
                    }
                    averageSum += peak;
                    averageCount++;
                    //pkd.sessionID = sqlite3_column_int( selectPKDByGroupStmt, 1);

                    //pkd.endTime = sqlite3_column_int( selectPKDByGroupStmt, 3);
                    //pkd.bytesShift = sqlite3_column_int( selectPKDByGroupStmt, 4);

                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );


    sqlite3_clear_bindings(selectPKDByGroupStmt);
    sqlite3_reset(selectPKDByGroupStmt);

    if( averageCount > 0 )
    {
        ChannelPkdLite pkd;
        memset(&pkd, 0, sizeof(pkd));
        pkd.peak = averageSum / averageCount;
        pkds.push_back(pkd);
    }

    if( res == SQLITE_ERROR )
        return -1;

    return 0;
}

int DatabasePKDWorker::sendSelectPKDByteShift(int sessionID, int limit,
                                     uint32_t startAddress, uint32_t endAddress, uint64_t &time)
{
    int res;
    const char * tail = 0;
    do
    {
        sqlite3_bind_int64(selectPKDStmt, 1 , sessionID);
        sqlite3_bind_int64(selectPKDStmt, 2 , startAddress);
        sqlite3_bind_int64(selectPKDStmt, 3 , endAddress);
        sqlite3_bind_int64(selectPKDStmt, 4 , limit);
        res = sqlite3_step(selectPKDStmt);
        switch( res )
        {
            case SQLITE_DONE:
                break;
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectPKDStmt );

                if(size == 1 ) // should always be one
                {
                    time = sqlite3_column_int( selectPKDStmt, 3);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );


    sqlite3_clear_bindings(selectPKDStmt);
    sqlite3_reset(selectPKDStmt);

    if( res == SQLITE_ERROR )
        return -1;

    return 0;
}

int DatabasePKDWorker::sendSelectPKDByGroup(int sessionID, int limit, int offset, int group,
                                            std::map<int, GraphZoom> &zoomPages)
{
    int res;
    bool isFirst = true;
    do
    {
         //group - теперь не используется!!!!!!!!!!!!!!!!!!!!!
        sqlite3_bind_int64(selectPKDByGroupStmt, 1 , sessionID);

        res = sqlite3_step(selectPKDByGroupStmt);
        switch( res )
        {
            case SQLITE_DONE:
                break;
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectPKDByGroupStmt );
                if(size == 2 ) // should always be one
                {
                    int peak = sqlite3_column_int( selectPKDByGroupStmt, 1);
                    uint64_t time = sqlite3_column_int( selectPKDByGroupStmt, 0);


                    uint16_t peakSel = 0;

                    auto peakRight = (uint16_t)(peak & 0xffff);
                    auto peakLeft = (uint16_t)(peak >> 16);


                    if( peakRight > peakLeft )
                        peakSel = peakRight;
                    else
                        peakSel = peakLeft;

                    ChannelPkdLite lite;
                    lite.id = false;
                    lite.peak = (uint16_t)peakSel;
                    lite.endTime = time;

                    if( isFirst )
                    {
//                        std::cerr << "Last lite time split " << time << std::endl;
                        lite.id = true;
                        isFirst = false;
                    }
                    zoomPages[0].peak.push_back(lite);
                    zoomPages[group].insertPeak(peakSel, time, lite.id);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );

    zoomPages[group].insertPeak(0, 0, false);
    sqlite3_clear_bindings(selectPKDByGroupStmt);
    sqlite3_reset(selectPKDByGroupStmt);

    if( res == SQLITE_ERROR )
        return -1;

    return 0;
}

int DatabasePKDWorker::sendSelectPKDAddress(int sessionID, uint64_t second, int channel, uint32_t &address)
{
    int res;
    const char * tail = 0;
    do
    {
        sqlite3_bind_int64(selectPKDAddress, 1 , sessionID);
        sqlite3_bind_int64(selectPKDAddress, 2 , second);

        res = sqlite3_step(selectPKDAddress);
        switch( res )
        {
            case SQLITE_DONE:
                break;
            case SQLITE_ROW:
            {
                uint16_t colSize = sqlite3_column_count( selectPKDAddress );

                if(colSize == 1 )
                {
                    address = sqlite3_column_int( selectPKDAddress, 0);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );


    sqlite3_clear_bindings(selectPKDAddress);
    sqlite3_reset(selectPKDAddress);

    if( res == SQLITE_ERROR )
        return -1;

    return 0;
}

int DatabasePKDWorker::sendSelectSessionsByDate(const uint64_t startDate, const uint64_t endDate,
                                                std::vector<ChannelParams> &sessions)
{
    int res;

    sqlite3_bind_int64(selectSessionsByDateStmt, 1 , startDate);
    sqlite3_bind_int64(selectSessionsByDateStmt, 2 , endDate);

    do
    {
        res = sqlite3_step(selectSessionsByDateStmt);
        switch( res )
        {
            /** No more data */
            case SQLITE_DONE:
                break;
                /** New data */
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectSessionsByDateStmt );

                if(size == 8 ) // should always be one
                {
                    ChannelParams session = {};
//                    memset(&session, 0, sizeof(session));
                    session.id = sqlite3_column_int( selectSessionsByDateStmt, 0);
                    session.startTime = sqlite3_column_int( selectSessionsByDateStmt, 1);
                    session.endTime = sqlite3_column_int( selectSessionsByDateStmt, 2);
                    session.channel = sqlite3_column_int( selectSessionsByDateStmt, 3);
                    session.sessionByteShift = sqlite3_column_int( selectSessionsByDateStmt, 4);
                    session.type = sqlite3_column_int( selectSessionsByDateStmt, 5);
                    session.startAddress = sqlite3_column_int( selectSessionsByDateStmt, 6);
                    session.packetsCount = sqlite3_column_int( selectSessionsByDateStmt, 7);
                    sessions.push_back(session);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );

    sqlite3_reset(selectSessionsByDateStmt);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

int DatabasePKDWorker::sendSelectSessionsByChannelType(uint8_t channelType, uint8_t channel,
                                                       std::vector<ChannelParams> &sessions)
{
    int res;

    sqlite3_bind_int64(selectSessionsByChannelTypeStmt, 1 , channel);
    sqlite3_bind_int64(selectSessionsByChannelTypeStmt, 2 , channelType);

    do
    {
        res = sqlite3_step(selectSessionsByChannelTypeStmt);
        switch( res )
        {
            /** No more data */
            case SQLITE_DONE:
                break;
                /** New data */
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectSessionsByChannelTypeStmt );

                if(size == 8 ) // should always be one
                {
                    ChannelParams session = {};
//                    memset(&session, 0, sizeof(session));
                    session.id = sqlite3_column_int( selectSessionsByChannelTypeStmt, 0);
                    session.startTime = sqlite3_column_int( selectSessionsByChannelTypeStmt, 1);
                    session.endTime = sqlite3_column_int( selectSessionsByChannelTypeStmt, 2);
                    session.channel = sqlite3_column_int( selectSessionsByChannelTypeStmt, 3);
                    session.sessionByteShift = sqlite3_column_int( selectSessionsByChannelTypeStmt, 4);
                    session.type = sqlite3_column_int( selectSessionsByChannelTypeStmt, 5);
                    session.startAddress = sqlite3_column_int( selectSessionsByChannelTypeStmt, 6);
                    session.packetsCount = sqlite3_column_int( selectSessionsByChannelTypeStmt, 7);
                    sessions.push_back(session);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );

    sqlite3_reset(selectSessionsByChannelTypeStmt);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

int DatabasePKDWorker::sendSelectSessionByTime(uint64_t time, std::vector<ChannelParams> &sessions)
{
    int res;

    sqlite3_bind_int64(selectSessionByTime, 1 , time);
    sqlite3_bind_int64(selectSessionByTime, 2 , time);
    do
    {
        res = sqlite3_step(selectSessionByTime);
        switch( res )
        {
            /** No more data */
            case SQLITE_DONE:
                break;
                /** New data */
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectSessionByTime );

                if(size == 8 ) // should always be one
                {
                    ChannelParams session = {};
//                    memset(&session, 0, sizeof(session));
                    session.id = sqlite3_column_int( selectSessionByTime, 0);
                    session.startTime = sqlite3_column_int( selectSessionByTime, 1);
                    session.endTime = sqlite3_column_int( selectSessionByTime, 2);
                    session.channel = sqlite3_column_int( selectSessionByTime, 3);
                    session.sessionByteShift = sqlite3_column_int( selectSessionByTime, 4);
                    session.type = sqlite3_column_int( selectSessionByTime, 5);
                    session.startAddress = sqlite3_column_int( selectSessionByTime, 6);
                    session.packetsCount = sqlite3_column_int( selectSessionByTime, 7);
                    sessions.push_back(session);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );

    sqlite3_reset(selectSessionByTime);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

int DatabasePKDWorker::sendSelectSessionsPartOfReused(uint32_t bytesUsed, std::vector<ChannelParams> &sessions)
{
    int res;

    sqlite3_bind_int64(selectSessionWithReused, 1 , bytesUsed);
    sqlite3_bind_int64(selectSessionWithReused, 2 , bytesUsed);
    do
    {
        res = sqlite3_step(selectSessionWithReused);
        switch( res )
        {
            /** No more data */
            case SQLITE_DONE:
                break;
                /** New data */
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectSessionWithReused );

                if(size == 8 ) // should always be one
                {
                    ChannelParams session = {};
//                    memset(&session, 0, sizeof(session));
                    session.id = sqlite3_column_int( selectSessionWithReused, 0);
                    session.startTime = sqlite3_column_int( selectSessionWithReused, 1);
                    session.endTime = sqlite3_column_int( selectSessionWithReused, 2);
                    session.channel = sqlite3_column_int( selectSessionWithReused, 3);
                    session.sessionByteShift = sqlite3_column_int( selectSessionWithReused, 4);
                    session.type = sqlite3_column_int( selectSessionWithReused, 5);
                    session.startAddress = sqlite3_column_int( selectSessionWithReused, 6);
                    session.packetsCount = sqlite3_column_int( selectSessionWithReused, 7);
                    sessions.push_back(session);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );

    sqlite3_reset(selectSessionWithReused);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

int DatabasePKDWorker::sendSelectSessionsWasReused(uint32_t startAddress , uint32_t lastByteUsed, uint32_t bytesUsed, std::vector<ChannelParams> &sessions)
{
    int res;

    sqlite3_bind_int64(selectSessionReused, 1 , startAddress);
    sqlite3_bind_int64(selectSessionReused, 2 , lastByteUsed);
    sqlite3_bind_int64(selectSessionReused, 3 , bytesUsed);


    do
    {
        res = sqlite3_step(selectSessionReused);
        switch( res )
        {
            /** No more data */
            case SQLITE_DONE:
                break;
                /** New data */
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectSessionReused );

                if(size == 8 ) // should always be one
                {
                    ChannelParams session = {};
//                    memset(&session, 0, sizeof(session));
                    session.id = sqlite3_column_int( selectSessionReused, 0);
                    session.startTime = sqlite3_column_int( selectSessionReused, 1);
                    session.endTime = sqlite3_column_int( selectSessionReused, 2);
                    session.channel = sqlite3_column_int( selectSessionReused, 3);
                    session.sessionByteShift = sqlite3_column_int( selectSessionReused, 4);
                    session.type = sqlite3_column_int( selectSessionReused, 5);
                    session.startAddress = sqlite3_column_int( selectSessionReused, 6);
                    session.packetsCount = sqlite3_column_int( selectSessionReused, 7);
                    sessions.push_back(session);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );

    sqlite3_reset(selectSessionReused);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

//TODO update packetsUpdate
int DatabasePKDWorker::sendUpdateFullSessionCommand(int id, uint64_t startTime, uint64_t endTime, int channel,
                                                    int byteShift, uint32_t startAddress)
{
    int res;

    sqlite3_bind_int64(sessionFullUpdate, 1 , startAddress);
    sqlite3_bind_int64(sessionFullUpdate, 2 , startTime);
    sqlite3_bind_int64(sessionFullUpdate, 3 , id);
    sqlite3_bind_int64(sessionFullUpdate, 4 , channel);


    res = sqlite3_step(sessionFullUpdate);

    sqlite3_clear_bindings(sessionFullUpdate);
    sqlite3_reset(sessionFullUpdate);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

int DatabasePKDWorker::sendRemovePkdsLowerByteShifts(int id, int byteShift)
{
    int res;

    sqlite3_bind_int64(removePkdsLowerByteShift, 1 , id);
    sqlite3_bind_int64(removePkdsLowerByteShift, 2 , byteShift);

    res = sqlite3_step(removePkdsLowerByteShift);

    sqlite3_clear_bindings(removePkdsLowerByteShift);
    sqlite3_reset(removePkdsLowerByteShift);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

int DatabasePKDWorker::sendSelectSessionByEndTime(uint64_t time, int channel, std::vector<ChannelParams> &sessions)
{
    int res;

    sqlite3_bind_int64(selectSessionByEndTime, 1 , channel);
    sqlite3_bind_int64(selectSessionByEndTime, 2 , time);
    sqlite3_bind_int64(selectSessionByEndTime, 3 , time + 6);

    do
    {
        res = sqlite3_step(selectSessionByEndTime);
        switch( res )
        {
            /** No more data */
            case SQLITE_DONE:
                break;
                /** New data */
            case SQLITE_ROW:
            {
                uint16_t size = sqlite3_column_count( selectSessionByEndTime );

                if(size == 8 ) // should always be one
                {
                    ChannelParams session = {};
//                    memset(&session, 0, sizeof(session));
                    session.id = sqlite3_column_int( selectSessionByEndTime, 0);
                    session.startTime = sqlite3_column_int( selectSessionByEndTime, 1);
                    session.endTime = sqlite3_column_int( selectSessionByEndTime, 2);
                    session.channel = sqlite3_column_int( selectSessionByEndTime, 3);
                    session.sessionByteShift = sqlite3_column_int( selectSessionByEndTime, 4);
                    session.type = sqlite3_column_int( selectSessionByEndTime, 5);
                    session.startAddress = sqlite3_column_int( selectSessionByEndTime, 6);
                    session.packetsCount = sqlite3_column_int( selectSessionByEndTime, 7);
                    sessions.push_back(session);
                }
                break;
            }
            default:
                break;
        }
    } while( res == SQLITE_ROW );

    sqlite3_reset(selectSessionByEndTime);

    if( res == SQLITE_ERROR )
        return -1;
    return 0;
}

