//
// Created by ivan on 10.11.2020.
//

#ifndef CM_INTERFACE_DATABASECREATOR_H
#define CM_INTERFACE_DATABASECREATOR_H

#include <memory>
#include <sqlite3.h>

#include <rosslog/log.h>

class DatabaseCreator
{
public:
    explicit DatabaseCreator(std::shared_ptr<rosslog::Log> log);

    sqlite3* createDatabase(bool dropTables, std::string dbPath);
    bool createPKDIndex();
    bool removePKDIndex();
    bool startTransaction();
    bool endTransaction();
    bool clearDatabase();
private:
    bool runRequest(const std::string &request);

    std::shared_ptr<rosslog::Log> logger;
    sqlite3* db;
    std::string dbPath;

};


#endif //CM_INTERFACE_DATABASECREATOR_H
