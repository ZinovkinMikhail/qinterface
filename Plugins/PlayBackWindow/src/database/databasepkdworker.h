//
// Created by ivan on 09.11.2020.
//

#ifndef CM_INTERFACE_DATABASEPKDWORKER_H
#define CM_INTERFACE_DATABASEPKDWORKER_H

#include <map>
#include <mutex>
#include <iostream>
#include <cstring>
#include <memory>

#include <sqlite3.h>

#include <rosslog/log.h>

#include "channelparamsmodel.h"
#include "../graph/graphzoom.h"

#define DB_DEFAULT_PATH "/tmp/yantra.db"

class DatabasePKDWorker
{
public:
    DatabasePKDWorker(std::shared_ptr<rosslog::Log> log, sqlite3 *db);
    ~DatabasePKDWorker();

    bool initDatabase(bool dropTables);
    bool startTransaction();
    bool finalize();
    bool endTransaction();
    int sendInsertCommand(uint64_t startTime, uint64_t endTime, int channel,
                          int type, uint64_t startAddress, ChannelParams &params, uint32_t packets);
    int sendUpdateCommand(int id, uint64_t startTime, uint64_t endTime, int channel, int byteShift, uint32_t packets);
    int sendInsertPKDCommand(int sessionID, uint64_t pkd, uint64_t time, int byteShift);
    int sendSelectSessionCommand(int sessionID, ChannelParams &params);
    int sendSelectSessions(std::vector<ChannelParams> &sessions);
    int sendSelectSessionsByDate(const uint64_t startDate,
                                 const uint64_t endDate,
                                 std::vector<ChannelParams> &sessions);
    int sendSelectSessionsByChannelType(uint8_t channelType,
                                         uint8_t channel,
                                         std::vector<ChannelParams> &sessions);
    int sendSelectPKDBySession(int sessionID, std::vector<ChannelPkd> &pkds);
    int sendYantraData( uint64_t bytesUsed, uint64_t pkdPos, bool isRing, uint64_t dwTime, int rCounter, uint32_t dataShift);
    int getYantraData( uint64_t &bytesUsed, uint64_t &pkdPos, bool &isRing, uint64_t &dwTime , int &rCounter, uint32_t &dataShift);
    int updateYantraData( uint64_t bytesUsed, uint64_t pkdPos, bool isRing, uint64_t dwTime, int rCounter, uint32_t dataShift );
    int clearPKDData(uint64_t from, uint64_t to);
    int selectSessionsForRemove(std::vector<int> &sessions, uint64_t startPos, uint64_t endPos);
    int removeSessionAndPkds(int sessionID);
    int sendSelectSessionPKDSize(size_t &size, const int sessionID);
    int sendSelectPKDByGroup(int sessionID, int limit,
                             int offset, int group, std::vector<ChannelPkdLite> &pkds);
    int sendSelectPKDByGroup(int sessionID, int limit,
                             int offset, int group, std::map<int, GraphZoom> &zoomPages);
    int sendSelectPKDByteShift(int sessionID, int limit, uint32_t startAddress,
                      uint32_t endAddress, uint64_t &time);
    int sendSelectPKDAddress(int sessionID, uint64_t second, int channel, uint32_t &address);
    int sendSelectSessionByTime( uint64_t time, std::vector<ChannelParams> &sessions);
    int sendSelectSessionsPartOfReused(uint32_t bytesUsed, std::vector<ChannelParams> &sessions);
    int sendSelectSessionsWasReused(uint32_t startAddress, uint32_t lastByteUsed, uint32_t bytesUsed, std::vector<ChannelParams> &sessions);
    int sendUpdateFullSessionCommand(int id, uint64_t startTime, uint64_t endTime, int channel, int byteShift, uint32_t startAddress);
    int sendRemovePkdsLowerByteShifts(int id, int byteShift);
    int sendSelectSessionByEndTime( uint64_t time, int channel, std::vector<ChannelParams> &sessions);


private:
    std::shared_ptr<rosslog::Log> logger;
    std::map<uint64_t , std::map<int, ChannelParams>> sessions;
    std::mutex mutex;
    sqlite3_stmt *insertStmt;
    sqlite3_stmt *insertPKDStmt;
    sqlite3_stmt *selectStmt;
    sqlite3_stmt *updateStmt;
    sqlite3_stmt *selectSessionsStmt;
    sqlite3_stmt *selectPKDBySesStmt;
    sqlite3_stmt *setYantraDataStmt;
    sqlite3_stmt *getYantraDataStmt;
    sqlite3_stmt *updateYantraDataStmt;
    sqlite3_stmt *selectSessionsForRemStmt;
    sqlite3_stmt *removeSessionByIDStmt;
    sqlite3_stmt *removePKDSByIDStmt;
    sqlite3_stmt *selectSessionPKDSize;
    sqlite3_stmt *selectPKDByGroupStmt;
    sqlite3_stmt *selectPKDStmt;
    sqlite3_stmt *selectPKDAddress;
    sqlite3_stmt *selectSessionsByDateStmt;
    sqlite3_stmt *selectSessionsByChannelTypeStmt;
    sqlite3_stmt *selectSessionByTime;
    sqlite3_stmt *selectSessionWithReused;
    sqlite3_stmt *selectSessionReused;
    sqlite3_stmt *sessionFullUpdate;
    sqlite3_stmt *removePkdsLowerByteShift;
    sqlite3_stmt *selectSessionByEndTime;

    sqlite3 *db;


};


#endif //CM_INTERFACE_DATABASEPKDWORKER_H
