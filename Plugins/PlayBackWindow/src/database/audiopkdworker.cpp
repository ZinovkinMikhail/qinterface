//
// Created by ivan on 09.11.2020.
//

#include "audiopkdworker.h"

#include <utility>
#include <random>
#include <bitset>

using namespace rosslog;

AudioPKDWorker::AudioPKDWorker(std::shared_ptr<rosslog::Log> log,
                               IAudioPKDWorker &audioWorkerInterface) :
    logger(std::move(log)),
    audioWorkerInterface(audioWorkerInterface),
    databaseCreator(nullptr),
    pkdDb(nullptr)
{
    initDatabaseCreator();

    bool databaseState = checkDatabaseExists();

    if( databaseState )
    {
        auto db = databaseCreator->createDatabase(false, DATABASE_PATH);

        if( db == nullptr )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__ , "Can't create database!");
            abort();
        }

        pkdDb = std::make_shared<DatabasePKDWorker>(logger, db);
        pkdDb->initDatabase(false);        
    }
}

AudioPKDWorker::~AudioPKDWorker()
{
    std::cerr << "AudioPKDWorker remove" << std::endl;
}

//Тип 0 - Нет базы парсим с 0
//Тип 1 - Не было кольца, время первого пакета не равно тому что в базе( опорная информация ) Чистим всю базу

bool AudioPKDWorker::startPKDParse(uint64_t bytesUsed, uint64_t flashCapacity)
{
    ParseCommonInfo parseCommonInfo;
    ParseResultInfo parseResultInfo;

    memset(&parseCommonInfo, 0, sizeof(ParseCommonInfo));
    memset(&parseResultInfo, 0, sizeof(ParseResultInfo));

    parseCommonInfo.bytesUsed = bytesUsed;
    parseCommonInfo.flashCapacity = flashCapacity;

    //Проверим что у нас есть база
    bool databaseState = checkDatabaseExists();
    bool haveYantraData = true;


    if( pkdDb == nullptr )
    {
        auto db = databaseCreator->createDatabase(true, DATABASE_PATH);

        if( db == nullptr )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__ , "Can't create database!");
            return false;
        }

        pkdDb = std::make_shared<DatabasePKDWorker>(logger, db);
        pkdDb->initDatabase(false);
    }

//    audioWorkerInterface.pkdFileParseFinished(false);

//    return true;
    std::shared_ptr<FilePKDWorker> filePKDWorker = std::make_shared<FilePKDWorker>(logger, pkdDb, shared_from_this());


    try
    {
        parseCommonInfo.isRing = checkRecordForRing(parseCommonInfo.rCounter); //Узнаем есть ли кольцо и сколько раз оно было
        parseCommonInfo.currentDWTime = getCurDwTime(); //Получим время первого пакета пкд
        std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Check first pkd packet info" << std::endl;
        std::cout << "isRing = " << parseCommonInfo.isRing << std::endl;
        std::cout << "curDwTime = " << parseCommonInfo.currentDWTime << std::endl;
        std::cout << "rCounter = " << parseCommonInfo.rCounter << std::endl;
        std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Check first pkd packet info" << std::endl;
    }
    catch (std::runtime_error &exception)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__ ,
                              "Error while checking for ring %s\n", exception.what());
        return false;
    }

    this->logger->Message(LOG_ERR, __LINE__, __func__ ,
                          "IS RING = %i\n", parseCommonInfo.isRing);




    //Входная информация парсинга

    double usedPercent = (double)bytesUsed * 100.0 / flashCapacity;
    parseCommonInfo.pkdBytePos = bytesUsed/64;

    std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Start pkd parse info" << std::endl;
    std::cout << "databaseState = " << databaseState << std::endl;
    std::cout << "bytesUsed = " << bytesUsed << std::endl;
    std::cerr << "Capacity = " << flashCapacity << std::endl;
    std::cerr << "USED percent = " << usedPercent << std::endl;
    std::cerr << "pkdBytePos = " << parseCommonInfo.pkdBytePos << std::endl;
    std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Start pkd parse info" << std::endl;

    parseCommonInfo.databaseState = databaseState;

    this->logger->Message(LOG_ERR, __LINE__, __func__ ,
                          "Used percent %f Byte PKD pos = %li\n", usedPercent, parseCommonInfo.pkdBytePos);

    if( !databaseState )
    {
        //Базы нет, но могло быть кольцо, тут надо тоже думать
        this->logger->Message(LOG_DEBUG, __LINE__, __func__ , "Database does not exists");
        haveYantraData = false;

        if( parseCommonInfo.isRing )
            parseResultInfo.parseType = RING_FIRST_TIME;
        else
            parseResultInfo.parseType = SIMPLE_FIRST_TIME;
    }
    else
    {

        pkdDb->startTransaction();

        //Получим опорную информацию для парсинга, Сколько было записано, последнюю позицию пкд было ли кольцо
        int ret = pkdDb->getYantraData(parseCommonInfo.lastRecordBytesUsed,
                                       parseCommonInfo.lastPkdPos,
                                       parseCommonInfo.lastIsRing,
                                       parseCommonInfo.lastDWTime, parseCommonInfo.lastRCounter, parseCommonInfo.lastDataShift);



        std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Main yantra parse data " << std::endl;
        std::cout << "1 - lastIsRing = " << parseCommonInfo.lastIsRing << std::endl;
        std::cerr << "2 - dwTime = " << parseCommonInfo.lastDWTime << std::endl;
        std::cerr << "3 - lastPkdPos = " << parseCommonInfo.lastPkdPos << std::endl;
        std::cout << "4 - lastRecordBytesUsed = " << parseCommonInfo.lastRecordBytesUsed << std::endl;
        std::cout << "5 - lastRCounter = " << parseCommonInfo.lastRCounter << std::endl;
        std::cout << "6 - lastDataShift = " << parseCommonInfo.lastDataShift << std::endl;

        std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Main yantra parse data " << std::endl;

        this->logger->Message(LOG_DEBUG, __LINE__, __func__ ,
                              "PKD 1 - lastIs ring %i, 2 - dwTime %li, 3 - lastPkdPos %li, 4 - lastRecordBytesUsde %li, 5 - lastRcounter %li , 6 - lastDataShift %li",
                              parseCommonInfo.lastIsRing, parseCommonInfo.lastDWTime, parseCommonInfo.lastPkdPos,
                              parseCommonInfo.lastRecordBytesUsed, parseCommonInfo.lastRCounter, parseCommonInfo.lastDataShift);

        pkdDb->endTransaction();

        if( ret == 0 )
        {
            this->logger->Message(LOG_DEBUG, __LINE__, __func__ ,
                                  "Database data = %li last pkd pos %li  pkd bytes pos = %li",
                                  parseCommonInfo.lastRecordBytesUsed, parseCommonInfo.lastPkdPos, parseCommonInfo.pkdBytePos);


            //Тип 1. Нет кольца. Проверяем время первого сектора на соответствие с базой, если не сходится Стираем

//            std::cerr << "----------------------- CHECK DWTIME -----------------" << std::endl;
//            std::cerr << "parseCommonInfo.currentDWTime = " << parseCommonInfo.currentDWTime << std::endl;
//            std::cerr << "parseCommonInfo.lastDWTime = " << parseCommonInfo.lastDWTime << std::endl;
//            std::cerr << "----------------------- CHECK DWTIME -----------------" << std::endl;

            if( parseCommonInfo.currentDWTime != parseCommonInfo.lastDWTime && !parseCommonInfo.isRing )
            {
                workOnParseType1(parseCommonInfo, parseResultInfo);
                parseResultInfo.parseType = SIMPLE_FIRST_TIME;
            }

            if( parseCommonInfo.isRing != parseCommonInfo.lastIsRing )
            {
                if( parseCommonInfo.lastIsRing ) //Тип 2 У нас было кольцо, а сейчас нет Чистим
                {
                    workOnParseType2(parseCommonInfo, parseResultInfo);
                    parseResultInfo.parseType = SIMPLE_FIRST_TIME;
                }
                else if( parseCommonInfo.isRing ) //У нас появилось кольцо
                {
                    if( parseCommonInfo.lastRecordBytesUsed == 0 ) // Тип 3 У нас не было записей, но сейчас кольцо. Читаем как кольцо
                    {
                        //У нас и не было записей
                        //Ring ContiueFULL from byteUsed like first time
                        parseResultInfo.parseType = RING_FIRST_TIME;
                    }
                    else if( parseCommonInfo.bytesUsed >= parseCommonInfo.lastRecordBytesUsed || parseCommonInfo.rCounter > 2 ) //Тип 4 Записанные данные при кольце были перезаписаны или кольцо поверх нашего больше 1 слое. Чистим
                    {
                        clearDatabase();
                        parseResultInfo.parseType = RING_FIRST_TIME;
                        //Тут мы понимаем что данные из базы уже неактуальны, стираем все и начинаем парсить с byteUsed
                        //Так же если rCounter > 2 значит нас точно перезаписали
                        //Ring ContiueFULL from byteUsed like first time
                    }
                    else //Тип 5 Данные при появившися кольце актуальны, но надо проверить время сектора если было стирание. Если не было стирания уменьшаем текущую сессию и дочитываем.
                    {
                        bool res =workOnParseCheckTimeAndResize(parseCommonInfo, parseResultInfo, filePKDWorker);
                        if( !res )
                        {
                            goto startParse;
                        }
                        else
                        {
                            parseResultInfo.pkdPos = parseCommonInfo.lastPkdPos;
                        }
                        std::cerr << "PARSE TYPE = " << parseResultInfo.parseType << std::endl;
                    }
                }
            }
            else if( parseCommonInfo.isRing && parseCommonInfo.lastIsRing ) //У нас было кольцо и есть кольцо
            {
                if( parseCommonInfo.bytesUsed >= parseCommonInfo.lastRecordBytesUsed && parseCommonInfo.rCounter == parseCommonInfo.lastRCounter ) //Тип 6 Есть и было кольцо, И записанных данных больше. Проверяем сектор времени на стирание. Если не было дочитываем
                {
                    //Тут мы понимаем что данные актуальны, просто дозаписали
                    //Проверка сектора времени
                    //Ring ContiueTILL from lastParsed
                    bool res = workOnParseCheckTimeAndResize(parseCommonInfo, parseResultInfo, filePKDWorker);
                    if( !res )
                    {
                        goto startParse;
                    }
                    else
                    {
                        parseResultInfo.pkdPos = parseCommonInfo.lastPkdPos;
                        parseResultInfo.parseType = RING_CONTINUE_TILL;
                    }
                    std::cerr << "SET 1 PARSE TYPE = " << parseResultInfo.parseType << std::endl;

                }
                else if( parseCommonInfo.bytesUsed < parseCommonInfo.lastRecordBytesUsed && parseCommonInfo.rCounter == parseCommonInfo.lastRCounter )//Тип 7 Есть и было кольцо. Количество циклов = Количеству в базе. Записано меньше чем было, значит было стирание.
                {
                    //Было стирание. Чистим все
                    //Ring ContiueFULL from byteUsed like first time
                    clearDatabase();
                    parseResultInfo.parseType = RING_FIRST_TIME;
                }
                else if( parseCommonInfo.rCounter > parseCommonInfo.lastRCounter && parseCommonInfo.bytesUsed >= parseCommonInfo.lastRecordBytesUsed  )//Тип 8 Есть и было кольцо. Количество циклов больше чем было. Записано >= чем есть. Данные не актуальны
                {
                    //Случай когда данные не актуальны
                    //Ring ContiueFULL from byteUsed like first time
                    clearDatabase();
                    parseResultInfo.parseType = RING_FIRST_TIME;

                }
                else if( parseCommonInfo.rCounter > parseCommonInfo.lastRCounter && parseCommonInfo.bytesUsed < parseCommonInfo.lastRecordBytesUsed  )//Тип 9 Есть и было кольцо. Количество циклов больше чем было. Проверяем сектор времени, данные актуальны. Уменьшаем необходимую сессию, стираем перезаписанные
                {
                    //Данные актуальны, но нужно проверить сектор время
                    //Проверка сектора времени
                    bool res = workOnParseCheckTimeAndResize(parseCommonInfo, parseResultInfo, filePKDWorker);
                    if( !res )
                    {
                        goto startParse;
                    }
                    else
                    {
                        parseResultInfo.pkdPos = parseCommonInfo.lastPkdPos;
                    }
                }
                else
                {
                    //Ring ContiueFULL from byteUsed like first time
                    //Не учли ситуацию, чистим и парсим
                    clearDatabase();
                    parseResultInfo.parseType = RING_FIRST_TIME;
                }
            }
            else if( !parseCommonInfo.isRing && !parseCommonInfo.lastIsRing )
            {
                if(  parseCommonInfo.bytesUsed < parseCommonInfo.lastRecordBytesUsed  ) //Тип 10 Не было кольца и нету сейчас. Записано меньше чем мы имеем, Было стирание
                {
                    //Было стирание
                    clearDatabase();
                    parseResultInfo.parseType = SIMPLE_FIRST_TIME;
                }
                else if( parseCommonInfo.bytesUsed > parseCommonInfo.lastRecordBytesUsed ) //Тип 11 Не было кольца и нету сейчас. Записано больше чем мы имеем, просто дочитываем
                {
                    //Просто дочитываем
                    parseResultInfo.parseType = SIMPLE_CONTINUE;
                    parseResultInfo.pkdPos = parseCommonInfo.lastPkdPos;
                }
                else if( parseCommonInfo.bytesUsed == parseCommonInfo.lastRecordBytesUsed )  //Тип 12 Не было кольца и нету сейчас. Записано столько же сколько и было, просто показываем сессии. Выходим
                {
                    audioWorkerInterface.pkdFileParseFinished(false, "");
                    return true;
                }
            }
        }
        else
        {
            //Данные из базы не получили чистим все и парсим по новой
            if( parseCommonInfo.isRing ) //Тип 13 База есть,но там пусто и появилось кольцо. Парсим как кольцо
            {
                //Ring ContiueFULL from byteUsed like first time
                parseResultInfo.parseType = RING_FIRST_TIME;
                //Но если кольцо то парсим по другому
            }
        }
    }
startParse:
    fileParsers.push_back(filePKDWorker);
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    std::cerr << "==== startFilePartParse() ====" << std::endl;
    std::cerr << "PKD_FILE_PATH = " << PKD_FILE_PATH << std::endl;
    std::cerr << "flashCapacity = " << flashCapacity << std::endl;
    std::cerr << "lastPos = " << parseResultInfo.pkdPos << std::endl;
    std::cerr << "pkdBytePos = " << parseCommonInfo.pkdBytePos << std::endl;
    std::cerr << "isRing = " << parseCommonInfo.isRing << std::endl;
    std::cerr << "isFirstTime = " << !databaseState << std::endl;
    std::cerr << "bytesUsed = " << bytesUsed << std::endl;
    std::cerr << "haveYantraData = " << haveYantraData << std::endl;

    std::cerr << "PARSE TYPE = " << parseResultInfo.parseType << std::endl;


    this->logger->Message(LOG_INFO, __LINE__, __func__ ,
                          "PKD 1 - flashCapacity %li, 2 - parseResultInfo.pkdPos %li, 3 - parseCommonInfo.pkdBytePos %li, 4 - parseCommonInfo.isRing %i,"
                          " 5 - !databaseState %li , 6 - bytesUsed %li 7 - haveYantraData %i 8 - parseType - %i",
                          flashCapacity, parseResultInfo.pkdPos,
                          parseCommonInfo.pkdBytePos, parseCommonInfo.isRing, !databaseState, bytesUsed, haveYantraData, parseResultInfo.parseType);

    if( !filePKDWorker->startFilePartParse(PKD_FILE_PATH, flashCapacity, parseResultInfo.pkdPos,
                                           parseCommonInfo.pkdBytePos, parseCommonInfo.isRing, !databaseState,
                                           bytesUsed, 0, parseCommonInfo.currentDWTime, haveYantraData,
                                           parseCommonInfo.rCounter, parseResultInfo.parseType) )
    {
        return false;
    }



    return true;
}

void AudioPKDWorker::initDatabaseCreator()
{
    if( databaseCreator == nullptr )
    {
        databaseCreator = std::make_shared<DatabaseCreator>(logger);
    }
}

static int filesLoaded = 0;
void AudioPKDWorker::fileLoadingFinished(int index, bool isError, const std::string &msg)
{
    audioWorkerInterface.pkdFileParseFinished(isError, msg);
}

bool AudioPKDWorker::getSessions(std::vector<ChannelParams> &sessions)
{
    pkdDb->startTransaction();
    pkdDb->sendSelectSessions(sessions);
    pkdDb->endTransaction();
    return true;
}

bool AudioPKDWorker::getPKDBySession(int channel, std::vector<ChannelPkd> &pkds)
{
    pkdDb->startTransaction();
    pkdDb->sendSelectPKDBySession(channel, pkds);
    pkdDb->endTransaction();
    return true;
}

void AudioPKDWorker::fileParseProcessState(int procent)
{
   // logger->Message(LOG_ERR, __LINE__, __func__, "File parse procent = %f\n", procent);
    audioWorkerInterface.pkdFileParseProgress(procent);
}

bool AudioPKDWorker::checkDatabaseExists()
{
    struct stat path_stat;
    int ret = stat(DATABASE_PATH, &path_stat);

    if( ret >= 0 )
    {
        std::cerr << "DB SIZE = " << path_stat.st_size << std::endl;
        std::cerr << "DB BLOCKS = " << path_stat.st_blocks << std::endl;
        std::cerr << "DB BLOCK SIZE = " << path_stat.st_blksize << std::endl;
        return true;
    }
    return false;
}

bool AudioPKDWorker::checkRecordForRing(int &rCounter)
{

//#ifdef DEBUG_NO_ADAPTER
//#ifdef ISRING
//    rCounter = RCOUNTER;
//    return true;
//#else
//    return false;
//#endif
//#endif

    std::cout << "checkRecordForRing" << std::endl;

    std::ifstream disk(PKD_FILE_PATH, std::ios_base::binary);

    if(!disk)
        throw std::runtime_error("Error while opening file!!");

    disk.seekg(80, std::ios_base::beg);

    if(!disk)
        throw std::runtime_error("Error while checking for ring!!");

    char buf[2];
    disk.read(buf, 2);

    uint16_t wCounter = SETUINT16(buf[0], buf[1]);

    std::cout << "wCounter = " << wCounter << std::endl;

    rCounter = wCounter;
    if( wCounter > 1 )
        return true;

    return false;
}

bool AudioPKDWorker::getLastRecordedInfo(uint64_t &lastBytesRecorded)
{
    uint64_t lastPkdPos = 0;
    bool lastIsRing = false;
    uint64_t dwTime = 0;
    uint32_t lastDataShift = 0;
    int rCounter;

    if( pkdDb == nullptr )
        return false;

    pkdDb->startTransaction();

    int ret = pkdDb->getYantraData(lastBytesRecorded, lastPkdPos, lastIsRing, dwTime, rCounter, lastDataShift);
    std::cerr << "dwTime = " << dwTime << std::endl;

    pkdDb->endTransaction();

    if( ret == 0 )
    {
        this->logger->Message(LOG_DEBUG, __LINE__, __func__ ,
                              "Database data bytes recoded = %li last pkd pos %li ",
                              lastBytesRecorded, lastPkdPos);
        return true;

    }
    return false;
}

bool AudioPKDWorker::startPKDParseTest()
{
    //DELETED
    return true;

}

bool AudioPKDWorker::getPKDSessionSize(const int channel, size_t &size)
{
    pkdDb->startTransaction();
    if( pkdDb->sendSelectSessionPKDSize(size, channel) < 0 )
    {
        pkdDb->endTransaction();
        return false;
    }
    pkdDb->endTransaction();
    return true;
}

bool AudioPKDWorker::getPKDByGroup(int sessionID, int limit,
                                   int offset, int group, std::vector<ChannelPkdLite> &pkds)
{
    pkdDb->startTransaction();

    if( pkdDb->sendSelectPKDByGroup(sessionID, limit, offset, group ,pkds) < 0 )
    {
        pkdDb->endTransaction();
        return false;
    }
    pkdDb->endTransaction();
    return true;
}

bool AudioPKDWorker::getPKDByGroup(int sessionID, int limit, int offset, int group, std::map< int , GraphZoom> &zoomPage)
{
    pkdDb->startTransaction();

    if( pkdDb->sendSelectPKDByGroup(sessionID, limit, offset, group ,zoomPage) < 0 )
    {
        pkdDb->endTransaction();
        return false;
    }
    pkdDb->endTransaction();
    return true;
}

bool AudioPKDWorker::getPKDAddress(int sessionID, uint64_t time, int channel, uint32_t &address)
{
    pkdDb->startTransaction();

    if( pkdDb->sendSelectPKDAddress(sessionID, time, channel ,address) < 0 )
    {
        pkdDb->endTransaction();
        return false;
    }
    pkdDb->endTransaction();
    return true;
}

bool AudioPKDWorker::removeDatabase()
{
    if( databaseCreator != nullptr )
    {
        if( databaseCreator->clearDatabase() )
        {
            fileParsers.clear();
            pkdDb = nullptr;
            return true;
        }
    }
    return false;
}

void AudioPKDWorker::stopPKDParse()
{
    fileParsers.clear();
}

bool AudioPKDWorker::getSessionsByDate(const uint64_t startDate, const uint64_t endDate,
                                       std::vector<ChannelParams> &sessions) {
    pkdDb->startTransaction();
    if( pkdDb->sendSelectSessionsByDate(startDate, endDate ,sessions) < 0 )
    {
        pkdDb->endTransaction();
        return false;
    }
    pkdDb->endTransaction();
    return true;
}

bool AudioPKDWorker::getSessionsByChannelType(uint8_t channelType,
                                              uint8_t channel,
                                              std::vector<ChannelParams> &sessions)
{
    pkdDb->startTransaction();
    if( pkdDb->sendSelectSessionsByChannelType(channelType, channel ,sessions) < 0 )
    {
        pkdDb->endTransaction();
        return false;
    }
    pkdDb->endTransaction();
    return true;
}

uint32_t AudioPKDWorker::getCurDwTime()
{
    std::cout << "getCurDwTime" << std::endl;

    std::ifstream disk(PKD_FILE_PATH, std::ios_base::binary);

    if(!disk)
        throw std::runtime_error("Error while opening file!!");

    disk.seekg(0, std::ios_base::beg);

    if(!disk)
        throw std::runtime_error("Error while checking for ring!!");

    char buf[16];
    disk.read(buf, 16);

    uint32_t curDwTime = SETUINT32(
                    SETUINT16(buf[14], buf[15]),
                    SETUINT16(buf[12], buf[13]));

    std::cerr << "curDwTime = " << curDwTime << std::endl;

    return curDwTime;
}

bool AudioPKDWorker::workOnParseType1(const ParseCommonInfo &parseCommonInfo, ParseResultInfo &resultInfo)
{
    std::cerr << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Clear db TYPE 1 " << std::endl;
    //Время первого пакета не равно тому что в базе и нет кольца, очевидно что было стирание.
    this->logger->Message(LOG_DEBUG, __LINE__, __func__ ,
                          "Remove all sessions and pkds cuz we do not have now ring and start packet time is different");

    clearDatabase();

    resultInfo.pkdPos = 0;
    resultInfo.dataBaseState = false;
    return true;
}

bool AudioPKDWorker::workOnParseType2(const ParseCommonInfo &parseCommonInfo, ParseResultInfo &resultInfo)
{
    clearDatabase();
    return true;
}

bool AudioPKDWorker::clearDatabase()
{
    pkdDb->startTransaction();
    this->logger->Message(LOG_DEBUG, __LINE__, __func__ ,
                          "Start remove all sessions!");
    std::vector<ChannelParams> sessions;
    pkdDb->sendSelectSessions(sessions);
    for(auto it: sessions)
    {
        this->logger->Message(LOG_DEBUG, __LINE__, __func__ ,
                              "Remove session id = %i sessions size %i",
                              it.id, sessions.size());


        pkdDb->removeSessionAndPkds(it.id);
    }

    pkdDb->endTransaction();
    return true;
}

bool AudioPKDWorker::workOnParseType3(const ParseCommonInfo &parseCommonInfo, ParseResultInfo &resultInfo)
{
    clearDatabase();
    return false;
}

bool
AudioPKDWorker::workOnParseCheckTimeAndResize(const ParseCommonInfo &parseCommonInfo, ParseResultInfo &parseResultInfo, std::shared_ptr<FilePKDWorker> filePKDWorker)
{
    std::cerr << "!!!!!!!!!!!!!!!!!!!!!!!!!!!1 TYPE 5" << std::endl;
    //Случай когда данные актуальны , нам необходимо обрезать данные в базе, но мы должны проверить время сектора
    //Проверка сектора времени
    std::cerr << "!!!!!!!!!!!!!!!!! " << parseCommonInfo.flashCapacity << std::endl;
    std::cerr << "!!!!!!!!!!!!!!!!! " << parseCommonInfo.lastPkdPos  << std::endl;
//    uint16_t spacing = 0;
//    //$$$ try remove -128 (now time -1 sek)
//    uint64_t time = filePKDWorker->getCurPKDParseTime(PKD_FILE_PATH,
//                                                      (parseCommonInfo.flashCapacity + parseCommonInfo.lastDataShift  ),
//                                                      parseCommonInfo.flashCapacity, spacing);

//    std::cerr << "Time = " << time << std::endl;

    parseResultInfo.parseType = RING_FIRST_TIME;


//    if( time != 0 )
//    {
//        std::vector<ChannelParams> sessions;
//        pkdDb->startTransaction();
//        if( pkdDb->sendSelectSessionByTime(time, sessions) < 0 )
//        {
//            std::cerr << "Error while get sessions by time " << std::endl;
//            pkdDb->endTransaction();
//            clearDatabase();
//            parseResultInfo.parseType = RING_FIRST_TIME;
//            return false;
//        }
//        pkdDb->endTransaction();

//        std::cerr << "SESSIONS SIZE = " << sessions.size() << std::endl;

//        if( sessions.size() == 0 )
//        {
//            clearDatabase();
//            //Ring ContiueFULL from byteUsed like first time
//            parseResultInfo.parseType = RING_FIRST_TIME;
//            std::cerr << "NO SESSIONS SET RING_FIRST_TIME"  << std::endl;
//            return false;
//        }
//        else
//        {
            //Запрос сессий  в которые входит byteused текущий start <= end >=
            //Устанавливаем
            //Запрос сессий которые меньше byteused


            std::vector<ChannelParams> sessionsWithUsed;
            pkdDb->startTransaction();
            if( pkdDb->sendSelectSessionsPartOfReused((parseCommonInfo.bytesUsed/64), sessionsWithUsed) < 0 )
            {
                std::cerr << "Error while get sessions with rused " << std::endl;
            }
            pkdDb->endTransaction();

            if( sessionsWithUsed.size() > 0 )
            {
                std::cerr << "!!!!!!!!!!!!!! WE HAVE REUSED " << std::endl;

                bool timeFound = false;
                uint64_t time2;
                int freshStartAddress = 0;

                for( auto &it: sessionsWithUsed ) //Все сессии равномерно записывались если они попали в part of reused значит у них так или иначе общая облачсть памяти в перемшку
                {

                    if( !timeFound )
                    {
                        freshStartAddress = (parseCommonInfo.bytesUsed/64) + 64;

                        std::cerr << "!!!!!!!!!! Fresh start address = " << freshStartAddress << std::endl;
                        std::cerr << "!!!!!!!!!! Fresh start address = " << parseCommonInfo.flashCapacity + freshStartAddress << std::endl;


                        if( freshStartAddress > parseCommonInfo.flashCapacity /64 )
                        {
                            std::cerr << "We will be over the flash just clear and continue" << std::endl;
                            clearDatabase();
                            parseResultInfo.parseType = RING_CONTINUE_FULL;
                            return false;
                        }

                        uint16_t spacing = 0;
                        time2 = filePKDWorker->getCurPKDParseTime(PKD_FILE_PATH, (parseCommonInfo.flashCapacity + freshStartAddress ), parseCommonInfo.flashCapacity, spacing);

                        if( time2 == 0 )
                        {
                            //Чистим все
                            //Ring ContiueFULL from byteUsed like first time
                            clearDatabase();
                            parseResultInfo.parseType = RING_CONTINUE_FULL;
                            return false;
                        }
                        else
                        {
                            timeFound = true;
                            freshStartAddress += spacing;
                            // обновляем у сессий время и старт адрес
                            // удаляем у сессий пкд ниже этой секунды

                            // Мы должны парсить от старого юзд до конца и до нового юзд

                            parseResultInfo.pkdPos = parseCommonInfo.lastPkdPos;
                            parseResultInfo.dataBaseState = true;
                            parseResultInfo.parseType = RING_CONTINUE_FULL_FROM_LAST;


                            std::cerr << "SET RING_CONTINUE_FULL_FROM_LAST "  << std::endl;

                            //Ring continue Full from lastParsed

                            pkdDb->startTransaction();
                            pkdDb->sendUpdateFullSessionCommand(it.id, time2, it.endTime, it.channel,
                                                                it.sessionByteShift, freshStartAddress);
                            pkdDb->sendRemovePkdsLowerByteShifts(it.id, freshStartAddress);
                            pkdDb->endTransaction();

                        }

                        std::cerr << "!!!!!!!! New time = " << time2 << std::endl;
                    }
                    else
                    {
                        pkdDb->startTransaction();
                        pkdDb->sendUpdateFullSessionCommand(it.id, time2, it.endTime, it.channel,
                                                            it.sessionByteShift, freshStartAddress);
                        pkdDb->sendRemovePkdsLowerByteShifts(it.id, freshStartAddress);
                        pkdDb->endTransaction();
                    }
                }
            }


            std::vector<ChannelParams> sessionsReused;
            pkdDb->startTransaction();

            uint32_t lastByteUsed = 0;

            //Если новое кольцо а прошлый раз мы парсили до начала нового кольца, то проверка
            //на переиспользование памияи начинается с 0. тк все перезаписанные не актуальны
            if( parseCommonInfo.lastRecordBytesUsed < parseCommonInfo.bytesUsed ){
                lastByteUsed = parseCommonInfo.lastRecordBytesUsed;
            }

            if( pkdDb->sendSelectSessionsWasReused( (parseCommonInfo.bytesUsed/64), lastByteUsed/64, (parseCommonInfo.bytesUsed/64), sessionsReused) < 0 )
            {
                std::cerr << "Error while get sessions with rused " << std::endl;
            }
            pkdDb->endTransaction();

            if( sessionsReused.size() > 0 )
            {
                std::cerr << "!!!!!!!!!!!!!! WE HAVE FULL REUSED SESSIONS " << std::endl;
                pkdDb->startTransaction();
//                bool isRingSessions = false;
                std::vector<ChannelParams> ringSessions;
                for( auto &it : sessionsReused )
                {
                    std::cerr << "REUSED ID = " << it.id << std::endl;
                    pkdDb->removeSessionAndPkds(it.id);
                }
                pkdDb->endTransaction();

            }
//        }
//    }
//    else
//    {
//        clearDatabase();
//        std::cerr << "SET RING_CONTINUE_FULL "  << std::endl;
//        parseResultInfo.parseType = RING_CONTINUE_FULL;
//        return false;
//    }

    return true;
}
