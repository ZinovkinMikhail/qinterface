//
// Created by ivan on 09.11.2020.
//

#include <unistd.h>

#include "filepkdworker.h"

using namespace rosslog;

FilePKDWorker::FilePKDWorker(std::shared_ptr<rosslog::Log> log,
                             std::shared_ptr<DatabasePKDWorker> databasePKDWorker,
                             std::shared_ptr<IFIlePKDWorker> fileInterface) :
    logger(log),
    dbWorker(databasePKDWorker),
    threadWork(false),
    fileInterface(fileInterface)
{
    channelParams.resize(6); //DEFINE
    currentDate = QDateTime::currentDateTime().toSecsSinceEpoch();
   // currentDate += 1*31536000;
    lowPossibleYear = currentDate - (1 * 31536000);
}

bool FilePKDWorker::startFilePartParse(std::string filePath, size_t recordCapacity,
                                       size_t startPos, size_t endPos, bool isRing,
                                       bool isFirst, size_t recordBytesUsed, int index,
                                       uint32_t dwTime, bool haveYantraData, int rCounter, ParseType parseType)
{
    fileIndex = index;
    this->pkdUsed = endPos;
    this->isFirstTime = isFirst;
    this->isRing = isRing;
    this->recordCapacity = recordCapacity;
    this->lastParsed = startPos;
    this->recordBytesUsed = recordBytesUsed;
    this->packets = 0;
    this->dwTime = dwTime;
    this->haveYantraData = haveYantraData;
    this->rCounter = rCounter;
    this->parseType = parseType;

    std::cerr << "FilePKDWorker PARSE TYPE = " << parseType << std::endl;

    inputFile = new QFile(QString::fromStdString(filePath));
    this->filePath = filePath;


    fp = fopen(filePath.c_str(), "rb");

    if(!fp)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error file open");
        abort();
    }

    threadWork = true;
    channelThread = std::thread(&FilePKDWorker::threadFileFunction, this);
    return true;
}

uint64_t FilePKDWorker::getCurPKDParseTime(std::string pkdFilePath, uint64_t pkdPos, uint64_t flashCapacity, uint16_t &spacing)
{
//    std::cerr << "$$$$$$$$-2 =  " << flashCapacity << std::endl;
    std::cout << "getCurPKDParseTime" << std::endl;


    FILE *ff;

    ff = fopen(pkdFilePath.c_str(), "rb");

    if(!ff)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error file open");
        return 0;
    }
    std::cerr << "PKD POS = " << pkdPos << std::endl;

    if(!ff)
        throw std::runtime_error("Error while opening file!!");


    if (fseek(ff,(pkdPos), SEEK_SET) == -1)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                              "Error file seek to %li", (pkdPos - sizeof(pkd_t)));
        return 0;
    }



    uint64_t curDwTime = 0;

    bool findLast = true;

    int iterate = 0;
    while(findLast)
    {
        pkd_t pkds[1];

        memset(pkds, 0, sizeof(pkds));

//        std::cerr << "$$$$ iterate * 8 = " << iterate * 8 << std::endl;
//        std::cerr << "$$$$ (pkdPos - flashCapacity) + (iterate * 8) = " << (pkdPos - flashCapacity) + (iterate * 8) << std::endl;
//        std::cerr << "$$$$ flashCapacity / 64 = " << flashCapacity / 64 << std::endl;
//        std::cerr << "$$$$ flashCapacity = " << flashCapacity << std::endl;

//        int64_t v1 = (pkdPos - flashCapacity) + (iterate * 8);
//        std::cerr << "$$$$ v1 (pkdPos - flashCapacity) + (iterate * 8) = " << v1 << std::endl;
        if( (pkdPos - flashCapacity) + (iterate * 8) > flashCapacity / 64 )
        {
            std::cerr << "We are over the flash !!! stop " << std::endl;
            return 0;
        }


        size_t readed = fread((uint8_t*)pkds , sizeof(uint8_t), sizeof(pkd_t), ff);
//        size_t readed = fread((uint8_t*)pkds , sizeof(uint8_t), FILE_PART_PARSE_SIZE, ff); //sigfolt!

        if( readed < 0 )
        {
            std::cerr << "Bytes readed = " << iterate << std::endl;
            break;
        }
        else
        {
            iterate++;
        }

        pkd_t curPkd = pkds[0];

        if( checkPKDHeader(curPkd))
        {
            if( curPkd.channelId == channelAudio  ) //channel info?
            {
                std::cerr << "PKD IS CHANNEL AUDIO" << std::endl;
                curDwTime = curPkd.ltime | (curPkd.htime << 16);
                std::cerr << "curPkd.ltime = " << curPkd.ltime << std::endl;
                std::cerr << "curPkd.htime = " << curPkd.htime << std::endl;
//                std::cerr << "$$$$ TEST curDwTime = " << (uint64_t)((uint64_t)curPkd.ltime | (uint64_t)((uint64_t)curPkd.htime << 16)) << std::endl;
//                std::cerr << "$$$$ TEST curDwTime = " << (uint64_t)(curPkd.ltime | curPkd.htime << 16) << std::endl;
                std::cerr << "CHECK PKD HEADER CHECHED " << curPkd.channelId << std::endl;
                std::cerr << "getCurPKDParseTime = " << curDwTime << std::endl;
                std::cerr << "Bytes readed = " << iterate << std::endl;
                findLast = false;
                spacing = iterate * 8;
                break;
            }
        }
        else
        {
            if ( iterate > 1000 )
                return 0;
            //break;
        }
    }




    fclose(ff);

    return curDwTime;
}

FilePKDWorker::~FilePKDWorker()
{

    std::cerr << "Remove file pkd worker" << std::endl;
    threadWork = false;
    imidiatlyStop = true;

    if( channelThread.joinable() )
        channelThread.join();

    std::cerr << "Remove file pkd worker end" << std::endl;
}

void FilePKDWorker::threadFileFunction()
{
    errorParseCount = 0;
    uint8_t data[FILE_PART_PARSE_SIZE];
    int ret = 0;
    int rc;
    int progressIt = 0;
    int progressCounter = 0;
    int progressProcent = 0;

    memset(&data, 0, sizeof(data));
    int channel = 0;
//    uint64_t packets = 0;
    ChannelParams *currentChannel = nullptr;


    //IF IS RECORD

    for(auto& it: channelParams)
    {
        it.isOnRecordSessionChecked = false;
    }

    //

    bool isWork = true;

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    size_t fileSeek = 0;
    size_t fileEndPos = 0;

    int allReaded = 0;

    switch (parseType)
    {
        case RING_FIRST_TIME:
        {
            fileSeek = recordCapacity + pkdUsed;
            std::cout << "------ RING_FIRST_TIME ------ " << std::endl;
            allReaded = pkdUsed;
            break;
        }
        case SIMPLE_FIRST_TIME:
        {
            fileSeek = recordCapacity;
            std::cout << "------ SIMPLE_FIRST_TIME ------ " << std::endl;
            break;
        }
        case RING_CONTINUE_TILL:
        {
            fileSeek = recordCapacity + lastParsed;
            std::cout << "------ RING_CONTINUE_TILL ------ " << std::endl;
            allReaded = lastParsed;
            break;
        }
        case RING_CONTINUE_FULL:
        {
            fileSeek = recordCapacity + pkdUsed;
            std::cout << "------ RING_CONTINUE_FULL ------ " << std::endl;
            allReaded = pkdUsed;
            break;
        }
        case RING_CONTINUE_FULL_FROM_LAST:
        {
            fileSeek = recordCapacity + lastParsed;
            allReaded = lastParsed;
            std::cout << "------ RING_CONTINUE_FULL_FROM_LAST ------ " << std::endl;
            break;
        }
        case SIMPLE_CONTINUE:
        {
            fileSeek = recordCapacity + lastParsed;
            allReaded = lastParsed;
            std::cerr << "LastParsed = " << lastParsed << std::endl;
            std::cerr << "pkdUsed = " << pkdUsed << std::endl;
            std::cerr << "fileSeek = " << fileSeek << std::endl;
            std::cout << "------ SIMPLE_CONTINUE ------ " << std::endl;
            break;
        }
    }

    if (fseek(fp, fileSeek, SEEK_SET) == -1)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                              "Error file seek to %li", fileSeek);
    }

    size_t size=ftell (fp);
    std::cerr << "SEEK POSITION " << size << std::endl;

    if( !fp )
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__,
                              "Error file seek to %li", fileSeek);
        return;
    }
    else
    {
        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__,
                              "File seek to %li success", fileSeek);
    }


    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();


    int readed = 0;
    int sessionByteShift = 0;

    begin = std::chrono::steady_clock::now();
    isRingFirstPartReaded = false;

    bool stopParse = false;



    dbWorker->startTransaction();

    bool printReadDeb = false;
    int trashCount = 0;


    int errorRead = 0;

    pkd_t pkds[PKD_BLOCKS_IN];

    size_t writed = 0;

    FILE *fpOut;

    fpOut = fopen("/tmp/outPkd", "wb");

    if(!fpOut)
    {
        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error file out open");
        //abort();
    }

    packets = allReaded / 8;

    bool isRing = false;

    int allAudioPackets = 0;
    int allPackets = 0;
    int allTrashPackets = 0;
    int allInfoPackets = 0;
    int allEnvelop = 0;
    int allNonAudio = 0;
    int allTimeError = 0;

    int test1 = 0;
    errorParseCountGroups = 0;

    while( threadWork )
    {
        int k = 0;

        while( isWork )
        {
            uint8_t *dataU = (uint8_t*)&data;

            if( stopParse )
            {
                this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__, "File parsed stop");
                threadWork = false;
                isWork = false;
                break;
            }


            if( imidiatlyStop )
            {
                this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__, "File parsed stop imidiatly");
                threadWork = false;
                isWork = false;
                break;
            }


            memset(pkds, 0, sizeof(pkds));
            readed = fread((uint8_t*)pkds , sizeof(uint8_t), FILE_PART_PARSE_SIZE, fp);


            if(!readed)
            {
                std::cerr << "ERROR FILE READ = " << readed << " errno " << errno << std::endl;
                if( errorRead > READ_ERROR_COUNT )
                {
                        this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error file read errno = %i" , errno);
                        threadWork = false;
                        isWork = false;
                        errorRead = 0;

                        dbWorker->endTransaction();
                        sync();
                        if( fp )
                        {
                            fclose(fp);
                            fp = nullptr;
                        }
                        fileInterface->fileLoadingFinished(fileIndex, true,
                                                           ErrorMessageFormat::formatString("EMMC недоступно.",
                                                                                            Y4C_PARSE_ERROR_FILE_READ, errno));
                        break;
                }
                errorRead++;
                sleep(1);
                continue;
            }




            errorRead = 0;

            bool do4itka = false;

            switch ( parseType )
            {
                case RING_FIRST_TIME:
                {
                    if( !isRingFirstPartReaded && allReaded + 512 >= recordCapacity/64 )
                    {
                        do4itka = true;
                        isRingFirstPartReaded = true;
                        std::cerr << "All readed = " << allReaded << std::endl;
                        std::cout << "RING_FIRST_TIME -> do seek to RingFirstPart = " << recordCapacity << std::endl;
                        if (fseek(fp, recordCapacity, SEEK_SET) == -1)
                        {
                            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error file seek!!");
                            return;
                        }
                    }
                    else if( isRingFirstPartReaded && allReaded + 512 >= pkdUsed )
                    {
                        std::cerr << "All readed stop = " << allReaded << " pkdUsed = " << pkdUsed << std::endl;
                        isRing = true;
                        stopParse = true;
                    }
                    break;
                }
                case RING_CONTINUE_TILL:
                {
                    if( allReaded + 512 >= pkdUsed )
                    {
                        isRing = true;
                        stopParse = true;
                    }
                    break;
                }
                case RING_CONTINUE_FULL_FROM_LAST:
                case RING_CONTINUE_FULL:
                {
                    if( !isRingFirstPartReaded && allReaded + 512 >= recordCapacity/64 )
                    {
                        do4itka = true;
                        isRingFirstPartReaded = true;
//                        allReaded = 0;
                        std::cout << "STOP FIRST PART PARSE allreaded = " << allReaded << " | " << recordCapacity/64 << std::endl;
                        if (fseek(fp, recordCapacity, SEEK_SET) == -1)
                        {
                            this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error file seek!!");
                            return;
                        }

                        //Закрываем текущую сессию и ставим флаги на начало новой
//                        updateCurrentSessionsData(channelParams);
//                        packets = 0;
                    }
                    else if( isRingFirstPartReaded && allReaded + 512 >= pkdUsed )
                    {
                        std::cout << "STOP SECOND PART PARSE allreaded = " << allReaded << " | " << pkdUsed << std::endl;
                        isRing = true;
                        stopParse = true;
                    }
                    break;
                }
                case SIMPLE_FIRST_TIME:
                case SIMPLE_CONTINUE:
                {
                    if( allReaded + 512 >= pkdUsed )
                    {
                        stopParse = true;
                    }
                    break;
                }
            }


            if( readed > 0)
            {
                uint16_t haveToParse = readed;

                switch (parseType)
                {
                    case RING_FIRST_TIME:
                    case RING_CONTINUE_FULL_FROM_LAST:
                    case RING_CONTINUE_FULL:
                    {
                        if( isRingFirstPartReaded )
                        {
                            if(do4itka)
                            {
                                if(readed > recordBytesUsed/64)
                                {
                                    haveToParse = recordBytesUsed/64 - allReaded;
                                    std::cerr << "haveToParse = " << haveToParse << std::endl;
                                }
                            }
                            else
                            {
                                if(allReaded + readed > recordBytesUsed/64)
                                {
                                    haveToParse = recordBytesUsed/64 - allReaded;
                                    std::cerr << "haveToParse = " << haveToParse << std::endl;
                                }
                            }
                        }
                        else {
                            if(allReaded + readed >= recordCapacity/64)
                            {
                                haveToParse = recordCapacity/64 - allReaded;
                                std::cerr << "haveToParse = " << haveToParse << std::endl;
                            }
                        }

                        break;
                    }
                    default:
                        if(allReaded + readed > pkdUsed)
                        {
                            haveToParse = pkdUsed - allReaded;
                        }
                        break;
                }

//                if(allReaded + readed > pkdUsed)
//                {
////                    haveToParse = allReaded + readed - pkdUsed;
//                    haveToParse = pkdUsed - allReaded;
//                    std::cerr << "--------allReaded + readed > pkdUsed -----" << std::endl;
//                    std::cerr << "--------allReaded = " << allReaded << std::endl;
//                    std::cerr << "---------readed = " << readed << std::endl;
//                    std::cerr << "--------pkdUsed = " << pkdUsed << std::endl;
//                }


//                for( int i = 0; i < PKD_BLOCKS_IN; i++ )
                for( int i = 0; i < haveToParse/8; i++ )
                {

                    if( imidiatlyStop )
                    {
                        this->logger->Message(rosslog::LOG_INFO, __LINE__, __func__, "File parsed stop imidiatly 2");
                        threadWork = false;
                        isWork = false;
                        break;
                    }


                    pkd_t curPkd = pkds[i];

//                    size_t writeRet = fwrite((uint8_t*)&curPkd , sizeof(uint8_t), 8, fpOut);
//                    if( writeRet < 0 )
//                    {
//                        std::cerr << "ERROR FP OUT" << std::endl;
//                        abort();
//                    }
//                    else
//                    {
//                        writed++;
//                    }


                    ////////////////////////////////
//                    if( allReaded + 512 >= pkdUsed )
//                    {
//                        uint64_t t_time = curPkd.ltime | curPkd.htime << 16;

//                        std::cout << "curPkd.headSector = " << curPkd.headSector << std::endl;
//                        std::cout << "curPkd.isEnvelope = " << curPkd.isEnvelope << std::endl;

//                        std::cout << " >= allReaded = " << allReaded << " pkdUsed " << pkdUsed << std::endl;
//                        std::cout << "curPkd.channel = " << curPkd.channel << std::endl;
//                        std::cout << "t_time = " << t_time << std::endl;
//                        std::cout << "lastParsed = " << lastParsed << std::endl;

//                        if( curPkd.channelId == channelAudio  )
//                            std::cout << "channelAudio = TRUE" << std::endl;
//                        else
//                            std::cout << "channelAudio = FALSE" << std::endl;
//                    }

//                    if( allReaded - lastParsed < 512 )
//                    {
//                        uint64_t t_time = curPkd.ltime | curPkd.htime << 16;

//                        std::cout << "curPkd.headSector = " << curPkd.headSector << std::endl;
//                        std::cout << "curPkd.isEnvelope = " << curPkd.isEnvelope << std::endl;

//                        std::cout << " < 512 allReaded = " << allReaded << std::endl;
//                        std::cout << "curPkd.channel = " << curPkd.channel << std::endl;
//                        std::cout << "t_time = " << t_time << std::endl;
//                        std::cout << "pkdUsed = " << pkdUsed << std::endl;
//                        std::cout << "lastParsed = " << lastParsed << std::endl;

//                        if( curPkd.channelId == channelAudio  )
//                            std::cout << "channelAudio = TRUE" << std::endl;
//                        else
//                            std::cout << "channelAudio = FALSE" << std::endl;
//                    }
                    ////////////////////////////////



                    int result = checkPKDHeader(curPkd);
                    if( result >= 0 )
                    {
                        if( curPkd.channelId == channelAudio  ) //channel info?
                        {
                            allAudioPackets++;
                            test1 = 1;

                            if( curPkd.isStereo )
                            {
                                if( curPkd.channel >= 0 && curPkd.channel <= 4 )
                                {
                                    if( curPkd.channel == 0 )
                                    {
                                        currentChannel = &channelParams.at(static_cast<int>(STEREO_1));
                                        if( !currentChannel->isOnRecordSessionChecked )
                                        {
                                            std::cerr << "Check on record session in db STEREO 1" << std::endl;
                                            //currentChannel->isOnRecordSessionChecked = true;
                                        }
                                    }
                                    else if( curPkd.channel == 2)
                                    {
                                        currentChannel = &channelParams.at(static_cast<int>(STEREO_2));
                                        if( !currentChannel->isOnRecordSessionChecked )
                                        {
                                            std::cerr << "Check on record session in db STEREO 2" << std::endl;
                                            //currentChannel->isOnRecordSessionChecked = true;
                                        }
                                    }
                                }
                                else{
                                    packets++;
                                    logger->Message(LOG_ERR, __LINE__, __func__, "Channel not found error\n");
                                    continue;
                                }

                                if( !currentChannel )
                                {
                                    packets++;
                                    logger->Message(LOG_ERR, __LINE__, __func__, "Channel not found error\n");
                                    continue;
                                }


                                if( !currentChannel->isOnRecordSessionChecked )
                                {

                                    std::cerr << "Check on record session in db Stereo " << curPkd.channel << std::endl;
                                    uint64_t time = 0;
                                    time = curPkd.ltime | curPkd.htime << 16;
                                    std::vector<ChannelParams>  sessionsAtTime;

                                    std::cerr << "Search time = " << time  << std::endl;
                                    if( dbWorker->sendSelectSessionByEndTime(time - 3, curPkd.channel,sessionsAtTime) >= 0 )
                                    {
                                        if( sessionsAtTime.size() > 0 )
                                        {
                                            for(auto it : sessionsAtTime )
                                            {
                                                currentChannel->id = it.id;
                                                currentChannel->channel = it.channel;
                                                currentChannel->endTime = it.endTime;
                                                currentChannel->isSend = false;
                                                currentChannel->isWrited = true;
                                                currentChannel->startTime = it.startTime;
                                                currentChannel->startAddress = it.startAddress;
                                                currentChannel->sessionByteShift = it.sessionByteShift;
                                                currentChannel->isRecordAlready = true;
                                                currentChannel->alreadyRecordByteShift = it.sessionByteShift;
                                                currentChannel->packetsCount = it.packetsCount;
                                                std::cerr << "Start time = " << it.startTime << " End Time = " << it.endTime << std::endl;
                                            }
                                        }
                                    }
                                    currentChannel->isOnRecordSessionChecked = true;
                                }
                                if(!parsePKD(currentChannel, curPkd, packets, STEREO, 0))
                                {
                                    errorParseCount++;
                                }
                                else
                                {
                                    errorParseCountGroups = 0;
                                }
                            }
                            else
                            {
                                try{
                                    currentChannel = &channelParams.at(static_cast<int>(curPkd.channel));
                                }
                                catch(std::exception &except){
                                    packets++;
                                    logger->Message(LOG_ERR, __LINE__, __func__, "Channel not found error\n");
                                    continue;
                                }
                                if( !currentChannel )
                                {
                                    packets++;
                                    logger->Message(LOG_ERR, __LINE__, __func__, "Channel not found error\n");
                                    continue;
                                }

                                if( !currentChannel->isOnRecordSessionChecked )
                                {
                                    std::cerr << "Check on record session in db MONO " << curPkd.channel << std::endl;
                                    uint64_t time = 0;
                                    time = curPkd.ltime | curPkd.htime << 16;
                                    std::vector<ChannelParams>  sessionsAtTime;

                                    std::cerr << "Search time = " << time  << std::endl;
                                    if( dbWorker->sendSelectSessionByEndTime(time - 3, curPkd.channel,sessionsAtTime) >= 0 )
                                    {
                                        if( sessionsAtTime.size() > 0 )
                                        {
                                            for(auto it : sessionsAtTime )
                                            {
                                                currentChannel->id = it.id;
                                                currentChannel->channel = it.channel;
                                                currentChannel->endTime = it.endTime;
                                                currentChannel->isSend = false;
                                                currentChannel->isWrited = true;
                                                currentChannel->startTime = it.startTime;
                                                currentChannel->startAddress = it.startAddress;
                                                currentChannel->sessionByteShift = it.sessionByteShift;
                                                currentChannel->isRecordAlready = true;
                                                currentChannel->alreadyRecordByteShift = it.sessionByteShift;
                                                currentChannel->packetsCount = it.packetsCount;
                                                std::cerr << "Start time = " << it.startTime << " End Time = " << it.endTime << std::endl;
                                            }
                                        }
                                    }
                                    currentChannel->isOnRecordSessionChecked = true;
                                }
                                if( !parsePKD(currentChannel, curPkd, packets, MONO, 0) )
                                {
                                    errorParseCount++;
                                }
                                else
                                {
                                    errorParseCountGroups = 0;
                                }
                            }
                        }
                        else if( curPkd.channelId == channelInfo )
                        {
                            allInfoPackets++;
                            test1 = 2;
                        }
                    }
                    else
                    {
                        allTrashPackets++;
                    }

                    if( result == -1 )
                    {
                        if(!isRingFirstPartReaded && allReaded < recordBytesUsed )
                        {
                            allEnvelop++;
                            test1 = 3;
                        }
                    }
                    else if( result == -2 )
                    {
                        if(!isRingFirstPartReaded && allReaded < recordBytesUsed)
                        allNonAudio++;
                        test1 = 4;
                    }
                    else if( result == -3 && allReaded < recordBytesUsed)
                    {
                        if(!isRingFirstPartReaded)
                        allTimeError++;
                        test1 = 5;
                    }

                    allPackets++;
                    packets++;
                }
            }

            if( errorParseCount > 3000 )
            {
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error Multiple trash packets groups %li %i" ,allReaded,  errorParseCountGroups);
                errorParseCountGroups++;
                errorParseCount = 0;
            }

            if( errorParseCountGroups > 10000 )
            {
                threadWork = false;
                isWork = false;
                errorRead = 0;

                dbWorker->endTransaction();

                if( fpOut )
                {
                    fclose(fpOut);
                    fpOut = nullptr;
                }

                //sync();
                if( fp )
                {
                    fclose(fp);
                    fp = nullptr;
                }
                this->logger->Message(rosslog::LOG_ERR, __LINE__, __func__, "Error Multiple trash packets 2" );

                fileInterface->fileLoadingFinished(fileIndex, true,
                                                   ErrorMessageFormat::formatString("Ошибка обработки данных.", Y4C_PARSE_ERROR_MULTIPLE_TRASH_PACKS, errno));
                return;
            }
            //errorParseCount = 0;

            allReaded += readed ;//* sizeof (pkd_t);


            if(do4itka)
            {
                allReaded = 0;
                //Закрываем текущую сессию и ставим флаги на начало новой
                updateCurrentSessionsData(channelParams);
                packets = 0;
            }

            sessionByteShift += readed ;//* sizeof (pkd_t);


            progressCounter += readed;
            progressIt++;
            if(progressIt%2000 == 0)
            {
                float progressTarget = 0;

                switch( parseType )
                {
                    case RING_FIRST_TIME:
                    case RING_CONTINUE_FULL:
                    {
                        progressTarget = recordCapacity/64;
                        break;
                    }
                    case SIMPLE_FIRST_TIME:
                    {
                        progressTarget = pkdUsed;
                        break;
                    }
                    case RING_CONTINUE_TILL:
                    case SIMPLE_CONTINUE:
                    {
                        progressTarget = pkdUsed - lastParsed;
                        break;
                    }
                    case RING_CONTINUE_FULL_FROM_LAST:
                    {
                        progressTarget = recordCapacity/64 - lastParsed + pkdUsed;
                        break;
                    }
                }

                progressProcent = progressCounter / progressTarget * 100;
                fileInterface->fileParseProcessState(progressProcent);
                std::cerr << "progress = " << progressProcent << std::endl;
            }

        }
    }

    if( imidiatlyStop )
    {
        dbWorker->endTransaction();

        if( fpOut )
        {
            fclose(fpOut);
        }
        if( fp != nullptr )
            fclose(fp);
        fileInterface->fileLoadingFinished(fileIndex, true, "Принудительная остановка обработки данных.");
        return;
    }
    updateCurrentSessionsData(channelParams);

    dbWorker->endTransaction();
    end = std::chrono::steady_clock::now();
    std::cout << "READED = " << allReaded << std::endl;
    std::cerr << "ALL = " << sessionByteShift << std::endl;
    std::cerr << "All audio packets " << allAudioPackets << std::endl;
    std::cerr << "All packets " << allPackets << std::endl;
    std::cerr << "All info pack " << allInfoPackets << std::endl;
    std::cerr << "All trash pack " << allTrashPackets << std::endl;
    std::cerr << "All envelop pack " << allEnvelop << std::endl;
    std::cerr << "All time pack " << allTimeError << std::endl;
    std::cerr << "All non audio pack " << allNonAudio << std::endl;


    std::cout << "WRITED = " << writed << std::endl;
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "[ms]" << std::endl;
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() << "[ns]" << std::endl;

    if( fpOut )
    {
        fclose(fpOut);
        fpOut = nullptr;

    }


//    if( allAudioPackets == 0 && pkdUsed > 1024 ) //Check if not empty
//    {
//        sync();
//        if( fp != nullptr )
//            fclose(fp);
//        fileInterface->fileLoadingFinished(fileIndex, true);
//        return;
//    }
    dbWorker->startTransaction();


//    if( isFirstTime )
    if( !haveYantraData )
    {
        int ret = dbWorker->sendYantraData(this->recordBytesUsed, pkdUsed, isRing, dwTime, rCounter, (packets * sizeof(pkd_t)));
        if( ret < 0 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Can't insert parse data in yantra_data table");
        }
    }
    else
    {
        int ret = dbWorker->updateYantraData(this->recordBytesUsed, pkdUsed, isRing, dwTime, rCounter, (packets * sizeof(pkd_t)));
        if( ret < 0 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Can't update parse data in yantra_data table");
        }
    }

    dbWorker->endTransaction();

    logger->Message(LOG_INFO, __LINE__, __func__, "Parse finished with success");

    sync();
    if( fp != nullptr )
    {
        fclose(fp);
        fp = nullptr;
    }
    fileInterface->fileLoadingFinished(fileIndex, false, "");
    return ;
}


bool FilePKDWorker::parsePKD(ChannelParams *currentChannel, pkd_t pkd, size_t offset, PKDType type, uint32_t sessionByteShift)
{

//    std::cout << "parsePKD()" << std::endl;
//    std::cout << "offset = " << offset << std::endl;

    uint64_t time = 0;
    int channel = 0;
    int ret;
    uint16_t peakR = 0;
    uint16_t peakL = 0;
    uint16_t unPackTable[256];
    char cShift;

    for(int j = 0; j < 256; j++)
    {
        cShift = (uint8_t)(11 - (j >> 4));
        if(cShift >= 0)
        {
            unPackTable[j] =((j & 0x0f) << cShift);
        }
        else
        {
            unPackTable[j] = ((j & 0x0f) >> cShift);
        }
    }


    //printf("LOG PEAK l = %x r = %x\n", pkd.logPeakL, pkd.logPeakR);
    channel = pkd.channel;
    time = pkd.ltime | pkd.htime << 16;

//    peak |= pkd.logPeakL & 0xff;
//    peak <<= 8;
//    peak |= pkd.logPeakR & 0xff;
    peakR = unPackTable[pkd.logPeakR];
    peakL = unPackTable[pkd.logPeakL];

    //printf("PKD l = %i, r = %i, table l = %i, table r = %i\n", pkd.logPeakL, pkd.logPeakR, peakL, peakR);

    //printf("PeakR = %x, PeakL = %x\n", peakR, peakL);

    if( !currentChannel->isWrited )
    {
        currentChannel->startTime = time;
        currentChannel->endTime = time;
        currentChannel->isWrited = true;
        currentChannel->packetsCount = 0;
        currentChannel->packetsCount++;
        currentChannel->channel = channel;
        currentChannel->pkdR = peakR;
        currentChannel->pkdL = peakL;
        currentChannel->midPkdL = 0;
        currentChannel->midPkdR = 0;
        currentChannel->pkdCount = 0;
        currentChannel->sessionByteShift = offset * sizeof(pkd_t);
        currentChannel->pkdShift = offset * sizeof(pkd_t); //TODO NEW
        currentChannel->startAddress = offset * sizeof(pkd_t);
        currentChannel->isSend = false;

        ChannelParams retParams;

        logger->Message(LOG_ERR, __LINE__, __func__, "Insert new 1 start time %li end time %li offset %li startAddress %li byte shift %lli\n",
                        currentChannel->startTime, currentChannel->endTime, offset, currentChannel->startAddress, currentChannel->sessionByteShift);
        ret = dbWorker->sendInsertCommand(time, time, channel, type, currentChannel->startAddress, retParams, currentChannel->packetsCount );
        currentChannel->id = ret;


        if( ret <= 0 )
        {
            logger->Message(LOG_ERR, __LINE__, __func__, "Error while insert data\n");
            return false;
        }
    }
    else
    {
        if( time > currentChannel->startTime && time < currentChannel->endTime )
        {
//            logger->Message(LOG_ERR, __LINE__, __func__, "Trash packet skip offset = %i time start = %li, time end = %li, time = %li",
//                            offset, currentChannel->startTime, currentChannel->endTime, time);
            return false;
        }
        else if( currentChannel->startTime <= time && currentChannel->endTime + 1 == time  )
        {
            //Сессия + 1 сек обновляем конец времени и считаем среднюю пкд для этого времени
            currentChannel->endTime = time;
            currentChannel->pkdCount++;
            currentChannel->midPkdR += peakR;
            currentChannel->midPkdL += peakL;
            currentChannel->packetsCount++;

//            printf("mid R = %x, mid L = %x count = %i\n", currentChannel->midPkdR, currentChannel->midPkdL, currentChannel->pkdCount);
//            printf("Peak ready R = %x\n", (currentChannel->midPkdR / currentChannel->pkdCount));
//            printf("Peak ready L = %x\n", (currentChannel->midPkdL / currentChannel->pkdCount));

            uint16_t midPkdReadyR = currentChannel->midPkdR;
            uint16_t midPkdReadyL = currentChannel->midPkdL;
            if( currentChannel->pkdCount > 0 )
            {
                midPkdReadyR = currentChannel->midPkdR / currentChannel->pkdCount;
                midPkdReadyL = currentChannel->midPkdL / currentChannel->pkdCount;
            }

//            printf("Peak ready R = %i\n", midPkdReadyR);
//            printf("Peak ready L = %i\n", midPkdReadyL);
            int byteShift = currentChannel->pkdShift; // 1 pkd = 8 byte

            uint32_t peak = 0;
            peak |= midPkdReadyL;
            peak <<= 16;
            peak |= midPkdReadyR;


            if( currentChannel->isRecordAlready )
            {
                currentChannel->sessionByteShift = currentChannel->alreadyRecordByteShift + offset * sizeof(pkd_t);
                currentChannel->pkdShift = currentChannel->alreadyRecordByteShift + offset * sizeof(pkd_t);
            }
            else
            {
                currentChannel->pkdShift = offset * sizeof(pkd_t);
                currentChannel->sessionByteShift = offset * sizeof(pkd_t);

            }
            ChannelPkd channelPkd;

            channelPkd.mid = peak;
            channelPkd.endTime = currentChannel->endTime;
            channelPkd.bytesShift = byteShift;


//            std::cerr << "PKD ON SECOND SHIFT = " << byteShift << " SESSION SHIFT = "
//                      << currentChannel->sessionByteShift  << " Time = " << time << std::endl;

            currentChannel->pkds.push_back(channelPkd);

            if( currentChannel->pkds.size() > 50 ) //TODO ADD LIMIT FROM DEFINE < 500
            {
                for( auto it: currentChannel->pkds )
                {
                    dbWorker->sendInsertPKDCommand(currentChannel->id, it.mid, it.endTime, it.bytesShift);
                }
                currentChannel->pkds.clear();
            }

            currentChannel->midPkdR = 0;
            currentChannel->midPkdL = 0;
            currentChannel->pkdCount = 0;

        }
        else if (currentChannel->startTime <= time && currentChannel->endTime  == time)
        {

            currentChannel->endTime = time;
            currentChannel->midPkdR += peakR;
            currentChannel->midPkdL += peakL;
            currentChannel->packetsCount++;

            //currentChannel->pkdShift = sessionByteShift + index;
            currentChannel->pkdCount++;
            if( currentChannel->isRecordAlready )
            {
                currentChannel->sessionByteShift = currentChannel->alreadyRecordByteShift + offset * sizeof(pkd_t);
            }
            else
            {
                currentChannel->sessionByteShift = offset * sizeof(pkd_t);

            }
            //std::cerr << "ON PKD SAME SEC SHIFT = " << currentChannel->pkdShift << " SESSION SHIFT = " << currentChannel->sessionByteShift << " index " << index << std::endl;
        }
        else
        {
            //Делаем инсерт пакетов, предыдущей сессии
            uint16_t midPkdReadyR = currentChannel->midPkdR;
            uint16_t midPkdReadyL = currentChannel->midPkdL;
            if( currentChannel->pkdCount > 0 )
            {
                midPkdReadyR = currentChannel->midPkdR / currentChannel->pkdCount;
                midPkdReadyL = currentChannel->midPkdL / currentChannel->pkdCount;
            }

            uint16_t peak = 0;
            peak |= midPkdReadyL;
            peak <<= 16;
            peak |= midPkdReadyR;
            int byteShift = currentChannel->pkdShift;//currentChannel->pkdCount * 8;

            ChannelPkd channelPkd;

            channelPkd.mid = peak;
            channelPkd.endTime = currentChannel->endTime;
            channelPkd.bytesShift = byteShift;

            currentChannel->pkds.push_back(channelPkd);
            currentChannel->isSend = true;

            for( auto it: currentChannel->pkds )
            {
                dbWorker->sendInsertPKDCommand(currentChannel->id, it.mid, it.endTime, it.bytesShift);
            }

            //Обновляем окончание времени сессии

            dbWorker->sendUpdateCommand(currentChannel->id, currentChannel->startTime, currentChannel->endTime,
                                        currentChannel->channel, currentChannel->sessionByteShift , currentChannel->packetsCount);
            logger->Message(LOG_ERR, __LINE__, __func__, "UPDATE  start time %li end time %li\n", currentChannel->startTime, currentChannel->endTime);


            logger->Message(LOG_ERR, __LINE__, __func__, "New session inserting \n");

            //Добавляем новую сессию
            currentChannel->startTime = time;
            currentChannel->endTime = time;
            currentChannel->isWrited = true;
            currentChannel->packetsCount = 0;
            currentChannel->packetsCount++;
            currentChannel->channel = channel;
            currentChannel->pkdR = peakR;
            currentChannel->pkdL = peakL;
            currentChannel->sessionByteShift = offset * sizeof(pkd_t);
            currentChannel->isSend = false;
            currentChannel->startAddress = offset * sizeof(pkd_t);
            currentChannel->pkdShift = offset * sizeof(pkd_t);

            ChannelParams retParams;

            logger->Message(LOG_ERR, __LINE__, __func__, "Insert new start time %li end time %li startAddress %li byte shift %li pkds size prev %li\n",
                            currentChannel->startTime, currentChannel->endTime, currentChannel->startAddress, currentChannel->sessionByteShift,
                            currentChannel->pkds.size());
            currentChannel->id = dbWorker->sendInsertCommand(currentChannel->startTime, currentChannel->endTime,
                                                             currentChannel->channel, type, currentChannel->startAddress, retParams, currentChannel->packetsCount);

            currentChannel->midPkdR = 0;
            currentChannel->midPkdL = 0;
            currentChannel->pkdCount = 0;
            currentChannel->pkds.clear();

        }
    }

    return true;
}

void FilePKDWorker::updateCurrentSessionsData(std::vector<ChannelParams> &channelParams)
{
    for( auto &it: channelParams )
    {
        if( !it.isSend )
        {
            //Обновляем последнюю доступную сессию
            dbWorker->sendUpdateCommand(it.id, it.startTime, it.endTime,
                                        it.channel, it.sessionByteShift, it.packetsCount);

            logger->Message(LOG_ERR, __LINE__, __func__, "UPDATE 2 session id %i start time %li end time %li  size %i byteShift = %i\n", it.id,
                            it.startTime, it.endTime, it.pkds.size(), it.sessionByteShift);

            //Добавляем пкд последней оставшейся сессии
            for( auto itPkds: it.pkds )
            {
                dbWorker->sendInsertPKDCommand(it.id, itPkds.mid, itPkds.endTime, itPkds.bytesShift);
            }
            it.pkds.clear();
            it.isSend = true;
            it.isWrited = false;
        }
    }
}

int FilePKDWorker::checkPKDHeader(pkd_t pkd)
{
    if( pkd.headSector )
        return -4;

    if ( !pkd.isEnvelope )
        return -1;

    if ( pkd.channelId != channelAudio && pkd.channelId != channelInfo )
        return -2;

    if ( !pkd.htime && !pkd.ltime )
        return -3;

    return true;
}
