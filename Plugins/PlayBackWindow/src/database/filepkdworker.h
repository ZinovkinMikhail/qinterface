//
// Created by ivan on 09.11.2020.
//

#ifndef CM_INTERFACE_FILEPKDWORKER_H
#define CM_INTERFACE_FILEPKDWORKER_H

#include <thread>
#include <memory>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <ios>

#include <QDateTime>
#include <QFile>
#include <QTextStream>

#include <rosslog/log.h>

#include "databasepkdworker.h"
#include "channelparamsmodel.h"

#include "ifilepkdworker.h"

#include <errormessageformat.h>

typedef enum
{
    RING_FIRST_TIME,
    RING_CONTINUE_FULL,
    RING_CONTINUE_TILL,
    RING_CONTINUE_FULL_FROM_LAST,
    SIMPLE_FIRST_TIME,
    SIMPLE_CONTINUE
} ParseType;

enum typeChannel {
    channelAudio = 0xA,
    channelInfo  = 0xC
};

typedef union {
    struct {
        uint8_t channelId    :4;
        uint8_t isStereo     :1;
        uint8_t isMonitoring :1;
        uint8_t isEnvelope   :1;
        uint8_t headSector   :1;
        uint8_t channel      :8;

        uint8_t logPeakL     :8;
        uint8_t logPeakR     :8;

        uint16_t htime       :16;
        uint16_t ltime       :16;
    };
    uint64_t firstHalf;
    uint64_t lastHalf;
} pkd_t;

#define FILE_PART_PARSE_SIZE 512
#define PKD_BLOCKS_IN FILE_PART_PARSE_SIZE / sizeof(pkd_t)
//#define PKD_FULL_SIZE 978845696
//#define PKD_FULL_SIZE 131072
//#define PKD_FULL_SIZE 524288 // = Total capacity/64
#define TIME_THRESHOLD 1
#define READ_ERROR_COUNT 10

class FilePKDWorker
{
public:
    explicit FilePKDWorker(std::shared_ptr<rosslog::Log> log, std::shared_ptr<DatabasePKDWorker> databasePKDWorker,
                           std::shared_ptr<IFIlePKDWorker> fileInterface);

    bool startFilePartParse(std::string filePath, size_t recordCapacity,
                            size_t startPos, size_t endPos, bool isRing, bool isFirst,
                            size_t recordBytesUsed, int index, uint32_t dwTime,
                            bool haveYantraData, int rCounter, ParseType parseType);
    uint64_t getCurPKDParseTime(std::string pkdFilePath, uint64_t pkdPos, uint64_t flashCapacity, uint16_t &spacing);

    ~FilePKDWorker();
private:
    void threadFileFunction();
    int checkPKDHeader(pkd_t pkd);
    bool parsePKD(ChannelParams *currentChannel, pkd_t pkd, size_t offset, PKDType type, uint32_t sessionByteShift);
    void updateCurrentSessionsData(std::vector<ChannelParams> &channelParams);

    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<DatabasePKDWorker> dbWorker;
    std::thread channelThread;
    bool threadWork;
    QFile *inputFile;
    std::vector<ChannelParams> channelParams;
    size_t fileReadSize;
    std::shared_ptr<IFIlePKDWorker> fileInterface;
    int fileIndex;
    uint64_t currentDate;
    uint64_t lowPossibleYear;
    std::string filePath;
    std::ifstream disk;
    FILE *fp;
    size_t recordCapacity;
    size_t pkdUsed;
    size_t lastParsed;
    size_t recordBytesUsed;
    uint64_t packets;
    bool isRing;
    bool isFirstTime;
    bool isRingFirstPartReaded;
    uint32_t dwTime;
    bool haveYantraData;
    int rCounter;
    ParseType parseType;
    int errorParseCount {0};
    int errorParseCountGroups{0};

    bool imidiatlyStop{false};
};


#endif //CM_INTERFACE_FILEPKDWORKER_H
