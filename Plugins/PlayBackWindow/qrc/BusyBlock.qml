import QtQuick 2.9
import QtQuick.Controls 2.2


Button
{
    id:busyDisplay
    z: 20

    property var rectSize: 100
    property var dotSize: 10
    property bool blockButton: true

    height: rectSize
    width: rectSize

    background: Rectangle
        {
            color: blockButton ? "#6c7980" : "transparent"
            opacity: 0.5
        }

    onBlockButtonChanged: blockButton ? enabled = true : enabled = false



    BusyIndicator
    {
        id: control
        anchors.centerIn: parent
        visible: busyDisplay.visible
        running: busyDisplay.visible

        contentItem: Item {
            implicitWidth: rectSize
            implicitHeight: rectSize

            Item {
                id: item1
                width: rectSize
                height: rectSize
                opacity: control.running ? 1 : 0

                Behavior on opacity {
                    OpacityAnimator {
                        duration: 500
                    }
                }

                RotationAnimator {
                    target: item1
                    running: control.visible && control.running
                    from: 0
                    to: 360
                    loops: Animation.Infinite
                    duration: 3000
                }

                Repeater {
                    id: repeater
                    model: 6

                    Rectangle {
                        x: item1.width / 2 - width / 2
                        y: item1.height / 2 - height / 2
                        implicitWidth: dotSize
                        implicitHeight: dotSize
                        radius: implicitWidth/2
                        color: "#fcfff5"
                        transform: [
                            Translate {
                                y: -Math.min(item1.width, item1.height) * 0.5 + 5
                            },
                            Rotation {
                                angle: index / repeater.count * 360
                                origin.x: dotSize/2
                                origin.y: dotSize/2
                            }
                        ]
                    }
                }
            }
        }
    }
}
