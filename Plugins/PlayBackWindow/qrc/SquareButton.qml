import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Button
{
    height: 65
    width: 75
    scale: down ? 0.95 : 1

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}


    property string img
    property bool activeState: false


    background: Rectangle
    {
        radius: 2
        color: cc.blue

    }


    ShadowRect{type: "panel"; opacity: down ? 0 : 1}


    Image
    {
        height: 40
        width: height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
//        mipmap: true
        source: img
    }

    Rectangle
    {
        id:highlight
        anchors.fill: parent
        color: cc.light_green
        opacity: 0.4
        visible: activeState
    }
}


