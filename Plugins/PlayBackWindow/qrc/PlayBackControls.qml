import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Item
{
    id: pleerControls
    height: 110
    width: 614

    property real defaultHeight: 110
    property bool bigSize: height > defaultHeight ? true : false


    onBigSizeChanged:
    {
        if(bigSize)
        {
            blockButton.enabled = false
            playerSizeAnimation.makeBig = true
            playerSizeAnimation.start()
        }
        else
        {
            blockButton.enabled = true
            playerSizeAnimation.makeBig = false
            playerSizeAnimation.start()
        }
    }


    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal controlsButtonPressed(string name)
    signal setSliderSeekPos(var pos)
    signal swipeSelectedSignal()
    signal swipeDeselectedSignal()
    signal showMeParent(string name, var item)
    signal makeFullSizedControll()

    function swipeSelected()
    {
        pleerControls.swipeSelectedSignal()
    }

    function swipeDeselected()
    {
        pleerControls.swipeDeselectedSignal()
    }


    function setNewData(current, count, duration, length, startDateTime,endDateTime)
    {
        countText.text = "(" + current + " / " + count + ")"
        lengthText.text = length
        elapsedText.text = duration
        elapsedTextDateTime.text = startDateTime
        lengthTextDateTime.text = endDateTime
    }


    function buttonClicked(name)
    {
        pleerControls.controlsButtonPressed(name)
    }


    Button
    {
        id: antihide
        anchors.fill: parent
        background: Rectangle { opacity: 0 }
    }


    Component.onCompleted:
    {
        var component = Qt.createComponent("PlayerButton.qml");
        component.createObject(row1, {"parentRow" : pleerControls, "name": "PreviousButt",
                                   "icon": "qrc:///PlayBackWindow/icons/i17.png", "rotate": "true"});
        component.createObject(row1, {"parentRow" : pleerControls, "name": "RepeatButt",
                                   "icon": "qrc:///PlayBackWindow/icons/loop.png",
                                   "iconSecondState" : "qrc:///PlayBackWindow/icons/loopgreen.png"});
        component.createObject(row1, {"parentRow" : pleerControls, "name": "SeekBackButt",
                                   "icon": "qrc:///PlayBackWindow/icons/i15.png", "rotate": "true"});
        component.createObject(row1, {"parentRow" : pleerControls, "name": "StopButt",
                                   "icon": "qrc:///PlayBackWindow/icons/i14.png"});
        component.createObject(row1, {"parentRow" : pleerControls, "name": "PlayButt",
                                   "icon": "qrc:///PlayBackWindow/icons/i13.png",
                                   "iconSecondState" : "qrc:///PlayBackWindow/icons/pause.png"});
        component.createObject(row1, {"parentRow" : pleerControls, "name": "SeekForwartButt",
                                   "icon": "qrc:///PlayBackWindow/icons/i15.png"});
        component.createObject(row1, {"parentRow" : pleerControls, "name": "NextButt",
                                   "icon": "qrc:///PlayBackWindow/icons/i17.png"});
    }


    Row
    {
        objectName: "ButtonsRow"
        id: row1
        x: parent.width/2 - width/2
        y:7
        spacing: 25
    }



    Text
    {
        id: elapsedText
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 4
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S7
        color: cc.white
        text: qsTr("00:01:01")
    }

    Text
    {
        id: elapsedTextDateTime
        anchors.left: elapsedText.right
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 4
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S7
        color: cc.white
        text: qsTr("")
    }

    Text
    {
        id: countText
        anchors.right: lengthText.left
        anchors.rightMargin: 30
        anchors.verticalCenter: lengthText.verticalCenter
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S7
        color: cc.white
        text: qsTr("")
    }

    Text
    {
        id: lengthText
        anchors.right: lengthTextDateTime.left
        anchors.rightMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 4
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S7
        color: cc.white
        text: qsTr("00:01:50")
    }

    Text
    {
        id: lengthTextDateTime
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 4
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S7
        color: cc.white
        text: qsTr("")
    }



    Slider
    {
        id: sliderElem
        objectName: "SliderItem"
        x: parent.width/2 - width/2
        y: parent.height - 47 - yShift
        value: 0
        from: 0.0
        to: 100.0

        property var yShift: 0
        property var pointSize: 0
        property bool wasPress: false
        property int valToSeek: 0

        onValueChanged:
        {
//            console.debug(sliderElem.value)
            if( sliderElem.pressed )
            {
                valToSeek = value
                if( !wasPress )
                    wasPress = !wasPress
            }
            else
            {
                if( wasPress )
                {
                    wasPress = false
                    pleerControls.setSliderSeekPos(valToSeek)
                }
            }
        }
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: 26 + yShift

        background: Rectangle
        {
            x: sliderElem.leftPadding
            y: sliderElem.topPadding + sliderElem.availableHeight / 2 - height / 2
            implicitWidth: 510
            implicitHeight: 2
            width: sliderElem.availableWidth
            height: implicitHeight
            color: cc.dark_blue
        }

        handle: Rectangle
        {
            x: sliderElem.leftPadding + sliderElem.visualPosition * (sliderElem.availableWidth - width)
            y: sliderElem.topPadding + sliderElem.availableHeight / 2 - height / 2
            implicitWidth: 20 + sliderElem.pointSize
            implicitHeight: 20 + sliderElem.pointSize
            radius: implicitWidth/2
            color: cc.light_green
            scale:   sliderElem.pressed ? 0.7 : 1
        }
    }



    Button
    {
        id: blockButton
        anchors.fill: parent

        background: Rectangle { opacity: 0 }

        onClicked: makeFullSizedControll()
    }



    ParallelAnimation
    {
        id: playerSizeAnimation

        property bool makeBig


        NumberAnimation
        {
            target: sliderElem
            property: "pointSize"
            duration: 200
            easing.type: Easing.InElastic;
            easing.amplitude: 2.0;
            easing.period: 1.5
            from: playerSizeAnimation.makeBig ? 0 : 12
            to : playerSizeAnimation.makeBig ? 12 : 0
        }
    }


}
















