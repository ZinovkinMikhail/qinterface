import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.1
import Cicada 1.0
import "."

Rectangle {
    id: chart
    color: "transparent"

    property var stockModel: null
    property var newPoints: null
    property var newSeparatorPoints: null
    property var separatorObjects: []
    property var separatorHighlightObj: null
    property string activeChart: "week"
    property var settings
    property int gridSize: 4
    property real gridStep: gridSize ? (canvas.width - canvas.tickMargin) / gridSize : canvas.xGridStep
    property int zoom: 100
    property int moveVal: 0
    property bool preparePoints: false
    property bool isDrawing: false
    property bool drawTrueData: false
    property real timeIndicatorPos: greenZone.timeIndicatorPos
    property bool timeIndicatorVisible: true

    z: 2
    signal zoomApplyed(var zoomProc)

    function changeZoom(val)
    {
        var proc = val * 100 / 20
        zoom = proc
        moveVal = 0
        canvas.requestPaint();
    }

    function changeZoomProc(val, move)
    {
        moveVal = move
        zoom = val
        drawTrueData = false
        canvas.requestPaint();
    }

    function highlightCurrentSession(proc, procEnd)
    {
        if( timeIndicatorVisible )
        {
            return
        }

        if(separatorHighlightObj != undefined)
            separatorHighlightObj.destroy()

        if( proc == 0 && procEnd == 0 )
        {
            return
        }
        else
        {
             var startX = 0;
             startX = proc * 1120 / 100;
             var endX = 0;
             endX = procEnd * 1120 / 100
             console.debug(startX)
             console.debug(endX)
             separatorHighlightObj = highlightComponent.createObject(greenZone, {"x": startX , "width": endX - startX });
        }
    }

    function updatePoints(data, zoomProc, separatorPoints)
    {
//        console.debug("xGridStep " + canvas.xGridStep);
//        console.debug("yGridOffset " + canvas.yGridOffset);
//        console.debug("yGridStep " + canvas.yGridStep);
//        console.debug("canvas.width " + canvas.width);
//        console.debug("canvas.height " + canvas.height);

        zoomApplyed(zoomProc)
        newPoints = data
        newSeparatorPoints = separatorPoints
        zoom = zoomProc
        moveVal = 0
        preparePoints = true;
        isDrawing = false;
        drawTrueData = true;
        canvas.requestPaint();







        var ff = zoom * 30000 / 100;

        if( ff > newPoints.length )
        {
            ff = newPoints.length;
        }

        var moveVV = moveVal * 30000 / 100

        if( drawTrueData )
        {
            ff = newPoints.length
            moveVV = 0;
        }


        canvas.xGridStep = 1120 / (ff - 1)


        //make separator >>>
        for(var i=0; i < separatorObjects.length; i++)
        {
            separatorObjects[i].destroy();
        }

        separatorObjects = [];

        if( drawTrueData )
        {
            var lastLinePos = 0;
            for(var i = 0; i < newSeparatorPoints.length; i++)
            {
                var curLinePos = (newSeparatorPoints[i] * canvas.xGridStep) - canvas.xGridStep - 2;

                if(curLinePos < 0 || curLinePos > canvas.width )
                {
                    continue
                }


                if( lastLinePos + 2 > curLinePos )
                    continue

                lastLinePos = curLinePos
                var obj = highlightComponent.createObject(chart, {"x": curLinePos});
                separatorObjects.push(obj);
            }

            if( timeIndicatorVisible )
            {
                updateSeparatorHighlight()
            }
        }
    }


    function movePos(val)
    {
        moveVal = val
        canvas.requestPaint();
    }


    function updateSeparatorHighlight()
    {
        if(separatorHighlightObj != undefined)
            separatorHighlightObj.destroy()

        if(separatorObjects.length == 0)
        {
            if((timeIndicatorPos - 80) >= 0 && (timeIndicatorPos - 80) <= 1120)
            {
                separatorHighlightObj = highlightComponent.createObject(greenZone, {"width": parent.width});
                console.debug("+++++ add ")
            }
        }
        else if(separatorObjects[separatorObjects.length - 1].x < timeIndicatorPos - 80)
        {
            separatorHighlightObj = highlightComponent.createObject(greenZone, {"x": separatorObjects[separatorObjects.length - 1].x, "width": parent.width - separatorObjects[separatorObjects.length - 1].x});
        }
        else
        {
            for(var i = 0; i < separatorObjects.length; i++)
            {
                if(separatorObjects[i].x > timeIndicatorPos - 80)
                {
                    if(i === 0)
                    {
                        separatorHighlightObj = highlightComponent.createObject(greenZone, {"width": separatorObjects[i].x});
                        break
                    }
                    else
                    {
                        separatorHighlightObj = highlightComponent.createObject(greenZone, {"x": separatorObjects[i - 1].x,"width": separatorObjects[i].x - separatorObjects[i-1].x});
                        break
                    }
                }
            }
        }
//        console.debug("+++++ end check +++++")
    }

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    onTimeIndicatorPosChanged:
    {
        console.debug(timeIndicatorPos)
        if( timeIndicatorVisible )
        {
            updateSeparatorHighlight()
        }

    }

    Column
    {
        anchors.fill: parent

        Canvas
        {
            id: canvas
            width: parent.width
            height: greenZone.height
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 6

            property int pixelSkip: 1
            property int numPoints: 1
            property int tickMargin: 34 // 34

            property real xGridStep: (canvas.width - tickMargin) / 50 //numPOints
            property real yGridOffset: canvas.height / 26
            property real yGridStep: canvas.height / 12 //12


            property int zeroX: 0
            property int lastPos: 0

            function drawPrice(ctx, from, to, color, price, points, highest, lowest)
            {
                ctx.save();
                ctx.globalAlpha = 0.7;
                ctx.strokeStyle = cc.dark_blue;

//                console.debug("Num points = " + numPoints)
                ctx.lineWidth = numPoints > 200 ? 1 : 3
                ctx.beginPath();


                var end = points.length;

                var range = highest - lowest;
                if (range == 0)
                {
                    range = 1;
                }

                for (var i = from; i < to; i += pixelSkip)
                {

                    var x = points[i].x;
                    var y = points[i][price];
                    var h = 9 * yGridStep;

//                    console.debug("Y2 = " + y)

                    y = h * (lowest - y) / range + h + yGridOffset;

//                    console.debug("Y = " + y)
//                    console.debug("lowest = " + lowest)
//                    console.debug("h = " + h)
//                    console.debug("range = " + range)
//                    console.debug("yGridOffset = " + yGridOffset)
                    if( i === 0 )
                    {
                        zeroX = x;
                    }

                    if (i === from)
                    {
                        ctx.moveTo(zeroX, y);
                    }
                    else
                    {
                        ctx.lineTo(x, y);
                    }
                }
                ctx.stroke();
                ctx.restore();
            }

            property var points : []
            onPaint:
            {
                if( zoom <= 0 )
                    zoom = 1
                if( zoom > 100 )
                    zoom = 100

                if( isDrawing )
                    return

                isDrawing = true

                var ff = zoom * 30000 / 100;

                console.debug("Zoom " + zoom);
//                console.debug("ff " + ff);



                if( ff > newPoints.length )
                {
                    ff = newPoints.length;
                }

                var moveVV = moveVal * 30000 / 100

                if( drawTrueData )
                {
                    ff = newPoints.length
                    moveVV = 0;
                }

                numPoints = ff //newPoints.length

                if(zoom < 100 && !drawTrueData)
                {
                    moveVV = newPoints.length - ff
                    moveVV = moveVal * moveVV / 100.0
                }
                else
                {
                    moveVV = 0;
                }

                xGridStep = 1120 / (ff - 1)


                var ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.globalCompositeOperation = "source-over";
                ctx.lineWidth = 2;

                var highestPrice = 0;
                var lowestPrice = 65000;

                points = [];
                for (var i = Math.round( moveVV ), j = 0; i < moveVV + numPoints; i ++, j ++)
                {

                    var price = newPoints[i];
                    if( highestPrice < price )
                        highestPrice = price

                    if( lowestPrice > price )
                        lowestPrice = price


                    points.push({
                                    x: j * xGridStep,
                                    price: price
                                });
                }

                var all = newPoints.length
                drawPrice(ctx,  0,  ff , "red", "price", points, highestPrice, lowestPrice);//ff - 1

                drawTrueData = false
                isDrawing = false
            }
        }
    }



    Component
    {
        id: highlightComponent;

        Rectangle
        {
            color: cc.sessionSeparator
            opacity: 1
            height: greenZone.height - 15 //greenZone.pointsScrollBar height
            width: 4
            z:1
        }
    }
}
