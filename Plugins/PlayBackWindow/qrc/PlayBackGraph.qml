import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Item
{
    id:playbackGraph
    width: 1280
    height: 415
    visible: false   

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal graphDrag(var pix, var startPix, var isRight)
    signal graphZoom(var procent, var moveProc, var isReleased)
    signal graphZoomMove(var procent)
    signal sliderSeekTo(var proc)
    signal sliderSeekValueChanged(var proc, var released)
    signal sliderSeekPress(var isPressed)


    function setElapsedTimeIndicatorPos(position, isTimeIndicatorVisible)
    {
        sliderElem.isUpdateByUser = true
        graphCanvas.timeIndicatorVisible = isTimeIndicatorVisible

        if( isTimeIndicatorVisible )
        {
            elapsedTimeIndicator.x = greenZone.x + position
            elapsedTimeIndicator.visible = true
        }
        else
            elapsedTimeIndicator.visible = false
        greenZone.timeIndicatorPos = elapsedTimeIndicator.x
    }

    function moveElapsedTimeIndicator(move)
    {
        elapsedTimeIndicator.x += move
        greenZone.timeIndicatorPos = elapsedTimeIndicator.x
    }


    function updateTime(startTime, endTime)
    {
        startTimeText.text = startTime
        endTimeText.text = endTime
    }

    function updateScroll(size ,pos)
    {
        scroll.width = size
        scroll.x = pos;

        if( pos + size > 1120 )
            scroll.x = 1120 - size
    }

    function updateMaxZoom(maxZoom)
    {
        //Мы не должны фейковым зумом уходить ближе реального
//        column.maxZoom = 100 - maxZoom
    }

    function setCurrentTime(time)
    {
        currentTime.text = time
    }

    function setSliderPos(proc)
    {
        console.debug("SLIDER SET PROC = " + proc)
        sliderElem.value = proc
    }

    function setBusy(isBusy) // Enable/disable loading visual
    {
        busyBlock.visible = isBusy
    }


    Rectangle //back
    {
        anchors.centerIn: parent
        width: parent.width - 36
        height: parent.height
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }


    Row //start time date
    {
        x:greenZone.x + 10
        y:2
        spacing: 25
        Text
        {
            id: startTimeText
            font.family: "Source Sans Pro"
            font.pixelSize: 25
            color: cc.white
            text: qsTr("12:00:00")
        }
        Text
        {
            visible: false
            font.family: "Source Sans Pro"
            font.pixelSize: 25
            color: cc.white
            text: qsTr("30.11.2019")
        }
    }

    Text //current time
    {
        id: currentTime
        anchors.horizontalCenter: parent.horizontalCenter
        y: 2
        font.family: "Source Sans Pro"
        font.pixelSize: 25
        color: cc.white
        text: qsTr("00:00:00")
    }

    Row //end time date
    {
        x:greenZone.x + greenZone.width - width - 10
        y:2
        spacing: 25
        Text
        {
            id: endTimeText
            font.family: "Source Sans Pro"
            font.pixelSize: 25
            color: cc.white
            text: qsTr("00:01:40")
        }
        Text
        {
            visible: false
            font.family: "Source Sans Pro"
            font.pixelSize: 25
            color: cc.white
            text: qsTr("01.12.2019")
        }
    }

    Rectangle
    {
        id: zoomMoveRect
        width: 10
        height: 10
        color: "green"
        x: greenZone.x
        z: 5
        anchors.verticalCenter:  greenZone.verticalCenter
        opacity: 0
    }


    Text
    {
        x:72 - width
        y: greenZone.y
        font.family: "Source Sans Pro"
        font.pixelSize: 18
        color: cc.white
        text: qsTr("100 %")
    }

    Text
    {
        x:72 - width
        y: greenZone.y + greenZone.height/2 - height/2
        font.family: "Source Sans Pro"
        font.pixelSize: 18
        color: cc.white
        text: qsTr("50 %")
    }

    Text
    {
        x:72 - width
        y: greenZone.y + greenZone.height - height
        font.family: "Source Sans Pro"
        font.pixelSize: 18
        color: cc.white
        text: qsTr("0 %")
    }


    Rectangle
    {
        id: elapsedTimeIndicator
        height: greenZone.height
        width: 2
        color: "#bd4932"
        x: greenZone.x
        y: greenZone.y
        z: 2
    }


    Rectangle //GreenZone >>
    {
        id: greenZone
        anchors.horizontalCenter: parent.horizontalCenter
        y: 30
        width: parent.width - 160
        height: 290
        radius: 6
        color: cc.light_green

        property real timeIndicatorPos: 0

        ShadowRect{x: -6; y: 6; radius: 6}


        Column //grey lines
        {
            topPadding: 46.5
            spacing: 46.5
            z:3
            Repeater
            {
                model: 5

                Rectangle
                {
                    width: greenZone.width
                    height: 2
                    color: cc.dark_blue
                    opacity: 0.2
//                    opacity: 1
                }
            }
        }


//        }


        GraphCanvas
        {
            property real defaultZoomLevel: 20
            anchors.fill: parent
            objectName: "GraphItem"
            id: graphCanvas
        }



        MultiPointTouchArea
        {
            id: multiPoint
            z:6
            anchors.fill: parent
            maximumTouchPoints : 2
            minimumTouchPoints : 1
            property real factor: 1
            property bool isReleased: true // ignore the TouchUpdate after released
            property bool skipPoint: false
            property int lastXMoveP1: 0
            property int lastXMoveP2: 0
            property int startXMoveP1: 0
            property int startXMoveP2: 0
            property bool isDrag: false
            property bool wasRightMove: false
            property bool isStartDrag: true
            property var moveRight: false
            property var scaleFin: 0
            property var zoomMove: 0
            property var zoomProcText: 0
            property var lastSC: 0
            property var tmpSC: 0
            touchPoints:
            [
                TouchPoint { id: point1 },
                TouchPoint { id: point2 }
            ]
            onTouchUpdated:
            {

                if(skipPoint)
                {
                    skipPoint = false
                    return
                }

                //Проверяем что 2 пальца коснулись экрана
                if(point1.pressed && point2.pressed)
                {
                    if(isReleased)
                    {
                        startXMoveP1 = point1.x
                        startXMoveP2 = point2.x
                        isReleased = false                       
                        return
                    }

                    var s = Math.abs(point1.x - point2.x) - Math.abs(point1.startX - point2.startX)

                    scaleFin = s;

                    var sc = rect.scale

                    sc = s * 0.003 + lastSC

                    if( sc >= 5.7 )
                        sc = 5.7

                    if( sc <= 0.1 )
                        sc = 0.1

                    rect.scale = sc

                    tmpSC = sc

                    var proc = rect.scale * 100 / 6.1

                    var test = 100 - proc
                    zoomProcText = test

                    var leftTochX

                    if(point1.startX < point2.startX)
                        leftTochX = point1.startX
                    else
                        leftTochX = point2.startX

                    var proc2 = ((leftTochX + Math.abs(point1.startX - point2.startX) / 2) / 1120) * 100


                    //Тут у нас фейк зум
//                    if( column.testZoom < column.maxZoom  )
                        graphCanvas.changeZoomProc(100 - proc-1, proc2)
                    isDrag = false
                }
                else
                {
                    zoomMoveRect.x = point1.x + greenZone.x
                    if(isReleased)
                    {
                        isReleased = false
                        startXMoveP1 = point1.x
                        isStartDrag = true;
                        moveRight = false;
                    }

                    rect.beginDrag = Qt.point(rect.x, rect.y)
                    isDrag = true

                    var newPoint = point1.x
                    var oldPoint = lastXMoveP1
                    lastXMoveP1 = point1.x


                    if( startXMoveP1 > lastXMoveP1 )
                    {
                        //Игнорируем случайные сдвиги чтоб не мешало зуму
                        if( startXMoveP1 - lastXMoveP1 < 10 )
                        {
                            console.debug("IGNORE")
                        }
                        else
                        {
                            if( newPoint > oldPoint  && newPoint < startXMoveP1 && wasRightMove && !isStartDrag )
                            {
                                moveRight = false
                            }
                            else
                            {
                                moveRight = true
                            }

                            if( isStartDrag )
                                moveRight = true

                            wasRightMove = true;
                            playbackGraph.graphDrag(point1.startX - lastXMoveP1, point1.startX, moveRight)
                        }
                    }
                    else if( startXMoveP1 < lastXMoveP1 )
                    {

                        if( newPoint < oldPoint && newPoint > startXMoveP1 && !wasRightMove && !isStartDrag )
                        {
                            moveRight = true;
                        }
                        else
                            moveRight = false

                        //Игнорируем случайные сдвиги чтоб не мешало зуму
                        if( lastXMoveP1 - startXMoveP1 < 10 )
                        {
                            console.debug("IGNORE")
                        }
                        else
                        {
                            if( isStartDrag )
                                moveRight = false

                            wasRightMove = false;
                            playbackGraph.graphDrag(lastXMoveP1 - point1.startX, point1.startX, moveRight)
                        }
                    }

                    isStartDrag = false
                }
            }


            onReleased:
            {
                lastSC = tmpSC
                if( !isReleased )
                {
                    //Тут у нас результат зума.
                    if( !isDrag )
                    {
                        multiPoint.factor = rect.scale
                        var proc = rect.scale * 100 / 6.1

                        var leftTochX

                        if(point1.startX < point2.startX)
                            leftTochX = point1.startX
                        else
                            leftTochX = point2.startX

                        var proc2 = ((leftTochX + Math.abs(point1.startX - point2.startX) / 2) / 1120) * 100


                        var test = 100 - proc

                        zoomProcText = test
                        graphZoom( proc, proc2, true)
                    }
                    else
                    {
                        if( startXMoveP1 === point1.x )
                        {
                            zoomMoveRect.x = point1.x + greenZone.x

                            proc = zoomMoveRect.x * 100 / 1120
                            multiPoint.zoomMove = proc
                            graphZoomMove(proc)
                        }
                        startXMoveP1 = 0
                        lastXMoveP1 = 0
                    }

                    isReleased = true
                    skipPoint = true
                }
            }
        }

        Rectangle
        {
            id: pointsScrollBar
            anchors.bottom: greenZone.bottom
            height: 15
            width: greenZone.width
            color: cc.highlight_green
            opacity: 0.6

            Rectangle
            {
                id: scroll
                width: 1120 / 6// screen width / num of total pages with minimal width 4
                height: parent.height
                x: 0
                y:0
                color: cc.dark_blue
            }

        }

        Rectangle
        {
            id: rect
            width: 500
            height: 100
            x: 0
            y: 0
            property point beginDrag
            color : "transparent"
            border { width:2; color: "transparent" }
            radius: 5
            scale: 2.0

            onBeginDragChanged:
            {
                rect.x = beginDrag.x
                rect.y = beginDrag.y
            }
        }
    }//GreenZone <<



    Rectangle //Slider
    {
        id: sliderRect
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height - height - 20
        width: parent.width - 160
        height: 60
        radius: 6
        color: cc.dark_blue

        ShadowRect{x: -4; y: 4; radius: 4}


        Slider
        {
            id: sliderElem
            anchors.fill: parent
            value: 0.0
            from: 0.0
            to: 100.0
            stepSize: 0.1

            property var yShift: 0
            property var pointSize: 0
            property bool wasPress: false
            property var valToSeek: 0
            property bool isUpdateByUser: false
            property bool valueChanged: false

            Timer {
                interval: 200; running: sliderElem.wasPress ; repeat: true
                onTriggered:
                {
                    if( !sliderElem.pressed )
                    {
                        playbackGraph.sliderSeekPress(false)
                    }
                    else
                        console.debug("NOTHING")
                }
            }


            onPressedChanged:
            {
                if( pressed )
                {
                    console.log("slider PRESSED. hide hover.")
                    isUpdateByUser = false
                    playbackGraph.sliderSeekPress(true)
                }
                else {
                    console.log("slider released. hide hover.")
                    wasPress = false
                    console.debug("WAS PRESS!!!!!!!!!!!!!!!!!!!!!!!")
                    playbackGraph.sliderSeekPress(false)
                    playbackGraph.sliderSeekValueChanged(valToSeek, true);
                }
            }

            onValueChanged:
            {
                valueChanged = true;
                console.debug("Slider value changed")

                if( sliderElem.pressed )
                {
                    playbackGraph.sliderSeekValueChanged(value, false);
                    valToSeek = value
                }
                console.debug("TIMER INDICATOR UPDATE VALUE")

                if( isUpdateByUser )
                {
                    console.debug("IS UPDATE BY USER")
                    isUpdateByUser = false
                    return
                }
                playbackGraph.setElapsedTimeIndicatorPos(value * greenZone.width / 100.0, true)
            }


            background: Rectangle {
                x: parent.leftPadding
                y: parent.topPadding + parent.availableHeight / 2 - height / 2
                width: parent.availableWidth
                height: 2
                color: cc.blue
            }

            handle: Rectangle {
                x: parent.leftPadding + parent.visualPosition * (parent.availableWidth - width)
                y: parent.topPadding + parent.availableHeight / 2 - height / 2
                implicitWidth: 38
                implicitHeight: implicitWidth
                radius: implicitWidth/2
                color: cc.light_green
                scale: parent.pressed ? 0.7 : 1
            }
        }
    }

    BusyBlock
    {
        id:busyBlock
        anchors.fill: greenZone
        rectSize: 200
        dotSize: 16
        blockButton: true
        visible: false
    }

//    Text
//    {
//        id:txtTets1
//        anchors.bottom: parent.bottom
//        anchors.bottomMargin: -40
//        anchors.horizontalCenter: parent.horizontalCenter
//        font.pixelSize: 40
//        color: "white"
//        text:"t1"
//    }

}
