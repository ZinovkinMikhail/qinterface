import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Item
{
    id: filterMaskingTab
    width: 1280
    height: 484
    visible: false

    signal showComboDrumDate(int type)
    signal showComboDrumChannel()
    signal saveButtonClicked()
    signal cancelButtonClicked()

    property var inputPanelY: parent ? parent.parent ? parent.parent.inputPanelY : 0 : 0
    property real pressedFieldY //for shift when virtual keyboard open

    ColorConfig {id: cc}
    FontsConfig{id:fontConf}

    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 415
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }

    Rectangle
    {
        id:mainZone
        objectName: "mainZone"
        anchors.fill: background
        anchors.margins: 13
        color: cc.dark_blue
        clip: true

        Item
        {
            id:shiftZone
            objectName: "ShiftZone"
            width: mainZone.width
            height: mainZone.height
            y:
            {
                if(inputPanelY < pressedFieldY)
                {
                    inputPanelY - pressedFieldY
                }
                else
                {
                    0
                }
            }


            Column
            {
                id:column1
                anchors.top: parent.top
                anchors.topMargin: 72
                anchors.left: parent.left
                anchors.leftMargin: 40
                spacing: 152

                TextCheckBox
                {
                    id: checkboxFilter
                    checkName: "CheckBoxFilter"
                    rightCheckbox: false
                    text: qsTr("Использовать фильтр")
                    spacing: 15
                    checked: false
                }

                TextCheckBox
                {
                    id: checkboxMasking
                    checkName: "CheckBoxMasking"
                    rightCheckbox: false
                    text: qsTr("Маскирование")
                    spacing: 15
                }
            }


            Column
            {
                id:column2
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 72
                spacing: 50

                Item
                {
                    id:dateButtons
                    height: dateStartButton.height
                    width: 500

                    Text
                    {
                        id:dateStartLabel
                        anchors.horizontalCenter: dateStartButton.horizontalCenter
                        anchors.bottom: dateStartButton.top
                        anchors.bottomMargin: 5
                        font.family: fontConf.mainFont
                        font.italic: true
                        font.pixelSize: fontConf.pixSize_S6 - 2
                        color: cc.light_green
                        opacity: dateStartButton.enabled ? 1 : 0.4
                        text: qsTr("Дата начала")
                    }

                    Text
                    {
                        id:dateEndLabel
                        anchors.horizontalCenter: dateEndButton.horizontalCenter
                        anchors.bottom: dateEndButton.top
                        anchors.bottomMargin: 5
                        font.family: fontConf.mainFont
                        font.italic: true
                        font.pixelSize: fontConf.pixSize_S6 - 2
                        color: cc.light_green
                        opacity: dateEndButton.enabled ? 1 : 0.4
                        text: qsTr("Дата окончания")
                    }


                    ButtonDrop
                    {
                        id:dateStartButton
                        objectName: "DateStartButton"
                        type: "date"
                        enabled:
                        {
                            if(radioButtonDate.enabled && radioButtonDate.checked)
                                return true
                            return false
                        }
                        opacity: enabled ? 1 : 0.4
                        txttxt: "01.01.2019"

                        onButtonClick: showComboDrumDate(0)
                    }

                    ButtonDrop
                    {
                        id:dateEndButton
                        objectName: "DateEndButton"
                        anchors.verticalCenter: dateButtons.verticalCenter
                        anchors.right: dateButtons.right
                        type: "date"
                        enabled:
                        {
                            if(radioButtonDate.enabled && radioButtonDate.checked)
                                return true
                            return false
                        }
                        opacity: enabled ? 1 : 0.4
                        txttxt: "01.01.2019"

                        onButtonClick: showComboDrumDate(1)
                    }
                }


                ButtonDrop
                {
                    id: channelButton
                    objectName: "ChannelButton"
                    anchors.horizontalCenter: parent.horizontalCenter
                    longlonglongStyle: true
                    enabled:
                    {
                        if(radioButtonChannel.enabled && radioButtonChannel.checked)
                            return true
                        return false
                    }
                    opacity: enabled ? 1 : 0.4
                    txttxt: "button text"

                    onButtonClick: showComboDrumChannel()
                }


                TextField
                {
                    id: maskingField
                    objectName: "MaskingField"
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: 50
                    width: 500
                    font.family: fontConf.mainFont
                    font.italic: true
                    font.pixelSize: fontConf.pixSize_S6
                    enabled: checkboxMasking.checked
                    opacity: enabled ? 1 : 0.3

                    background: Rectangle {color: "transparent"}

                    Rectangle
                    {
                        anchors.fill: parent
                        z:-1
                        color: cc.white
                    }

                    ShadowRect{x: -4; y: 4}

                    onPressed:
                    {
                        if(inputPanelY > pressedFieldY)
                        {
                            pressedFieldY = mapToItem(filterMaskingTab.parent.parent,x,y).y +14
                        }
                    }
                }
            }


            Column
            {
                id:column3
                anchors.top: parent.top
                anchors.topMargin: 72
                anchors.right: parent.right
                anchors.rightMargin: 50
                spacing: 50

                RadioButton
                {
                    id: radioButtonDate
                    objectName: "RadioButtonDate"
                    text: qsTr("Фильтр по дате")
                    checked: true
                    enabled: checkboxFilter.checked
                    opacity: enabled ? 1 : 0.4
                    spacing: 15

                    indicator: Rectangle
                    {
                        implicitWidth: 40
                        implicitHeight: 40
                        radius: implicitWidth/2
                        x: radioButtonDate.leftPadding
                        y: parent.height / 2 - height / 2
                        color: cc.white
                        scale: radioButtonDate.down ? 0.9 : 1

                        Rectangle
                        {
                            anchors.centerIn: parent
                            width: 26
                            height: 26
                            radius: width/2
                            color: cc.dark_blue
                            visible: radioButtonDate.checked
                        }
                    }

                    contentItem: Text
                    {
                        text: radioButtonDate.text
                        font.family: fontConf.mainFont
                        font.pixelSize: fontConf.pixSize_S6
                        color: cc.white
                        scale: radioButtonDate.down ? 0.9 : 1
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: radioButtonDate.indicator.width + radioButtonDate.spacing
                    }
                }

                RadioButton
                {
                    id: radioButtonChannel
                    objectName: "RadioButtonChannel"
                    text: qsTr("Фильтр по каналам")
                    enabled: checkboxFilter.checked
                    opacity: enabled ? 1 : 0.4
                    spacing: 15

                    indicator: Rectangle {
                        implicitWidth: 40
                        implicitHeight: 40
                        radius: implicitWidth/2
                        x: radioButtonChannel.leftPadding
                        y: parent.height / 2 - height / 2
                        color: cc.white
                        scale: radioButtonChannel.down ? 0.9 : 1

                        Rectangle
                        {
                            anchors.centerIn: parent
                            width: 26
                            height: 26
                            radius: width/2
                            color: cc.dark_blue
                            visible: radioButtonChannel.checked
                        }
                    }

                    contentItem: Text
                    {
                        text: radioButtonChannel.text
                        font.family: fontConf.mainFont
                        font.pixelSize: fontConf.pixSize_S6
                        color: cc.white
                        scale: radioButtonChannel.down ? 0.9 : 1
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: radioButtonChannel.indicator.width + radioButtonChannel.spacing
                    }
                }
            }
        }
    }


    Item
    {
        id:controlButtons
        anchors.top: background.bottom
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        width: 480

        ControlListButton
        {
            anchors.right: parent.right
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked: cancelButtonClicked()
        }

        ControlListButton
        {
            anchors.left: parent.left
            type: "SlimType"
            text: qsTr("Сохранить")
            onClicked: saveButtonClicked()
        }
    }


}
