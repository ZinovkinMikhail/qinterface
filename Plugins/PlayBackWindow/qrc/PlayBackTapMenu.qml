import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Rectangle
{
    id:root
    width: 210
    height: columnElem.height
    radius: 10
    color: cc.white

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
    ShadowRect{x: -6; y: 6; radius: 10}

    signal play()
    signal copy()
    signal select()
    signal selectAll()
    signal graph()

    function changeSelectAllName(newName)
    {
        selectAll.item.text = newName
    }

    function changeSelectName(newName)
    {
        select.item.text = newName
    }

    function click(id)
    {
        console.debug(id)
        if( id === "play" )
        {
            root.play()
        }
        if(id === "copy")
        {
            root.copy()
        }
        if(id === "select")
        {
            root.select()
        }
        if(id === "selectAll")
        {
            root.selectAll()
        }
        if(id === "graph")
        {
            root.graph()
        }
    }


    Component.onCompleted:
    {
        playButton.item.text = "Воспроизвести"
        playButton.item.objectName = "play"
        copyButton.item.text = "Копировать"
        copyButton.item.objectName = "copy"
        select.item.text = "Выбрать"
        select.item.objectName = "select"
        selectAll.item.text = "Выбрать все"
        selectAll.item.objectName = "selectAll"
        graph.item.text = "График"
        graph.item.objectName = "graph"

    }


    Button
    {
        id:blockZone
        x:-parent.x
        y:-parent.y - 150
        width:1280
        height: 720
        z:-1

        background: Rectangle {opacity: 0}

        onClicked: root.visible = false
    }

    Button // blockZone fix
    {
        anchors.fill: parent
        background: Rectangle {opacity: 0}
    }


    Column
    {
        id:columnElem
        anchors.horizontalCenter: parent.horizontalCenter
        topPadding: 10
        bottomPadding: 10
        spacing: 10

        Loader{ sourceComponent: tapButton; id: playButton; objectName: "selectAll" }
        Loader{ sourceComponent: line; anchors.horizontalCenter: parent.horizontalCenter }
        Loader{ sourceComponent: tapButton; id:copyButton }
        Loader{ sourceComponent: line; anchors.horizontalCenter: parent.horizontalCenter }
        Loader{ sourceComponent: tapButton; id: select }
        Loader{ sourceComponent: line; anchors.horizontalCenter: parent.horizontalCenter }
        Loader{ sourceComponent: tapButton; id:selectAll }
        Loader{ sourceComponent: line; anchors.horizontalCenter: parent.horizontalCenter }
        Loader{ sourceComponent: tapButton; id: graph }
    }


    Component
    {
        id:tapButton

        Button
        {
            signal click()
            width: root.width
            height: 28
            scale: down ? 0.9 : 1
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S6 + 3

            onClicked:
            {
                root.click(objectName)
                root.visible = false
            }

            background: Rectangle {
                opacity: 0
            }

            contentItem: Text {
                text: parent.text
                font: parent.font
                color: cc.blue
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }
        }
    }

    Component
    {
        id:line

        Rectangle
        {
            width: 180
            height: 2
            color: cc.blue
            opacity: 0.6
        }
    }
}
