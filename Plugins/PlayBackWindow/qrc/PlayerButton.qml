import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0

Rectangle
{
    ColorConfig{id:cc}
    id: root

    width: 56
    height: 56
    color: cc.dark_blue
    scale: buttonElem.down ? 0.9 : 1

    ShadowRect{x: -3; y: 9; height: 50; opacity: buttonElem.down ? 0 : 1}


    property string icon: ""
    property string iconSecondState: ""
    property bool rotate: false
    property var parentRow
    property var name: ""


    function setState(state)
    {
        if(state === 1)
            image.source = icon
        else if (state === 2)
            image.source = iconSecondState
    }

    function setEnable(isEnable)
    {
        if(isEnable)
        {
            buttonElem.enabled = true
            image.opacity = 1
        }
        else
        {
            buttonElem.enabled = false
            image.opacity = 0.3
        }
    }

    Component.onCompleted:
    {
        root.objectName = name
    }

    Image
    {
        id: image
        anchors.centerIn: parent
        width: 30
        fillMode: Image.PreserveAspectFit
        mipmap: true
        mirror: rotate
        source: icon
        objectName: "Icon"
    }


    Button
    {
        id: buttonElem
        anchors.fill: parent

        background: Rectangle
        {
            opacity: 0
        }

        onClicked:
        {
            root.parentRow.buttonClicked(root.name)
        }
    }
}
