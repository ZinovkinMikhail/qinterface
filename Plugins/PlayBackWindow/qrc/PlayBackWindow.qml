import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Item
{
    id: root
    y: 150

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal showGraphClicked()
    signal showFilterClicked()
    signal showCopyClicked()
    signal showSessions()
    signal updateDatabase()

//start visible element setup by hand change linine visible in to element

    function setBackgroundTitle(newtitle)
    {
        backgroundTitle.text = newtitle
    }

    function setHighlightButton(button, activeState)
    {
        if(button === 0)
        {
            filterButton.activeState = activeState
            graphButton.activeState = false
        }
        if(button === 1)
        {
            filterButton.activeState = false
            graphButton.activeState = activeState

        }
    }

    function squareButtonsSetActive(isActive)
    {
        filterButton.enabled = isActive
        graphButton.enabled = isActive
        copyButton.enabled = isActive
        updateButton.enabled = isActive


        filterButton.opacity = isActive ? 1 : 0.4
        graphButton.opacity = isActive ? 1 : 0.4
        copyButton.opacity = isActive ? 1 : 0.4
        updateButton.opacity = isActive ? 1 : 0.4
    }


    function updateButtonSetActive(isActive)
    {
        updateButton.enabled = isActive
        updateButton.opacity = isActive ? 1 : 0.4
    }



    BackgroundTitle
    {
        id:backgroundTitle
        text: qsTr("Воспроизведение")
    }


    Timer
    {
        id: updateDatabaseDelay
        repeat: false
        running: false
        interval: 10
        onTriggered:
        {
            root.updateDatabase()
        }
    }

    Row
    {
        id:squareButtons
        x:40 //18
        y:428
        spacing: 30

        SquareButton
        {
            id:filterButton
            img: "qrc:///PlayBackWindow/icons/i35.png"
            onClicked: root.showFilterClicked()
        }
        SquareButton
        {
            id:graphButton
            img: "qrc:///PlayBackWindow/icons/i36.png"

            onClicked: root.showGraphClicked()
        }
        SquareButton
        {
            id:copyButton
            img: "qrc:///PlayBackWindow/icons/i38.png"
            onClicked: root.showCopyClicked()
        }
        SquareButton
        {
            id:updateButton
            img: "qrc:///PlayBackWindow/icons/loop.png"

            onClicked: updateDatabaseDelay.start()
        }
    }

}

