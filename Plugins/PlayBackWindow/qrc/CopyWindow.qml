import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Item
{
    id: copyWindow
    anchors.fill: parent
    visible: false

    signal copyButtonClicked()
    signal cancelButtonClicked()
    signal closeButtonClicked()
    signal copySessionsChecked()
    signal copyAllChecked()
    signal searchButtonClicked()
    signal copyLogsClicked()


    ColorConfig {id: cc}
    FontsConfig {id:fontConf}


    function searchStateON(_info)
    {
        busyBlock.visible = true
        externalStorageIcon.visible = false
        storageFoundText.visible = false
        storageSearchText.visible = true
        storageSearchText.text = _info
        freeSpaceText.visible = false
        copyButton.enabled = false
        storageNotFoundText.visible = false
        updateButton.visible = false
    }

    function searchStateOFF()
    {
        busyBlock.visible = false
        externalStorageIcon.visible = true
        storageFoundText.visible = true
        storageSearchText.visible = false
        freeSpaceText.visible = true
        copyButton.enabled = true
        storageNotFoundText.visible = false
        updateButton.visible = false
    }

    function notFoundState()
    {
        busyBlock.visible = false
        storageSearchText.visible = false
        storageNotFoundText.visible = true
        updateButton.visible = true
        externalStorageIcon.visible = false
        storageFoundText.visible = false
        freeSpaceText.visible = false

    }

    function selectSessionsRadioButton()
    {
        radioButtonSessions.checked = true;
    }

    function setGuiByCopyState()
    {
        copyButton.enabled = false
        cancelButton.enabled = true
    }

    function setGuiByCopyEndState()
    {
        cancelButton.enabled = false
    }
    function setGuiByCopyStopState()
    {
        copyButton.enabled = false
        cancelButton.enabled = false
    }


    Button
    {
        id:blockZone
        anchors.fill: parent
        anchors.margins: -200

        background: Rectangle {
            color: cc.fog
            opacity: 0.4
        }
    }


    Rectangle
    {
        id:mainZone
        objectName: "mainZone"
        anchors.centerIn: parent
        height: 320
        width: 1000
        radius: 4
        color: cc.dark_blue

        ShadowRect{type: "panel"}

        BackgroundTitle
        {
            x: parent.width - width - 25
            y: parent.height - height - 10
            font.pixelSize: 42
            text: qsTr("Копирование")
        }


        Image
        {
            id:externalStorageIcon
            objectName: "ExternalStorageIcon"
            x: 65
            y: 100
//            fillMode: Image.PreserveAspectFit
//            mipmap: true
            scale: 0.8
            source: "qrc:///PlayBackWindow/icons/i4.png"
            visible: true
        }

        RadioButton
        {
            id: radioButtonSessions
            objectName: "RadioButtonSessions"
            x: parent.width - width - 50
            y: 40
            width: 260
            text: qsTr("Копировать сеансы")
            spacing: 15
            checked: true

            onClicked: copySessionsChecked()

            indicator: Rectangle
            {
                implicitWidth: 40
                implicitHeight: implicitWidth
                radius: implicitWidth/2
                x: radioButtonSessions.width - width - radioButtonSessions.rightPadding
                y: parent.height / 2 - height / 2
                color: "#fcfff5"
                scale: radioButtonSessions.down ? 0.9 : 1

                Rectangle
                {
                    width: 26
                    height: width
                    radius: width/2
                    anchors.centerIn: parent
                    color: cc.dark_blue
                    visible: radioButtonSessions.checked
                }
            }

            contentItem: Text
            {
                text: radioButtonSessions.text
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6
                color: cc.white
                scale: radioButtonSessions.down ? 0.9 : 1
                rightPadding: radioButtonSessions.indicator.width + radioButtonSessions.spacing
                verticalAlignment: Text.AlignVCenter
            }
        }

        RadioButton
        {
            id: radioButtonAll
            objectName: "RadioButtonAll"
            x: parent.width - width - 50
            y: radioButtonSessions.y + 65
            width: 260
            text: qsTr("Копировать всё")
            spacing: 15
            onClicked: copyAllChecked()

            indicator: Rectangle
            {
                implicitWidth: 40
                implicitHeight: implicitWidth
                radius: implicitWidth/2
                x: radioButtonAll.width - width - radioButtonAll.rightPadding
                y: parent.height / 2 - height / 2
                color: "#fcfff5"
                scale: radioButtonAll.down ? 0.9 : 1

                Rectangle
                {
                    width: 26
                    height: width
                    radius: width/2
                    anchors.centerIn: parent
                    color: cc.dark_blue
                    visible: radioButtonAll.checked
                }
            }

            contentItem: Text
            {
                text: radioButtonAll.text
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6
                color: cc.white
                scale: radioButtonAll.down ? 0.9 : 1
                rightPadding: radioButtonAll.indicator.width + radioButtonAll.spacing
                verticalAlignment: Text.AlignVCenter
            }
        }

        RadioButton
        {
            id: radioButtonLogs
            objectName: "RadioButtonLogs"
            x: parent.width - width - 50
            y: radioButtonAll.y + 65
            width: 260
            text: qsTr("Копировать логи")
            spacing: 15
            onClicked: copyLogsClicked()

            indicator: Rectangle
            {
                implicitWidth: 40
                implicitHeight: implicitWidth
                radius: implicitWidth/2
                x: radioButtonLogs.width - width - radioButtonLogs.rightPadding
                y: parent.height / 2 - height / 2
                color: "#fcfff5"
                scale: radioButtonLogs.down ? 0.9 : 1

                Rectangle
                {
                    width: 26
                    height: width
                    radius: width/2
                    anchors.centerIn: parent
                    color: cc.dark_blue
                    visible: radioButtonLogs.checked
                }
            }

            contentItem: Text
            {
                text: radioButtonLogs.text
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6
                color: cc.white
                scale: radioButtonLogs.down ? 0.9 : 1
                rightPadding: radioButtonLogs.indicator.width + radioButtonLogs.spacing
                verticalAlignment: Text.AlignVCenter
            }
        }


        Item
        {
            id: txtZone1
            y: radioButtonSessions.y + 15
            x: 250
            height: cancelButton.height

            Text
            {
                id:selectedToCopyText
                objectName: "SelectedToCopyText"
                anchors.top: parent.top
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6 -2
                color: cc.white
//                text: qsTr("Выбрано 1.2 Гб.")
            }
            Text
            {
                id:selectedDurationText
                objectName: "SelectedDurationText"
                anchors.bottom: parent.bottom
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6 -2
                color: cc.white
//                text: qsTr("00 ч. 33 мин. 45 сек.")
            }
        }
        Timer
        {
            id: cancelTimer
            running: false
            repeat: false
            interval: 10
            onTriggered:
            {
                copyWindow.cancelButtonClicked()
            }
        }

        Item {
            id: searchItem
            y:cancelButton.y
            x: 45
            height: cancelButton.height
            width: 120
            visible: false
            ControlListButton
            {
                id:searchButton
                anchors.centerIn: parent
                height: 40
                width: 100
                type: "SlimType"
                text: qsTr("Поиск")
                onClicked: copyWindow.cancelButtonClicked()
            }

        }


        Item
        {
            id: txtZone2
            y:cancelButton.y
            x: 45
            height: cancelButton.height
            visible: true

            Text
            {
                id:storageSearchText
                objectName: "StorageSearchText"
                anchors.top: parent.top
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6 -2
                color: cc.white
                visible: false
//                text: qsTr("Определение внешнего накопителя...")
            }
            Text
            {
                id:storageNotFoundText
                objectName: "StorageNotFoundText"
                anchors.top: parent.top
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6 -2
                color: cc.white
                visible: false
                text: qsTr("Внешний накопитель не найден!")
            }
            Text
            {
                id:storageFoundText
                objectName: "StorageFoundText"
                anchors.top: parent.top
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6 -2
                color: cc.white
//                text: qsTr("Найден внешний накопитель 64Гб.")
            }
            Text
            {
                id:freeSpaceText
                objectName: "FreeSpaceText"
                anchors.bottom: parent.bottom
                font.family: fontConf.mainFont
                font.pixelSize: fontConf.pixSize_S6 -2
                color: cc.white
//                text: qsTr("Свободно 16 Гб.")
            }
        }


        ControlListButton
        {
            id:updateButton
            x: 45
            y: 260
            height: 30
            width: 275
            type: "SlimType"
            text: qsTr("Повторить")
            visible: false
            onClicked: copyWindow.searchButtonClicked()
        }

        ControlListButton
        {
            id:cancelButton
            x: parent.width - width - 50
            y: parent.height - height - 30
            type: "SlimType"
            text: qsTr("Отмена")
            onReleased: cancelTimer.start()
        }
        ControlListButton
        {
            id:copyButton
            x: parent.width - width - width - 90
            y: parent.height - height - 30
            type: "SlimType"
            text: qsTr("Копировать")
            onClicked: copyWindow.copyButtonClicked()
        }


        CloseSmallButton
        {
            onClicked: closeButtonClicked()
        }


        BusyBlock
        {
            id:busyBlock
            anchors.centerIn: externalStorageIcon
            rectSize: 100
            dotSize: 10
            blockButton: false
            visible: false
        }

    }

}
