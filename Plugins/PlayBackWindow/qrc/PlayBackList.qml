import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Item
{
    id: root
    width: 1280
    height: 415
    visible: true

    property int currentRow: 0
    property int state: 0
    property int fontSizeAdj: -2
    property real lastPosition: 0
    property real lastContentY: -33
    property real headerHeight: 33

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    function setListModel(model)
    {
        lv.model = model
        lv.update()
    }

    signal rowChanged(int index, string role, int val)
    signal play()
    signal copy()
    signal select(var row)
    signal selectAll()
    signal graph()

    function reload()
    {
        playBackTapMenu.changeSelectAllName("Выбрать все")
        root.state = 0;
    }


    function selectRowOnly(index, role, val, lastPos)
    {
        rowChanged(index, role, val)
        currentRow = index
        //console.debug("INDEX = " + index)
        sb.position = lastPos
       // console.debug("Position = " + lastPos)
    }

    function selectRow(index, role, val, pointerPos)
    {
        currentRow = index
        var selected = lv.model.getSelectedState(currentRow)
        if( selected )
            playBackTapMenu.changeSelectName("Отменить выбор")
        else
            playBackTapMenu.changeSelectName("Выбрать")

        setContextMenuState(true, pointerPos)
    }


    function setContextMenuState(state, pointerPos)
    {
        playBackTapMenu.x = pointerPos.x < playBackTapMenu.width ?
                    pointerPos.x : pointerPos.x - playBackTapMenu.width
        playBackTapMenu.y = pointerPos.y < 240 ?
                    pointerPos.y : pointerPos.y - playBackTapMenu.height
        playBackTapMenu.visible = state
    }

    function setBusy(isBusy) // Enable/disable loading visual
    {
        busyBlock.visible = isBusy
    }

    function setBlockScreen(isDisable) // Enable/disable block records list
    {
        lockRect.visible = isDisable
    }

    function setLastTablePos()
    {
        lv.contentY = root.lastContentY
    }


    Rectangle
    {
        id:back
        anchors.centerIn: parent
        width: parent.width - 36
        height: parent.height
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }


    Item
    {
        id:mainZone
        anchors.centerIn: parent
        width: parent.width - 64
        height: parent.height - 18
    }


    Rectangle
    {
        x:lv.x + sb.x - 4
        y:lv.y + sb.y
        height: sb.height
        width: sb.width + 8
        color: cc.dark_blue
    }



    ListView
    {
        id:lv
        anchors.fill: mainZone
        clip: true
        spacing: 2

        headerPositioning: ListView.OverlayHeader

        header: playlistHeader
     //   model: playlistModel
        delegate: playlistRow

        onContentYChanged: {
            if( contentY > -root.headerHeight)
                root.lastContentY = contentY
            else if (contentY != -root.headerHeight)
                root.lastContentY = -root.headerHeight
        }

        ScrollBar.vertical: ScrollBar
        {
            id:sb
            topPadding:5
            bottomPadding:5
//            minimumSize: 0.15 // QT 5.11

            onPositionChanged:
            {
                root.lastPosition = position
            }

            contentItem: Rectangle
            {
                implicitWidth: 28
                radius: 2
                color: cc.light_green
//                color: "transparent" //variant for add indicator
                opacity: sb.pressed ? 0.7 : 1
            }
        }
    }


    PlayBackTapMenu
    {
        id: playBackTapMenu
        visible: false
        onSelectAll:
        {
            if( !root.state )
            {
                root.state = 1
                playBackTapMenu.changeSelectAllName("Отменить все")
            }
            else
            {
                root.state = 0
                playBackTapMenu.changeSelectAllName("Выбрать все")
            }
            root.selectAll()
        }
        onCopy:
        {
            root.copy()
        }
        onSelect:
        {
            var selected = lv.model.getSelectedState(currentRow)

            if( selected )
            {
                root.selectRowOnly(root.currentRow, "selected", 0, root.lastPosition)
                playBackTapMenu.changeSelectName("Выбрать")
            }
            else
            {
                root.selectRowOnly(root.currentRow, "selected", 1, root.lastPosition)
                playBackTapMenu.changeSelectName("Отменить выбор")
            }

        }
        onGraph:
        {
            root.graph()
        }
        onPlay:
        {
            root.play()
        }
    }



    Component
    {
        id: playlistHeader        

        Row
        {
           z:2
           spacing: 2

           Rectangle
           {
               height: root.headerHeight
               width: 50
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                   color: cc.light_green
                   text: qsTr("№")
               }
           }

           Rectangle
           {
               height: root.headerHeight
               width: 220
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                   color: cc.light_green
                   text: qsTr("Старт")
               }
           }

           Rectangle
           {
               height: root.headerHeight
               width: 220
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                   color: cc.light_green
                   text: qsTr("Стоп")
               }
           }

           Rectangle
           {
               height: root.headerHeight
               width: 230
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                   color: cc.light_green
                   text: qsTr("Продолжительность")
               }
           }

           Rectangle
           {
               height: root.headerHeight
               width: (1176 - 490 - 230 - 12)/3
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                   color: cc.light_green
                   text: qsTr("Размер (МБ)")
               }
           }

           Rectangle
           {
               height: root.headerHeight
               width: (1176 - 490 - 230 - 12)/3
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                   color: cc.light_green
                   text: qsTr("Канал")
               }
           }

           Rectangle
           {
                height: root.headerHeight
                width: (1176 - 490 - 230 - 12)/3
                color: cc.dark_blue

                Rectangle //Line
                {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
                }

                Text
                {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                   color: cc.light_green
                   text: qsTr("Выбор")
                }
            }
        }
    }



   Component
   {
       id: playlistRow

       Item
       {
           height: 58

           Row
           {
               id: rowElem
               height: parent.height
               spacing: 2

               Rectangle
               {
                   height: parent.height
                   width: 50
                   color:
                   {
                       if( current_play )
                       {
                           return "#79bd8f"
                       }
                       else if( selected )
                       {
                           return cc.highlight_green;
                       }
                       else
                       {
                           return cc.dark_blue;
                       }
                   }


                   Text
                   {
                       anchors.centerIn: parent
                       font.family: fontConf.mainFont
                       font.italic: true
                       font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                       color: cc.light_green
                       text: "" + (index + 1)
                   }
               }

               Rectangle
               {
                   height: parent.height
                   width: 220
                   color:
                   {
                       if( current_play )
                       {
                           return "#79bd8f"
                       }
                       else if( selected )
                       {
                           return cc.highlight_green;
                       }
                       else
                       {
                           return cc.dark_blue;
                       }
                   }
                   Text
                   {
                       anchors.centerIn: parent
                       font.family: fontConf.mainFont
                       font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                       color: cc.white
                       text: time_start
                   }
               }

               Rectangle
               {
                   height: parent.height
                   width: 220
                   color:
                   {
                       if( current_play )
                       {
                           return "#79bd8f"
                       }
                       else if( selected )
                       {
                           return cc.highlight_green;
                       }
                       else
                       {
                           return cc.dark_blue;
                       }
                   }
                   Text
                   {
                       anchors.centerIn: parent
                       font.family: fontConf.mainFont
                       font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                       color: cc.white
                       text: time_end
                   }
               }

               Rectangle
               {
                   height: parent.height
                   width: 230
                   color:
                   {
                       if( current_play )
                       {
                           return "#79bd8f"
                       }
                       else if( selected )
                       {
                           return cc.highlight_green;
                       }
                       else
                       {
                           return cc.dark_blue;
                       }
                   }
                   Text
                   {
                       anchors.centerIn: parent
                       font.family: fontConf.mainFont
                       font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                       color: cc.white
                       text: duration
                   }
               }

               Rectangle
               {
                   height: parent.height
                   width: (1176 - 490 - 230 - 12)/3
                   color:
                   {
                       if( current_play )
                       {
                           return "#79bd8f"
                       }
                       else if( selected )
                       {
                           return cc.highlight_green;
                       }
                       else
                       {
                           return cc.dark_blue;
                       }
                   }
                   Text
                   {
                       anchors.centerIn: parent
                       font.family: fontConf.mainFont
                       font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                       color: cc.white
                       text: size.toFixed(2)
                   }
               }

               Rectangle
               {
                   height: parent.height
                   width: (1176 - 490 - 230 - 12)/3
                   color:
                   {
                       if( current_play )
                       {
                           return "#79bd8f"
                       }
                       else if( selected )
                       {
                           return cc.highlight_green;
                       }
                       else
                       {
                           return cc.dark_blue;
                       }
                   }
                   Text
                   {
                       anchors.centerIn: parent
                       font.family: fontConf.mainFont
                       font.pixelSize: fontConf.pixSize_S6 + fontSizeAdj
                       color: cc.white
                       text: type
                   }
               }

               Rectangle
               {
                   id:checkRect
                   height: parent.height
                   width: (1176 - 490 - 230 - 12)/3
                   color:
                   {
                       if( current_play )
                       {
                           return "#79bd8f"
                       }
                       else if( selected )
                       {
                           return cc.highlight_green;
                       }
                       else
                       {
                           return cc.dark_blue;
                       }
                   }
                   CheckBox
                   {
                       id: checkBoxButton
                       property bool prevState : selected
                       anchors.fill: parent
                       checked: selected
                      // visible: checkEnable

                       onCheckedChanged:
                       {
                           if( checked != checkBoxButton.prevState )
                           {
                               checkBoxButton.prevState = checked
                               root.selectRowOnly(index, "selected", checked, root.lastPosition)// playlistModel.setProperty(index, "selected", checked)
                           }
                           else
                           {
//                               console.debug("KOKOKOK")
                           }
                       }

                       indicator: Rectangle {
                           anchors.centerIn: parent
                           implicitWidth: 35
                           implicitHeight: 35
                           color: cc.white
                           scale: checkBoxButton.down ? 0.9 : 1

                           Image
                           {
                               width: 30
                               height: 30
                               anchors.centerIn: parent
                               fillMode: Image.PreserveAspectFit
                               mipmap: true
                               source: "qrc:///icons/i34.png"
                               visible: checkBoxButton.checked
                           }
                       }
                   }
               }
           }


           MouseArea
           {
               height: rowElem.height
               width: rowElem.width - checkRect.width
               enabled: checkBoxButton.visible

               onPressAndHold:
               {
                   var pointerPos = mapToItem(root, mouseX, mouseY)
                   root.selectRow(index, "selected", true, pointerPos)
               }
           }
       }
   }


   Button
   {
       id:lockRect
       anchors.fill: back
       visible: true

       background: Rectangle
       {
           color: "#6c7980"
           opacity: 0.5
       }
   }


   BusyBlock
   {
       id:busyBlock
       anchors.fill: back
       rectSize: 200
       dotSize: 16
       blockButton: true
       visible: false
   }


   ListModel
   {
       id: playlistModel

       ListElement { startTime:"12:00:00"; startDate:"30.11.2019"; endTime:"13:01:00"; endDate:"1.12.2019"; duration:"00:01:01"; size:"600 Мб"; channel:"1-2 Стерео"; checkEnable:true; selected:false }
       ListElement { startTime:"12:00:00"; startDate:"30.11.2019"; endTime:"13:01:00"; endDate:"1.12.2019"; duration:"00:01:40"; size:"500 Мб"; channel:"3 Моно"; checkEnable:true; selected:false }
       ListElement { startTime:""; startDate:""; endTime:""; endDate:""; duration:""; size:""; channel:""; selected:false }
       ListElement { startTime:""; startDate:""; endTime:""; endDate:""; duration:""; size:""; channel:""; selected:false }
       ListElement { startTime:""; startDate:""; endTime:""; endDate:""; duration:""; size:""; channel:""; selected:false }
       ListElement { startTime:""; startDate:""; endTime:""; endDate:""; duration:""; size:""; channel:""; selected:false }
       ListElement { startTime:""; startDate:""; endTime:""; endDate:""; duration:""; size:""; channel:""; selected:false }
   }
}

