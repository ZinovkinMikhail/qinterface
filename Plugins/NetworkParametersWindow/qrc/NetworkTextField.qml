import QtQuick 2.0
import QtQuick.Controls 2.2
import Cicada 1.0

Item
{
    width: 450
    height: 45
    property string fieldName
    property string fieldText
    property string fieldType: ""
    property bool disableTexfield: false

    property var ipValidator: RegExpValidator {
        regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
    }
    property var macValidator: RegExpValidator {
        regExp:  /^((([a-fA-F0-9][a-fA-F0-9]+[-]){5}|([a-fA-F0-9][a-fA-F0-9]+[:]){5})([a-fA-F0-9][a-fA-F0-9])$)|(^([a-fA-F0-9][a-fA-F0-9][a-fA-F0-9][a-fA-F0-9]+[.]){2}([a-fA-F0-9][a-fA-F0-9][a-fA-F0-9][a-fA-F0-9]))$/
    }
    property var subnetValidator: RegExpValidator {
        regExp:  /^(((255\.){3}(255|254|252|248|240|224|192|128|0))|((255\.){2}(255|254|252|248|240|224|192|128|0)\.0)|((255\.)(255|254|252|248|240|224|192|128|0)(\.0){2})|((255|254|252|248|240|224|192|128|0+)(\.0){3}))$/
    }

    ColorConfig {id: cc}


    Text
    {
        anchors.verticalCenter: parent.verticalCenter
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S6
        color: cc.light_green
        text: fieldName
    }


    Component.onCompleted:
    {
        if(fieldType == "ipMask")
        {
            tField.validator = ipValidator
            tField.placeholderText = "0.0.0.0"
        }
        else if(fieldType == "macMask")
        {
            tField.validator = macValidator
        }
        else if(fieldType == "subnetMask")
        {
            tField.validator = subnetValidator
        }
        else if(fieldType == "password")
        {
            tField.echoMode = TextInput.Password
            tField.passwordCharacter
            tField.placeholderText = "**********"
        }
    }


    TextField
    {
        id:tField
        anchors.right: parent.right
        height: 45
        width: 280
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S6
        horizontalAlignment: Text.AlignHCenter
//        inputMask: fieldType == "ipMask" ? "000.000.000.000;_" : ""
        enabled: !disableTexfield
        text: parent.fieldText
        inputMethodHints: fieldType === "ipMask" ||  fieldType === "subnetMask"  ? Qt.ImhDigitsOnly : Qt.ImhNone

        background: Rectangle { color: "transparent" }

        Rectangle //background fix for yocto
        {
            anchors.fill: parent
            z:-1
            color: disableTexfield ? "#7f7f7f" : cc.white
        }

        onTextChanged:
        {
            parent.fieldText = text
        }

        ShadowRect{x: -4; y: 4}

        onPressed:
        {
            if(inputPanelY > pressedFieldY)
            {
                pressedFieldY = mapToItem(networkParametersWindow.parent.parent,x,y).y + 60
            }
        }

        onFocusChanged:
        {
            if(focus)
            {
                selectAll()
            }
        }
    }
}
