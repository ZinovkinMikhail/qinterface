import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0

Item
{
    id: networkParametersWindow
    y: 150
    width: 1280
    height: 484

    property var inputPanelY: parent ? parent.inputPanelY : 0
    property real pressedFieldY //for shift when virtual keyboard open

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal showComboDrum()
    signal applyParams()
    signal saveParams()
    signal cancelChanges()
    signal checkProtocolType()


    BackgroundTitle
    {
        text: qsTr("Параметры сети")
    }

    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 415
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }

    Rectangle
    {
        id:mainZone
        anchors.fill: background
        color: cc.dark_blue
        anchors.rightMargin: 25
        anchors.bottomMargin: 13
        anchors.leftMargin: 25
        anchors.topMargin: 13
        clip: true
    }

    Item
    {
        id:shiftZone
        parent: mainZone
        width: mainZone.width
        height: mainZone.height
        y:
        {
            if(inputPanelY < pressedFieldY)
            {
                inputPanelY - pressedFieldY
            }
            else
            {
                0
            }
        }

        SwipeView
        {
            id: view
            anchors.fill: parent
            objectName: "SwipeView"
            orientation: Qt.Vertical
            currentIndex: 0
            clip: true

            Item
            {
                height: view.height
                width: view.width

                TextCheckBox
                {
                    id: checkboxDHCP
                    checkName: "CheckBoxDHCP"
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.topMargin: 33
                    anchors.leftMargin: 56
                    rightCheckbox: false
                    text: qsTr("Автоматическое получение IP адреса (DHCP)")
                    spacing: 15

                    onCheckStateChanged:
                    {
                        for(var i = inputRows.children.length; i > 0 ; i--)
                        {
                          inputRows.children[i-1].disableTexfield = checked
                        }
                        for(var i = inputRows2.children.length; i > 0 ; i--)
                        {
                            if( inputRows2.children[i-1].objectName === "MAC" )
                                continue
                          inputRows2.children[i-1].disableTexfield = checked
                        }
//                        if( !checked )
//                        {
//                            networkParametersWindow.checkProtocolType()
//                        }
                        networkParametersWindow.checkProtocolType()
                    }
                }

                Item
                {
                    id: protocolButton
                    objectName: "ProtocolButton"
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.topMargin: 33
                    anchors.rightMargin: 89
                    width: 418
                    height: 50
                    property string protocolName: "TCP/IP"
                    property int protocolIndex : 0
                    Text
                    {
                        anchors.verticalCenter: parent.verticalCenter
                        font.family: fontConf.mainFont
                        font.pixelSize: fontConf.pixSize_S6
                        color: cc.light_green
                        text: qsTr("Протокол")
                    }
                    ButtonDrop
                    {
                        anchors.right: parent.right
                        txttxt: parent.protocolName
                        type: "default"
                        onButtonClick:
                        {
                            networkParametersWindow.showComboDrum()
                        }
                    }
                }


                Rectangle
                {
                    id:line
                    anchors.horizontalCenter: parent.horizontalCenter
                    y:30
                    height: 60
                    width: 2
                    color: cc.blue
                }

                Grid
                {
                    id:inputRows
                    objectName: "Grid"
                    y:130
                    anchors.horizontalCenter: parent.horizontalCenter
                    flow: Grid.TopToBottom
                    columns: 2
                    columnSpacing: 180
                    rowSpacing: 40

                    NetworkTextField { id: fieldIP ; objectName: "IpAddress"; fieldName: "IP адрес"; fieldType: "ipMask"}
                    NetworkTextField { id: fieldMASK ; objectName: "NetworkMask"; fieldName: "Маска подсети"; fieldType: "subnetMask"}
                    NetworkTextField { id: fieldGATE ; objectName: "Gate"; fieldName: "Шлюз"; fieldType: "ipMask"}
                    NetworkTextField { id: fieldSERVER ; objectName: "ServerIp"; fieldName: "Сервер"; fieldType: "ipMask"}
                    NetworkTextField { id: fieldLOGIN ; objectName: "Login" ; fieldName: "Логин"; fieldType: ""}
                    NetworkTextField { id: fieldPASS ; objectName: "Password"; fieldName: "Пароль"; fieldType: "password"}
                }
            }


            Item
            {
                height: view.height
                width: view.width

                Grid
                {
                    id:inputRows2
                    objectName: "Grid2"
                    y:50
                    anchors.horizontalCenter: parent.horizontalCenter
                    flow: Grid.TopToBottom
                    columns: 2
                    columnSpacing: 180
                    rowSpacing: 40

                    NetworkTextField { id: fieldDNS1 ; objectName: "DNS1"; fieldName: "DNS1"; fieldType: "ipMask"}
                    NetworkTextField { id: fieldDNS2 ; objectName: "DNS2"; fieldName: "DNS2"; fieldType: "ipMask"}
                    NetworkTextField { id: fieldMAC ; objectName: "MAC"; fieldName: "MAC-адрес"; fieldType: "macMask"}
                }
            }
        }
    }


    Item
    {
        parent: background
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: -6
        height: indicator.height
        width: indicator.width
        rotation: 90

        PageIndicator
        {
            id: indicator
            count: view.count
            currentIndex: view.currentIndex

            delegate:
                Rectangle
                {
                    implicitWidth: 10
                    implicitHeight: 10
                    radius: width / 2
                    border.color: cc.white
                    border.width: 1

                    color: index === indicator.currentIndex ? cc.blue : cc.white

                    Behavior on color
                    {
                        ColorAnimation
                        {
                            from: cc.white
                            duration: 200
                        }
                    }
                }
        }
    }


    Item
    {
        id:controlButtons
        anchors.top: background.bottom
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        width: 480

        ControlListButton
        {
            anchors.right: parent.right
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked:
            {
                networkParametersWindow.cancelChanges()
            }
        }

//        ControlListButton
//        {
//            x: parent.width/2 - width/2
//            type: "SlimType"
//            text: qsTr("Применить")
//            onClicked:
//            {
//                networkParametersWindow.applyParams()
//            }
//        }
        ControlListButton
        {
            anchors.left: parent.left
            type: "SlimType"
            text: qsTr("Сохранить")
            onClicked:
            {
                networkParametersWindow.saveParams()
            }
        }
    }



}

