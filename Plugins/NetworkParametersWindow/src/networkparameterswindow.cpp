#include <assert.h>

#include <QQmlComponent>
#include <bitset>

#include "networkparameterswindow.h"

using namespace adapter_status_ctrl;
using namespace rosslog;

NetworkParametersWindow::NetworkParametersWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    dataProvider(nullptr),
    adapterStatusCtrl(nullptr),
    adapterSettingsControl(nullptr),
    userManipulators(nullptr)
{  
    memset(&adapterStatus, 0, sizeof(SoftAdapterDeviceCurrentStatus_t));

    // init combo drum model
#if __V1
    comboDrumModel = {"PPTP", "PPPOE", "TCP/IP"};
#else
    comboDrumModel = {"TCP/IP"};
#endif
}

std::shared_ptr<IUserManipulators> NetworkParametersWindow::initWindow(EWindSizeType winSizeType,
                                                                      std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                      std::shared_ptr<IStateWidget> stateWidget,
                                                                      std::shared_ptr<IUserManipulators> userManipulators,
                                                                      std::shared_ptr<IQmlEngine> engine,
                                                                      std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);
    Q_UNUSED(userManipulators);
    assert(engine);
    assert(userManipulators);

    this->dataProvider = dataProvider;
    this->engine = engine;
    this->logger = log;
    this->userManipulators = userManipulators;

    windowComponent = engine->createComponent(NETWORK_PARAMETERS_WINDOW_QML_PATH);
    assert(windowComponent);
    gridFirstPage = windowComponent->findChild<QQuickItem*>("Grid");
    assert(gridFirstPage);
    gridSecondPage = windowComponent->findChild<QQuickItem*>("Grid2");
    assert(gridSecondPage);
    loginItem = gridFirstPage->findChild<QQuickItem*>("Login");
    assert(loginItem);
    gateItem = gridFirstPage->findChild<QQuickItem*>("Gate");
    assert(gateItem);
    serverIpItem = gridFirstPage->findChild<QQuickItem*>("ServerIp");
    assert(serverIpItem);
    networkMaskItem = gridFirstPage->findChild<QQuickItem*>("NetworkMask");
    assert(networkMaskItem);
    passwordItem = gridFirstPage->findChild<QQuickItem*>("Password");
    assert(passwordItem);
    dns1Item = gridSecondPage->findChild<QQuickItem*>("DNS1");
    assert(dns1Item);
    dns2Item = gridSecondPage->findChild<QQuickItem*>("DNS2");
    assert(dns2Item);
    macItem = gridSecondPage->findChild<QQuickItem*>("MAC");
    assert(macItem);
    checkBoxItem = windowComponent->findChild<QQuickItem*>("CheckBoxDHCP");
    assert(checkBoxItem);
    protocolButtonItem = windowComponent->findChild<QQuickItem*>("ProtocolButton");
    assert(protocolButtonItem);
    ipAddressItem = gridFirstPage->findChild<QQuickItem*>("IpAddress");
    assert(ipAddressItem);

    initAdapterStatusControl();
    initAdapterSettingsControl();

    connect(this, SIGNAL(signalRecivedAdapterData()),
            this, SLOT(slotRecievedAdapterData()));
    connect(windowComponent, SIGNAL(showComboDrum()),
            this, SLOT(slotShowComboDrum()));
    connect(windowComponent, SIGNAL(applyParams()),
            this, SLOT(slotApplyParams()));
    connect(windowComponent, SIGNAL(saveParams()),
            this, SLOT(slotSaveParams()));
    connect(windowComponent, SIGNAL(cancelChanges()),
            this, SLOT(slotCancelChanges()));
    connect(windowComponent, SIGNAL(checkProtocolType()),
            this, SLOT(slotCheckProtocolType()));

    return nullptr;
}

QObject *NetworkParametersWindow::getComponentObject()
{
    return windowComponent;
}

std::string NetworkParametersWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string NetworkParametersWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string NetworkParametersWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string NetworkParametersWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string NetworkParametersWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap NetworkParametersWindow::getIcon()
{
    return QPixmap(":/NetworkParams/icons/i3.png");
}

std::pair<bool, QPixmap> NetworkParametersWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/NetworkParams/icons/i19.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > NetworkParametersWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool NetworkParametersWindow::showWindow()
{
    this->updateData();
    return true;
}

bool NetworkParametersWindow::hideWindow()
{
    return  true;
}

EWindSizeType NetworkParametersWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool NetworkParametersWindow::prepareToDelete()
{
    return true;
}

void NetworkParametersWindow::initAdapterStatusControl()
{
    if( adapterStatusCtrl == nullptr )
    {
        AdapterStatusControl *statCtrl = new AdapterStatusControl(dataProvider, *logger.get());
        adapterStatusCtrl = std::shared_ptr<AdapterStatusControl>(statCtrl);
    }
}

void NetworkParametersWindow::initAdapterSettingsControl()
{
    if( adapterSettingsControl == nullptr )
    {
        adapterSettingsControl = std::shared_ptr<AdapterSettingsControl>(
                new AdapterSettingsControl(dataProvider, *logger.get()));
    }
}

void NetworkParametersWindow::getInfoFromAdapter()
{
    this->userManipulators->showLoadingWidget();
    try
    {
        dataMutex.lock();

        adapterStatusCtrl->updateStatus();
        this->adapterStatus = adapterStatusCtrl->getAdapterStatus();

        this->adapterSettingsControl->updateSettings();
        this->adapterSettings = adapterSettingsControl->getAdapterSettings().Block0.AdapterSettings;
        dataMutex.unlock();
        emit signalRecivedAdapterData();
        this->userManipulators->hideLoadingWidget();
    }
    catch( std::system_error &systemErr )
    {
        this->userManipulators->hideLoadingWidget();
        dataMutex.unlock();
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        userManipulators->showMessage(std::string("Не удалось получить данные.\nКод ошибки: ").append(
                                          std::to_string(systemErr.code().value())));
    }
}

void NetworkParametersWindow::changeDesignByProtocolType(const TProtocolType& protocolType)
{

    LOG(logger, LOG_INFO, "=========== set protocolType=%d", protocolType );

    switch(protocolType){
#if 0
        case ptPPTP:{
            QQmlProperty(serverIpItem, "disableTexfield").write(false);
            QQmlProperty(loginItem,    "disableTexfield").write(false);
            QQmlProperty(passwordItem, "disableTexfield").write(false);
        } break;
        case ptPPPOE:{
            QQmlProperty(serverIpItem, "disableTexfield").write(true );
            QQmlProperty(loginItem,    "disableTexfield").write(false);
            QQmlProperty(passwordItem, "disableTexfield").write(false);
        } break;
        case ptWIREGUARD:{
            QQmlProperty(serverIpItem, "disableTexfield").write(false );
            QQmlProperty(loginItem,    "disableTexfield").write(true );
            QQmlProperty(passwordItem, "disableTexfield").write(true );
        } break;
#endif
        case ptTCP_IP:{
            QQmlProperty(serverIpItem, "disableTexfield").write(true );
            QQmlProperty(loginItem,    "disableTexfield").write(true );
            QQmlProperty(passwordItem, "disableTexfield").write(true );
        } break;
    }

    try
    {
        QQmlProperty(protocolButtonItem, "protocolName").write(
                    QString::fromStdString(comboDrumModel.at(protocolType)));
        QQmlProperty(protocolButtonItem, "protocolIndex").write(
                    static_cast<int>(protocolType));

    }
    catch (std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__,
                              "Error while setting protocol name: %s", except.what());
    }


}

TProtocolType NetworkParametersWindow::getProtocolType(const uint32_t &adapterSettingsCon)
{
    TProtocolType res = ptTCP_IP;
    
    switch( adapterSettingsCon & AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE ) {
#if 0
        case AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPTP: {
            res = ptPPTP;
        } break;
        case AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPPOE: {
            res = ptPPPOE;
        } break;
        case AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_WIREGUARD: {
            res = ptWIREGUARD;
        } break;
#endif
        case AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_TCP_IP:
        default: {
            res = ptTCP_IP;
        }
    }
    
    return res;
}

std::pair<bool, uint32_t> NetworkParametersWindow::getValidIp(QString ip)
{
    if(ip.length() == 0)
    {
        return std::make_pair(false, 0);
    }

    char c_ip[32] = {0};
    uint32_t ipNum;

    memcpy(&c_ip, ip.toLocal8Bit().data(),sizeof(c_ip));
    ipNum = axis_create_ip(c_ip);

    if(ip <= 0)
    {
        return std::make_pair(false, 0);
    }
    return std::make_pair(true, ipNum);
}

void NetworkParametersWindow::updateDataOnAdapter()
{
    try
    {
        sleep(2);
        dataMutex.lock();
        adapterSettingsControl->applyAdapterSettings(adapterSettings);
        dataMutex.unlock();
    }
    catch( std::system_error &except )
    {
        dataMutex.unlock();
        userManipulators->showMessage(std::string("Не удалось применить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

void NetworkParametersWindow::saveDataOnAdapter()
{
    try
    {
        sleep(2);
        dataMutex.lock();
        adapterSettingsControl->saveAdapterSettings(adapterSettings);
        dataMutex.unlock();
        userManipulators->showMessage(std::string("Изменения втупят в силу после перезагрузки устройства."));
    }
    catch( std::system_error &except )
    {
        dataMutex.unlock();
        userManipulators->showMessage(std::string("Не удалось сохранить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

void NetworkParametersWindow::updateData()
{
    updateWorker = std::async(std::launch::async,
                              &NetworkParametersWindow::getInfoFromAdapter, this);
    updateWorker.wait_for(std::chrono::seconds(0));
}

bool NetworkParametersWindow::packNetworkParams(SoftAdapterSettingsStruct_t &settings)
{
    uint32_t ipAddr = 0;
    uint32_t maskAddr = 0;
    uint8_t macAddr[128] = {0};

    bool dhcpChecked = QQmlProperty(checkBoxItem, "checked").read().toBool();
    settings.Connect = 0x00;

    if( !dhcpChecked )
    {
        settings.Connect |= AXIS_SOFTADAPTER_CONNECT__IP_STATIC;
    }
    else
    {
        settings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__IP_STATIC;
    }

    TProtocolType type = static_cast<TProtocolType>(QQmlProperty(protocolButtonItem, "protocolIndex").read().toInt());

    switch(type){
#if 0
        case ptPPTP: {
            settings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE;
            settings.Connect |= AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPTP;
        } break;
        case ptPPPOE: {
            settings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE;
            settings.Connect |= AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_PPPOE;
            
        } break;
        case ptWIREGUARD: {
            settings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE;
            settings.Connect |= AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_WIREGUARD;
        } break;
#endif
        case ptTCP_IP: {
            settings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE;
            settings.Connect |= AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_TCP_IP;
        } break;
        default: {
            settings.Connect &= ~AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE;
            settings.Connect |= AXIS_SOFTADAPTER_CONNECT__PROTOCOL_TYPE_TCP_IP;
        }
    }


    if(!dhcpChecked)
    {
        std::pair<bool, uint32_t> res = getValidIp(QQmlProperty(ipAddressItem, "fieldText").read().toString());

        if( !res.first )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid IP address");
            userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля IP адрес."));
            return  false;
        }

        ipAddr = res.second;;

        res = getValidIp(QQmlProperty(networkMaskItem, "fieldText").read().toString());

        if( !res.first )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid NetworkMask address");
            userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля Маска подсети."));
            return  false;
        }

        maskAddr = res.second;

        // Check ip address is empty
        if( axis_is_ip_empty(ipAddr) == 1 )
        {
           this->logger->Message(LOG_ERR, __LINE__, __func__, "ip address is empty");
           userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля IP адрес."));
           return  false;
        }

        // Check ip address is full
        if( axis_is_ip_full(ipAddr) == 1 )
        {
          this->logger->Message(LOG_ERR, __LINE__, __func__, "ip address is full");
          userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля IP адрес."));
          return  false;
        }

        // Check ip address third octet not 4 (its USB network)
        if(getIpOctet(ipAddr, 1) == 4)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "ip address = .4");
            userManipulators->showMessage(std::string("IP адрес не может быть установлен в четвертой подсети."));
            return  false;
        }

        // Check mask is valid
        if( axis_is_ip_mask(maskAddr) == 0 )
        {
          this->logger->Message(LOG_ERR, __LINE__, __func__, "mask is not valid");
          userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля Маска подсети."));
          return  false;
        }

         // Check ip address != network address
        if(ipAddr == (ipAddr & maskAddr))
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "ip address == network address");
            userManipulators->showMessage(std::string("IP адрес не может быть равен адресу сети."));
            return  false;
        }

        // Check ip address != broadcast address
       if(ipAddr == (ipAddr | ( ~ maskAddr )))
       {
           this->logger->Message(LOG_ERR, __LINE__, __func__, "ip address == broadcast address");
           userManipulators->showMessage(std::string("IP адрес не может быть широковещательным."));
           return  false;
       }

        settings.IPAddress = ipAddr;
        settings.NetMask = maskAddr;


        res = getValidIp(QQmlProperty(gateItem, "fieldText").read().toString());

        if( !res.first )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid GateWay address");
            userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля Шлюз."));
            return  false;
        }

        // Check gateway address is empty
        if( axis_is_ip_empty(res.second) == 1 )
        {
          this->logger->Message(LOG_ERR, __LINE__, __func__, "gateway address is empty");
          userManipulators->showMessage(std::string("Адрес шлюза не может быть пустым."));
        }
        else if( axis_is_ip_in_network(res.second, ipAddr, maskAddr) == 0 ) // Check gateway address belongs to the network
        {
          this->logger->Message(LOG_ERR, __LINE__, __func__, "gateway address out of network");
          userManipulators->showMessage(std::string("Шлюз не принадлежит сети устройства."));
          return  false;
        }

        settings.GWAddress = res.second;


        res = getValidIp(QQmlProperty(dns1Item, "fieldText").read().toString());

        if( !res.first )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid DNS1 address");
            userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля DNS1."));
            return  false;
        }

        settings.DNS1Address = res.second;


        res = getValidIp(QQmlProperty(dns2Item, "fieldText").read().toString());

        if( !res.first )
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid DNS2 address");
            userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля DNS2."));
            return  false;
        }

        settings.DNS2Address = res.second;
    }


    QString mac = QQmlProperty(macItem, "fieldText").read().toString();

    if(axis_create_mac(macAddr, mac.toLatin1().data() ) != 0)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid mac");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля MAC-адрес."));
        return false;
    }

    // Check mac is empy
    if( axis_is_mac_empty(macAddr) == 1 )
    {
      this->logger->Message(LOG_ERR, __LINE__, __func__, "mac is empy");
      userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля MAC-адрес."));
      return  false;
    }

    // Check mac is broadcast
    if( axis_is_mac_broadcast(macAddr) == 1 )
    {
      this->logger->Message(LOG_ERR, __LINE__, __func__, "mac is broadcast");
      userManipulators->showMessage(std::string("MAC-адрес не может быть широковещательным."));
      return  false;
    }

    axis_create_mac(adapterSettings.HWAddress, mac.toLatin1().data()); //already check


    QString userLogin = QQmlProperty(loginItem, "fieldText").read().toString();

    if( type != ptTCP_IP && userLogin.isEmpty() )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid Login is empty");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля Логин."));
        return  false;
    }

    strncpy(settings.VPNUser, userLogin.toLocal8Bit().data(), sizeof (settings.VPNUser));


    QString password = QQmlProperty(passwordItem, "fieldText").read().toString();
    
#if __V1
    if( type == ptPPTP && password.isEmpty() )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid password is empty");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля Пароль."));
        return  false;
    }
#endif

    strncpy(settings.VPNPasswd, password.toLocal8Bit().data(), sizeof (settings.VPNPasswd));

    QString server = QQmlProperty(serverIpItem, "fieldText").read().toString();

    if( type != ptTCP_IP && server.isEmpty() )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid server is empty");
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных поля Сервер."));

        return  false;
    }

    strncpy(settings.VPNServer, server.toLocal8Bit().data(), sizeof (settings.VPNServer));


    return  true;
}

void NetworkParametersWindow::slotRecievedAdapterData()
{
    char buff[32] = {0};
    bool isDHCP;

    if(adapterSettings.Connect & AXIS_SOFTADAPTER_CONNECT__IP_STATIC)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Static ip selected");
        QQmlProperty(checkBoxItem, "checked").write(false);
        isDHCP = false;
    }
    else
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "DHCP selected");
        QQmlProperty(checkBoxItem, "checked").write(true);
        isDHCP = true;
    }   

    currentProtocolType = getProtocolType(adapterSettings.Connect);
    changeDesignByProtocolType(currentProtocolType);

    QQmlProperty(loginItem, "fieldText").write(QString(adapterSettings.VPNUser));

    QQmlProperty(passwordItem, "fieldText").write(QString(adapterSettings.VPNPasswd));


    if(     ( !isDHCP && !axis_sprint_ip(buff, adapterSettings.IPAddress ) )
         || (  isDHCP && !axis_sprint_ip(buff, adapterStatus.DHCP.ip_addr) ) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Canno't convert ip to char");
    }
    else
        QQmlProperty(ipAddressItem, "fieldText").write(QString(buff));

    if(     ( !isDHCP && !axis_sprint_ip(buff, adapterSettings.GWAddress) ) 
        ||  (  isDHCP && !axis_sprint_ip(buff, adapterStatus.DHCP.gw_addr) ) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Canno't convert gateway to char");
    }
    else
        QQmlProperty(gateItem, "fieldText").write(QString(buff));

    if(     ( !isDHCP && !axis_sprint_ip(buff, adapterSettings.NetMask) ) 
        ||  (  isDHCP && !axis_sprint_ip(buff, adapterStatus.DHCP.ip_mask) ) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Canno't convert netmask to char");
    }
    else
        QQmlProperty(networkMaskItem, "fieldText").write(QString(buff));


    if( !axis_sprint_ip(buff, adapterSettings.DNS1Address) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Canno't convert dns 1 to char");
    }
    else
        QQmlProperty(dns1Item, "fieldText").write(QString(buff));  

    if( !axis_sprint_ip(buff, adapterSettings.DNS2Address) )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Canno't convert dns 2 to char");
    }
    else
        QQmlProperty(dns2Item, "fieldText").write(QString(buff));

    QQmlProperty(serverIpItem, "fieldText").write(QString(adapterSettings.VPNServer));

    if(!axis_sprint_eth(buff, adapterSettings.HWAddress))
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Canno't convert mac to char");
    }
    else
        QQmlProperty(macItem, "fieldText").write(QString(buff));

}


void NetworkParametersWindow::slotShowComboDrum()
{
    std::pair<bool, int> res = userManipulators->openComboDrum(currentProtocolType, comboDrumModel);
    if( res.first )
    {
        currentProtocolType = static_cast<TProtocolType>(res.second);
        changeDesignByProtocolType(currentProtocolType);
    }
}

void NetworkParametersWindow::slotApplyParams()
{
    if( packNetworkParams(adapterSettings) )
    {
        userManipulators->showLoadingWidget();
        updateWorker = std::async(std::launch::async,
                                  &NetworkParametersWindow::updateDataOnAdapter,
                                  this);
        updateWorker.wait_for(std::chrono::seconds(0));
    }
    else
    {
        userManipulators->showMessage(std::string("Проверьте корректность введенных данных."));
    }
}

void NetworkParametersWindow::slotSaveParams()
{   
    if( packNetworkParams(adapterSettings) )
    {
        userManipulators->showLoadingWidget();
        updateWorker = std::async(std::launch::async,
                                  &NetworkParametersWindow::saveDataOnAdapter,
                                  this);
        updateWorker.wait_for(std::chrono::seconds(0));        
    }
}

void NetworkParametersWindow::slotCancelChanges()
{
    updateData();
}

void NetworkParametersWindow::slotCheckProtocolType()
{
    currentProtocolType = getProtocolType(adapterSettings.Connect);
    changeDesignByProtocolType(currentProtocolType);

    UpdateConnectionFields();
}

uint8_t NetworkParametersWindow::getIpOctet(uint32_t ipAddr, int octetNum)
{
    uint8_t bytes[4];
    bytes[0] = ipAddr & 0xFF;
    bytes[1] = (ipAddr >> 8) & 0xFF;
    bytes[2] = (ipAddr >> 16) & 0xFF;
    bytes[3] = (ipAddr >> 24) & 0xFF;

    return bytes[octetNum];
}

void NetworkParametersWindow::UpdateConnectionFields()
{
    if(!QQmlProperty(checkBoxItem, "checked").read().toBool())
    {
        char buff[32] = {0};

        if( axis_is_ip_empty(adapterSettings.IPAddress) == 1 &&
            axis_is_ip_empty(adapterStatus.DHCP.ip_addr) == 1 )
        {
            axis_sprint_ip(buff, AXIS_SOFTADAPTER_IP_ADDRESS__DEFAULT);
            QQmlProperty(ipAddressItem, "fieldText").write(QString(buff));

            axis_sprint_ip(buff, AXIS_SOFTADAPTER_NET_MASK__DEFAULT);
            QQmlProperty(networkMaskItem, "fieldText").write(QString(buff));

            axis_sprint_ip(buff, AXIS_SOFTADAPTER_GW_ADDRESS__DEFAULT);
            QQmlProperty(gateItem, "fieldText").write(QString(buff));
        }
        else if ( axis_is_ip_empty(adapterStatus.DHCP.ip_addr) == 1 )
        {
            std::pair<bool, uint32_t> res = getValidIp(QQmlProperty(ipAddressItem, "fieldText").read().toString());

            if( ( res.first && axis_is_ip_empty(res.second) == 1 ) || !res.first)
            {
                axis_sprint_ip(buff, adapterSettings.IPAddress);
                QQmlProperty(ipAddressItem, "fieldText").write(QString(buff));

                axis_sprint_ip(buff, adapterSettings.NetMask);
                QQmlProperty(networkMaskItem, "fieldText").write(QString(buff));

                axis_sprint_ip(buff, adapterSettings.GWAddress);
                QQmlProperty(gateItem, "fieldText").write(QString(buff));
            }
        }
    }
}

NetworkParametersWindow::~NetworkParametersWindow()
{
    std::cerr << "Removed NetworkParametersWindow" << std::endl;
}
