#ifndef SETTINGSPARAMSWINDOW_H
#define SETTINGSPARAMSWINDOW_H

#include <future>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlProperty>
#include <QTextCodec>

#include <axis_ifconfig.h>
#include <axis_softadapter.h>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/adaptersettingscontrol.h>
#include <dataprovider/adapterstatuscontrol.h>

#define WINDOW_IDENTIFICATOR "NetworkParameter"
#define WINDOW_FOLDER_NAME "Adapter"
#define ICON_PRESENTATION_NAME "Сеть"
#define FOLDER_PRESENTATION_NAME "Адаптер"

enum TProtocolType
{
    //ptPPTP,
    //ptPPPOE,
    ptTCP_IP,
};

#define NETWORK_PARAMETERS_WINDOW_QML_PATH "qrc:/NetworkParameters/NetworkParametersWindow.qml"

class NetworkParametersWindow : public QObject, public IEmptyPlaceWindow
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "NetworkParametersWindow")
public:
    explicit NetworkParametersWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~NetworkParametersWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;

private:
    void initAdapterStatusControl();
    void initAdapterSettingsControl();
    void getInfoFromAdapter();
    void changeDesignByProtocolType(const TProtocolType &protocolType);
    TProtocolType getProtocolType(const uint32_t &adapterSettingsCon);
    std::pair<bool, uint32_t> getValidIp(QString ip);
    void updateDataOnAdapter();
    void saveDataOnAdapter();
    void updateData();
    bool packNetworkParams(SoftAdapterSettingsStruct_t &settings);
    uint8_t getIpOctet(uint32_t ipAddr, int octetNum);
    void UpdateConnectionFields();

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusCtrl;
    std::shared_ptr<AdapterSettingsControl> adapterSettingsControl;
    SoftAdapterSettingsStruct_t adapterSettings;
    SoftAdapterDeviceCurrentStatus_t adapterStatus;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::vector<std::string> comboDrumModel;
    std::future<void> updateWorker;
    std::mutex dataMutex;
    TProtocolType currentProtocolType;

private:
    QQuickItem *gridFirstPage;
    QQuickItem *gridSecondPage;
    QQuickItem *ipAddressItem;
    QQuickItem *gateItem;
    QQuickItem *serverIpItem;
    QQuickItem *networkMaskItem;
    QQuickItem *loginItem;
    QQuickItem *passwordItem;
    QQuickItem *dns1Item;
    QQuickItem *dns2Item;
    QQuickItem *macItem;
    QQuickItem *checkBoxItem;
    QQuickItem *protocolButtonItem;



signals:
    void signalRecivedAdapterData();
private slots:
    void slotRecievedAdapterData();
    void slotShowComboDrum();
    void slotApplyParams();
    void slotSaveParams();
    void slotCancelChanges();
    void slotCheckProtocolType();
};

QT_BEGIN_NAMESPACE

#define NetworkParametersWindowPluginInterface "NetworkParametersWindow"

Q_DECLARE_INTERFACE(NetworkParametersWindow, NetworkParametersWindowPluginInterface)

QT_END_NAMESPACE

#endif // MONITORINGWINDOWPLUGIN_H
