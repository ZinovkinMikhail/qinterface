#include <assert.h>

#include <QQmlComponent>
#include <QQuickItem>

#include "recordertimerparamswindow.h"

using namespace recorder_status_ctrl;
using namespace rosslog;

RecorderTimerParamsWindow::RecorderTimerParamsWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    recorderStatusCtrl(nullptr),
    recorderSettingsCtrl(nullptr),
    timerParamsSection(nullptr)
{
    memset(&recordSettings, 0, sizeof(SoftRecorderSettingsStruct_t));
    memset(&recordStatus, 0, sizeof(SoftRecorderStatusStruct_t));
    memset(&recordTimers, 0, sizeof(DspRecorderExchangeTimerSettingsStruct_t));

    connect(this, SIGNAL(signalSettingsApplyed()),
            this, SLOT(slotSettingsApplyed()));
}

std::shared_ptr<IUserManipulators> RecorderTimerParamsWindow::initWindow(EWindSizeType winSizeType,
                                                                    std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                    std::shared_ptr<IStateWidget> stateWidget,
                                                                    std::shared_ptr<IUserManipulators> userManipulators,
                                                                    std::shared_ptr<IQmlEngine> engine,
                                                                    std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);

    assert(userManipulators);

    dataProvider->getConnectStatus();

    this->engine = engine;
    this->userManipulators = userManipulators;
    this->dataProvider = dataProvider;
    this->logger = log;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(RECORDER_TIMERS_QML_PATH);
        assert(windowComponent);
    }

    initRecorderStatusControl();
    initRecorderSettingsControl();
    initTimersSection();

    connect(this, SIGNAL(signalUpdateLocalForms()),
            this, SLOT(slotUpdateLocalForms()), Qt::QueuedConnection);

    return nullptr;
}

QObject *RecorderTimerParamsWindow::getComponentObject()
{
    return windowComponent;
}

std::string RecorderTimerParamsWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string RecorderTimerParamsWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string RecorderTimerParamsWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string RecorderTimerParamsWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string RecorderTimerParamsWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap RecorderTimerParamsWindow::getIcon()
{
    return QPixmap(":/icons/i25.png");
}

std::pair<bool, QPixmap> RecorderTimerParamsWindow::getFolderPixmap()
{
#warning Set Pair
    return std::pair<bool, QPixmap>(false,QPixmap(":/icons/i18.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > RecorderTimerParamsWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool RecorderTimerParamsWindow::showWindow()
{
    updateData();
    return false;
}

bool RecorderTimerParamsWindow::hideWindow()
{
    return false;
}

EWindSizeType RecorderTimerParamsWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool RecorderTimerParamsWindow::prepareToDelete()
{
    return true;
}

void RecorderTimerParamsWindow::saveTimers(std::list<dsp_timer> timers)
{
    userManipulators->showLoadingWidget();

    updateWorker = std::async(std::launch::async,
                              std::bind(&RecorderTimerParamsWindow::updateDataOnAdapter,
                                        this,  std::placeholders::_1), timers);
    updateWorker.wait_for(std::chrono::seconds(0));
}

void RecorderTimerParamsWindow::initRecorderStatusControl()
{
    if( recorderStatusCtrl == nullptr )
    {
        RecorderStatusControl *statCtrl = new RecorderStatusControl(dataProvider, *logger.get());
        recorderStatusCtrl = std::shared_ptr<RecorderStatusControl>(statCtrl);
    }
}

void RecorderTimerParamsWindow::initRecorderSettingsControl()
{
    if( recorderSettingsCtrl == nullptr )
    {
        RecorderSettingsContol *setCtrl = new RecorderSettingsContol(dataProvider, *logger.get());
        recorderSettingsCtrl = std::shared_ptr<RecorderSettingsContol>(setCtrl);
    }
}

void RecorderTimerParamsWindow::updateData()
{
    updateWorker = std::async(std::launch::async,
                              &RecorderTimerParamsWindow::getInfoFromAdapter, this);
}

void RecorderTimerParamsWindow::getInfoFromAdapter()
{
    try
    {
//        dataMutex.lock();
        recorderSettingsCtrl->updateStatus();
        recorderStatusCtrl->updateStatus();
        this->recordSettings = recorderSettingsCtrl->getRecordSettings();
        this->recordStatus = recorderStatusCtrl->getRecorderStatus();
        recorderSettingsCtrl->updateTimers();
        this->recordTimers = recorderSettingsCtrl->getRecorderTimers();
        emit signalUpdateLocalForms();
//        dataMutex.unlock();
    }
    catch( std::system_error &systemErr )
    {
//        dataMutex.unlock();
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        userManipulators->showMessage(std::string("Не удалось получить данные.\nКод ошибки: ").append(
                                          std::to_string(systemErr.code().value())));
    }
    catch( std::runtime_error &exceptr )
    {

    }
}

void RecorderTimerParamsWindow::updateDataOnAdapter(std::list<dsp_timer> timers)
{
    try
    {
        memset(recordTimers.Timers, 0 , sizeof(recordTimers.Timers));
        int index = 0;
        for(auto &it : timers )
        {
            recordTimers.Timers[index] = it;
            index++;
        }

        recorderSettingsCtrl->saveRecorderTimers(recordTimers);
        getInfoFromAdapter();
        //emit slotSettingsApplyed();
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR,__LINE__,__func__, except.what());
        userManipulators->showMessage(std::string("Не удалось обновить данные.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
        Q_UNUSED(except);
#warning add error
    }
    this->userManipulators->hideLoadingWidget();
}

void RecorderTimerParamsWindow::initTimersSection()
{
    if(timerParamsSection == nullptr)
    {
        timerParamsSection = std::shared_ptr<TimerParamsSection>(
                    new TimerParamsSection(this, userManipulators,
                                           engine, logger, windowComponent));
        timerParamsSection->init();
    }
}


void RecorderTimerParamsWindow::slotUpdateLocalForms()
{
    this->logger->Message(LOG_ERR, __LINE__, __func__, "Timers count %i", MAX_NUM_TIMERS);
    int timerscount  = MAX_NUM_TIMERS;

    std::list<dsp_timer> timers;
    for(int i = 0; i < timerscount;i++)
    {
        if( recordTimers.Timers[i].stop.reserv_0 != TIMER_FLAG_DISABLE &&
                recordTimers.Timers[i].stop.reserv_0 != 0)
        {
            //recordTimers.Timers[i].stop.reserv_0 = (unsigned char)(recordStatus.TimerStatus[i] >> 8);
            this->logger->Message(LOG_ERR, __LINE__, __func__, "Push timer %i", i);
            printf("PUSH Timer flag = %x\n", recordTimers.Timers[i].stop.reserv_0);
            timers.push_back(recordTimers.Timers[i]);
        }

    }

    currentTimersList = timers;
    timerParamsSection->updateTimers(timers);
}

void RecorderTimerParamsWindow::slotSettingsApplyed()
{
    updateData();
}

RecorderTimerParamsWindow::~RecorderTimerParamsWindow()
{
    std::cerr << "Removed RecorderTimerParamsWindow" << std::endl;
}
