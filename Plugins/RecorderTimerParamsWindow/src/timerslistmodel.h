#ifndef TIMERSLISTMODEL_H
#define TIMERSLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QStringList>

#include "timerparamsmodel.h"



enum Roles
{
    ColorRole = Qt::UserRole + 1,
    TimeStart,
    TimeEnd,
    Duration,
    Selected,
    ColorType
};

class TimersListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit TimersListModel(QObject *parent = nullptr);

    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;

    void appendModel(TimersModel model);
    void clearModel();
    void updateModel();
    void setProperty(int index, QString type, int val);
    void selectAllRows();
    void getSelectedRows(std::vector<int> &selected);
    TimersModel getTimer(int index);
private:

    QList<TimersModel> listData;
signals:

};

#endif // TIMERSLISTMODEL_H
