#ifndef ADDSESSIONWIDGET_H
#define ADDSESSIONWIDGET_H

#include <memory>

#include <QObject>
#include <QQuickItem>
#include <QQmlProperty>
#include <QDateTime>
#include <QVariant>

#include <axis_time.h>

#include <iqmlengine.h>
#include <iusermanipulators.h>
#include "timerparamsmodel.h"
#include "iaddsession.h"

#define ADD_SESSION_WIDGET_QML_PATH "qrc:/RecorderTimers/AddSession.qml"
#define DATE_TIME_FORMAT "dd.MM.yyyy hh:mm"
#define DATE_FORMAT "dd.MM.yyyy"
#define TIME_FORMAT "hh:mm"

typedef enum TimeType
{
    START_TIME,
    END_TIME
} TimeType;

typedef enum
{
    START_DATE,
    END_DATE
} DateType;

typedef enum
{
    CHANGE,
    NEW
} SessionType;

class AddSessionWidget : public QObject
{
    Q_OBJECT
public:
    explicit AddSessionWidget(std::shared_ptr<IAddSession> addSessionInterface,
                              std::shared_ptr<IUserManipulators> userManipulators,
                              std::shared_ptr<IQmlEngine> qmlEngine, QObject *parent = nullptr);

    void init();
    void show();
    void changeTimer(TimersModel timer, int index);
    void copyTimer(TimersModel timer);

private:
    bool getCheckStateOfItem(QQuickItem *item);
    void setCheckStateItem(QQuickItem *item, bool value);
    void setDateStart(QQuickItem *item, QString startDate);
    void setDateEnd(QQuickItem *item, QString endDate);
    void setTimeStart(QQuickItem *item, QString startTime);
    void setTimeEnd(QQuickItem *item, QString endTime);
    void setDaysParams(uint16_t days);
    void setUseStartDay(QQuickItem *item, bool value);
    void setUseEndDay(QQuickItem *item, bool value);
    QString getStartDate(QQuickItem *item);
    QString getEndDate(QQuickItem *item);
    QString getStartTime(QQuickItem *item);
    QString getEndTime(QQuickItem *item);
    bool getUseStartDateState(QQuickItem *item);
    bool getUseEndDateState(QQuickItem *item);

    void changeDaysParams(uint16_t &days);
    bool packTimerModel(TimersModel &model);
    void setExistTimerModel(const TimersModel &timer);

    std::shared_ptr<IAddSession> addSessionInterface;
    std::shared_ptr<IQmlEngine> engine;
    QQuickItem *currentItem;
    std::shared_ptr<IUserManipulators> userManipulators;
    int currentIndex;
    SessionType currentSessionType;
    TimersModel currentTimerModel;


public slots:
    void slotShowTimeComboDrum(int type);
    void slotShowDateComboDrum(int type);
    void slotSaveButtonClicked();
    void slotCancelButtonClicked();


signals:

};

#endif // ADDSESSIONWIDGET_H
