#include <assert.h>

#include "timerparamssection.h"

using namespace rosslog;

TimerParamsSection::TimerParamsSection(IRecorderTimerParams *recorderTimersInterface,
                                       std::shared_ptr<IUserManipulators> userManipulators,
                                       std::shared_ptr<IQmlEngine> qmlEngine,
                                       std::shared_ptr<rosslog::Log> log,
                                       QObject *parent) :
    QObject(parent),
    recorderTimersInterface(recorderTimersInterface),
    qmlEngine(qmlEngine),
    logger(log),
    currentItem(nullptr),
    timerList(nullptr),
    addSessionWidget(nullptr),
    userManipulators(userManipulators)
{
    assert(qmlEngine);
    assert(logger);
    assert(parent);
}

void TimerParamsSection::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(
                    qmlEngine->createComponent(TIMER_SECTION_QML_PARH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
        connect(currentItem, SIGNAL(showAddSession()),
                this, SLOT(slotShowAddSession()));
    }
    initTimerList();
    initAddSessionWidget();
}

void TimerParamsSection::updateTimers(std::list<dsp_timer> timers)
{
    assert(timerList);
    this->timers = timers;
    timerList->updateTimers(timers);

    if(timers.size() >= MAX_NUM_TIMERS)
        QMetaObject::invokeMethod(currentItem, "addButtonSetActive",
                                  Q_ARG(QVariant, false));
    else
        QMetaObject::invokeMethod(currentItem, "addButtonSetActive",
                                  Q_ARG(QVariant, true));
}

void TimerParamsSection::changeTimer(TimersModel timer, int index)
{
    addSessionWidget->changeTimer(timer, index);
}

void TimerParamsSection::copyTimer(TimersModel timer, int index)
{
    Q_UNUSED(index);
    addSessionWidget->copyTimer(timer);
}

void TimerParamsSection::deleteTimers(std::vector<int> timersIndex)
{
    dsp_timer timerDsp;
    memset(&timerDsp, 0 , sizeof(timerDsp));
    for(auto &it: timersIndex)
    {
        auto timer = std::next(timers.begin(), it);
        (*timer) = timerDsp;
    }

    recorderTimersInterface->saveTimers(timers);
}

void TimerParamsSection::saveNewTimer(TimersModel model, int index)
{
    dsp_timer timer;
    memset(&timer, 0 , sizeof(timer));

    packTimer(timer, model, index);
    timers.push_back(timer);
    recorderTimersInterface->saveTimers(timers);
    std::cerr << "(RECORDER TIMERS) saveNewTimer index = " << index << std::endl;
    std::cerr << "timeStart = " << model.timeStart << std::endl;
    std::cerr << "timeEnd = " << model.timeEnd << std::endl;
    std::cerr << "duration = " << model.duration << std::endl;
    std::cerr << "colorType = " << model.colorType << std::endl;
    std::cerr << "selected = " << model.selected << std::endl;
    std::cerr << "daysParams = " << model.daysParams << std::endl;
}

void TimerParamsSection::saveTimerChanges(TimersModel model, int index)
{
    dsp_timer timer;
    memset(&timer, 0 , sizeof(timer));

    packTimer(timer, model, index);

    auto it = std::next(timers.begin(), index);
    timer.stop.reserv_0 = it->stop.reserv_0;
    timer.start.reserv_0 = it->start.reserv_0;

    (*it) = timer;

    recorderTimersInterface->saveTimers(timers);
}

void TimerParamsSection::initTimerList()
{
    if( timerList == nullptr )
    {
        timerList = std::shared_ptr<TimersList>(
                    new TimersList(shared_from_this(), qmlEngine, logger, currentItem));
        timerList->init();
    }
}

void TimerParamsSection::initAddSessionWidget()
{
    if( addSessionWidget == nullptr )
    {
        addSessionWidget = std::shared_ptr<AddSessionWidget>(new AddSessionWidget(shared_from_this(),
                                                                                  userManipulators,
                                                                                  qmlEngine, currentItem));
        addSessionWidget->init();
    }
}

void TimerParamsSection::packTimer(dsp_timer &timer, TimersModel model, int index)
{
    Q_UNUSED(index);
    QDateTime dateTimeStart = QDateTime::fromString(QString::fromStdString(model.timeStart), "dd.MM.yyyy hh:mm");
    QDateTime dateTimeEnd = QDateTime::fromString(QString::fromStdString(model.timeEnd), "dd.MM.yyyy hh:mm");
    QTime time = QTime::fromString(QString::fromStdString(model.timeStart),"hh:mm");
    QTime timeEnd = QTime::fromString(QString::fromStdString(model.timeEnd),"hh:mm");
    if(dateTimeStart.isValid() )
        system_time_create(dateTimeStart.toString("dd.MM.yyyy hh:mm").toUtf8().data(),
                       &timer.start);
    else
    {
        dateTimeStart = QDateTime::fromString(QString::fromStdString(model.timeStart), "hh:mm");
        dateTimeStart.setTime(time);
        dateTimeStart.setDate(QDate::currentDate());
        system_time_create(dateTimeStart.toString("dd.MM.yyyy hh:mm").toUtf8().data(),
                       &timer.start);
        timer.start.mon = 0x00;
        timer.start.day = 0x00;
        timer.start.year = 0x00;

    }

    if( dateTimeEnd.isValid() )
    {
        system_time_create(dateTimeEnd.toString("dd.MM.yyyy hh:mm").toUtf8().data(),
                       &timer.stop);
    }
    else
    {
        dateTimeEnd = QDateTime::fromString(QString::fromStdString(model.timeEnd), "hh:mm");
        dateTimeEnd.setTime(timeEnd);
        dateTimeEnd.setDate(QDate::currentDate());
        system_time_create(dateTimeEnd.toString("dd.MM.yyyy hh:mm").toUtf8().data(),
                       &timer.stop);
        timer.stop.mon = 0x00;
        timer.stop.day = 0x00;
        timer.stop.year = 0x00;
    }


    timer.start.dow = model.daysParams;
    timer.stop.dow = model.daysParams;
    timer.start.reserv_0 =  TIMER_FLAG_READY;
    timer.stop.reserv_0 =  TIMER_FLAG_READY;
}

void TimerParamsSection::slotShowAddSession()
{
    assert(addSessionWidget);
    addSessionWidget->show();
}
