#ifndef TIMERPARAMSSECTION_H
#define TIMERPARAMSSECTION_H

#include <memory>
#include <string>
#include <utility>
#include <iterator>

#include <dataprovider/recordersettingscontol.h>

#include <QObject>
#include <QQuickItem>

#include <rosslog/log.h>
#include <iqmlengine.h>

#include <iusermanipulators.h>

#include "timerslist.h"
#include "itimerlist.h"
#include "addsessionwidget.h"
#include "iaddsession.h"
#include "irecordertimerparams.h"

#define TIMER_SECTION_QML_PARH "qrc:/RecorderTimers/SessionsWindow.qml"

class TimerParamsSection : public QObject, public ITimerList, public IAddSession,
        public std::enable_shared_from_this<TimerParamsSection>
{
    Q_OBJECT
public:
    explicit TimerParamsSection(IRecorderTimerParams *recorderTimersInterface,
                                std::shared_ptr<IUserManipulators> userManipulators,
                                std::shared_ptr<IQmlEngine> qmlEngine,
                                std::shared_ptr<rosslog::Log> log,
                                QObject *parent = nullptr);

    void init();
    void updateTimers(std::list<dsp_timer> timers);

    void changeTimer(TimersModel timer, int index) override;
    void copyTimer(TimersModel timer, int index) override;
    void deleteTimers(std::vector<int> timersIndex) override;

    void saveNewTimer(TimersModel model, int index) override;
    void saveTimerChanges(TimersModel model, int index) override;
private:
    void initTimerList();
    void initAddSessionWidget();
    void packTimer(dsp_timer &timer, TimersModel model, int index);

    IRecorderTimerParams *recorderTimersInterface;
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<rosslog::Log> logger;
    QQuickItem *currentItem;
    std::shared_ptr<TimersList> timerList;
    std::shared_ptr<AddSessionWidget> addSessionWidget;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::list<dsp_timer> timers;


signals:

public slots:
    void slotShowAddSession();

};

#endif // TIMERPARAMSSECTION_H
