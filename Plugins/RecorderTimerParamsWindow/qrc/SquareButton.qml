import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0

Button
{
    height: 65
    width: 75
    scale: down ? 0.95 : 1

    ColorConfig{id:cc}
    ShadowRect{type: "panel"; opacity: down ? 0 : 1}

    background: Rectangle
    {
        radius: 2
        color: cc.blue

    }

    Text
    {
        anchors.centerIn: parent
        font.family: fontConf.mainFont
        font.bold: true
        font.pixelSize: 56
        color: cc.light_green
        text: qsTr("+")
    }
}


