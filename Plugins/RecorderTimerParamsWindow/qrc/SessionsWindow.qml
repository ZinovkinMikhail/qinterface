import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Item
{
    id: sessionsWindow


    signal showAddSession()

    function addButtonSetActive(isActive)
    {
        addButton.enabled = isActive
        addButton.opacity = isActive ? 1 : 0.4
    }


    BackgroundTitle
    {
        text: qsTr("Сеансы записи")
    }

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    SquareButton
    {
        id: addButton
        x:40 //18
        y:428        

        onClicked:
        {
            sessionsWindow.showAddSession()
        }
    }

    Rectangle //ColorStatusInfo
    {
        anchors.left: addButton.right
        anchors.leftMargin: 30
        anchors.verticalCenter: addButton.verticalCenter
        width: 280
        height: 50
        color: "transparent"

        Text
        {
            anchors.left: parent.left
            anchors.top: parent.top
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S7
            color: "#79bd8f"
            text: qsTr("Активен")
        }
        Text
        {
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S7
            color: "#d17cb0"
            text: qsTr("Ожидание")
        }
        Text
        {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S7
            color: cc.light_green
            text: qsTr("Отработан")
        }
        Text
        {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S7
            color: "#e8c4da"
            text: qsTr("Просрочен")
        }
        Text
        {
            anchors.right: parent.right
            anchors.top: parent.top
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S7
            color: "#fcfff5"
            text: qsTr("Пассивен")
        }
    }
}
