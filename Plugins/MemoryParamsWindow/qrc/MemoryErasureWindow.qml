import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0

Item
{
    id: memoryErasureWindow
    width: 1280
    height: 484

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal fullClear()
    signal clearHeaders()

    function setMemoryParams(used, capacity, progressVal)
    {
        memoryTotalVALUE.text = capacity;
        memoryUsedVALUE.text = used
        progress.value = progressVal
    }

    function setMemoryTime(timeCurr, timeStart)
    {
        timeFromStartVALUE.text = timeStart
        timeToEndVALUE.text = timeCurr
    }

    Component.onCompleted:
    {
        memoryTotalVALUE.text = "60416 Мб"
        memoryUsedVALUE.text = "196 Мб"
        timeFromStartVALUE.text = "00:00"
        timeToEndVALUE.text = "00:00"
    }

    BackgroundTitle
    {
        text: qsTr("Стирание внутренней памяти")
    }


    Rectangle
    {
        id:background
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 18
        anchors.leftMargin: 18
        height: 320
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }



    Rectangle
    {
        id:mainZone
        anchors.fill: background
        anchors.margins: 18
        color: cc.dark_blue


        Text
        {
            id:memoryTotalLABEL
            anchors.left: progress.left
            anchors.top: parent.top
            anchors.topMargin: 40
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S5 + 2
            color: cc.light_green
            text: "Объём:"
        }
        Text
        {
            id: memoryTotalVALUE
            anchors.bottom: memoryTotalLABEL.bottom
            anchors.left: memoryTotalLABEL.left
            anchors.leftMargin: 120
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S5 + 2
            color: cc.light_green
        }

        Text
        {
            id:memoryUsedLABEL
            anchors.left: progress.left
            anchors.top: parent.top
            anchors.topMargin: 83
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S5 + 2
            color: cc.light_green
            text: "Занято:"
        }
        Text
        {
            id:memoryUsedVALUE
            anchors.bottom: memoryUsedLABEL.bottom
            anchors.left: memoryUsedLABEL.left
            anchors.leftMargin: 120
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S5 + 2
            color: cc.light_green
        }

    }


    ProgressBar
    {
        parent: mainZone
        id: progress
        anchors.horizontalCenter: mainZone.horizontalCenter
        y:150
        hoverEnabled: false
        from: 0.0
        to: 100.0
        value: 20

        background: Rectangle
        {
            implicitWidth: mainZone.width - 120
            implicitHeight: 34
            color: cc.blue
        }

        contentItem: Item
        {
            implicitWidth: 200
            implicitHeight: 4

            Rectangle
            {
                width: progress.visualPosition * parent.width
                height: parent.height
                color: "#52dbb2"
                opacity: 0.65
            }
        }

        Text
        {
            x:parent.width/2 - width/2
            y:parent.height/2 - height/2
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S5
            color: cc.light_green
            text: progress.value + " %"
            z: 2
        }

        Text
        {
            id:timeFromStartLABEL
            y: progress.height + 30
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S5 - 1
            color: cc.light_green
            text: qsTr("Время с начала стирания:")
        }
        Text
        {
            id:timeFromStartVALUE
            x: timeFromStartLABEL.width + 10
            y: progress.height + 30
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S5 - 1
            color: cc.light_green
        }

        Text
        {
            id:timeToEndLABEL
            x: progress.width - width - timeToEndVALUE.width - 10
            y: progress.height + 30
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: 25
            color: cc.light_green
            text: qsTr("Время до окончания стирания:")
        }
        Text
        {
            id:timeToEndVALUE
            x: progress.width - width
            y: progress.height + 30
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S5 - 1
            color: cc.light_green
        }
    }



    Item
    {
        id:controlButtons
        anchors.top: background.bottom
        anchors.right: background.right
        anchors.rightMargin: 30
        anchors.topMargin: 10
        width: 600

        ControlListButton
        {
            anchors.left: parent.left
            type: "SlimType"
            width: 280
            text: qsTr("Быстрое стирание")
            onClicked: clearHeaders()
            visible:  false
        }
        ControlListButton
        {
            anchors.right: parent.right
            type: "SlimType"
            width: 280
            text: qsTr("Полное стирание")
            onClicked: pressDelay.start()
        }
    }


    Timer
    {
        id: pressDelay
        running: false
        repeat: false
        interval: 10
        onTriggered: fullClear()
    }
}
