#include <assert.h>

#include <QQmlComponent>
#include <QQuickItem>
#include <sys/time.h>

#include "memoryparamswindow.h"

using namespace recorder_status_ctrl;
using namespace rosslog;

MemoryParamsWindow::MemoryParamsWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    recorderStatusCtrl(nullptr),
    recorderSettingsCtrl(nullptr),
    monitoringProcessWidget(nullptr),
    memoryErase(nullptr),
    threadWork(true),
    conditionWait(true),
    wasErasing(false),
    isUpdateData(false),
    erasureWaitFreshData(false)
//    erasureWaitStopRec(false),
//    erasureWaitCounter(0)
//    eraseStarted(false)
{
    memset(&recordSettings, 0, sizeof(SoftRecorderSettingsStruct_t));
    memset(&recordStatus, 0, sizeof(SoftRecorderStatusStruct_t));

    connect(this, SIGNAL(signalSettingsApplyed()),
            this, SLOT(slotSettingsApplyed()));
}

std::shared_ptr<IUserManipulators> MemoryParamsWindow::initWindow(EWindSizeType winSizeType,
                                                                    std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                    std::shared_ptr<IStateWidget> stateWidget,
                                                                    std::shared_ptr<IUserManipulators> userManipulators,
                                                                    std::shared_ptr<IQmlEngine> engine,
                                                                    std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    this->stateWidget = stateWidget;

    assert(userManipulators);

    dataProvider->getConnectStatus();

    this->engine = engine;
    this->userManipulators = userManipulators;
    this->dataProvider = dataProvider;
    this->logger = log;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(MEMORY_PARAMS_QML_PATH);
        assert(windowComponent);
    }

    initMemoryErase();
    initRecorderStatusControl();
    initRecorderSettingsControl();

    connect(this, SIGNAL(signalUpdateLocalForms()),
            this, SLOT(slotUpdateLocalForms()));


    eraseThread = std::thread(&MemoryParamsWindow::threadFunction, this);


    return nullptr;
}

QObject *MemoryParamsWindow::getComponentObject()
{
    return windowComponent;
}

std::string MemoryParamsWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string MemoryParamsWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string MemoryParamsWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string MemoryParamsWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string MemoryParamsWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap MemoryParamsWindow::getIcon()
{
    return QPixmap(":/MemoryParams/icons/i27.png");
}

std::pair<bool, QPixmap> MemoryParamsWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/MemoryParams/icons/i18.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > MemoryParamsWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool MemoryParamsWindow::showWindow()
{
    updateData();
    return false;
}

bool MemoryParamsWindow::hideWindow()
{
    return false;
}

EWindSizeType MemoryParamsWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool MemoryParamsWindow::prepareToDelete()
{
    return true;
}

void MemoryParamsWindow::fullMemoryClearCommand()
{
    auto states = stateWidget->countStates();

    for(auto it: states)
    {
        if( it == "DATABASE_WORK_PROGRESS" )
        {
            this->userManipulators->showMessage("В данный момент производится подготовка данных сессий."
                                                "Пожалуйста дождитесь завершения операции.");

            this->userManipulators->hideLoadingWidget();
            return;
        }
        else if( it == "COPY_WORK_PROGRESS" )
        {
            this->userManipulators->showMessage("В данный момент производится копирование данных. "
                                                "Пожалуйста дождитесь завершения операции.");
            this->userManipulators->hideLoadingWidget();
            return;
        }
    }
    this->userManipulators->showLoadingWidget();
    updateData();
    erasureWaitFreshData = true;
}

void MemoryParamsWindow::fullMemoryClear()
{
    eraseStarted = true;
    updateWorker = std::async(std::launch::async,
                              &MemoryParamsWindow::startEraseInternalMemory, this);
    updateWorker.wait_for(std::chrono::seconds(0));
}


void MemoryParamsWindow::clearMemoryHeaders()
{
}


void MemoryParamsWindow::initRecorderStatusControl()
{
    if( recorderStatusCtrl == nullptr )
    {
        RecorderStatusControl *statCtrl = new RecorderStatusControl(dataProvider, *logger.get());
        recorderStatusCtrl = std::shared_ptr<RecorderStatusControl>(statCtrl);
    }
}

void MemoryParamsWindow::initRecorderSettingsControl()
{
    if( recorderSettingsCtrl == nullptr )
    {
        RecorderSettingsContol *setCtrl = new RecorderSettingsContol(dataProvider, *logger.get());
        recorderSettingsCtrl = std::shared_ptr<RecorderSettingsContol>(setCtrl);
    }
}

void MemoryParamsWindow::sethighlightsChannel(int channel, bool state)
{
    if( state )
        QMetaObject::invokeMethod(windowComponent, "highlightChannel", Q_ARG(
                                      QVariant, QVariant::fromValue(channel)));
    else
        QMetaObject::invokeMethod(windowComponent, "unHihglightChannel", Q_ARG(
                                      QVariant, QVariant::fromValue(channel)));

}

void MemoryParamsWindow::threadFunction()
{
    struct timeval tv;
    __time_t lastTime = 0;
    int timeout = 0;

    while(threadWork)
    {
        if( eraseStarted )
            timeout = 1;
        else
            timeout = 5;

        gettimeofday(&tv, nullptr);
        if( tv.tv_sec >= (lastTime + timeout) || !conditionWait || tv.tv_sec < lastTime )
        {
            conditionWait = true;
            lastTime = tv.tv_sec;


            this->logger->Message(LOG_INFO, __LINE__, __func__, "Update recorder data Memory");

            if( isUpdateData )
            {
                try
                {
                    dataMutex.lock();
                    recorderStatusCtrl->updateStatus();
                    this->recordStatus = recorderStatusCtrl->getRecorderStatus();
                    dataMutex.unlock();
                    emit signalUpdateLocalForms();
                }
                catch( std::system_error &systemErr )
                {
                    dataMutex.unlock();
                    this->logger->Message(LOG_ERR, __LINE__,
                                          __func__, dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
                }
                isUpdateData = false;
            }
        }
        else
        {
            usleep(10000);
        }
    }
}

void MemoryParamsWindow::setRecievedData()
{
    this->memoryErase->setMemoryParams(recordStatus.FlashUsed,
                                       recordStatus.FlashCapacity,
                                       recordStatus.FlashProgress);
    switch (static_cast<FLASH_STATUS>(recordStatus.FlashStatus))
    {
        case fsFlashErasing:
        {
            isUpdateData = true;
            eraseStarted = true;
            break;
        }
        default:
        {
            eraseStarted = false;
//            if(eraseStarted)
//            {
//                eraseStarted = false;
//                userManipulators->showMessage(std::string("Стирание завершено"));
//            }
            break;
        }
    }

    if(erasureWaitFreshData)
    {
        erasureWaitFreshData = false;

        if( static_cast<FLASH_STATUS>(recordStatus.FlashStatus) == fsFlashErasing )
        {
            this->userManipulators->hideLoadingWidget();
            userManipulators->showMessage(std::string("Стирание уже запущено"));
        }        
        else if(isRecordingNow())
        {
            this->userManipulators->hideLoadingWidget();
            userManipulators->showMessage(std::string("Стирание недоступно во время записи."));
        }
        else if (recordStatus.PlayStatus != AXIS_SOFTRECORDER_PLAYER_COMMAND__STOP)
        {
            this->userManipulators->hideLoadingWidget();
            userManipulators->showMessage(std::string("Стирание недоступно во время воспроизведения."));
        }
        else
        {
            fullMemoryClear();
        }
    }

}

void MemoryParamsWindow::updateData()
{
    isUpdateData = true;
    conditionWait = false;
//    cv.notify_one();
}

void MemoryParamsWindow::getInfoFromAdapter()
{
    try
    {
        recorderStatusCtrl->updateStatus();
        this->recordStatus = recorderStatusCtrl->getRecorderStatus();
        emit signalUpdateLocalForms();
    }
    catch( std::system_error &systemErr )
    {
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
    }
}

void MemoryParamsWindow::initMemoryErase()
{
    if( memoryErase == nullptr )
    {
        memoryErase = std::shared_ptr<MemoryErase>(new MemoryErase(this,
                                                                   userManipulators,
                                                                   this->engine,
                                                                   windowComponent));
        memoryErase->init();
    }
}


void MemoryParamsWindow::slotCancelButtonClicked()
{
    this->updateData();
}

void MemoryParamsWindow::slotApplyButtonClicked()
{

    updateWorker = std::async(std::launch::async,
                              std::bind(&MemoryParamsWindow::updateDataOnAdapter,
                                        this,  std::placeholders::_1), recordSettings);
    updateWorker.wait_for(std::chrono::seconds(0));
}

void MemoryParamsWindow::updateDataOnAdapter(SoftRecorderSettingsStruct_t data)
{
    try
    {
        sleep(1);
        recorderSettingsCtrl->saveRecordSettings(data);
        emit slotSettingsApplyed();
    }
    catch( std::system_error &except )
    {
        Q_UNUSED(except);
    }
    this->userManipulators->hideLoadingWidget();
}

void MemoryParamsWindow::startEraseInternalMemory()
{
    try
    {
        sleep(2);
        recorderStatusCtrl->eraseInternalMemory(true);
        isUpdateData = true;
//        eraseStarted = true;
    }
    catch( std::system_error &except )
    {
        userManipulators->showMessage(std::string("Не удалось начать стирание.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
        eraseStarted = false;
    }
    this->userManipulators->hideLoadingWidget();
}

bool MemoryParamsWindow::isRecordingNow()
{
    if( recordStatus.Chanel1Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
            recordStatus.Chanel2Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
            recordStatus.Chanel3Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW ||
            recordStatus.Chanel4Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW )
    {
        return true;
    }
    else
    {
        return false;
    }
}

//bool MemoryParamsWindow::stopAllRecording()
//{
//    std::bitset<4> channels = 0;

//    if(recordStatus.Chanel1Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
//        channels.set(0, true);

//    if(recordStatus.Chanel2Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
//        channels.set(1, true);

//    if(recordStatus.Chanel3Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
//        channels.set(2, true);

//    if(recordStatus.Chanel4Status & AXIS_SOFTRECORDER_STATUS__RECORD_NOW)
//        channels.set(3, true);

//    recorderStatusCtrl->stopRecord(channels);


//    if(recordStatus.Chanel1Status & AXIS_SOFTRECORDER_STATUS__PLAY_NOW)
//        channels.set(0, true);

//    if(recordStatus.Chanel2Status & AXIS_SOFTRECORDER_STATUS__PLAY_NOW)
//        channels.set(1, true);

//    if(recordStatus.Chanel3Status & AXIS_SOFTRECORDER_STATUS__PLAY_NOW)
//        channels.set(2, true);

//    if(recordStatus.Chanel4Status & AXIS_SOFTRECORDER_STATUS__PLAY_NOW)
//        channels.set(3, true);

//    recorderStatusCtrl->stopAudioMonitoring(channels);
//}


void MemoryParamsWindow::slotUpdateLocalForms()
{
    setRecievedData();
}

void MemoryParamsWindow::slotSettingsApplyed()
{
}


MemoryParamsWindow::~MemoryParamsWindow()
{
    threadWork = false;
    conditionWait = false;
//    cv.notify_all();

    if( eraseThread.joinable() )
        eraseThread.join();

    std::cerr << "Removed MemoryParamsWindow" << std::endl;

}
