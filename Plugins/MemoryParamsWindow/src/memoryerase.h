#ifndef MEMORYERASE_H
#define MEMORYERASE_H

#include <memory>

#include <QObject>
#include <QQuickItem>
#include <QDateTime>

#include <iqmlengine.h>

#include "imemoryparams.h"
#include "iusermanipulators.h"

#define MEMORY_ERASE_QML_PATH "qrc:/MemoryParams/MemoryErasureWindow.qml"

class MemoryErase : public QObject
{
    Q_OBJECT
public:
    explicit MemoryErase(IMemoryParams *memoryParamsInterface,
                         std::shared_ptr<IUserManipulators> userManipulators,
                         std::shared_ptr<IQmlEngine> qmlEngine,
                         QObject *parent = nullptr);
    void init();
    void setMemoryParams(uint64_t flashUsed, uint64_t flashCapacity, uint16_t progress);

private:
    std::shared_ptr<IQmlEngine> qmlEngine;
    QQuickItem *currentItem;
    time_t timeStart;
    QDateTime dateTime;
    IMemoryParams *memoryParamsInterface;
    std::shared_ptr<IUserManipulators> userManipulators;

public slots:
    void slotFullClear();
    void slotClearHeaders();

};

#endif // MEMORYERASE_H
