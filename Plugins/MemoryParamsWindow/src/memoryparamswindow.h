#ifndef MONITORINGWINDOWPLUGIN_H
#define MONITORINGWINDOWPLUGIN_H

#include <math.h>
#include <future>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/recordersettingscontol.h>
#include <dataprovider/recorderstatuscontrol.h>

#include "irecorderwindow.h"
#include "memoryerase.h"
#include "imemoryparams.h"

#define WINDOW_IDENTIFICATOR "MemoryParamsWindow"
#define WINDOW_FOLDER_NAME "Recorder"
#define ICON_PRESENTATION_NAME "Стирание"
#define FOLDER_PRESENTATION_NAME "Накопитель"

#define PARAMS_PER_PAGE 5

#define MEMORY_PARAMS_QML_PATH "qrc:/MemoryParams/MemoryParamsWindow.qml"

class MemoryParamsWindow : public QObject, public IEmptyPlaceWindow, public IMemoryParams
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "MemoryParamsWindow")
public:
    explicit MemoryParamsWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~MemoryParamsWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;

    void fullMemoryClearCommand() override;
    void clearMemoryHeaders() override;

private:
    void initRecorderStatusControl();
    void initRecorderSettingsControl();
    void sethighlightsChannel(int channel, bool state);
    void setRecievedData();
    void updateData();
    void getInfoFromAdapter();
    void initMemoryErase();
    void threadFunction();

    void updateDataOnAdapter(SoftRecorderSettingsStruct_t data);
    void startEraseInternalMemory();
    bool isRecordingNow();
//    bool stopAllRecording();
    void fullMemoryClear();

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<recorder_status_ctrl::RecorderStatusControl> recorderStatusCtrl;
    std::shared_ptr<RecorderSettingsContol> recorderSettingsCtrl;
    std::shared_ptr<IProcessWidget> monitoringProcessWidget;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    std::shared_ptr<IStateWidget> stateWidget;
    std::future<void> updateWorker;
    std::shared_ptr<MemoryErase> memoryErase;

    SoftRecorderSettingsStruct_t recordSettings;
    SoftRecorderStatusStruct_s recordStatus;
    std::thread eraseThread;
//    std::condition_variable cv;
//    std::mutex cv_m;
    std::mutex dataMutex;
    bool threadWork;
    bool conditionWait;
    bool wasErasing;
    bool isUpdateData;
    bool eraseStarted {false};
    bool erasureWaitFreshData;
//    bool erasureWaitStopRec;
//    int erasureWaitCounter;


signals:
    void signalUpdateLocalForms();
    void signalSettingsApplyed();

public slots:
    void slotTimerTimeout();
    void slotCancelButtonClicked();
    void slotApplyButtonClicked();
    void slotUpdateLocalForms();
    void slotSettingsApplyed();
};

#endif // MONITORINGWINDOWPLUGIN_H
