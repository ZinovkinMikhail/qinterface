#include "memoryerase.h"

#include <assert.h>

MemoryErase::MemoryErase(IMemoryParams *memoryParamsInterface, std::shared_ptr<IUserManipulators> userManipulators, std::shared_ptr<IQmlEngine> qmlEngine, QObject *parent)
    : QObject(parent),
      qmlEngine(qmlEngine),
      currentItem(nullptr),
      memoryParamsInterface(memoryParamsInterface),
      userManipulators(userManipulators)
{
    assert(this->qmlEngine);
    timeStart = 0;
}

void MemoryErase::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(qmlEngine->createComponent(MEMORY_ERASE_QML_PATH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
        connect(currentItem, SIGNAL(fullClear()),
                this, SLOT(slotFullClear()));
        connect(currentItem, SIGNAL(clearHeaders()),
                this, SLOT(slotClearHeaders()));
    }

}

void MemoryErase::setMemoryParams(uint64_t flashUsed,
                                  uint64_t flashCapacity,
                                  uint16_t progress)
{

    QString usedStr = QString::number( flashUsed / 1000 / 1000) + " Мб.";
    QString capacityStr = QString::number( flashCapacity / 1000 / 1000) + " Мб.";
    QMetaObject::invokeMethod(currentItem,"setMemoryParams",
                              Q_ARG(QVariant, usedStr),
                              Q_ARG(QVariant, capacityStr),
                              Q_ARG(QVariant, progress));

    if(progress > 0)
    {
        if(timeStart == 0)
        {
            timeStart = time(NULL);
        }
        time_t timeCurr = time(NULL);
        timeCurr = timeCurr - timeStart;
        timeCurr = ((int)(((float)(100 * timeCurr)) / (float)progress)) - timeCurr;
        dateTime.setTime_t(timeCurr);
        QString timeCurrStr = dateTime.toString("mm:ss");
        dateTime.setTime_t(time(NULL) - timeStart);
        QMetaObject::invokeMethod(currentItem,"setMemoryTime",
                                  Q_ARG(QVariant, timeCurrStr),
                                  Q_ARG(QVariant, dateTime.toString("mm:ss")));
    }
    else
    {
        timeStart = 0;
//        userManipulators->showMessage(std::string("Стирание завершено"));
    }

    if(progress == 0)
    {
        QMetaObject::invokeMethod(currentItem,"setMemoryTime",
                                  Q_ARG(QVariant, "00:00"),
                                  Q_ARG(QVariant, "00:00"));
    }
}

void MemoryErase::slotFullClear()
{
    bool approveed = this->userManipulators->requestUserConfirm(std::string("Вы уверены, что хотите стереть внутреннюю память устройства?"));

    if(approveed)
        memoryParamsInterface->fullMemoryClearCommand();
}

void MemoryErase::slotClearHeaders()
{
    memoryParamsInterface->clearMemoryHeaders();
}
