#ifndef IMEMORYPARAMS_H
#define IMEMORYPARAMS_H

class IMemoryParams
{
public:
    virtual void fullMemoryClearCommand() = 0;
    virtual void clearMemoryHeaders() = 0;

    virtual ~IMemoryParams(){}
};

#endif // IMEMORYPARAMS_H
