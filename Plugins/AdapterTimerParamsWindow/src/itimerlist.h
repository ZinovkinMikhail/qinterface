#ifndef ITIMERLIST_H
#define ITIMERLIST_H

#include "timerslist.h"

class ITimerList
{
public:
    virtual void changeTimer(TimersModel timer, int index) = 0;
    virtual void copyTimer(TimersModel timer, int index) = 0;
    virtual void deleteTimers(std::vector<int> timersIndex) = 0;
    virtual ~ITimerList() {}
};

#endif // ITIMERLIST_H
