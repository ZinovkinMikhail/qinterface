#include "addsessionwidget.h"

#include <assert.h>
#include <iostream>

AddSessionWidget::AddSessionWidget(std::shared_ptr<IAddSession> addSessionInterface,
                                   std::shared_ptr<IUserManipulators> userManipulators,
                                   std::shared_ptr<IQmlEngine> qmlEngine,
                                   QObject *parent) :
    QObject(parent),
    addSessionInterface(addSessionInterface),
    engine(qmlEngine),
    currentItem(nullptr),
    userManipulators(userManipulators),
    currentIndex(0),
    currentSessionType(CHANGE)
{}

void AddSessionWidget::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(
                    engine->createComponent(ADD_SESSION_WIDGET_QML_PATH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));
        connect(currentItem, SIGNAL(showComboDrumTime(int)),
                this, SLOT(slotShowTimeComboDrum(int)));
        connect(currentItem, SIGNAL(showComboDrumDate(int)),
                this, SLOT(slotShowDateComboDrum(int)));
        connect(currentItem, SIGNAL(save()),
                this, SLOT(slotSaveButtonClicked()));
        connect(currentItem, SIGNAL(cancel()),
                this, SLOT(slotCancelButtonClicked()));
    }
}

void AddSessionWidget::show()
{
    QMetaObject::invokeMethod(currentItem, "show");
    currentSessionType = NEW;
    QQuickItem * mainZone = currentItem->findChild<QQuickItem*>("mainZone");
    QDateTime dateTime = QDateTime::currentDateTime();
    setDateStart(mainZone, dateTime.toString(DATE_FORMAT));
    setDateEnd(mainZone, dateTime.toString(DATE_FORMAT));

    setTimeStart(mainZone, dateTime.time().toString(TIME_FORMAT));
    setTimeEnd(mainZone, dateTime.time().toString(TIME_FORMAT));

    setUseStartDay(mainZone, false);
    setUseEndDay(mainZone, false);

    setDaysParams(0);
}

void AddSessionWidget::changeTimer(TimersModel timer, int index)
{
    currentSessionType = CHANGE;
    currentIndex = index;
    setExistTimerModel(timer);

    QMetaObject::invokeMethod(currentItem, "show");
}

void AddSessionWidget::copyTimer(TimersModel timer)
{
    currentSessionType = NEW;
    setExistTimerModel(timer);
    QMetaObject::invokeMethod(currentItem, "show");
}

void AddSessionWidget::setCheckStateItem(QQuickItem *item, bool value)
{
    QQmlProperty(item, "checked").write(value);
}

void AddSessionWidget::setDateEnd(QQuickItem *item, QString endDate)
{
    QQuickItem *endDateItem = item->findChild<QQuickItem*>("endDate");
    QQmlProperty(endDateItem, "txttxt").write(endDate);
}

void AddSessionWidget::setTimeEnd(QQuickItem *item, QString endTime)
{
    QQuickItem *endTimeItem = item->findChild<QQuickItem*>("endTime");
    QQmlProperty(endTimeItem, "txttxt").write(endTime);
}


void AddSessionWidget::setDateStart(QQuickItem *item, QString startDate )
{
    QQuickItem *startDateItem = item->findChild<QQuickItem*>("startDate");
    QQmlProperty(startDateItem, "txttxt").write(startDate);

}

void AddSessionWidget::setTimeStart(QQuickItem *item, QString startTime)
{
    QQuickItem *startTimeItem = item->findChild<QQuickItem*>("startTime");
    QQmlProperty(startTimeItem, "txttxt").write(startTime);
}

bool AddSessionWidget::getCheckStateOfItem(QQuickItem *item)
{
    QVariant checkedVar = QQmlProperty(item, "checked").read();
    return checkedVar.toBool();
}

void AddSessionWidget::setDaysParams(uint16_t days)
{
    QQuickItem * grid = currentItem->findChild<QQuickItem*>("grid");

    for(auto &it : grid->childItems())
    {
        if(it->objectName() == "mondayCheck")
        {
            if( days & AXIS_TIME_DAY_OF_WEEK__MONDAY )
                setCheckStateItem(it, true);
            else
                setCheckStateItem(it, false);
        }
        else if(it->objectName() == "fridayCheck")
        {
            if( days & AXIS_TIME_DAY_OF_WEEK__FRIDAY )
                setCheckStateItem(it, true);
            else
                setCheckStateItem(it, false);
        }
        else if(it->objectName() == "tuesdayCheck")
        {
            if( days & AXIS_TIME_DAY_OF_WEEK__TUESDAY )
                setCheckStateItem(it, true);
            else
                setCheckStateItem(it, false);
        }
        else if(it->objectName() == "saturdayCheck")
        {
            if( days & AXIS_TIME_DAY_OF_WEEK__SATURDAY )
                setCheckStateItem(it, true);
            else
                setCheckStateItem(it, false);
        }
        else if(it->objectName() == "wednesdayCheck")
        {
            if( days & AXIS_TIME_DAY_OF_WEEK__WEDNESDAY )
                setCheckStateItem(it, true);
            else
                setCheckStateItem(it, false);
        }
        else if(it->objectName() == "sundayCheck")
        {
            if( days & AXIS_TIME_DAY_OF_WEEK__SUNDAY )
                setCheckStateItem(it, true);
            else
                setCheckStateItem(it, false);
        }
        else if(it->objectName() == "thursdayCheck")
        {
            if( days & AXIS_TIME_DAY_OF_WEEK__THURSDAY )
                setCheckStateItem(it, true);
            else
                setCheckStateItem(it, false);
        }
    }
//    QQuickItem * mainZone = currentItem->findChild<QQuickItem*>("mainZone");

//    for(auto &it : mainZone->childItems())
//    {
//        if( it->objectName().isEmpty() )
//            continue;
//        if(it->objectName() == "useStartDateCheck")
//        {
//            std::cerr << getCheckStateOfItem(it) << std::endl;
//        }
//        else if(it->objectName() == "useEndDateCheck")
//        {
//            std::cerr << getCheckStateOfItem(it) << std::endl;
//        }
//    }
}

void AddSessionWidget::setUseStartDay(QQuickItem *item, bool value)
{
    QQuickItem *useStartDay = item->findChild<QQuickItem*>("useStartDateCheck");
    QQmlProperty(useStartDay, "checked").write(value);
}

void AddSessionWidget::setUseEndDay(QQuickItem *item, bool value)
{
    QQuickItem *useStartDay = item->findChild<QQuickItem*>("useEndDateCheck");
    QQmlProperty(useStartDay, "checked").write(value);
}

QString AddSessionWidget::getStartDate(QQuickItem *item)
{
    QQuickItem *startItem = item->findChild<QQuickItem*>("startDate");
    return QQmlProperty(startItem, "txttxt").read().toString();
}

QString AddSessionWidget::getEndDate(QQuickItem *item)
{
    QQuickItem *endItem = item->findChild<QQuickItem*>("endDate");
    return QQmlProperty(endItem, "txttxt").read().toString();
}

QString AddSessionWidget::getStartTime(QQuickItem *item)
{
    QQuickItem *sartItem = item->findChild<QQuickItem*>("startTime");
    return QQmlProperty(sartItem, "txttxt").read().toString();
}

QString AddSessionWidget::getEndTime(QQuickItem *item)
{
    QQuickItem *endItem = item->findChild<QQuickItem*>("endTime");
    return QQmlProperty(endItem, "txttxt").read().toString();
}

bool AddSessionWidget::getUseStartDateState(QQuickItem *item)
{
    QQuickItem *useStartItem = item->findChild<QQuickItem*>("useStartDateCheck");
    return QQmlProperty(useStartItem, "checked").read().toBool();
}

bool AddSessionWidget::getUseEndDateState(QQuickItem *item)
{
    QQuickItem *useEndItem = item->findChild<QQuickItem*>("useEndDateCheck");
    return QQmlProperty(useEndItem, "checked").read().toBool();
}

void AddSessionWidget::changeDaysParams(uint16_t &days)
{
    auto * grid = currentItem->findChild<QQuickItem*>("grid");

    for(auto &it : grid->childItems())
    {
        if(it->objectName() == "mondayCheck")
        {
            if(getCheckStateOfItem(it))
                days = days | AXIS_TIME_DAY_OF_WEEK__MONDAY;
        }
        else if(it->objectName() == "fridayCheck")
        {
            if(getCheckStateOfItem(it))
                days = days | AXIS_TIME_DAY_OF_WEEK__FRIDAY;
        }
        else if(it->objectName() == "tuesdayCheck")
        {
            if(getCheckStateOfItem(it))
                days = days | AXIS_TIME_DAY_OF_WEEK__TUESDAY;
        }
        else if(it->objectName() == "saturdayCheck")
        {
            if(getCheckStateOfItem(it))
                days = days | AXIS_TIME_DAY_OF_WEEK__SATURDAY;
        }
        else if(it->objectName() == "wednesdayCheck")
        {
            if(getCheckStateOfItem(it))
                days = days | AXIS_TIME_DAY_OF_WEEK__WEDNESDAY;
        }
        else if(it->objectName() == "sundayCheck")
        {
            if(getCheckStateOfItem(it))
                days = days | AXIS_TIME_DAY_OF_WEEK__SUNDAY;
        }
        else if(it->objectName() == "thursdayCheck")
        {
            if(getCheckStateOfItem(it))
                days = days | AXIS_TIME_DAY_OF_WEEK__THURSDAY;
        }
    }
}

bool AddSessionWidget::packTimerModel(TimersModel &model)
{
    uint16_t days = 0;
    changeDaysParams(days);

    auto * mainZone = currentItem->findChild<QQuickItem*>("mainZone");

    QDateTime dateTimeStart = QDateTime::fromString(getStartTime(mainZone), TIME_FORMAT);
    QDateTime dateTimeEnd = QDateTime::fromString(getEndTime(mainZone), TIME_FORMAT);


    QString dateStartStr = getStartDate(mainZone);
    QDate dateStart = QDate::fromString(dateStartStr, DATE_FORMAT);
    dateTimeStart.setDate(dateStart);

    QString dateEndStr = getEndDate(mainZone);
    QDate dateEnd = QDate::fromString(dateEndStr, DATE_FORMAT);
    dateTimeEnd.setDate(dateEnd);

    if( !getUseStartDateState(mainZone) && days > 0)
    {
        model.timeStart = dateTimeStart.time().toString(TIME_FORMAT).toStdString();

    }
    else
    {
        model.timeStart = dateTimeStart.toString(DATE_TIME_FORMAT).toStdString();
    }


    if( !getUseEndDateState(mainZone) && days > 0)
    {
        model.timeEnd = dateTimeEnd.time().toString(TIME_FORMAT).toStdString();
        if(dateTimeEnd.time().msecsTo(dateTimeStart.time()) == 0)
        {
            userManipulators->showMessage(std::string("Интервал работы таймера должен быть больше 0."));
            return false;
        }
    }
    else
    {
        model.timeEnd = dateTimeEnd.toString(DATE_TIME_FORMAT).toStdString();

        if(dateTimeStart.daysTo(dateTimeEnd) == 0 && dateTimeEnd.time().msecsTo(dateTimeStart.time()) == 0)
        {
            userManipulators->showMessage(std::string("Интервал работы таймера должен быть больше 0."));
            return false;
        }
    }

    model.daysParams = days;
    return  true;
}

void AddSessionWidget::setExistTimerModel(const TimersModel &timer)
{
    setDaysParams(timer.daysParams);

    QDateTime dateTimeStart = QDateTime::fromString(
                QString::fromStdString(timer.timeStart), DATE_TIME_FORMAT);

    QQuickItem * mainZone = currentItem->findChild<QQuickItem*>("mainZone");

    if(dateTimeStart.isValid() )
    {
        setDateStart(mainZone, dateTimeStart.date().toString(DATE_FORMAT));
        setTimeStart(currentItem, dateTimeStart.time().toString(TIME_FORMAT));

        if( timer.daysParams != 0 )
            setUseStartDay(mainZone, true);
        else
            setUseStartDay(mainZone, false);
    }
    else
    {
        QDateTime dateTimeStart = QDateTime::fromString(
                    QString::fromStdString(timer.timeStart), TIME_FORMAT);

        setTimeStart(currentItem, dateTimeStart.time().toString(TIME_FORMAT));
        setUseStartDay(mainZone, false);
    }

    QDateTime dateTimeEnd = QDateTime::fromString(
                QString::fromStdString(timer.timeEnd), DATE_TIME_FORMAT);

    if( dateTimeEnd.isValid() )
    {
        setDateEnd(mainZone, dateTimeEnd.date().toString(DATE_FORMAT));
        setTimeEnd(mainZone, dateTimeEnd.time().toString(TIME_FORMAT));
        if( timer.daysParams != 0 )
            setUseEndDay(mainZone, true);
        else
            setUseEndDay(mainZone, false);
    }
    else
    {
        QDateTime dateTimeEnd = QDateTime::fromString(
                    QString::fromStdString(timer.timeEnd), TIME_FORMAT);
        setTimeEnd(mainZone, dateTimeEnd.time().toString(TIME_FORMAT));
        setUseEndDay(mainZone, false);
    }
}

void AddSessionWidget::slotShowTimeComboDrum(int type)
{
    QTime time = QTime::currentTime();
    QQuickItem * mainZone = currentItem->findChild<QQuickItem*>("mainZone");

    QTime timeStart = QTime::fromString(getStartTime(mainZone), TIME_FORMAT);
    QTime timeEnd = QTime::fromString(getEndTime(mainZone), TIME_FORMAT);
    uint16_t days = 0;
    changeDaysParams(days);

    int hour = 0;
    int min = 0;

    std::pair<bool, std::string> result;

    if( static_cast<DateType>(type) == END_DATE )
    {
        result = userManipulators->openTimeComboDrum(
                    timeEnd.hour(), timeEnd.minute(), -1);
    }
    else
    {
        result = userManipulators->openTimeComboDrum(
                    timeStart.hour(), timeStart.minute(), -1);
    }

    if( result.first && result.second.length() > 0)
    {
        QTime timePres = QTime::fromString(QString::fromStdString(result.second), TIME_FORMAT);

        hour = timePres.hour();
        min = timePres.minute();

        QDate startDate = QDate::fromString(getStartDate(mainZone), DATE_FORMAT);
        QDate endDate = QDate::fromString(getEndDate(mainZone), DATE_FORMAT);

        if( startDate.daysTo(endDate) == 0 || !getUseStartDateState(mainZone) || !getUseEndDateState(mainZone) )
        {
            if( startDate.daysTo(endDate) <= 0 )
            {
                if(static_cast<TimeType>(type) == END_TIME )
                {
                    if( hour < timeStart.hour() )
                        return;
                    if( min <= timeStart.minute() && hour == timeStart.hour() )
                        return;
                }
                else
                {
                    if( time.msecsTo(timePres) < 0 && days == 0)
                        return; //!!!!
                    std::cerr <<  timeEnd.msecsTo(timePres)  << std::endl;
                    if( timeEnd.msecsTo(timePres )  > 0 )
                        setTimeEnd(mainZone, timePres.toString(TIME_FORMAT));
                }
            }
        }
        else if ( getUseStartDateState(mainZone) || getUseEndDateState(mainZone) )
        {
            if(static_cast<TimeType>(type) == END_TIME )
            {
                if( hour < timeStart.hour() )
                    return;
                if( min <= timeStart.minute() && hour == timeStart.hour() )
                    return;
            }

        }
        if(static_cast<TimeType>(type) == START_TIME )
        {
            setTimeStart(mainZone, QString::fromStdString(result.second));
        }
        else
        {
            setTimeEnd(mainZone, QString::fromStdString(result.second));
        }
    }
}

void AddSessionWidget::slotShowDateComboDrum(int type)
{
    QDate date = QDate::currentDate();
    QQuickItem * mainZone = currentItem->findChild<QQuickItem*>("mainZone");
    QDate startDate = QDate::fromString(getStartDate(mainZone), DATE_FORMAT);
    QDate endDate = QDate::fromString(getEndDate(mainZone), DATE_FORMAT);

    std::pair<bool, std::string> result;

    if( static_cast<DateType>(type) == END_DATE )
    {
        result = userManipulators->openDateComboDrum(
                    endDate.day(), endDate.month(), endDate.year());
    }
    else
    {
        result = userManipulators->openDateComboDrum(
                    startDate.day(), startDate.month(), startDate.year());
    }

    if( result.first && result.second.length() > 0)
    {
        QDate datePres = QDate::fromString(QString::fromStdString(result.second), DATE_FORMAT);


        if( date.daysTo( datePres ) < 0 )
            return;

        if(static_cast<DateType>(type) == END_DATE )
        {

            if( startDate.daysTo( datePres ) < 0 )
                return;
            if( startDate.daysTo( datePres ) == 0 )
            {
                QTime startTime = QTime::fromString(getStartTime(mainZone), TIME_FORMAT);
                QTime endTime = QTime::fromString(getEndTime(mainZone), TIME_FORMAT);
                if( startTime.msecsTo(endTime) <= 0 )
                    setTimeEnd(mainZone, startTime.toString(TIME_FORMAT));
            }
        }
        else
        {
            if( datePres.daysTo( endDate ) <= 0 )
                setDateEnd(mainZone, QString::fromStdString(result.second));
        }



        if(static_cast<DateType>(type) == START_DATE )
        {
            setDateStart(mainZone, QString::fromStdString(result.second));
        }
        else
        {
            setDateEnd(mainZone, QString::fromStdString(result.second));
        }
    }
}

void AddSessionWidget::slotSaveButtonClicked()
{
    TimersModel model;
    if(!packTimerModel(model))
        return;

    if( currentSessionType == NEW )
        addSessionInterface->saveNewTimer(model, currentIndex);
    else
        addSessionInterface->saveTimerChanges(model, currentIndex);
    QMetaObject::invokeMethod(currentItem, "close");
}

void AddSessionWidget::slotCancelButtonClicked()
{
    QMetaObject::invokeMethod(currentItem, "close");
}
