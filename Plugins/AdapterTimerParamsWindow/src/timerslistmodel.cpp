#include <iostream>

#include "timerslistmodel.h"

TimersListModel::TimersListModel(QObject *parent) : QAbstractListModel(parent)
{
}

int TimersListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
    {
        return 0;
    }
    return listData.size();
}

QVariant TimersListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    TimersModel currentModel = listData.at(index.row());

    switch (role)
    {
    case ColorRole:
        return QVariant(index.row() < 2 ? "orange" : "skyblue");
    case TimeStart:
        return QString::fromStdString(currentModel.timeStart);
    case TimeEnd:
        return QString::fromStdString(currentModel.timeEnd);
    case Duration:
        return QString::fromStdString(currentModel.duration);
    case Selected:
        return currentModel.selected;
    case ColorType:
        return  (quint8)currentModel.colorType;
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> TimersListModel::roleNames() const
{
    QHash<int, QByteArray> roles =  QAbstractListModel::roleNames();
    roles[ColorType] = "colorType";
    roles[ColorRole] = "color";
    roles[TimeStart] = "time_start";
    roles[TimeEnd] = "time_end";
    roles[Duration] = "duration";
    roles[Selected] = "selected";
    return roles;
}

void TimersListModel::appendModel(TimersModel model)
{
    listData.append(model);
}

void TimersListModel::clearModel()
{
    beginResetModel();
    listData.clear();
    endResetModel();
}

void TimersListModel::updateModel()
{
    beginResetModel();
    endResetModel();
}

void TimersListModel::setProperty(int index, QString type, int val)
{
    if( type == "selected")
    {
        listData[index].selected = val;
        updateModel();
    }
}

void TimersListModel::selectAllRows()
{
    for(auto &it : listData )
    {
        it.selected = true;
    }
    beginResetModel();
    endResetModel();
}

void TimersListModel::getSelectedRows(std::vector<int> &selected)
{
    int index = 0;
    for(auto &it : listData )
    {
        if(it.selected)
            selected.push_back(index);
        index++;
    }
}

TimersModel TimersListModel::getTimer(int index)
{
    return listData.at(index);
}
