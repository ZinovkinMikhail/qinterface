#ifndef IRECORDERTIMERPARAMS_H
#define IRECORDERTIMERPARAMS_H

#include <list>

#include <axis_softrecorder.h>

class IRecorderTimerParams
{
public:
    virtual void saveTimers(std::list<dsp_timer> timers) = 0;

    virtual ~IRecorderTimerParams(){}

};

#endif // IRECORDERTIMERPARAMS_H
