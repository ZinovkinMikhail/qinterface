#ifndef TIMERSLIST_H
#define TIMERSLIST_H

#include <memory>

#include <dataprovider/recordersettingscontol.h>

#include <QObject>
#include <QQuickItem>
#include <QVariant>
#include <QDateTime>

#include <iqmlengine.h>
#include <rosslog/log.h>

#include "timerparamsmodel.h"
#include "timerslistmodel.h"
#include "itimerlist.h"

#define TIMER_FLAG_READY			0x01
#define TIMER_FLAG_EXPIRED			0x02
#define TIMER_FLAG_DISABLE			0x03
#define TIMER_FLAG_WORKING			0x04
#define TIMER_FLAG_DONE				0x05

#define TIMER_STATUS_READY			0x0100
#define TIMER_STATUS_EXPIRED			0x0200
#define TIMER_STATUS_DISABLE			0x0000
#define TIMER_STATUS_DISABLE_2			0x0300
#define TIMER_STATUS_WORKING			0x0400
#define TIMER_STATUS_DONE				0x0800
#define TIMER_STATUS_WAITING				0x1000

#define TIMERS_LIST_QML_PATH "qrc:/AdapterTimers/SessionList.qml"

class TimersList : public QObject
{
    Q_OBJECT
public:
    explicit TimersList(std::shared_ptr<ITimerList> timerInterface,
                        std::shared_ptr<IQmlEngine> qmlEngine,
                        std::shared_ptr<rosslog::Log> log,
                        QObject *parent = nullptr);
    void init();
    void updateTimers(std::list<dsp_timer> timers);
private:
    void initTimerListModel();
    int timeRangeByStructData(const struct_data * start, const struct_data *end,
                              char *value, size_t size);
    int getTimeRangeNoData(const struct_data *start, const struct_data *stop,
                           char *value, size_t size);

    std::string getDaysString(const struct_data *start);

    std::shared_ptr<ITimerList> timerInterface;
    std::shared_ptr<IQmlEngine> qmlEngine;
    std::shared_ptr<rosslog::Log> logger;
    QQuickItem *currentItem;
    std::shared_ptr<TimersListModel> timerListModel;

signals:

public slots:
    void slotRowChanged(int index, QString row, int value);
    void slotSelectAllRows();
    void slotDeleteRow(int index);
    void slotCopyRow(int index);
    void slotChangeRow(int index);

};

#endif // TIMERSLIST_H
