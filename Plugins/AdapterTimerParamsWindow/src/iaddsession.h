#ifndef IADDSESSION_H
#define IADDSESSION_H

#include "timerparamsmodel.h"

class IAddSession
{
public:

    virtual void saveNewTimer(TimersModel model, int index) = 0;
    virtual void saveTimerChanges(TimersModel model, int index) = 0;
    virtual ~IAddSession() {}
};

#endif // IADDSESSION_H
