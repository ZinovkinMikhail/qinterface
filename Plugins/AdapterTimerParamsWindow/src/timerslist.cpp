#include <assert.h>

#include "timerslist.h"

using namespace rosslog;

TimersList::TimersList(std::shared_ptr<ITimerList> timerInterface,
                       std::shared_ptr<IQmlEngine> qmlEngine,
                       std::shared_ptr<rosslog::Log> log,
                       QObject *parent) :
    QObject(parent),
    timerInterface(timerInterface),
    qmlEngine(qmlEngine),
    logger(log),
    currentItem(nullptr),
    timerListModel(nullptr)
{
    assert(qmlEngine);
    assert(logger);
    assert(parent);
}

void TimersList::init()
{
    if( currentItem == nullptr )
    {
        currentItem = qobject_cast<QQuickItem*>(
                    qmlEngine->createComponent(TIMERS_LIST_QML_PATH));
        assert(currentItem);
        currentItem->setParentItem(qobject_cast<QQuickItem*>(this->parent()));

        connect(currentItem, SIGNAL(rowChanged(int,QString,int)),
                this, SLOT(slotRowChanged(int,QString,int)));

        connect(currentItem, SIGNAL(selectAllRows()),
                this, SLOT(slotSelectAllRows()));
        connect(currentItem, SIGNAL(copyRow(int)),
                this, SLOT(slotCopyRow(int)));
        connect(currentItem, SIGNAL(changeRow(int)),
                this, SLOT(slotChangeRow(int)));
        connect(currentItem, SIGNAL(deleteRow(int)),
                this, SLOT(slotDeleteRow(int)));
    }
    initTimerListModel();

    QMetaObject::invokeMethod(currentItem,"setListModel",
                              Q_ARG(QVariant, QVariant::fromValue(timerListModel.get())));
}

void TimersList::updateTimers(std::list<dsp_timer> timers)
{
    assert(timerListModel);
    timerListModel->clearModel();
    for(auto it: timers)
    {
        TimersModel model;
        memset(&model, 0, sizeof(TimersModel));
        char time[64];

        if(it.start.mon == 0x00)
        {
            if(!system_time_no_date_print(time, sizeof(time), it.start))
            {
                qWarning("%s: It wasn't possible to convert timer to char\n",__func__);
            }
        }
        else
        {
            if(!system_time_print(time, sizeof(time), it.start))
            {
                qWarning("%s: It wasn't possible to convert timer to char\n",__func__);
            }
            this->logger->Message(LOG_INFO, __LINE__, __func__, "Timer LIST date = mon %x time = %s", it.start.mon, time );
        }

        model.timeStart = time;

        if(it.stop.mon == 0x00)
        {
            if(!system_time_no_date_print(time, sizeof(time), it.stop))
            {
                qWarning("%s: It wasn't possible to convert timer to char\n",__func__);
            }
            this->logger->Message(LOG_INFO, __LINE__, __func__, "Timer LIST no date = mon %x time = %s", it.stop.mon, time );
        }
        else
        {
            if(!system_time_print(time, sizeof(time), it.stop))
            {
                qWarning("%s: It wasn't possible to convert timer to char\n",__func__);
            }
            this->logger->Message(LOG_INFO, __LINE__, __func__, "Timer LIST date = mon %x time = %s", it.stop.mon, time );
        }

        model.timeEnd = time;

        if( it.start.dow && it.stop.dow)
        {
            if(getTimeRangeNoData(&it.start, &it.stop, time, sizeof(time)) < 0)
            {
               qWarning("%s:It wasn't possible to compare time\n",__func__);
            }
            this->logger->Message(LOG_INFO, __LINE__, __func__, "Timer LIST = mon %x time = %s", it.start.mon, time );
        }
        else
        {
            if(timeRangeByStructData(&it.start, &it.stop, time, sizeof(time)) < 0)
            {
               qWarning("%s:It wasn't possible to compare time\n",__func__);
            }
            this->logger->Message(LOG_INFO, __LINE__, __func__, "Timer LIST = mon %x time = %s", it.start.mon, time );

        }

        model.duration = time;

        model.daysParams = it.start.dow;


        if(it.stop.reserv_0 == TIMER_FLAG_WORKING)
        {
            model.colorType = 1;
        }
        else if(it.stop.reserv_0 == TIMER_FLAG_READY)
        {
            model.colorType = 2;
        }
        else if(it.stop.reserv_0 == TIMER_FLAG_EXPIRED)
        {
            model.colorType = 4;
        }
        else if(it.stop.reserv_0 == TIMER_FLAG_DONE || it.stop.reserv_0 == (TIMER_STATUS_DONE >> 8))
        {
            model.colorType = 3;
        }
        else
        {
            model.colorType = 2;
        }
        std::cerr << "APPPEND MODEL " << std::endl;
        std::cerr << "Start " << model.timeStart << std::endl;
        std::cerr << "End " << model.timeEnd << std::endl;
        std::cerr << "Duration " << model.duration << std::endl;
        std::cerr << "COlor type " << (int)model.colorType << std::endl;
        std::cerr << "Days " << model.daysParams << std::endl;
        timerListModel->appendModel(model);
    }
    timerListModel->updateModel();

    QMetaObject::invokeMethod(currentItem,"scrollToEnd");
}

void TimersList::initTimerListModel()
{
    if( timerListModel == nullptr )
    {
        timerListModel = std::shared_ptr<TimersListModel>(new TimersListModel());
    }
}

int TimersList::timeRangeByStructData(const struct_data *start, const struct_data *end, char *value, size_t size)
{
    if( !start || !end || !value )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Bad arument");
        return -1;
    }
    char c_time[32];

    uint32_t hours;
    int minutes;
    int64_t secsRange;

    uint64_t secsStart = 0 ;
    uint64_t secsStop = 0 ;

    QDateTime dateTimerStart;
    QDateTime dateTimerStop;

    system_time_print(c_time, sizeof(c_time), *start);

    dateTimerStart =  QDateTime::fromString( c_time, "dd.MM.yyyy hh:mm");

    system_time_print(c_time, sizeof(c_time), *end);

    dateTimerStop =  QDateTime::fromString( c_time, "dd.MM.yyyy hh:mm");

    secsStart = dateTimerStart.toTime_t();
    secsStop = dateTimerStop.toTime_t();

    secsRange = secsStop - secsStart;

    if( secsRange < 0)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid value");
        return -1;
    }

    hours = secsRange/3600;
    secsRange = secsRange - (hours * 3600);

    minutes = secsRange/60;

    snprintf(value, size, "%i ч : %i м ", hours, minutes);

    return 0;
}

int TimersList::getTimeRangeNoData(const struct_data *start, const struct_data *stop, char *value, size_t size)
{
    if( !start || !stop || !value )
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Bad arument");
        return -1;
    }
    char c_time[32];

    uint32_t hours;
    int minutes;
    int64_t secsRange;


    QDateTime dateTimerStart;
    QDateTime dateTimerStop;

    system_time_no_date_print(c_time, sizeof(c_time), *start);

    dateTimerStart =  QDateTime::fromString( c_time, "hh:mm");

    system_time_no_date_print(c_time, sizeof(c_time), *stop);

    dateTimerStop =  QDateTime::fromString( c_time, "hh:mm");

    secsRange = dateTimerStart.secsTo(dateTimerStop);

    if( secsRange < 0)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, "Invalid value");
        return -1;
    }

    hours = secsRange/3600;
    secsRange = secsRange - (hours * 3600);

    minutes = secsRange/60;

    snprintf(value, size, "%i ч : %i м %s", hours, minutes, getDaysString(start).c_str());

    return 0;
}

std::string TimersList::getDaysString(const struct_data *start)
{
    uint16_t days = start->dow;
    std::string daysStr = "";
    if( days & AXIS_TIME_DAY_OF_WEEK__MONDAY )
        daysStr += "Пн. ";

    if( days & AXIS_TIME_DAY_OF_WEEK__TUESDAY )
        daysStr += "Вт. ";

    if( days & AXIS_TIME_DAY_OF_WEEK__WEDNESDAY )
        daysStr += "Ср. ";

    if( days & AXIS_TIME_DAY_OF_WEEK__THURSDAY )
        daysStr += "Чт. ";

    if( days & AXIS_TIME_DAY_OF_WEEK__FRIDAY )
        daysStr += "Пт. ";

    if( days & AXIS_TIME_DAY_OF_WEEK__SATURDAY )
        daysStr += "Сб. ";

    if( days & AXIS_TIME_DAY_OF_WEEK__SUNDAY )
        daysStr += "Вс. ";

    return  daysStr;
}

void TimersList::slotRowChanged(int index, QString row, int value)
{
    assert(timerListModel);
    timerListModel->setProperty(index, row, value);
}

void TimersList::slotSelectAllRows()
{
    assert(timerListModel);
    timerListModel->selectAllRows();
}

void TimersList::slotDeleteRow(int index)
{
    Q_UNUSED(index);
    std::vector<int> selectedRows;
    selectedRows.reserve(32);
    timerListModel->getSelectedRows(selectedRows);

    if( selectedRows.size() > 0 )
    {
        try
        {
            this->timerInterface->deleteTimers(selectedRows);
        }
        catch (std::exception &except)
        {
            this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
        }
    }

}

void TimersList::slotCopyRow(int index)
{
    try
    {
        this->timerInterface->copyTimer(timerListModel->getTimer(index), index);
    }
    catch (std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
}

void TimersList::slotChangeRow(int index)
{
    try
    {
        this->timerInterface->changeTimer(timerListModel->getTimer(index), index);
    }
    catch (std::exception &except)
    {
        this->logger->Message(LOG_ERR, __LINE__, __func__, except.what());
    }
}
