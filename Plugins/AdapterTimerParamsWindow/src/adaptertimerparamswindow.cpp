#include <assert.h>

#include <QQmlComponent>
#include <QQuickItem>

#include "adaptertimerparamswindow.h"

using namespace adapter_status_ctrl;
using namespace rosslog;

AdapterTimerParamsWindow::AdapterTimerParamsWindow(QObject *parent) :
    QObject(parent),
    windowComponent(nullptr),
    engine(nullptr),
    adapterStatusCtrl(nullptr),
    adapterSettingsCtrl(nullptr),
    timerParamsSection(nullptr)
{
    memset(&adapterSettingsCtrl, 0, sizeof(SoftRecorderSettingsStruct_t));
    memset(&adapterStatusCtrl, 0, sizeof(SoftRecorderStatusStruct_t));
    memset(&adapterTimers, 0, sizeof(DspRecorderExchangeTimerSettingsStruct_t));

    connect(this, SIGNAL(signalSettingsApplyed()),
            this, SLOT(slotSettingsApplyed()));
}

std::shared_ptr<IUserManipulators> AdapterTimerParamsWindow::initWindow(EWindSizeType winSizeType,
                                                                    std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                                    std::shared_ptr<IStateWidget> stateWidget,
                                                                    std::shared_ptr<IUserManipulators> userManipulators,
                                                                    std::shared_ptr<IQmlEngine> engine,
                                                                    std::shared_ptr<rosslog::Log> log)
{
    Q_UNUSED(winSizeType);
    Q_UNUSED(stateWidget);

    assert(userManipulators);

    dataProvider->getConnectStatus();

    this->engine = engine;
    this->userManipulators = userManipulators;
    this->dataProvider = dataProvider;
    this->logger = log;

    if( windowComponent == nullptr )
    {
        windowComponent = engine->createComponent(RECORDER_TIMERS_QML_PATH);
        assert(windowComponent);
    }

    initAdapterStatusControl();
    initAdapterSettingsControl();
    initTimersSection();

    connect(this, SIGNAL(signalUpdateLocalForms()),
            this, SLOT(slotUpdateLocalForms()), Qt::QueuedConnection);

    return nullptr;
}

QObject *AdapterTimerParamsWindow::getComponentObject()
{
    return windowComponent;
}

std::string AdapterTimerParamsWindow::getWindowName()
{
    return WINDOW_IDENTIFICATOR;
}

std::string AdapterTimerParamsWindow::getWindowAbsolutePath()
{
#warning Add Path
    return "";
}

std::string AdapterTimerParamsWindow::getWindowFolderName()
{
    return WINDOW_FOLDER_NAME;
}

std::string AdapterTimerParamsWindow::getIconPresentationName()
{
    return ICON_PRESENTATION_NAME;
}

std::string AdapterTimerParamsWindow::getWindowFolderPresentationName()
{
    return FOLDER_PRESENTATION_NAME;
}

QPixmap AdapterTimerParamsWindow::getIcon()
{
    return QPixmap(":/AdapterTimers/icons/i25.png");
}

std::pair<bool, QPixmap> AdapterTimerParamsWindow::getFolderPixmap()
{
    return std::make_pair(true, QPixmap(":/AdapterTimers/icons/i19.png"));
}

std::pair<bool, std::shared_ptr<IProcessWidget> > AdapterTimerParamsWindow::getProcessWidget()
{
    return std::pair<bool, std::shared_ptr<IProcessWidget>>(false, nullptr);
}

bool AdapterTimerParamsWindow::showWindow()
{
    updateData();
    return false;
}

bool AdapterTimerParamsWindow::hideWindow()
{
    return false;
}

EWindSizeType AdapterTimerParamsWindow::getWinSizeType()
{
    return NORMALSIZE;
}

bool AdapterTimerParamsWindow::prepareToDelete()
{
    return true;
}

void AdapterTimerParamsWindow::saveTimers(std::list<dsp_timer> timers)
{
    userManipulators->showLoadingWidget();

    updateWorker = std::async(std::launch::async,
                              std::bind(&AdapterTimerParamsWindow::updateDataOnAdapter,
                                        this,  std::placeholders::_1), timers);
    updateWorker.wait_for(std::chrono::seconds(0));
}

void AdapterTimerParamsWindow::initAdapterStatusControl()
{
    if( adapterStatusCtrl == nullptr )
    {
        AdapterStatusControl *statCtrl = new AdapterStatusControl(dataProvider, *logger.get());
        adapterStatusCtrl = std::shared_ptr<AdapterStatusControl>(statCtrl);
    }
}

void AdapterTimerParamsWindow::initAdapterSettingsControl()
{
    if( adapterSettingsCtrl == nullptr )
    {
        AdapterSettingsControl *setCtrl = new AdapterSettingsControl(dataProvider, *logger.get());
        adapterSettingsCtrl = std::shared_ptr<AdapterSettingsControl>(setCtrl);
    }
}

void AdapterTimerParamsWindow::updateData()
{
    updateWorker = std::async( std::launch::async,[this]
    {
        try
        {
            userManipulators->showLoadingWidget();
            getInfoFromAdapter();
            userManipulators->hideLoadingWidget();
        } catch (std::system_error &except)
        {
            userManipulators->hideLoadingWidget();
            userManipulators->showMessage(std::string("Не удалось отправить команду.\nКод ошибки: ").append(
                                              std::to_string(except.code().value())));
        }

        return;
    });
    updateWorker.wait_for(std::chrono::seconds(0));

}

void AdapterTimerParamsWindow::getInfoFromAdapter()
{
    try
    {
        adapterSettingsCtrl->updateTimers();
        adapterStatusCtrl->updateStatus();
        this->adapterStatus = adapterStatusCtrl->getAdapterStatus();
        this->adapterTimers = adapterSettingsCtrl->getAdapterTimers();
        emit signalUpdateLocalForms();
    }
    catch( std::system_error &systemErr )
    {
        this->logger->Message(LOG_ERR, __LINE__,
                              __func__,
                              dpr::ProviderError::getInstance().getErrorString(systemErr).c_str());
        userManipulators->showMessage(std::string("Не удалось получить данные.\nКод ошибки: ").append(
                                          std::to_string(systemErr.code().value())));
    }
    catch( std::runtime_error &exceptr )
    {
        userManipulators->showMessage(std::string("Не удалось получить данные."));
    }
}

void AdapterTimerParamsWindow::updateDataOnAdapter(std::list<dsp_timer> timers)
{
    try
    {
        memset(adapterTimers.Timers, 0 , sizeof(adapterTimers.Timers));
        int index = 0;
        for(auto &it : timers )
        {
            std::cerr << "PUSH ADAPTER TIMNER "  << std::endl;
            adapterTimers.Timers[index] = it;
            printf("Adapter time = %x\n", adapterTimers.Timers[index].stop.reserv_0);
            index++;
        }

        //timerParamsSection->updateTimers(timers);

        adapterSettingsCtrl->saveAdapterTimers(adapterTimers);
        emit signalSettingsApplyed();
    }
    catch( std::system_error &except )
    {
        this->logger->Message(LOG_ERR,__LINE__,__func__, except.what());
        userManipulators->showMessage(std::string("Не удалось сохранить изменения.\nКод ошибки: ").append(
                                          std::to_string(except.code().value())));
    }
    this->userManipulators->hideLoadingWidget();
}

void AdapterTimerParamsWindow::initTimersSection()
{
    if(timerParamsSection == nullptr)
    {
        timerParamsSection = std::shared_ptr<TimerParamsSection>(
                    new TimerParamsSection(this, userManipulators,
                                           engine, logger, windowComponent));
        timerParamsSection->init();
    }
}


void AdapterTimerParamsWindow::slotUpdateLocalForms()
{
    this->logger->Message(LOG_ERR, __LINE__, __func__, "Timers count %i", MAX_NUM_TIMERS);
    int timerscount  = MAX_NUM_TIMERS;

    std::list<dsp_timer> timers;
    for(int i = 0; i < timerscount;i++)
    {
        if( adapterTimers.Timers[i].stop.reserv_0 != TIMER_FLAG_DISABLE &&
                adapterTimers.Timers[i].stop.reserv_0 != 0)
        {
            timers.push_back(adapterTimers.Timers[i]);
        }
    }
    currentTimersList = timers;
    timerParamsSection->updateTimers(timers);
}

void AdapterTimerParamsWindow::slotSettingsApplyed()
{
    updateData();
}

AdapterTimerParamsWindow::~AdapterTimerParamsWindow()
{
    std::cerr << "Removed AdapterTimerParamsWindow" << std::endl;
}
