#ifndef TIMERPARAMSMODEL_H
#define TIMERPARAMSMODEL_H

#include <string>

typedef struct
{
    std::string timeStart;
    std::string timeEnd;
    std::string duration;
    uint8_t colorType;
    bool selected;
    uint16_t daysParams;
} TimersModel;




#endif // TIMERPARAMSMODEL_H
