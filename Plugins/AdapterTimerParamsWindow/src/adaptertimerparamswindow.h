#ifndef MONITORINGWINDOWPLUGIN_H
#define MONITORINGWINDOWPLUGIN_H

#include <math.h>
#include <future>

#include <QObject>
#include <QtPlugin>
#include <QQmlApplicationEngine>

#include <iemptyplacewindow.h>
#include <iqmlengine.h>
#include <dataprovider/adaptersettingscontrol.h>
#include <dataprovider/adapterstatuscontrol.h>

#include <axis_time.h>

#include "timerparamssection.h"
#include "irecordertimerparams.h"

#define WINDOW_IDENTIFICATOR "AdapterTimersParamsWindow"
#define WINDOW_FOLDER_NAME "Adapter"
#define ICON_PRESENTATION_NAME "Таймера"
#define FOLDER_PRESENTATION_NAME "Адаптер"

#define RECORDER_TIMERS_QML_PATH "qrc:/AdapterTimers/AdapterTimersParamsWindow.qml"


class AdapterTimerParamsWindow : public QObject, public IEmptyPlaceWindow, public IRecorderTimerParams
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "AdapterTimerParamsWindow")
public:
    explicit AdapterTimerParamsWindow(QObject *parent = nullptr);
    std::shared_ptr<IUserManipulators> initWindow(EWindSizeType winSizeType,
                                                  std::shared_ptr<data_provider::DataProvider> dataProvider,
                                                  std::shared_ptr<IStateWidget> stateWidget,
                                                  std::shared_ptr<IUserManipulators> userManipulators,
                                                  std::shared_ptr<IQmlEngine> engine,
                                                  std::shared_ptr<rosslog::Log> log) override;
    ~AdapterTimerParamsWindow();
    QObject *getComponentObject() override;
    std::string getWindowName() override;
    std::string getWindowAbsolutePath() override;
    std::string getWindowFolderName() override;
    std::string getIconPresentationName() override;
    std::string getWindowFolderPresentationName() override;
    QPixmap getIcon() override;
    std::pair<bool, QPixmap> getFolderPixmap() override;
    std::pair<bool, std::shared_ptr<IProcessWidget>> getProcessWidget() override;
    bool showWindow() override;
    bool hideWindow() override;
    EWindSizeType getWinSizeType() override;
    std::pair<bool, std::vector<std::shared_ptr<IProcessWidget>>> getProcessWidgetsVector()
    { return std::make_pair(false, std::vector<std::shared_ptr<IProcessWidget>>()); }
    bool prepareToDelete() override;

    //interface
    void saveTimers(std::list<dsp_timer> timers) override;
private:
    void initAdapterStatusControl();
    void initAdapterSettingsControl();
    void sethighlightsChannel(int channel, bool state);
    void updateData();
    void getInfoFromAdapter();
    void updateDataOnAdapter(std::list<dsp_timer> timers);

    void initTimersSection();

    QObject *windowComponent;
    std::shared_ptr<IQmlEngine> engine;
    std::shared_ptr<adapter_status_ctrl::AdapterStatusControl> adapterStatusCtrl;
    std::shared_ptr<AdapterSettingsControl> adapterSettingsCtrl;
    std::shared_ptr<IProcessWidget> monitoringProcessWidget;
    std::shared_ptr<IUserManipulators> userManipulators;
    std::shared_ptr<data_provider::DataProvider> dataProvider;
    std::shared_ptr<rosslog::Log> logger;
    std::future<void> updateWorker;

    SoftAdapterSettingsStruct_t adapterSettings;
    DspRecorderExchangeTimerSettingsStruct_t adapterTimers;
    SoftAdapterDeviceCurrentStatus_t adapterStatus;
    std::shared_ptr<TimerParamsSection> timerParamsSection;
    std::list<dsp_timer> currentTimersList;


signals:
    void signalUpdateLocalForms();
    void signalSettingsApplyed();

public slots:
    void slotUpdateLocalForms();
    void slotSettingsApplyed();
};

#endif // MONITORINGWINDOWPLUGIN_H
