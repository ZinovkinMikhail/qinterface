import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Item
{
    id: addSession
    width: 1280
    height: 720
    y: -135
    visible: false
    z:20

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    signal showComboDrumTime(int type)
    signal showComboDrumDate(int type)
    signal save()
    signal cancel()

    function show()
    {
        visible = true
    }

    function close()
    {
        visible = false
    }

    Button
    {
        id:blockZone
        anchors.fill: parent
        anchors.margins: -20

        background: Rectangle
        {
            color: cc.fog
            opacity:0.4
        }
    }


    Rectangle
    {
        id:background
        x:parent.width/2 - width/2
        y:parent.height/2 - height/2 + 15
        height: 400 + 20
        width: 1100 + 20
        radius: 4
        color: cc.blue

        ShadowRect{type: "panel"}
    }

    Rectangle
    {
        id:mainZone
        objectName: "mainZone"
        anchors.centerIn: background
        height: background.height - 20
        width: background.width - 20
        color: cc.dark_blue


        BackgroundTitle
        {
            x: parent.width - width - 25
            y: parent.height - height - 10
            font.pixelSize: 52
            text: qsTr("Таймер")
        }


        ControlListButton
        {
            id:cancelButton
            x: parent.width - width - 50
            y: parent.height - height - 30
            type: "SlimType"
            text: qsTr("Отменить")
            onClicked: cancel()
        }
        ControlListButton
        {
            id:saveButton
            x: parent.width - width - width - 90
            y: parent.height - height - 30
            type: "SlimType"
            text: qsTr("Сохранить")
            onClicked:
            {
                save()
            }
        }



        Timer {
            id: timer
            running: false
            repeat: false
            interval: 100
            onTriggered:
            {
                if(timeStartDropButton.checked)
                {
                    timeStartDropButton.checked = false
                    showComboDrumTime(0)
                }
                else if( timeEndDropButton.checked )
                {
                    timeEndDropButton.checked = false
                    showComboDrumTime(1)
                }
                else if( dateStartDropButton.checked )
                {
                    dateStartDropButton.checked = false
                    showComboDrumDate(0);
                }
                else if( dateEndDropButton.checked )
                {
                    dateEndDropButton.checked = false
                    showComboDrumDate(1);
                }
            }
        }



        Text
        {
            anchors.horizontalCenter: timeStartDropButton.horizontalCenter
            y: timeStartDropButton.y - height - 15
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S6 - 2
            color: cc.light_green
            text: qsTr("Начало")
        }
        Text
        {
            anchors.horizontalCenter: timeEndDropButton.horizontalCenter
            y: timeEndDropButton.y - height - 15
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S6 - 2
            color: cc.light_green
            text: qsTr("Окончание")
        }

        Text
        {
            x: timeStartDropButton.x - 80
            anchors.verticalCenter: timeStartDropButton.verticalCenter
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S6 - 2
            color: cc.light_green
            text: qsTr("Время")
        }
        Text
        {
            x: dateStartDropButton.x - 75
            anchors.verticalCenter: dateStartDropButton.verticalCenter
            font.family: fontConf.mainFont
            font.italic: true
            font.pixelSize: fontConf.pixSize_S6 - 2
            color: cc.light_green
            text: qsTr("Дата")
        }


        ButtonDrop
        {
            id: timeStartDropButton
            objectName: "startTime"
            x: 100
            y: 100 - 40
            type: "time"
            txttxt: "00:00:00"
            onClicked:
            {
                timeStartDropButton.checked = true
                timer.start()
            }
        }
        ButtonDrop
        {
            id: timeEndDropButton
            objectName: "endTime"
            x: 360
            y: 100  - 40
            type: "time"
            txttxt: "00:00:00"
            onClicked:
            {
                timeEndDropButton.checked = true
                timer.start()
            }
        }
        ButtonDrop
        {
            id: dateStartDropButton
            objectName: "startDate"
            x: 100
            y: 170  - 40
            type: "date"
            txttxt: "01.01.2019"
            onClicked:
            {
                dateStartDropButton.checked = true
                timer.start()
            }

            opacity:
            {
                if( useStartDateCheck.checked )
                {
                    enabled = true
                    return 1.0
                }
                if(mondayCheck.checked || tuesdayCheck.checked || wednesdayCheck.checked ||
                        fridayCheck.checked || sundayCheck.checked ||
                        thursdayCheck.checked || saturdayCheck.checked  )
                {
                    enabled = false
                    return  0.3
                }
                else if (!mondayCheck.checked || !tuesdayCheck.checked || !wednesdayCheck.checked ||
                         !fridayCheck.checked || !sundayCheck.checked || !thursdayCheck.checked || !saturdayCheck.checked)
                {
                    enabled = true
                    return  1.0
                }
            }
        }
        ButtonDrop
        {
            id: dateEndDropButton
            objectName: "endDate"
            x: 360
            y: 170  - 40
            type: "date"
            txttxt: "01.01.2019"
            onClicked:
            {
                dateEndDropButton.checked = true
                timer.start()
            }

            opacity:
            {
                if( useEndDateCheck.checked )
                {
                    enabled = true
                    return 1.0
                }
                if(mondayCheck.checked || tuesdayCheck.checked || wednesdayCheck.checked ||
                        fridayCheck.checked || sundayCheck.checked ||
                        thursdayCheck.checked || saturdayCheck.checked  )
                {
                    enabled = false
                    return  0.3
                }
                else if (!mondayCheck.checked || !tuesdayCheck.checked || !wednesdayCheck.checked ||
                         !fridayCheck.checked || !sundayCheck.checked || !thursdayCheck.checked || !saturdayCheck.checked)
                {
                    enabled = true
                    return  1.0
                }
            }
        }


        TextCheckBox
        {
            id:useStartDateCheck
            objectName: "useStartDateCheck"
            x: 90
            y: 220
            width: 380
            text: qsTr("Использовать дату начала:")
        }
        TextCheckBox
        {
            id:useEndDateCheck
            objectName: "useEndDateCheck"
            x: 90
            y: 280
            width: 380
            text: qsTr("Использовать дату окончания:")
        }


        Grid
        {
            objectName: "grid"
            x: timeEndDropButton.x + timeEndDropButton.width + 40
            y: timeEndDropButton.y - 20
            rows:4
            rowSpacing: 5
            columnSpacing: 25

            TextCheckBox
            {
                id:mondayCheck
                objectName: "mondayCheck"
                width: 200
                text: qsTr("Понедельник:")
            }
            TextCheckBox
            {
                id:fridayCheck
                objectName: "fridayCheck"
                width: 200
                text: qsTr("Пятница:")
            }
            TextCheckBox
            {
                id:tuesdayCheck
                objectName: "tuesdayCheck"
                width: 200
                text: qsTr("Вторник:")
            }
            TextCheckBox
            {
                id:saturdayCheck
                objectName: "saturdayCheck"
                width: 200
                text: qsTr("Суббота:")
            }
            TextCheckBox
            {
                id:wednesdayCheck
                objectName: "wednesdayCheck"
                width: 200
                text: qsTr("Среда:")
            }
            TextCheckBox
            {
                id:sundayCheck
                objectName: "sundayCheck"
                width: 200
                text: qsTr("Воскресенье:")
            }
            TextCheckBox
            {
                id:thursdayCheck
                objectName: "thursdayCheck"
                width: 200
                text: qsTr("Четверг:")
            }
        }
    }

    CloseSmallButton
    {
        x: background.x - width/3
        y: background.y - height/3
        onClicked: addSession.visible = false
    }
}
