import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0


Item
{
    id: root
    width: 1280
    height: 415

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    property int currentRow: 0
    property int selectedRow: 0
    property bool wasSelectedRow: false

    signal rowChanged(int index, string role, int val)
    signal selectAllRows();
    signal deleteRow(int row);
    signal changeRow(int row);
    signal copyRow(int row);

    function selectRowOnly(index, role, val, state)
    {
        if( val )
        {
            wasSelectedRow = true
            selectedRow = index
        }
        else
        {
            if( wasSelectedRow && index === selectedRow )
            {
                wasSelectedRow = false
            }
        }

        rowChanged(index, role, val)
        setContextMenuStateOnly(state)
        lv.positionViewAtIndex(index,ListView.Center)
    }

    function selectRow(index, role, val, pointerPos)
    {
        rowChanged(index, role, true)
        currentRow = index
        setContextMenuState(true, pointerPos)
    }

    function setListModel(model)
    {
        lv.model = model
        lv.update()
    }



    function setContextMenuStateOnly(state)
    {
        sessionTapMenu.visible = state
    }

    function setContextMenuState(state, pointerPos)
    {
        sessionTapMenu.x = pointerPos.x < sessionTapMenu.width ?
                    pointerPos.x : pointerPos.x - sessionTapMenu.width
        sessionTapMenu.y = pointerPos.y < 240 ?
                    pointerPos.y : pointerPos.y - sessionTapMenu.height
        sessionTapMenu.visible = state
    }

    function scrollToEnd()
    {
        lv.positionViewAtEnd()
    }



    Rectangle
    {
        id:back
        anchors.centerIn: parent
        width: parent.width - 36
        height: parent.height
        radius: 3
        color: cc.blue

        ShadowRect{type: "panel"}
    }


    Item
    {
        id:mainZone
        anchors.centerIn: parent
        width: parent.width - 64
        height: parent.height - 18
    }


    Rectangle
    {
        x:lv.x + sb.x - 4
        y:lv.y + sb.y
        height: sb.height
        width: sb.width + 8
        color: cc.dark_blue
    }



    ListView
    {
        id:lv
        anchors.fill: mainZone
        clip: true
        spacing: 2

        headerPositioning: ListView.OverlayHeader

        header: sessionsHeader
        //model: sessionsModel
        delegate: sessionsRow

        ScrollBar.vertical: ScrollBar
        {
            id:sb
            topPadding:5
            bottomPadding:5
//            minimumSize: 0.15 // QT 5.11

            contentItem: Rectangle
            {
                implicitWidth: 28
                radius: 2
                color: cc.light_green
//                color: "transparent" //variant for add indicator
                opacity: sb.pressed ? 0.7 : 1
            }
        }
    }


    SessionTapMenu
    {
        id:sessionTapMenu
        visible: false
        onSelectAll:
        {
            root.selectAllRows()
        }
        onCopy:
        {
            root.copyRow(root.currentRow)
        }
        onChange:
        {
            root.changeRow(root.currentRow)
        }
        onDeleteButt:
        {
            root.deleteRow(root.currentRow)
        }
        onHideMenu:
        {
            if( wasSelectedRow && currentRow === selectedRow )
                return
            rowChanged(currentRow, "selected", false)
        }
    }


    Component
    {
        id: sessionsHeader

        Row
        {
           z:2
           spacing: 2

           Rectangle
           {
               height: 29 + 4
               width: 50
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 - 2
                   color: cc.light_green
                   text: qsTr("№")
               }
           }

           Rectangle
           {
               height: 29  + 4
               width: lv.width/4 - 32
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 - 2
                   color: cc.light_green
                   text: qsTr("Начало")
               }
           }

           Rectangle
           {
               height: 29  + 4
               width: lv.width/4 - 32
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 - 2
                   color: cc.light_green
                   text: qsTr("Окончание")
               }
           }

           Rectangle
           {
               height: 29  + 4
               width: lv.width/2 - 32 - 150
               color: cc.dark_blue

               Rectangle //Line
               {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
               }

               Text
               {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 - 2
                   color: cc.light_green
                   text: qsTr("Продолжительность")
               }
           }

           Rectangle
           {
                height: 29  + 4
                width: 148
                color: cc.dark_blue

                Rectangle //Line
                {
                   y: parent.height
                   width: parent.width
                   height: 2
                   color: cc.blue
                }

                Text
                {
                   anchors.centerIn: parent
                   font.family: fontConf.mainFont
                   font.italic: true
                   font.pixelSize: fontConf.pixSize_S6 - 2
                   color: cc.light_green
                   text: qsTr("Выбор")
                }
            }
        }
    }


    Component
    {
        id: sessionsRow
        Item
        {
            height: 58

            Row
            {
                id: rowElem
                height: parent.height
                spacing: 2

                property var lineColor:
                {
                    switch (colorType)
                    {
                      case 1:
                         return "#79bd8f"; //Активен
                      case 2:
                         return "#d17cb0"; //Ожидание
                      case 3:
                         return cc.light_green; //Отработан
                      case 4:
                         return "#e8c4da"; //Просрочен
                      case 5:
                         return "#fcfff5"; //Пассивен
                      default:
                         return "white"
                    }
                }

                Rectangle
                {
                    height: parent.height
                    width: 50
                    color: selected ? cc.highlight_green : cc.dark_blue

                    Text
                    {
                        anchors.centerIn: parent
                        font.family: fontConf.mainFont
                        font.italic: true
                        font.pixelSize: fontConf.pixSize_S6 - 2
                        color: cc.light_green
                        text: "" + (index + 1)
                    }
                }

                Rectangle
                {
                    height: parent.height
                    width: lv.width/4 - 32
                    color: selected ? cc.highlight_green : cc.dark_blue

                    Text
                    {
                        anchors.centerIn: parent
                        font.family: fontConf.mainFont
                        font.pixelSize: fontConf.pixSize_S6 - 2
                        color: selected ? cc.white : rowElem.lineColor
                        text: time_start
                    }
                }

                Rectangle
                {
                    height: parent.height
                    width: lv.width/4 - 32
                    color: selected ? cc.highlight_green : cc.dark_blue

                    Text
                    {
                        anchors.centerIn: parent
                        font.family: fontConf.mainFont
                        font.pixelSize: fontConf.pixSize_S6 - 2
                        color: selected ? cc.white : rowElem.lineColor
                        text: time_end
                    }
                }

                Rectangle
                {
                    height: parent.height
                    width: lv.width/2 - 32 - 150
                    color: selected ? cc.highlight_green : cc.dark_blue


                    Text
                    {
                        anchors.centerIn: parent
                        font.family: fontConf.mainFont
                        font.pixelSize: fontConf.pixSize_S6 - 2
                        color: selected ? cc.white : rowElem.lineColor
                        text: duration
                    }
                }

                Rectangle
                {
                    id:checkRect
                    height: parent.height
                    width: 148
                    color: selected ? cc.highlight_green : cc.dark_blue

                    CheckBox
                    {
                        id: checkBoxButton
                        anchors.fill: parent
                        checked: selected
                        visible: true//emptyElement ? false : true


                        onCheckedChanged:
                        {
                            if( checked && selected )
                                return
                            root.selectRowOnly(index, "selected", checked, false)
                        }

                        indicator: Rectangle {
                            anchors.centerIn: parent
                            implicitWidth: 35
                            implicitHeight: 35
                            color: cc.white
                            scale: checkBoxButton.down ? 0.9 : 1

                            Image
                            {
                                width: 30
                                height: 30
                                anchors.centerIn: parent
                                fillMode: Image.PreserveAspectFit
                                mipmap: true
                                source: "qrc:///icons/checkmark.png"
                                visible: checkBoxButton.checked
                            }
                        }
                    }
                }
            }

            MouseArea
            {
                height: rowElem.height
                width: rowElem.width - checkRect.width

                onPressAndHold:
                {
                    var pointerPos = mapToItem(root, mouseX, mouseY)
                    root.selectRow(index, "selected", true, pointerPos)
                }
            }
        }
    }


    ListModel
    {
      id: sessionsModel

      ListElement { emptyElement: false; colorType: "1"; startTime:"12:00:00"; startDate:"30.11.2019"; endTime:"13:01:00"; endDate:"1.12.2019"; duration:"00:01:01"; selected:false }
      ListElement { emptyElement: false; colorType: "2"; startTime:"00:00:00"; startDate:"01.12.2019"; endTime:"00:01:40"; endDate:"1.12.2019"; duration:"00:01:40"; selected:false }
      ListElement { emptyElement: true; startTime:""; startDate:""; endTime:""; endDate:""; duration:""; selected:false }
      ListElement { emptyElement: true; startTime:""; startDate:""; endTime:""; endDate:""; duration:""; selected:false }
      ListElement { emptyElement: true; startTime:""; startDate:""; endTime:""; endDate:""; duration:""; selected:false }
      ListElement { emptyElement: true; startTime:""; startDate:""; endTime:""; endDate:""; duration:""; selected:false }
      ListElement { emptyElement: true; startTime:""; startDate:""; endTime:""; endDate:""; duration:""; selected:false }
    }

}
