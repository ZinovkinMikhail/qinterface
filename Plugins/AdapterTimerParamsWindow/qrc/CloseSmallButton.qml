import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Button
{
    x: -width/3
    y: -height/3
    height: 60
    width: 70
    scale: down ? 0.8 : 1

    ColorConfig{id:cc}

    background: Rectangle
        {
            radius: 14
            color: cc.light_green
        }


    ShadowRect{x: -3; y: 3; radius: 14}


    Image
    {
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        scale: 0.5
        source: "qrc:///AdapterTimers/icons/cancel_button.png"
    }
}
