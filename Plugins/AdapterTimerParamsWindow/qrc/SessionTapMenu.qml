import QtQuick 2.9
import QtQuick.Controls 2.2
import Cicada 1.0



Rectangle
{
    id:sessionTapMenu
    width: 210
    height: columnElem.height
    radius: 10
    color: cc.white

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
    ShadowRect{x: -6; y: 6; radius: 10}

    signal selectAll()
    signal copy()
    signal change()
    signal deleteButt()
    signal hideMenu()

    function click(id)
    {
        if( id === "selectAll" )
        {
            selectAll()
        }
        if(id === "copy")
        {
            copy()
        }
        if(id === "change")
        {
            change()
        }
        if(id === "delete")
        {
            deleteButt()
        }
    }


    Component.onCompleted:
    {
        selectAllButton.item.text = "Выбрать всё"
        deleteButton.item.text = "Удалить"
        deleteButton.item.objectName = "delete"
        changeButton.item.text = "Изменить"
        changeButton.item.objectName = "change"
        copyButton.item.text = "Дублировать"
        copyButton.item.objectName = "copy"
        selectAllButton.item.objectName = "selectAll"
    }


    Button
    {
        id:blockZone
        x:-parent.x
        y:-parent.y - 150
        width:1280
        height: 720
        z:-1

        background: Rectangle {opacity: 0}

        onClicked:
        {
            sessionTapMenu.hideMenu()
            sessionTapMenu.visible = false
        }
    }

    Button // blockZone fix
    {
        anchors.fill: parent
        background: Rectangle {opacity: 0}
    }


    Column
    {
        id:columnElem
        anchors.horizontalCenter: parent.horizontalCenter
        topPadding: 10
        bottomPadding: 10
        spacing: 10

        Loader{ sourceComponent: tapButton; id:selectAllButton; objectName: "selectAll" }
        Loader{ sourceComponent: line; anchors.horizontalCenter: parent.horizontalCenter }
        Loader{ sourceComponent: tapButton; id:changeButton }
        Loader{ sourceComponent: line; anchors.horizontalCenter: parent.horizontalCenter }
        Loader{ sourceComponent: tapButton; id:copyButton }
        Loader{ sourceComponent: line; anchors.horizontalCenter: parent.horizontalCenter }
        Loader{ sourceComponent: tapButton; id:deleteButton }
    }


    Component
    {
        id:tapButton

        Button
        {
            signal click()
            width: sessionTapMenu.width
            height: 28
            scale: down ? 0.9 : 1
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S6 + 3

            onClicked:
            {
                sessionTapMenu.click(objectName)
                sessionTapMenu.visible = false
            }

            background: Rectangle { opacity: 0 }

            contentItem: Text
            {
                text: parent.text
                font: parent.font
                color: cc.blue
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }
        }
    }

    Component
    {
        id:line

        Rectangle
        {
            width: 180
            height: 2
            color: cc.blue
            opacity: 0.6
        }
    }
}
