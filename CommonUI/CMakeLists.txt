cmake_minimum_required(VERSION 3.5)
project(commonplugin)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Core Test REQUIRED)


add_library(${PROJECT_NAME} SHARED import/icons.qrc)


include_directories(${Qt5Widgets_INCLUDE_DIRS} ${CMAKE_BINARY_DIR})

target_compile_definitions(${PROJECT_NAME}
        PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(${PROJECT_NAME} Qt5::Core Qt5::Qml Qt5::Quick )

set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/Cicada)


set(QMLDIR Cicada)
set (DIRINSTALL share/${PROJECT_NAME})
install(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION ${DIRINSTALL}/${QMLDIR})

configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)

add_custom_target(uninstall_${PROJECT_NAME}
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)

add_subdirectory(import)



