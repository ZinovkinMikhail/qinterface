#ifndef COMMON_H
#define COMMON_H

#include <QQmlExtensionPlugin>

class Common : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)
public:
    void registerTypes(const char *uri) override
    {
        Q_UNUSED(uri);
    }
};

#endif // COMMON_H
