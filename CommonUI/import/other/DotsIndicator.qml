//import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2


Item
{
    property real indicatorWidth: indicator.width
    property real indicatorHeight: indicator.height
    property real indicatorX: 0
    property real indicatorY: 0

    ColorConfig{id:cc}

    PageIndicator
    {
        id: indicator
        count: swView.count
        x: indicatorX
        y: indicatorY
        currentIndex: swView.currentIndex

        delegate:
            Rectangle
            {
                implicitWidth: 10
                implicitHeight: 10
                radius: width / 2
                border.color: cc.white
                border.width: 1

                color: index === indicator.currentIndex ? cc.blue : cc.white

                Behavior on color
                {
                    ColorAnimation
                    {
                        from: cc.white
                        duration: 200
                    }
                }
            }
    }
}


