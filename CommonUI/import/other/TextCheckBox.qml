import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2



CheckBox
{
    id:root

    property bool rightCheckbox: true
    property string checkName: ""

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    objectName: checkName


    indicator: Rectangle
        {
            implicitWidth: 40
            implicitHeight: 40
            x: rightCheckbox ? parent.width - width - parent.rightPadding : 0
            y: parent.height / 2 - height / 2
            color: cc.white
            scale: down ? 0.9 : 1

            ShadowRect{x:-4; y: 4}

            Image
            {
                width: 30
                height: 30
                anchors.centerIn: parent
                fillMode: Image.PreserveAspectFit
                mipmap: true
                source: "qrc:///icons/i34.png"

                visible: checked
            }
        }


    contentItem: Text
        {
            text: parent.text
            font.family: fontConf.mainFont
            font.pixelSize: fontConf.pixSize_S6
            color: cc.light_green
            scale: down ? 0.9 : 1

            rightPadding: rightCheckbox ? root.indicator.width + root.spacing : 0
            leftPadding: rightCheckbox ? 0 : root.indicator.width + root.spacing
            verticalAlignment: Text.AlignVCenter
        }
}
