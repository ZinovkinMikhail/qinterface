import QtQuick 2.9



Rectangle
{
    id: rect

    property string type: ""

    onTypeChanged:
    {
        switch (type)
        {
          case "panel":
            rect.x = -6;
            rect.y = 6;
            rect.radius = 3;
            break;
          case "inner":
            rect.x = 6;
            rect.y = -6;
            rect.radius = 3;
            break;
          case "custom":
            break;
        }
    }



    z:-3
    height: parent.height
    width: parent.width + Math.abs(rect.x)*2
    color: "#50000000"
}
