import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2



Text
{
    x: 1280 - width - 25
    y: 720 - height/2 - 258
    font.family: fontConf.backFont
    font.pixelSize: fontConf.pixSize_S1
    color: cc.blue

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}
}

