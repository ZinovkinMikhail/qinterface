import QtQuick 2.9
import QtQuick.Controls 2.2


SequentialAnimation
{
    property var targetName: control
    property var shadowName: shadow


    onStarted: shadowName.opacity = 0
    onStopped: shadowName.opacity = 1

        NumberAnimation
        {
            target: targetName
            properties: "scale"
            duration: 50
            easing.type: Easing.InCubic
            from: 1
            to: 0.9
        }
        NumberAnimation
        {
            target: targetName
            properties: "scale"
            duration: 50
            easing.type: Easing.InCubic
            from: 0.9
            to: 1
        }

}
