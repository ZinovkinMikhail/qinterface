import QtQuick 2.9

Item
{
    readonly property string mainFont: "Source Sans Pro"
    readonly property string backFont: "CharterITC"     // BackgroundTitle
    readonly property string controlFont: "Open Sans"   // ControlListButton - normal type, Time

    readonly property int pixSize_S1: 62   // BackgroundTitle
    readonly property int pixSize_S2: 56   // Time text
    readonly property int pixSize_S3: 48   // Channel
    readonly property int pixSize_S4: 38   // ComboDrum
    readonly property int pixSize_S5: 26   // ControlListButton, Middle text
    readonly property int pixSize_S6: 22   // ButtonDrop, Small text
    readonly property int pixSize_S7: 18   // Smallest text
}
