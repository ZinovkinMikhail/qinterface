import QtQuick 2.9

Item
{
    readonly property string blue: "#3e606f"
    readonly property string dark_blue: "#193441"
    readonly property string red: "#e74d3c"
    readonly property string green: "#00a388"
    readonly property string light_green: "#d1dbbd"
    readonly property string highlight_green: "#91aa9d"
    readonly property string white: "#fcfff5"
    readonly property string fog: "#6c7980" //for block zone
    readonly property string sessionSeparator: "#91aa9d"
}
