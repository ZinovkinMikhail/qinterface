import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2


Button
{
    id: control
    height: 52
    width: 220
    enabled: true

    property string txttxt: ""
    property string type: ""
    signal buttonClick

    property bool longStyle: false
    onLongStyleChanged:
    {
        if(longStyle)
        {
            control.width = 280
            txt.x = 20
            triang.x = control.width - 80
        }
    }

    property bool longlongStyle: false
    onLonglongStyleChanged:
    {
        if(longlongStyle)
        {
            control.width = 452
            txt.x = 40
            triang.x = control.width - 100
        }
    }

    property bool longlonglongStyle: false
    onLonglonglongStyleChanged:
    {
        if(longlonglongStyle)
        {
            control.width = 500
            txt.x = 40
            triang.x = control.width - 100
        }
    }



    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    background:
        Rectangle
        {
            id: back
            implicitWidth: 100
            implicitHeight: 40
            border.color: "#2d4752"
            border.width: 1
            color: cc.blue
            radius: 2
        }

    function setState(change)
    {
        if( change )
        {
            control.state = 'changed'
        }
        else
        {
            control.state = ""
        }
    }

    onClicked:
    {
        anim.start();
        timer.start();
    }


    ShadowRect{id: shadow; x: -4; y: 4; radius: 3}


    states:
    [
        State
        {
            name: "changed"
            PropertyChanges { target: triangle; color: "#eec070" }
            PropertyChanges { target: txt; color: "#eec070" }
        },
        State
        {
            name: "off"
            PropertyChanges { target: triangle; color: "grey" }
            PropertyChanges { target: txt; color: "grey" }
            PropertyChanges { target: control; enabled: false }
        }
    ]



    PressAnimation
    {
        id: anim
        targetName: control
        shadowName: shadow
    }

    Timer {
        id: timer
        running: false
        repeat: false
        interval: 10
        onTriggered: buttonClick()
    }


    Text
    {
        id:txt
        x: 90 - width/2
        y: parent.height/2 - height/2
        font.family: fontConf.mainFont
        font.pixelSize: fontConf.pixSize_S6
        text: txttxt
        color: cc.light_green
    }



    Item
    {
        id:triang
        x: 120
        y: -18
        height: 100
        width: 120
        clip: true
        scale: 0.3

        Rectangle
        {
            id:triangle
            x: parent.width/2 - 40
            y: -50
            height: 100
            width: 100
            color: cc.light_green
            radius: 10
            transform:
            [
                Rotation { origin.x: 50; origin.y: 50; angle: 45},
                Scale { origin.x: 0; origin.y: 0; xScale: 0.6}
            ]
        }
    }
}

