import Cicada 1.0
import QtQuick 2.9
import QtQuick.Controls 2.2

Button
{
    id: controlListButton
    height: 66
    width: 220

    property string type: ""
    property string activeStateColor: ""
    property bool testStates

    ColorConfig{id:cc}
    FontsConfig{id:fontConf}

    function setState(change)
    {
        if( change )
        {
            controlListButton.state = 'active'
        }
        else
        {
            controlListButton.state = ""
        }
    }

    onTypeChanged:
    {
        switch (type)
        {
          case "Record":
            controlListButton.font.pixelSize = fontConf.pixSize_S5 + 2
            controlListButton.text = qsTr("ЗАПИСЬ")
            controlListButton.testStates = true
            controlListButton.activeStateColor = cc.red
            break;
          case "Monitoring":
            controlListButton.font.pixelSize = fontConf.pixSize_S5
            controlListButton.text = qsTr("МОНИТОРИНГ")
            controlListButton.testStates = true
            controlListButton.activeStateColor = cc.green
            break;
          case "Apply":
            controlListButton.font.pixelSize = fontConf.pixSize_S5
            controlListButton.text = qsTr("Применить")
            controlListButton.testStates = false
            break;
          case "SlimType":
            controlListButton.font.pixelSize = fontConf.pixSize_S5
            controlListButton.testStates = false
            controlListButton.height = 60
            back.radius = 0
            controlListButton.font.family = fontConf.mainFont
            controlListButton.font.weight = Font.Normal
            txt.color = cc.dark_blue
            break;
        }
    }


    font.family: fontConf.controlFont
    font.weight: Font.DemiBold
    onClicked: anim.start()


    contentItem:
        Text
        {
            id: txt
            text: controlListButton.text
            font: controlListButton.font
            opacity: enabled ? 1.0 : 0.3
            color: cc.blue
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

    background:
        Rectangle
        {
            id: back
            implicitWidth: 100
            implicitHeight: 40
            opacity: enabled ? 1 : 0.3
            color: cc.light_green
            radius: 2
        }


    ShadowRect{id: shadow; x: -4; y: 4; radius: 3}


    states:
        State
        {
            name: "active"
            PropertyChanges { target: back; color: activeStateColor }
            PropertyChanges { target: txt; text: "СТОП"; color: cc.light_green}
        }


    PressAnimation
    {
        id: anim
        targetName: controlListButton
        shadowName: shadow
//        onStopped: controlListButton.testStates ? controlListButton.state == 'active' ? controlListButton.state = "" : controlListButton.state = 'active' : 0;
    }

}
