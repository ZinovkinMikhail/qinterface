1.Зависимости проекта:
	1) rosslog - git@gitlab.ross-jsc.ru:ROSS/rossutils.git(master)

	2) axis-lib - git clone git@git.ross-jsc.ru:axis_lib(feature-to-pre-master3530)   -> -lpthread -lm -lssl -lcrypto
		Сборка: 1- make gitupdate
			2- make i386
			3- make i386_install
 

2. Сборка проекта из терминала:
	1) export LD_LIBRARY_PATH="../DataProvider"
	2) ~/Qt5.9.6/5.9.6/gcc_64/bin/qmake CONFIG+=TESTS CONFIG+=MASTER ../DataProvider.pro && /usr/bin/make

3. Дополнительные аргументы сборки:
	1) Сборка проекта стандартная:
		CONFIG+=SLAVE
	2) Сборка проекта с тестами провайдера( DataProvider ):
		CONFIG+=DATA_PROVIDER_TEST CONFIG+=SLAVE CONFIG+=TESTS
	3) Сборка проекта с тестами провайдера как Мастер( DataProvider ):
		CONFIG+=DATA_PROVIDER_TEST CONFIG+=MASTER CONFIG+=TESTS

*При запуске Мастера и Клиента при сборке с тестами DataProvider происходит внутреннее тестирование команд адаптеру.
*Класс - DataProviderTests связь библиотеки DataProvider с тестами. 
	Там добавляем новые или корректируем аргументы существующих для теста с cm_adapter

4. Содержание:

	1) DataProvider - библиотека которая отвечает за общение интерфейса с адаптером
	2) Plugins - директория с плагинами ( библиотеками *.so ) интерфейса.
		Директории новых плагинов добавляются в Plugins.pro В SUBDIRS ( SUBDIRS = MonitoringWindow \
										 	 YourWindow )
		Создавать новые плагины на примере других.
	3)App - сам интерфейс

5. Дополнительно:

	1. Если не стоит noexcept(true) в заголовке функции, то она может выбросить исключение.
	2. Если стоит #warning то это будет доделано. 

6. Есть библиотека CommonUI с общими qml файлами с поддержкой qml + c++ . Если есть общие
    кастомные элементы, то они добавляются в эту библиотеку.

    В главном проекте или плагинах , которые импортируют эту библиотеку в qml файлах
    добавляем import Cicada 1.0  и все подцепится.

    Правила добавления в библиотеку CommonUI:

    1) Добавить в нужную директорию по смыслу /CommonUI/import/"buttons"
        *Обязательно в import
        Если директория новая, то прописать в import.pro в cpqmldir.files ( сделать по аналогии с buttons )
    2) Добавить сам файл в qmldir строку с наименованием файла и версию TestButton 1.0 TestButton.qml


Нужно сделать:
    *Нужно доделать красивую установку всех файлов
